#!/bin/bash

# It isn't strictly necessary, but set -euxo pipefail turns on a few useful features.
#
# set -e makes bash exit if a command fails.
# set -u makes bash exit if a variable is undefined.
# set -o pipefail makes bash exit if a command in a pipeline fails.
set -euo pipefail

GITLAB_LABEL=broken-link
EXISTING_ISSUE_IID=0
ISSUES_API_URL="$CI_API_V4_URL/projects/$CI_PROJECT_ID/issues"

function find_existing_issue() {
	# Returns the first issue that is created by the access token or 0 otherwise
	EXISTING_ISSUE_IID=$(curl --header "Private-Token: $BROKEN_LINK_CHECK_ACCESS_TOKEN" "$ISSUES_API_URL?labels=broken-link-checker-issue" | jq -r '.[0].iid // 0')
}

function create_new_issue() {
	curl --request POST \
	 	--header "Private-Token: $BROKEN_LINK_CHECK_ACCESS_TOKEN" \
		--data-urlencode "title=Broken links" \
		--data-urlencode "description=$(cat broken-links.md)" \
		--data-urlencode "labels=$GITLAB_LABEL" \
		"$ISSUES_API_URL"
}

function update_existing_issue() {
	curl --request PUT \
	 	--header "Private-Token: $BROKEN_LINK_CHECK_ACCESS_TOKEN" \
		--data-urlencode "description=$(cat broken-links.md)" \
		--data-urlencode "state_event=reopen" \
		"$ISSUES_API_URL/$EXISTING_ISSUE_IID"
}

function info() {
	# prints a bold green text
	echo -e "\e[1m\e[32m$*"

	# resets the formatting
	echo -e "\e[0m"
}

info "Searching for existing issue..."
find_existing_issue

if [ $EXISTING_ISSUE_IID == 0 ]; then
	info "Found no issue yet, creating a new one."
	create_new_issue
else
	info "Updating existing issue $EXISTING_ISSUE_IID"
	update_existing_issue
fi
