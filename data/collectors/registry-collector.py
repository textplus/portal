import json
import os
import shutil
import sys

import requests

registry_url = "https://registry.text-plus.org/api/v1/e/dienst/"
service_assets_folder = "themes/textplus/assets/img/service-images"


def download_and_store_image(url: str, filename: str) -> str | None:
    # download image
    img_response = requests.get(url, stream=True)
    if img_response.status_code == 200:
        # save to disk
        with open(f'{service_assets_folder}/{filename}', 'wb') as img_file:
            img_response.raw.decode_content = True
            shutil.copyfileobj(img_response.raw, img_file)
            return filename
    return None


registry_json_request = requests.get(registry_url)
if registry_json_request.status_code == 200:
    path = f"data/registry-services.json"
    registry_json = registry_json_request.json()

    # clear all service images from asset folder in order to clean old or changed images
    os.system(f'rm -rf {service_assets_folder}/*')

    for item in registry_json["items"]:
        # check if service has image
        if "image" in item["properties"]:
            img_url = item["properties"]["image"][0]["url"]
            # check if filename for image is set
            if "filename" in item["properties"]["image"][0]:
                download_and_store_image(url=img_url,
                                         filename=item["properties"]["image"][0]['filename'])
            else:
                # if filename is not set in json, we need to extract and set it
                img_filename = os.path.basename(img_url)
                download_and_store_image(url=item["properties"]["image"][0]['url'],
                                         filename=img_filename)
                # set filename in json
                item["properties"]["image"][0]["filename"] = img_filename
    # write json to folder
    with open(path, 'w') as output_file:
        json.dump(registry_json, output_file, indent=2)
else: 
    sys.exit(f"Could not fetch json from {registry_url}")
