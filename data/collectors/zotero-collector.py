# This script collects entries from Zotero using pyzotero and stores them to a local JSON without any further processing
# This is needed, since the response from the API slows down every Hugo build
# Should run every night scheduled by the GitLab CI to keep the JSON updated
import json
import sys
import time

from pyzotero import zotero, zotero_errors


def fetch_zotero_entries(locale: str) -> list:
    # init some variables
    retry_request_max = 3

    # initialize Text+ library object
    tplus_zotero_library = zotero.Zotero(library_id='4533881', library_type='group', locale=locale)

    for i in range(retry_request_max):
        # this loop is needed, because the zotero library is big and API timeouts occur often
        try:
            # add required formats to request
            tplus_zotero_library.add_parameters(format='json', include='bibtex,bib,csljson,data', linkwrap='1')

            # request top level items and wrap them in zotero.everything
            # a single top() request would only allow up to 100 items per request
            tplus_entries = tplus_zotero_library.everything(tplus_zotero_library.top())
            return tplus_entries
        except zotero_errors.HTTPError:
            # wait for 180 seconds, since the Zotero API somtimes needs time to generate the answer
            print("Zotero API timeout. Trying again in 180 seconds")
            time.sleep(180)
            pass
    raise TimeoutError(f"Zotero API did not respond in time after {retry_request_max} retries")


def write_zotero_json(data, locale: str):
    path = f"data/zotero-unprocessed.{locale}.json"
    with open(path, 'w') as output_file:
        json.dump(data, output_file, indent=2)


def refresh_json(locale: str):
    zotero_items = fetch_zotero_entries(locale=locale)
    if zotero_items is not None:
        print(f"Successfully fetched entries for {locale}")
        write_zotero_json(zotero_items, locale)
    else:
        sys.exit(1)


refresh_json("de-DE")
refresh_json("en-US")
