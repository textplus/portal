# Search

Page search is done using FlexSearch, much is copied/adopted from the zen theme (https://github.com/frjo/hugo-theme-zen#search).

Necessary files:
- https://github.com/frjo/hugo-theme-zen/blob/main/assets/js/search.js
- https://github.com/frjo/hugo-theme-zen/blob/main/layouts/home.searchindex.json

Also needs following config options to generate `searchindex.json`:

```toml
[outputs]
home = [ "HTML", "SearchIndex" ]

[outputFormats.SearchIndex]
mediaType = "application/json"
baseName = "searchindex"
isPlainText = true
notAlternative = true
```

## Advanced Search Integration

- on each update, run
  ```bash
  # docker compose down -v
  npm uninstall textplus-fcs-vuetify
  npm install git+https://git.saw-leipzig.de/text-plus/FCS/textplus-fcs-vuetify.git
  docker compose build
  ```
  to force a fresh reinstall of the advanced search integration package;  
  this updates the commit hashes of the git dependency links in the `package[-lock].json` to fetch and use the latest sources
- run the web portal with `docker compose up`

### Changes

- there does not seem to be any major conflict between Bootstrap and Vuetify (Material UI) styles
- `config/_default/module.toml` defines the static dependencies to styles
- `config/_default/hugo.toml` configures the API URLs in the `[params]` section
- `themes/textplus/assets/js/advancedsearch.ts` sets up the Vue app (and includes the javascript sources)
- `themes/textplus/layouts/partials/head.html` includes the custom textplus-fcs-vuetify style (note that the `themes/textplus/assets/sass/main.scss` does not seem to be able to import vanilla CSS due to PostCSS ...)
- `themes/textplus/layouts/partials/header.html` for now adds the search button to include the search in all pages
