#!/bin/bash

# see: https://gitlab-ce.gwdg.de/core-dev/hugo/automatic-review-apps/-/blob/main/deploy.sh
# almost everything here was copied from there and written by Jasmin 

# It isn't strictly necessary, but set -euxo pipefail turns on a few useful features.
#
# set -e makes bash exit if a command fails.
# set -u makes bash exit if a variable is undefined.
# set -o pipefail makes bash exit if a command in a pipeline fails.
set -euo pipefail

# CC-BY-SA 3.0 okdewit: https://stackoverflow.com/a/49035906
# Adjusted to limit the length to 63 characters for DNS usage.
slugify () {
    echo "$1" | iconv -t ascii//TRANSLIT | cut -b1-63 | sed -r s/[~\^]+//g | sed -r s/[^a-zA-Z0-9]+/-/g | sed -r s/^-+\|-+$//g | tr A-Z a-z
}

function build_slug() {
	# We can override the slug on a per-job basis. This is done by setting the SLUG environment variable.
	# The following check evaluates to nothing if `SLUG` is unset and to `x` otherwise.
	if [ -z ${SLUG+x} ]; then
		# If the slug is not set, we calculate it based on the current CI variables.
		SLUG=$(slugify "$CI_PROJECT_PATH_SLUG-$CI_COMMIT_REF_SLUG-${SLUG_SUFFIX:-}")

		if [[ ${CI_COMMIT_BRANCH:-undefined} == $CI_DEFAULT_BRANCH ]]; then
			SLUG=$CI_PROJECT_NAME
		fi
	else
		debug "SLUG is set to $SLUG, overriding the default values."
	fi

	DEPLOY_FINAL_URL="${CI_ENVIRONMENT_URL:-https://$SLUG.$STAGE_BASE_URL}"

	echo "DEPLOY_SLUG=$SLUG" >> deploy.env
	echo "DEPLOY_FINAL_URL=$DEPLOY_FINAL_URL" >> deploy.env
}

function build() {
	hugo --cacheDir="$CI_PROJECT_DIR/resources/" --minify --environment "$HUGO_ENVIRONMENT" --baseURL "$DEPLOY_FINAL_URL" --templateMetrics
}

function deploy() {
	# Explanation of the rules:
	#
	# 1. First, we use an explicit version of the --archive flag which deliberately omits the time comparison. It's not useful, as Git does not store timestamps.
	# 2. Display a progress bar for the transmission.
	# 3. Use a checksum for a comparison, everything else would be useless.
	# 4. Delete files that do not longer exist in the source folder, e.g. updated CSS files and stuff, after the transfer is completed.
	# 5. Update all files after the transfer has been completed. It's not completely atomic but reduces the time for deployments to roll out.
	# 6. Pass additional options, e.g. the --link-dest stuff to the command.
	#
	rsync \
	--recursive --perms --owner --group \
	--info=progress2 --human-readable \
	--checksum \
	--delete --delete-after \
	--delay-updates \
	./public/ "/var/www/$SLUG"
	rm -rf ./public/
}

function delete_deployment() {
	info "Deleting deployment for environment: $CI_ENVIRONMENT_NAME"
	echo "Deleting /var/www/$SLUG"
	rm -rf "/var/www/$SLUG"
	echo "...done."
}

function info() {
	# prints a bold green text
	echo -e "\e[1m\e[32m$*"

	# resets the formatting
	echo -e "\e[0m"
}

function debug() {
	# prints bold text
	echo -e "\e[1m$*"

	# resets the formatting
	echo -e "\e[0m"
}

build_slug

if [ "${1:-fallback}" == "stop" ]; then
	delete_deployment
	exit 0
fi

if [ "${1:-fallback}" == "deploy" ]; then
	deploy
	
	info "Deployment successful!"

	debug "-> /var/www/$SLUG"
	debug "-> $DEPLOY_FINAL_URL"

	exit 0
fi


info "Building webpage..."
build
