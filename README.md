# Text+

## Beschreibung

- [ ] TODO

## Verwendete Technologien

- Hugo: Static-Site-Generator. Macht aus dem Content in Kombi mit dem Theme eine funktionierende Webseite
- GitLab-CI: generiert mit Hugo die Seite und deployed diese automatisch über die Runner auf den Servern.
- Python: aktualisiert die externen Daten die die Webseite einbindet.
- Docker: wird vor allem als Dev-Tool (s.u.) verwendet, aber auch im GitLab-CI kommt ein (externes) Docker-Image zum Einsatz

## Repo-Aufbau

### Content

Der Content für das Text+ Portal ist als Markdown geschrieben und befindet sich im `content`-Ordner des Repos. Für Seiten, die ggf noch Unterseiten oder Bilder aus Unterordnern enthalten können werden `_index.md` (Listenseiten in Hugo) angelegt, für Seiten ohne Unterseiten/Bilder reicht `index.md` (normale Seiten in Hugo).

### Data

Via GitLab-CI werden die in der Webseite eingebundenen Daten nächtlich um ca. 2:30 aktualisiert. Das betrifft vor allem die [Diensteliste](https://text-plus.org/daten-dienste/dienste/) und die [Bibliographie](https://text-plus.org/themen-dokumentation/bibliographie/). Die jeweiligen Daten werden als `json` im `data`-Ordner abgelegt. Ebenfalls im `data`-Ordner liegen die Python-Scripte, die diese Daten von den externen Quellen (Text+ Registry API und Zotero) abrufen.

### Hugo-Config

Die Hugo-Konfiguration des Portals findet sich im `config`-Ordner und ist aufgeteilt in verschiedene Config-Dateien für verschiedene Deployments (Production/Staging), welche beide von der default-Config erben bzw. Teile dieser überschreiben.
Die Haupt-Config ist in `config/_default/hugo.toml` zu finden. In diesem Ordner sind auch noch weitere Config-Files für Sprach-, Privacy-Einstellungen. In `config/_default/modules.toml` sind einige mounts eingerichtet, die verschiedene npm/css Ordner/Dateien an der für Hugo richtigen Stelle einbinden. Außerdem werden alle deutschsprachigen Content-Seiten mit einem mount auf der englischen Version des Portals als Fallback eingebunden. In `config/_default/server.toml` sind die Einstellungen für den von Hugo mitgelieferten Webserver enthalten, der nur zum Einsatz kommt, wenn das Portal lokal (auf dem eigenen Computer) gestartet wird. Diese Datei hat aber keinen Einfluss auf die Produktiv-/Review-Instanzen.

### Text+ Hugo-Theme

Im `themes/textplus`-Ordner befindet sich das Hugo-Theme für das Text+ Portal. Dieses beinhaltet neben den Hugo-Shortcodes und Partials der Webseite auch die Übersetzungsdateien für Texte die im Code (nicht im Content) genutzt werden. Außerdem finden sich im `themes/textplus/assets`-Ordner das CSS der Seite, die verwendeten JavaScript/TypeScript-Scripte sowie einige statische Bilder (vor allem für die o.g. Dienste).

#### Text+ Theme Layouts, Shortcodes und Partials

Im `themes/textplus/layouts`-Ordner befinden sich Layouts für die verschiedenen Seitentypen (z.B. Landing, Event, etc) die im Text+ Portal genutzt werden. Im Wiki gibt eine [ausführlichere Beschreibung dieser Seitentypen](https://gitlab.gwdg.de/textplus/portal/-/wikis/Nutzer:innen-Dokumentation/Seitentypen) und ihrer Front-Matter Optionen.

Die nutzbaren Shortcodes sind im `themes/textplus/layouts/shortcodes`-Ordner. Auch für die Shortcodes gibt es [eine detaillierte Beschreibung](https://gitlab.gwdg.de/textplus/portal/-/wikis/Nutzer:innen-Dokumentation/Hugo-Shortcodes) im Wiki.

Die Layouts und Shortcodes nutzen außerdem oft als `partial` definierte HTML-Bausteine, welche sich in `themes/textplus/layouts/partials` befinden.

- [ ] TODO: Dokumentation der einzelnen Partials im Wiki

#### Text+ Portal Commons

Einige der Shortcodes und Partials sind in das [Text+ Commons Repo](https://gitlab.gwdg.de/textplus/tplus-portal-commons) ausgelagert um sie in anderen Hugo-Projekten nutzbar zu machen. Das Repo wird als `git submodule` eingebunden und kann via `git submodule update --remote` aktualisiert werden. Die Shortcodes werden durch die Hugo-Config in den Shortcode- bzw. Partial-Ordner gemounted (siehe `config/module.toml`)

Das Text+ Commons Repo enthält folgende Shortcodes:
- Accordion-Item und Accordion
- Button und Button-Row
- Call to Action
- Lead Text
- Image, Gallery, Lightbox und Carousel
- Card und Cards
- Liste der Text+ Zentren
- Institution und Institution-List
- Team-Member und Team
- Raw HTML

## Build und Deploy

Das Text+ Portal ist als [HUGO](https://gohugo.io/)-Seite mit eigenem Theme angelegt. Der Content wird zusammen mit dem Theme in einer GitLab CI/CD-Pipeline von HUGO verarbeitet, sodass die gebauten Seiten von einem simplen Webserver (für text-plus.org aktuell Apache) ausgeliefert werden kann und kein weiteres Backend auf dem Server selbst benötigt wird. Außerdem werden von HUGO bzw PostCSS die JavaScript/Typescript und CSS-Dateien verarbeitet, teilweise gemerged und dabei minimalisiert, damit auch nur CSS-Tags (o.ä.) geladen beim Laden der Seite ausgeliefert werden die tatsächlich für eine korrekte Darstellung der Seite nötig sind.  

Um die Seite lokal und anzuzeigen, müssen die npm-Abhängigkeiten mit `npm i` installiert werden. Außerdem wird eine aktuelle HUGO-Installation benötigt. Die Webseite lässt sich dann mit `hugo server` starten und sollte unter http://localhost:1313 erreichbar.


- [ ] TODO: review-instanzen und server config dafür

### GitLab CI/CD
Für das automatische Deployment und eine Test der Seite wird eine GitLab CI/CD-Pipeline genutzt. Diese sind in `.gitlab-ci.yml` definiert und werden automatisch bei jedem `git push` oder Merge-Request ausgefürt.

Die CI/CD Pipeline enthält u.a. diese Jobs: 
- [ ] TODO Jobs beschreiben
- `build`:
  - `build-review`:
  - `build-staging`:
  - `build-prod`:
- `deploy`:
  - `build-review`:
  - `build-staging`:
  - `build-prod`:
  - `stop_review`:
- `broken_link_check`:
- `update-external-data`:

In der Pipeline wird für die automatischen Commits der nächtlich aktualierten Daten ein Token benötigt, welches jährlich abläuft und erneuert werden muss. Dieses Token kann in den Repo-Settings unter `Access tokens` gesetzt werden, benötigt die Scopes `read_repository` und `write_repository` und die Rolle `Maintainer` (um automatisch auf `main` mergen zu dürfen). Dementsprechend kann ein Token nur von einem Redaktionsmitglied angelegt werden, das selber mindestens die `Maintainer`-Rolle im Repo hat. Anschließend muss das angezeigt Token als CI/CD-Variable mit dem Namen `DATA_COLLECTOR_TOKEN` gespeichert werden. Dies kann unter Settings -> CI/CD -> Variables gemacht werden. Das aktuelle Ablaufdatum für dieses Token ist der 22.09.2025.

Der Link-Checker braucht ebenfalls ein Token um das Gitlab-Issue mit den nicht erreichbaren Links zu aktualisieren. Für dieses wird nur die Rolle `Developer` und der Scope `api` benötigt. Es muss ebenfalls als CI/CD-Variable mit dem Namen `BROKEN_LINK_CHECKER_ACCESS_TOKEN` gesetzt werden. Das aktuelle Ablaufdatum für dieses Token ist der 20.11.2025.
- [ ] TODO: Issue anlegen als Reminder für Token-Erneuerung


### Dev-Tools

- [ ] TODO: expand and describe

- Docker
- Prettier

### Installation und Nutzung

- [ ] TODO: remove?


## Lizenz

- [ ] TODO

## Kontakt

Feedback ist gerne als GitLab-Issue gesehen. Alternativ gibt es ein [Kontaktformular im Text+ Portal](https://text-plus.org/helpdesk/)

## Weiteres

Zusätzlich zu dieser README gibt es noch eine ausführlichere [Nutzer:innen-Dokumentation im Projekt-Wiki](https://gitlab.gwdg.de/textplus/portal/-/wikis/Nutzer:innen-Dokumentation). Außerdem ist die FCS/Suche [hier etwas beschrieben](https://gitlab.gwdg.de/textplus/portal/-/blob/main/SEARCH.md).
