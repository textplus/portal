# Datenschutzerklärung bis zum 10. Juli 2024

Es gilt die Datenschutzerklärung des IDS Mannheim als Herausgeber dieser Seite.

#### Registrierung der Zugriffe

Alle Zugriffe auf diesen Server werden registriert. Es werden Datum und Rechneradresse gespeichert sowie die Information erfasst, auf welches Dokument zugegriffen wurde. Diese Daten können für statistische Zwecke ausgewertet werden. Persönliche Daten werden darüber hinaus nicht gespeichert.

#### Twitter

Diese Seite bietet die Möglichkeit, den Twitter-Account von Text+ in einer Seitenleiste einzuspiegeln. Hierfür ist ein sog. „Opt-in“ installiert mit dem Hinweis, dass nach erfolgter Zustimmung auf diese Weise personenbezogene Daten durch Twitter verarbeitet werden. Es liegt den Nutzenden dieser Seite frei, diese Option nicht zu wählen. Andernfalls werden Daten an Dritte übermittelt. Text+ hat keinerlei Einfluss auf diese externe Datenverarbeitung; es gilt in diesem Fall zusätzlich die Datenschutzerklärung von Twitter (s.u.).

1. Von Twitter verarbeitete Daten

Text+ greift für den hier angebotenen Kurznachrichtendienst auf die technische Plattform und die Dienste der Twitter Inc., 1355 Market Street, Suite 900, San Francisco, CA 94103 U.S.A. zurück. Verantwortlich für die Datenverarbeitung von außerhalb der Vereinigten Staaten lebenden Personen ist die Twitter International Company, One Cumberland Place, Fenian Street, Dublin 2 D02 AX07, Irland.

Wir weisen Sie darauf hin, dass Sie den hier angebotenen Twitter-Kurznachrichtendienst und dessen Funktionen in eigener Verantwortung nutzen. Dies gilt insbesondere für die Nutzung der interaktiven Funktionen (z.B. Teilen, Bewerten).

Angaben darüber, welche Daten durch Twitter verarbeitet und zu welchen Zwecken genutzt werden, finden Sie in der Datenschutzerklärung von Twitter: https://twitter.com/de/privacy

Text+ hat keinen Einfluss auf Art und Umfang der durch Twitter verarbeiteten Daten, die Art der Verarbeitung und Nutzung oder die Weitergabe dieser Daten an Dritte. Text+ hat insoweit keine effektiven Kontrollmöglichkeiten.

Mit der Verwendung von Twitter werden Ihre personenbezogenen Daten von der Twitter Inc. erfasst, übertragen, gespeichert, offengelegt und verwendet und dabei unabhängig von Ihrem Wohnsitz in die Vereinigten Staaten, Irland und jedes andere Land, in dem die Twitter Inc. geschäftlich tätig wird, übertragen und dort gespeichert und genutzt.

Twitter verarbeitet dabei zum einen Ihre freiwillig eingegebenen Daten wie Name und Nutzername, E-Mail-Adresse, Telefonnummer oder die Kontakte Ihres Adressbuches, wenn Sie dieses hochladen oder synchronisieren.

Zum anderen wertet Twitter aber auch die von Ihnen geteilten Inhalte daraufhin aus, an welchen Themen Sie interessiert sind, speichert und verarbeitet vertrauliche Nachrichten, die Sie direkt an andere Nutzer schicken und kann Ihren Standort anhand von GPS-Daten, Informationen zu Drahtlosnetzwerken oder über Ihre IP-Adresse bestimmen, um Ihnen Werbung oder andere Inhalte zukommen zu lassen.

Zur Auswertung benutzt die Twitter Inc. unter Umständen Analyse-Tools wie Twitter- oder Google-Analytics. Text+ hat keinen Einfluss auf eine Nutzung solcher Tools durch die Twitter Inc. und wurde über einen solchen potentiellen Einsatz auch nicht informiert. Sollten Tools dieser Art von der Twitter Inc. für den Account von Text+ eingesetzt werden, hat Text+ dies weder in Auftrag gegeben, noch abgesegnet oder sonst in irgendeiner Art unterstützt. Auch werden die bei der Analyse gewonnenen Daten Text+ nicht zur Verfügung gestellt. Lediglich bestimmte, nicht-personenbezogene Informationen über die Tweet-Aktivität, also etwa die Anzahl der Profil- oder Link-Klicks durch einen bestimmten Tweet, sind für Text+ einsehbar. Überdies hat Text+ keine Möglichkeit, den Einsatz solcher Tools auf dem Twitter-Account zu verhindern oder abzustellen.

Schließlich erhält Twitter auch Informationen, wenn Sie z.B. Inhalte ansehen, auch wenn Sie keinen Account erstellt haben. Bei diesen sog. „Log-Daten” kann es sich um die IP-Adresse, den Browsertyp, das Betriebssystem, Informationen zu der zuvor aufgerufenen Website und den von Ihnen aufgerufenen Seiten, Ihrem Standort, Ihrem Mobilfunkanbieter, dem von Ihnen genutzten Endgerät (einschließlich Geräte-ID und Anwendungs-ID), die von Ihnen verwendeten Suchbegriffe und Cookie-Informationen handeln.

Über in Webseiten eingebundene Twitter-Buttons oder -Widgets und die Verwendung von Cookies ist es Twitter möglich, Ihre Besuche auf diesen Webseiten zu erfassen und Ihrem Twitter-Profil zuzuordnen. Anhand dieser Daten können Inhalte oder Werbung auf Sie zugeschnitten angeboten werden.

Möglichkeiten, die Verarbeitung Ihrer Daten zu beschränken, haben Sie bei den allgemeinen Einstellungen Ihres Twitter-Kontos sowie unter dem Punkt „Datenschutz und Sicherheit“. Darüber hinaus können Sie bei Mobilgeräten (Smartphones, Tablet-Computer) in den dortigen Einstellmöglichkeiten den Zugriff von Twitter auf Kontakt- und Kalenderdaten, Fotos, Standortdaten etc. beschränken. Dies ist jedoch abhängig vom genutzten Betriebssystem.

Weitere Informationen zu diesen Punkten sind auf den folgenden Twitter-Supportseiten vorhanden:
https://support.twitter.com/articles/105576#

https://help.twitter.com/de/search?q=datenschutz

Über die Möglichkeit, eigene Daten bei Twitter einsehen zu können, können Sie sich hier informieren: https://support.twitter.com/articles/20172711#

Informationen über die von Twitter zu Ihnen gezogenen Rückschlüsse finden Sie hier:
https://twitter.com/your_twitter_data

Informationen zu den vorhandenen Personalisierungs- und Datenschutzeinstellmöglichkeiten finden Sie hier (mit weiteren Verweisen):
https://twitter.com/personalization

Weiterhin haben Sie die Möglichkeit, über das Twitter-Datenschutzformular oder die Archivanforderungen Informationen anzufordern:
https://support.twitter.com/forms/privacy

https://support.twitter.com/articles/20170320#

2. Von Text+ verarbeitete Daten von Twitter

Auch Text+ verarbeitet Ihre Daten von Twitter.

a) Kontaktdaten
Verantwortlich ist insoweit der Beauftragte des IDS, wie auch obig verlinkt.

b) Zweck und Rechtsgrundlage
Die Verarbeitung erfolgt für die Zwecke der Öffentlichkeitsarbeit von Text+, namentlich der Sensibilisierung und Aufklärung der Öffentlichkeit, einschließlich Betroffenen, Verantwortlichen und Auftragsverarbeitern, hinsichtlich Risiken, Vorschriften, Garantien, Rechten und Pflichten im Zusammenhang mit Datenverarbeitungen (Art. 57 Abs. 1 lit b, lit d, Art. 58 Abs. 3 lit b, Art. 6 Abs. 1 UAbs. 1 lit e DS-GVO i.V.m. § 4 LDSG Baden-Württemberg).

c) Empfänger
Empfänger der Daten ist zunächst Twitter, wo sie ggf. zu eigenen Zwecken und unter der Verantwortung von Twitter an Dritte weitergegeben werden. Empfänger von Veröffentlichungen ist zudem die Öffentlichkeit, also potentiell jedermann.

d) Kategorien personenbezogener Daten
Zwar erhebt Text+ selbst keine Daten über den Twitter-Account. Aber über die Einbindung der Tweets von Text+ auf der Website werden die IP-Adressen der Seitenbesucher ggf. an die Twitter Inc. übertragen.

Die von Ihnen bei Twitter eingegebenen Daten, insbesondere Ihr Nutzername und die unter Ihrem Account veröffentlichten Inhalte, werden von uns aber insofern verarbeitet, als wir Ihre Tweets gegebenenfalls retweeten oder auf diese antworten oder auch von uns aus Tweets verfassen, die auf Ihren Account verweisen. Die von Ihnen frei bei Twitter veröffentlichten und verbreiteten Daten werden so von Text in dasn Angebot einbezogen und den Followern zugänglich gemacht.

e) Drittlandsübermittlung
Wir übermitteln die von Twitter uns zu Verfügung gestellten Daten nicht weiter. Dies heißt nicht, dass Twitter ihre Daten nicht weiter übermittelt (s.o.).

f) Rechte, Dokumentation, Evaluation
Bei Fragen zu unserem Informationsangebot können Sie uns unter der auf der Seite “Kontakt” angegebene Adresse erreichen. Ihre Rechte auf Auskunft, Einschränkung der Verarbeitung, Widerspruch, Berichtigung oder Löschung von Daten können Sie bei unserem Datenschutzbeauftragten geltend machen (s.o.).

Diese Datenschutzerklärung finden Sie hier in der jeweils geltenden Fassung.

Im Fall von Änderungen der Datenschutzerklärung finden Sie die Vorversionen ebenso an dieser Stelle.

Es wird regelmäßig von uns hinsichtlich des Ob und Wie der Nutzung evaluiert. Die Nutzer werden von uns regelmäßig hinsichtlich der Risiken für ihr Recht auf informationelle Selbstbestimmung sensibilisiert.

Weitere Informationen zu Twitter und anderen Sozialen Netzen und wie Sie Ihre Daten schützen können, finden Sie auch auf youngdata.de.

#### Kontaktformular und E-Mail-Kontakt

1. Beschreibung und Umfang der Datenspeicherung und -verarbeitung

Unsere Website enthält ein Kontaktformular, über das Nutzende direkt mit uns in Verbindung treten können. Wenn Nutzende von dieser Möglichkeit Gebrauch machen, werden die in das Eingabefeld eingegebenen Daten an uns übermittelt und gespeichert. Dies sind: Name und E-Mail-Adresse.
Zudem werden die Uhrzeit und das Datum der jeweiligen Absendung gespeichert. Um die im Zusammenhang mit dem Absenden des Kontaktformulars erhobenen Daten zu verarbeiten, müssen Sie Ihre Einwilligung erteilen und werden auf diese Datenschutzerklärung verwiesen. Alternativ besteht die Möglichkeit, über die angegebene E-Mail-Adresse Kontakt mit uns aufzunehmen. In diesem Fall werden die mit der E-Mail übermittelten personenbezogenen Daten des Nutzers gespeichert. Ihr Anliegen erreicht Mitarbeiterinnen und Mitarbeiter der antragstellenden und beteiligten Institutionen von Text+.

1. Rechtsgrundlage für die Datenverarbeitung

Rechtsgrundlage für die Datenverarbeitung ist die Einwilligung des Nutzers gemäß Artikel 6 (1) lit. a) GDPR.
Rechtsgrundlage für die Verarbeitung der Daten, die mit der Übermittlung der E-Mail versendet werden, ist Art. 6 (1) lit. f) GDPR. Dient der E-Mail-Kontakt dem Abschluss eines Vertrages, so ist die Rechtsgrundlage für die Verarbeitung Artikel 6 (1) lit. b) DSGVO.

2. Zweck der Datenverarbeitung

Die Verarbeitung der personenbezogenen Daten aus dem Eingabefeld erfolgt zur Bearbeitung der Kontaktaufnahme. Erfolgt die Kontaktaufnahme per E-Mail, so liegt auch ein erforderliches berechtigtes Interesse an der Verarbeitung der Daten vor.
Die übrigen bei der Übermittlung verarbeiteten personenbezogenen Daten dienen dazu, einen Missbrauch des Kontaktformulars zu verhindern und die Sicherheit unserer IT-Systeme zu gewährleisten.
Die Anfragen werden inhaltlich ausgewertet. Daten über angefragte Beratungs- und Schulungsaktivitäten werden zum Zwecke des Qualitätsmanagements und der Verbesserung der Dienstleistungen gespeichert.

3. Dauer der Speicherung

Die Daten werden gelöscht, wenn sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind. Für die personenbezogenen Daten aus dem Eingabefeld des Kontaktformulars und diejenigen, die per E-Mail übersandt wurden, ist dies der Fall, wenn die jeweilige Konversation und Auswertung abgeschlossen ist. Die Konversation ist abgeschlossen, wenn sich aus den Umständen entnehmen lässt, dass das jeweilige Anliegen erledigt ist.

4. Möglichkeiten des Widerspruchs und der Löschung

Die Nutzenden haben jederzeit die Möglichkeit, ihre Einwilligung zur Verarbeitung der personenbezogenen Daten zu widerrufen. Nehmen Nutzende per E-Mail-Kontakt mit uns auf, so können sie der Speicherung personenbezogenen Daten jederzeit widersprechen. In einem solchen Fall kann die Konversation jedoch nicht fortgesetzt werden.
