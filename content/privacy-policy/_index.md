---
title: Datenschutzerklärung

aliases:
  - /datenschutzerklaerung
---

# Datenschutzerklärung

Es gilt die [Datenschutzerklärung des IDS Mannheim](https://www.ids-mannheim.de/allgemein/datenschutz/) als Herausgeber dieser Seite.

## Registrierung der Zugriffe

Bei jedem Aufruf unserer Internetseite erfasst unser System automatisiert Daten und Informationen vom Computersystem des aufrufenden Rechners.

Folgende Daten werden hierbei in jedem Fall erhoben:

- Datum des Zugriffs
- Name des auf dem zugreifenden Gerät installierten Betriebssystems
- Name des verwendeten Browsers
- Quellsystem, über welches der Zugriff erfolgt ist
- Die IP-Adresse des zugreifenden Geräts.

Die Daten werden ebenfalls in den Logfiles unseres Systems gespeichert. Eine Speicherung dieser Daten zusammen mit anderen personenbezogenen Daten des Nutzers findet nicht statt.

### Rechtsgrundlage für die Datenverarbeitung

Rechtsgrundlage für die vorübergehende Speicherung der Daten und der Logfiles ist Art. 6 Abs. 1 lit. f DSGVO.

### Zweck der Datenverarbeitung

Die vorübergehende Speicherung der IP-Adresse durch das System ist notwendig, um eine Auslieferung der Website an den Rechner des Nutzers zu ermöglichen. Hierfür muss die IP-Adresse des Nutzers für die Dauer der Sitzung gespeichert bleiben.

Die Speicherung in Logfiles erfolgt, um die Funktionsfähigkeit der Website sicherzustellen. Zudem dienen uns die Daten zur Optimierung der Website und zur Sicherstellung der Sicherheit unserer informationstechnischen Systeme. Eine Auswertung der Daten zu Marketingzwecken findet in diesem Zusammenhang nicht statt.

In diesen Zwecken liegt auch unser berechtigtes Interesse an der Datenverarbeitung nach Art. 6 Abs. 1 lit. f DSGVO vor.

### Dauer der Speicherung

Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind. Im Falle der Erfassung der Daten zur Bereitstellung der Website ist dies der Fall, wenn die jeweilige Sitzung beendet ist.

Im Falle der Speicherung der Daten in Logfiles ist dies nach spätestens sieben Tagen der Fall. Eine darüberhinausgehende Speicherung ist möglich. In diesem Fall werden die IP-Adressen der Nutzer gelöscht oder verfremdet, sodass eine Zuordnung des aufrufenden Clients nicht mehr möglich ist.

### Widerspruchs- und Beseitigungsmöglichkeit

Die Erfassung der Daten zur Bereitstellung der Website und die Speicherung der Daten in Logfiles ist für den Betrieb der Internetseite zwingend erforderlich. Es besteht folglich seitens des Nutzers keine Widerspruchsmöglichkeit.

Alle Zugriffe auf diesen Server werden registriert. Es werden Datum und Rechneradresse gespeichert sowie die Information erfasst, auf welches Dokument zugegriffen wurde. Diese Daten können für statistische Zwecke ausgewertet werden. Persönliche Daten werden darüber hinaus nicht gespeichert.

## Verwendung von Cookies

Unser Webangebot verwendet Cookies. Bei Cookies handelt es sich um Textdateien, die im Internetbrowser bzw. vom Internetbrowser auf dem Computersystem des Nutzers gespeichert werden. Ruft ein Nutzer eine Webseite auf, so kann ein Cookie auf dem Betriebssystem des Nutzers gespeichert werden.

Wir setzen Cookies ein, um unsere Webseiten nutzerfreundlicher zu gestalten. In den Cookies werden dabei folgende Daten gespeichert und übermittelt:

- Anwendungsspezifische Voreinstellungen des Nutzers
- Zugriffs-Token für das Kontaktformular

## Kontaktformulare und E-Mail-Kontakt

Unsere Website enthält Kontaktformulare, über die Nutzende direkt mit uns in Verbindung treten können. Wenn Nutzende von dieser Möglichkeit Gebrauch machen, werden die in die Eingabefelder eingegebenen Daten an uns übermittelt und gespeichert. Dies betrifft alle ausgefüllten bzw. -gewählten Felder; insbesondere:

- Name
- E-Mail-Adresse.

Wenn Nutzende mit einem Formular auch Dateien an uns übermitteln, die personenbezogene Daten enthalten, werden diese Daten in gleicher Weise behandelt wie direkt erfolgte Angaben.

Zudem wird eine Support-Anfrage über einen Service der GWDG versendet. Dieser Service speichert:

- das Datum des Zugriffs
- den Namen des auf dem zugreifenden Gerät installierten Betriebssystems
- den Name des verwendeten Browsers
- das Quellsystem, über welches der Zugriff erfolgt ist
- die IP-Adresse des zugreifenden Geräts.

Um die im Zusammenhang mit dem Absenden des Kontaktformulars erhobenen Daten zu verarbeiten, müssen Sie Ihre Einwilligung erteilen und werden auf diese Datenschutzerklärung verwiesen. Alternativ besteht die Möglichkeit, über die angegebene E-Mail-Adresse Kontakt mit uns aufzunehmen. In diesem Fall werden die mit der E-Mail übermittelten personenbezogenen Daten des Nutzers gespeichert. Ihr Anliegen erreicht Mitarbeiterinnen und Mitarbeiter der antragstellenden und beteiligten Institutionen von Text+.

### Rechtsgrundlage für die Datenverarbeitung

Rechtsgrundlage für die Datenverarbeitung ist die Einwilligung des Nutzers gemäß Artikel 6 (1) lit. a) GDPR.
Rechtsgrundlage für die Verarbeitung der Daten, die mit der Übermittlung der E-Mail versendet werden, ist Art. 6 (1) lit. f) GDPR. Dient der E-Mail-Kontakt dem Abschluss eines Vertrages, so ist die Rechtsgrundlage für die Verarbeitung Artikel 6 (1) lit. b) DSGVO.

### Zweck der Datenverarbeitung

Die Verarbeitung der personenbezogenen Daten aus dem Eingabefeld erfolgt zur Bearbeitung der Kontaktaufnahme. Erfolgt die Kontaktaufnahme per E-Mail, so liegt auch ein erforderliches berechtigtes Interesse an der Verarbeitung der Daten vor. Die übrigen bei der Übermittlung verarbeiteten personenbezogenen Daten dienen dazu, einen Missbrauch des Kontaktformulars zu verhindern und die Sicherheit unserer IT-Systeme zu gewährleisten. Die Anfragen werden inhaltlich ausgewertet. Daten über angefragte Beratungs- und Schulungsaktivitäten werden zum Zwecke des Qualitätsmanagements und der Verbesserung der Dienstleistungen gespeichert.

### Dauer der Speicherung

Die Daten werden gelöscht, wenn sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind. Für die personenbezogenen Daten aus dem Eingabefeld des Kontaktformulars und diejenigen, die per E-Mail übersandt wurden, ist dies der Fall, wenn die jeweilige Konversation und Auswertung abgeschlossen ist. Die Konversation ist abgeschlossen, wenn sich aus den Umständen entnehmen lässt, dass das jeweilige Anliegen erledigt ist.

### Möglichkeiten des Widerspruchs und der Löschung

Die Nutzenden haben jederzeit die Möglichkeit, ihre Einwilligung zur Verarbeitung der personenbezogenen Daten zu widerrufen. Nehmen Nutzende per E-Mail-Kontakt mit uns auf, so können sie der Speicherung personenbezogenen Daten jederzeit widersprechen. In einem solchen Fall kann die Konversation jedoch nicht fortgesetzt werden.

## Federated Content Search von Text+

Über die Suchfunktion unserer Website ist die Text+ Federated Content Search (https://fcs.text-plus.org) nutzbar. Zur Durchführung einer Suche werden an die durch Text+ betriebene Federated Content Search folgende Daten übermittelt:

- Suchbegriff
- ausgewählte Ressource
- Art der Suche

Des Weiteren ist es möglich dass folgende Daten gespeichert werden können:

- Datum des Zugriffs
- Name des auf dem zugreifenden Gerät installierten Betriebssystems
- Name des verwendeten Browsers
- Quellsystem, über welches der Zugriff erfolgt ist
- Die IP-Adresse des zugreifenden Geräts

Weitere Informationen finden Sie in den Datenschutzbestimmungen der [Text+-FCS](https://fcs.text-plus.org/about)

## Webanalyse durch Matomo

Wir nutzen auf unserer Website das Open-Source-Software-Tool Matomo (ehemals PIWIK) zur Analyse des Surfverhaltens unserer Nutzer. Werden Einzelseiten unserer Website aufgerufen, so werden die im folgenden aufgeführten Daten gespeichert.

Im Einzelnen werden über jeden Zugriff/Abruf folgende Basisdaten gespeichert:

- IP-Adresse
- Zuvor besuchte URL (Referrer), sofern vom Browser übermittelt
- Name und Version des Betriebssystems
- Name, Version und Spracheinstellung des Browsers

Zusätzlich werden, sofern JavaScript aktiviert ist, über jeden Zugriff/Abruf folgende Daten gespeichert:

- Besuchte URLs auf dieser Webseite
- Zeitpunkte der Seitenaufrufe
- Art der HTML-Anfragen
- Bildschirmauflösung und Farbtiefe
- Vom Browser unterstützte Techniken und Formate (z. B. Cookies, Java, Flash, PDF, WindowsMedia, QuickTime, Realplayer, Director, SilverLight, Google Gears)

Die Software läuft dabei ausschließlich auf unseren Servern, die in unserem Rechenzentrum durch uns betrieben werden. Eine Speicherung der personenbezogenen Daten der Nutzer findet nur dort statt. Eine Weitergabe der Daten an Dritte erfolgt nicht.

### Rechtsgrundlage für die Datenverarbeitung

Rechtsgrundlage für die vorübergehende Speicherung der Daten und der Logfiles ist Art. 6 Abs. 1 lit. f DSGVO.

### Zweck der Datenverarbeitung

Die Verarbeitung der personenbezogenen Daten der Nutzer ermöglicht uns eine Analyse des Surfverhaltens unserer Nutzer. Wir sind in durch die Auswertung der gewonnen Daten in der Lage, Informationen über die Nutzung der einzelnen Komponenten unserer Webseite zusammenzustellen. Dies hilft uns dabei unsere Webseite und deren Nutzerfreundlichkeit stetig zu verbessern. In diesen Zwecken liegt auch unser berechtigtes Interesse in der Verarbeitung der Daten nach Art. 6 Abs. 1 lit. f DSGVO.

### Dauer der Speicherung

Die Daten werden gelöscht, sobald sie für unsere Aufzeichnungszwecke nicht mehr benötigt werden. In unserem Fall ist dies nach 7 Tagen der Fall.

### Widerspruchs- und Beseitigungsmöglichkeit

Cookies werden auf dem Rechner des Nutzers gespeichert und von diesem an unserer Seite übermittelt. Daher haben Sie als Nutzer auch die volle Kontrolle über die Verwendung von Cookies. Durch eine Änderung der Einstellungen in Ihrem Internetbrowser können Sie die Übertragung von Cookies deaktivieren oder einschränken. Bereits gespeicherte Cookies können jederzeit gelöscht werden. Dies kann auch automatisiert erfolgen. Werden Cookies für unsere Website deaktiviert, können möglicherweise nicht mehr alle Funktionen der Website vollumfänglich genutzt werden

Sie haben folgende unabhängige Möglichkeit, einer Datenerfassung durch Matomo zu widersprechen: Aktivieren Sie in Ihrem Browser die Einstellung „Do-Not-Track“ bzw. „Nicht folgen“. Solange diese Einstellung aktiv ist, wird unser Matomo-System keinerlei Daten von Ihnen speichern. Bitte beachten Sie: Die „Do-Not-Track-Anweisung“ gilt in der Regel nur für das eine Gerät und den einen Browser, in dem Sie die Einstellung aktivieren. Sollten Sie mehrere Geräte oder Browser nutzen, dann müssen Sie „Do-Not-Track“ überall separat aktivieren.

Nähere Informationen zu den Privatsphäreeinstellungen der Matomo Software finden Sie unter folgendem Link: https://matomo.org/docs/privacy/.

## YouTube

Diese Seite bietet die Möglichkeit, einzelne Videos von YouTube einzubinden. Bevor entsprechende Inhalte von YouTube angezeigt werden, wird ein Hinweis zur Übermittlung von personenbezogenen Daten an YouTube angezeigt. Es liegt den Nutzenden dieser Seite frei, diese Option nicht zu wählen. Andernfalls werden Daten an Dritte übermittelt. Wird diese Option ausgewählt, so wird ein Cookie gesetzt der die Auswahl für zukünftige Besuche speichert. Text+ hat keinerlei Einfluss auf diese externe Datenverarbeitung; es gilt in diesem Fall zusätzlich die Datenschutzerklärung von YouTube. Die vollständige Datenschutzerklärung von YouTube finden Sie hier: https://policies.google.com/privacy?hl=de

## X (ehem. Twitter)

Diese Seite bietet die Möglichkeit, einzelne Tweets einzubinden. Bevor entsprechende Inhalte von X angezeigt werden, wird ein Hinweis zur Übermittlung von personenbezogenen Daten an X angezeigt. Es liegt den Nutzenden dieser Seite frei, diese Option nicht zu wählen. Andernfalls werden Daten an Dritte übermittelt. Wird diese Option ausgewählt, so wird ein Cookie gesetzt der die Auswahl für zukünftige Besuche speichert. Text+ hat keinerlei Einfluss auf diese externe Datenverarbeitung; es gilt in diesem Fall zusätzlich die Datenschutzerklärung von X. Die vollständige Datenschutzerklärung von X finden Sie hier: https://twitter.com/de/privacy

## Rechte der betroffenen Person

Ihnen stehen verschiedene Rechte in Bezug auf die Verarbeitung ihrer personenbezogenen Daten zu. Nachfolgend sind diese aufgeführt, zusätzlich sind Verweise auf die Artikel (DSGVO) bzw. Paragraphen (BDSG (2018)) mit detaillierteren Informationen angegeben.

### Auskunftsrecht (DSGVO Art. 15, BDSG §34)

Sie können von dem Verantwortlichen eine Bestätigung darüber verlangen, ob personenbezogene Daten, die Sie betreffen, von uns verarbeitet werden. Dies schließt das Recht ein, Auskunft darüber zu verlangen, ob die Sie betreffenden personenbezogenen Daten in ein Drittland oder an eine internationale Organisation übermittelt werden.

### Recht auf Berichtigung (DSGVO Art. 16)

Sie haben ein Recht auf Berichtigung und/oder Vervollständigung gegenüber dem Verantwortlichen, sofern die verarbeiteten personenbezogenen Daten, die Sie betreffen, unrichtig oder unvollständig sind. Der Verantwortliche hat die Berichtigung unverzüglich vorzunehmen.

### Recht auf Löschung / „Recht auf Vergessen werden“ / Recht auf Einschränkung der Verarbeitung (DSGVO Art. 17, 18, BDSG §35)

Sie haben das Recht, die unverzügliche Löschung ihrer personenbezogenen Daten vom Verantwortlichen zu verlangen. Alternativ können Sie die Einschränkung der Verarbeitung vom Verantwortlichen verlangen.

Einschränkungen sind in der DSGVO und dem BDSG unter den genannten Artikeln bzw. Paragraphen genannt.

### Recht auf Unterrichtung (DSGVO Art. 19)

Haben Sie das Recht auf Berichtigung, Löschung oder Einschränkung der Verarbeitung gegenüber dem Verantwortlichen geltend gemacht, ist dieser verpflichtet, allen Empfängern, denen die Sie betreffenden personenbezogenen Daten offengelegt wurden, diese Berichtigung oder Löschung der Daten oder Einschränkung der Verarbeitung mitzuteilen, es sei denn, dies erweist sich als unmöglich oder ist mit einem unverhältnismäßigen Aufwand verbunden.

Ihnen steht gegenüber dem Verantwortlichen das Recht zu, über diese Empfänger unterrichtet zu werden.

### Recht auf Datenübertragbarkeit (DSGVO Art. 20)

Sie haben das Recht, die Sie betreffenden personenbezogenen Daten, die Sie dem Verantwortlichen bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten.

Ergänzend zur DSGVO ist festzustellen, dass sich die Datenübertragbarkeit bei Massendaten / Nutzerdaten ausschließlich auf die technische Lesbarkeit beschränkt. Das Recht auf Datenübertragbarkeit umfasst nicht, dass die vom Nutzer in einem proprietären Format erstellen Daten vom Verantwortlichen in ein “gängiges”, d.h. standardisiertes Format konvertiert werden.

### Widerspruchsrecht (DSGVO Art. 21, BDSG §36)

Sie haben das Recht, Widerspruch gegen die Verarbeitung einzulegen, wenn diese ausschließlich auf Basis einer Abwägung des Verantwortlichen geschieht (vgl. DSGVO Art. 6 Abs. 1 lit f);
Recht auf Widerruf der datenschutzrechtlichen Einwilligungserklärung (DSGVO Art. 7 Abs. 3)

Sie haben das Recht, Ihre datenschutzrechtliche Einwilligungserklärung jederzeit zu widerrufen. Durch den Widerruf der Einwilligung wird die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung nicht berührt.

### Recht auf Beschwerde bei einer Aufsichtsbehörde

Unbeschadet eines anderweitigen verwaltungsrechtlichen oder gerichtlichen Rechtsbehelfs steht Ihnen das Recht auf Beschwerde bei einer Aufsichtsbehörde, insbesondere in dem Mitgliedstaat ihres Aufenthaltsorts, ihres Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes, zu, wenn Sie der Ansicht sind, dass die Verarbeitung der Sie betreffenden personenbezogenen Daten gegen die DSGVO verstößt.

## Vorherige Versionen der Datenschutzerklärung
- [Datenschutzerklärung bis zum 3. Februar 2025](privacy-policy/old/privacy-policy-pre-2025-02-03)
- [Datenschutzerklärung bis zum 10. Juli 2024](privacy-policy/old/privacy-policy-pre-2024-07-10)