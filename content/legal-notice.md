---
title: "Impressum"

aliases:
- /impressum

---

# Impressum

#### Herausgeber

Leibniz-Institut für Deutsche Sprache (IDS)
R 5, 6-13
D-68161 Mannheim

#### Förderung

Das NFDI-Konsortium Text+ wird gefördert durch die Deutsche Forschungsgemeinschaft (DFG) – Projektnummer 460033370.

#### Gesetzlicher Vertreter

Wissenschaftlicher Direktor des IDS: Prof. Dr. Henning Lobin

#### Zuständige Aufsichtsbehörde

Regierungspräsidium Karlsruhe
76247 Karlsruhe

Umsatzsteuer-Identifikationsnummer gemäß § 27 a Umsatzsteuergesetz: DE 143845359

#### Redaktion

Inhaltlich verantwortlich: Prof. Dr. Henning Lobin

Redaktion: Text+ Konsortium

#### Haftungsausschluss

Das IDS ist als Anbieter für die eigenen Inhalte, die zur Nutzung bereitgehalten werden, nach den allgemeinen Gesetzen verantwortlich. Von diesen eigenen Inhalten sind Querverweise („externe Links“) auf die von anderen Anbietern bereit gehaltenen Inhalte zu unterscheiden. Diese fremden Inhalte stammen weder vom IDS, noch hat das IDS die Möglichkeit, den Inhalt von Seiten Dritter zu beeinflussen.

Die Inhalte fremder Seiten, auf welche das IDS mittels Links hinweist, spiegeln nicht die Meinung des IDS wider, sondern dienen lediglich der Information und der Darstellung von Zusammenhängen.

Diese Feststellungen gelten für alle innerhalb des eigenen Internetangebotes gesetzten Links und Verweise sowie für Fremdeinträge in vom IDS eingerichteten Internetangeboten. Für illegale, fehlerhafte oder unvollständige Inhalte und insbesondere für Schäden, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde.

Das IDS ist nicht verpflichtet, in periodischen Abständen den Inhalt von Angeboten Dritter auf deren Rechtswidrigkeit oder Strafbarkeit zu überprüfen. Sobald das IDS von dem rechtswidrigen Inhalt der Webseiten Dritter in Kenntnis gesetzt wird, wird die entsprechende Verknüpfung von den Webseiten des IDS entfernt.
