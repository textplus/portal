---
title: Architektur

menu:
  main:
    weight: 50
    parent: ueber-uns
type: non-scrolling-toc


---

# Die Text+ Architektur

An der verteilten Infrastruktur von Text+ sind über 30 Institutionen beteiligt. Das führt zwangsläufig zu Komplexitäten, wenn es darum geht, den Aufbau des Verbunds darzustellen.

Bei der bildlichen Darstellung der Architektur handelt sich um eine Sicht auf Text+, die wesentliche technische Komponenten entlang ihrer Funktionen zeigt. Die Architektur besteht aus drei Ebenen – User, Frontend, Backend – und wird ausgehend vom User bzw. einem neuen Datenbestand/neuen Datenzentrum von oben nach unten gelesen.

{{< standout theme="white">}}
  {{< rawhtml src="gfx/Architektur.html" />}}
{{< /standout >}}

Im Zentrum (rot) steht das Text+ Portal, also die Website des Verbunds. Darunter liegen zentrale Angebote von Text+ wie das [Consulting](/daten-dienste/consulting), die [Suche nach Daten und Diensten](https://registry.text-plus.org/) sowie die Möglichkeit, Forschungsdaten an das Konsortium zu übergeben ([Datenaufnahme](/daten-dienste/depositing)). Viele weitere Dienste und Themen sind jedoch für das Funktionieren von Text+ von zentraler Bedeutung.

Ein besonderes Feature dieser Darstellung besteht darin, dass die einzelnen Komponenten verlinkt sind. Man findet also bspw. von der Bibliographie den direkten Weg zur entsprechenden Seite im Portal. Es handelt sich insofern nicht nur um eine Abbildung von Funktionen, sondern die Abbildung ist selbst funktional.

### Changelog

* Version 0.9: Februar 2024. Diskussion im Rahmen des Text+ Frühjahrstreffens an der Deutschen Nationalbibliothek in Frankurt/Main.
* Version 1.0: April 2024. [Erste veröffentlichte Version der Architektur auf dem Portal](https://viewer.diagrams.net/?tags=%7B%7D&highlight=0000ff&edit=_blank&layers=1&nav=1&page-id=NsHAukxiDZblU5d1yVMV&title=Architektur#Uhttps%3A%2F%2Fdrive.google.com%2Fuc%3Fid%3D1jTllBtIdIUAwldby4T03rcAPlbtevufb%26export%3Ddownload). 


