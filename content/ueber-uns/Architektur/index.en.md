---
title: Architecture

menu:
  main:
    weight: 50
    parent: ueber-uns


---

# About the Text+ Architecture

Over 30 institutions are involved in the distributed infrastructure of Text+. This inevitably leads to complexities when it comes to visualising the structure of the network.

The visual representation of the architecture is a view of Text+ that shows the key technical components along their functions. The architecture consists of three levels - user, frontend, backend - and is read from top to bottom starting from the user or a new dataset/new data centre.

{{< standout theme="white">}}
  {{< rawhtml src="gfx/Architektur.html" />}}
{{< /standout >}}

In the centre (red) is the Text+ portal, i.e. the consortium's website. Below it are central Text+ services such as [consulting](/en/daten-dienste/consulting), the [search for data and services](https://registry.text-plus.org/?lang=en) and the option of [submitting research data](/daten-dienste/depositing) to the consortium. However, many other services and topics are of central importance for the functioning of Text+.

A special feature of this presentation is that the individual components are linked. For example, you can find the direct path from the bibliography to the corresponding page in the portal. In this respect, it is not just a mapping of functions, but the mapping itself is functional.

## Changelog
* Version 0.9: February 2024. Discussion during the Text+ Spring Meeting at the German National Library in Frankfurt/Main.
* Version 1.0: April 2024. [First published version of the architecture on the portal](https://viewer.diagrams.net/?tags=%7B%7D&highlight=0000ff&edit=_blank&layers=1&nav=1&page-id=NsHAukxiDZblU5d1yVMV&title=Architektur#Uhttps%3A%2F%2Fdrive.google.com%2Fuc%3Fid%3D1jTllBtIdIUAwldby4T03rcAPlbtevufb%26export%3Ddownload).
