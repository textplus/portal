---
title: Applicant and Participating Institutions

aliases:
- /about-us/applicant-institutions
- /about-us/participating-institutions
- /en/vernetzung/antragstellende-beteiligte-institutionen/

menu:
  main:
    weight: 30
    parent: ueber-uns 
---

## Applicant Institutions

{{<lead-text>}}
The applicant and participating institutions of Text+ represent the interest groups that provide and utilize research data for the humanities. All Text+ institutions contribute significant resources, thus demonstrating their strong support for Text+.
{{</lead-text>}}

{{< image img=2021-02-08-Logos-Antragstellende-Institutionen-2048x819.png />}}

**Applicant Institution**

[Leibniz-Institut für Deutsche Sprache, Mannheim](https://www1.ids-mannheim.de/)

**Co-applicant Institutions**

[Berlin-Brandenburg Academy of Sciences and Humanities](https://www.bbaw.de/en/)

[German National Library](https://www.dnb.de/EN/Home/home_node.html)

[Lower Saxony State and University Library Göttingen](https://www.sub.uni-goettingen.de/en/news/)

[North Rhine-Westphalian Academy of Sciences and Arts](https://www.awk.nrw.de/)


## Participating Institutions

{{<lead-text>}}
The following institutions actively participate in Text+ by contributing their specialized expertise to shape the initiative and by providing and utilizing research data. The specific contributions of all institutions are outlined in Letters of Commitment.
{{</lead-text>}}

<!-- TODO: search for better logos, maybe reorder -->
{{<institution-list>}}
{{<institution url="https://www.awhamburg.de/en/academy/portrait.html" img="logo-awhh.png">}}Academy of Sciences in Hamburg{{</institution>}}
{{<institution url="https://www.adwmainz.de/en/home.html" img="AWLM_Logo_Schrift.svg">}}Academy of Sciences and Literature, Mainz{{</institution>}}  
{{<institution url="https://adw-goe.de/en/home/" img="nadwg-logo-mitschrift.svg">}}Academy of Sciences in Göttingen{{</institution>}}
{{<institution url="https://uni-freiburg.de/" img="ufr-logo-white.svg">}}Albert Ludwig University Freiburg{{</institution>}}
{{<institution url="https://badw.de/" img="badw.png">}}Bavarian Academy of Sciences{{</institution>}}
{{<institution url="https://www.dla-marbach.de/en/" img="Deutsches_Literaturarchiv_Marbach_Logo.svg">}}German Literature Archive Marbach{{</institution>}}
{{<institution url="https://uni-tuebingen.de/en/" img="Logo_Universitaet_Tuebingen.svg">}}Eberhard Karls University Tübingen{{</institution>}}
{{<institution url="https://gwdg.de/en/" img="gwdg_logo.svg">}}Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen{{</institution>}}
{{<institution url="https://www.hadw-bw.de/" img="Logo_Heidelberg.svg">}}Heidelberg Academy of Sciences{{</institution>}}
{{<institution url="https://www.hab.de/en/" img="hab-logo.svg">}}Herzog August Bibliothek Wolfenbüttel{{</institution>}}
{{<institution url="https://h-da.de/en/" img="hda.svg">}}Darmstadt University{{</institution>}}
{{<institution url="https://www.fz-juelich.de/en" img="logo_juelich.svg">}}Jülich Supercomputing Centre (JSC){{</institution>}}
{{<institution url="https://www.uni-wuerzburg.de/en/" img="uni-wuerzburg-logo.svg">}}Julius Maximilian University Würzburg{{</institution>}}
{{<institution url="https://www.klassik-stiftung.de/en/" img="klassik-stiftung-weimar-logo.svg">}}Klassik Stiftung Weimar{{</institution>}}
{{<institution url="https://www.leopoldina.org/en/leopoldina-home/" img="leopoldina-logo.png">}}Leopoldina, National Academy of Sciences{{</institution>}}
{{<institution url="https://www.lmu.de/en/" img="Logo_LMU.svg">}}Ludwig Maximilian University Munich{{</institution>}}
{{<institution url="https://www.maxweberstiftung.de/en/" img="MWS-Logo.svg">}}Max Weber Foundation, Bonn{{</institution>}}
{{<institution url="https://www.uni-bamberg.de/en/" img="Otto-Friedrich-Universität_Bamberg_logo.svg">}}Otto Friedrich University Bamberg{{</institution>}}
{{<institution url="https://www.saw-leipzig.de/en" img="saw_logo.png">}}Saxon Academy of Sciences, Leipzig{{</institution>}}
{{<institution url="https://www.steinheim-institut.org" img="steinheim-logo_Webseite.png">}}Salomon Ludwig Steinheim Institute, Essen{{</institution>}}
{{<institution url="https://www.tu-darmstadt.de/index.en.jsp" img="tu_logo_web.svg">}}Technical University Darmstadt{{</institution>}}
{{<institution url="https://tu-dresden.de/zih?set_language=en" img="TU_dresden.svg">}}Technical University Dresden, Center for Information Services and High-Performance Computing (ZIH){{</institution>}}
{{<institution url="https://www.uni-saarland.de/en/home.html" img="logo_uni_saarland.svg">}}University of Saarland{{</institution>}}
{{<institution url="https://www.uni-due.de/en/index.php" img="UDE-logo-claim.svg">}}University of Duisburg-Essen{{</institution>}}
{{<institution url="https://www.uni-hamburg.de/en.html" img="uhh-logo.svg">}}University of Hamburg{{</institution>}}
{{<institution url="https://www.uni-paderborn.de/en/" img="uni-paderborn-logo.png">}}University of Paderborn{{</institution>}}
{{<institution url="https://www.uni-trier.de/en/" img="Logo_Universitaet_Trier.svg">}}University of Trier{{</institution>}}
{{<institution url="https://portal.uni-koeln.de/en/uoc-home" img="uni-koeln-logo.svg">}}University of Cologne{{</institution>}}
{{<institution url="https://www.ulb.tu-darmstadt.de/die_bibliothek/index.en.jsp" img="ulb_logo_schriftzug_tuerkis_rgb.jpg">}}University and State Library Darmstadt{{</institution>}}
{{<institution url="https://www.hs-wismar.de/en/" img="HS-Wismar_Logo.jpg">}}Wismar University{{</institution>}}
{{<institution url="https://www.uni-wuppertal.de/en/" img="BUW_Uni_Logo.png">}}Bergische Universität Wuppertal{{</institution>}}
{{<institution url="https://www.uni-marburg.de/en" img="Uni_Marburg_Logo.svg">}}Philipps-Universität Marburg{{</institution>}}
{{</institution-list>}}
