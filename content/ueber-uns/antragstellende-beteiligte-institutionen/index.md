---
title: Antragstellende und beteiligte Institutionen

aliases:
- /ueber-uns/antragstellende-institutionen
- /ueber-uns/beteiligte-institutionen
- /vernetzung/antragstellende-beteiligte-institutionen/

menu:
  main:
    weight: 30
    parent: ueber-uns
---

## Antragstellende Institutionen

{{<lead-text>}}
Die antragstellenden und beteiligten Institutionen von Text+ repräsentieren die Interessengruppen, die Forschungsdaten für die Geisteswissenschaften bereitstellen und nutzen. Alle Text+-Institutionen stellen signifikante Eigenmittel zur Verfügung und dokumentieren damit ihre starke Unterstützung für Text+.
{{</lead-text>}}

{{< image img=2021-02-08-Logos-Antragstellende-Institutionen-2048x819.png />}}

**Antragstellende Einrichtung**

[Leibniz-Institut für Deutsche Sprache, Mannheim](https://www1.ids-mannheim.de/)

**Mitantragstellende Einrichtungen**

[Berlin-Brandenburgische Akademie der Wissenschaften](https://www.bbaw.de/)

[Deutsche Nationalbibliothek](https://www.dnb.de/)

[Niedersächsische Staats- und Universitätsbibliothek Göttingen](https://www.sub.uni-goettingen.de/sub-aktuell/)

[Nordrhein-Westfälische Akademie der Wissenschaften und der Künste](https://www.awk.nrw.de/)


## Beteiligte Institutionen

{{<lead-text>}}
Die folgenden Institutionen beteiligen sich aktiv an Text+, indem sie ihre fachspezifische Expertise in die Gestaltung der Initiative einbringen sowie Forschungsdaten bereitstellen und nutzen. Ihren konkreten Beitrag haben alle Institutionen in Letters of Commitment spezifiziert.
{{</lead-text>}}

<!-- TODO: search for better logos, maybe reorder -->
{{<institution-list>}}
{{<institution url="https://www.awhamburg.de/" img="logo-awhh.png">}}Akademie der Wissenschaften in Hamburg{{</institution>}}
{{<institution url="https://www.adwmainz.de/startseite.html" img="AWLM_Logo_Schrift.svg">}}Akademie der Wissenschaften und der Literatur | Mainz{{</institution>}}  
{{<institution url="https://adw-goe.de/" img="nadwg-logo-mitschrift.svg">}}Akademie der Wissenschaften zu Göttingen{{</institution>}}
{{<institution url="https://uni-freiburg.de/" img="ufr-logo-white.svg">}}Albert-Ludwigs-Universität Freiburg{{</institution>}}
{{<institution url="https://badw.de/" img="badw.png">}}Bayerische Akademie der Wissenschaften{{</institution>}}
{{<institution url="https://www.dla-marbach.de/" img="Deutsches_Literaturarchiv_Marbach_Logo.svg">}}Deutsches Literaturarchiv Marbach{{</institution>}}
{{<institution url="https://uni-tuebingen.de/" img="Logo_Universitaet_Tuebingen.svg">}}Eberhard Karls Universität Tübingen{{</institution>}}
{{<institution url="https://www.gwdg.de/" img="gwdg_logo.svg">}}Gesellschaft für wissenschaftliche Datenverarbeitung mbH Göttingen{{</institution>}}
{{<institution url="https://www.hadw-bw.de/" img="Logo_Heidelberg.svg">}}Heidelberger Akademie der Wissenschaften{{</institution>}}
{{<institution url="https://www.hab.de/" img="hab-logo.svg">}}Herzog August Bibliothek Wolfenbüttel{{</institution>}}
{{<institution url="https://h-da.de/" img="hda.svg">}}Hochschule Darmstadt{{</institution>}}
{{<institution url="https://www.fz-juelich.de" img="logo_juelich.svg">}}Jülich Supercomputing Centre (JSC){{</institution>}}
{{<institution url="https://www.uni-wuerzburg.de/" img="uni-wuerzburg-logo.svg">}}Julius-Maximilians-Universität Würzburg{{</institution>}}
{{<institution url="https://www.klassik-stiftung.de/" img="klassik-stiftung-weimar-logo.svg">}}Klassik Stiftung Weimar{{</institution>}}
{{<institution url="https://www.leopoldina.org/" img="leopoldina-logo.png">}}Leopoldina, Nationale Akademie der Wissenschaften{{</institution>}}
{{<institution url="https://www.lmu.de/" img="Logo_LMU.svg">}}Ludwig-Maximilians-Universität München{{</institution>}}
{{<institution url="https://www.maxweberstiftung.de" img="MWS-Logo.svg">}}Max Weber Stiftung, Bonn{{</institution>}}
{{<institution url="https://www.uni-bamberg.de/" img="Otto-Friedrich-Universität_Bamberg_logo.svg">}}Otto-Friedrich-Universität Bamberg{{</institution>}}
{{<institution url="https://www.saw-leipzig.de/" img="saw_logo.png">}}Sächsische Akademie der Wissenschaften, Leipzig{{</institution>}}
{{<institution url="https://www.steinheim-institut.org" img="steinheim-logo_Webseite.png">}}Salomon Ludwig Steinheim-Institut, Essen{{</institution>}}
{{<institution url="https://www.tu-darmstadt.de/" img="tu_logo_web.svg">}}Technische Universität Darmstadt{{</institution>}}
{{<institution url="https://tu-dresden.de/zih" img="TU_dresden.svg">}}Technische Universität Dresden, Zentrum für Informationsdienste und Hochleistungsrechnen (ZIH){{</institution>}}
{{<institution url="https://www.uni-saarland.de/start.html" img="logo_uni_saarland.svg">}}Universität des Saarlandes{{</institution>}}
{{<institution url="https://www.uni-due.de/" img="UDE-logo-claim.svg">}}Universität Duisburg-Essen{{</institution>}}
{{<institution url="https://www.uni-hamburg.de/" img="uhh-logo.svg">}}Universität Hamburg{{</institution>}}
{{<institution url="https://www.uni-paderborn.de/" img="uni-paderborn-logo.png">}}Universität Paderborn{{</institution>}}
{{<institution url="https://www.uni-trier.de/" img="Logo_Universitaet_Trier.svg">}}Universität Trier{{</institution>}}
{{<institution url="https://www.uni-koeln.de/" img="uni-koeln-logo.svg">}}Universität zu Köln{{</institution>}}
{{<institution url="https://www.ulb.tu-darmstadt.de/die_bibliothek/index.de.jsp" img="ulb_logo_schriftzug_tuerkis_rgb.jpg">}}Universitäts- und Landesbibliothek Darmstadt{{</institution>}}
{{<institution url="https://www.hs-wismar.de/" img="HS-Wismar_Logo.jpg">}}Hochschule Wismar{{</institution>}}
{{<institution url="https://www.uni-wuppertal.de/de/" img="BUW_Uni_Logo.png">}}Bergische Universität Wuppertal{{</institution>}}
{{<institution url="https://www.uni-marburg.de/de" img="Uni_Marburg_Logo.svg">}}Philipps-Universität Marburg{{</institution>}}
{{</institution-list>}}
