---
title: Ludwig-Maximilians-Universität München (LMU)
short_name: LMU
text_plus_domains:
- Collections 

text_plus_clusters:
  Collections: 
  - Gegenwartssprache

external_url: "https://www.lmu.de/de/index.html"

type: competence-center
---

# Ludwig-Maximilians-Universität München (LMU)
    
**Text+ Zentrum:** [Bayerisches Archiv für Sprachsignale](https://www.bas.uni-muenchen.de/Bas/BasInfoStandardsTemplatesdeu.html)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Die LMU gehört zu den forschungsstärksten Universitäten Europas. Mit ihrem ausdifferenzierten Fächerspektrum verfügt sie über ein herausragendes Potenzial für zukunftsweisende Forschung.
Das Bayerische Archiv für Sprachsignale (BAS) des Fachbereichs Phonetik an der LMU sammelt, standardisiert, pflegt und distribuiert digitale Sprachressourcen mit dem Schwerpunkt gesprochenes Deutsch. Dies können technisch motivierte, aber auch wissenschaftliche Audio- und Annotationsdaten sein, die in Form von Sprachdatenbanken für die Nachnutzung zur Verfügung gestellt werden.

Die vom BAS für Text+ bereitgestellten Ressourcen umfassen ein Repository für Sprachdatenbanken mit mehr als 40 Sammlungen von Sprachdaten in mehreren Sprachen (Deutsch, Englisch, Japanisch, Italienisch usw.), eine Reihe von webbasierten Diensten zur Sprachverarbeitung und verschiedene eigenständige Tools zur Datensammlung und -analyse.

Für Forschende sowie Projekte, die ihre Daten langfristig speichern möchten bietet das BAS Unterstützung bei der Datenaufbereitung und dem Datenimport an. Diese Unterstützung schließt eine Beratung zur Erhebung, Verarbeitung und Verwaltung der Daten bereits während eines Forschungsvorhabens ein. Zudem bietet das Archiv Online- und Vor-Ort-Workshops zu den verfügbaren sprachtechnologischen Webdiensten und Tools an.
    
## Highlights bereitgestellter Daten und Dienste
    
* [WebMAUS](https://clarin.phonetik.uni-muenchen.de/BASWebServices/interface/WebMAUSGeneral) erstellt eine phonetische Segmentierung und Beschriftung für mehrere Sprachen auf der Grundlage des Sprachsignals und eines phonologischen Transkripts
* [TextAlign](https://clarin.phonetik.uni-muenchen.de/BASWebServices/interface/TextAlign): Zuordnung einer orthografischen Textzeichenfolge zum entsprechenden phonologischen Transkript
* [SpeechRecorder](https://www.bas.uni-muenchen.de/Bas/software/speechrecorder/): plattformunabhängige Audioaufnahmesoftware, die auf die Anforderungen von Sprachaufnahmen zugeschnitten ist
* [EMU Speech Database Management System](https://ips-lmu.github.io/EMU.html): Sammlung von Software-Tools für die Erstellung, Bearbeitung und Analyse von Sprachdatenbanken
* [BAS Corpora](https://www.bas.uni-muenchen.de/forschung/Bas/BasKorporadeu.html): große Sammlung gesprochensprachlicher Aufnahmen und Transkriptionen

## Entgegennahme Daten Dritter 
Das BAS Repository bietet einzelnen Forscherinnen und Forschern sowie Projekten die Möglichkeit, Forschungsdaten dauerhaft (mindestens 10 Jahre) zu speichern. Bedingung ist, dass die notwendigen Einverständniserklärungen vorliegen, dass die Medien- und Annotationsdaten in den unterstützten Formaten bereitstehen, und dass sie durch Metadaten im CMDI-Format beschrieben sind.

Die bevorzugten Daten sind phonetische Tools und Services, Sprachstatistiken, Aussprache-Lexika sowie deutsche Sprachaufnahmen und Transkriptionen und multimodale Daten in den vom BAS unterstützten Formaten. Anfragen an das BAS bitte über den Text+ Helpdesk oder direkt per Mail an bas@phonetik.uni-muenchen.de

## Kontakt 
    
**Ansprechperson für Text+:** bas@bas.uni-muenchen.de