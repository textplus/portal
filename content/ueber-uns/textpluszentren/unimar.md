---
title: Philipps-Universität Marburg, Forschungszentrum Deutscher Sprachatlas (DSA)
short_name: DSA
text_plus_domains:
- Lexikalische Ressourcen 

text_plus_clusters:
  Lexikalische Ressourcen: 
  - Deutsche Wörterbücher im europäischen Kontext 

external_url: "www.dsa.info"

type: competence-center
---

# Philipps-Universität Marburg, Forschungszentrum Deutscher Sprachatlas (DSA)
    
**Text+ Zentrum:** [Forschungszentrum Deutscher Sprachatlas](https://www.uni-marburg.de/de/fb09/dsa/einrichtung)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Das Forschungszentrum Deutscher Sprachatlas an der Philipps-Universität Marburg ist eines der führenden Zentren für die Dokumentation und Erforschung der regionalen Vielfalt im deutschsprachigen Raum. Es archiviert langfristig schriftliche, tonmediale und visuelle Daten mit regionalen Bezügen wie Dialekte, Akzente und Alltagssprache. Eine zeitliche oder sprachgeschichtliche Eingrenzung der zu archivierenden Daten besteht nicht. 

Zu den zentralen Beständen gehören neben aktuellen Sprachdaten die wichtigsten historischen Dokumentationen zu den Dialekten und regionalen Sprachformen des Deutschen, darunter Sprachkarten, Tonaufnahmen und Fragebögen aus Sprachatlanten, Wörterbüchern und individuellen Spracherhebungen. Eine Spezialisierung besteht in der nachhaltigen Archivierung und Bereitstellung von Daten abgeschlossener Projekte. 

Als Datenzentrum bringt der Deutsche Sprachatlas seine Expertise im Bereich lexikalischer Ressourcen in die Text+-Infrastruktur ein. Neben der Bereitstellung linguistischer Daten unterstützt das Zentrum durch Beratung, regelmäßige Fortbildungen und die Entwicklung innovativer Ansätze zur Sprachdatennutzung und zur Analyse von Sprachressourcen.

Der Deutsche Sprachatlas übernimmt nach Absprache Daten (Bild, Text, Ton) zur Sprachgeschichte, Dialektologie und Regionalsprachenforschung des Deutschen. Die Archivierung von Daten anderer Sprachen kann ebenfalls nach Absprache erfolgen.

## Highlights bereitgestellter Daten und Dienste
    
* [Erhebungsbögen des Deutschen Wortatlas](https://hdl.handle.net/20.500.14450/429): Über 52.000 ausgefüllte Fragebögen zur Dialektlexik vom Beginn des 20. Jahrhunderts
* [Lego-Korpus](https://hdl.handle.net/20.500.14450/286): Korpus informeller deutscher Gespräche während einer Bauanleitungsaufgabe (Audiodateien und Transkripte)
* [Althochdeutsche und altniederdeutsche werden-Periphrasen](https://hdl.handle.net/20.500.14450/434): Datensatz aller im Referenzkorpus Altdeutsch (ReA) erzielten Treffer für eine infinite Verbform in Kombination mit dem Hilfsverb *werden*
* [Phonotactics of German Dialects (PhonD2)](https://www.regionalsprache.de/phonD2/): Dokumentation der phonotaktischen Strukturen in den Dialekten des Deutschen auf einer mikrotypologischen Ebene für die Analyse der Segmentkombination
* [Sprachatlas der Rheinprovinz](https://hdl.handle.net/20.500.14450/295): 25 Karten aus Georg Wenker‘s Sprachatlas des rheinischen Dialektgebiets
    
## Entgegennahme Daten Dritter 
Innerhalb von Text+ übernimmt der Deutsche Sprachatlas in Marburg insbesondere (aber nicht ausschließlich) Daten, die zu seinen angestammten Daten und zu seinem Arbeitsschwerpunkt in der Dialektologie und Regionalsprachenforschung des Deutschen und angrenzender Sprachen passen sowie weitere historische Datensätze mit regionalem Bezug.

## Kontakt 
    
**Ansprechperson für Text+:** [Robert Engsterhold](mailto:engsterhold@uni-marburg.de)