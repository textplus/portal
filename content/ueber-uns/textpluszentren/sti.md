---
title: Salomon Ludwig Steinheim-Institut (STI)
short_name: STI
text_plus_domains:
- Editionen 

text_plus_clusters:
  Editionen: 
  - Antike und mittelalterliche Texte 
  - Frühe moderne, moderne und zeitgenössische Texte 

external_url: "http://www.steinheim-institut.de"

type: competence-center
---

# Salomon Ludwig Steinheim-Institut (STI)
    
**Text+ Zentrum:** [Steinheim-Institut](http://www.steinheim-institut.de)

**Zentrumstyp:** Kompetenzzentrum

Das Steinheim-Institut ist seit Jahren auf dem Feld der Digitalen Geisteswissenschaften aktiv: Digitale Editionen hebräischer Inschriften und Memoria, Dokumentation jüdischer Friedhöfe, digitale Sammlungen und Bibliografien, georeferenzierte Online-Datenbanken zur deutsch-jüdischen Geschichte, Linked-Data-Anwendungen,  Mitarbeit in Infrastrukturprojekten wie Text+, DARIAH und TextGrid.

Das Steinheim-Institut ist in der Task-Area Editionen an dem Verbund Text+ beteiligt. Zu den Schwerpunkten gehört u.a. der Bereich „Linked Open Data“: Anwendung von Normdaten (GND) und Vernetzung digitaler Editionen.
    
## Highlights bereitgestellter Daten und Dienste
    
* Das Institut hat ca. 250 digitale Editionen zu jüdischen Friedhöfen mit ca. 50.000 einzelnen Grabinschriften (hebräisch / deutsch / jiddisch) veröffentlicht. Die Daten sind in einem Standard-Format (TEI/XML) und unter Creative Commons Lizenz zugänglich.
* Der STI Linked Data Service ist eine Komponente der Linked Data Strategie des Steinheim-Instituts. Die digitalen Veröffentlichungen des Instituts werden durch diesen Service untereinander und mit weiteren relevanten Quellen und Diensten im Web semantisch vernetzt.
    
## Entgegennahme Daten Dritter 
Innerhalb von Text+ übernimmt das Steinheim-Institut Daten zur Dokumentation jüdischer Friedhöfe, insbesondere Grabsteininschriften und Fotodokumentationen.

## Kontakt 
    
**Ansprechperson für Text+:** [Harald Lordick](mailto:lordick@steinheim-institut.org)
