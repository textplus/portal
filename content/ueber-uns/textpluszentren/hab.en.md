---
title: Herzog August Bibliothek Wolfenbüttel (HAB)
short_name: HAB
text_plus_domains:
- Editions 

text_plus_clusters:
  Editionen: 
  - Ancient and Medieval Texts
  - Early Modern, Modern, and Contemporary Texts 

external_url: "https://www.hab.de/en/"

type: competence-center
---

# Herzog August Bibliothek Wolfenbüttel (HAB)

**Text+ center:** [HAB Repositorium (Herzog August Bibliothek Wolfenbüttel)](https://repo.hab.de/home)

**Type of center:** competence center

The HAB is a non-university study and research centre for the European cultural history of the Middle Ages and the early modern period. It is a centre for manuscript cataloguing funded by the German Research Foundation (DFG) and part of the project for a retrospective German National Library (AG Sammlung Deutscher Drucke). In addition, the library participates in the Marbach Weimar Wolfenbüttel research network. With the Wolfenbüttel Digital Library, it provides a specially developed editorial infrastructure that is used for numerous projects. 

As a competence centre in the Text+ data domain of editions, the HAB is involved in the two clusters ‘Ancient and Medieval Texts’ and ‘Early Modern, Modern, and Contemporary Texts’. This entails the provision of (meta-)data and tools, collaboration on standardisation processes, but also on a comprehensive and coordinated range of advisory services to which the HAB contributes its expertise. The HAB is also playing a leading role in the development of a curated directory of editions (so-called registry), which is intended to offer users of editions as well as editors structured access to the large number of available resources. 

In addition to tasks within the editions data domain, the HAB is also committed to various cross-domain working groups. For example, it co-leads the Registry and FID Koop working groups and is a member of the Text+ Blog editorial team.

## Highlights of provided data and services

* [Annotated digital edition of the travel and collection descriptions of Philipp Hainhofer (1578-1647)](https://hainhofer.hab.de/)
* [Lessing digital.](https://diglib.hab.de/?link=180): An exemplary digital edition of G.E. Lessing's writings based on ‘Ernst und Falk’
* [Digital edition and annotation of the diaries of Prince Christian II of Anhalt-Bernburg (1599-1656)](http://www.tagebuch-christian-ii-anhalt.de/)
* Coordination project for the further development of [Optical Character Recognition OCR-D methods](https://diglib.hab.de/?link=068)
* [Digital Humanities in Discuss Data](https://discuss-data.net/): Development of a community space


## Contact

**Contact for Text+:** [Johannes Mangei](mailto:mangei@hab.de)
