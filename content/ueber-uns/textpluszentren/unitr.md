---
title: Universität Trier (UniTR)
short_name: UniTR
text_plus_domains:
- Lexikalische Ressourcen 
- Collections 

text_plus_clusters:
  Lexikalische Ressourcen: 
  - Deutsche Wörterbücher im europäischen Kontext 
  Collections: 
  - Gegenwartssprache 

external_url: "https://www.uni-trier.de/"

type: competence-center
---

# Universität Trier (UniTR)
    
**Text+ Zentrum:** [Kompetenzzentrum - Trier Center for Digital Humanities](https://tcdh.uni-trier.de/de)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Das Trier Center for Digital Humanities (TCDH), eine zentrale wissenschaftliche Einrichtung der Universität Trier, führt Projekte in den Bereichen Digitale Edition, Digitale Lexikographie, Forschungssoftware, Datenmodellierung und digitale Literatur- und Kulturwissenschaften durch. Ein besonderer Schwerpunkt liegt auf der Modellierung, Erschließung und Bereitstellung wichtiger historischer Wörterbücher wie bspw. das deutsche Wörterbuch von Jacob Grimm und Wilhelm Grimm.

Als Kompetenzzentrum stellt das TCDH Text+ seinen Datenbestand über die Plattform des Trierer Wörterbuchnetzes über offene Anwendungsschnittstellen zur Verfügung. Dies umfasst TEI/XML-kodierte Daten zu großen historischen Wörterbüchern sowie zu den regionalsprachlichen Wörterbüchern des Westmitteldeutschen wie Pfälzisch und Elsässisch.

Des Weiteren bringt das TCDH seine Expertise in der Domäne Collections ein und erforscht dort die Arbeit mit abgeleiteten Textformaten. Insbesondere geht es um die Klärung der Frage, wie gut die abgeleiteten Textformate für unterschiedliche Text- und Data-Mining-Aufgaben nutzbar bleiben und inwieweit sie (z.B. durch *Large Language Models*) rekonstruierbar sind.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Erstausgabe und Überarbeitung des Deutschen Wörterbuchs von Jacob Grimm und Wilhelm Grimm](http://dwb.uni-trier.de/de/): das deutschsprachige Wörterbuch mit der längsten Bearbeitungszeit und der bei weitem umfangreichsten Erfassung der deutschen Sprache aus dem Jahre 1838
* [Mittelhochdeutsches Wörterbuch von Matthias Lexer](https://woerterbuchnetz.de/?sigle=Lexer): eines der größten mittelhochdeutschen Wörterbücher, das für eine genaue Lektüre und die Erschließung mittelhochdeutscher Texte und Quellen unentbehrlich ist
* [Pfälzisches Wörterbuch](https://woerterbuchnetz.de/?sigle=PfWB): großlandschaftliches Dialektwörterbuch für das Gebiet der Pfalz und erfasst den Gesamtwortschatz der pfälzischen Basisdialekte sowie örtliche Umgangssprachen, koareale Soziolekte und Namensüberlieferungen
    
## Entgegennahme Daten Dritter 
Innerhalb von Text+ übernimmt die Universität Trier insbesondere Daten, die zu ihren angestammten Daten und zu ihrem Arbeitsschwerpunkt passen, insbesondere Wörterbücher im TEI-Lex0-Format.

## Kontakt 
    
**Ansprechperson für Text+:** [Thomas Burch](mailto:burch@uni-trier.de) und [Christof Schöch](mailto:schoech@uni-trier.de)
