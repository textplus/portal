---
title: Deutsches Literaturarchiv Marbach (DLA)
short_name: DLA
text_plus_domains:
- Collections 

text_plus_clusters:
  Collections: 
  - Unstructured Text 

external_url: "https://www.dla-marbach.de/en/"

type: competence-center
---

# Deutsches Literaturarchiv Marbach (DLA)

**Text+ center:** [Kallías](https://www.dla-marbach.de/en/katalog/)

**Type of center:** competence center

The German Literature Archive (Deutsches Literaturarchiv – DLA) is one of the most significant literary institutions in the world. Its collections bring together and preserve an abundance of valuable sources of literary and intellectual history from 1750 to the present day. It is a non-university research institution with a high level of infrastructure and at the same time it is an Institute of Advanced Study with its own research and fellowship programs. The DLA acquires, catalogues, research, and communicates literature to the public.

In Text+, the DLA is primarily involved in the area of collections of recent literature and their archiving. The data service [DLA Data+](https://www.dla-marbach.de/katalog/dla-dataplus/) offers open access to the data of the DLA Marbach on the basis of research-related questions.

## Highlights of provided data and services

* [Marbach Schiller collection](https://www.dla-marbach.de/archiv/friedrich-schiller/): The collection also includes biographical and historical documents, including Schiller's own notes from his time at Charles School, as well as testimonies from others, family papers or entire estates from relatives and imitations of Schiller autographs (the so-called Gerstenbergk forgeries).
* [Marbach Authors' Libraries](https://www.dla-marbach.de/bibliothek/spezialsammlungen/autorenbibliotheken/): In recent years, central authors' libraries have been catalogued according to current provenance research criteria. The extensive and growing collection of such libraries opens up the possibility of researching poetic and scientific working practices of the 20th and 21st centuries. The book collections of Gottfried Benn, Hans Blumenberg, Paul Celan, Ernst Jünger, Reinhart Koselleck, Siegfried Kracauer, Martin Heidegger, Hermann Hesse and W. G. Sebald are among the outstanding and most frequently used authors' and scholars' libraries at the German Literature Archive Marbach.
* Since its establishment in 1955, the German Literature Archive Marbach (DLA) has been among the most significant collections for German-language exile literature. The aspiration to collect German-language literature which was subject to book burnings and censorship already guided the founders of the DLA and remains a hallmark of the archive’s policies for acquisitions, exhibitions and events to the present day. The holdings of the estates of authors who lived in exile include those of Hannah Arendt, Alfred Döblin, Hilde Domin, Yvan Goll, Mascha Kaléko, Siegfried Kracauer, Else Lasker-Schüler, Karl Löwith, Heinrich Mann, Joseph Roth, Nelly Sachs, Carl and Thea Sternheim, Kurt Tucholsky, Kurt Wolff, Karl Wolfskehl, Carl Zuckmayer and Stefan Zweig. Along with numerous other holdings, they form the Helen and Kurt Wolff Archive.


## Contact

**Contact for Text+:** [Karin Schmidgall](mailto:karin.schmidgall@dla-marbach.de)