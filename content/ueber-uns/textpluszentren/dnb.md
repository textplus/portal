---
title: Deutsche Nationalbibliothek (DNB)
short_name: DNB
text_plus_domains:
- Collections 
- Infrastruktur/Betrieb

text_plus_clusters:
  Collections: 
  - Unstrukturierte Texte 

external_url: "https://www.dnb.de/DE/Home/home_node.html"

type: competence-center
---

# Deutsche Nationalbibliothek (DNB)
    
**Text+ Zentrum:** [Deutsche Nationalbibliothek](https://www.dnb.de/DE/Professionell/Services/WissenschaftundForschung/wissenschaftundforschung_node.html#doc578708bodyText3/pp/p)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Die DNB ist Deutschlands zentrale Archivbibliothek. Sie sammelt, dokumentiert und archiviert alle seit 1913 in Deutschland erschienenen Publikationen und Tonträger sowie Werke, die in deutscher Sprache erstellt wurden oder einen Bezug zu Deutschland haben. Diese Sammlung reicht von zeitgenössischer deutschsprachiger Literatur über alle Tageszeitungen, wissenschaftlichen Artikel aus deutschen Verlagen bis hin zu Kioskliteratur. 

Der Zugang zu den meisten Objekten in den Beständen der DNB ist aus urheberrechtlichen Gründen beschränkt. Indem die DNB die digitale Sammlung von Texten des 21. Jahrhunderts so flexibel wie möglich bereitstellt, erleichtert sie die Umsetzung von Forschungsprojekten verschiedenster Disziplinen und unterstützt Projekte bei der Korpusbildung. Auf das Repositorium der Deutschen Nationalbibliothek kann nur indirekt über den Katalog der DNB bzw. durch individuelle Bereitstellung zugegriffen werden.

Die DNB spielt eine aktive Rolle bei der Weiterentwicklung von Techniken zur Verknüpfung von Sammlungen mit anderen lokal und thematisch getrennten Datensätzen aus Text+ über Linked Open Data (LOD) und insbesondere über Normdateien wie die Gemeinsame Normdatei (GND) oder über lexikalische Ressourcen. Sie entwickelt außerdem die GND auch im Hinblick auf die Bedürfnisse der wissenschaftlichen Gemeinschaften weiter. Zusammen mit dem [Leibniz-Institut für Deutsche Sprache (IDS)](https://www.ids-mannheim.de/) stellt die DNB ein zentraler Anlaufpunkt für die Vielzahl rechtlicher Themen dar, die sich aus der Nutzung und Veröffentlichung textbasierter Daten ergeben.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Freie Online-Hochschulschriften](https://portal.dnb.de/opac.htm?query=catalog%3Ddnb.hss+location%3Donlinefree+&method=simpleSearch&cqlMode=true): Mehr als 325.000 Online-Hochschulschriften ohne Zugriffsbeschränkung aus Deutschland 
* [Sammlung Erster Weltkrieg](https://portal.dnb.de/opac/simpleSearch?query=cod%3D2d009&cqlMode=true): Mehr als 3.000 Bücher, Broschüren, Plakate und andere Einblattdrucke zum Weltkrieg 1914-1918 (fortlaufende Ergänzung)
* [Kunst, Architektur, Bauhaus 1918-1933](https://portal.dnb.de/opac.htm?query=cod%3Dd028+location%3Donlinefree+&method=simpleSearch&cqlMode=true): Digitalisierte Werke aus Architektur, Design, Malerei und Druckkunst von 1918 bis 1933
* [Exilpresse digital – Exilzeitschriften 1933 – 1945](https://portal.dnb.de/opac.htm?method=showPreviousResultSite&currentResultId=%22exilpresse%22+and+%22digital%22%26any%26dnb.dea.exp%26leipzig&currentPosition=10): 100.000 Seiten aus 30 Titeln, die ein breites Spektrum der Exilpresse 1933-1945 repräsentieren
    
## Entgegennahme Daten Dritter 
Im Rahmen ihres gesetzlichen Sammelauftrages (siehe [Gesetz über die Deutsche Nationalbibliothek vom 22. Juni 2006](https://www.gesetze-im-internet.de/dnbg/BJNR133800006.html)) übernimmt die DNB alle entsprechenden Publikationen.

## Kontakt 
    
**Ansprechperson für Text+:** nfdi@dnb.de
