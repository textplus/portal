---
title: Universität Hamburg (UniHH)
short_name: UniHH
text_plus_domains:
- Collections 

text_plus_clusters:
  Collections: 
  - Gegenwartssprache 

external_url: "https://www.uni-hamburg.de/"

type: competence-center
---

# Universität Hamburg (UniHH)
    
**Text+ Zentrum:** [Universität Hamburg: Hamburger Zentrum für Sprachkorpora](https://www.slm.uni-hamburg.de/hzsk/)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Die Universität Hamburg (UHH) unterstützt basierend auf der Erfahrung des Hamburger Zentrums für Sprachkorpora (HZSK), einem Zusammenschluss von Mitgliedern verschiedener Fakultäten und Institute der UHH, die Konsistenz und Koordination computergestützter empirischer Forschung und Lehre der Sprachwissenschaft sowie der an die UHH angegliederten Nachbardisziplinen. Dabei verfolgt sie zusammen mit der Community HZSK das Ziel, die nachhaltige Nutzbarkeit sprachwissenschaftlicher Primärforschungsdaten über befristete Forschungsprojekte hinaus zu gewährleisten.

Das Repositorium der UHH, welches durch das Zentrum für nachhaltiges Forschungsdatenmanagement (ZFDM) betrieben wird, beherbergt in der HZSK Community mehr als 50 Korpora. Mehrheitlich gehören die Korpora dem thematischen Bereich der mehrsprachigen mündlichen und schriftlichen Kommunikation sowie Daten aus weniger verbreiteten oder gefährdeten Sprachen an. Neben einer Vielzahl von Korpora, die der Dokumentation der kindlichen Sprachaneignung dienen, finden sich Korpora, die eine Bandbreite von Aspekten der Mehrsprachigkeit im Alltag beinhalten, u.a. das Dolmetschen im Krankenhaus (DiK-Korpus), und für eine Nachnutzung zur Verfügung stehen.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Corpus Services](https://www.slm.uni-hamburg.de/hzsk/angebote/corpus-services.html): Die Software bietet verschiedene Funktionen zur Prüfung und Aufbereitung von Sprachkorpora.
* [Kiezdeutschkorpus (KiDKo)](https://www.fdr.uni-hamburg.de/record/8247): Multimodales, digitales Korpus spontansprachlicher Gesprächsdaten aus informellen Peer-Group-Situationen.
* [Dolmetschen im Krankenhaus (DiK)](https://www.fdr.uni-hamburg.de/record/8308): Transkriptionen verschiedener Arten von Arzt-Patienten-Kommunikation in Krankenhäusern (einsprachige Gespräche in Deutsch, Portugiesisch und Türkisch sowie gedolmetschte Gespräche).
* [Referenzkorpus Mittelniederdeutsch/Niederrheinisch (1200–1650) (ReN)](https://www.fdr.uni-hamburg.de/record/9195): Das ReN bietet Handschriften, Drucke sowie Inschriften und ist Teil des „Korpus historischer Texte des Deutschen“.
* [The Hamburg MapTask Corpus (HAMATAC)](https://www.fdr.uni-hamburg.de/record/1481): Ein gesprochensprachliches Korpus, das die Leistung von 24 Deutsch-L2-Lernenden dokumentiert.
* [Phonologie-Erwerb Deutsch-Spanisch als Erste Sprachen (PEDSES)](https://www.fdr.uni-hamburg.de/record/1544): Phonetisch und orthografisch transkribiertes Korpus von deutsch-spanischen simultan bilingualen Kindern (longitudinal: Alter ein bis dreieinhalb/vier Jahre).
    
## Entgegennahme Daten Dritter 
Neue Datensätze (Korpora gesprochener Sprache) können auf Antrag an Prof. Dr. Kristin Bührig in die HZSK-Community innerhalb des FDRs aufgenommen werden. Voraussetzung für die Integration der Daten ins FDR ist ihre bereits vollständige Kuration, für die wir beratend zur Seite stehen.

## Kontakt 
    
**Ansprechperson für Text+:** corpora@uni-hamburg.de
