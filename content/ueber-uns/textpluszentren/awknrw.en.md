---
title: North Rhine-Westphalian Academy of Sciences, Humanities and the Arts (NRW Akademie)
short_name: NRW Akademie
text_plus_domains:
- Editions
text_plus_clusters:
  Editionen: 
  - Ancient and Medieval Texts 
  - Early Modern, Modern, and Contemporary Texts 

external_url: "https://www.awk.nrw/"

type: competence-center
---

# North Rhine-Westphalian Academy of Sciences, Humanities and the Arts (AWK NRW)

**Text+ center:** [Central Coordination Office Digital Humanities of the North Rhine-Westphalian Academy of Sciences, Humanities and the Arts (AWK NRW)](https://cceh.uni-koeln.de/en/academy-nrw/)

**Type of center:** data center/competence center

The Cologne Center for eHumanities ([CCeH](https://cceh.uni-koeln.de/en/)) at the University of Cologne is the Central Coordination Office Digital Humanities of the North Rhine-Westphalian Academy of Sciences, Humanities and the Arts ([AWK NRW](https://www.awk.nrw/)) as co-applicant of Text+. 

In the context of Text+ and NFDI, the CCeH acts as a competence center of the AWK NRW. It leads the [Task Area Editions](/en/ueber-uns/arbeitsbereiche/editionen/), a group of Text+ partners that are developing an infrastructure of services for a community of researchers, teachers and learners in whose field of research and research practice scholarly editions play an essential role. An overview of our services can be found [here](/en/ueber-uns/editionen/).

{{<horizontal-line>}}
## Highlights of provided data and services

* We play a key role in coordinating the services of [Text+ Consulting](/en/daten-dienste/consulting/) and [Text+ Registry](https://registry.text-plus.org/).
* [Egyptian everyday life online](https://www.awk.nrw/aegyptischer-alltag-vernetzt-im-web): Digital edition of the "Kölner Papyri", one of the most important papyrus collections in Germany.
* [Averroes-Edition: A multi-generational project](https://www.awk.nrw/averroes-edition-ein-mehr-generationen-projekt): Critical edition of 18 works by the early pioneer of the European Enlightenment Ibn Rušd alias Averroes.
* [Text database and dictionary of Classic Maya](https://www.mayawoerterbuch.de): Text-technological and epigraphic processing of all Maya hieroglyphic texts for the construction of a comprehensive dictionary of the Classic Maya language.

{{<horizontal-line>}}
## Third-party data reception

In cooperation with the Cologne Data Center for the Humanities ([DCH](https://dch.phil-fak.uni-koeln.de/bestaende/datensicherung)), the DCH fulfils the function of a data centre for the AWK NRW with a focus on an archiving workflow for editions.

For any [questions or consultation](/en/daten-dienste/consulting/), please feel free to contact the [Text+ Helpdesk](/en/helpdesk/) or visit our “[Research Rendezvous](https://events.gwdg.de/category/208/)”. This open consultation hour takes place every fortnight and does not require registration.

{{<horizontal-line>}}
## Contact
**Contact for Text+:** [Kilian Hensen](mailto:Kilian.Hensen@uni-koeln.de)