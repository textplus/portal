---
title: Niedersächsische Staats- und Universitätsbibliothek Göttingen (SUB)
short_name: SUB
text_plus_domains:
- Collections 
- Editionen 
- Infrastruktur/Betrieb

text_plus_clusters:
  Collections: 
  - Unstrukturierte Texte 
  Editionen: 
  - Frühe moderne, moderne und zeitgenössische Texte 
  - Antike und mittelalterliche Texte 

external_url: "https://www.sub.uni-goettingen.de/sub-aktuell/"

type: competence-center
---

# Niedersächsische Staats- und Universitätsbibliothek Göttingen (SUB)
    
**Text+ Zentrum:** 
* [TextGrid Repository](https://textgridrep.org)
* [DARIAH-DE Repository](https://repository.de.dariah.eu/search/)
* [GRO.data](https://data.goettingen-research-online.de/)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Mit ihrem derzeit rund 9 Millionen Medieneinheiten umfassenden Bestand zählt die SUB zu den größten Bibliotheken in Deutschland. Sie stellt umfassende Angebote für die Archivierung und Bereitstellung von geistes- und kulturwissenschaftlichen Forschungsdaten zur Verfügung. Gemeinsam mit der eResearch Alliance, der GWDG, dem Rechenzentrum der Max-Planck-Gesellschaft und der Georg-August-Universität Göttingen, fungiert die SUB Göttingen als Datenzentrum insbesondere für textbasierte Forschungsdaten. Darüber hinaus komplettieren eine umfassende FDM-Beratung sowie ein Data Depositing-Dienst für generische Forschungsdaten das Portfolio.

In Text+ ist die SUB Teil der Datendomänen Editionen und Collections. Zudem ist die SUB der führende Partner in der Domäne Infrastruktur/Betrieb, die sich unter anderem um die Bereitstellung einer FAIR-konformen Plattform für generische Dienste und die Verknüpfung existierender und in Entwicklung befindlicher Daten- und Diensteportfolios der Datendomänen kümmert. 

Zusätzlich bietet die SUB Text+ Zugang zu drei Datenrepositorien: GRO.data, DH-rep und TG-rep. Das Göttingen Research Online Data Repository (GRO.data) ist ein disziplinunabhängiges Datenrepositorium für die Veröffentlichung von Forschungsdaten am Campus Göttingen. Das DARIAH-DE Repository (DH-rep) ist ein digitales Langzeitarchiv für geistes- und kulturwissenschaftliche Forschungsdaten. Das TextGrid Repository (TG-rep) ist ein digitales Archiv zur Erhaltung von Forschungsdaten aus den mit textuellen Forschungsdaten arbeitenden Geisteswissenschaften. Es umfasst die Digitale Bibliothek von TextGrid, die Werke von 700 Autoren der Belletristik (Prosa, Lyrik und Drama) sowie Sachliteratur vom Beginn des Buchdrucks bis zum frühen 20. Jahrhundert in deutscher Sprache oder Übersetzung.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Research Data Management Organizer (RDMO)](https://rdmorganiser.github.io/): Ein Tool, um dynamische Datenmanagementpläne zu erstellen und zu verwalten, mit welchen die systematische Organisation von Forschungsdaten geplant und dokumentiert werden kann
* [European Literacy Text Collection (ELTeC)](https://textgridrep.de/project/TGPR-99d098e9-b60f-98fd-cda3-6448e07e619d): Eine Sammlung von Korpora in über 15 europäischen Sprachen
* [Modes of Narration and Attribution Pipeline (MONAPipe)](https://gitlab.gwdg.de/textplus/collections/mona-pipe): Bereitstellung von Werkzeugen zur Verarbeitung natürlicher Sprache für Deutsch, implementiert in Python/spaCy
    
## Entgegennahme Daten Dritter 
Sofern Forschungsdaten als Self Deposit übernommen werden sollen, können das TG-rep, das DH-rep sowie GRO.data frei genutzt werden. Eine Unterstützung beim Ingest kann im Helpdesk angefragt werden.

## Kontakt 
    
**Ansprechperson für Text+:** [TextGrid Repository](mailto:anfragen@textgrid.de), [DARIAH-DE Repository](mailto:info@de.dariah.eu) und [GRO.data](mailto:dataverse@gwdg.de)
