---
title: Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen (GWDG)
short_name: GWDG
text_plus_domains:
- Collections
- Lexikalische Ressourcen
- Editionen
- Infrastruktur/Betrieb

external_url: "https://gwdg.de/"

type: competence-center
---

# Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen (GWDG)
    
**Text+ Zentrum:** [Langzeitarchiv](https://text-plus-archiv.gwdg.de/)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Als Hochschulrechenzentrum für die [Georg-August-Universität Göttingen](https://www.uni-goettingen.de/) und Rechen- und IT-Kompetenzzentrum für die [Max-Planck-Gesellschaft](https://www.mpg.de/) bietet die Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen (GWDG) ein breites Spektrum an Informations- und Kommunikationsleistungen für die Wissenschaft.

Durch eigene Forschung im Bereich der Praktischen und Angewandten Informatik und die Beteiligung an zahlreichen Forschungsprojekten schafft die GWDG die Basis für innovative und kundennahe IT-Lösungen.

Die GWDG koordiniert gemeinsam mit der SUB die Text+ Task Area Infrastruktur/Betrieb. Sie bringt Kompetenzen im Bereich Authentication Authorization Infrastructure (AAI) und Persistent Identifiers (PID) in Text+ ein und baut diese weiter aus.
    
## Highlights bereitgestellter Daten und Dienste
    
* Die GWDG betreibt einen [Jupyterhub](https://jupyter-cloud.gwdg.de/) und ist maßgeblich beteiligt an der Entwicklung und Verfügbarmachung von Jupyter-Notebooks-basierten Anwendungen zur Verarbeitung und Publikation von Daten, ebenso an der Erstellung des [Text+-Webportals](https://text-plus.org/).
* Die GWDG betreibt außerdem in Text+ ein Datenarchiv, das unabhängig von den Spezialisierungen der anderen Datenzentren ist, und steht für die Bitstream-Archivierung für alle Daten der Community von Text+ als Fallback zur Verfügung.
    
## Entgegennahme Daten Dritter 
Das Langzeitarchiv kann mittels Self Deposit frei von der Community für die Archivierung von Forschungsdaten zum Zwecke der Langzeitarchivierung genutzt werden.

## Kontakt 
    
**Ansprechperson für Text+:** [George Dogaru](mailto:george.dogaru@gwdg.de) und [Bernd Schlör](mailto:bernd.schloer@gwdg.de)

