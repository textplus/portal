---  
title: Bayerische Akademie der Wissenschaften (BAdW)  
short_name: BAdW  
text_plus_domains:  
- Collections   
- Lexikalische Ressourcen   
- Editionen
  
external_url: "https://badw.de/die-akademie.html"  
  
type: competence-center  
---  
  
# Bayerische Akademie der Wissenschaften (BAdW)  
      
**Text+ Zentrum:** [BAdW Digital Huamnities Forschung & Entwicklung](https://badw.de/badw-digital.html)  
  
**Zentrumstyp:** Kompetenzzentrum  
  
Die Bayerische Akademie der Wissenschaften ist die größte sowie eine der forschungsstärksten und ältesten der acht Landesakademien in Deutschland. Sie betreibt innovative Langzeitforschung, vernetzt Gelehrte über Fach- und Ländergrenzen hinweg, wirkt mit ihrer wissenschaftlichen Expertise in Politik und Gesellschaft, fördert den wissenschaftlichen Nachwuchs und ist ein Forum für den Dialog zwischen Wissenschaft und Öffentlichkeit.   

Die BAdW stellt Forschungsergebnisse und Forschungsdaten langfristig bereit. Durch das Leibniz-Rechenzentrum ist die Archivierung digitaler Daten für beliebig lange Zeit gesichert.    
Darüber hinaus unterstützt das DH-Referat der BAdW insbesondere die geisteswissenschaftlichen Forschungsvorhaben bei der Digitalisierung. Die Unterstützung deckt dabei alle Phasen eines Forschungsprojekts ab: Sie beginnt mit der Beratung zu den digitalen Anteilen von Forschungsanträgen und endet mit der Langzeitarchivierung des digitalen Materials abgeschlossener Projekte.  
      
## Highlights bereitgestellter Daten und Dienste  
* Unter https://publikationen.badw.de/ als zentralem Ort werden Publikationen der Bayerischen Akademie der Wissenschaften und ihrer Forschungsvorhaben digital zur Verfügung gestellt
* [Thesaurus Linguae Latinae](https://publikationen.badw.de/de/thesaurus/lemmata): Ein praktisch vollständiges Wörterbuch des antiken Lateins
* [Bayerns Dialekte Online](https://bdo.badw.de/): Ein digitales Sprachinformationssystem zu den bayerischen Dialekten: Fränkisch, Schwäbisch, Bairisch 
* https://geschichtsquellen.de: Ein vielgenutztes und laufend aktualisiertes Verzeichnis historischer Quellen zur Mittelalterforschung 
* https://www.bibeluebersetzer-digital.de: Edition einer noch vor Luther entstanden deutschen Bibelübersetzung eines unbekannten Autors  
* https://daten.badw.de: Im Aufbau befindliche zentrale Anlaufstelle für Forschungsdaten der BAdW  

Quelloffene Software:  
* [DHParser](https://dhparser.readthedocs.io): Ein Parser-Generator und Baukasten für domänenspezifische Notationen, Tanspiler etc.
* [Geist](https://gitlab.lrz.de/badw-it/geist): Ein leichtgewichtiges Framework für schwergewichtige Web-Publikationen   
* [Heureka-HTML](https://dhmuc.hypotheses.org/files/2022/10/Speakeasy_HTML_dhmuc_22-09-12.pdf): Eine leichtgewichtige und moderne Alternative zu TEI-XML
      
  
## Kontakt   
 **Ansprechperson für Text+:** [Eckart Arnold](mailto:Arnold@badw-muenchen.de)
 
 **Digitalisierung, Publikationsserver, Website-Technik:** digitalisierung@badw.de
