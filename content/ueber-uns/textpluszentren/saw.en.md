---
title: Saxon Academy of Sciences and Humanities in Leipzig (SAW)
short_name: SAW
text_plus_domains:
- Lexical Resources 
- Infrastructure/Operations 

text_plus_clusters:
  Lexikalische Ressourcen: 
  - German Dictionaries in a European Context 
  - Born-Digital Lexical Resources 
  - Non-Latin Scripts 

external_url: "https://www.saw-leipzig.de/en"

type: competence-center
---

# Saxon Academy of Sciences and Humanities in Leipzig (SAW)

**Text+ center:** [Saxon Academy of Sciences and Humanities in Leipzig repository](https://repo.data.saw-leipzig.de/en)

**Type of center:** data center/competence center

The repository provides long-term preservation of digital resources and their metadata. Its mission is to ensure the long-term availability of research data, to secure research results, to facilitate the transfer of knowledge to new subject areas and to integrate new methods and resources into the university curriculum. A particular focus is on lexical resources, text corpora, and general language resources for so-called “underrepresented” languages.

In particular, the SAW contributes its expertise in the areas of search and retrieval in distributed environments, metadata infrastructure and semantic web technology as well as quality assurance of services and data to the Text+ infrastructure.

## Highlights of provided data and services

* [Wortschatz Leipzig](https://wortschatz-leipzig.de/en): The international corpora portal offers access to more than 900 corpora of the Leipzig Corpora Collection (LCC) in over 250 languages
* [Canonical Text Services (CTS)](http://cts.informatik.uni-leipzig.de/Canonical_Text_Service.html): Protocol for identifying and retrieving text passages cited by canonical references
* [Lower-Sorbian-German dictionaries](https://niedersorbisch.de/ndw/): digitized dictionaries for Lower-Sorbian-German, mapped into the standardized format TEI Lex-0

## Third-party data reception 
Within Text+, the SAW particularly takes on lexical resources and text corpora.

## Contact

**Contact for Text+:** clarin@saw-leipzig.de
