---
title: Niedersächsische Akademie der Wissenschaften zu Göttingen (AdWGö)
short_name: AdWGö
text_plus_domains:
- Editionen 
- Lexikalische Ressourcen 

external_url: "https://adw-goe.de/startseite/"

type: competence-center
---

# Niedersächsische Akademie der Wissenschaften zu Göttingen (AdWGö)
    
**Text+ Zentrum:** [Göttinger Digitale Akademie](https://digitale-akademie.adw-goe.de/)

**Zentrumstyp:** Kompetenzzentrum

Die Akademie der Wissenschaften zu Göttingen hat sich früh auf den digitalen Umbruch in den Geisteswissenschaften eingestellt und vielfach Pionierarbeit geleistet. Das Land Niedersachsen fördert seit dem 1. Januar 2022 ein Projekt an der Niedersächsischen Akademie der Wissenschaften zu Göttingen, das die nachhaltige Digitalisierung in den geisteswissenschaftlichen Langzeitvorhaben vorantreibt. 

Das Projekt „Göttinger Digitale Akademie“ hat die Aufgabe, die Forschungsprojekte der Akademie digital so aufzustellen, dass sie an die Nationale Forschungsdateninfrastruktur (NFDI) angebunden werden können.

Das Ziel ist eine dauerhafte digitale und frei zugängliche Bereitstellung der im Rahmen der Akademievorhaben im Akademienprogramm erarbeiteten Forschungsdaten, Online-Portale und Plattformen für die geisteswissenschaftliche Forschung. Hierzu zählen vor allem die an der Göttinger Akademie oder in interakademischen Projekten betriebenen Epochenwörterbücher, digitale Editionen, historische Datenbanken und Handbücher sowie Projekte aus dem Bereich der Bibelwissenschaften.
    
## Highlights bereitgestellter Daten und Dienste
    
* [res doctae](https://rep.adw-goe.de/) Repositorium/Dokumentenserver der NAWG de: In diesem dspace Repositorium speichert die Akademie Forschungsdaten und Publikationen aus den Vorhaben des Akademienprogramms.  
* [Webarchiv](https://webarchiv.adw-goe.de): Das Webarchiv bietet Zugriff auf verschiedene Versionen der online Portale und kann mit [solrwayback](https://github.com/netarchivesuite/solrwayback) projektübergreifend durchsucht werden.
* [Digitale Bibliothek](https://adw-goe.de/publikationen/digitale-bibliothek/): Die Digitale Bibliothek der Göttinger Akademie bietet eine Übersicht zu den Forschungsdaten der Projekte und den online-Plattformen der Akademie.
* Research data hosting (https://digiberichte.de/home/; https://kuvis.adw-goe.de etc.): Die GDA bringt Forschungsdaten aus dem Umfeld von ausgelaufenen Akademievorhaben und Forschungsprojekten wieder online und nutzt dazu eine modern, dockerbasierte Infrastruktur. 
* [Fortbildungen](https://digitale-akademie.adw-goe.de/projekte/fortbildungen/): Die GDA organisiert Fortbildungen für MitarbeiterInnen der Akademievorhaben, die teilweise auch für externe Teilnehmende offen sind. Themen in den letzten Jahren waren und sind u.a. KI-Methoden im Akademienprogramm, Datenarchivierung und Datenpflege, Urheber- und Leistungsschutzrechte, Linked Open Data und SW-Technologie.
    

## Kontakt 
    
**Ansprechperson für Text+:** [Dr. Jörg Wettlaufer](mailto:joerg.wettlaufer@adwgoe.de)

