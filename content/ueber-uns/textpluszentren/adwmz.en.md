---
title: Academy of Sciences and Literature | Mainz (AWLM)
short_name: AWLM
text_plus_domains:
- Editions 
text_plus_clusters:
  Editionen: 
  - Ancient and Medieval Texts 
  - Early Modern, Modern, and Contemporary Texts 

external_url: "https://www.adwmainz.de/en/"

type: competence-center
---

# Academy of Sciences and Literature | Mainz (AWLM)

**Text+ center:** [Digital Academy](https://www.adwmainz.de/en/digitalitaet/digitale-akademie.html)

**Type of center:** data center/competence center

The AWLM contributes to Text+ with numerous digital editions, text collections and software applications. Thematically, these range from large medieval and early modern text corpora to editions focusing on 19th and 20th century sources. In addition, the AWLM hosts and curates overarching research information systems such as AGATE (a European portal for the Academies of Sciences and Humanities) or the “Portal Kleine Fächer” (together with the Johannes Gutenberg University). In technical terms, AWLM will provide Text+ solutions for the creation of integrated edition and web portals, annotation software for graph-based digital editions and LOD applications that enable the semantic enrichment and linking of digital scientific editions.

## Highlights of provided data and services

Software:
* [Cultural Heritage Framework](https://github.com/digicademy-chf): CMS-based extension suite for the management and publication of digital heritage resources
* [LOD.ACADEMY – A hub for Linked Open Data in Academies of Sciences and Beyond with the Service XTriples](https://lod.academy/site/): crawling XML repositories and extracting RDF statements with a simple configuration based on XPATH/XQuery expressions

Digital editions:
* [Hans Kelsen Werke (HKW)](https://agate.academy/id/PR270): The complete oeuvre of the Austrian-American legal scholar Hans Kelsen (1881-1973) will be made available in a historical-critical hybrid edition by 2042.
* [Buber-Korrespondenzen Digital](https://agate.academy/id/PR768): Aim of the long-term project (until 2045) is a digital edition of letters, the focus of which will be on systematic reconstruction, editorial indexing to produce a text as true to the original as possible, and the cultural-historical analysis of Martin Buber's dialogical relationships and networks of scholars and intellectuals.
* and much more.

## Third-party data reception 
Within Text+, the Academy in Mainz particularly takes on data that belongs to editions and fits in with its work focus.

## Contact

**Contact for Text+:** [Martin Sievers](mailto:martin.sievers@adwmainz.de)
