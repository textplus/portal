---
title: Otto-Friedrich-Universität Bamberg (UniBA)
short_name: UniBA
text_plus_domains:
- Infrastruktur/Betrieb

external_url: "https://www.uni-bamberg.de/"

type: competence-center
---

# Otto-Friedrich-Universität Bamberg (UniBA)
    
**Text+ Zentrum:** [Zentrum für innovative Anwendungen der Informatik](https://www.uni-bamberg.de/zentren/ziai/)

**Zentrumstyp:** Kompetenzzentrum

Das Zentrum für innovative Anwendungen der Informatik (ZIAI) fördert fächerübergreifende Forschung auf dem Gebiet der Angewandten Informatik und deren Transfer in Zusammenarbeit mit Fächern aus den Forschungsschwerpunkten der Universität. Es befasst sich insbesondere mit der Erforschung, Entwicklung und Erprobung von innovativen Informationstechnologien für geistes-, kultur-, sozial- und humanwissenschaftliche Disziplinen.

Die Technologieentwicklung wird getragen von Forscherinnen und Forschern der Angewandten Informatik, während die Vielfalt der Anwendungsperspektiven durch informatiknah forschende Mitglieder aus anderen Fächern vertreten wird. Die Gründungsmitglieder stammen aus allen vier Fakultäten der Universität.

Das Zentrum fördert die Kooperation mit Institutionen und Unternehmen der Angewandten Informatik sowie mit der nationalen und internationalen Forschung auf diesem Gebiet. Es stellt ein Beratungsangebot für Methoden der Angewandten Informatik bereit.

In Text+ liegt der Schwerpunkt der Arbeiten auf Konzepten und Systemen zur Strukturierung und zur Auffindbarkeit und Interoperabilität der Angebote von Text+. Hierzu gehören Arbeiten am Datenkatalog oder allgemeiner an der Registry, Arbeiten im Bereich der Datenintegration sowie der Suche und des Filterns insbesondere in den Metadatenbeständen.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Bamberg Survey of Language Variation and Change](https://www.uni-bamberg.de/zentren/ziai/ag-digitale-ueberlieferung/the-international-corpus-of-english-malta-puerto-rico/): digitale Datenbank lexikalischer und grammatischer Präferenzen von Sprecher*innen verschiedener Varietäten des Englischen.
* [Steinerne Zeugen digital. Deutsch-jüdische Sepulkralkultur zwischen Mittelalter und Moderne](https://www.uni-bamberg.de/zentren/ziai/ag-digitale-ueberlieferung/steinerne-zeugen-digital/): Durch Auswahl, Sammlung und Edition hebräischer Grabinschriften und die Analyse von Form, Struktur, Konstruktion, Material und Erhaltung wird ein digitales Text- und Bildkorpus erstellt.
* [Die Text+ Registry](https://registry.text-plus.org) als Nachweissystem für Text+: In der föderierten Struktur von Text+ werden die Informationen zu den verschiedenen Angeboten und Ressourcen an einer Stelle zusammengeführt. Dazu werden Ansätze zur Anbindung heterogener Quellen und zur Datenintegration innovativ kombiniert.
    

## Kontakt 
    
**Ansprechperson für Text+:** [Tobias Gradl](mailto:tobias.gradl@uni-bamberg.de)

