---
title: Technical University of Darmstadt, University and State Library Darmstadt, University of Applied Sciences Darmstadt (DACo)
short_name: DACo
text_plus_domains:
- Editions 

text_plus_clusters:
  Editionen: 
  - Early Modern, Modern, and Contemporary Texts 

external_url: "https://www.tu-darmstadt.de/index.en.jsp"

type: competence-center
---

# Technical University of Darmstadt, University and State Library Darmstadt, University of Applied Sciences Darmstadt (DACo)

**Text+ center:** [Zentrum für digitale Editionen in Darmstadt](https://www.ulb.tu-darmstadt.de/die_bibliothek/einrichtungen/zeid/index.de.jsp)

**Type of center:** data center/competence center

DACo consists of three partners with a long tradition of institutional and personal collaboration in research, infrastructure development, teaching and training in the field of textual studies, digital editions and beyond: the Institute of Linguistics and Literary Studies, the University and State Library Darmstadt (USLDA), both at the Technische Universität Darmstadt (TUDa), and the Chair of Information Science/Digital Library at the Darmstadt University of Applied Sciences.

The University and State Library operates the institutional repository for all research data created or worked with at the Technical University and founded the Centre for Digital Editions in Darmstadt (ZEiD) in 2019. ZEiD operates and supports a range of digital editions, including long-term projects funded by the Academies' Programme, projects funded by the German Research Foundation (DFG), small projects by individual researchers without funding and other projects with a particular focus on letters.

In Text+, the working group in Darmstadt concentrates on historical texts, including those from the field of editions. There is also a focus on education and training in the field of data literacy.

## Highlights of provided data and services

ZEiD covers all aspects of the preparation of texts for scholarly editions and all areas of digital editions, from planning to publication. Among others:
* [Digitisation of the Darmstädter Tagblatt (1740 - 1986)](https://www.ulb.tu-darmstadt.de/forschen_publizieren/forschen/darmstaedter_tagblatt.de.jsp): Digitisation of issues of the Darmstädter Tagblatt spanning three centuries
* [European Religious Peace Digital](https://www.ulb.tu-darmstadt.de/forschen_publizieren/forschen/eured.de.jsp): Critical digital edition of European religious peace settlements
* [Open Access Transformation by Cooperation (OATbyCO)](https://www.ulb.tu-darmstadt.de/forschen_publizieren/forschen/oatbyco.de.jsp): Establishment of a joint workflow based on XML/TEI between publisher and library for the creation of open access texts

## Third-party data reception 
Within Text+, the partners in Darmstadt particularly takes on data that belongs to editions and fits in with its work focus.

## Contact

**Contact for Text+:** [Zentrum für digitale Editionen](mailto:zeid@ulb.tu-darmstadt.de)

