---
title: University of Bamberg (UniBA)
short_name: UniBA
text_plus_domains:
- Infrastructure/Operations 

external_url: "https://www.uni-bamberg.de/en/"

type: competence-center
---

# University of Bamberg (UniBA)

**Text+ center:** [Center for Innovative Applications of Computer Science](https://www.uni-bamberg.de/en/centres/translate-to-english-zentrum-fuer-innovative-anwendungen-der-informatik/)

**Type of center:** competence center

The Center for Innovative Applications of Computer Science (ZIAI) promotes interdisciplinary research in the field of applied computer science and its transfer in cooperation with subjects from the university's research focus areas. It is particularly concerned with the research, development and testing of innovative information technologies for the humanities, cultural studies, social sciences and human sciences.

Technology development is supported by researchers in applied computer science, while the variety of application perspectives is represented by computer science-related research members from other subjects. The founding members come from all four faculties of the university.

The center promotes cooperation with institutions and companies in applied computer science as well as with national and international research in this field. It provides advice on methods of applied computer science.

In Text+, the focus of the work is on concepts and systems for structuring and for the findability and interoperability of the Text+ offerings. This includes work on the data catalogue or more generally on the registry, work in the area of data integration as well as searching and filtering, particularly in the metadata collections.

## Highlights of provided data and services

* [Bamberg survey of Language Variation and change](https://www.uni-bamberg.de/zentren/ziai/ag-digitale-ueberlieferung/the-international-corpus-of-english-malta-puerto-rico/): digital database of lexical and grammatical preferences of speakers of different varieties of English.
* [Stone Witnesses digital. German-Jewish Sepulchral Culture between the Middle Ages and Modernity](https://www.uni-bamberg.de/zentren/ziai/ag-digitale-ueberlieferung/steinerne-zeugen-digital/): By selecting, collecting and editing Hebrew grave inscriptions from German-speaking countries and by analyzing the form, structure, construction, material and preservation, a digital text and image corpus is being created. 
* [The Text+ Registry](https://registry.text-plus.org) as a reference system for Text+: In the federated structure of Text+, information on the various offers and resources is brought together in one place. To this end, approaches for connecting heterogeneous sources and for data integration are innovatively combined.


## Contact

**Contact for Text+:** [Tobias Gradl](mailto:tobias.gradl@uni-bamberg.de)

