---
title: Klassik Stiftung Weimar (KSW)
short_name: KSW
text_plus_domains:
- Editions 

text_plus_clusters:
  Editionen: 
  - Early Modern, Modern, and Contemporary Texts 

external_url: "https://www.klassik-stiftung.de/en/"

type: competence-center
---

# Klassik Stiftung Weimar (KSW)

**Text+ center:** [Klassik Stiftung Weimar](https://www.klassik-stiftung.de/en/home/digital/)

**Type of center:** competence center

The Klassik Stiftung Weimar is a unique ensemble of cultural monuments. With its numerous museums, palaces, historic houses and parks as well as its collections of literature and art, it is one of the largest and most important cultural institutions in Germany. 

The Klassik Stiftung’s work focuses on the era of Weimar Classicism and its effects on the art and culture of the 19th century as well as on the modernity with Franz Liszt, Friedrich Nietzsche, Henry van de Veldeand Bauhaus. By bridging the gap with present-day arts and sciences, the Stiftung aims to create scope for the discussion of questions in the spirit of these traditions.

Research data and digital images are being progressively published as part of cataloging and editing projects and made available for research in accordance with the FAIR principles.

## Highlights of provided data and services

* [Research database so:fie](https://ores.klassik-stiftung.de/ords/f?p=900:1):  The name so:fie stands for: "Collections online: research, inform, discover". The database currently contains detailed information on around 35,000 individuals, 1,000 corporate bodies and 2,500 geographical objects spanning more than three centuries - and for the first time links the databases of the Goethe and Schiller Archives and the museums with a common user interface.
* [Goethe und Schiller Archive](https://www.klassik-stiftung.de/goethe-und-schiller-archiv/): The archive database allows you to search the entire collection in the archiv. Furthermore, there are different specific databases. With a total collection of around 5 million pages, it is a central archive, especially for German-language literature and culture of the 18th, 19th and early 20th centuries.
* [Digitization of cultural assets](https://www.klassik-stiftung.de/digital/digitalisierung-von-kulturgut/): In its collections, the Klassik Stiftung Weimar preserves a unique fund of museum objects, works of art, bequests, old prints, photographs, drawings and graphic sheets, which are part of the core of the national cultural heritage. The historic buildings themselves and the parks are also part of the cultural assets that need to be preserved and communicated in a contemporary way. The collections and holdings are digitised for the purpose of indexing and researching the objects.
* [PROPYLÄEN – Research platform on the life of Goethe](https://goethe-biographica.de/): The Academy project represents a pivotal element in the digital transformation of the KSW. The objective is to create a digital portal on Goethe’s life, work and oeuvre based on Goethe's Biographica. Furthermore, the project will continue and complete the historical-critical editions of Goethe's letters and diaries, the full-text edition of the letters to Goethe and the edition of Goethe’s “Begegnungen und Gespräche” (Encounters and Conversations), which are currently in progress at the Goethe and Schiller Archive. The content and research functions will be expanded in a phased approach.


## Contact

**Contact for Text+:** [Gerrit Brüning](mailto:gerrit.bruening@klassik-stiftung.de), [Dirk Wintergrün](mailto:dirk.wintergruen@klassik-stiftung.de)