---
title: Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen (GWDG)
short_name: GWDG
text_plus_domains:
- Collections
- Lexical Resources
- Editions
- Infrastructure/Operations

external_url: "https://gwdg.de/en/"

type: competence-center
---

# Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen (GWDG)

**Text+ center:** [Long-term Preservation Archive](https://text-plus-archiv.gwdg.de/)

**Type of center:** data center/competence center

As the university computing centre for the [Georg-August-Universität Göttingen](https://www.uni-goettingen.de/en/1.html), and as a computing and IT competence centre for the [Max Planck Society](https://www.mpg.de/en), the GWDG offers a wide range of information and communication services for science.

Through own research in the field of computer science and participation in numerous research projects, the GWDG creates the basis for innovative, customer-oriented IT solutions.

The GWDG together with the Göttingen State and University Library coordinates the Text+ task area Infrastructure/Operations. The GWDG contributes competencies in the field of Authentication Authorization Infrastructure (AAI) and Persistent Identifiers (PID) to Text+ and develops them further.

## Highlights of provided data and services

* The GWDG operates a [Jupyterhub](https://jupyter-cloud.gwdg.de/) and is significantly involved in the development and provision of Jupyter notebook-based applications for data processing and publication, as well as in the creation of the [Text+ web portal](https://text-plus.org/).
* The GWDG operates a data archive in Text+ that is independent of the specializations of the other data centers and is available as a fallback for bitstream archiving for all data from the Text+ community.

## Third-party data reception 
The long-term archive can be freely used by the community for the self-deposit and archiving of research data for the purpose of long-term preservation.

## Contact

**Contact for Text+:** [George Dogaru](mailto:george.dogaru@gwdg.de) and [Bernd Schlör](mailto:bernd.schloer@gwdg.de)

