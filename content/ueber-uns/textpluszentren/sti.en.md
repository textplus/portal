---
title: Salomon Ludwig Steinheim Institute (STI)
short_name: STI
text_plus_domains:
- Editions 

text_plus_clusters:
  Editionen: 
  - Ancient and Medieval Texts 
  - Early Modern, Modern, and Contemporary Texts 

external_url: "http://www.steinheim-institut.de"

type: competence-center
---

# Salomon Ludwig Steinheim Institute (STI)

**Text+ center:** [Steinheim-Institute](http://www.steinheim-institut.de)

**Type of center:** competence center

The Steinheim Institute has been active in the field of digital humanities for many years: digital editions of Hebrew inscriptions and memoria, documentation of Jewish cemeteries, digital collections and bibliographies, georeferenced online databases on German-Jewish history, linked data applications, collaboration in infrastructure projects such as Text+, DARIAH and TextGrid.

The Steinheim Institute is involved in the Text+ network in the Editions task area. One of the focal points is the area of ‘Linked Open Data’: application of authority data (GND) and linking up digital editions.

## Highlights of provided data and services

* The institute has published around 250 digital editions on Jewish cemeteries with around 50,000 individual grave inscriptions (Hebrew/German/Yiddish). The data is accessible in a standard format (TEI/XML) and under a Creative Commons License.
* The STI Linked Data Service is a component of the Linked Data Strategy of the Steinheim Institute. The Institute's digital publications are semantically linked on the web beyond the Institute's domain using this service.

## Third-party data reception 
Within Text+, the Steinheim Institute  takes on data that belongs to the research field of Jewish cemeteries, in particular, gravestone inscriptions and photo documentation.

## Contact

**Contact for Text+:** [Harald Lordick](mailto:lordick@steinheim-institut.org)
