---
title: Albert-Ludwigs-Universität Freiburg (UniFR)
short_name: UniFR
text_plus_domains:
- Collections 

text_plus_clusters:
  Collections: 
  - Gegenwartssprache 

external_url: "https://uni-freiburg.de/"

type: competence-center
---

# Albert-Ludwigs-Universität Freiburg (UniFR)
    
**Text+ Zentrum:** [Englisches Seminar](https://www.anglistik.uni-freiburg.de/abteilungen/sprachwissenschaft/lsmair/index.html)

**Zentrumstyp:** Kompetenzzentrum

Das Kompetenzzentrum an der Universität Freiburg bietet umfassende Unterstützung und Beratung im Bereich der englischsprachigen Korpora, und zwar in allen Bereichen des korpuslinguistischen Arbeitsprozesses, von der Datenaufbereitung und –annotation bis zur Analyse und Visualisierung der Ergebnisse. Es blickt auf eine lange Tradition der Arbeit mit englischsprachigen Korpora zurück und hat international sichtbare Kompetenzen in diesem Bereich aufgebaut. Mair und sein Team sind führend bei der korpusbasierten Analyse von aktuell ablaufenden Sprachwandelsprozessen und der regionalen Ausdifferenzierung des Englischen. Mair hat zum Ausbau breit genutzter Forschungsinfrastrkturen („Brown family“ und International Corpus of English (ICE)) beigetragen und Großkorpora mit Daten aus den sozialen Medien erstellt, die die Erforschung des Englischen in mehrsprachigen postkolonialen Migrationskontexten ermöglichen. Diese Korpora werden aktuell für die Überführung in die Text+ Infrastruktur aufbereitet.

Das Freiburger Zentrum war auch treibende Kraft bei den Verhandlungen um eine Nationallizenz für die weltweit größte [Sammlung englischsprachiger Web-Corpora](https://www.english-corpora.org/). In Freiburg beteiligt sich Mair bei der Etablierung des Freiburger Digital Humanities Lab, in dem Studierende ein DH-Zeritifikat erwerben können und die vielfältige lokale und überregionale Beratungstätigkeit auf nachhaltige Weise verstetigt und ausgebaut werden wird.

Das Freiburger Zentrum ist zentraler Akteur in der Arbeitsgruppe „Dissemination“ der Task Area „Collections“. Es fungiert als eine Schnittstelle zwischen Text+ und den philologischen Fachverbänden, insbesondere den Digitalbeauftragten der jeweiligen Vorstände, und präsentiert das Text+ Angebot auf Tagungen.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Brown family](https://clarino.uib.no/korpuskel/home)
* [International Corpus of English (ICE)](https://www.ice-corpora.uzh.ch/en/joinice/Teams/icejam.html): Das ICE stellt Daten unterschiedlicher Varianten des Englischen zur Verfügung. Das Freiburger Kompetenzzentrum war am Aufbau des Korpus mit dem Jamaikanischen Englisch beteiligt.
    
## Entgegennahme Daten Dritter 
Keine Annahme, sondern Vermittlung an geeignetes Text+ Repositorium

## Kontakt 
    
**Ansprechperson für Text+:** [Christian Mair](mailto:christian.mair@anglistik.uni-freiburg.de)

