---
title: University of Cologne (UniK)
short_name: UniK
text_plus_domains:
- Collections 
- Lexical Resources

text_plus_clusters:
  Collections: 
  - Contemporary Language
  Lexikalische Ressourcen:
  - Born-Digital Lexical Resources
  - Non-Latin Scripts

external_url: "https://portal.uni-koeln.de/en/uoc-home"

type: competence-center
---

# University of Cologne (UniK)

**Text+ center:** [Language Archive Cologne / Data Center for the Humanities](https://lac.uni-koeln.de/)

**Type of center:** data center/competence center

The University of Cologne (UniK) is internationally visible as a research location in the field of Digital Humanities/eHumanities. As a faculty institution, the Data Center for the Humanities (DCH) is actively involved in teaching the relevant courses in Digital Humanities and (Linguistic) Information Processing at the University of Cologne.

For Text+, the UniK and the DCH provide data from two different domains, collections and lexical resources. The DCH has a particular focus on language data from the Global South and has special expertise in data collections and lexical resources on endangered and non-European languages as well as in recordings of non-European oral literature.

## Highlights of provided data and services

Collections:
* [Documentation of language variety in Latin America and the Caribbean](https://lac.uni-koeln.de/collection/11341/0000-0000-0000-3D31): Audio recordings capturing the linguistic varieties of Spanish and Portuguese of indigenous languages
* [Oral Tales of Mongolian Bards](https://lac.uni-koeln.de/collection/11341/0000-0000-0000-271F): Oral tales of bards from eastern Inner Mongolia, including epic poems and tales based on Chinese adventure novels

Lexical resources: 
* [Kosh](https://cceh.github.io/kosh/): Generic infrastructure for publishing any XML-based lexical resources via standardized Application Programming Interfaces (APIs)
* [Cologne Digital Sanskrit Dictionaries](https://sanskrit-lexicon.uni-koeln.de/): The largest resource for the classical South Asian language Sanskrit

## Third-party data reception 
UniK offers their archive services to any researchers, institutions and individuals – explicitly including students and native speakers – worldwide and in particular in regions where there are no local institutions offering such support.

## Contact

**Contact for Text+:** lac-manager@uni-koeln.de
