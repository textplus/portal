---
title: Technische Universität Darmstadt, Universitäts- und Landesbibliothek Darmstadt, Hochschule Darmstadt (DACo)
short_name: DACo
text_plus_domains:
- Editionen 

text_plus_clusters:
  Editionen: 
  - Frühe moderne, moderne und zeitgenössische Texte

external_url: "https://www.tu-darmstadt.de/"

type: competence-center
---

# Technische Universität Darmstadt, Universitäts- und Landesbibliothek Darmstadt, Hochschule Darmstadt (DACo)
    
**Text+ Zentrum:** [Zentrum für digitale Editionen in Darmstadt](https://www.ulb.tu-darmstadt.de/die_bibliothek/einrichtungen/zeid/index.de.jsp)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Die DACo besteht aus drei Partnern mit einer langen Tradition institutioneller und persönlicher Zusammenarbeit in Forschung, Infrastrukturentwicklung, Lehre und Ausbildung auf dem Gebiet der Textwissenschaft, der digitalen Editionen und darüber hinaus: dem Institut für Sprach- und Literaturwissenschaft, der Universitäts- und Landesbibliothek Darmstadt (USLDA), beide an der Technischen Universität Darmstadt (TUDa), und dem Lehrstuhl für Informationswissenschaft/Digitale Bibliothek an der Hochschule Darmstadt.

Die Universitäts- und Landesbibliothek betreibt das institutionelle Repositorium für alle Forschungsdaten, die an der Technischen Universität entstehen oder mit denen an der Technischen Universität gearbeitet wird, und hat 2019 das Zentrum für digitale Editionen in Darmstadt (ZEiD) gegründet. Das ZEiD betreibt und unterstützt eine Reihe von digitalen Editionen, darunter Langzeitprojekte, die im Rahmen des Akademienprogramms gefördert werden, Projekte, die von der Deutschen Forschungsgemeinschaft (DFG) finanziert werden, sowie Kleinprojekte von Einzelforschenden ohne Förderung und andere Projekte mit besonderem Schwerpunkt auf Briefen.

Die Arbeitsgemeinschaft in Darmstadt konzentriert sich in Text+ auf historische Texte, auch aus dem Bereich der Editionen. Dazu liegt ein Schwerpunkt auf Aus- und Weiterbildung im Bereich der Data Literacy.
    
## Highlights bereitgestellter Daten und Dienste
    
Das ZEiD deckt alle Aspekte der Aufbereitung von Texten für wissenschaftliche Editionen und alle Bereiche digitaler Editionen, von der Planung bis zur Veröffentlichung, ab. U.a:
* [Digitalisierung des Darmstädter Tagblatts (1740 - 1986)](https://www.ulb.tu-darmstadt.de/forschen_publizieren/forschen/darmstaedter_tagblatt.de.jsp): Digitalisierung von Ausgaben des Darmstädter Tagblattes über drei Jahrhunderte hinweg
* [Europäische Religionsfrieden Digital](https://www.ulb.tu-darmstadt.de/forschen_publizieren/forschen/eured.de.jsp): Kritische digitale Edition von europäischen Religionsfriedensregelungen
* [Open Access Transformation by Cooperation  (OATbyCO)](https://www.ulb.tu-darmstadt.de/forschen_publizieren/forschen/oatbyco.de.jsp):  Etablierung eines gemeinsamen Workflows auf der Basis von XML/TEI von Verlag und Bibliothek zur Erstellung von Open-Access-Texten
    
## Entgegennahme Daten Dritter 
Innerhalb von Text+ übernehmen die Darmstädter Partner insbesondere Daten, die zu Editionen gehören und zu ihrem Arbeitsschwerpunkt passen.

## Kontakt 
    
**Ansprechperson für Text+:** [Zentrum für digitale Editionen](mailto:zeid@ulb.tu-darmstadt.de)

