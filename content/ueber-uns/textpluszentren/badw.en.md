---  
title: Bavarian Academy of Sciences and Humanities (BAdW)  
short_name: BAdW  
text_plus_domains:  
- Collections   
- Lexical Resources   
- Editions   
  
external_url: "https://badw.de/en/the-academy.html"  
  
type: competence-center  
---  
  
# Bavarian Academy of Sciences and Humanities (BAdW)  
  
**Text+ center:** [BAdW Digital Humanities Research & Development](https://badw.de/en/digital-academy.html)  
  
**Type of center:** competence center  
  
The Bavarian Academy of Sciences and Humanities is the largest and one of the oldest of the eight state academies in Germany. It conducts innovative long-term research, networks scholars across disciplines and national borders, uses its scientific expertise to influence politics and society, promotes young scientists and is a forum for dialogue between science and the public.   

The BAdW makes research results and research data available on a long-term basis. The Leibniz Supercomputing Center ensures the archiving of digital data for any length of time.    
In addition, the BAdW's IT department supports the digitization of research projects in the humanities in particular. The support covers all phases of a research project: it begins with advice on the digital parts of research proposals and ends with the long-term archiving of digital material from completed projects.  
  
## Highlights of provided data and services  
  
* Publications of the Bavarian Academy of Sciences and Humanities and its research projects are made available digitally at https://publikationen.badw.de/ as a central location
* [Thesaurus Linguae Latinae](https://publikationen.badw.de/de/thesaurus/lemmata): A practically complete dictionary of ancient Latin
* [Bavarian Dialects Online](https://bdo.badw.de/): A digital language information system on the Bavarian dialects: Franconian, Swabian, Bavarian
* https://geschichtsquellen.de: A widely used and constantly updated directory of historical sources for medieval research
* https://www.bibeluebersetzer-digital.de: Edition of a German translation of the Bible by an unknown author written before Luther  
* https://daten.badw.de: Central contact point for BAdW research data (still under construction)  

Open Source Software:   
* [DHParser](https://dhparser.readthedocs.io): A parser-generator and construction kit for domain-specific notations, tanspilers etc.
* [Geist](https://gitlab.lrz.de/badw-it/geist): A lightweight framework for heavy-weight web-publications
* [Heureka-HTML](https://dhmuc.hypotheses.org/files/2022/10/Speakeasy_HTML_dhmuc_22-09-12.pdf): A lightweight and modern alternative to TEI-XML  
  
## Contact  
  
**Contact for Text+:** [Eckart Arnold](mailto:Arnold@badw-muenchen.de)

**Digitization, institutional repository, website technology:** digitalisierung@badw.de
