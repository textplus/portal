---
title: Hochschule Wismar (HSWI)
short_name: HSWI
text_plus_domains:
- Infrastruktur/Betrieb
- Collections 

text_plus_clusters:
  Collections: 
  - Unstrukturierte Texte 
  Lexikalische Ressourcen: 
  - Nicht-lateinische Schriftsysteme 

external_url: "https://www.hs-wismar.de/"

type: competence-center
---

# Hochschule Wismar (HSWI)
    
**Text+ Zentrum:** [Hochschule Wismar](https://www.hs-wismar.de/forschung/aus-der-forschung/fdb/)

**Zentrumstyp:** Kompetenzzentrum

Die Hochschule Wismar ist eine leistungsstarke Forschungsstätte mit einer langjährigen Tradition. Sie ist international ausgerichtet und verwurzelt in Mecklenburg-Vorpommern. Die interdisziplinäre Vernetzung der drei Fakultäten für Ingenieurwissenschaften, Wirtschaftswissenschaften und Gestaltung stellt ein besonderes Markenzeichen dar und ist in vier fakultätsübergreifenden Forschungsschwerpunkten dokumentiert. 

In Hinblick auf die text- und sprachbasierten Wissenschaften, wie sie durch das Text+ Konsortium unterstützt werden, versteht sich die Hochschule Wismar als potenzieller Nutzer der Infrastruktur und als Quelle von Forschungsdaten. Die Hochschule Wismar nimmt damit innerhalb des Text+ Konsortiums die Rolle eines Anwenders ein und fungiert innerhalb des Landes MV als Inkubator, indem sie sich zum Beispiel im landesweiten FDM-Netzwerk engagiert.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Werkzeugunterstützung für die automatische Extraktion von Tabellendaten aus historischen Zeitungen](https://www.hs-wismar.de/forschung/aus-der-forschung/fdb/detail/n/scannedtables/): Im Rahmen dieses Projektes soll untersucht werden, inwieweit sich existierende Lösungen zur Tabellenextraktion auf historische Zeitschriften anwenden lassen. Ziel ist es, eine Toolchain zu entwickeln, die es in reproduzierbarer Weise erlaubt, Tabellen mit Personendaten anhand der historischen Zeitschrift „Swinemünder Badeanzeiger“ zu extrahieren und aufzubereiten, um als Datenbasis für nachgestellte Analyse eingesetzt zu werden.
* [Revisionen von Genderkonstruktionen in Textüberlieferungen](https://www.hs-wismar.de/forschung/aus-der-forschung/fdb/detail/n/gendervarianten/): Das Projekt verbindet neutestamentliche Textkritik, Analysen zur Rezeption von Genderkonstruktionen und informationswissenschaftliche Forschung sowie systematisch-ethische Untersuchungen.
* [Datenkompass M-V: Kompetenznetzwerk Forschungsdaten Mecklenburg-Vorpommern](https://www.hs-wismar.de/forschung/aus-der-forschung/fdb/detail/n/dkmv/) verfolgt das Ziel, ein Datenkompetenznetzwerk in Mecklenburg-Vorpommern zu etablieren. Hierbei sollen die Stärken wissenschaftlicher Institutionen gebündelt werden, um Forschenden und forschungsnahen Wirtschaftsunternehmen im Bereich der Datenwissenschaften (Data Science) den Zugang zu erleichtern.
    

## Kontakt 
    
**Ansprechperson für Text+:** [Frank Krüger](mailto:frank.krueger@hs-wismar.de)

