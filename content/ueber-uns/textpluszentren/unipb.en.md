---
title: Paderborn University (UniPB)
short_name: UniPB
text_plus_domains:
- Editions
- Collections

text_plus_clusters:
  Editionen: 
  - Early Modern, Modern, and Contemporary Texts
  Collections: 
  - Historical Texts

external_url: "https://www.uni-paderborn.de/en/"

type: competence-center
---

# Paderborn University (UniPB)

**Text+ center:** [Zentrum Musik – Edition – Medien](https://www.uni-paderborn.de/en/zenmem)

**Type of center:** competence center

At the University of Paderborn, research data is highly valued as the basis of scientific knowledge. To this end, information, guidelines for handling research data, and practical advice on their implementation are provided.

The Center for Music - Edition - Media (ZenMEM) is an academic institution of the Faculty of Cultural Studies at Paderborn University. ZenMEM is a decidedly open association of academics and endeavors to jointly promote and establish new, digitally supported research opportunities in the field of cultural studies and to connect with international developments.In particular, the Edirom Summer School is offered for this purpose.

## Highlights of provided data and services

* [Edirom-Summer-School](https://ess.upb.de/): for Introductory classes into Digital Humanities
* [Measure Detector](https://measure-detector.edirom.de/): Measure Detector is the first fully AI-based approach to automatically determine measure positions in printed and handwritten music. The framework provides a valid MEI file with bounding boxes for the recognized measurements.
* [Edirom Online](https://github.com/Edirom/Edirom-Online): Edirom Online is a tool for presenting historical-critical music editions in digital form.
* [SMuFL Browser](https://github.com/Edirom/SMuFL-Browser): The SMuFL Browser makes it easy to incorporate musical symbols into TEI texts. The “TEI code for embedding” is used for this purpose when browsing the symbols, and a TEI description of the symbols can also be called up.


## Contact

**Contact for Text+:** [Peter Stadler](mailto:peter.stadler@uni-paderborn.de)
