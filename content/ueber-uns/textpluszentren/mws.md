---
title: Max Weber Stiftung – Deutsche geisteswissenschaftliche Institute im Ausland (MWS)
short_name: MWS
text_plus_domains:
- Editionen 
- Infrastruktur/Betrieb

text_plus_clusters:
  Editionen: 
  - Frühe moderne, moderne und zeitgenössische Texte 
  - Antike und mittelalterliche Texte 

external_url: "https://www.maxweberstiftung.de/startseite.html"

type: competence-center
---

# Max Weber Stiftung – Deutsche Geisteswissenschaftliche Institute im Ausland (MWS)
    
**Text+ Zentrum:** 
* [perspectivia.net](https://perspectivia.net/)
* [qed.perspectivia.net](https://qed.perspectivia.net/)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Die [Max Weber Stiftung (MWS)](https://ror.org/02esxtn62) zählt zu den maßgeblichen Trägern deutscher geistes- und sozialwissenschaftlicher Forschung im Ausland. Sie unterhält weltweit elf wissenschaftlich autonome Institute, die eine Brückenfunktion zwischen den Gastländern und Deutschland einnehmen und eine wichtige Rolle in der internationalen Wissenschaftslandschaft spielen.

In Text+ ist die MWS in den Task Areas Editions und Infrastructure/Operations aktiv. Dabei verantwortet die MWS das [Text+ Blog](https://textplus.hypotheses.org/), das auf dem Blogportal [de.hypotheses.org](https://de.hypotheses.org/), dem deutschsprachigen Zweig des internationalen Blogportals [hypotheses.org](https://hypotheses.org/), läuft. Im Helpdesk von Text+ berät die MWS zu den Themen Forschungsdatenmanagement (RDMO) und Editionen.

Zur internationalen Vernetzung von Text+ trägt die MWS bei durch ihre Institute sowie durch ihr Engagement in OPERAS und DARIAH.
    
## Highlights bereitgestellter Daten und Dienste
    
* Mit [perspectivia.net](https://perspectivia.net/) steht ein internationales, epochenübergreifendes und interdisziplinäres Portal für geisteswissenschaftliche Publikationen und Forschungsdaten bereit.
* Mit dem Portal für Quellen und Editionen ([qed.perspectivia.net](https://qed.perspectivia.net/)) bietet die MWS für Projekte, die TEI-XML-Daten produzieren, eine nachhaltige Lösung.
* Das Blogportal [de.hypotheses.org](https://textplus.hypotheses.org/) ist der deutschsprachige Zweig des internationalen Blogportals [hypotheses.org](https://de.hypotheses.org/). Es präsentiert wissenschaftliche Blogs aus den Geistes- und Sozialwissenschaften. Für die deutschsprachigen Blogs hat die MWS dauerhaft eine Stelle für Community Management (blogs@MaxWeberStiftung.de).
    
## Entgegennahme Daten Dritter 
Der wissenschaftliche Beirat von [perspectivia.net](https://perspectivia.net/ "https://perspectivia.net/") entscheidet über die Aufnahme von Daten aus der Text+ Community. Alle Angebote der MWS sind prinzipiell kostenfrei. Bei projektspezifischen Anpassungen können Kosten anfallen. Anfragen: fdm-perspectivia@maxweberstiftung.de

## Kontakt 
    
**Ansprechperson für Text+:** fdm-perspectivia@maxweberstiftung.de
