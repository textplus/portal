---
title: Text+ Centers

aliases:
- /research-data/data-and-competence-centers
- /en/vernetzung/daten-kompetenzzentren/
- /en/ueber-uns/daten-kompetenzzentren/

menu:
  main:
    weight: 20
    parent: ueber-uns

params:
  last_mod_today: true

type: non-scrolling-toc
---

# Text+ Centers

Text+ forms a distributed infrastructure that is supported by Text+ centers. The centers can be both data and competence centers or fulfill both functions.

## Data centers

In Text+, data centers function as partner institutions that focus (in some cases for specific domains) on the collection, storage, management and provision of research data. On the one hand, they provide their own data, but also accept data from third parties and form the cornerstones of the Text+ Data Space. They provide the technical infrastructure and the necessary services to ensure that the research data
* are available in standardized formats that enable long-term use and reuse,
* are appropriately documented and described to facilitate reuse by other researchers,
* are stored and archived securely in the sense of long-term archiving, and
* can be made accessible via suitable interfaces and platforms.

## Competence centers

Competence centers are specialized partner institutions that contribute their expertise to the development of Text+ services and offer support in specific areas of research data provision and use. Their main tasks include:
* Advice and training Competence centers for researchers in the use and management of research data. 
* Supporting the integration of data from different datasets and sources to create more comprehensive and connected research resources.
* Promoting data culture by nurturing and engaging networks of Text+ target audiences to foster a culture of data sharing and reuse in the scientific community.


The Text+ centers are assigned to the Collections, Lexical Resources, Editions and Infrastructure/Operations domains. The three data domains Collections, Lexical Resources and Editions are also organized in thematic clusters, which simplifies the collection of relevant research data: The clusters bundle all activities related to specific subtypes of data and research methods in a data domain according to the needs and research priorities of the respective community of interest.

{{<image img="Data-Domains-Thematic-Clusters_2023.png">}}
Data domains and their thematic clusters
{{</image>}}

Infrastructure/Operations coordinates the development of the distributed infrastructure and pursues the goal of an integrated and coordinated portfolio of services, data and services.

## Overview

{{< standout theme="white" col_width="9">}}
{{<competence-center-list>}}
{{< /standout >}}
