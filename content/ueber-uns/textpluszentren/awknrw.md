---
title: Nordrhein-Westfälische Akademie der Wissenschaften und der Künste (NRW Akademie)
short_name: NRW Akademie
text_plus_domains:
- Editionen
text_plus_clusters:
  Editionen: 
  - Antike und mittelalterliche Texte 
  - Frühe moderne, moderne und zeitgenössische Texte 

external_url: "https://www.awk.nrw/"

type: competence-center
---

# Nordrhein-Westfälische Akademie der Wissenschaften und der Künste (AWK NRW)
    
**Text+ Zentrum:** [Koordinierungsstelle Digital Humanities der Nordrhein-Westfälischen Akademie der Wissenschaften und der Künste (AWK NRW)](https://cceh.uni-koeln.de/akademie-nrw/)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Das Cologne Center for eHumanities ([CCeH](https://cceh.uni-koeln.de/)) an der Universität zu Köln ist die Koordinierungsstelle Digital Humanities der Nordrhein-Westfälischen Akademie der Wissenschaften und der Künste ([AWK NRW](https://www.awk.nrw/)), die Mitantragsstellerin von Text+ ist. 

In diesem Kontext fungiert das CCeH als Kompetenzzentrum der AWK NRW. Es leitet den [Arbeitsbereich Editionen](/ueber-uns/arbeitsbereiche/editionen/), einen Zusammenschluss von Text+ Partnern, der eine Infrastruktur aus Diensten für eine Community von Forschenden, Lehrenden und Lernenden entwickelt, in deren Forschungsfeld und -praxis wissenschaftliche Editionen eine wesentliche Rolle spielen. Eine Übersicht unserer Angebote findet sich [hier](/ueber-uns/arbeitsbereiche/editionen/).

{{<horizontal-line>}}    
## Highlights bereitgestellter Daten und Dienste

* Wir koordinieren maßgeblich die Angebote [Text+ Consulting](/daten-dienste/consulting/) und [Text+ Registry](https://registry.text-plus.org/).
* [Ägyptischer Alltag vernetzt im Web](https://www.awk.nrw/aegyptischer-alltag-vernetzt-im-web): Digitale Edition des „Kölner Papyri“, eine der bedeutendsten Papyrussammlungen Deutschlands.
* [Averroes-Edition: Ein Mehr-Generationen-Projekt](https://www.awk.nrw/averroes-edition-ein-mehr-generationen-projekt): Kritische Edition von 18 Werken des frühen Wegbereiters der europäischen Aufklärung Ibn Rušd alias Averroes.
* [Textdatenbank und Wörterbuch des klassischen Maya](https://www.mayawoerterbuch.de): Texttechnologische und epigraphische Aufarbeitung sämtlicher Maya-Hieroglyphentexte für den Aufbau eines umfassenden Wörterbuchs der klassischen Mayasprache.

{{<horizontal-line>}}
## Entgegennahme Daten Dritter

In Zusammenarbeit mit dem Kölner Data Center for the Humanities ([DCH](https://dch.phil-fak.uni-koeln.de/bestaende/datensicherung)) erfüllt das DCH die Funktion eines Datenzentrums für die AWK NRW mit Fokus auf einen Archivierungsworkflow für Editionen.

Für [Anfragen und Beratung](/daten-dienste/consulting/) wenden Sie sich gerne an den [Text+ Helpdesk](/helpdesk/) oder besuchen unser „[Research Rendezvous](https://events.gwdg.de/category/208/)“. Diese offene Sprechstunde findet alle zwei Wochen statt und erfordert keine Anmeldung.

{{<horizontal-line>}}
## Kontakt     
**Ansprechperson für Text+:** [Kilian Hensen](mailto:Kilian.Hensen@uni-koeln.de)