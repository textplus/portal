---
title: Academy of Sciences and Humanities in Hamburg (AdWHH)
short_name: AdWHH
text_plus_domains:
- Collections
text_plus_clusters:
  Collections: 
  - Contemporary Language 
  - Historical Texts 

external_url: "https://www.awhamburg.de/en/academy/portrait.html"

type: competence-center
---

# Academy of Sciences and Humanities in Hamburg (AdWHH)

**Text+ center:** [UHH research data respository](https://www.awhamburg.de/digitalisierung.html)

**Type of center:** data center/competence center

Since its founding in 2004, the AdWHH has been promoting interdisciplinary research into future social issues and fundamental scientific problems. In addition, the AdWHH is currently coordinating five long-term research projects, each with a strong focus on the digital indexing and analysis of unique and diverse language material. This includes the collection of sign language data and endangered minority languages.

As a central operating unit at the University of Hamburg, the Center for Sustainable Research Data Management provides a local technical infrastructure, including a data repository, for sustainable research data management in Text+.

## Highlights of provided data and services

* [Beta maṣāhǝft](https://www.awhamburg.de/en/research/long-term-projects/manuscripts-of-ethiopia-and-eritrea.html): A systematic study of the Christian manuscript tradition of Ethiopia and Eritrea
* [DGS-corpus](https://www.awhamburg.de/en/research/long-term-projects/dictionary-german-sign-language.html): Systematically records and documents German Sign Language (DGS) and creates an electronic dictionary based on the corpus data
* [Etymologika](https://www.awhamburg.de/en/research/long-term-projects/etymologika.html): Ordering and interpretation of knowledge in Greek-Byzantine lexicons up to the Renaissance
* [Indigenous North Eurasian Languages (INEL) Corpus](https://www.awhamburg.de/en/research/long-term-projects/indigenous-northern-eurasian-languages-inel.html): Provides language resources for indigenous languages and extensively annotated, glossed and mostly audio-aligned corpora of the languages Dolgan, Kamas and Selkup
* [Formulae - Litterae - Chartae](https://www.awhamburg.de/en/research/long-term-projects/formulae-litterae-chartae.html): Research and critical edition of early medieval Formulae, providing an exploration of Formulae writing in Western Europe before the development of ars dictaminis, based on letters and charters
* [Tamilex](https://www.tamilex.uni-hamburg.de/): Establishment of an electronic corpus of Classical Tamil literature and the corresponding historical lexicon informed by emic exegetical and lexicographical sources

## Third-party data reception 
In particular, the AdWHH takes data from third parties that focus on multilingual spoken corpora, transcription tools and sign language.

## Contact

**Contact for Text+:** [Timm Lehmberg](mailto:timm.lehmberg@awhamburg.de)

