---
title: Akademie der Wissenschaften und der Literatur | Mainz (AWLM)
short_name: AWLM
text_plus_domains:
- Editionen 
text_plus_clusters:
  Editionen: 
  - Antike und mittelalterliche Texte 
  - Frühe moderne, moderne und zeitgenössische Texte 

external_url: "https://www.adwmainz.de/"

type: competence-center
---

# Akademie der Wissenschaften und der Literatur | Mainz (AWLM)
    
**Text+ Zentrum:** [Digitale Akademie](https://www.adwmainz.de/digitalitaet/digitale-akademie.html)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Die Digitale Akademie (DA), die Forschungsabteilung Digital Humanities (DH) der AWLM, trägt mit zahlreichen digitalen Editionen, Textsammlungen und Softwareanwendungen zu Text+ bei. Thematisch reichen diese von großen mittelalterlichen und frühneuzeitlichen Textkorpora bis hin zu Editionen mit Schwerpunkt auf Quellen des 19. und 20. Jahrhunderts. Darüber hinaus hostet und kuratiert die AWLM übergreifende Forschungsinformationssysteme wie AGATE (ein europäisches Portal für die Akademien der Wissenschaften) oder das „Portal Kleine Fächer“ (zusammen mit der Johannes Gutenberg-Universität). 

Technisch gesehen wird die AWLM Text+ Lösungen für die Erstellung integrierter Editions- und Webportale, Annotationssoftware für graphbasierte digitale Editionen und LOD-Anwendungen zur Verfügung stellen, die eine semantische Anreicherung und Verknüpfung digitaler wissenschaftlicher Editionen ermöglichen.
    
## Highlights bereitgestellter Daten und Dienste
    
Software:
* [Cultural Heritage Framework v2](https://github.com/digicademy-chf): CMS-basierte Erweiterungssuite für die Verwaltung und Veröffentlichung digitaler Ressourcen aus dem Bereich des kulturellen Erbes
* [LOD.ACADEMY – A hub for Linked Open Data in Academies of Sciences and Beyond mit dem Service XTriples](https://lod.academy/site/): Crawlen von XML-Repositories und Extrahieren von RDF- Statements mit einer einfachen Konfiguration auf der Basis von XPATH/XQuery- Expressions

Digitale Editionen:
* [Hans Kelsen Werke (HKW)](https://agate.academy/id/PR270): Bis 2042 wird das Gesamtœuvre des österreichisch-amerikanischen Rechtswissenschaftlers Hans Kelsen (1881–1973) in einer historisch-kritischen Hybridedition zugänglich gemacht.
* [Buber-Korrespondenzen Digital](https://agate.academy/id/PR768): Ziel des Langzeitvorhabens (bis 2045) ist eine digitale Briefedition, deren Fokus auf der systematischen Rekonstruktion, der editorischen Erschließung zur Herstellung eines möglichst originalgetreuen Textverlaufs und der kulturgeschichtlichen Analyse der dialogischen Beziehungen wie der Gelehrten- und Intellektuellennetzwerke Martin Bubers liegen soll.
* u.v.m.
    
## Entgegennahme Daten Dritter 
Innerhalb von Text+ übernimmt die Akademie in Mainz insbesondere Daten, die zu Editionen gehören und zu ihrem Arbeitsschwerpunkt passen.

## Kontakt 
    
**Ansprechperson für Text+:** [Martin Sievers](mailto:martin.sievers@adwmainz.de)
