---
title: University of Freiburg (UniFR)
short_name: UniFR
text_plus_domains:
- Collections 

text_plus_clusters:
  Collections: 
  - Contemporary Language 

external_url: "https://uni-freiburg.de/en/"

type: competence-center
---

# University of Freiburg (UniFR)

**Text+ center:** [English Department](https://www.anglistik.uni-freiburg.de/sections/linguistics/lsmair)

**Type of center:** competence center

The Competence Centre at the University of Freiburg offers comprehensive support and advice in the area of English-language corpora, covering all aspects of the corpus linguistic work process, from data preparation and annotation to analysis and visualization of results. It has a long tradition of working with English-language corpora and has built internationally recognized expertise in this field. Mair and his team are leaders in corpus-based analysis of ongoing language change and regional differentiation in English. Mair has contributed to the expansion of widely used research infrastructures (such as the "Brown family" and the International Corpus of English (ICE)) and has created large corpora with data from social media, enabling the study of English in multilingual post-colonial migration contexts. These corpora are currently being prepared for integration into the Text+ infrastructure.

The Freiburg Centre was a driving force in the negotiations for a national license for the world's largest [collection of English-language web corpora](https://www.english-corpora.org/). In Freiburg, Mair is involved in establishing the Freiburg Digital Humanities Lab, where students can earn a DH certificate, and the diverse local and regional advisory activities will be sustainably consolidated and expanded.

The Freiburg Centre is a key player in the "Dissemination" working group of the Task Area "Collections." It serves as an interface between Text+ and the philological professional associations, particularly the digital officers of the respective boards, and presents the Text+ resources and tools at conferences.

## Highlights of provided data and services

* [Brown family](https://clarino.uib.no/korpuskel/home)
* [International Corpus of English (ICE)](https://www.ice-corpora.uzh.ch/en/joinice/Teams/icejam.html): The ICE provides data on different varieties of English. The Freiburg Competence Center was involved in setting up the corpus with Jamaican English.

## Third-party data reception 
Requests for data deposits are referred to suitable Text+ repositories.

## Contact

**Contact for Text+:** [Christian Mair](mailto:christian.mair@anglistik.uni-freiburg.de)

