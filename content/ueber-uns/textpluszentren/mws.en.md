---
title: Max Weber Foundation – German Humanities Institutes Abroad (MWS)
short_name: MWS
text_plus_domains:
- Editions 
- Infrastructure/Operations 

text_plus_clusters:
  Editionen: 
  - Early Modern, Modern, and Contemporary Texts 
  - Ancient and Medieval Texts 

external_url: "https://www.maxweberstiftung.de/en/home.html"

type: competence-center
---

# Max Weber Foundation – German Humanities Institutes Abroad (MWS)

**Text+ center:** 
* [perspectivia.net](https://perspectivia.net/content/index.xml?lang=en)
* [qed.perspectivia.net](https://qed.perspectivia.net)

**Type of center:** data center/competence center

The [Max Weber Foundation (MWS)](https://ror.org/02esxtn62) is one of the leading agencies supporting German research in the humanities and social sciences abroad. Around the world it finances eleven academically autonomous institutes, which provide a bridge function between the host nations and Germany and which play an important role in the international research landscape.

In Text+, the MWS is active in the task areas Editions and Infrastructure/Operations. Among other things, the MWS is responsible for the [Text+ Blog](https://textplus.hypotheses.org/), hosted on [de.hypotheses.org](https://de.hypotheses.org/), which is the German-language branch of the international blog portal [hypotheses.org](https://hypotheses.org/). For the Text+ Helpdesk, the MWS offers consulting on questions related to research date management (RDMO) and scholarly editions.

With its research institutes around the globe and its involvement in OPERAS and DARIAH, the MWS fosters the global interconnectedness of Text+.

## Highlights of provided data and services

* [perspectivia.net](https://perspectivia.net/content/index.xml?lang=en) is an international, epoch-spanning and interdisciplinary portal for publications and research data in the humanities.
* With the portal for sources and editions ([qed.perspectivia.net](https://qed.perspectivia.net/)), the MWS provides a sustainable solution for editorially orientated projects that produce TEI-XML data.
* [de.hypotheses.org](https://textplus.hypotheses.org/) is the German branch of [hypotheses.org](https://hypotheses.org/) which is an international platform for humanities and social science research blogs. For blogs in German, a community manager is employed at the MWS (blogs@MaxWeberStiftung.de).

## Third-party data reception 
The advisory board of [perspectivia.net](https://perspectivia.net/content/index.xml?lang=en) decides on the inclusion of data from the Text+ community. All services provided by the MWS are free of charge. If a project requires specific modifications, charges may occur. Contact: fdm-perspectivia@maxweberstiftung.de

## Contact

**Contact for Text+:** fdm-perspectivia@maxweberstiftung.de
