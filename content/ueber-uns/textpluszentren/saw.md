---
title: Sächsische Akademie der Wissenschaften zu Leipzig (SAW)
short_name: SAW
text_plus_domains:
- Lexikalische Ressourcen 
- Infrastruktur/Betrieb 

text_plus_clusters:
  Lexikalische Ressourcen: 
  - Deutsche Wörterbücher im europäischen Kontext 
  - Digitale lexikalische Ressourcen
  - Nicht-lateinische Schriftsysteme 

external_url: "https://www.saw-leipzig.de/de"

type: competence-center
---

# Sächsische Akademie der Wissenschaften zu Leipzig (SAW)
    
**Text+ Zentrum:** [Repositorium der Sächsischen Akademie der Wissenschaften zu Leipzig](https://repo.data.saw-leipzig.de)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Das Repositorium bietet die langfristige Sicherung digitaler Ressourcen und ihrer Metadaten. Auftrag des Repositoriums ist es, die langfristige Verfügbarkeit von Forschungsdaten zu gewährleisten, Forschungsergebnisse zu sichern, den Wissenstransfer in neue Fachbereiche zu erleichtern und neuartige Methoden und Ressourcen in den universitären Lehrplan zu integrieren. Ein besonderer inhaltlicher Fokus liegt auf lexikalischen Ressourcen, Textkorpora und allgemein auf Sprachressourcen für sogenannte "unterrepräsentierte" Sprachen.

Die SAW bringt ihre Expertise insbesondere im Bereich der Suche und Recherche in verteilten Umgebungen, der Metadateninfrastruktur und der semantischen Webtechnologie sowie der Qualitätssicherung von Diensten und Daten in die Text+-Infrastruktur ein.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Wortschatz Leipzig](https://wortschatz-leipzig.de/de): Das internationale Korporaportal bietet Zugriff auf mehr als 900 Korpora der Leipzig Corpora Collection (LCC) in über 250 Sprachen
* [Canonical Text Services (CTS)](http://cts.informatik.uni-leipzig.de/Canonical_Text_Service.html): Protokoll zur Identifizierung und zum Abruf von Textpassagen, die durch kanonische Verweise zitiert werden
* [Niedersorbisch-deutsche Wörterbücher](https://niedersorbisch.de/ndw/?rec=de): Digitalisierte Wörterbücher für Niedersorbisch-Deutsch, aufbereitet im Standardformat TEI Lex-0
    
## Entgegennahme Daten Dritter 
Die SAW übernimmt im Rahmen von Text+ insbesondere Daten im Bereich lexikalischer Ressourcen und Textkorpora.

## Kontakt 
    
**Ansprechperson für Text+:** clarin@saw-leipzig.de
