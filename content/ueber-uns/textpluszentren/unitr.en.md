---
title: Trier University (UniTR)
short_name: UniTR
text_plus_domains:
- Lexical Resources 
- Collections 

text_plus_clusters:
  Lexikalische Ressourcen: 
  - German Dictionaries in a European Context 
  Collections: 
  - Contemporary Language 

external_url: "https://www.uni-trier.de/en/"

type: competence-center
---

# Trier University (UniTR)

**Text+ center:** [Competence Centre - Trier Center for Digital Humanities](https://tcdh.uni-trier.de/en)

**Type of center:** data center/competence center

The Trier Center for Digital Humanities (TCDH), a central academic institution at Trier University, carries out projects in the fields of digital editions, digital lexicography, research software, data modeling and digital literary and cultural studies. A particular focus is on the modeling, indexing and making available of important historical dictionaries such as the German dictionary of Jacob Grimm and Wilhelm Grimm.

As a competence center in Text+, the TCDH makes its database available via the Trier Dictionary Network platform using open application interfaces. This includes TEI/XML-encoded data on large historical dictionaries as well as regional dictionaries of West Central German such as Palatine and Alsatian.

The TCDH is also contributing its expertise in the Collections domain, where it is researching work with derived text formats. In particular, the aim is to clarify the question of how well the derived text formats remain usable for different text and data mining tasks and to what extent they can be reconstructed (e.g., using large language models).

## Highlights of provided data and services

* [First edition and revision of the German dictionary by Jacob Grimm and Wilhelm Grimm](http://dwb.uni-trier.de/de/): the German-language dictionary with the longest processing time and by far the most comprehensive coverage of the German language from 1838
* [Middle High German dictionary by Matthias Lexer](https://woerterbuchnetz.de/?sigle=Lexer): one of the largest Middle High German dictionaries, which is essential for a precise reading and indexing of Middle High German texts and sources
* [Palatine Dictionary](https://woerterbuchnetz.de/?sigle=PfWB): a dictionary of dialects for the Palatinate region, covering the entire vocabulary of the basic Palatine dialects as well as local colloquial languages, local sociolects and name traditions

## Third-party data reception 
Within Text+, the University Trier particularly takes on data that fits in with its traditional data and its work focus, especially dictionaries in TEI-Lex0 Format.

## Contact

**Contact for Text+:** [Thomas Burch](mailto:burch@uni-trier.de) and [Christof Schöch](mailto:schoech@uni-trier.de)