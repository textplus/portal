---
title: Klassik Stiftung Weimar (KSW)
short_name: KSW
text_plus_domains:
- Editionen 

text_plus_clusters:
  Editionen: 
  - Frühe moderne, moderne und zeitgenössische Texte 

external_url: "https://www.klassik-stiftung.de/"

type: competence-center
---

# Klassik Stiftung Weimar (KSW)
    
**Text+ Zentrum:** [Klassik Stiftung Weimar](https://www.klassik-stiftung.de/startseite/digital/)

**Zentrumstyp:** Kompetenzzentrum

Die Klassik Stiftung Weimar bildet ein einzigartiges Ensemble von Kulturdenkmalen. Mit ihren zahlreichen Museen, Schlössern, historischen Häusern und Parks sowie den Sammlungen der Literatur und Kunst zählt sie zu den größten und bedeutendsten Kultureinrichtungen Deutschlands. 

Die Arbeitsschwerpunkte der Stiftung liegen auf der Epoche der Weimarer Klassik, ihren Nachwirkungen in der Kunst und Kultur des 19. Jahrhunderts sowie auf der Moderne mit Franz Liszt, Friedrich Nietzsche, Henry van de Velde und dem Bauhaus. Diese sollen in einem Brückenschlag zu den Künsten und Wissenschaften der Gegenwart Raum für Fragen aus dem Geist der Tradition öffnen.

Sukzessive werden Forschungsdaten und Digitalisate im Zuge von Erschließungs- und Editionsprojekten publiziert und für die Forschung nach den FAIR-Prinzipien zur Verfügung gestellt.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Forschungsdatenbank so:fie](https://ores.klassik-stiftung.de/ords/f?p=900:1): Der Name so:fie steht für: „Sammlungen online: forschen, informieren, entdecken“. Die Datenbank enthält derzeit detaillierte Angaben zu rund 35.000 Personen, 1.000 Körperschaften und 2.500 Geografika über mehr als drei Jahrhunderte hinweg – und verbindet erstmalig die Datenbanken des Goethe- und Schiller-Archivs und der Museen mit einer gemeinsamen Benutzeroberfläche.
* [Goethe und Schiller Archiv](https://www.klassik-stiftung.de/goethe-und-schiller-archiv/): Die Archivdatenbank ermöglicht eine Recherche über den Gesamtbestand des Goethe- und Schiller-Archivs. Darüber hinaus gibt es verschiedene Spezialdatenbanken.  Mit einem Gesamtbestand von rund 5 Millionen Blatt ist es ein zentrales Archiv vor allem für die deutschsprachige Literatur und Kultur des 18., 19. und frühen 20. Jahrhunderts.
* [Digitalisierung von Kulturgut](https://www.klassik-stiftung.de/digital/digitalisierung-von-kulturgut/): Die Klassik Stiftung Weimar bewahrt in ihren Sammlungen einen einzigartigen Fundus an musealen Objekten, Kunstgegenständen, Nachlässen, alten Drucken sowie Fotografien, Zeichnungen und grafischen Blättern auf, der zum Kern des nationalen Kulturerbes gehört. Ebenso sind die historischen Bauten selbst sowie die Parkanlagen Teil des Kulturguts, das es zu bewahren und zeitgemäß zu vermitteln gilt. Die Digitalisierung der Sammlungen und Bestände geschieht zum Zwecke der Erschließung und Erforschung der Objekte.
* [PROPYLÄEN – Forschungsplattforn zu Goethes Biographica](https://goethe-biographica.de/): Das Akademienprojekt ist ein zentraler Baustein in der digitalen Transformation der KSW. Ziele sind der Aufbau eines digitalen Portals zu Goethes Leben, Wirken und Werk auf der Basis von Goethes Biographica sowie die Fortführung und der Abschluss der im Goethe- und Schiller-Archiv laufenden historisch-kritischen Editionen der Briefe und Tagebücher von Goethe, der um die Volltextwiedergabe erweiterten Regestausgabe der Briefe an Goethe und der Edition von Goethes »Begegnungen und Gesprächen«. Sukzessive erweitern sich Inhalte und Recherchefunktionalitäten.
    

## Kontakt 
    
**Ansprechperson für Text+:** [Gerrit Brüning](mailto:gerrit.bruening@klassik-stiftung.de), [Dirk Wintergrün](mailto:dirk.wintergruen@klassik-stiftung.de)
