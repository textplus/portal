---
title: Philipps-University Marburg, Research Center Deutscher Sprachatlas (DSA)
short_name: DSA
text_plus_domains:
- Lexical resources 

text_plus_clusters:
  Lexikalische Ressourcen: 
  - German Dictionaries in a European Context 

external_url: "www.dsa.info"

type: competence-center
---

# Philipps-University Marburg, Research Center Deutscher Sprachatlas (DSA)

**Text+ center:** [Research Center Deutscher Sprachatlas](https://www.uni-marburg.de/en/fb09/dsa/institution)

**Type of center:** data center/competence center

The Research Center Deutscher Sprachatlas (DSA) at the Philipps-Universität Marburg is one of the leading centers for the documentation and research of regional diversity in German-speaking countries. It archives written, audio and visual data with regional references such as dialects, accents and everyday language on a long-term basis. There is no temporal or linguistic restriction for the data to be archived.

In addition to current language data, the central holdings include the most important historical documentation of the dialects and regional language forms of German, including language maps, sound recordings and questionnaires from language atlases, dictionaries and individual language surveys. One specialization is the long-term archiving and provision of data from closed projects.

As a data center, the DSA contributes its expertise in the field of lexical resources to the Text+ infrastructure. In addition to providing linguistic data, the center offers support in the form of consulting, regular training, and the development of innovative approaches to language data use and the analysis of language resources.

The DSA accepts data (e.g., maps, text, audio) related to research on the linguistic history and regional variation of German by agreement. Archiving of data from other languages can also be arranged upon request.

## Highlights of provided data and services

* [Erhebungsbögen des Deutschen Wortatlas](https://hdl.handle.net/20.500.14450/429): More than 52,000 dialect questionnaires regarding 200 German lexical items at the start of the 20th century
* [Lego-Korpus](https://hdl.handle.net/20.500.14450/286): Corpus of informal German conversations obtained during  construction tasks (audio files and corresponding transcripts)
* [Old High German and Old Low German *werden* periphrases](https://hdl.handle.net/20.500.14450/434): Dataset of all hits for an infinitive verb form in combination with auxiliary *werden* obtained from the Old German  reference corpus (ReA)
* [Phonotactics of German Dialects (PhonD2)](https://www.regionalsprache.de/phonD2/): Documentation of phonotactic structures in the dialects of German on a microtypological level for the analysis of segment combination
* [Sprachatlas der Rheinprovinz](https://hdl.handle.net/20.500.14450/295): 25 maps from Georg Wenker‘s language atlas mapping the dialects of the Rhine area in the late 1800s

## Third-party data reception 
Within Text+, the Research Center Deutscher Sprachatlas in Marburg particularly (but not exclusively) takes on data that fits within the areas of German dialectology and variationist linguistics as well as historical data sets with spatial reference.

## Contact

**Contact for Text+:** [Robert Engsterhold](mailto:engsterhold@uni-marburg.de)

