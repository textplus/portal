---
title: Heidelberg Academy of Sciences and Humanities (HAdW)
short_name: HAdW
text_plus_domains:
- Editions 
- Lexical Resources 
- Collections 

text_plus_clusters:
  Editionen: 
  - Ancient and Medieval Texts 
  - Early Modern, Modern, and Contemporary Texts 
  Lexikalische Ressourcen: 
  - German Dictionaries in a European Context
  - Non-Latin Scripts 
  Collections: 
  - Historical Texts 

external_url: "https://www.hadw-bw.de/"

type: competence-center
---

# Heidelberg Academy of Sciences and Humanities (HAdW)

**Text+ center:** [HAdW digital](https://www.hadw-bw.de/publikationen/hadw-digital)

**Type of center:** competence center

HAdW digital brings together all the data and publications of the Heidelberg Academy of Sciences and Humanities that are available online. The range of materials currently available extends from data collections such as dictionaries, lexicons and object catalogues to digital editions and commentaries to open access publications from the individual research centers, publication series, the yearbooks and the Academy's Athene magazine. The freely accessible database often reflects the current status of the projects and is regularly updated.

The HAdW attaches great importance to the digital visibility of its research, organization and events as well as the broadest possible digital accessibility and long-term digital archiving of research results. It thus follows an Open Access and Open Science strategy (internal link), in which publicly subsidized research results are also made available to the public free of charge wherever this is legally permitted.

The HAdW is actively involved in Text+ by contributing its subject-specific expertise to the design of the initiative and by making research data available and usable wherever this is legally permitted.

## Highlights of provided data and services

* [German Legal Dictionary](https://drw.hadw-bw.de/drw/info/): The DRW is a historical dictionary that contains not only legal terms in the narrower sense, but also terms that were included because of their legal references.
* [Melanchthon's correspondence - Regesten online](https://melanchthon.hadw-bw.de): This database is based on the nine-volume set of ‘Melanchthon's correspondence’: Addenda and corrections adopted after publication of a text volume. 
* [Goethe dictionary](https://www.hadw-bw.de/gwb): The GWb is an individual-language dictionary of meanings that reproduces Goethe's entire vocabulary in alphabetical order and word articles systematically organised according to usage.
* [Theologen-Briefwechsel](https://thbw.hadw-bw.de): The aim of the research project is to record, catalogue and partially edit the letters of all prominent theologians and church leaders of the Kurpfalz, Württemberg and Strasbourg in the years from 1550 to 1620. 
* [German Inscriptions Online](https://www.inschriften.net/suche.html): with AdW Mainz, NAdWG, BAdW, NRWAW, SAW
* [Malala's commentary](https://malalas.hadw-bw.de)
* [ALMA](https://www.hadw-bw.de/alma)
* [Religious and legal-historical sources of pre-modern Nepal](https://nepalica.hadw-bw.de/nepal/)
* [Buddhist stone sutras in China](https://www.stonesutras.org/)


## Contact

**Contact for Text+:** [Dieta Svoboda-Baas](mailto:Dieta.Svoboda-Baas@hadw-bw.de)

