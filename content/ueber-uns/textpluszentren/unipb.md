---
title: Universität Paderborn (UniPB)
short_name: UniPB
text_plus_domains:
- Editionen
- Collections

text_plus_clusters:
  Editionen: 
  - Frühe moderne, moderne und zeitgenössische Texte
  Collections:
  - Historische Texte

external_url: "https://www.uni-paderborn.de/"

type: competence-center
---

# Universität Paderborn (UniPB)
    
**Text+ Zentrum:** [Zentrum Musik – Edition – Medien](https://www.uni-paderborn.de/zenmem)

**Zentrumstyp:** Kompetenzzentrum

An der Universität Paderborn wird Forschungsdaten als Grundlage wissenschaftlicher Erkenntnis eine hohe Bedeutung beigemessen. Zu diesem Zweck werden Informationen, Leitlinien zum Umgang mit Forschungsdaten sowie praktische Hinweise zu deren Umsetzung angeboten.

Das Zentrum Musik – Edition – Medien (ZenMEM) ist eine wissenschaftliche Einrichtung der Fakultät für Kulturwissenschaften an der Universität Paderborn. ZenMEM ist ein dezidiert offener Verbund von Wissenschaftlern und Wissenschaftlerinnen und darum bemüht, gemeinsam neue, digital gestützte Forschungsmöglichkeiten im Bereich der Kulturwissenschaften zu fördern, zu etablieren und an internationale Entwicklungen anzuschließen. Dazu wird insbesondere die Edirom-Summer-School angeboten.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Edirom-Summer-School](https://ess.upb.de/): für Einführungen in die Digital Humanities
* [Measure Detector](https://measure-detector.edirom.de/): Der Measure Detector ist der erste vollständig KI-basierte Ansatz zur automatischen Bestimmung von Taktpositionen in gedruckten wie handschriftlichen Noten. Das Framework liefert eine gültige MEI-Datei mit Begrenzungsrahmen für die erkannten Messungen.
* [Edirom Online](https://github.com/Edirom/Edirom-Online): Edirom Online ist eine Webanwendung zur Darstellung von historisch kritischen Musikeditionen in digitaler Form.
* [SMuFL Browser](https://smufl-browser.edirom.de/index): Der SMuFL Browser ermöglicht das einfache Einbinden von musikalischen Symbolen in TEI-Texte. Dafür dient einerseits der "TEI code for embedding" beim Browsen der Zeichen, andererseits kann eine TEI-Beschreibung der Zeichen abgerufen werden.
    

## Kontakt 
    
**Ansprechperson für Text+:** [Peter Stadler](mailto:peter.stadler@uni-paderborn.de)
