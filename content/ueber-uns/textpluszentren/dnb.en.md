---
title: German National Library (DNB)
short_name: DNB
text_plus_domains:
- Collections 
- Infrastructure/Operations 

text_plus_clusters:
  Collections: 
  - Unstructured Text 

external_url: "https://www.dnb.de/EN/Home/home_node.html"

type: competence-center
---

# German National Library (DNB)

**Text+ center:** [German National Library](https://www.dnb.de/EN/Professionell/Services/WissenschaftundForschung/wissenschaftundforschung_node.html)

**Type of center:** data center/competence center

The German National Library is Germany's central archival library. It collects, documents and archives all publications and recordings published in Germany since 1913, as well as works that were created in German or have a connection to Germany. This collection ranges from contemporary German-language literature to all daily newspapers, scientific articles from German publishers and kiosk literature.

Access to most of the items in the DNB's holdings is restricted for copyright reasons. By making the digital collection of 21st century texts available as flexibly as possible, the DNB facilitates the realization of research projects in a wide range of disciplines and supports projects in corpus creation. The German National Library repository can be accessed indirectly via the library's catalog or through individual provision.

The DNB plays an active role in the further development of techniques for linking collections with other locally and thematically separated datasets from Text+ via Linked Open Data (LOD) and in particular via authority files such as the Gemeinsame Normdatei (GND) or via lexical resources. It is also developing the GND further with regard to the needs of the scientific communities. Together with the Leibniz Institute for the German Language (IDS), the DNB is a central point of contact for the multitude of legal issues arising from the use and publication of text-based data.

## Highlights of provided data and services

* [Free online university publications](https://portal.dnb.de/opac.htm?query=catalog%3Ddnb.hss+location%3Donlinefree+&method=simpleSearch&cqlMode=true): More than 325,000 online dissertations and postdoctoral theses from Germany with no access restrictions
* [World War I collection 1914–1918](https://portal.dnb.de/opac/simpleSearch?query=cod%3D2d009&cqlMode=true): More than 3,000 books, brochures, posters and other popular prints relating to World War I 1914–1918 (continuous addition)
* [Art, Architecture, Bauhaus 1918–1933](https://portal.dnb.de/opac.htm?query=cod%3Dd028+location%3Donlinefree+&method=simpleSearch&cqlMode=true): Digitized works on architecture, design, painting and printing dating from 1918 to 1933
* [Digital Exile Press – German-language Exile Journals 1933-1945](https://portal.dnb.de/opac.htm?method=showPreviousResultSite&currentResultId=%22exilpresse%22+and+%22digital%22%26any%26dnb.dea.exp%26leipzig&currentPosition=10): 100,000 pages from 30 titles that represent a wide spectrum of exile literature from between 1933 and 1945

## Third-party data reception 
As part of its statutory collection mandate (see [German National Library Act of 22 June 2006](https://www.gesetze-im-internet.de/dnbg/BJNR133800006.html)), the DNB takes on all relevant publications.

## Contact

**Contact for Text+:** nfdi@dnb.de
