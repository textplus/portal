---
title: German National Academy of Sciences Leopoldina (Leopoldina)
short_name: Leopoldina
text_plus_domains:
- Editions 

text_plus_clusters:
  Editionen: 
  - Early Modern, Modern, and Contemporary Texts 

external_url: "https://www.leopoldina.org/en/leopoldina-home/"

type: competence-center
---

# German National Academy of Sciences Leopoldina (Leopoldina)

**Text+ center:** [German National Academy of Sciences Leopoldina](https://www.leopoldina.org/ueber-uns/zentrum-fuer-wissenschaftsforschung/)

**Type of center:** competence center

The Leopoldina originated in 1652 as a classical scholarly society and now has 1,600 members from almost all branches of science. In 2008, the Leopoldina was appointed as the German National Academy of Sciences and, in this capacity, was invested with two major objectives: representing the German scientific community internationally, and providing policymakers and the public with science-based advice. The Leopoldina presents its policy recommendations in a scientifically qualified, independent, transparent and prospective manner, ever mindful of the standards and consequences of science.

The Leopoldina Center for Science Studies (ZfW) has been coordinating this process of reflection since 2012 and is responsible for projects in the history, theory and philosophy of science.  The Centre's work focuses on the one hand on historical research (specifically the history of science and knowledge) and on the other hand on science studies, the reflective academic study of institutions, forms and content of science itself. In addition to providing the necessary infrastructure, the centre itself initiates reflective research on science in general. In addition to third-party funded projects in the field of digital humanities, this also includes the planning and organisation of events that focus on the digital transformation of science. The ZfW stands for stronger networking in the digital humanities community with science studies and will be involved in events on reflective topics from the field of (digital) editing and the significance and handling of research data.

Through the ZfW, the Leopoldina is particularly involved in the establishment of a competence centre for digital editions within Text+ and is committed to the area of community services.

## Highlights of provided data and services

* [Academy project: „Ernst Haeckel (1834–1919): Briefedition“](https://www.leopoldina.org/ueber-uns/akademien-und-forschungsvorhaben/ernst-haeckel-1834-1919-briefedition/): complete digital edition of the correspondence of the naturalist Ernst Haeckel as well as a printed historical-critical edition of selected letters 
* [Leopoldina Project Goethe](https://goethe.leopoldina.org/): the online register for the in the history of science relevant historical-critical Leopoldina edition of Johann Wolfgang Goethe's scientific writings 
* [Project: ‘Objektsprache und Ästhetik –Wertdimensionen des Objektbezugs in historischer Perspektive. The example of conchylia’](https://www.leopoldina.org/ueber-uns/zentrum-fuer-wissenschaftsforschung/projekte/konchylien/): Graph database on conchylia collections based on a TEI-coded transcription of the illustrated manuscript (1759-1762) by Johann Hieronymus Kniphof


## Contact

**Contact for Text+:** [Ronja Steffensky](mailto:Ronja.Steffensky@leopoldina.org)

