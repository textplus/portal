---
title: Göttingen Academy of Sciences and Humanities in Lower Saxony (AdWGö)
short_name: AdWGö
text_plus_domains:
- Editions 
- Lexical Resources 

external_url: 'https://adw-goe.de/en/home/'

type: competence-center
---

# Göttingen Academy of Sciences and Humanities in Lower Saxony (AdWGö)

**Text+ center:** [Göttinger Digitale Akademie](https://digitale-akademie.adw-goe.de/)

**Type of center:** competence center

The Academy was quick to adapt to the digital revolution in the humanities and has done pioneering work in many areas. Since 1 January 2022, the state of Lower Saxony has been funding a project at the Göttingen Academy of Sciences and Humanities that promotes sustainable digitization in long-term humanities projects. 

The Göttingen Digital Academy project has the task of digitizing the research projects of the academy in such a way that they can be connected to the National Research Data Infrastructure (NFDI).

The aim is to provide permanent digital and freely accessible access to the research data, thematic online portals and platforms for research in the humanities developed as part of the academy projects in the academy program. These include, in particular, the epoch dictionaries, digital editions, historical databases and handbooks as well as projects in the field of biblical studies that are being conducted at the Göttingen Academy or in inter-academic projects.

## Highlights of provided data and services

* [res doctae](https://rep.adw-goe.de/) Repository/Document Server of the NAWG: In this DSpace repository, the Academy stores research data and publications from the projects of the academy program.
* [Web Archive](https://webarchiv.adw-goe.de): The web archive provides access to various versions of the online portals and can be searched across projects using [solrwayback](https://github.com/netarchivesuite/solrwayback).
* [Digital Library](https://adw-goe.de/publikationen/digitale-bibliothek/): The Digital Library of the Göttingen Academy provides an overview of the research data of the projects and the online platforms of the Academy.
* Research Data Hosting (https://digiberichte.de/home/; https://kuvis.adw-goe.de etc.): The GDA brings research data from completed academy projects and research projects back online, utilizing a modern, Docker-based infrastructure.
* [Further Training](https://digitale-akademie.adw-goe.de/projekte/fortbildungen/): The GDA organizes further training for employees of the academy projects, which are sometimes also open to external participants. Topics in recent years have included, among others, AI methods in the academy program, data archiving and data maintenance, copyright and related rights, Linked Open Data, and SW technologies, etc.


## Contact

**Contact for Text+:** [Dr. Jörg Wettlaufer](mailto:joerg.wettlaufer@adwgoe.de)

