---
title: Julius-Maximilians University Würzburg (UniWü)
short_name: UniWü
text_plus_domains:
- Editions 
- Collections 

text_plus_clusters:
  Editionen: 
  - Early Modern, Modern, and Contemporary Texts
  - Ancient and Medieval Texts 
  Collections: 
  - Contemporary Language
  - Historical Texts 

external_url: "https://www.uni-wuerzburg.de/en/"

type: competence-center
---

# Julius-Maximilians University Würzburg (UniWü)

**Text+ center:** [Centre for Philology and Digitality ‘Kallimachos’](https://www.uni-wuerzburg.de/zpd/)

**Type of center:** competence center

The chair of computerphilology at the Centre for Philology and Digitality ‘Kallimachos’ (ZPD) at the Julius-Maximilians University Würzburg is responsible for the analysis of cultural heritage data, as well as the construction of novel text corpora and datasets. The chair was involved in setting up Textgrid, Dariah and Clariah. Its focus lies in the development of new corpus-based methods for the analysis of literary texts. 
Within the context of Text+, the chair predominantly explores which requirements emerge from these approaches for research data infrastructures and libraries.

## Highlights of provided data and services

* [Corpus of early modern and realist German poetry](https://zenodo.org/records/6047514)
* [Faust Edition](https://faustedition.net/): Digital edition of manuscripts, important prints, and a constituted text of "Faust I" and "Faust II"
* [Corpus of historical German encyclopedias of the 19th century](https://zenodo.org/records/4554112)

## Contact

**Contact for Text+:** dh-sekretariat@uni-wuerzburg.de
