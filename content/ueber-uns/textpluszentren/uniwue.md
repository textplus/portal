---
title: Julius-Maximilians-Universität Würzburg (UniWü)
short_name: UniWü
text_plus_domains:
- Editionen 
- Collections 

text_plus_clusters:
  Editionen: 
  - Frühe moderne, moderne und zeitgenössische Texte 
  - Antike und mittelalterliche Texte 
  Collections: 
  - Gegenwartssprache
  - Historische Texte 

external_url: "https://www.uni-wuerzburg.de/"

type: competence-center
---

# Julius-Maximilians-Universität Würzburg (UniWü)
    
**Text+ Zentrum:** [Zentrum für Philologie und Digitalität "Kallimachos"](https://www.uni-wuerzburg.de/zpd/)

**Zentrumstyp:** Kompetenzzentrum

Die Rolle des Lehrstuhls für Computerphilologie im Zentrum für Philologie und Digitalität „Kallimachos“ (ZPD) an der Julius-Maximilians-Universität Würzburg ist die Analyse von kulturhistorischen Daten sowie der Aufbau von einschlägigen Korpora und Datensätzen. Der Lehrstuhl war am Aufbau von Textgrid, Dariah und Clariah beteiligt. Sein Schwerpunkt ist die Entwicklung neuer korpusbasierter Methoden zur Analyse von literarischen Texten. 

Im Kontext von Text+ erforscht er insbesondere, welche Anforderungen sich aus dieser Arbeitsweise für Forschungsdateninfrastrukturen und Bibliotheken ergeben.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Korpus von deutschspr. Gedichten des Realismus und der frühen Moderne](https://zenodo.org/records/6047514)
* [Faust Edition](https://faustedition.net/): Digitale Edition von Handschriften, wichtigen Drucken und einem konstituierten Text aus "Faust I" und "Faust II"
* [Korpus von historischen deutschen Enzyklopädien des 19ten Jahrhunderts](https://zenodo.org/records/4554112) 

## Kontakt 
    
**Ansprechperson für Text+:** dh-sekretariat@uni-wuerzburg.de





