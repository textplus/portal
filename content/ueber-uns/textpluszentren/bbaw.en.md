---
title: Berlin-Brandenburg Academy of Sciences and Humanities (BBAW)
short_name: BBAW
text_plus_domains:
- Collections
- Lexical Resources 
- Editions

text_plus_clusters:
  Collections: 
  - Historical Texts
  - Contemporary Language
  Lexikalische Ressourcen: 
  - German Dictionaries in a European Context
  - Non-Latin Scripts
  Editionen:
  - Ancient and Medieval Texts
  - Early Modern, Modern, and Contemporary Texts


external_url: "https://www.bbaw.de/en/"

type: competence-center
---

# Berlin-Brandenburg Academy of Sciences and Humanities (BBAW)

**Text+ center:** 
* [CLARIN-D service center of the Zentrum Sprache at the BBAW](https://clarin.bbaw.de/en) (BBAW-CLARIN-Repository)
* [TELOTA - The Electronic Life Of The Academy](https://www.bbaw.de/en/bbaw-digital/telota) (TELOTA)

**Type of center:**
* **BBAW-CLARIN-Repository**: data centre/competence centre (for the domains Collections und Lexical Resources)
* **TELOTA**: competence centre (for the domain Editions)

The Zentrum Sprache at the BBAW participates in the data domains "Collections" and "Lexical Resources". The Zentrum Sprache has many years of expertise in the field of dictionaries and historical corpora. This is reflected not least in the coordination of the data domain "Lexical Resources" in Text+.

The central resources of the Language Center at the BBAW are historical, German-language text corpora (of the DTA) and lexical resources (dictionaries of the DWDS).  
The Language Centre provides the technical prerequisites that ensure the long-term availability of linguistic and lexicographic services and resources. It also offers the possibility of publishing research data from external institutions. The Zentrum Sprache also provides researchers with consulting and support on issues related to research data management.

The work of the BBAW competence centre TELOTA in the field of digital humanities includes digital editions, i.e. editions of correspondence, research about markup, research data management, research software engineering, and much more. In addition to the conception and development of research software, TELOTA is involved in the development of standards in the field of text encoding.

TELOTA is responsible for the support and maintenance of digital resources and central services of the BBAW.
In addition to cooperations of university teaching, the competence centre offers workshops for developers and researchers and also organizes the Berlin DH Colloquium.

## Highlights of provided data and services

BBAW-CLARIN-Repository:

* [Digital Dictionary of the German Language (DWDS)](https://www.dwds.de/): The contemporary DWDS dictionary is at the centre of the DWDS word information system. It combines several contemporary language dictionaries. 
* [German Text Archive (DTA)](https://www.deutschestextarchiv.de/): The DTA provides cross-disciplinary and cross-genre collections and corpora of German-language texts on its platform. The DTA core corpus of around 1500 titles forms the basis for a reference corpus of Modern High German.

TELOTA: 

* [correspSearch](https://correspsearch.net/en/home.html): The webservice allows searching within the metadata of different digital and printed editions of correspondence for sender, recipient, place, date, and more.
* [ediarum](https://www.ediarum.org/index.html): ediarum is an open-source software project. The digital working environment can be used to create and publish digital editions. The structure is modularized and can be adapted to different requirements. 

## Third-party data reception 

The service center of the Zentrum Sprache at the BBAW accepts a wide range of data, including primarily historical German-language research data provided via the DTA, as well as dictionaries and encyclopedias that fall within the responsibility of the DWDS.

The competence centre TELOTA provides the digital editions from academy projects. Currently, data from third parties cannot be submitted.

### Contact

**Contact for Text+:** [BBAW-CLARIN-Repository](mailto:support@clarin-d.de) and [TELOTA](mailto:telota@bbaw.de)