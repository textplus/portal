---
title: Heidelberger Akademie der Wissenschaften (HAdW)
short_name: HAdW
text_plus_domains:
- Editionen 
- Lexikalische Ressourcen 
- Collections
text_plus_clusters:
  Editionen: 
  - Antike und mittelalterliche Texte 
  - Frühe moderne, moderne und zeitgenössische Texte 
  Lexikalische Ressourcen: 
  - Deutsche Wörterbücher im europäischen Kontext 
  - Nicht-lateinische Schriftsysteme 
  Collections: 
  - Historische Texte 

external_url: "https://www.hadw-bw.de/"

type: competence-center
---

# Heidelberger Akademie der Wissenschaften (HAdW)
    
**Text+ Zentrum:** [HAdW digital](https://www.hadw-bw.de/publikationen/hadw-digital)

**Zentrumstyp:** Kompetenzzentrum

HAdW digital bündelt alle online verfügbaren Daten und Veröffentlichungen der Heidelberger Akademie der Wissenschaften. Die Bandbreite der aktuell verfügbaren Materialien erstreckt sich dabei von Datensammlungen wie Wörterbüchern, Lexika und Objektkatalogen über digitale Editionen und Kommentare bis zu Open-Access-Publikationen der einzelnen Forschungsstellen, Schriftenreihen, den Jahrbüchern und dem Athene-Magazin der Akademie. Der frei zugängliche Datenbestand spiegelt häufig den aktuellen Arbeitsstand der Projekte wider und wird regelmäßig ergänzt.

Die HAdW misst der digitalen Sichtbarkeit ihrer Forschungen, Organisation und Veranstaltungen sowie der möglichst breiten digitalen Zugänglichkeit und langfristigen digitalen Archivierung von Forschungsergebnissen eine große Bedeutung zu. Sie folgt damit einer Open-Access- und Open-Science-Strategie, bei der die öffentlich subventionierten Forschungsergebnisse auch unentgeltlich der Öffentlichkeit zugänglich gemacht werden, wo immer dies rechtlich erlaubt ist.

Die HAdW beteiligt sich aktiv an Text+, indem sie ihre fachspezifische Expertise in die Gestaltung der Initiative einbringt sowie Forschungsdaten bereitstellt und nutzt.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Deutsches Rechtswörterbuch](https://drw.hadw-bw.de/drw/info/): Das DRW ein historisches Wörterbuch, das nicht nur Rechtswörter im engeren Sinne enthält, sondern auch Begriffe, die aufgrund ihrer rechtlichen Bezüge aufgenommen wurden.
* [Melanchthons Briefwechsel – Regesten online](https://melanchthon.hadw-bw.de): Dieser Datenbank liegt das neunbändige Regestenwerk von 'Melanchthons Briefwechsel' zugrunde: Nachträge und Korrekturen nach Erscheinen eines Textbandes übernommen. 
* [Goethe-Wörterbuch](https://www.hadw-bw.de/gwb): Das GWb ist ein individualsprachliches Bedeutungswörterbuch, das den gesamten Wortschatz Goethes in alphabetischer Anordnung und systematisch nach Gebrauchsweisen gegliederten Wortartikeln wiedergibt.
* [Theologen-Briefwechsel](https://thbw.hadw-bw.de): Ziel des Forschungsvorhabens ist die Erfassung, Erschließung und Teiledition der Briefe aller führenden Theologen und kirchenleitenden Persönlichkeiten der Kurpfalz, Württembergs und Straßburgs in den Jahren von 1550 bis 1620. 
* [Deutsche Inschriften Online](https://www.inschriften.net/suche.html): mit AdW Mainz, NAdWG, BAdW, NRWAW, SAW
* [Malalas Kommentar](https://malalas.hadw-bw.de)
* [ALMA](https://www.hadw-bw.de/alma)
* [Religions- und rechtsgeschichtliche Quellen des vormodernen Nepal](https://nepalica.hadw-bw.de/nepal/)
* [Buddhistische Steinschriften in China](https://www.stonesutras.org/)
    

## Kontakt 
    
**Ansprechperson für Text+:** [Dieta Svoboda-Baas](mailto:Dieta.Svoboda-Baas@hadw-bw.de)

