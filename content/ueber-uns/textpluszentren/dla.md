---
title: Deutsches Literaturarchiv Marbach (DLA)
short_name: DLA
text_plus_domains:
- Collections 

text_plus_clusters:
  Collections: 
  - Unstrukturierte Texte 

external_url: "https://www.dla-marbach.de/"

type: competence-center
---

# Deutsches Literaturarchiv Marbach (DLA)
    
**Text+ Zentrum:** [Kallías](https://www.dla-marbach.de/katalog/)

**Zentrumstyp:** Kompetenzzentrum

Das Deutsche Literaturarchiv Marbach (DLA) ist eine der bedeutendsten Literaturinstitutionen weltweit. In seinen Sammlungen sichert es eine Fülle kostbarster und bedeutsamster Quellen der Literatur- und Geistesgeschichte von 1750 bis zur Gegenwart. Es ist eine außeruniversitäre Forschungseinrichtung mit hohem Infrastruktur-Anteil und zugleich ein Institute of Advanced Study mit eigenem Forschungs- und Fellowprogramm. Das DLA erwirbt, erschließt, erforscht und vermittelt Literatur für die Öffentlichkeit.

In Text+ beteiligt sich das DLA vor allem im Bereich der Sammlungen neuerer Literatur und deren Archivierung. Der Datendienst [DLA Data+](https://www.dla-marbach.de/katalog/dla-dataplus/) bietet einen offenen Zugang zu den Daten des DLA Marbach auf Basis forschungsbezogener Fragestellungen.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Marbacher Schiller-Bestand](https://www.dla-marbach.de/archiv/friedrich-schiller/): Zum Bestand gehören ferner biografische und wirkungsgeschichtliche Dokumente, u. a. eigene Aufzeichnungen aus der Karlsschulzeit, aber auch fremde Zeugnisse, Familienpapiere oder ganze Nachlässe aus der Verwandtschaft und Imitationen von Schiller-Autographen (die sogenannten Gerstenbergk'schen Fälschungen).
* [Marbacher Autorenbibliotheken](https://www.dla-marbach.de/bibliothek/spezialsammlungen/autorenbibliotheken/): In den vergangenen Jahren wurden zentrale Autorenbibliotheken nach aktuellen Kriterien der Provenienzforschung erschlossen. Die umfangreiche und wachsende Sammlung derartiger Bibliotheken eröffnet die Möglichkeit, poetische und wissenschaftliche Arbeitspraktiken des 20. und 21. Jahrhunderts zu erforschen. Zu den herausragenden und meistbenutzten Autoren- und Gelehrtenbibliotheken im Deutschen Literaturarchiv Marbach gehören die Büchersammlungen von Gottfried Benn, Hans Blumenberg, Paul Celan, Ernst Jünger, Reinhart Koselleck, Siegfried Kracauer, Martin Heidegger, Hermann Hesse und W. G. Sebald.
* Seit seiner Gründung 1955 gehört das Deutsche Literaturarchiv Marbach zu den wichtigsten Sammelstätten für Exilliteratur. Der Anspruch, Zeugnisse verbrannter und verfemter deutschsprachiger Literatur zu sammeln, leitete schon die Gründer des DLA und prägt die Erwerbungs-, Ausstellungs- und Veranstaltungspolitik des Hauses bis heute. Zu den Vor- und Nachlässen sowie Teilnachlässen von Exilschriftstellerinnen und Exilschriftstellern zählen die von Hannah Arendt, Alfred Döblin, Hilde Domin, Yvan Goll, Mascha Kaléko, Siegfried Kracauer, Else Lasker-Schüler, Karl Löwith, Heinrich Mann, Joseph Roth, Nelly Sachs, Carl und Thea Sternheim, Kurt Tucholsky, Kurt Wolff, Karl Wolfskehl, Carl Zuckmayer und Stefan Zweig. Zusammen mit zahlreichen weiteren Exilbeständen bilden sie das Helen und Kurt Wolff-Archiv.
    

## Kontakt 
    
**Ansprechperson für Text+:** [Karin Schmidgall](mailto:karin.schmidgall@dla-marbach.de) 


