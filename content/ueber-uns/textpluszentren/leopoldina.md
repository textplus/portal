---
title: Nationale Akademie der Wissenschaften Leopoldina (Leopoldina)
short_name: Leopoldina
text_plus_domains:
- Editionen 

text_plus_clusters:
  Editionen: 
  - Frühe moderne, moderne und zeitgenössische Texte

external_url: "https://www.leopoldina.org/leopoldina-home/"

type: competence-center
---

# Nationale Akademie der Wissenschaften Leopoldina (Leopoldina)
    
**Text+ Zentrum:** [Nationale Akademie der Wissenschaften Leopoldina](https://www.leopoldina.org/ueber-uns/zentrum-fuer-wissenschaftsforschung/)

**Zentrumstyp:** Kompetenzzentrum

Die 1652 gegründete Deutsche Akademie der Naturforscher Leopoldina ist mit ihren rund 1.600 Mitgliedern aus nahezu allen Wissenschaftsbereichen eine klassische Gelehrtengemeinschaft. Sie wurde 2008 zur Nationalen Akademie der Wissenschaften Deutschlands ernannt. In dieser Funktion hat sie zwei besondere Aufgaben: die Vertretung der deutschen Wissenschaft im Ausland sowie die Beratung von Politik und Öffentlichkeit. Sie unterstützt diesen Prozess mit einer kontinuierlichen Reflexion über Voraussetzungen, Normen und Folgen wissenschaftlichen Handelns.

Das Leopoldina-Zentrum für Wissenschaftsforschung (ZfW) koordiniert seit 2012 diesen Reflexionsprozess und verantwortet wissenschaftshistorische, wissenschaftstheoretische und wissenschaftsphilosophische Projekte. Schwerpunkte der Arbeiten des Zentrums liegen zum einen im Bereich historischer (spezifisch wissenschafts- und wissenshistorischer) Forschung, zum anderen im Bereich der *science studies*, der reflektierten wissenschaftlichen Beschäftigung mit Institutionen, Formen und Inhalten der Wissenschaft an sich. Neben der Bereitstellung der dafür notwendigen Infrastruktur, initiiert das Zentrum selbst reflektierende Forschung zu Wissenschaft im Allgemeinen. Hierzu gehören neben Drittmittelprojekten aus dem Bereich der Digital Humanities auch die Planung und Durchführung von Veranstaltungen, die die digitale Transformation der Wissenschaft zum Gegenstand haben. Das ZfW steht für eine stärkere Vernetzung in die Digital Humanities-Community mit der Wissenschaftsforschung und wird sich durch Veranstaltungen zu reflektierenden Themen aus dem Bereich der (digitalen) Editorik sowie der Bedeutung und dem Umgang mit Forschungsdaten engagieren.

Über das ZfW bringt sich die Leopoldina innerhalb von Text+ besonders in den Aufbau eines Kompetenzzentrums für Digitale Editionen ein und engagiert sich im Bereich der Community Services.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Akademienprojekt: „Ernst Haeckel (1834–1919): Briefedition“](https://www.leopoldina.org/ueber-uns/akademien-und-forschungsvorhaben/ernst-haeckel-1834-1919-briefedition/): vollständige digitale Edition der Korrespondenz des Naturforschers Ernst Haeckel sowie gedruckte historisch-kritische Ausgabe ausgewählter Briefe 
* [Leopoldina-Projekt Goethe](https://goethe.leopoldina.org/): das Online-Register zur in der Wissenschaftsgeschichte einschlägigen historisch-kritischen Leopoldina-Ausgabe der naturwissenschaftlichen Schriften Johann Wolfgang Goethes
* [Projekt: „Objektsprache und Ästhetik –Wertdimensionen des Objektbezugs in historischer Perspektive. Das Beispiel Konchylien“](https://www.leopoldina.org/ueber-uns/zentrum-fuer-wissenschaftsforschung/projekte/konchylien/): Graph-Datenbank zu Konchylien-Sammlungen basierend auf einer TEI-kodierten Transkription des illustrierten Manuskriptes (1759-1762) von Johann Hieronymus Kniphof

## Kontakt 
    
**Ansprechperson für Text+:** [Ronja Steffensky](mailto:Ronja.Steffensky@leopoldina.org)

