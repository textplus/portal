---
title: Eberhard Karls Universität Tübingen (UniTü)
short_name: UniTü
text_plus_domains:
- Collections 
- Lexikalische Ressourcen

text_plus_clusters:
  Collections: 
  - Gegenwartssprache 
  - Historische Texte
  Lexikalische Ressourcen:
  - Digitale lexikalische Ressourcen

external_url: "https://uni-tuebingen.de/"

type: competence-center
---

# Eberhard Karls Universität Tübingen (UniTü)
    
**Text+ Zentrum:** [Tübingen Archive of Language Resources](https://uni-tuebingen.de/de/261174)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Das Seminar für Sprachwissenschaften der Eberhard Karls Universität Tübingen (UniTü) beschäftigt sich mit den sprachtheoretischen Grundlagen der Computerlinguistik und Kognitionswissenschaft, mit den texttechnologischen Grundlagen der Korpuslinguistik sowie mit Anwendungsgebieten der maschinellen Sprachverarbeitung. Besondere Schwerpunkte liegen auf den folgenden Forschungsfeldern Grammatikformalismen, Diskurssemantik, Entwicklung von Sprachressourcen für das Deutsche auf den Gebieten der Morphologie, der Syntax und der Semantik, Information Retrieval, Dialektometrie und maschinelle Lernverfahren für natürliche Sprachen.

Für Text+ stellt die UniTü ihre Datenressourcen in dem Tübingen Archive for Language Resources (TALAR) zur Verfügung. Diese tragen zur Erweiterung des Text+-Portfolios in den zwei Domänen Collections und lexikalische Ressourcen bei. Insbesondere werden Korpora für gesprochene Sprache und geschriebene Texte, die auf morphologischer, syntaktischer und semantischer Ebene annotiert werden, archiviert. Dies umfasst beispielsweise die TüBa-Baumbanken für Deutsch, Englisch und Japanisch. Zusätzlich zu den sprachlich annotierten Korpora bietet die UniTü Datendienste in Form von Vektorraum-Wortdarstellungen und zugehörigen Softwaretools an.
    
## Highlights bereitgestellter Daten und Dienste
    
Collections:
* [WebLicht](https://weblicht.sfs.uni-tuebingen.de/weblichtwiki/index.php/Main_Page): Umgebung für die automatische Annotation von Textkorpora. Linguistische Werkzeuge wie Tokenizer und Part of Speech Tagger sind als Webservices gekapselt, die vom Benutzer zu individuellen Verarbeitungsketten kombiniert werden können
* Tübinger Baumbank-Collection (Tündra): Basierend auf der Sammlung Tübinger Baumbanken (einschließlich der [TüBa-D/Z](https://uni-tuebingen.de/de/134290)) sowie Baumbanken der Universal Dependencies bietet Tündra eine Suchplattform in syntaktisch annotierten Korpora

Lexikalische Ressourcen:
* [GermaNet](https://uni-tuebingen.de/fakultaeten/philosophische-fakultaet/fachbereiche/neuphilologie/seminar-fuer-sprachwissenschaft/arbeitsbereiche/allg-sprachwissenschaft-computerlinguistik/ressourcen/lexica/germanet-1/): Lexikalisch-semantisches Wortnetz, das deutsche Nomina, Verben und Adjektive semantisch zueinander in Beziehung setzt
* [GermaNet-Rover](https://weblicht.sfs.uni-tuebingen.de/rover/): Suchwerzeug zur Suche nach Daten im GermaNet
    
## Entgegennahme Daten Dritter 

Die UniTü nimmt ein breit gefächertes Spektrum an Daten von Dritten an, die ihr Profil im Bereich Collections und lexikalische Ressourcen erweitern. Ein Schwerpunkt liegt dabei auf syntaktisch annotierten Korpora (Baumbanken) und lexikalische Daten im Format des GermaNet. Andere Datentypen entsprechend den Bestandsdaten, z. B. Wordembeddings und Experimentaldaten, auf Anfrage.

## Kontakt 
    
**Ansprechperson für Text+:** data-steward@semsprach.uni-tuebingen.de
