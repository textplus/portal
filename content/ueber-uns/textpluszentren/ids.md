---
title: Leibniz-Institut für Deutsche Sprache (IDS)
short_name: IDS
text_plus_domains:
- Collections 
- Lexikalische Ressourcen

text_plus_clusters:
  Collections: 
  - Gegenwartssprache 
  - Historische Texte
  Lexikalische Ressourcen: 
  - Deutsche Wörterbücher im europäischen Kontext
  - Nicht-lateinische Schriftsysteme


external_url: "https://www.ids-mannheim.de/"

type: competence-center
---

# Leibniz-Institut für Deutsche Sprache (IDS)
    
**Text+ Zentrum:** [IDS Text- und Sprachdaten-Repositorium](https://repos.ids-mannheim.de/)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Das IDS besitzt die weltweit größte linguistisch motivierte Sammlung elektronischer Korpora mit geschriebenen deutschsprachigen Texten aus der Gegenwart und der neueren Vergangenheit. Zudem verfügt es über Korpora mit gesprächs- und varietätenlinguistischen Aufnahmen des Deutschen. Angeboten werden Werkzeuge und Schnittstellen zur Abfrage und Analyse der Korpora. 

Das IDS Repository zielt auf die langfristige Archivierung von linguistischen Ressourcen (Text und gesprochene Sprache) im Bereich der Germanistik ab. Es ist auch einer der zentralen Knotenpunkte in den Text+ Clustern mit zwei Spezialgebieten, Collections und Lexikalischen Ressourcen. Unter anderem beherbergt das IDS die wichtigsten Sammlungen des Gegenwartsdeutschen und Wörterbücher mit thematischen Schwerpunkten auf Neologismen, Diskurswortschatz, Fremdwörter und Lehnwörter. In Text+ werden die Daten zusammen mit Metadaten in den Formaten Dublin Core und CMDI bereitgestellt. Die Metadaten werden von der Text+ Registry über eine Suchoberfläche durchsuchbar gemacht.
    
## Highlights bereitgestellter Daten und Dienste
    
Collections: 
* [Deutsche Referenzkorpus (DeReKo)](https://www.ids-mannheim.de/digspra/kl/projekte/korpora): die weltweit größte linguistisch motivierte Sammlung elektronischer Korpora mit geschriebenen deutschsprachigen Texten aus der Gegenwart und der neueren Vergangenheit
* [Archiv für Gesprochenes Deutsch (AGD)](https://agd.ids-mannheim.de/index_en.shtml) beherbergt mehr als 80 verschiedene Korpora, darunter die wichtigsten Ressourcen zur Dokumentation des gesprochenen Deutsch in der Interaktion und der regionalen Variation des gesprochenen Deutsch 

Lexikalische Ressourcen:
* [OWID und OWIDplus](https://www.owid.de/): Online-Plattform für verschiedene erarbeitete Wörterbücher und experimentelle, datenzentrierte, multilinguale Ressourcen
* [grammis](https://grammis.ids-mannheim.de/): wissenschaftliches Informationssystem zur deutschen Grammatik, das aktuelle Forschungsergebnisse, Erklärungen und Hintergrundwissen präsentiert
    
## Entgegennahme Daten Dritter 
Das IDS nimmt primär Daten Dritter an, die zu den Bestandsdaten passen. Das umfasst in erster Linie große textuelle Ressourcen, die analog zum DeReKo-Korpus im I5 Format vorliegen, aber auch gesprochen-sprachliche Daten, die gemäß ISO 24624 („Transcription of spoken language“) vorliegen, Weitere Datenformate auf Anfrage.

## Kontakt 
    
**Ansprechperson für Text+:** data-steward@ids-mannheim.de
