---
title: Universität des Saarlandes (SLUni)
short_name: SLUni
text_plus_domains:
- Collections 

text_plus_clusters:
  Collections: 
  - Gegenwartssprache 
  - Historische Texte 

external_url: "https://www.uni-saarland.de/start.html"

type: competence-center
---

# Universität des Saarlandes (SLUni)
    
**Text+ Zentrum:** [CLARIND-UdS: Repositorium für Sprachressourcen an der Universität des Saarlandes](https://fedora.clarin-d.uni-saarland.de/)

**Zentrumstyp:** Daten-/Kompetenzzentrum

An der Fachrichtung Sprachwissenschaft und Sprachtechnologie der Universität des Saarlandes (UdS) wird in verschiedenen sprachwissenschaftlichen Disziplinen geforscht und gelehrt, darunter die Computerlinguistik, Psycholinguistik, Phonetik, Sprachtechnologie, Korpuslinguistik und Übersetzungswissenschaft.

Im Rahmen von Text+ ist die Universität des Saarlandes Teil der Datendomäne Collections. Das Datenzentrum, CLARIND-UdS, hat sich auf Registerkorpora, multilinguale Korpora und Übersetzungskorpora spezialisiert. Es wurden bereits mehr als 100 Datenressourcen im Repositorium für Sprachressourcen archiviert. Zum Bestand gehören beispielsweise diachrone Korpora wie das Royal Society Corpus (RSC) und das Old Bailey Corpus (OBC).
    
## Highlights bereitgestellter Daten und Dienste
    
* [Royal Society Corpus (RSC)](https://fedora.clarin-d.uni-saarland.de/rsc_v6/index.html) beinhaltet annotierte wissenschaftliche Artikel aus den Jahren 1665 bis 1920, die in den Philosophical Transactions und Proceedings der Royal Society of London veröffentlicht wurden
* [Old Bailey Corpus (OBC)](https://fedora.clarin-d.uni-saarland.de/oldbailey/) dokumentiert gesprochenes Englisch aus zwei Jahrhunderten (1720 bis 1913) basierend auf Verhandlungsprotokollen des zentralen Strafgerichtshofs in London 
* [EuroParl-UdS Corpus](https://fedora.clarin-d.uni-saarland.de/europarl-uds/) basiert auf Parlamentsdebatten des Europäischen Parlaments von 1999 bis 2017; englische, deutsche und spanische Texte wurden angereichert mit Metadaten zu Texten und Sprechenden
* [EPIC-UdS Corpus](https://fedora.clarin-d.uni-saarland.de/epic-uds/index.html): dreisprachiges paralleles und vergleichbares Dolmetschkorpus mit Reden, die im Europäischen Parlament zwischen 2008 und 2013 gehalten wurden
    
## Entgegennahme Daten Dritter 
CLARIND-UdS nimmt Daten an, die thematisch zum bestehenden Inventar passen. Das sind in erster Linie nicht-deutsche und multilinguale Korpora. Beispielsweise wurde mit Manas-UdS ein annotiertes Korpus kirgisischer Texte aufgenommen. Weiterhin wurde GermaParl über die Korpusplattform der UdS verfügbar gemacht.

## Kontakt 
    
**Ansprechperson für Text+:** [Elke Teich](mailto:e.teich@mx.uni-saarland.de) (Admin.) und [Jörg Knappen](mailto:j.knappen@mx.uni-saarland.de) (Techn.)

