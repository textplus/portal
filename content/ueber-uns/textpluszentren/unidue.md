---
title: Universität Duisburg-Essen (UniDUE)
short_name: UniDUE
text_plus_domains:
- Collections 

text_plus_clusters:
  Collections: 
  - Gegenwartssprache 

external_url: "https://www.uni-due.de/"

type: competence-center
---

# Universität Duisburg-Essen (UniDUE)
    
**Text+ Zentrum:** [PolMine](https://polmine.github.io/)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Die Datenressourcen des PolMine Project als Beitrag der UniDUE zu Text+ umfassen Sammlungen gesprochener Sprache, wie sie in Manuskripten und Protokollen des politischen Diskurses enthalten sind. Da die in Duisburg erstellten Sprachressourcen linguistisch annotiert sind und sich an die Richtlinien der Text Encoding Initiative (TEI) halten, sind sie auch für die sprachwissenschaftliche und zeitgeschichtliche Forschung sehr relevant. Verfahren für die reproduzierbare Aufbereitung der Daten und eine Analyseumgebung sind Teil des Angebots des PolMine Project. Derzeit werden die Daten über verschiedene Langzeitrepositorien sowie über die Webumgebung des Projekts verbreitet.
    
## Highlights bereitgestellter Daten und Dienste
    
* [GermaParl - Corpus of Plenary Protocols](https://zenodo.org/records/10421773): digitale Sammlung aller Parlamentsdebatten im Deutschen Bundestag von 1949-2024. Im Zuge von Qualitätsevaluierungen und Erweiterung der Abdeckung erfolgen halbjährliche Updates in einem zweistufigen Verfahren: Nutzung zunächst für registrierte Nutzende, dann Veröffentlichung ohne Restriktionen. Datenformate: TEI-XML und CWB.
* Der reproduzierbare Workflow und die eigens entwickelte Toolchain inkl. aktueller Annotationstools stehen zur Nachnutzung bereit.
* [R-Paket *polmineR*](<https://github.com/PolMine/polmineR>) stellt für Korpora im CWB-Daten-format eine Analyseumgebung bereit, welche die interaktive Kombination qualitativer und quantitativer Analyseschritte ermöglicht. 
* Demnächst: Release des GermaParl-Korpus nach dem ParlaMint-Standard und damit feste Verankerung in der europäischen Familie der parlamentarischen Sprachdaten.
    
## Entgegennahme Daten Dritter 
Als Kompetenzzentrum für parlamentarische Sprachdaten stellt das PolMine-Projekt Expertise und Werkzeuge zur Erstellung von Korpora mit Manuskripten und Protokollen des politischen Diskurses bereit und leistet für Unterstützung bei entsprechenden Vorhaben.

## Kontakt 
    
**Ansprechperson für Text+:** [Andreas Blätte](mailto:andreas.blaette@uni-due.de)
