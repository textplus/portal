---
title: Göttingen State and University Library (SUB)
short_name: SUB
text_plus_domains:
- Collections 
- Editions 
- Infrastructure/Operations 

text_plus_clusters:
  Collections: 
  - Unstructured Text 
  Editionen: 
  - Early Modern, Modern, and Contemporary Texts 
  - Ancient and Medieval Texts 

external_url: "https://www.sub.uni-goettingen.de/en/news/"

type: competence-center
---

# Göttingen State and University Library (SUB)

**Text+ center:** 
* [TextGrid Repository](https://textgridrep.org)
* [DARIAH-DE Repository](https://repository.de.dariah.eu/search/?lang=en)
* [GRO.data](https://data.goettingen-research-online.de/)

**Type of center:** data center/competence center

With its current holdings of around 9 million media units, the SUB is one of the largest libraries in Germany. It provides comprehensive services for the archiving and provision of research data in the humanities and cultural sciences. Together with the eResearch Alliance, the GWDG, the computing centre of the Max Planck Society and the Georg August University Göttingen, the SUB Göttingen acts as a data centre, especially for text-based research data. In addition, comprehensive RDM consulting and a data depositing service for generic research data complete the portfolio.

In Text+, the SUB is part of the data domains Editions and Collections. In addition, the SUB is the leading partner in the infrastructure/operations domain, which is responsible for providing a FAIR-compliant platform for generic services and linking existing and developing data and service portfolios of the data domains.

In addition, SUB offers Text+ access to three data repositories: GRO.data, DH-rep and TG-rep. The Göttingen Research Online Data Repository (GRO.data) is a discipline-independent data repository for the publication of research data at the Göttingen campus. The DARIAH-DE Repository (DH-rep) is a digital long-term archive for research data in the humanities and cultural sciences. And the TextGrid Repository (TG-rep) is a digital archive for the preservation of research data from the humanities working with textual research data. It comprises the TextGrid Digital Library, the works of 700 authors of fiction (prose, poetry and drama) and non-fiction from the beginning of printing to the early 20th century in German language or translation.

## Highlights of provided data and services

* [Research Data Management Organizer (RDMO)](https://rdmorganiser.github.io/): a tool to create and manage dynamic data management plans to plan and document the systematic organisation of research data
* [European Literacy Text Collection (ELTeC)](https://textgridrep.de/project/TGPR-99d098e9-b60f-98fd-cda3-6448e07e619d): a collection of corpora in over 15 European languages
* [Modes of Narration and Attribution Pipeline (MONAPipe)](https://gitlab.gwdg.de/textplus/collections/mona-pipe): provision of natural language processing tools for German, implemented in Python/spaCy

## Third-party data reception 
If research data are to be accepted as self-deposit, TG-rep, DH-rep, and GRO.data can be freely used. Support for ingestion can be requested from the Helpdesk.

## Contact

**Contact for Text+:** [TextGrid Repository](mailto:anfragen@textgrid.de), [DARIAH-DE Repository](mailto:info@de.dariah.eu) and [GRO.data](mailto:dataverse@gwdg.de)

