---
title: Herzog August Bibliothek Wolfenbüttel (HAB)
short_name: HAB
text_plus_domains:
- Editionen 

text_plus_clusters:
  Editionen: 
  - Antike und mittelalterliche Texte 
  - Frühe moderne, moderne und zeitgenössische Texte 

external_url: "https://www.hab.de/"

type: competence-center
---

# Herzog August Bibliothek Wolfenbüttel (HAB)
    
**Text+ Zentrum:** [HAB Repositorium (Herzog August Bibliothek Wolfenbüttel)](https://repo.hab.de/home)

**Zentrumstyp:** Kompetenzzentrum

Die HAB ist eine außeruniversitäre Studien- und Forschungseinrichtung für die europäische Kulturgeschichte des Mittelalters und der frühen Neuzeit. Sie ist ein von der Deutschen Forschungsgemeinschaft (DFG) gefördertes Zentrum für Handschriftenkatalogisierung und Teil des Projekts für eine retrospektive Deutsche Nationalbibliothek (AG Sammlung Deutscher Drucke). Ebenso partizipiert sie am Forschungsverbund Marbach Weimar Wolfenbüttel. Mit der Wolfenbütteler Digitalen Bibliothek stellt sie eine eigens entwickelte Editionsinfrastruktur bereit, die für zahlreiche Projekte genutzt wird. 

Die HAB ist als Kompetenzzentrum in der Text+ Datendomäne Editionen in den beiden Clustern „Ancient and Medieval Texts“ und „Early Modern, Modern, and Contemporary Texts“ beteiligt. Damit einher geht die Bereitstellung von (Meta-)Daten und Werkzeugen, die Mitarbeit an Standardisierungsprozessen, aber auch an einem umfassenden und abgestimmten Beratungsangebot, in das die HAB ihre Expertise einbringt. Weiterhin trägt die HAB u.a. federführend zum Aufbau eines kuratierten Verzeichnisses von Editionen (sog. Registry) bei, das Nutzenden von Editionen aber auch Editoren und Editorinnen einen strukturierten Zugang zu der großen Anzahl an vorhandenen Ressourcen bieten soll. 

Abgesehen von Aufgaben innerhalb der Datendomäne Editionen engagiert sich die HAB auch in verschiedenen domänenübergreifenden AGs. So ist sie als Co-Lead in den AGs Registry und FID Koop tätig und ist u.a. Mitglied im Redaktionsteam des Text+ Blogs.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Kommentierte digitale Edition der Reise- und Sammlungsbeschreibungen Philipp Hainhofers (1578-1647)](https://hainhofer.hab.de/)
* [Lessing digital](https://diglib.hab.de/?link=180): Eine exemplarische Digitaledition von G. E. Lessings Schriften anhand von „Ernst und Falk“
* [Digitale Edition und Kommentierung der Tagebücher des Fürsten Christian II. von Anhalt-Bernburg (1599–1656)](http://www.tagebuch-christian-ii-anhalt.de/)
* Koordinierungsprojekt zur Weiterentwicklung von Verfahren der [Optical Character Recognition *OCR-D*](https://diglib.hab.de/?link=068)
* [Digital Humanities in Discuss Data](https://discuss-data.net/): Aufbau eines Communityspace


## Kontakt 
    
**Ansprechperson für Text+:** [Johannes Mangei](mailto:mangei@hab.de)
