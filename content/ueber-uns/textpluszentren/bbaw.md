---
title: Berlin-Brandenburgische Akademie der Wissenschaften (BBAW)
short_name: BBAW
text_plus_domains:
- Collections
- Lexikalische Ressourcen 
- Editionen 

text_plus_clusters:
  Collections: 
  - Historische Texte
  - Gegenwartssprache
  Lexikalische Ressourcen: 
  - Deutsche Wörterbücher im europäischen Kontext
  - Nicht-lateinische Schriftsysteme
  Editionen:
  - Antike und mittelalterliche Texte
  - Frühe moderne, moderne und zeitgenössische Texte


external_url: "https://www.bbaw.de/"

type: competence-center
---

# Berlin-Brandenburgische Akademie der Wissenschaften (BBAW)

**Text+ Zentrum:** 
* [Repositorium des CLARIN-D-Servicezentrums des Zentrums Sprache an der BBAW](https://clarin.bbaw.de/de) (BBAW-CLARIN-Repository)
* [TELOTA - The Electronic Life Of The Academy](https://www.bbaw.de/bbaw-digital/telota) (TELOTA)

**Zentrumstyp:** 
* **BBAW-CLARIN-Repository**: Daten-/Kompetenzzentrum (für die Domänen Collections und Lexikalische Ressourcen)
* **TELOTA**: Kompetenzzentrum (für die Domäne Editionen)

Das Zentrum Sprache an der BBAW ist an den beiden Datendomänen Collections und Lexikalische Ressourcen von Text+ beteiligt. Die langjährige Expertise des Zentrums Sprache im Bereich der Wörterbücher und historischen Korpora spiegelt sich nicht zuletzt in der Koordination des Bereichs Lexikalische Ressourcen wider.

Die zentralen Ressourcen des Zentrums Sprache an der BBAW sind historische, deutschsprachige Textkorpora (des DTA) und lexikalische Ressourcen (Wörterbücher des DWDS). Das Zentrum Sprache stellt die technischen Voraussetzungen (incl. BBAW-CLARIN-Repository) bereit, die die langfristige Verfügbarkeit von linguistischen und lexikographischen Services und Ressourcen sichern. Es bietet außerdem die Möglichkeit, Forschungsdaten externer Institutionen zu veröffentlichen. Das Zentrum Sprache berät und unterstützt Forschende darüber hinaus in Fragen des Forschungsdatenmanagements.

Zu den Arbeitsfeldern des TELOTA Kompetenzzentrums der BBAW im Bereich der Digital Humanities zählen unter anderem digitale Editionen, z. B. Briefeditionen, Forschung zu Markup, Forschungsdatenmanagement, Research Software Engineering und vieles mehr. Neben der Konzeption und Entwicklung von Forschungssoftware setzt sich TELOTA mit der Weiterentwicklung von Standards im Bereich der Textkodierung auseinander. TELOTA ist für die Betreuung und Wartung digitaler Ressourcen und zentraler Services der BBAW verantwortlich.

Das Kompetenzzentrum bietet neben Kooperationen in der universitären Lehre auch Workshops für Entwickler/Entwicklerinnen und Wissenschaftler/Wissenschaftlerinnen an und organisiert zudem das Berliner DH-Kolloquium mit.

## Highlights bereitgestellter Daten und Dienste

BBAW-CLARIN-Repository:

* [Digitales Wörterbuch der Deutschen Sprache (DWDS)](https://www.dwds.de/): Das gegenwartssprachliche DWDS-Wörterbuch steht im Zentrum des DWDS-Wortinformationssystems. Es kombiniert mehrere gegenwartssprachliche Wörterbücher. 
* [Deutsches Textarchiv (DTA)](https://www.deutschestextarchiv.de/): Das DTA stellt auf seiner Plattform disziplinen- und gattungsübergreifende Sammlungen und Korpora von deutschsprachigen Texten bereit. Als Grundlage für ein Referenzkorpus der neuhochdeutschen Sprache steht das rund 1500 Titel umfassende DTA-Kernkorpus im Zentrum.

TELOTA: 

* [correspSearch](https://correspsearch.net/de/start.html): Der Webservice ermöglicht die Suche in Verzeichnissen verschiedener digitaler und gedruckter Briefeditionen, zum Beispiel nach Absendern, Empfängern, Schreibort und Datum.
* [ediarum](https://www.ediarum.org/): ediarum ist ein Open-Source Softwareprojekt. Mit der digitalen Arbeitsumgebung können digitale Editionen erstellt und veröffentlicht werden. Der Aufbau ist modularisiert und kann verschiedenen Bedarfen angepasst werden.

## Entgegennahme Daten Dritter 
Das Servicezentrum des Zentrums Sprache der BBAW nimmt eine Vielzahl von Daten auf, darunter vor allem historische deutschsprachige Forschungsdaten, die über das DTA bereitgestellt werden, sowie Wörterbücher und Lexika, die in den Zuständigkeitsbereich des DWDS fallen.

Das Kompetenzzentrum TELOTA stellt die digitalen Editionen aus Akademienprojekten zur Nachnutzung bereit. Gegenwärtig werden Daten Dritter nicht aufgenommen.

## Kontakt

**Ansprechperson für Text+:** [BBAW-CLARIN-Repository](mailto:support@clarin-d.de) und [TELOTA](mailto:telota@bbaw.de)