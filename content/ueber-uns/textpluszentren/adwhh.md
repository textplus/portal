---
title: Akademie der Wissenschaften in Hamburg (AdWHH)
short_name: AdWHH
text_plus_domains:
- Collections
text_plus_clusters:
  Collections: 
  - Gegenwartssprache 
  - Historische Texte 

external_url: "https://www.awhamburg.de/"

type: competence-center
---

# Akademie der Wissenschaften in Hamburg (AdWHH)
    
**Text+ Zentrum:** [Forschungsdatenrepositorium der UHH](https://www.awhamburg.de/digitalisierung.html)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Seit ihrer Gründung im Jahr 2004 fördert die AdWHH die interdisziplinäre Forschung zu gesellschaftlichen Zukunftsfragen und grundlegenden wissenschaftlichen Problemen. Darüber hinaus koordiniert die AdWHH derzeit fünf langfristig angelegte Forschungsprojekte, die jeweils einen starken Fokus auf die digitale Erschließung und Analyse einzigartigen und vielfältigen Sprachmaterials legen. Dies umfasst unter anderem die Sammlung von Gebärdensprachdaten und bedrohten Minderheitensprachen.

Als zentrale Betriebseinheit an der Universität Hamburg stellt das Zentrum für nachhaltiges Forschungsdatenmanagement (FDM) Text+ eine lokale technische Infrastruktur, einschließlich eines Datenrepositoriums, für nachhaltiges Forschungsdatenmanagement zur Verfügung.
    
## Highlights bereitgestellter Daten und Dienste
    
* [Beta maṣāhǝft](https://www.awhamburg.de/forschung/langzeitvorhaben/die-schriftkultur-des-christlichen-aethiopiens-und-eritreas-eine-multimediale-forschungsumgebung.html): Eine systematische Studie der christlichen Manuskripttradition Äthiopiens und Eritreas
* [DGS-Korpus](https://www.awhamburg.de/forschung/langzeitvorhaben/woerterbuch-gebaerdensprache.html) erfasst und dokumentiert systematisch die Deutsche Gebärdensprache (DGS) und erstellt auf der Grundlage der Korpusdaten ein elektronisches Wörterbuch
* [Etymologika](https://www.awhamburg.de/en/research/long-term-projects/etymologika.html): Ordnung und Interpretation des Wissens in griechisch-byzantinischen Lexika bis in die Renaissance
* [Indigene nordeurasische Sprachen (INEL) Corpus](https://www.awhamburg.de/forschung/langzeitvorhaben/grammatiken-korpora-und-sprachtechnologie-fuer-indigene-nordeurasische-sprachen-ines.html): Bereitstellung von Sprachressourcen für indigene Sprachen und ausführlich kommentierte und audio-alignierte Korpora der Sprachen Dolgan, Kamas und Selkup
* [Formulae – Litterae – Chartae](https://www.awhamburg.de/en/research/long-term-projects/formulae-litterae-chartae.html): Erforschung und kritische Edition der frühmittelalterlichen Formulae, die eine Erforschung der Formulae-Schrift in Westeuropa vor der Entwicklung der ars dictaminis ermöglicht
* [Tamilex](https://www.tamilex.uni-hamburg.de/): Erstellung eines elektronischen Korpus der klassischen tamilischen Literatur und eines historischen Wörterbuches unter Berücksichtigung von einheimischen exegetischen und lexikographischen Quellen
    
## Entgegennahme Daten Dritter 
Die AdWHH nimmt insbesondere Daten Dritter mit Schwerpunkt auf mehrsprachige gesprochene Korpora, Transkriptionswerkzeuge sowie Gebärdensprache an.

## Kontakt 
    
**Ansprechperson für Text+:** [Timm Lehmberg](mailto:timm.lehmberg@awhamburg.de)

