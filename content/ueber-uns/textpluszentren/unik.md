---
title: Universität zu Köln (UniK)
short_name: UniK
text_plus_domains:
- Collections 
- Lexikalische Ressourcen 

text_plus_clusters:
  Collections: 
  - Gegenwartssprache
  Lexikalische Ressourcen:
  - Digitale lexikalische Ressourcen
  - Nicht-lateinische Schriftsysteme

external_url: "https://www.uni-koeln.de/"

type: competence-center
---

# Universität zu Köln (UniK)
    
**Text+ Zentrum:** [Language Archive Cologne / Data Center for the Humanities](https://lac.uni-koeln.de/)

**Zentrumstyp:** Daten-/Kompetenzzentrum

Die Universität zu Köln (UniK) ist als Forschungsstandort im Bereich Digital Humanities/eHumanities international sichtbar. Als Fakultätseinrichtung ist das Data Center for the Humanities (DCH) aktiv in die Lehre in den einschlägigen Studiengängen der Digital Humanities und der (Sprachlichen-)Informationsverarbeitung an der Universität zu Köln eingebunden.

Die UniK und das DCH stellen Text+ Daten aus zwei verschiedenen Domänen, Collections und lexikalische Ressourcen, zur Verfügung. Das DCH legt einen besonderen Schwerpunkt auf Sprachdaten aus dem globalen Süden und verfügt über besondere Expertise bei Datensammlungen und lexikalischen Ressourcen über gefährdete und außereuropäische Sprachen sowie bei Aufnahmen außereuropäischer mündlicher Literatur.
    
## Highlights bereitgestellter Daten und Dienste
    
Collections:
* [Documentation of language variety in Latin America and the Caribbean](https://lac.uni-koeln.de/collection/11341/0000-0000-0000-3D31): Audioaufnahmen, die die Sprachvarietäten des Spanischen und Portugiesischen der Sprachen der indigenen Bevölkerung erfassen
* [Oral Tales of Mongolian Bards](https://lac.uni-koeln.de/collection/11341/0000-0000-0000-271F): Mündlich vorgetragene Erzählungen von Barden aus der östlichen Inneren Mongolei, darunter epische Gedichte und Erzählungen auf der Grundlage chinesischer Abenteuerromane
 
Lexikalische Ressourcen:
* [Kosh](https://cceh.github.io/kosh/): Generische Infrastruktur zur Veröffentlichung beliebiger XML-basierter lexikalischer Ressourcen über standardisierte Application Programming Interfaces (APIs)
* [Cologne Digital Sanskrit Dictionaries](https://sanskrit-lexicon.uni-koeln.de/): Die größte Ressource für die klassische südasiatische Sprache Sanskrit
    
## Entgegennahme Daten Dritter 
Die UniK bietet ihre Archivierungsdienste allen Forschenden, Institutionen und Einzelpersonen – ausdrücklich auch Studenten und Muttersprachlern – weltweit und insbesondere in Regionen an, in denen es keine lokalen Institutionen gibt, die diese Unterstützung anbieten.

## Kontakt 
    
**Ansprechperson für Text+:** lac-manager@uni-koeln.de
