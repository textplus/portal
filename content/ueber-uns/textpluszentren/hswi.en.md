---
title: Wismar University of Applied Sciences (HSWI)
short_name: HSWI
text_plus_domains:
- Infrastructure/Operations 
- Collections

text_plus_clusters:
  Collections: 
  - Unstructured Text
  Lexikalische Ressourcen: 
  - Non-Latin Scripts

external_url: "https://www.hs-wismar.de/en/"

type: competence-center
---

# Wismar University of Applied Sciences (HSWI)

**Text+ center:** [Wismar University of Applied Sciences](https://www.hs-wismar.de/en/research/from-the-research/rdb/)

**Type of center:** competence center

Wismar University of Applied Sciences is a high-performance research institution with a long-standing tradition. It is internationally oriented and rooted in Mecklenburg-Vorpommern. The interdisciplinary networking of the three faculties of Engineering, Economics and Design is a particular trademark and is documented in four cross-faculty research focuses. 

With regard to the text- and language-based sciences supported by the Text+ consortium, Wismar University of Applied Sciences sees itself as a potential user of the infrastructure and a source of research data. Within the Text+ consortium, Wismar University of Applied Sciences thus assumes the role of a user and acts as an incubator within the state of MV, for example by being involved in the state-wide RDM network.

## Highlights of provided data and services

* [Tool support for the automatic extraction of tabular data from historical newspapers](https://www.hs-wismar.de/forschung/aus-der-forschung/fdb/detail/n/scannedtables/): The aim of this project is to investigate the extent to which existing solutions for table extraction can be applied to historical journals. The aim is to develop a toolchain that allows tables with personal data to be extracted and processed in a reproducible manner using the historical journal “Swinemünder Badeanzeiger” in order to be used as a database for subsequent analysis.
* [Revisions of gender constructions in textual traditions](https://www.hs-wismar.de/forschung/aus-der-forschung/fdb/detail/n/gendervarianten/): The project combines New Testament textual criticism, analyses of the reception of gender constructions and information science research as well as systematic-ethical investigations.
* [Datenkompass M-V: Kompetenznetzwerk Forschungsdaten Mecklenburg-Vorpommern](https://www.hs-wismar.de/forschung/aus-der-forschung/fdb/detail/n/dkmv/) pursues the goal of establishing a data competence network in Mecklenburg-Vorpommern. The aim is to pool the strengths of scientific institutions in order to facilitate access for researchers and research-related commercial enterprises in the field of data science.


## Contact

**Contact for Text+:** [Frank Krüger](mailto:frank.krueger@hs-wismar.de)

