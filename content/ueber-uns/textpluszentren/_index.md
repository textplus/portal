---
title: Text+ Zentren

aliases:
- /forschungsdaten/daten-und-kompetenzzentren
- /vernetzung/daten-und-kompetenzzentren
- /ueber-uns/daten-kompetenzzentren/

menu:
  main:
    weight: 20
    parent: ueber-uns

params:
  last_mod_today: true

type: non-scrolling-toc
---

# Text+ Zentren

Text+ bildet eine ortsverteilte Infrastruktur, die von Text+ Zentren getragen wird. Die Zentren können sowohl Daten- als auch Kompetenzzentren sein oder auch beide Funktionen erfüllen.

## Datenzentren

Datenzentren fungieren in Text+ als Partnereinrichtungen, die sich (teilweise für spezifische Domänen) auf die Erfassung, Speicherung, Verwaltung und Bereitstellung von Forschungsdaten konzentrieren. Sie stellen zum einen eigene Daten zur Verfügung, nehmen aber auch Daten Dritter entgegen und bilden die Grundpfeiler des Text+ Data Space. Sie bieten die technische Infrastruktur und die nötigen Dienstleistungen, um sicherzustellen, dass die Forschungsdaten
* in standardisierten Formaten vorliegen, die eine langfristige Nutzung und Nachnutzung ermöglichen,
* angemessen dokumentiert und beschrieben sind, um die Wiederverwendung durch andere Forschende zu erleichtern,
* im Sinne der Langzeitarchivierung sicher gespeichert und archiviert werden, sowie
* über geeignete Schnittstellen und Plattformen zugänglich gemacht werden können.

## Kompetenzzentren

Bei Kompetenzzentren handelt es sich um spezialisierte Partnereinrichtungen, die ihr Fachwissen beim Aufbau der Angebote von Text+ einbringen und Unterstützung in spezifischen Bereichen der Forschungsdatenbereitstellung und ‑nutzung anbieten. Ihre Hauptaufgaben umfassen:
* Beratung und Schulung Kompetenzzentren für Forschende bei der Nutzung und Verwaltung von Forschungsdaten. 
* Unterstützung bei der Datenintegration verschiedener Datensätze und -quellen, um umfassendere und stärker vernetzte Forschungsressourcen zu schaffen.
* Förderung der Datenkultur durch die Pflege und Einbindung von Netzwerken der Zielgruppen von Text+ um eine Kultur des Teilens und der Wiederverwendung von Daten in der wissenschaftlichen Gemeinschaft zu fördern.

Die Text+ Zentren werden den Bereichen Collections, Lexikalische Ressourcen, Editionen und Infrastruktur/Betrieb zugeordnet. Die drei Datendomänen Collections, Lexikalische Ressourcen und Editionen sind zudem in thematischen Clustern organisiert, wodurch eine einschlägige Erfassung der Forschungsdaten vereinfacht wird: Die Cluster bündeln alle Aktivitäten im Zusammenhang mit bestimmten Subtypen von Daten und Forschungsmethoden in einer Datendomäne entsprechend den Bedürfnissen und Forschungsprioritäten der jeweiligen Interessengemeinschaft.

{{<image img="Data-Domains-Thematic-Clusters_2023.png">}}
Datendomänen und ihre thematischen Cluster
{{</image>}}

Infrastruktur/Betrieb koordiniert die Entwicklung der ortsverteilten Infrastruktur und verfolgt das Ziel eines integrierten und aufeinander abgestimmten Portfolios an Services, Daten und Diensten.


## Übersicht

{{< standout theme="white" col_width="9">}}
{{<competence-center-list>}}
{{< /standout >}}
