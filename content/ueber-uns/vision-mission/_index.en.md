---
title: Vision and Mission

menu:
  main:
    weight: 35 
    parent: ueber-uns
---

# Vision and Mission

### Vision

The text- and language-oriented humanities and social sciences
extensively leverage the possibilities of digitization in their
research, teaching, and transfer, establishing a common data culture.


### Mission

We empower text and language data and access to such data. Access to
digital sources should become a standard. We enhance the digital
literacy of researchers. We encompass the diversity of the research
community and build on their participation. We promote inter- and
transdisciplinarity as well as innovation – for instance in the field
of artificial intelligence – through the integration of infrastructure
and research.
