---
title: Vision und Mission

menu:
  main:
    weight: 35 
    parent: ueber-uns
---

# Vision und Mission

### Vision

Die text- und sprachorientierten Geistes- und Sozialwissenschaften
nutzen die Möglichkeiten der Digitalisierung in ihrer Forschung und
Lehre sowie im Transfer umfassend und haben eine gemeinsame
Datenkultur.

### Mission

Wir ertüchtigen Text- und Sprachdaten und den Zugang zu solchen Daten.
Der Zugriff auf digitale Quellen soll zum Standard werden. Wir stärken
die Digital Literacy der Forschenden. Wir decken die Diversität der
Forschungsgemeinde ab und bauen auf ihre Partizipation. Wir befördern
Inter- und Transdisziplinarität sowie Innovation – etwa im Bereich der
künstlichen Intelligenz – durch Integration von Infrastruktur und
Forschung.
          
