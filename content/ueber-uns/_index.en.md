---
# just a dummy file to set the title and menu

title: "About us"

menu:
  main:
    identifier: "ueber-uns"
    weight: 40

build:
  render: never

---
