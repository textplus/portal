---
title: Governance
type: non-scrolling-toc

aliases:
- /forschungsdaten/datendomaenen/
- /ueber-uns/boards
- /ueber-uns/coordination-committees
- /en/ueber-uns/struktur-und-governance/
  
menu:
  main:
    weight: 40 
    parent: ueber-uns

---


## Governance

At the core of Text+'s governance is the shared responsibility of infrastructure and community, as well as cooperation across disciplinary boundaries. The listed boards are tasked with continuously evaluating Text+'s portfolio of data, tools, and services and driving its development in collaboration with the community.

The speaker of the consortium is [Prof. Dr. Andreas Witt](https://www1.ids-mannheim.de/digspra/personal/witt.html), the Operations Speaker is [Prof. Dr. Philipp Wieder](https://gwdg.de/research-education/researchgroup_wieder/).

In the NFDI Association, Text+ is also represented by the elected spokesperson [Prof. Dr. Andreas Witt](https://www1.ids-mannheim.de/digspra/personal/witt.html) and the elected deputy spokesperson [Prof. Dr. Andrea Rapp](https://www.linglit.tu-darmstadt.de/institutlinglit/mitarbeitende/andrearapp/).

{{<image img="Kap.3-Governance-Organigramm-Text-768x691.png" alt="Text+-Governance-Organigram"/>}}

## Boards

The **Scientific Board** holds the scientific leadership of the consortium and decides on portfolio development.

The **Steering Committee** is responsible for implementing the work program and takes charge of the professional and financial monitoring of ongoing work. The Overall Coordination, consisting of Scientific and Operations Speakers, is the link between these committees and is responsible for consortium management and office leadership.

The leaders of the (co-)applying institutions form the **Management Group**. It supports the Steering Committee and Overall Coordination in overarching and strategic matters.

{{<team name="Scientific Board">}}
{{<team-member img="gfx/Witt-Andreas.jpg" url="https://www1.ids-mannheim.de/digspra/personal/witt.html" role="Scientific Speaker" institution="Leibniz Institute for the German Language">}}Prof. Dr. Andreas Witt{{</team-member>}}
{{<team-member img="gfx/Rapp-Andrea-Katrin-Binner.jpg" url="https://www.linglit.tu-darmstadt.de/institutlinglit/mitarbeitende/andrearapp/index.de.jsp" role="Scientific Vice Speaker" institution="Technical University of Darmstadt">}}Prof. Dr. Andrea Rapp{{</team-member>}}
{{<team-member img="gfx/Teich-Elke-Quelle-privat.jpg" url="https://www.uni-saarland.de/lehrstuhl/teich.html" role="Scientific Vice Speaker" institution="University of Saarland">}}Prof. Dr. Elke Teich{{</team-member>}}
{{<team-member img="gfx/Wieder-Philipp.jpg" url="https://gwdg.de/research-education/researchgroup_wieder/" role="Operations Speaker" institution="Georg-August University of Göttingen, Göttingen State and University Library">}}Prof. Dr. Philipp Wieder{{</team-member>}}
{{<team-member img="gfx/Berenike_Herrmann_1480.jpg" url="https://ekvv.uni-bielefeld.de/pers_publ/publ/PersonDetail.jsp;jsessionid=9361A63F248C0E3485779F02FB68A18A?personId=262987169" role="SCC Chair Collections" institution="Bielefeld University">}}Prof. Dr. Berenike Herrmann{{</team-member>}}
{{<team-member img="gfx/Acquavella-Rauch-Stefanie.jpg" url="https://www.musikwissenschaft.uni-mainz.de/personen/prof-dr-stefanie-acquavella-rauch/" role="SCC Chair Editions" institution="Johannes Gutenberg University Mainz">}}Prof. Dr. Stefanie Acquavella-Rauch{{</team-member>}}
{{<team-member img="gfx/Schroder-Ingrid-Quelle-privat.jpg" url="https://www.slm.uni-hamburg.de/germanistik/personen/schroeder.html" role="SCC Chair Lexical Resources" institution="University of Hamburg">}}Prof. Dr. Ingrid Schröder{{</team-member>}}
{{<team-member img="gfx/Petras-Vivien-Quelle-privat.jpg" url="https://www.ibi.hu-berlin.de/de/ueber-uns/personen/petras" role="OCC Chair" institution="Institut für Bibliothekswissenschaft an der Humboldt-Universität zu Berlin">}}Prof. Vivien Petras, PhD{{</team-member>}}
{{</team>}}

{{<team name="Steering Group" theme="white">}}
{{<team-member img="gfx/Geyken-Alexander-Quelle-privat.jpg" url="https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/geyken-alexander" role="Data Domain Speaker Lexical Resources" institution="Berlin-Brandenburg Academy of Sciences and Humanities">}}PD Dr. Alexander Geyken{{</team-member>}}
{{<team-member img="gfx/Witt-Andreas.jpg" url="https://www1.ids-mannheim.de/digspra/personal/witt.html" role="Scientific Speaker" institution="Leibniz Institute for the German Language">}}Prof. Dr. Andreas Witt{{</team-member>}}
{{<team-member img="gfx/Leinen-Peter-Quelle-privat.jpeg" url="https://www.dnb.de/DE/Ueber-uns/Organisation/organisation_node.html#doc57844bodyText4" role="Data Domain Speaker Collections" institution="German National Library">}} Dr. Peter Leinen{{</team-member>}}
{{<team-member img="gfx/Speer-Andreas-Quelle-privat.jpg" url="https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/andreas-speer" role="Data Domain Speaker Editions" institution="North Rhine-Westphalian Academy of Sciences and Arts">}}Prof. Dr. Andreas Speer{{</team-member>}}
{{<team-member img="gfx/Wieder-Philipp.jpg" url="https://gwdg.de/research-education/researchgroup_wieder/" role="Operations Speaker" institution="Georg-August University Göttingen, Göttingen State and University Library">}}Prof. Dr. Philipp Wieder{{</team-member>}}
{{</team>}} 

{{<team name="Management Group">}}
{{<team-member img="" url="https://www.awk.nrw/akademie/akademieverwaltung" role="Delegate of the North Rhine-Westphalian Academy of Sciences and Arts" institution="">}}N.N. {{</team-member>}}
{{<team-member img="gfx/Lobin-Henning-Quelle-privat-1.jpg" url="https://www.ids-mannheim.de/zfo/personal/lobin" role="Scientific Director of the Leibniz Institute for the German Language" institution="">}}Prof. Dr. Henning Lobin{{</team-member>}}
{{<team-member img="gfx/Markschies_Christoph_Pablo-Castagnola.jpg" url="https://www.bbaw.de/die-akademie/bbaw-mitglieder/mitglied-christoph-markschies" role="President of the Berlin-Brandenburg Academy of Sciences and Humanities" institution="">}}Prof. Dr. Dr. h. c. mult. Christoph Markschies{{</team-member>}}
{{<team-member img="gfx/Scholze-Frank.jpg" url="https://www.dnb.de/DE/Ueber-uns/Organisation/organisation_node.html;jsessionid=529AE39F5AD2ED2E53DEB67910C7C2FE.internet282#doc57844bodyText1" role="Director-General of the German National Library" institution="">}}Frank Scholze{{</team-member>}}
{{<team-member img="gfx/Kaufmann.png" url="https://www.sub.uni-goettingen.de/en/contact/staff-a-z/staff-details/person/thomas-kaufmann/" role="Director of the Göttingen State and University Library" institution="">}}Prof. Dr. Thomas Kaufmann{{</team-member>}}
{{</team>}}

## Coordination Committees

The Coordination Committees consist of three different Scientific Coordination Committees, each responsible for one of the data domains (Collections, Editions, Lexical Resources), and an Operations Coordination Committee. Their task is to continuously evaluate and expand the portfolio of data, tools, and services. The Coordination Committees consist of experts from the respective (subject) domains.

{{<team name="Scientific Coordination Committee: Collections">}}
{{<team-member img="gfx/Berenike_Herrmann_1480.jpg" url="https://ekvv.uni-bielefeld.de/pers_publ/publ/PersonDetail.jsp;jsessionid=9361A63F248C0E3485779F02FB68A18A?personId=262987169" role="SCC Chair Collections" institution="University of Bielefeld">}}Prof. Dr. Berenike Herrmann{{</team-member>}}
{{<team-member img="gfx/Bender_Michael.jpg" url="https://www.linglit.tu-darmstadt.de/institutlinglit/mitarbeitende/michaelbender/index.de.jsp" role="SCC Collections Member" institution="Technical University Darmstadt">}} Dr. Michael Bender{{</team-member>}}
{{<team-member img="gfx/Eisler-Cornelia_Stefan-Wilde.jpg" url="https://www.bkge.de/BKGE/MitarbeiterInnen/Wissenschaftlich/Eisler/" role="SCC Collections Member" institution="Federal Institute for Culture and History of the Germans in Eastern Europe">}}Dr. Cornelia Eisler{{</team-member>}}
{{<team-member img="gfx/kurzawe-daniel.jpg" url="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/daniel-kurzawe/" role="SCC Collections Member" institution="Göttingen State and University Library Göttingen">}}Dr. Daniel Kurzawe{{</team-member>}}
{{<team-member img="gfx/Leinen-Peter-Quelle-privat.jpeg" url="https://www.dnb.de/DE/Ueber-uns/Organisation/organisation_node.html#doc57844bodyText4" role="Data Domain Speaker Collections" institution="German National Library">}} Dr. Peter Leinen{{</team-member>}}
{{<team-member img="gfx/Meier-Vieracker-Simon.jpg" url="https://tu-dresden.de/gsw/slk/germanistik/al/die-professur/inhaber" role="SCC Collections Member" institution="Technical University Dresden">}}Prof. Dr. Simon Meier-Vieracker{{</team-member>}}
{{<team-member img="" url="https://www.slawistik.hu-berlin.de/de/member/meyerrol" role="SCC Collections Member" institution="Humboldt-Universität zu Berlin">}}Prof. Dr. Roland Meyer{{</team-member>}}
{{<team-member img="" url="" role="SCC Collections Member" institution="Utrecht University">}}Max Noichl{{</team-member>}}
{{<team-member img="gfx/Plaksin-Anna.jpg" url="https://www.uni-paderborn.de/person/102981" role="SCC Collections Member" institution="University of Paderborn">}}Prof. Dr. Anna Plaksin{{</team-member>}}
{{<team-member img="" url="" role="SCC Collections Member" institution="University Library of Humboldt-Universität zu Berlin">}}Dr. Yong-Mi Rauch{{</team-member>}}
{{<team-member img="" url="https://qis.server.uni-frankfurt.de/qisserver/rds?state=verpublish&status=init&vmfile=no&moduleCall=webInfo&publishConfFile=webInfoPerson&publishSubDir=personal&keep=y&purge=y&personal.pid=19636&showsearch=n" role="SCC Collections Member" institution="Universitätsbibliothek Frankfurt am Main">}}Dr. Tobias Weber{{</team-member>}}
{{<team-member img="" url="" role="SCC Collections Member" institution="Technische Universität Braunschweig">}}Prof. Dr. Johannes Wienand{{</team-member>}}
{{</team>}}


{{<team name="Scientific Coordination Committee: Editions" theme="white">}}
{{<team-member img="gfx/Acquavella-Rauch-Stefanie.jpg" url="https://www.musikwissenschaft.uni-mainz.de/personen/prof-dr-stefanie-acquavella-rauch/" role="SCC Chair Editions" institution="Johannes Gutenberg University Mainz">}}Prof. Dr. Stefanie Acquavella-Rauch{{</team-member>}}
{{<team-member img="gfx/Anne-Bohnenkamp.jpg" url="https://www.adwmainz.de/mitglieder/profil/prof-dr-anne-bohnenkamp-renken.html" role="SCC Editions Member" institution="Goethe University Frankfurt and Freies Deutsches Hochstift/Goethe Museum">}}Prof. Dr. Anne Bohnenkamp-Renken{{</team-member>}}
{{<team-member img="gfx/Busch_Hannah.png" url="https://cceh.uni-koeln.de/personen/hannah-busch/" role="SCC Editions Member" institution="Cologne Center for eHumanities">}}Hannah Busch{{</team-member>}}
{{<team-member img="" url="" role="SCC Editions Member" institution="Thuringian University and State Library Jena">}}Swantje Dogunke{{</team-member>}}
{{<team-member img="" url="https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/heil-matthaeus" role="SCC Editions Member" institution="Berlin-Brandenburg Academy of Sciences and Humanities">}}Prof. Dr. Matthäus Heil{{</team-member>}}
{{<team-member img="gfx/Henny-Krahmer_Ulrike.jpg" url="https://www.germanistik.uni-rostock.de/lehrende/professorinnen-und-professoren/jun-prof-dr-ulrike-henny-krahmer/" role="SCC Editions Member" institution="University of Rostock">}}Jun.-Prof. Ulrike Henny-Krahmer{{</team-member>}}
{{<team-member img="" url="https://www.uni-flensburg.de/germanistik/abteilungen/niederdeutsche-sprache-und-literatur-und-ihre-didaktik/langhanke-robert-ma" role="SCC Editions Member" institution="Europa-Universität Flensburg">}}Robert Langhanke{{</team-member>}}
{{<team-member img="gfx/pfeiffer_judith_Foto-Humboldt-Stiftung-Wolfgang Hemmann.jpg" url="https://www.ioa.uni-bonn.de/isl/de/pers/pfeiffer" role="SCC Editions Member" institution="University of Bonn">}}Prof. Dr. Judith Pfeiffer{{</team-member>}}
{{<team-member img="" url="https://philosophie.phil-fak.uni-koeln.de/personen/universitaetsprofessorinnen/jun-prof-fiorella-retucci" role="SCC Editions Member" institution="University of Cologne">}}Prof. Dr. Fiorella Retucci{{</team-member>}}
{{<team-member img="" url="https://uni-tuebingen.de/fakultaeten/philosophische-fakultaet/fachbereiche/neuphilologie/romanisches-seminar/ehrlicher/ehrlicher/team/dr-antonio-rojas-castro/" role="SCC Editions Member" institution="University of Tübingen">}}Dr. Antonio Rojas Castro{{</team-member>}}
{{<team-member img="gfx/Speer-Andreas-Quelle-privat.jpg" url="https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/andreas-speer" role="Data Domain Speaker Editions" institution="North Rhine-Westphalian Academy of Sciences and Arts">}}Prof. Dr. Andreas Speer{{</team-member>}}
{{</team>}}


{{<team name="Scientific Coordination Committee: Lexical Resources">}}
{{<team-member img="gfx/Schroder-Ingrid-Quelle-privat.jpg" url="https://www.slm.uni-hamburg.de/germanistik/personen/schroeder.html" role="SCC Chair Lexical Resources" institution="University of Hamburg">}}Prof. Dr. Ingrid Schröder{{</team-member>}}
{{<team-member img="gfx/Heid-Ulrich.jpg" url="https://www.uni-hildesheim.de/fb3/institute/iwist/mitglieder/heid/" role="SCC Co-Chair Lexical Resources" institution="University of Hildesheim">}}Prof. Dr. Ulrich Heid{{</team-member>}}
{{<team-member img="gfx/Aehnlich_Barbara.JPG" url="https://www.uni-bremen.de/fb-10/fachbereich/wissenschaftlerinnen-wissenschaftler/lektorinnen-lektoren/pd-dr-phil-barbara-aehnlich" role="SCC Lexical Ressources Member" institution="University of Bremen">}}Dr. Barbara Aehnlich {{</team-member>}}
{{<team-member img="" url="https://www.linglit.tu-darmstadt.de/institutlinglit/mitarbeitende/sabinebartsch/index.de.jsp" role="SCC Lexical Resources Member" institution="Technical University of Darmstadt">}}Dr. Sabine Bartsch{{</team-member>}}
{{<team-member img="gfx/Fischer_Hanna.jpg" url="https://www.germanistik.uni-rostock.de/personen/professuren/prof-dr-hanna-fischer/" role="SCC Lexical Ressources Member" institution="University of Rostock">}}Prof. Dr. Hanna Fischer{{</team-member>}}
{{<team-member img="gfx/Geyken-Alexander-Quelle-privat.jpg" url="https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/geyken-alexander" role="Data Domain Speaker Lexical Resources" institution="Berlin-Brandenburg Academy of Sciences and Humanities">}}PD Dr. Alexander Geyken{{</team-member>}}
{{<team-member img="" url="" role="SCC Lexical Ressources Member" institution="University of Münster">}}Dr. Stefan Heßbrüggen-Walter{{</team-member>}}
{{<team-member img="gfx/Nowak-Jessica.png" url="https://idsl1.phil-fak.uni-koeln.de/personen/lehrende-a-z/dr-jessica-nowak" role="SCC Lexical Resources Member" institution="University of Cologne">}}Dr. Jessica Nowak{{</team-member>}}
{{<team-member img="gfx/Osswald-Rainer.jpg" url="https://user.phil.hhu.de/osswald/" role="SCC Lexical Resources Member" institution="Heinrich-Heine-University Düsseldorf">}}Dr. Rainer Osswald{{</team-member>}}
{{<team-member img="" url="" role="SCC Lexical Ressources Member" institution="Martin-Luther-Universität Halle-Wittenberg">}}Prof. Dr. Simone Schultz-Balluff{{</team-member>}}
{{<team-member img="gfx/Heike-Zinsmeister.jpg" url="https://www.slm.uni-hamburg.de/germanistik/personen/zinsmeister.html" role="SCC Lexical Resources Member" institution="University of Hamburg">}}Prof. Dr. Heike Zinsmeister{{</team-member>}}
{{</team>}}

{{<team name="Operations Coordination Committee" theme="white">}}
{{<team-member img="gfx/Altenhoner-Reinhard.jpg" url="https://staatsbibliothek-berlin.de/die-staatsbibliothek/abteilungen/generaldirektion/info-sv" role="OCC Member" institution="Berlin State Library Prussian Cultural Heritage">}}Reinhard Altenhöner{{</team-member>}}
{{<team-member img="gfx/Degkwitz-Andreas.jpg" role="OCC Member" institution="University Library of Humboldt-Universität zu Berlin">}}Prof. Dr. Andreas Degkwitz{{</team-member>}}
{{<team-member img="gfx/eggert_eric.jpg" url="https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/eric-eggert" role="OCC Member" institution="University of Cologne">}}Eric Eggert{{</team-member>}}
{{<team-member img="gfx/patrick_helling.png" url="https://dch.phil-fak.uni-koeln.de/ueber-das-dch/team/patrick-helling" role="OCC Member" institution="University of Cologne, Data Center for the Humanities">}}Patrick Helling{{</team-member>}}
{{<team-member img="gfx/Henrich_Andreas-Quelle-privat.jpg" url="https://www.uni-bamberg.de/minf/team/henrich/" role="Operation Vice Speaker" institution="Otto-Friedrich-University Bamberg">}}Prof. Dr. Andreas Henrich{{</team-member>}}
{{<team-member img="gfx/Herrmann-Sebastian.jpg" url="https://www.philol.uni-leipzig.de/en/institute-for-american-studies/institute/faculty/sebastian-m-herrmann" role="OCC Member" institution="University of Leipzig">}}Dr. Sebastian Herrmann{{</team-member>}}
{{<team-member img="gfx/brigitte_mathiak.png" url="https://www.gesis.org/institut/ueber-uns/mitarbeitendenverzeichnis/person/brigitte.mathiak" role="OCC Co-Chair" institution="GESIS – Leibniz Institute for the Social Sciences">}}Dr. Brigitte Mathiak{{</team-member>}}
{{<team-member img="gfx/Petras-Vivien-Quelle-privat.jpg" url="https://www.ibi.hu-berlin.de/de/ueber-uns/personen/petras" role="OCC Chair" institution="Humboldt-Universität zu Berlin">}}Prof. Vivien Petras, PhD{{</team-member>}}
{{<team-member img="gfx/Razum-Matthias.png" url="https://www.fiz-karlsruhe.de/de/ueber-uns/ueber-uns#management" role="OCC Member" institution="FIZ Karlsruhe – Leibniz Institute for Information Infrastructure">}}Matthias Razum{{</team-member>}}
{{<team-member img="gfx/Renner-Westermann_Heike.jpg" url="https://www.linguistik.de/de/about/team/" role="OCC Member" institution="University of Frankfurt">}}Heike Renner-Westermann{{</team-member>}}
{{<team-member img="gfx/verena_weiland.jpg" url="https://www.romanistik.uni-bonn.de/romanistik-bonn/personal/personal-webseiten/dr-verena-weiland" role="OCC Member" institution="University of Bonn">}}Dr. Verena Weiland{{</team-member>}}
{{<team-member img="gfx/Wieder-Philipp.jpg" url="https://gwdg.de/research-education/researchgroup_wieder/" role="Operations Speaker" institution="Georg-August-University Göttingen, Göttingen State and University Library">}}Prof. Dr. Philipp Wieder{{</team-member>}}
{{</team>}}
