---
title: Verfahrensbeschreibung zur Besetzung der Koordinationskomitees in Text+

---

# Verfahrensbeschreibung zur Besetzung der Koordinationskomitees in Text+

Die Leitungsgruppe des NFDI-Konsortiums Text+ (nachfolgend kurz “Text+“) hat am 29. August 2022 im Rahmen eines Umlaufverfahrens das nachfolgend beschriebene Verfahren zur Besetzung der Koordinationskommitees von Text+ ab dem Herbst 2022 beschlossen. In der Governance-Struktur von Text+ werden diese Komitees als Scientific Coordination Committees und Operations Coordination Committee bezeichnet.

## Allgemeines

1. Diese Verfahrensbeschreibung zur Besetzung der Koordinationskomitees in Text+ ist nicht Bestandteil des Mittelweiterleitungs- und Kooperationsvertrags des Konsortiums Text+, sondern dient ausschließlich der Organisation der Scientific Coordination Committees (SCC) und des Operations Coordination Committee (OCC).
2. Große Bedeutung kommt bei der Besetzung der Koordinationskomitees in Text+ einer ausgewogene Mitgliedschaft zu:
  - Forschende verschiedener Stufen der akademischen Ausbildung sollen vertreten sein, also sowohl Forschende in einem frühen Stadium ihrer wissenschaftlichen Karriere (z. B. Doktoranden/Doktorandinnen) als auch erfahrene Forschende (z. B. Professoren/Professorinnen am Ende ihrer wissenschaftlichen Laufbahn).
  - Die Zusammensetzung soll möglichst die Vielfalt der Disziplinen repräsentieren, sowohl was die Größe als auch die Inhalte betrifft.
  -  Das Geschlechterverhältnis soll ausgewogen sein.
  -  Die geografische Verteilung der Mitglieder soll einer Konzentration auf wenige Standorte entgegenwirken. 

**Aufgaben der Koordinationskomitees**

Die Scientific Coordination Committees (kurz SCCs) und das Operations Coordination Committee (kurz OCC; im Folgenden unter dem Begriff Koordinationskomitees subsumiert) sind Teil der wissenschaftsgeleiteten Governance von Text+. Pro Task Area außerhalb von Administration gibt es ein solches Koordinationskomitee. Diese Koordinationskomitees begleiten Text+ dabei, das Portfolio von Text+ aus verschiedenen Angeboten sukzessive auszubauen. Zur Finanzierung dieser Erweiterungen stehen im Budget von Text+ sogn. Flex-Mittel zur Verfügung, d.h. Mittel, die bislang keiner Institution zugewiesenen sind. Die Zuweisung dieser Mittel erfolgt im Rahmen mehrerer Ausschreibungsrunden und den sich dabei ergebenden Förderempfehlungen durch die Koordinationskomitees.

Aus diesem Mandat ergeben sich für die **Koordinationskomitees insbesondere folgende Aufgaben:**

- Beteiligung an der Vergabe der Flex-Mittel:
  - Beteiligung an der Erstellung der Ausschreibung
    - Sichtung der eingegangenen Anträge für die Datendomänen; Begutachtung durch die Koordinationskomitees, ggf. unter Zuhilfenahme externer Gutachtenden
    - Priorisierung der eingegangenen Anträge in den Koordinationskomitees zwecks Vorbereitung für die (übergreifende) Priorisierung im Scientific Board als Vorlage für die Leitungsgruppe
  - Abnahme der Abschlussberichte der Flexmittelprojekte
- Monitoring des Fortschritts von Text+
  - anhand der bereitgestellten Jahresberichte von Text+ (ab Jahr 2)
  - durch Teilnahme an den jährlichen Plenarversammlungen von Text+ 
  

## Geltungsbereich

Diese Verfahrensbeschreibung zur Besetzung der Koordinationskomitees in Text+ gilt für die Besetzung der Komitees des Konsortiums Text+. Dazu wird das Wahlverfahren verwendet, das im Folgenden beschrieben wird.

## Wahlausschuss

Der Wahlausschuss wird aus den Vize-Sprechenden des Konsortiums mit Unterstützung der Koordination aus dem Scientific und Operations Office gebildet. Alle Mitglieder des Wahlausschusses haben kein passives Wahlrecht.

## Zusammensetzung der Koordinationskomitees

Jedes Koordinationskomitee besteht aus

  - bis zu 10 gewählten Mitgliedern, darunter mindestens eine Person, die zum wissenschaftlichen Nachwuchs gezählt wird. Diese Personen sollen einschlägig für den zugehörigen Bereich sein und aus verschiedenen Disziplinen, Einrichtungen, Verbänden und Verbünden sowie wissenschaftlichen Karrierestufen stammen und dürfen nicht Teil der Arbeitsgruppen des Konsortiums sein
  - nicht stimmberechtigte ständige Gäste:
    - Task Area Lead der korrespondierenden Arbeitsgruppe von Text+
    - ein Office-Mitglied als administrative und organisatorische Unterstützung
    - wissenschaftliche Spokesperson
  - Weitere Gäste können nach Bedarf eingeladen werden.

## Passive Wahlberechtigung

- Gewählt werden können Forschende, für die die folgenden Kriterien gelten:
  - Sie sind an einer deutschen akademischen Institution (im Sinne einer bei der [DFG antragsberechtigten Institution](https://www.dfg.de/foerderung/faq/allg_antragstellung_faq/index.html)) tätig.
  - Sie identifizieren sich mit den Zielen und Aufgaben von Text+.
  - Sie sind nicht Teil des geförderten Projekts.
  - Ihre Forschungsinteressen können einem der Bereiche von Text+ zugeordnet werden.
- Die Wahl erfordert eine fristgerechte Nominierung. Vorschlagsberechtigt sind
  - Fachverbände und -verbünde, die ihre Unterstützung für Text+ in einem Unterstützungsbrief ausgedrückt haben,
  - Mitglieder und Institutionen des Konsortiums Text+,
  - einschlägige Forschende außerhalb des Konsortiums Text+ (Selbstnominierung).
- Um die Nominierung von Personen aus dem Kreis des wissenschaftlichen Nachwuchses werden insbesondere auch die Fachverbände und -verbünde gebeten.
- Der Wahlausschuss ist bemüht, ein ausgewogenes Feld der Kandidierenden sicherzustellen und kann ggf. Gremien von Text+ sowie die Verbände und Verbünde um weitere Nominierungen bitten.
- Die Nominierung erfolgt jeweils für ein Komitee; eine Nominierung für mehrere Komitees ist möglich.
- Jede Person, die nominiert wurde, wird spätestens bis zum Abschluss der Nominierungsphase gefragt, ob sie kandidieren möchte.
  - Falls jemand für mehrere Komitees nominiert wurde, wählt diese Person aus, für welches Komitee sie kandidieren möchte.
  - Nur diejenigen, die bis eine Woche vor der Wahl die Bereitschaft zur Kandidatur gegenüber einem Mitglied des Wahlausschusses eindeutig per E-Mail oder in einer anderen schriftlichen Form ausdrücken, werden auf die jeweilige Wahlliste aufgenommen.
- Die Nominierungsphase endet 14 Tage vor dem festgesetzten Beginn der Wahlen.
- Zwischen Bekanntgabe des Wahltermins und dem Ende der Nominierungsphase sollen nicht weniger als 14 Tage liegen.
- Eine erneute Wahl ist möglich. 

## Aktive Wahlberechtigung

Ziel der Einschränkung der Wahlberechtigung ist, dass keine Institution oder kein Verband/Verbund die Wahlen dominiert.

Die aktive Wahlberechtigung hat:

- ein Vertreter/eine Vertreterin für jeden Fachverband oder -verbund, der seine Unterstützung für Text+ in einem Unterstützungsbrief entweder vor dem Antrag des Konsortiums oder während der Laufzeit ausgedrückt hat. Die Vertretung erfolgt durch eine Person, die von dem jeweiligen Verband oder Verbund gegenüber Text+ benannt wird.
- jede Institution, die Teil des Konsortiums Text+ ist; die Vertretung erfolgt durch die Person, die im Rahmen der Projektdurchführung dem Erstempfänger genannt wurde.

Ausschlaggebend für die Wahlberechtigung ist der Status am Tag der Wahlbekanntmachung.

Die Wahlberechtigten dürfen sich vertreten lassen, wobei keine Person mehr als einen Wahlberechtigten vertreten darf. Wenn jemand nicht selbst abstimmen möchte, leitet diese Person die Zugangsinformationen nach Erhalt einer von ihr bevollmächtigten Person weiter. Die Verantwortung für die Vertraulichkeit der Weitergabe liegt bei der Vollmacht-gebenden Person, da mehrfache Stimmabgaben technisch unterbunden werden.

Nach der Wahlbekanntmachung zeigt der Wahlausschuss bei den Kontaktpersonen der Verbände und Verbünde an, dass sie als Wahlberechtigte als Vertretung für einen Verband oder Verbund geführt werden und bittet ggf. um eine Aktualisierung der Kontaktinformationen, falls der Verband oder Verbund eine neue Ansprechperson benannt, aber noch nicht mitgeteilt hat.

Entsprechend erhalten die Vertretungen der Institutionen in Text+ die Information, dass sie als Wahlberechtigte geführt werden.

## Wahlperiode

Die Wahlperiode dauert jeweils bis zur nächsten Konstituierung eines neuen Komitees nach dem Wahlaufruf, der in der Regel im Rahmen der jährlichen Plenarversammlungen in geradzahligen Kalenderjahren veröffentlich wird. Die Konstituierung des jeweiligen neuen Komitees besteht aus einem Treffen des neu gewählten Komitees und der Wahl einer Person, die dem Komitee vorsitzt. Zwischen der Wahlbekanntmachung und der konstituierenden Sitzung mit Wahl des Vorsitzes ist das vorherige Komitee kommissarisch im Amt.

## Durchführung der Wahlen

Die Wahlen von Text+ werden im Online-Wahlverfahren mit Hilfe eines Systems durchgeführt, das eine anonyme Wahl der Wahlberechtigten erlaubt, z. B. mithilfe des Angebots der Firma Polyas [https://www.polyas.de/](https://www.polyas.de/blog/de/tag/polyas-wahlsystem) oder der Firma Votebox <https://votebox.com/> .

Der Ablauf der Wahl erfolgt folgendermaßen:

- Der Wahltermin wird vom Wahlausschuss zusammen mit der Steuerungsgruppe festgelegt.
- Die Wahl wird auf dem Plenary von Text+ angekündigt.
- Bis 14 Tage vor dem Wahltermin können Nominierungen für die Komitees erfolgen.
  - Für jedes Komitee wird eine separate Nominierungsliste geführt.
  - Der Wahlaussschuss nimmt Kontakt mit den Nominierten auf und bittet um die Bereitschaft zum Kandidieren. Personen, die für mehrere Komitees nominiert wurden, entscheiden sich, für welches Komitee sie kandidieren möchten. Diese Bereitschaft ist spätestens eine Woche vor Beginn der Wahlen zu erklären.
- Die Wahlen werden im Online-System angelegt. Jedes Komitee erhält einen separaten Stimmzettel. Kandidierende aus dem wissenschaftlichen Nachwuchs werden getrennt von den anderen Kandidierenden aufgeführt.
- Die Wahlberechtigten erhalten die Zugangsinformationen zum Wahlsystem.
- Die Wahlberechtigten haben für jedes Komitee jeweils eine Stimme für Kandidierende aus dem wissenschaftlichen Nachwuchs und bis zu 9 Stimmen für weitere Kandidierende.
- Falls die Anzahl der Plätze der Anzahl der Kandidierenden entspricht, kann auch eine Listenwahl erfolgen.
- Die Abstimmung wird eine Woche nach Beginn der Abstimmung geschlossen und die Stimmen ausgezählt.

## Protokollierung der Wahl

- Das Ergebnis der Wahlen wird vom Wahlausschuss im Rahmen einer gemeinsamen Sitzung, die auch online erfolgen kann, ausgewertet.
- Die Gewählten werden um die Annahme der Wahl gebeten. Nimmt eine gewählte Person die Wahl nicht an, gilt die Person mit den nächst-häufigen Stimmen als gewählt, sofern sie die Wahl annimmt.
- Der Bericht über die Wahlen wird den Wahlberechtigten und dem Konsortium auf geeignete Weise bekannt gegeben, z. B. durch die Ablage in einem nicht-öffentlich zugänglichen Verzeichnis des Projekts.

## Konstituierende Sitzung der Komitees

- Innerhalb von 6 Wochen nach Bekanntgabe der Wahlergebnisse tritt das Komitee zusammen; ein entsprechender Terminplaner wird aus dem Office von Text+ zur Verfügung gestellt.
- In der konstituierenden Sitzung wird eine Person aus der Mitte der gewählten Mitglieder des Komitees gewählt, die den Vorsitz (Chair) übernimmt.
- Die Wahl kann in der konstituierenden Sitzung durch ein beliebiges Mitglied des Komitees initiiert werden, auch durch den Task Area Lead, der als ständiger Gast nicht wahlberechtigt ist.
- Die Wahl erfordert, dass mindestens ¾ der gewählten Mitglieder an der Sitzung teilnehmen.
- Der oder die Vorsitzende des Komitees wird damit auch in das Scientific Board von Text+ gewählt und nimmt an den Sitzungen des Scientific Boards in der laufenden Wahlperiode teil.