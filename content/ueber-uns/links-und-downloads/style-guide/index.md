---
title: Style Guide

draft: false

aliases:
- /ueber-uns/style-guide

---
# Style Guide
Diese Seite bietet Hinweise und Dateien für einen einfachen und stilsicheren Gebrauch der Text+ Marke. Sie bietet Orientierung für das Erstellen von Projektmaterialien aller Art. Berücksichtigt werden die wesentlichen Gestaltungselemente des Konsortiums – Logo, Farben und Schrift.

## Logo
Wir stellen anderen NFDI-Konsortien, unseren Partnern sowie Kooperationspartnern, die zum Beispiel im Rahmen von Events mit uns zusammenarbeiten, unser Logo zur Nutzung zur Verfügung. Mit dem Download einer Logodatei bestätigen Sie, es gemäß unseren Verwendungsrichtlinien (s.u.) zu nutzen.

Für verschiedenen Verwendungszwecke sind verschiedene Dateiformate sinnvoll. Die einschlägigen sind hier gelistet. Wenn Sie nicht wissen, welches Dateiformat das richtige ist, nehmen Sie das erste: PNG. Sollten Sie darüber hinaus Bedarfe haben, [kontaktieren Sie uns gerne über den Helpdesk](https://text-plus.org/helpdesk/#kontaktformular).

{{<cards>}}
{{<card image="payload/textplus_logo_RGB.png" width="small" title="Logo.PNG" download="ueber-uns/links-und-downloads/style-guide/payload/textplus_logo_RGB.png">}}
{{</card>}}

{{<card image="payload/textplus_logo_RGB.png" width="small" title="Logo.SVG" download="/ueber-uns/links-und-downloads/style-guide/payload/textplus_logo_RGB.svg">}}
{{</card>}}

{{<card image="payload/textplus_logo_RGB.png" width="small" title="Logo.AI" download="/ueber-uns/links-und-downloads/style-guide/payload/textplus_logo_RGB.ai">}}
{{</card>}}

{{<card image="payload/textplus_logo_RGB.png" width="small" title="Logo.AI (CMYK)" download="/ueber-uns/links-und-downloads/style-guide/payload/textplus_logo_CMYK.ai">}}
{{</card>}}

{{<card image="payload/textplus_logo_RGB.png" width="small" title="Logo.EPS (CMYK)" download="/ueber-uns/links-und-downloads/style-guide/payload/textplus_logo_CMYK.eps">}}
{{</card>}}

{{</cards>}}

### Favicon
Bei sehr kleiner Darstellung (<40px bzw. 9mm) kann alternativ zum Logo das Favicon genutzt werden. Ein Verwenden des Favicons für größere Darstellung ist nicht zulässig. 

{{<cards>}}
{{<card image="payload/Logo_vs_Favicon.png" title="kleine Darstellung (<40px bzw. 9mm)">}}
{{</card>}}
{{</cards>}}

{{<cards>}}
{{<card image="payload/textplus_favicon_RGB.png" width="small" title="Favicon.PNG" download="ueber-uns/links-und-downloads/style-guide/payload/textplus_favicon_RGB.png">}}
{{</card>}}

{{<card image="payload/textplus_favicon_RGB.png" width="small" title="Favicon.SVG" download="ueber-uns/links-und-downloads/style-guide/payload/textplus_favicon_RGB.svg">}}
{{</card>}}

{{<card image="payload/textplus_favicon_RGB.png" width="small" title="Favicon.AI" download="ueber-uns/links-und-downloads/style-guide/payload/textplus_favicon_RGB.ai">}}
{{</card>}}

{{<card image="payload/textplus_favicon_RGB.png" width="small" title="Favicon.ICO" download="ueber-uns/links-und-downloads/style-guide/payload/favicon.ico">}}
{{</card>}}

{{</cards>}}

### Verwendungsrichtlinien
Das Logo von Text+ besteht aus einem Icon. Es darf in keiner Weise verändert werden und ist freistehend, nicht gedreht horizontal vor einem ruhigen Hintergrund zu platzieren. Es ist ein Abstand von mind. 1/2 Logobreite zu anderen Logos einzuhalten. Bei farbigen und/oder unruhigen Hintergründen ist auf einen ausreichenden Kontrast zu achten.

{{<cards>}}
{{<card image="payload/Logo_Schutzbereich.png" width="small" title="Schutzbereich">}}
{{</card>}}

{{<card image="payload/Logo_Verwendung1.png" width="small" title="neutraler Hintergrund">}}
{{</card>}}

{{<card image="payload/Logo_Verwendung2.png" width="small" title="Textfarbe verändert">}}
{{</card>}}

{{<card image="payload/Logo_Verwendung3.png" width="small" title="unzulässiger Hintergrund">}}
{{</card>}}

{{<card image="payload/Logo_Verwendung4.png" width="small" title="zulässiger Hintergrund">}}
{{</card>}}

{{<card image="payload/Logo_Verwendung5.png" width="small" title="Logo gestaucht und gedreht">}}
{{</card>}}

{{</cards>}}


## Typographie
Text+ verwendet die Schriftart [Roboto Flex](https://fonts.google.com/specimen/Roboto+Flex). Sie steht unter der [Open Font License](https://openfontlicense.org/) und kann damit weitgehend frei verwendet werden.

Als sogenannter [Variable Font](https://fonts.google.com/knowledge/glossary/variable_fonts) enthält die Roboto Flex überaus weite Möglichkeiten, Schnitt, Stärke und Laufweite zu verändern. Text+ nutzt hauptsächlich die Attribute „weight“, „size“ und „line height“. Weiterhin wird bei Überschriften das Attribut „width“ und „letter spacing“ verändert. Hier im Detail:

### Überschriften Ebene 1
**font-size** 2.7368rem | 32.84pt  
**line-height** 3.575rem | 42.9pt  
**letter-spacing** 0.005em | 0.1pt  
**weight** 700 | bold  
**width** 87.5

### Überschriften Ebene 2
**font-size** 2.074rem | 24.89pt  
**line-height** 2.69rem | 32.3pt  
**letter-spacing** 0.005em | 0.1pt  
**weight** 700 | bold  
**width** 87.5

### Überschriften Ebene 3
**font-size** 1.728rem | 20.74pt  
**line-height** 2.25rem | 27pt  
**letter-spacing** 0.005em | 0.1pt  
**weight** 700 | bold  
**width** 87.5

### Lead
**font-size** 1.45rem | 17.4pt  
**line-height** 2.25rem | 27pt  
**weight** 300 | light  

### Lauftext
**font-size** 1rem | 12pt  
**line-height** 1.75rem | 21pt  
**weight** 404 | regular  

Weitere typographische Hinweise im Mini-Styleguide.

## Farben und gestalterische Elemente
Text+ verwendet verschiedene, aufeinander abgestimmte Farben zur Kennzeichnung der Arbeitsbereiche oder zur Hervorhebung von Informationen. Als primär digitale Infrastruktur sind die Farbwerte in Hex bzw. RGB definiert; CMYK ist abgeleitet. Es sollen keine anderen als die hier aufgeführten Farbwerte verwendet werden.

### Hauptfarben

{{<cards>}}

{{<card image="payload/color-primary.png" width="small" title="Primärfarbe">}}
#CD0022  
RGB 205,0,34    
c12 m100 y89 k4
{{</card>}}

{{<card image="payload/color-secondary.png" width="small" title="Sekundärfarbe">}}
#245787  
RGB 36,87,135    
c91 m63 y23 k8
{{</card>}}
{{</cards>}}

Die rote Primärfarbe ist sparsam zu verwenden. Die blaue Sekundärfarbe ist ruhiger und kann auch für größere Flächen etc. genutzt werden.

### Farben der Arbeitsbereiche

{{<cards>}}

{{<card image="payload/color-collections.png" width="small" title="Collections">}}
#29A900  
RGB 41,169,0    
c77 m0 y100 k0
{{</card>}}

{{<card image="payload/color-lr.png" width="small" title="Lexikalische Ressourcen">}}
#E77E00  
RGB 231,126,0  
c5 m59 y100 k0
{{</card>}}

{{<card image="payload/color-editions.png" width="small" title="Editionen">}}
#00A8CC 
RGB 0,168,204  
c75 m11 y15 k0
{{</card>}}

{{<card image="payload/color-io.png" width="small" title="Infrastruktur/ Betrieb">}}
#AB86F0  
RGB 171,134,240  
c47 m51 y0 k0
{{</card>}}

{{</cards>}}

### Schriftfarbe und Alternativhintergrund

{{<cards>}}

{{<card image="payload/color-dark.png" width="small" title="Textfarbe">}}
#212529  
RGB 33,37,41  
c0 m0 y0 k100
{{</card>}}

{{<card image="payload/color-alternate-background.png" width="small" title="Alternativ">}}
#EDEDED  
RGB 237,237,237  
c0 m0 y0 k7
{{</card>}}
{{</cards>}}

Text ist bei Bildschirmdarstellung nicht schwarz (#ffffff), sondern ein sehr dunkles Grau mit leichtem Überschuss an Grün und Blau (#212529). Für Druckdarstellung (z.B. Office-Dokumente etc.) ist reines Schwarz und für Druckproduktion CMYK mit K=100 zu wählen. Der Alternativhintergrund hebt sonst weiße Flächen zur Kontrastierung hervor.

### Styleguide zum Download (Stand 08/2023)
Einen [Mini-Styleguide als PDF zum Download befindet sich hier](ueber-uns/links-und-downloads/style-guide/payload/Text-plus_Styleguide_1.4.pdf).

## Icons
Diese Icons werden verwendet, um Angebote und Informationen aus Text+ zu kennzeichnen und schnell visuell darzustellen.

{{<cards>}}

{{<card image="payload/Icon_png_2000x2000/Blog_Icon_2000x2000.png" width="small" title="Blog">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Blog_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Blog_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Blog_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Community_Activities_Icon_2000x2000.png" width="small" title="Community Activities">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Community_Activities_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Community_Activities_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Community_Activities_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Consulting_Icon_2000x2000.png" width="small" title="Consulting">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Consulting_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Consulting_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Consulting_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Data_Depositing_Icon_2000x2000.png" width="small" title="Data Depositing">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Data_Depositing_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Data_Depositing_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Data_Depositing_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Data_Space_Icon_2000x2000.png" width="small" title="Data Space">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Data_Space_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Data_Space_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Data_Space_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Federated_Content_Search_Icon_2000x2000.png" width="small" title="Federated Content Search">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Federated_Content_Search_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Federated_Content_Search_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Federated_Content_Search_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/GND_Agentur_Icon_2000x2000.png" width="small" title="GND Agentur">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/GND_Agentur_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/GND_Agentur_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/GND_Agentur_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Helpdesk_Icon_2000x2000.png" width="small" title="Helpdesk">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Helpdesk_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Helpdesk_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Helpdesk_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Registry_Icon_2000x2000.png" width="small" title="Registry">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Registry_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Registry_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Registry_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Zentrum_Icon_2000x2000.png" width="small" title="Zentrum">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Zentrum_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Zentrum_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Zentrum_Icon.svg)
{{</card>}}

{{</cards>}}

[Download komplettes Iconset PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000.zip)  
[Downloag komplettes Iconset PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64.zip)  
[Download komplettes Iconset SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg.zip)

### Icon-Übersicht
Eine [Icon-Übersicht als PDF zum Download befindet sich hier](ueber-uns/links-und-downloads/style-guide/payload/Textplus_Branches_Icons_RGB.pdf).

## Textbausteine
Zur Beschreibung von Ressourcen und Diensten der Institutionen, die im Rahmen von Text+ entwickelt wurden und/oder betrieben werden bzw. im Rahmen von Text+ (nach)genutzt werden, können je nach Kontext die folgenden Formulierungen verwendet werden:
- XXX ist (auch) Teil von [Text+](/)
- XXX ist aufgenommen / verzeichnet / registriert in der [Text+ Registry](https://registry.text-plus.org/)
- XXX ist aufgenommen / verzeichnet / registriert in der [Text+ Federated Content Search (FCS)](/daten-dienste/suche/#f%C3%B6derierte-inhaltssuche)
- XXX wurde entwickelt von / mit / durch / in [Text+](/)

### Förderhinweis
Um die Projektförderung von Text+ durch die DFG kenntlich zu machen, kann der nachfolgende Vorschlag verwendet werden: 

{{<cards>}}
{{<card image="payload/textplus_logo_RGB.png">}}
Text+ wird gefördert durch die [Deutsche Forschungsgemeinschaft (DFG) — 460033370](https://gepris.dfg.de/gepris/projekt/460033370) und ist Teil der [Nationalen Forschungsdateninfrastruktur (NFDI)](https://www.nfdi.de/).
{{</card>}}
{{</cards>}}

Bei Nicht-Exklusivität sollte stattdessen folgender Zusatz verwendet werden: „Unterstützt durch das NFDI-Konsortium Text+. Text+ wird gefördert durch die [Deutsche Forschungsgemeinschaft (DFG) — 460033370](https://gepris.dfg.de/gepris/projekt/460033370) und ist Teil der [Nationalen Forschungsdateninfrastruktur (NFDI)](https://www.nfdi.de/)“.


### Logos der DFG und der NFDI
Die [Deutsche Forschungsgemeinschaft](https://www.dfg.de/de/service/logo-corporate-design) und die [Nationale Forschungsdateninfrastruktur](https://www.nfdi.de/downloads/) stellen auf ihren Seiten Logos und Erläuterungen zum jeweiligen Corporate Design bereit. Es gelten die jeweiligen Verwendungsrichtlinien.



### Changelog
* 09/2024: Abschnitt „Textbausteine“ ergänzt
* 08/2024: „Lead“ Typographie ergänzt
* 08/2024: Passus zum Förderhinweis ergänzt
* 07/2024: Erste Online-Fassung des Style Guides mit Logo, Verwendungsrichtlinien sowie Farbpalette und typographischen Hinweisen
* 08/2023: Mini-Styleguide als PDF
