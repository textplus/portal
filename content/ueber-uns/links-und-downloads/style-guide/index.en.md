---
title: Style Guide

aliases:
- /about-us/style-guide

---

# Style Guide
This page provides guidelines and files for easy and stylish use of the Text+ brand. It offers guidance for creating project materials of all kinds. The essential design elements of the consortium – logo, colors, and font – are taken into account.

## Logo
We make our logo available to other NFDI consortia, our partners, and cooperation partners who work with us in the context of events, for example. By downloading a logo file, you confirm to use it according to our usage guidelines (see below).

Different file formats are useful for different purposes. The relevant ones are listed here. If you do not know which file format is the right one, use the first: PNG. If you have further needs, [feel free to contact us through the Helpdesk](https://text-plus.org/en/helpdesk/#contact-form).

{{<cards>}}
{{<card image="payload/textplus_logo_RGB.png" width="small" title="Logo.PNG" download="ueber-uns/links-und-downloads/style-guide/payload/textplus_logo_RGB.png">}}
{{</card>}}

{{<card image="payload/textplus_logo_RGB.png" width="small" title="Logo.SVG" download="/ueber-uns/links-und-downloads/style-guide/payload/textplus_logo_RGB.svg">}}
{{</card>}}

{{<card image="payload/textplus_logo_RGB.png" width="small" title="Logo.AI" download="/ueber-uns/links-und-downloads/style-guide/payload/textplus_logo_RGB.ai">}}
{{</card>}}

{{<card image="payload/textplus_logo_RGB.png" width="small" title="Logo.AI (CMYK)" download="/ueber-uns/links-und-downloads/style-guide/payload/textplus_logo_CMYK.ai">}}
{{</card>}}

{{<card image="payload/textplus_logo_RGB.png" width="small" title="Logo.EPS (CMYK)" download="/ueber-uns/links-und-downloads/style-guide/payload/textplus_logo_CMYK.eps">}}
{{</card>}}

{{</cards>}}

### Favicon
For very small displays (<40px or 9mm), the favicon can be used as an alternative to the logo. Using the favicon for larger displays is not allowed.

{{<cards>}}
{{<card image="payload/Logo_vs_Favicon.png" title="small display (<40px or 9mm)">}}
{{</card>}}
{{</cards>}}

{{<cards>}}
{{<card image="payload/textplus_favicon_RGB.png" width="small" title="Favicon.PNG" download="ueber-uns/links-und-downloads/style-guide/payload/textplus_favicon_RGB.png">}}
{{</card>}}

{{<card image="payload/textplus_favicon_RGB.png" width="small" title="Favicon.SVG" download="ueber-uns/links-und-downloads/style-guide/payload/textplus_favicon_RGB.svg">}}
{{</card>}}

{{<card image="payload/textplus_favicon_RGB.png" width="small" title="Favicon.AI" download="ueber-uns/links-und-downloads/style-guide/payload/textplus_favicon_RGB.ai">}}
{{</card>}}

{{<card image="payload/textplus_favicon_RGB.png" width="small" title="Favicon.ICO" download="ueber-uns/links-und-downloads/style-guide/payload/favicon.ico">}}
{{</card>}}

{{</cards>}}

### Usage Guidelines
The Text+ logo consists of an icon. It must not be altered in any way and should be placed horizontally against a calm background. A clearspace of at least 1/2 logo width must be maintained from other logos. For colored and/or busy backgrounds, ensure sufficient contrast.

{{<cards>}}
{{<card image="payload/Logo_Schutzbereich.png" width="small" title="clearspace">}}
{{</card>}}

{{<card image="payload/Logo_Verwendung1.png" width="small" title="neutral background">}}
{{</card>}}

{{<card image="payload/Logo_Verwendung2.png" width="small" title="text color changed">}}
{{</card>}}

{{<card image="payload/Logo_Verwendung3.png" width="small" title="impermissible background">}}
{{</card>}}

{{<card image="payload/Logo_Verwendung4.png" width="small" title="permissible background">}}
{{</card>}}

{{<card image="payload/Logo_Verwendung5.png" width="small" title="logo stretched and rotated">}}
{{</card>}}

{{</cards>}}


## Typography
Text+ uses the [Roboto Flex](https://fonts.google.com/specimen/Roboto+Flex) font. It is licensed under the [Open Font License](https://openfontlicense.org/) and can therefore be used freely.

As a [variable font](https://fonts.google.com/knowledge/glossary/variable_fonts), Roboto Flex offers extensive options for adjusting weight, size, and spacing. Text+ primarily uses the attributes "weight", "size", and "line height". Additionally, the attributes "width" and "letter spacing" are adjusted for headings. Here are the details:

### Heading Level 1
**font-size** 2.7368rem | 32.84pt  
**line-height** 3.575rem | 42.9pt  
**letter-spacing** 0.005em | 0.1pt  
**weight** 700 | bold  
**width** 87.5

### Heading Level 2
**font-size** 2.074rem | 24.89pt  
**line-height** 2.69rem | 32.3pt  
**letter-spacing** 0.005em | 0.1pt  
**weight** 700 | bold  
**width** 87.5

### Heading Level 3
**font-size** 1.728rem | 20.74pt  
**line-height** 2.25rem | 27pt  
**letter-spacing** 0.005em | 0.1pt  
**weight** 700 | bold  
**width** 87.5

### Lead
**font-size** 1.45rem | 17.4pt  
**line-height** 2.25rem | 27pt  
**weight** 300 | light  

### Body Text
**font-size** 1rem | 12pt  
**line-height** 1.75rem | 21pt  
**weight** 404 | regular  

Further typographic notes can be found in the mini style guide.

## Colors and Design Elements
Text+ uses various coordinated colors to label work areas or highlight information. As a primarily digital infrastructure, the color values are defined in Hex and RGB; CMYK is derived. Only use the color values listed here.

### Main Colors

{{<cards>}}

{{<card image="payload/color-primary.png" width="small" title="Primary color">}}
#CD0022  
RGB 205,0,34    
c12 m100 y89 k4
{{</card>}}

{{<card image="payload/color-secondary.png" width="small" title="Secondary color">}}
#245787  
RGB 36,87,135    
c91 m63 y23 k8
{{</card>}}
{{</cards>}}

The red primary color should be used sparingly. The blue secondary color is calmer and can also be used for larger areas, etc.

### Task Area Colors

{{<cards>}}

{{<card image="payload/color-collections.png" width="small" title="Collections">}}
#29A900  
RGB 41,169,0    
c77 m0 y100 k0
{{</card>}}

{{<card image="payload/color-lr.png" width="small" title="Lexical Resources">}}
#E77E00  
RGB 231,126,0  
c5 m59 y100 k0
{{</card>}}

{{<card image="payload/color-editions.png" width="small" title="Editions">}}
#00A8CC 
RGB 0,168,204  
c75 m11 y15 k0
{{</card>}}

{{<card image="payload/color-io.png" width="small" title="Infrastructure/ Operations">}}
#AB86F0  
RGB 171,134,240  
c47 m51 y0 k0
{{</card>}}

{{</cards>}}

### Text Color and Alternative Background

{{<cards>}}

{{<card image="payload/color-dark.png" width="small" title="Text color">}}
#212529  
RGB 33,37,41  
c0 m0 y0 k100
{{</card>}}

{{<card image="payload/color-alternate-background.png" width="small" title="Alternative">}}
#EDEDED  
RGB 237,237,237  
c0 m0 y0 k7
{{</card>}}
{{</cards>}}

On screens, text is not black (#ffffff), but a very dark gray with a slight excess of green and blue (#212529). For standard print display (e.g. Office documents, etc.), choose pure black, and for offset print production, CMYK with K=100. The alternative background highlights otherwise white areas for contrast.

### Style Guide for Download (as of 08/2023)
A [mini style guide as a PDF can be downloaded here](ueber-uns/links-und-downloads/style-guide/payload/Text-plus_Styleguide_1.4.pdf).

## Icons
Use these icons to mark and visually represent services and information from Text+.

{{<cards>}}

{{<card image="payload/Icon_png_2000x2000/Blog_Icon_2000x2000.png" width="small" title="Blog">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Blog_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Blog_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Blog_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Community_Activities_Icon_2000x2000.png" width="small" title="Community Activities">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Community_Activities_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Community_Activities_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Community_Activities_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Consulting_Icon_2000x2000.png" width="small" title="Consulting">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Consulting_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Consulting_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Consulting_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Data_Depositing_Icon_2000x2000.png" width="small" title="Data Depositing">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Data_Depositing_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Data_Depositing_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Data_Depositing_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Data_Space_Icon_2000x2000.png" width="small" title="Data Space">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Data_Space_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Data_Space_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Data_Space_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Federated_Content_Search_Icon_2000x2000.png" width="small" title="Federated Content Search">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Federated_Content_Search_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Federated_Content_Search_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Federated_Content_Search_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/GND_Agentur_Icon_2000x2000.png" width="small" title="GND Agency">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/GND_Agentur_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/GND_Agentur_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/GND_Agentur_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Helpdesk_Icon_2000x2000.png" width="small" title="Helpdesk">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Helpdesk_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Helpdesk_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Helpdesk_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Registry_Icon_2000x2000.png" width="small" title="Registry">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Registry_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Registry_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Registry_Icon.svg)
{{</card>}}

{{<card image="payload/Icon_png_2000x2000/Zentrum_Icon_2000x2000.png" width="small" title="Center">}}
[PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000/Zentrum_Icon_2000x2000.png)  
[PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64/Zentrum_Icon_64x64.png)  
[SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg/Zentrum_Icon.svg)
{{</card>}}

{{</cards>}}

[Download set of icons PNG 2000px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_2000x2000.zip)  
[Downloag set of icons PNG 64px](ueber-uns/links-und-downloads/style-guide/payload/Icon_png_64x64.zip)  
[Download set of icons SVG](ueber-uns/links-und-downloads/style-guide/payload/Icon_svg.zip)

### Icon Overview
[Here is an overview document (PDF) of all icons](ueber-uns/links-und-downloads/style-guide/payload/Textplus_Branches_Icons_RGB.pdf).

## Text modules
To describe the resources and services of the institutions developed and/or operated within Text+ or (re)used within the framework of Text+, the following formulations may be used depending on the context:
- XXX is (also) part of [Text+](/en/)
- XXX is included / listed / registered in the [Text+ Registry](https://registry.text-plus.org/)
- XXX is included / listed / registered in the [Text+ Federated Content Search (FCS)](/en/daten-dienste/suche/#federated-content-search)
- XXX was developed by / with / through / in [Text+](/en/)

### Funding Notice
To acknowledge the project funding of Text+ by the DFG, the following notice can be used:

{{<cards>}}
{{<card image="payload/textplus_logo_RGB.png">}}
Text+ is funded by the [German Research Foundation (DFG) – 460033370](https://gepris.dfg.de/gepris/projekt/460033370) and is part of the [National Research Data Infrastructure (NFDI)](https://www.nfdi.de/downloads/?lang=en).
{{</card>}}
{{</cards>}}

In the case of non-exclusivity, the following addition should be used instead: “Supported by the NFDI consortium Text+. Text+ is funded by the [German Research Foundation (DFG) – 460033370](https://gepris.dfg.de/gepris/projekt/460033370) and is part of the [National Research Data Infrastructure (NFDI)](https://www.nfdi.de/downloads/?lang=en)”.


### Changelog
* 09/2024: Add section “Text modules”
* 08/2024: Add “Lead” typography
* 08/2024: Add notice on funding 
* 07/2024: First online version of the Style Guide with logo, usage guidelines, as well as color palette and typographic notes
* 08/2023: Mini style guide as PDF