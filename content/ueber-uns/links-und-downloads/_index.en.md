---
title: Links and Downloads

menu:
  main:
    weight: 80
    parent: ueber-uns
    
aliases:
- /en/themen-dokumentation/links-und-downloads/

---

# Links and Downloads

## Publications

- [Text+ Community on zenodo.org](https://zenodo.org/communities/textplus_nfdi/)
- [Language- and text-based Research Data Infrastructure (Application Document)](https://doi.org/10.5281/zenodo.6452002)
- [Community Involvement in Research Infrastructures: The User Story Call for Text+](https://doi.org/10.5281/zenodo.5384085)
- [Conference Abstract 2020](https://www.dfg.de/resource/blob/174340/cd946d4500c2eaab0fc031585a349dbf/text-abstract-data.pdf)
[Text+ Presentation at the NFDI Conference 2020](files/Hinrichs_Henrich_Rapp_Stein_Textplus_Forschungsdateninfrastruktur_2020.pdf)

## Style Guide
{{<cards>}}
{{<card image="/ueber-uns/links-und-downloads/style-guide/payload/textplus_logo_RGB.png">}}
[**Logo, Typography, Colors and Design Elements in the Text+ Style Guide**](/ueber-uns/links-und-downloads/style-guide)
{{</card>}}
{{</cards>}}

## Press Releases

- [Official press release from the applying institution IDS Mannheim on the approval of the Text+ consortium, July 2, 2021](https://www.ids-mannheim.de/aktuell/presse/pm-02072021/)
- [Press release from the Joint Science Conference on the funding decision of the second NFDI application round, July 2, 2021](https://www.gwk-bonn.de/fileadmin/Redaktion/Dokumente/Pressemitteilungen/pm2021-04.pdf)

## NFDI

- [NFDI e.V.](https://www.nfdi.de/)
- [NFDI Funding Program on the German Research Foundation (DFG) website](https://www.dfg.de/foerderung/programme/nfdi/)
- [GWK: Joint Federal-State Agreement of 2018](https://www.gwk-bonn.de/fileadmin/Redaktion/Dokumente/Papers/NFDI.pdf)
- [DFG Announcement 2020](https://www.dfg.de/foerderung/info_wissenschaft/2020/info_wissenschaft_20_29/index.html)
- [Consortia Job Openings](https://www.nfdi.de/jobs/)

## Collaboration with other NFDI Consortia

- [Berlin Declaration](https://doi.org/10.5281/zenodo.3457213)
- [Leipzig-Berlin Declaration](https://zenodo.org/record/3895209#.XzvdkegzZdg)
- Workshop Series: [Science-Guided Research Infrastructures for the Humanities and Cultural Sciences in Germany](https://www.forschungsinfrastrukturen.de/doku.php/zusammenfassung-2018-02-15)
- [Memorandum of Understanding](https://zenodo.org/record/4045000#.X3RHSmgzY2w) of the humanities consortia (Version 28.09.2020)
- [Poster for DHd2022 in collaboration with NFDI4Culture](files/NFDI4Culture-Text-Poster-DHd2022.pdf)
