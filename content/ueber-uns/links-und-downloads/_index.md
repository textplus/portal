---
title: Links und Downloads

menu:
  main:
    weight: 80
    parent: ueber-uns 

aliases:
- /themen-dokumentation/links-und-downloads/
---

# Links und Downloads

## Publikationen

- [Text+ Community auf zenodo.org](https://zenodo.org/communities/textplus_nfdi/)
- [Language- and text-based Research Data Infrastructure (Antragsdokument)](https://doi.org/10.5281/zenodo.6452002)
- [Community Involvement in Research Infrastructures: The User Story Call for Text+](https://doi.org/10.5281/zenodo.5384085)
- [NFDI-Konferenz: Abstract 2020](https://www.dfg.de/resource/blob/174340/cd946d4500c2eaab0fc031585a349dbf/text-abstract-data.pdf)
- [Text+ Präsentation bei der NFDI Konferenz 2020](files/Hinrichs_Henrich_Rapp_Stein_Textplus_Forschungsdateninfrastruktur_2020.pdf)


## Style Guide
{{<cards>}}
{{<card image="/ueber-uns/links-und-downloads/style-guide/payload/textplus_logo_RGB.png">}}
[**Logo, Typographie, Farben und gestalterische Elemente im Text+ Style Guide**](/ueber-uns/links-und-downloads/style-guide).
{{</card>}}
{{</cards>}}

## Pressemitteilungen

- [Offizielle Pressemitteilung der antragstellenden Institution IDS Mannheim zur Bewilligung des Verbunds Text+, 2.7.2021](https://www.ids-mannheim.de/aktuell/presse/pm-02072021/)
- [Pressemitteilung der Gemeinsamen Wissenschaftskonferenz zum Förderentscheid der zweiten NFDI-Ausschreibungsrunde, 2.7.2021](https://www.gwk-bonn.de/fileadmin/Redaktion/Dokumente/Pressemitteilungen/pm2021-04.pdf)

## NFDI

- [NFDI e.V.](https://www.nfdi.de/)
- [Förderprogramm NFDI auf den Seiten der Deutschen Forschungsgemeinschaft (DFG)](https://www.dfg.de/foerderung/programme/nfdi/)
- [GWK: Bund-Länder-Vereinbarung von 2018](https://www.gwk-bonn.de/fileadmin/Redaktion/Dokumente/Papers/NFDI.pdf)
- [DFG Ausschreibung 2020](https://www.dfg.de/foerderung/info_wissenschaft/2020/info_wissenschaft_20_29/index.html)
- [Stellenausschreibungen der Konsortien](https://www.nfdi.de/jobs/)

## Zusammenwirken mit anderen NFDI-Konsortien

- [Berlin-Erklärung](https://doi.org/10.5281/zenodo.3457213) 
- [Leipzig-Berlin-Erklärung](https://zenodo.org/record/3895209#.XzvdkegzZdg)
- Workshopreihe: [Wissenschaftsgeleitete Forschungsinfrastrukturen für die Geistes- und Kulturwissenschaften in Deutschland](https://www.forschungsinfrastrukturen.de/doku.php/zusammenfassung-2018-02-15)
- Geisteswissenschaftliches [Memorandum of Understanding](https://zenodo.org/record/4045000#.X3RHSmgzY2w) (Version 28.09.2020)
- [Poster zur DHd2022 gemeinsam mit NFDI4Culture](files/NFDI4Culture-Text-Poster-DHd2022.pdf)
