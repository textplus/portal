---
title: Arbeitsbereiche 

menu:
  main:
    weight: 55
    parent: ueber-uns 
---

# Arbeitsbereiche 

Text+ gliedert sich in verschiedene Arbeitsbereiche. Die Arbeitsbereiche Collections, lexikalische Ressourcen und Editionen stellen die Datendomänen dar, auf die Text+ sich zunächst konzentriert. Die Daten, mit denen diese Datendomänen arbeiten, haben eine lange Tradition in der geisteswissenschaftlichen Forschung. Sie sind mit ausgereiften methodologischen Paradigmen verknüpft, die jeweils charakteristische, aber auch bereichsübergreifende Praktiken der Datenerzeugung, -nutzung, -analyse, -vernetzung und -kuratierung erfordern. Die drei Datendomänen sind außerdem grundlegend für interdisziplinäre Forschungspraktiken der Hermeneutik, Paläographie, Genealogie, Editionsphilologie, Lexikographie und Computerphilologie sowie Computerlinguistik.

Der Name Text+ soll vermitteln, dass sich diese Initiative auf typischerweise textbasierte digitale Forschungsdaten konzentriert, die bzgl. Sprachräumen (auch über Europa hinaus) und Modalitäten von Sprache und Schriftsystemen heterogen sind; das Plus-Zeichen weist darauf hin, dass sprachbasierte Ressourcen auch Ressourcen und Werkzeuge für gesprochene Sprache und für multimodale Daten umfassen.

Neben diesen Datendomänen gibt es den Arbeitsbereich Infrastruktur/Operations und der Bereich der Projektkoordination. Im Bereich Infrastruktur/Operations werden technische Grundlagen behandelt, z. B. Schnittstellen und gemeinsame technische Lösungen. Die Gesamtkoordination adressiert die übergreifenden Aspekte und die Verwaltung des Gesamtprojekts.

## Collections

{{<image img="/ueber-uns/arbeitsbereiche/collections/gfx/Website Collections.jpg" link="/ueber-uns/arbeitsbereiche/collections" license="Hintergrundbild: Pexels (Pixabay), https://pixabay.com/de/users/pexels-2286921"/>}}

{{<button url="/ueber-uns/arbeitsbereiche/collections" is_primary="true">}} Infoseite Collections {{</button>}}

&nbsp;

## Editionen

{{<image img="/ueber-uns/arbeitsbereiche/editionen/gfx/2023-08-30-Website Editionen-editpiaf.png" link="/ueber-uns/arbeitsbereiche/editionen" license="Theodor Fontane: Notizbücher. Digitale genetisch-kritische und kommentierte Edition. Hrsg. von Gabriele Radecke. https://fontane-nb.dariah.eu"/>}}

{{<button url="/ueber-uns/arbeitsbereiche/editionen" is_primary="true">}} Infoseite Editionen {{</button>}}

&nbsp;

## Lexikalische Ressourcen

{{<image img="/ueber-uns/arbeitsbereiche/lexikalische-ressourcen/gfx/Website LR.jpg" link="/ueber-uns/arbeitsbereiche/lexikalische-ressourcen/" license="diannehope14 (Pixabay), https://pixabay.com/de/photos/zusammenarbeit-mitarbeiter-buchen-1106196/"/>}}

{{<button url="/ueber-uns/arbeitsbereiche/lexikalische-ressourcen" is_primary="true">}} Infoseite Lexikalische Ressourcen {{</button>}}

&nbsp;

## Infrastructure/Operations

{{<image img="/ueber-uns/arbeitsbereiche/infrastructure-operations/gfx/Website IO.jpg" link="/ueber-uns/arbeitsbereiche/infrastructure-operations/" license="Generated with Stable Diffusion"/>}}

{{<button url="/ueber-uns/arbeitsbereiche/infrastructure-operations" is_primary="true">}} Infoseite Infrastructure/Operations {{</button>}}

&nbsp;
