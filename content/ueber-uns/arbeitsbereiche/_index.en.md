---
title: Work Areas

menu:
  main:
    weight: 55
    parent: ueber-uns 
---

# Work Areas in Text+ 

Text+ is divided into various task areas. The Collections, Lexical Resources, and Editions task areas represent the data domains that Text+ initially focuses on. The data within these data domains have a long tradition in humanities research and are linked to sophisticated methodological paradigms, each requiring characteristic but also interdisciplinary practices of data generation, utilization, analysis, networking, and curation. These three data domains are also fundamental for interdisciplinary research practices in hermeneutics, palaeography, genealogy, edition philology, lexicography, computer philology, and computer linguistics.

The name *Text+* conveys that this initiative focuses on typically text-based digital research data that are heterogeneous concerning language spaces (also beyond Europe) and modalities of language and writing systems. The plus sign indicates that language-based resources also encompass resources and tools for spoken language and multimodal data.

In addition to these data domains, there are the work areas of Infrastructure/Operations and Project Coordination. The Infrastructure/Operations area covers technical foundations, such as interfaces and shared technical solutions. Overall coordination addresses overarching aspects and the administration of the entire project.


## Collections

{{<image img="/ueber-uns/arbeitsbereiche/collections/gfx/Website Collections.jpg" link="/en/ueber-uns/arbeitsbereiche/collections" license="Hintergrundbild: Pexels (Pixabay), https://pixabay.com/de/users/pexels-2286921"/>}}

{{<button url="/en/ueber-uns/arbeitsbereiche/collections" is_primary="true">}} Infosite Collections {{</button>}}

&nbsp;

## Editions

{{<image img="/ueber-uns/arbeitsbereiche/editionen/gfx/2023-08-30-Website Editionen-editpiaf.png" link="/en/ueber-uns/arbeitsbereiche/editionen" license="Theodor Fontane: Notizbücher. Digitale genetisch-kritische und kommentierte Edition. Hrsg. von Gabriele Radecke. https://fontane-nb.dariah.eu"/>}}

{{<button url="/en/ueber-uns/arbeitsbereiche/editionen" is_primary="true">}} Infosite Editions {{</button>}}

&nbsp;

## Lexical Resources

{{<image img="/ueber-uns/arbeitsbereiche/lexikalische-ressourcen/gfx/Website LR.jpg" link="/en/ueber-uns/arbeitsbereiche/lexikalische-ressourcen/" license="diannehope14 (Pixabay), https://pixabay.com/de/photos/zusammenarbeit-mitarbeiter-buchen-1106196/"/>}}

{{<button url="/en/ueber-uns/arbeitsbereiche/lexikalische-ressourcen" is_primary="true">}} Infosite Lexical Resources {{</button>}}

&nbsp;

## Infrastructure/Operations

{{<image img="/ueber-uns/arbeitsbereiche/infrastructure-operations/gfx/Website IO.jpg" link="/en/ueber-uns/arbeitsbereiche/infrastructure-operations/" license="Generated with Stable Diffusion"/>}}

{{<button url="/en/ueber-uns/arbeitsbereiche/infrastructure-operations" is_primary="true">}} Infosite Infrastructure/Operations {{</button>}}

&nbsp;
