---
title: Editionen
type: non-scrolling-toc

aliases:
- /ueber-uns/editionen/
- /editionen

---

{{<image img="gfx/2023-08-30-Website-Editionen-editpiaf.png" license="Theodor Fontane: Notizbücher. Digitale genetisch-kritische und kommentierte Edition. Hrsg. von Gabriele Radecke. https://fontane-nb.dariah.eu"/>}}

{{<horizontal-line>}}
## Unser Selbstverständnis
Die Task Area Editionen ist ein [Zusammenschluss von 18 Institutionen](/ueber-uns/editionen/#beteiligte-institutionen), die an Text+ beteiligt sind. Sie wird koordiniert durch die [Nordrhein-Westfälische Akademie der Wissenschaften und der Künste](https://www.awk.nrw/akademie/koordinierungsstelle-digital-humanities). 

Wir entwickeln eine Infrastruktur aus Diensten für eine Community von Forschenden, Lehrenden und Lernenden, in deren Forschungsfeld und -praxis wissenschaftliche Editionen eine wesentliche Rolle spielen. 

Die Task Area Editionen versteht sich zugleich als Interessenvertretung für diese Community beim Aufbau der Nationalen Forschungsdateninfrastruktur (NFDI), die zu einem Hub vernetzter Services unter dem Leitbild der FAIR-Prinzipien werden soll. Wir fungieren als Multiplikatorin und als offene Plattform, die auf die Mitwirkung aller Akteur:innen im Umfeld wissenschaftlicher Editionen angewiesen ist. Daher laden wir herzlich dazu ein, folgende Angebote mitzugestalten, die im Rahmen unseres Arbeitsprogramms entstehen.

{{<accordion>}}
  {{<accordion-item title="Interesse geweckt?">}}
  **Unser Onboarding Guide zeigt konkrete Optionen der Zusammenarbeit und Mitbestimmung auf:**

  Speer, A., Hensen, K. E., Geißler, N., Kudella, C., Sievers, M., Lemke, K., & König, S. (2024). Onboarding Guide der Task Area Editions (Version v1). Zenodo. [DOI: 10.5281/zenodo.10854729](https://doi.org/10.5281/zenodo.10854729) 
  {{</accordion-item>}}
{{</accordion>}}

{{<horizontal-line>}}
## Offenes Editionsverzeichnis der Text\+ Registry
Die Task Area Editionen entwickelt ein offenes Verzeichnis für gedruckte, hybride und digitale Editionen und Editionsprojekte sowie deren Tools – auf Basis standardisierter Beschreibungen und unter Einbezug von Tutorials. 
Im Fokus stehen die Interoperabilität mit bestehenden Verzeichnissen und Katalogen sowie die Bereitstellung von Schnittstellen zur Vernetzung mit einschlägigen Informationssystemen und Infrastrukturen.

Das Verzeichnis speist sich aus verschiedenen Quellen: 
- Datenbanken von Förderinstitutionen, bibliothekarische Nachweissysteme, bestehende Kataloge, Informationen von Fachinformationsdiensten.
- Nutzende haben zudem die Möglichkeit, Editionen über ein Formular einzugeben und bestehende Katalogeinträge zu ergänzen oder zu korrigieren.
- Die Task Area Editionen organisiert hierfür offene Edit-a-thons, im Zuge derer Editionen mit Live-Support eingetragen werden können.

Das offene Editionsverzeichnis ist Teil der übergreifenden Text\+ Registry, die Ressourcen der verschiedenen Datendomänen miteinander vernetzt. 

{{<button url="https://registry.text-plus.org/" is_primary="true">}} Zur Text+ Registry {{</button>}}

&nbsp;

{{<accordion>}}
  {{<accordion-item title="mehr Infos zur Text+ Registry">}}
  **Folgende Publikation bietet eine konzeptionelle und technische Gesamtdarstellung:**
  - Gradl, T., Schulz, D. et al. Towards a Registry for Digital Resources – The Text\+ Registry for Editions. Datenbank Spektrum (2024). [DOI: 10.1007/s13222-024-00479-0](https://doi.org/10.1007/s13222-024-00479-0)
  - Für (gemeinsame) Edit-a-thons sowie Anregungen und Kritik wenden Sie sich gern an unseren [Text\+ Helpdesk](/helpdesk).
  {{</accordion-item>}}
{{</accordion>}}

{{<horizontal-line>}}
## Consulting
Die Task Area Editionen etabliert das [Text\+ Consulting](daten-dienste/consulting) auf Basis einer koordinierten, persönlichen und vertraulichen Beratung.

Wir beraten zu allen Themen rund um Editionen – von der Antragsberatung über allgemeine Fragen zum Forschungsdatenmanagement (FDM), von der Diskussion von Forschungsideen und -prozessen bis hin zu editorischen Spezialfragen über den gesamten Datenlebenszyklus hinweg. Und wenn wir allein nicht weiterkommen, verbinden wir uns mit Kolleg:innen von [NFDI4Culture](https://nfdi4culture.de/helpdesk.html), [NFDI4Objects](https://www.nfdi4objects.net/index.php/en/help-desk) und [NFDI4Memory](https://4memory.de/).

{{<button url="/daten-dienste/consulting/" is_primary="true">}} Zum Text+ Consulting {{</button>}}

&nbsp;

{{<accordion>}}
  {{<accordion-item title="Unterstützung benötigt?">}}
  - [Text\+ Helpdesk](/helpdesk) 
  - [Text\+ Research Rendezvous](https://events.gwdg.de/category/208/)
  - [Text\+ Consultingkonzept](daten-dienste/consulting)
  {{</accordion-item>}}
{{</accordion>}}

{{<horizontal-line>}}
##  Community Activities
Die Task Area Editionen organisiert Workshops, Informations- und Vernetzungsveranstaltungen sowie Schulungen. Außerdem erarbeiten wir curriculare Empfehlungen, Tutorials und Handreichungen zu Standards wie den FAIR-Prinzipien sowie zu allen Phasen des Edierens und zu allen Datenschichten einer Edition.

{{<accordion>}}
  {{<accordion-item title="Auswahl an Aktivitäten">}}
  - Im [Text\+ Blog](https://textplus.hypotheses.org/) dokumentieren wir Highlights aus unserem Veranstaltungsprogramm.
  - [Link zur Veranstaltungsübersicht](aktuelles/veranstaltungen)

  - **Für Kooperationsanfragen oder gemeinsame Veranstaltungen wenden Sie sich gern an den [Text\+ Helpdesk](/helpdesk).** 
  {{</accordion-item>}}
{{</accordion>}}
   
{{<horizontal-line>}}

{{<team name="Kolleginnen und Kollegen der TA Editions">}}
{{<team-member img="gfx/Martin-Sievers.jpg" url="https://www.adwmainz.de/mitarbeiterinnen/profil/dipl-math-martin-sievers.html" institution="Akademie der Wissenschaften und der Literatur | Mainz">}}Martin Sievers{{</team-member>}}
{{<team-member img="" url="https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/czmiel-alexander" institution="Berlin-Brandenburgische Akademie der Wissenschaften">}}Alexander Czmiel{{</team-member>}}
{{<team-member img="" url="https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/lemke-karoline" institution="Berlin-Brandenburgische Akademie der Wissenschaften">}}Karoline Lemke{{</team-member>}}
{{<team-member img="" url="" institution="Niedersächsische Akademie der Wissenschaften zu Göttingen">}}Jörg Wettlaufer{{</team-member>}}
{{<team-member img="gfx/Sandra-Koenig.jpg" url="https://www.leopoldina.org/fileadmin/redaktion/Ueber_uns/ZfW/2023_CV_Koenig_Sandra.pdf" institution="Deutsche Akademie der Naturforscher Leopoldina – Nationale Akademie der Wissenschaften">}}Sandra König{{</team-member>}}
{{<team-member img="gfx/Daniela-Schulz.jpg" url="https://www.hab.de/author/danielaschulz/" institution="Herzog August Bibliothek Wolfenbüttel">}}Daniela Schulz{{</team-member>}}
{{<team-member img="" url="" institution="Klassik Stiftung Weimar">}}Gerrit Brüning{{</team-member>}}
{{<team-member img="gfx/Annika-Wienert.jpg" url="" institution="Max Weber Stiftung – Deutsche Geisteswissenschaftliche Institute im Ausland">}}Annika Wienert{{</team-member>}}
{{<team-member img="" url="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/melina-leonie-jander/" institution="Niedersächsische Staats- und Universitätsbibliothek Göttingen">}}Melina Jander{{</team-member>}}
{{<team-member img="gfx/Christoph-Kudella.png" url="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/christoph-kudella/" institution="Niedersächsische Staats- und Universitätsbibliothek Göttingen">}}Christoph Kudella{{</team-member>}}
{{<team-member img="gfx/Lukas-Weimer.jpg" url="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/lukas-weimer/" institution="Niedersächsische Staats- und Universitätsbibliothek Göttingen">}}Lukas Weimer{{</team-member>}}
{{<team-member img="gfx/Jonathan-Blumtritt.png" url="https://cceh.uni-koeln.de/personen/jonathan-blumtritt/" institution="Nordrhein-Westfälische Akademie der Wissenschaften und der Künste">}}Jonathan Blumtritt{{</team-member>}}
{{<team-member img="gfx/Elisa-Cugliana.png" url="https://cceh.uni-koeln.de/personen/elisa-cugliana/" institution="Nordrhein-Westfälische Akademie der Wissenschaften und der Künste">}}Elisa Cugliana{{</team-member>}}
{{<team-member img="gfx/Nils-Geissler.png" url="https://cceh.uni-koeln.de/personen/nils-geissler/" institution="Nordrhein-Westfälische Akademie der Wissenschaften und der Künste">}}Nils Geißler{{</team-member>}}
{{<team-member img="gfx/Tessa-Gengnagel.png" url="https://cceh.uni-koeln.de/personen/tessa-gengnagel/" institution="Nordrhein-Westfälische Akademie der Wissenschaften und der Künste">}}Tessa Gengnagel{{</team-member>}}
{{<team-member img="gfx/Kilian-Erasmus.png" url="https://cceh.uni-koeln.de/personen/kilian-hensen/" role="Koordination TA Editionen" institution="Nordrhein-Westfälische Akademie der Wissenschaften und der Künste">}}Kilian Hensen{{</team-member>}}
{{<team-member img="gfx/Aleksander-Marcic.jpg" url="https://cceh.uni-koeln.de/personen/aleksander-marcic/" institution="Nordrhein-Westfälische Akademie der Wissenschaften und der Künste">}}Aleksander Marčić{{</team-member>}}
{{<team-member img="gfx/Claes-Neuefeind.png" url="https://cceh.uni-koeln.de/personen/claes-neuefeind/" institution="Nordrhein-Westfälische Akademie der Wissenschaften und der Künste">}}Claes Neuefeind{{</team-member>}}
{{<team-member img="gfx/Andreas-Speer.jpg" url="https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/andreas-speer" role="Sprecher TA Editionen" institution="Nordrhein-Westfälische Akademie der Wissenschaften und der Künste">}}Andreas Speer{{</team-member>}}
{{<team-member img="" url="" institution="Salomon Ludwig Steinheim-Institut für deutsch-jüdische Geschichte an der Universität Duisburg-Essen">}}Harald Lordick{{</team-member>}}
{{<team-member img="gfx/Fernanda-Freires.jpg" url="" institution="Technische Universität Darmstadt">}}Fernanda Alvares Freire{{</team-member>}}
{{<team-member img="" url="" institution="Technische Universität Darmstadt">}}Andrea Rapp{{</team-member>}}
{{<team-member img="" url="" institution="University and State Library Darmstadt">}}Kevin Wunsch{{</team-member>}}
{{<team-member img="gfx/Edit-new.png" role="Maskottchen" url="/ueber-uns/editionen" institution="Text+ TA Editionen">}}Edit Piaf{{</team-member>}}
{{</team>}}

{{<horizontal-line>}}
## Beteiligte Institutionen
{{<accordion>}}
  {{<accordion-item title="Akademie der Wissenschaften und der Literatur | Mainz">}}  
  - Martin Sievers (2024, 4. März). Partner-Parade #03: Akademie der Wissenschaften und der Literatur | Mainz. Text+ Blog. [https://doi.org/10.58079/vy9l](https://doi.org/10.58079/vy9l)
  - [Link zur AWLM](https://www.adwmainz.de/)
  {{</accordion-item>}}
    
  {{<accordion-item title="Berlin-Brandenburgische Akademie der Wissenschaften">}}
  - [Link zur BBAW](https://www.bbaw.de/)
  {{</accordion-item>}}

  {{<accordion-item title="Niedersächsische Akademie der Wissenschaften zu Göttingen">}}
  - [Link zur NAWG](https://adw-goe.de/)
  {{</accordion-item>}}

  {{<accordion-item title="Deutsche Akademie der Naturforscher Leopoldina – Nationale Akademie der Wissenschaften">}}
  - [Link zur Leopoldina](https://www.leopoldina.org/)
  {{</accordion-item>}}

  {{<accordion-item title="Heidelberger Akademie der Wissenschaften">}}
  **Text\+ Kooperationsprojekt 2023: „edition2LD“**
  
  - Text\+ Gast (2023, 18. Dezember). Text+Plus, #04: Modellierung von Texteditionen als Linked Data (edition2LD). Text\+ Blog. [https://doi.org/10.58079/vfb4](https://doi.org/10.58079/vfb4) 
  - [Link zur HAdW](https://www.hadw-bw.de/)
  {{</accordion-item>}}
    
  {{<accordion-item title="Herzog August Bibliothek Wolfenbüttel">}}
  - Daniela Schulz (2024, 27. Juni). Partner-Parade #04: Bibliosibirsk und die NFDI – Zur Rolle der HAB in den geisteswissenschaftlichen Konsortien. Text\+ Blog. [https://doi.org/10.58079/11w94](https://doi.org/10.58079/11w94)
  - [Link zur HAB](https://www.hab.de/)
  {{</accordion-item>}}
  
  {{<accordion-item title="Hochschule Darmstadt">}}
  - [Link zur h_da](https://h-da.de/)
  {{</accordion-item>}}

  {{<accordion-item title="Klassik Stiftung Weimar">}}
  - [Link zur KSW](https://www.klassik-stiftung.de/)
  {{</accordion-item>}}
  
  {{<accordion-item title="Leibniz-Institut für Europäische Geschichte">}}
  **Text\+ Kooperationsprojekt 2023: „FriVer+“**
  
  - Jaap Geraerts (2023, 29. September). Text+Plus, #01: From Treaty to FAIR Data: FriVer+. Text+ Blog. [https://doi.org/10.58079/uqhb](https://doi.org/10.58079/uqhb) 
  - [Link zum IEG](https://www.ieg-mainz.de/)
  {{</accordion-item>}}

  {{<accordion-item title="Max Weber Stiftung – Deutsche Geisteswissenschaftliche Institute im Ausland">}}
  - [Link zur MWS](https://www.maxweberstiftung.de/)
  {{</accordion-item>}}
  
  {{<accordion-item title="Niedersächsische Staats- und Universitätsbibliothek Göttingen">}}
  - [Link zur SUB](https://www.sub.uni-goettingen.de/)
  {{</accordion-item>}}
  
  {{<accordion-item title="Nordrhein-Westfälische Akademie der Wissenschaften und der Künste">}}
  - [Link zur AWK NRW](https://www.awk.nrw/)
  {{</accordion-item>}}
  
  {{<accordion-item title="Salomon Ludwig Steinheim-Institut für deutsch-jüdische Geschichte an der Universität Duisburg-Essen">}}
  - [Link zum STI](http://www.steinheim-institut.de/)
  {{</accordion-item>}}

  {{<accordion-item title="Technische Universität Darmstadt">}}
  - [Link zur TUDa](https://www.tu-darmstadt.de/)
  {{</accordion-item>}}
  
  {{<accordion-item title="Universität Paderborn">}}
  - [Link zur UPB](https://www.uni-paderborn.de/)
  {{</accordion-item>}}

  {{<accordion-item title="Universität Rostock">}}
  **Text\+ Kooperationsprojekt 2023: „Pessoa digital“**
  
  - Henny-Krahmer, U. (2023). Pessoa digital. Zenodo. [https://doi.org/10.5281/zenodo.10257440](https://doi.org/10.5281/zenodo.10257440)
  - [Link zur Universität Rostock](https://www.uni-rostock.de/)
  {{</accordion-item>}}
  
  {{<accordion-item title="Universitäts- und Landesbibliothek Darmstadt">}}
  - [Link zur ULBDa](https://www.ulb.tu-darmstadt.de/)
  {{</accordion-item>}}
    
  {{<accordion-item title="Bergische Universität Wuppertal">}}
  **Text\+ Kooperationsprojekt 2024: „RedBook1305“**

  - Text\+ Blog-Redaktion (2023, 4. Oktober). Bekanntgabe der neuen Kooperationsprojekte von Text+. Text+ Blog. [https://doi.org/10.58079/uqhc](https://doi.org/10.58079/uqhc)
  - [Link zur BUW](https://www.uni-wuppertal.de/de/)
  {{</accordion-item>}}
{{</accordion>}}
