---
title: Editions
type: non-scrolling-toc

aliases:
- /en/ueber-uns/editionen/
- /en/editionen
---

{{<image img="gfx/2023-08-30-Website-Editionen-editpiaf.png" license="Theodor Fontane: Notizbücher. Digitale genetisch-kritische und kommentierte Edition. Hrsg. von Gabriele Radecke. https://fontane-nb.dariah.eu"/>}}

{{<horizontal-line>}}
## How we see ourselves
The Task Area Editions is an [association of 18 institutions](/en/ueber-uns/editionen/#participating-institutions) that are involved in Text\+. It is coordinated by the [North Rhine-Westphalian Academy of Sciences, Humanities and the Arts](https://www.awk.nrw/akademie/koordinierungsstelle-digital-humanities). 

We are developing an infrastructure of services for a community of researchers, scholars and students in whose research field and practice scholarly editions play an essential role. 

The Task Area Editions also views itself as an advocate for this community in the development of the National Research Data Infrastructure (NFDI), which is to become a hub of networked services based on the FAIR principles. We act as a multiplier and as an open platform that relies on the participation of all actors in the field of scientific editions. We therefore cordially invite you to help shape the following services that will be created as part of our work programme.

{{<accordion>}}
  {{<accordion-item title="More about us">}}
  **Our onboarding guide presents specific options for collaboration and co-determination:**

  Speer, A., Hensen, K. E., Geißler, N., Kudella, C., Sievers, M., Lemke, K., & König, S. (2024). Onboarding Guide der Task Area Editions (Version v1). Zenodo. [DOI: 10.5281/zenodo.10854729](https://doi.org/10.5281/zenodo.10854729) 
  {{</accordion-item>}}
{{</accordion>}}

{{<horizontal-line>}}
## Open Edition Directory as part of Text\+ Registry
Task Area Editions is developing an open directory for printed, hybrid and digital editions and edition projects as well as their tools - based on standardised descriptions and including tutorials. 
The focus is on interoperability with existing directories and catalogues as well as the provision of interfaces for linking with relevant information systems and infrastructures.

The directory is based on various sources: 
- Databases of funding bodies, library indexing systems, existing catalogues, information from Specialist Information Services (=Fachinformationsdienste).
- Users also have the option of registering editions via a form and adding or correcting existing catalogue entries.
- Task Area Editions organises "edit-a-thons" for this purpose, during which editions can be registered with live support.

The Open Edition Directory is part of the overarching Text\+ Registry, which connects the resources of all partners of Text\+. 

{{<button url="https://registry.text-plus.org/" is_primary="true">}} Text+ Registry {{</button>}}

&nbsp;

{{<accordion>}}
  {{<accordion-item title="More about the Text+ Registry">}}
  **The following publication provides a conceptual and technical overview:**
  - Gradl, T., Schulz, D. et al. Towards a Registry for Digital Resources – The Text\+ Registry for Editions. Datenbank Spektrum (2024). [DOI: 10.1007/s13222-024-00479-0](https://doi.org/10.1007/s13222-024-00479-0)
  - For (joint) edit-a-thons as well as suggestions and criticism, please contact our [Text\+ Helpdesk](/en/helpdesk/).
  {{</accordion-item>}}
{{</accordion>}}

{{<horizontal-line>}}
## Consulting
Task Area Editions establishes the [Text\+ Consulting](/en/daten-dienste/consulting/) as coordinated, personalised and confidential consultancy service.

We provide advice on all topics relating to editions - from consultation on applications to general questions on research data management (RDM), from the discussion of research ideas and processes to specialised editorial questions throughout the entire data life cycle. And if necessary, we liaise with partners from [NFDI4Culture](https://nfdi4culture.de/helpdesk.html), [NFDI4Objects](https://www.nfdi4objects.net/index.php/en/help-desk) and [NFDI4Memory](https://4memory.de/).

{{<button url="/en/daten-dienste/consulting/" is_primary="true">}} Text+ Consulting {{</button>}}

&nbsp;

{{<accordion>}}
  {{<accordion-item title="Support needed?">}}
  - [Text\+ Helpdesk](/en/helpdesk) 
  - [Text\+ Research Rendezvous](https://events.gwdg.de/category/208/)
  - [Text\+ Consultingkonzept](/en/daten-dienste/consulting/)
  {{</accordion-item>}}
{{</accordion>}}

{{<horizontal-line>}}
##  Community Activities
Task Area Editions organises workshops, informative and networking events and training courses. We also develop curricular recommendations, tutorials and handouts on standards such as the FAIR principles as well as on all phases of editing and all data layers of an edition.

{{<accordion>}}
  {{<accordion-item title="Central Activities">}}
  - In the [Text\+ Blog](https://textplus.hypotheses.org/) we document highlights from our event programme.
  - [Event Overview](/en/aktuelles/veranstaltungen/)
  - **For enquiries or joint events, please contact the [Text\+ Helpdesk](/en/helpdesk).** 
  {{</accordion-item>}}
{{</accordion>}}
   
{{<horizontal-line>}}

{{<team name="Colleagues of TA Editions">}}
{{<team-member img="gfx/Martin-Sievers.jpg" url="https://www.adwmainz.de/mitarbeiterinnen/profil/dipl-math-martin-sievers.html" institution="Academy of Sciences and Literature | Mainz">}}Martin Sievers{{</team-member>}}
{{<team-member img="" url="https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/czmiel-alexander" institution="Berlin-Brandenburg Academy of Sciences and Humanities">}}Alexander Czmiel{{</team-member>}}
{{<team-member img="" url="https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/lemke-karoline" institution="Berlin-Brandenburg Academy of Sciences and Humanities">}}Karoline Lemke{{</team-member>}}
{{<team-member img="" url="" institution="Göttingen Academy of Sciences and Humanities in Lower Saxony">}}Jörg Wettlaufer{{</team-member>}}
{{<team-member img="gfx/Sandra-Koenig.jpg" url="https://www.leopoldina.org/fileadmin/redaktion/Ueber_uns/ZfW/2023_CV_Koenig_Sandra.pdf" institution="Deutsche Akademie der Naturforscher Leopoldina – Nationale Akademie der Wissenschaften">}}Sandra König{{</team-member>}}
{{<team-member img="gfx/Daniela-Schulz.jpg" url="https://www.hab.de/author/danielaschulz/" institution="Herzog August Bibliothek Wolfenbüttel">}}Daniela Schulz{{</team-member>}}
{{<team-member img="" url="" institution="Klassik Stiftung Weimar">}}Gerrit Brüning{{</team-member>}}
{{<team-member img="gfx/Annika-Wienert.jpg" url="" institution="Max Weber Foundation – German Humanities Institutes Abroad">}}Annika Wienert{{</team-member>}}
{{<team-member img="" url="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/melina-leonie-jander/" institution="Göttingen State and University Library">}}Melina Jander{{</team-member>}}
{{<team-member img="gfx/Christoph-Kudella.png" url="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/christoph-kudella/" institution="Göttingen State and University Library">}}Christoph Kudella{{</team-member>}}
{{<team-member img="gfx/Lukas-Weimer.jpg" url="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/lukas-weimer/" institution="Niedersächsische Staats- und Universitätsbibliothek Göttingen">}}Lukas Weimer{{</team-member>}}
{{<team-member img="gfx/Jonathan-Blumtritt.png" url="https://cceh.uni-koeln.de/personen/jonathan-blumtritt/" institution="North Rhine-Westphalian Academy of Sciences, Humanities and the Arts">}}Jonathan Blumtritt{{</team-member>}}
{{<team-member img="gfx/Elisa-Cugliana.png" url="https://cceh.uni-koeln.de/personen/elisa-cugliana/" institution="North Rhine-Westphalian Academy of Sciences, Humanities and the Arts">}}Elisa Cugliana{{</team-member>}}
{{<team-member img="gfx/Nils-Geissler.png" url="https://cceh.uni-koeln.de/personen/nils-geissler/" institution="North Rhine-Westphalian Academy of Sciences, Humanities and the Arts">}}Nils Geißler{{</team-member>}}
{{<team-member img="gfx/Tessa-Gengnagel.png" url="https://cceh.uni-koeln.de/personen/tessa-gengnagel/" institution="North Rhine-Westphalian Academy of Sciences, Humanities and the Arts">}}Tessa Gengnagel{{</team-member>}}
{{<team-member img="gfx/Kilian-Erasmus.png" url="https://cceh.uni-koeln.de/personen/kilian-hensen/" role="Koordination TA Editions" institution="North Rhine-Westphalian Academy of Sciences, Humanities and the Arts">}}Kilian Hensen{{</team-member>}}
{{<team-member img="gfx/Aleksander-Marcic.jpg" url="https://cceh.uni-koeln.de/personen/aleksander-marcic/" institution="North Rhine-Westphalian Academy of Sciences, Humanities and the Arts">}}Aleksander Marčić{{</team-member>}}
{{<team-member img="gfx/Claes-Neuefeind.png" url="https://cceh.uni-koeln.de/personen/claes-neuefeind/" institution="North Rhine-Westphalian Academy of Sciences, Humanities and the Arts">}}Claes Neuefeind{{</team-member>}}
{{<team-member img="gfx/Andreas-Speer.jpg" url="https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/andreas-speer" role="Sprecher TA Editions" institution="North Rhine-Westphalian Academy of Sciences, Humanities and the Arts">}}Andreas Speer{{</team-member>}}
{{<team-member img="" url="" institution="Salomon Ludwig Steinheim Institute of German-Jewish Studies at the University of Duisburg-Essen">}}Harald Lordick{{</team-member>}}
{{<team-member img="gfx/Fernanda-Freires.jpg" url="" institution="Technical University of Darmstadt">}}Fernanda Alvares Freire{{</team-member>}}
{{<team-member img="" url="" institution="Technical University of Darmstadt">}}Andrea Rapp{{</team-member>}}
{{<team-member img="" url="" institution="University and State Library Darmstadt">}}Kevin Wunsch{{</team-member>}}
{{<team-member img="gfx/Edit-new.png" role="Mascot" url="/en/ueber-uns/editionen/" institution="Text+ TA Editions">}}Edit Piaf{{</team-member>}}
{{</team>}}

{{<horizontal-line>}}

## Participating institutions
{{<accordion>}}
  {{<accordion-item title="Academy of Sciences and Literature | Mainz">}}  
  - Martin Sievers (2024, 4. März). Partner-Parade #03: Akademie der Wissenschaften und der Literatur | Mainz. Text\+ Blog. [https://doi.org/10.58079/vy9l](https://doi.org/10.58079/vy9l)
  - [Link to institution](https://www.adwmainz.de/en/)
  {{</accordion-item>}}
    
  {{<accordion-item title="Berlin-Brandenburg Academy of Sciences and Humanities">}}
  - [Link to institution](https://www.bbaw.de/)
  {{</accordion-item>}}

  {{<accordion-item title="Göttingen Academy of Sciences and Humanities in Lower Saxony">}}
  - [Link to institution](https://adw-goe.de/en/home/)
  {{</accordion-item>}}
  
  {{<accordion-item title="German National Academy of Sciences Leopoldina">}}
  - [Link to institution](https://www.leopoldina.org/en/leopoldina-home/)
  {{</accordion-item>}}

  {{<accordion-item title="Heidelberg Academy of Sciences and Humanities">}}
  **Text\+ Kooperationsprojekt 2023: “edition2LD”**
  
  - Text\+ Gast (2023, 18. Dezember). Text\+Plus, #04: Modellierung von Texteditionen als Linked Data (edition2LD). Text\+ Blog. [https://doi.org/10.58079/vfb4](https://doi.org/10.58079/vfb4) 
  - [Link to institution](https://www.hadw-bw.de/)
  {{</accordion-item>}}
    
  {{<accordion-item title="Herzog August Bibliothek Wolfenbüttel">}}
  - Daniela Schulz (2024, 27. Juni). Partner-Parade #04: Bibliosibirsk und die NFDI – Zur Rolle der HAB in den geisteswissenschaftlichen Konsortien. Text\+ Blog. [https://doi.org/10.58079/11w94](https://doi.org/10.58079/11w94)
  - [Link to institution](https://www.hab.de/en/)
  {{</accordion-item>}}
  
  {{<accordion-item title="Darmstadt University of Applied Sciences">}}
  - [Link to institution](https://h-da.de/en/)
  {{</accordion-item>}}

  {{<accordion-item title="Klassik Stiftung Weimar">}}
  - [Link to institution](https://www.klassik-stiftung.de/en/)
  {{</accordion-item>}}
  
  {{<accordion-item title="Leibniz Institute of European History">}}
  **Text\+ Kooperationsprojekt 2023: “FriVer+”**
  
  - Jaap Geraerts (2023, 29. September). Text\+Plus, #01: From Treaty to FAIR Data: FriVer+. Text\+ Blog. [https://doi.org/10.58079/uqhb](https://doi.org/10.58079/uqhb) 
  - [Link to institution](https://www.ieg-mainz.de/likecms.php?function=set_lang&lang=en)
  {{</accordion-item>}}

  {{<accordion-item title="Max Weber Foundation – German Humanities Institutes Abroad">}}
  - [Link to institution](https://www.maxweberstiftung.de/en/)
  {{</accordion-item>}}
  
  {{<accordion-item title="Göttingen State and University Library">}}
  - [Link to institution](https://www.sub.uni-goettingen.de/en/)
  {{</accordion-item>}}
  
  {{<accordion-item title="North Rhine-Westphalian Academy of Sciences, Humanities and the Arts">}}
  - [Link to institution](https://www.awk.nrw/)
  {{</accordion-item>}}
  
  {{<accordion-item title="Salomon Ludwig Steinheim Institute of German-Jewish Studies at the University of Duisburg-Essen">}}
  - [Link to institution](http://www.steinheim-institut.de/)
  {{</accordion-item>}}

  {{<accordion-item title="Technical University of Darmstadt">}}
  - [Link to institution](https://www.tu-darmstadt.de/index.en.jsp)
  {{</accordion-item>}}
  
  {{<accordion-item title="Paderborn University">}}
  - [Link to institution](https://www.uni-paderborn.de/en/)
  {{</accordion-item>}}

  {{<accordion-item title="University of Rostock">}}
  **Text\+ Kooperationsprojekt 2023: “Pessoa digital”**
  
  - Henny-Krahmer, U. (2023). Pessoa digital. Zenodo. [https://doi.org/10.5281/zenodo.10257440](https://doi.org/10.5281/zenodo.10257440)
  - [Link to institution](https://www.uni-rostock.de/en/)
  {{</accordion-item>}}
  
  {{<accordion-item title="University and State Library Darmstadt">}}
  - [Link to institution](https://www.ulb.tu-darmstadt.de/die_bibliothek/index.en.jsp)
  {{</accordion-item>}}
    
  {{<accordion-item title="University of Wuppertal">}}
  **Text\+ Kooperationsprojekt 2024: “RedBook1305”**

  - Text\+ Blog-Redaktion (2023, 4. Oktober). Bekanntgabe der neuen Kooperationsprojekte von Text\+. Text\+ Blog. [https://doi.org/10.58079/uqhc](https://doi.org/10.58079/uqhc)
  - [Link to institution](https://www.uni-wuppertal.de/en/)
  {{</accordion-item>}}
{{</accordion>}}
