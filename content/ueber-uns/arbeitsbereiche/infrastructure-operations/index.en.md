---
title: Infrastructure/Operations

aliases:
- /en/ueber-uns/infrastructure-operations/

---

# Infrastructure/Operations

{{<image img="gfx/Website IO.jpg" license="Generated with Stable Diffusion"/>}}

## Core Questions
* What does Text+ offer me, especially regarding research data management?
* How can I use these offerings?
* Who supports me with questions?
* Where can I find this support?
* What research data can I find at Text+ and what can I use it for?
* Can I submit my own research data to Text+?

This brief list of potential user questions outlines the tasks of the Text+ task area Infrastructure/Operations (IO in short), which coordinates infrastructure development in Text+. What tasks does IO undertake based on these questions?

## Goals
IO pursues the goal of an integrated and coordinated Text+ portfolio consisting of interoperable offerings and services (Data Services, Community Activities, Software Services). IO supports the development of the portfolio with basic services and infrastructure, expertise, and development resources.

The Text+ data domains – Editions, Lexical Resources, and Collections – are thus the primary target audience for IO. IO works with them on cross-cutting topics, the so-called cross domain topics. Cross-cutting topics include helpdesk, research data management planning, web portal, data center integration, registries, search & retrieval, metadata, GND agency Text+, and software services.

## External Connection
Beyond this internal view, IO serves as an interface to the infrastructure developments in the NFDI as a whole and ensures that developments, standards, and specific offerings align with the NFDI. The five sections of the [NFDI Association](https://www.nfdi.de/verein/), collaboration with the humanities and cultural consortia ([NFDI4Culture](https://nfdi4culture.de/de/index.html), [NFDI4Memory](https://4memory.de/), [NFDI4Objects](https://www.nfdi4objects.net/), Text+), and the NFDI basic services initiative [Base4NFDI](https://base4nfdi.de/) play an important role. On the national level and beyond, IO is also connected to relevant research infrastructures in Europe. For Text+, these are particularly the two European Research Infrastructure Consortia [CLARIN](https://www.clarin.eu/) and [DARIAH](https://www.dariah.eu/).

## Tasks
In summary, within Text+, IO is tasked with the following:

* Providing a FAIR-compliant platform for generic services, such as PID service, generic search, schema management services.
* Linking existing and developing data and service portfolios of data domains while adhering to standards and norm data.
* Supporting the linking to or integration of resources and services in the NFDI and beyond (including Base4NFDI).
* Enabling easy access to the service and data offerings of Text+ through user-friendly interfaces and APIs.
* Supporting a reliable and sustainable operation of the Text+ infrastructure.

## Text+ IO-Lectures
A virtual event series initiated by IO, open to everyone, featuring 20 lectures from May 2022 to the end of 2024.

### What is it about?
IO acts in a provider role within Text+ and identifies scientific needs for offerings from Text+ together with the other task areas. This creates a demand for information and advice, which IO meets through the regular, accessible series of Text+ IO lectures. Additionally, the IO lectures cover topics that are of interest beyond Text+ within NFDI and for research infrastructures in general.

### Who can participate? 
Anyone! Our offerings are free of charge. They are aimed at staff from Text+, the humanities and social sciences NFDI consortia, the NFDI as a whole, and research infrastructures. We want to engage both staff (the user perspective) and developers and infrastructure staff (the operator perspective). Some lectures have a limited number of participants, but most are open without restriction.

### How does it all work? 
Since May 2022, 20 Text+ IO lectures have already been held. Do you have a topic? Would you like to offer a lecture together with us? [Reach out, we look forward to your ideas!](https://text-plus.org/contact/) Want to participate? Just sign up. We provide a dedicated registration page for each lecture, through which we distribute Zoom links and further information.

All lectures take place virtually via Zoom and usually last between 60 and 120 minutes. Ideally, lively discussions follow the presentations by our speakers, which may lead to concrete working agreements and ideas for collaboration. 

{{< rawhtml >}}
<style>
tbody tr:nth-child(even) {
  background-color: #ededed;
  color: #000;
}

tbody tr:nth-child(even) td:first-child {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}

tbody tr:nth-child(even) td:last-child {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
}

td {
  padding: 0 5px 0 0;
}
</style>

<table>
  <tr>
    <th width="15%">Date</th>
    <th width="45%">Event</th>
    <th width="40%">Presenters</th>
  </tr>
  <tr valign="top">
    <td>10.02.2025</td>
    <td>
      <a href="https://events.gwdg.de/event/1009/" target="_blank">
        OAPEN & DOAB
      </a>
    </td>
    <td>Stefan Buddenbohm, Alexander Steckel (beide SUB)</td>
  </tr>
    <tr valign="top">
    <td>04.12.2024</td>
    <td>
      <a href="https://events.gwdg.de/event/980/" target="_blank">
        Wie kommt mein Dienst ins Text+ Portal?
      </a>
    </td>
    <td>Stefan Buddenbohm, Lukas Weimer (beide SUB)</td>
  </tr>
  <tr valign="top">
    <td>06.11.2024</td>
    <td>
      <a href="https://events.gwdg.de/event/979/" target="_blank">
        Migration von RocketChat zu Matrix
      </a>
    </td>
    <td>Stefan Buddenbohm, Lukas Weimer (beide SUB)</td>
  </tr>
  <tr valign="top">
    <td>24.10.2024</td>
    <td>
      <a href="https://events.gwdg.de/event/914/" target="_blank">
        RDMO in Text+
      </a>
    </td>
    <td>Eva-Maria Gerstner (MWS), Melina L. Jander (SUB)</td>
  </tr>
  <tr valign="top">
    <td>18.09.2024</td>
    <td>
      <a href="https://events.gwdg.de/event/913/" target="_blank">
        Entity Linking in Text+
      </a>
    </td>
    <td>Felix Helfer (SAW) et al.</td>
  </tr>
  <tr valign="top">
    <td>30.06.2024</td>
    <td>
      <a href="https://events.gwdg.de/event/700/" target="_blank">
        Die Basisklassifikation in den User Stories von Text+
      </a>
    </td>
    <td>José Calvo Tello, Charlotte Feidicker (beide SUB)</td>
  </tr>
  <tr valign="top">
    <td>29.05.2024</td>
    <td>
      <a href="https://events.gwdg.de/event/685/" target="_blank">
        TGRep und das Core Trust Seal
      </a>
    </td>
    <td>Stefan E. Funk, Lukas Weimer (beide SUB)</td>
  </tr>
  <tr valign="top">
    <td>18.04.2024</td>
    <td>
      <a href="https://events.gwdg.de/event/680/" target="_blank">
        Das TGRep - Aktuelle Entwicklungen und der fluffy Import-Workflow
      </a>
    </td>
    <td>Florian Barth, José Calvo Tello, Mathias Göbel, Ubbo Veentjer (alle SUB)</td>
  </tr>
  <tr valign="top">
    <td>10.01.2024</td>
    <td>
    <a href="https://events.gwdg.de/event/680/" target="_blank">
      Der Redaktionsworkflow mit GitLab
      </a>
        <td>Alex Steckel (SUB), Maik Wegener (GWDG)</td>
  </tr>
  <tr valign="top">
    <td>30.11.2023</td>
    <td>
      <a href="https://events.gwdg.de/e/coeso" target="_blank">
        COESCO/VERA & Citizen Science in Text+
      </a>
    </td>
    <td>Marlen Töpfer (MWS)</td>
  </tr>
  <tr valign="top">
    <td>13.07.2023</td>
    <td>
      <a href="https://events.gwdg.de/e/gotriple" target="_blank">
        GoTriple
      </a>
    </td>
    <td>Pattrick Piel, Marlen Töpfer (beide MWS)</td>
  </tr>
  <tr valign="top">
    <td>27.04.2023</td>
    <td>
      <a href="https://events.gwdg.de/e/entityXML" target="_blank">
        entityXML - Ein Werkzeug aus dem GND-Task
      </a>
    </td>
    <td>Susanne Al-Eryani, Uwe Sikora (beide SUB)</td>
  </tr>
  <tr valign="top">
    <td>22.03.2023</td>
    <td>
      <a href="https://events.gwdg.de/e/dfa_dme" target="_blank">
        Die DARIAH Data Federation Architecture/Data Modelling Environment
      </a>
    </td>
    <td>Tobias Gradl (MInfBA)</td>
  </tr>
  <tr valign="top">
    <td>15.02.2023</td>
    <td>
      <a href="https://events.gwdg.de/e/pipeline" target="_blank">
        Der Redaktionsworkflow im Webportal aka CI/CD via GitLab
      </a>
    </td>
    <td>Alex Steckel (SUB), Maik Wegener (GWDG)</td>
  </tr>
  <tr valign="top">
    <td>01.12.2022</td>
    <td>
      <a href="https://events.gwdg.de/event/367/" target="_blank">
        Git - Eine Einführung ins Arbeiten
      </a>
    </td>
    <td>Jasmin Oster (GWDG)</td>
  </tr>
  <tr valign="top">
    <td>18.11.2022</td>
    <td>
      <a href="https://pad.gwdg.de/3QefSWCKTZaTBX8C9wy7EQ" target="_blank">
        GitLab und seine Einsatzmöglichkeiten in Text+
      </a>
    </td>
    <td>George Dogaru (GWDG)</td>
  </tr>
    <tr valign="top">
    <td>15.08.2022</td>
    <td>
      <a href="https://textplus.sync.academiccloud.de/s/GXFsY6nDQoqsxj9" target="_blank">
        Jupyter Notebooks
      </a>
    </td>
    <td>George Dogaru (GWDG), Melina Jander (SUB)</td>
  </tr>
  <tr valign="top">
    <td>20.07.2022</td>
    <td>
      <a href="https://textplus.sync.academiccloud.de/s/E3DyeiFMxckTk2r" target="_blank">
        Der Text+ Helpdesk in GOTRS, Teil 2
      </a>
    </td>
    <td>Sonja Friedrichs (SUB)</td>
  <tr valign="top">
    <td>05.07.2022</td>
    <td>
      <a href="https://textplus.sync.academiccloud.de/s/E3DyeiFMxckTk2r" target="_blank">
        Der Text+ Helpdesk in GOTRS, Teil 1
      </a>
    </td>
    <td>Sonja Friedrichs (SUB)</td>
      <tr valign="top">
    <td>21.05.2022</td>
    <td>
      <a href="https://wiki.de.dariah.eu/x/9iBQC" target="_blank">
        Zotero und die Projektbibliographie von Text+
      </a>
    </td>
    <td>Lukas Weimer (SUB)</td>
   </tr>
</table>
{{< /rawhtml >}}
