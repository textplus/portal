---
title: Infrastructure/Operations

aliases:
- /ueber-uns/infrastructure-operations/

---

# Infrastructure/Operations

{{<image img="gfx/Website IO.jpg" license="Generated with Stable Diffusion"/>}}

## Kernfragen
* Welche Angebote, insbesondere hinsichtlich Forschungsdatenmanagement hat Text+ für mich?
* Wie kann ich die Angebote nutzen?
* Wer unterstützt mich bei Fragen?
* Wo finde ich diese Unterstützung?
* Welche Forschungsdaten finde ich bei Text+ und wofür kann ich sie nutzen?
* Kann ich eigene Forschungsdaten an Text+ übergeben? 

Diese kurze Liste von möglichen Fragestellungen von Nutzenden umreißt das Aufgabenfeld der Text+ Task Area Infrastructure/Operations (kurz IO), welche die Infrastrukturentwicklung in Text+ koordiniert. Welche Aufgaben folgen daraus für IO?

## Ziele
IO verfolgt das Ziel eines integrierten und aufeinander abgestimmten Text+ Angebotsportfolios, das aus interoperablen Angeboten und Diensten (Data Services, Community Activities, Software Services) besteht. Die Entwicklungsarbeiten für das Angebotsportfolio werden von IO mit Basisdiensten und -infrastruktur, Expertise und Entwicklungsressourcen unterstützt.

Die Text+ Datendomänen – Editionen, lexikalische Ressourcen und Sammlungen – sind somit die erste Zielgruppe für IO. Mit ihnen zusammen bearbeitet IO Querschnittsthemen, die sogenannten Cross Domain Topics. Querschnittsthemen sind Helpdesk, Forschungsdatenmanagementplanung, Webportal, Anbindung Datenzentren, Registries, Search & Retrieval, Metadaten, GND-Agentur Text+, Software Services.

## Außenanbindung
Über diese Binnensicht hinaus, fungiert IO als Schnittstelle zu den Infrastruktur-Entwicklungen in der NFDI insgesamt und stellt sicher, dass die Entwicklungen, Standards und konkreten Angebote zur NFDI passen. Hierbei spielen die fünf Sektionen des [NFDI-Vereins](https://www.nfdi.de/verein/), die Zusammenarbeit der geistes- und kulturwissenschaftlichen Konsortien ([NFDI4Culture](https://nfdi4culture.de/de/index.html), [NFDI4Memory](https://4memory.de/), [NFDI4Objects](https://www.nfdi4objects.net/), Text+) sowie die NFDI-Basisdiensteinitiative [Base4NFDI](https://base4nfdi.de/) eine wichtige Rolle. Über die nationale Ebene hinaus ist IO auch mit den einschlägigen Forschungsinfrastrukturen auf europäischer Ebene verbunden. Für Text+ sind dies insbesondere die beiden European Research Infrastructure Consortia [CLARIN](https://www.clarin.eu/) und [DARIAH](https://www.dariah.eu/).

## Aufgaben
Zusammengefasst ist IO innerhalb von Text+ mit den folgenden Aufgaben betraut:

*	Bereitstellung einer FAIR-konform Plattform für generische Dienste, bspw. PID-Service, generische Suche, Schema-Management Services.
*	Verknüpfung existierender und in Entwicklung befindlicher Daten- und Diensteportfolios der Datendomänen unter Beachtung von Standards und Normdaten.
*	Unterstützung bei der Verknüpfung zu bzw. Integration von Ressourcen und Diensten in der NFDI und darüber hinaus (u.a. Base4NFDI).
*	Ermöglichung eines einfachen Zugangs zum Dienste- und Datenangebot von Text+ durch nutzungsfreundlich gestaltete Interfaces und APIs.
*	Unterstützung eines verlässlichen und nachhaltigen Betriebs der Text+ Infrastruktur.

## Text+ IO-Lectures
Von IO ins Leben gerufene, für alle offene, virtuelle Veranstaltungsreihe, in der von Mai 2022 bis Ende 2024 bereits 20 Lectures erschienen sind.

### Worum geht es?
IO tritt in Text+ in einer Providerrolle auf und ermittelt zusammen mit den anderen Task Areas wissenschaftliche Bedarfe für Angebote von Text+. Daraus ergibt sich ein Bedarf an Information und Beratung, den IO durch die regelmäßige, niedrigschwellige Reihe der Text+ IO-Lectures bedient. Zusätzlich decken die IO-Lectures Themen ab, die über Text+ hinaus in der NFDI und für Forschungsinfrastrukturen im Allgemeinen von Interesse sind.

### Wer kann teilnehmen?
Jede/jeder! Unser Angebot ist kostenlos. Es richtet sich an Mitarbeitende von Text+, aus den geistes- und sozialwissenschaftlichen NFDI-Konsortien, der NFDI insgesamt sowie Forschungsinfrastrukturen. Wir wollen sowohl Mitarbeitende (d.h. die Nutzungsperspektive) als auch die Entwickler und Infrastrukturmitarbeitende (die Betreiberperspektive) ansprechen. Manche Lectures sind auf eine bestimmte Teilnehmendenanzahl beschränkt, die meisten sind aber ohne Beschränkung.

### Wie funktioniert das Ganze?
Seit Mai 2022 sind bereits 20 Text+ IO-Lectures erschienen. Haben Sie ein Thema? Möchten Sie vielleicht gemeinsam mit uns eine Lecture anbieten? [Schreiben Sie uns, wir freuen uns auf Ihre Ideen!](https://text-plus.org/contact/) Wollen Sie teilnehmen? Einfach anmelden. Wir bieten zu jeder Lecture eine eigene Anmeldeseite an, über die wir die Zoomlinks und weitere Infos verteilen.

Alle Lectures finden virtuell via Zoom statt und dauern in der Regel zwischen 60 und 120 Minuten. Idealerweise schließt sich an die Impulse unserer Vortragenden eine lebhafte Diskussion an, die gerne in konkrete Arbeitsabsprachen und Kooperationsideen münden darf.

{{< rawhtml >}}
<style>
tbody tr:nth-child(even) {
  background-color: #ededed;
  color: #000;
}

tbody tr:nth-child(even) td:first-child {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}

tbody tr:nth-child(even) td:last-child {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
}

td {
  padding: 0 5px 0 0;
}
</style>

<table>
  <tr>
    <th width="15%">Datum</th>
    <th width="55%">Event</th>
    <th width="30%">Referent:innen</th>
  </tr>
  <tr valign="top">
    <td>10.02.2025</td>
    <td>
      <a href="https://events.gwdg.de/event/1009/" target="_blank">
        OAPEN & DOAB
      </a>
    </td>
    <td>Stefan Buddenbohm, Alexander Steckel (beide SUB)</td>
  </tr>
    <tr valign="top">
    <td>04.12.2024</td>
    <td>
      <a href="https://events.gwdg.de/event/980/" target="_blank">
        Wie kommt mein Dienst ins Text+ Portal?
      </a>
    </td>
    <td>Stefan Buddenbohm, Lukas Weimer (beide SUB)</td>
  </tr>
  <tr valign="top">
    <td>06.11.2024</td>
    <td>
      <a href="https://events.gwdg.de/event/979/" target="_blank">
        Migration von RocketChat zu Matrix
      </a>
    </td>
    <td>Stefan Buddenbohm, Lukas Weimer (beide SUB)</td>
  </tr>
  <tr valign="top">
    <td>24.10.2024</td>
    <td>
      <a href="https://events.gwdg.de/event/914/" target="_blank">
        RDMO in Text+
      </a>
    </td>
    <td>Eva-Maria Gerstner (MWS), Melina L. Jander (SUB)</td>
  </tr>
  <tr valign="top">
    <td>18.09.2024</td>
    <td>
      <a href="https://events.gwdg.de/event/913/" target="_blank">
        Entity Linking in Text+
      </a>
    </td>
    <td>Felix Helfer (SAW) et al.</td>
  </tr>
  <tr valign="top">
    <td>30.06.2024</td>
    <td>
      <a href="https://events.gwdg.de/event/700/" target="_blank">
        Die Basisklassifikation in den User Stories von Text+
      </a>
    </td>
    <td>José Calvo Tello, Charlotte Feidicker (beide SUB)</td>
  </tr>
  <tr valign="top">
    <td>29.05.2024</td>
    <td>
      <a href="https://events.gwdg.de/event/685/" target="_blank">
        TGRep und das Core Trust Seal
      </a>
    </td>
    <td>Stefan E. Funk, Lukas Weimer (beide SUB)</td>
  </tr>
  <tr valign="top">
    <td>18.04.2024</td>
    <td>
      <a href="https://events.gwdg.de/event/680/" target="_blank">
        Das TGRep - Aktuelle Entwicklungen und der fluffy Import-Workflow
      </a>
    </td>
    <td>Florian Barth, José Calvo Tello, Mathias Göbel, Ubbo Veentjer (alle SUB)</td>
  </tr>
  <tr valign="top">
    <td>10.01.2024</td>
    <td>
    <a href="https://events.gwdg.de/event/680/" target="_blank">
      Der Redaktionsworkflow mit GitLab
      </a>
        <td>Alex Steckel (SUB), Maik Wegener (GWDG)</td>
  </tr>
  <tr valign="top">
    <td>30.11.2023</td>
    <td>
      <a href="https://events.gwdg.de/e/coeso" target="_blank">
        COESCO/VERA & Citizen Science in Text+
      </a>
    </td>
    <td>Marlen Töpfer (MWS)</td>
  </tr>
  <tr valign="top">
    <td>13.07.2023</td>
    <td>
      <a href="https://events.gwdg.de/e/gotriple" target="_blank">
        GoTriple
      </a>
    </td>
    <td>Pattrick Piel, Marlen Töpfer (beide MWS)</td>
  </tr>
  <tr valign="top">
    <td>27.04.2023</td>
    <td>
      <a href="https://events.gwdg.de/e/entityXML" target="_blank">
        entityXML - Ein Werkzeug aus dem GND-Task
      </a>
    </td>
    <td>Susanne Al-Eryani, Uwe Sikora (beide SUB)</td>
  </tr>
  <tr valign="top">
    <td>22.03.2023</td>
    <td>
      <a href="https://events.gwdg.de/e/dfa_dme" target="_blank">
        Die DARIAH Data Federation Architecture/Data Modelling Environment
      </a>
    </td>
    <td>Tobias Gradl (MInfBA)</td>
  </tr>
  <tr valign="top">
    <td>15.02.2023</td>
    <td>
      <a href="https://events.gwdg.de/e/pipeline" target="_blank">
        Der Redaktionsworkflow im Webportal aka CI/CD via GitLab
      </a>
    </td>
    <td>Alex Steckel (SUB), Maik Wegener (GWDG)</td>
  </tr>
  <tr valign="top">
    <td>01.12.2022</td>
    <td>
      <a href="https://events.gwdg.de/event/367/" target="_blank">
        Git - Eine Einführung ins Arbeiten
      </a>
    </td>
    <td>Jasmin Oster (GWDG)</td>
  </tr>
  <tr valign="top">
    <td>18.11.2022</td>
    <td>
      <a href="https://pad.gwdg.de/3QefSWCKTZaTBX8C9wy7EQ" target="_blank">
        GitLab und seine Einsatzmöglichkeiten in Text+
      </a>
    </td>
    <td>George Dogaru (GWDG)</td>
  </tr>
    <tr valign="top">
    <td>15.08.2022</td>
    <td>
      <a href="https://textplus.sync.academiccloud.de/s/GXFsY6nDQoqsxj9" target="_blank">
        Jupyter Notebooks
      </a>
    </td>
    <td>George Dogaru (GWDG), Melina Jander (SUB)</td>
  </tr>
  <tr valign="top">
    <td>20.07.2022</td>
    <td>
      <a href="https://textplus.sync.academiccloud.de/s/E3DyeiFMxckTk2r" target="_blank">
        Der Text+ Helpdesk in GOTRS, Teil 2
      </a>
    </td>
    <td>Sonja Friedrichs (SUB)</td>
  <tr valign="top">
    <td>05.07.2022</td>
    <td>
      <a href="https://textplus.sync.academiccloud.de/s/E3DyeiFMxckTk2r" target="_blank">
        Der Text+ Helpdesk in GOTRS, Teil 1
      </a>
    </td>
    <td>Sonja Friedrichs (SUB)</td>
      <tr valign="top">
    <td>21.05.2022</td>
    <td>
      <a href="https://wiki.de.dariah.eu/x/9iBQC" target="_blank">
        Zotero und die Projektbibliographie von Text+
      </a>
    </td>
    <td>Lukas Weimer (SUB)</td>
   </tr>
</table>
{{< /rawhtml >}}
