---
title: Lexikalische Ressourcen

aliases:
- /ueber-uns/lexikalische-ressourcen/

---

# Lexikalische Ressourcen

{{<image img="gfx/Website LR.jpg" license="diannehope14 (Pixabay), https://pixabay.com/de/photos/zusammenarbeit-mitarbeiter-buchen-1106196/">}}{{</image>}}

Lexikalische Ressourcen sind Daten, die die Verwendung von Wörtern in Sätzen, Texten und multimodaler Kommunikation beschreiben. Darunter fallen:

* Wörterbücher (mehrsprachige Wörterbücher, historische Wörterbücher, Fachwörterbücher),
* Enzyklopädien,
* Normdaten,
* terminologische Datenbanken,
* Ontologien,
* Wortlisten,
* Wortkarten und linguistische Atlanten,
* Übersetzungswörterbücher (für menschliche oder maschinelle Übersetzung)
* und andere Daten.

Ziel der Arbeiten in der Datendomäne *Lexikalische Ressourcen* ist es, den Fachwissenschaften die Fülle des vorliegenden lexikalischen Wissens zur Verfügung zu stellen, also Wissen über Wörter aus jeweils relevanten Perspektiven anzubieten.

## Aufgaben

### Referenzimplementation

Für eine Nutzung der durch Text+ bereitgestellten lexikalischen Daten wird eine Basisinfrastruktur implementiert und bereitgestellt.

### Portfolioentwicklung

Sowohl bereits durch Text+ angebotene, als auch an das Konsortium durch externe Beitragende herangetragene Daten werden kuratiert, angereichert, diversifiziert und mit anderen Datensätzen durch standardisierte Verfahren und über Normdatenbanken verknüpft.

### Standardisierung

Bei der Vereinheitlichung von Daten und Datentypen arbeitet die Datendomäne der Lexikalischen Ressourcen nach den [FAIR-](https://www.nature.com/articles/sdata201618) und [CARE-Prinzipien](https://zenodo.org/record/5995059#.YkqnZTWxU2w).

### Nutzer:innen-Einbindung

Beratung von Nutzenden, Unterstützung von Lehre und Bereistellung von Lehrmaterial und Dissemination sind ein essentieller Bestandteil während der Projektlaufzeit.

### Software-Services

Zur Erschließung, Verknüpfung und Anreicherung lexikalischer Ressourcen werden (domänenspezifische) Werkzeuge entwickelt.

Darüber hinaus beteiligen sich Mitglieder der Datendomäne auch an verschiedenen übergreifenden Arbeitsgruppen, zum Beispiel bei der *Federated Content Search*<!-- TODO: verlinken -->, bei *Linked Open Data*<!-- TODO: verlinken --> oder dem [Helpdesk](/helpdesk).

## Beteiligte Institutionen

* [Berlin-Brandenburgische Akademie der Wissenschaften](https://www.bbaw.de/) (BBAW)
* [Kompetenzzentrum – Trier Center for Digital Humanities](https://tcdh.uni-trier.de/de) (TCDH)
* [Leibniz-Institut für Deutsche Sprache, Mannheim](https://www.ids-mannheim.de/) (IDS)
* [Sächsische Akademie der Wissenschaften zu Leipzig](https://www.saw-leipzig.de/de) (SAW)
* [Universität Tübingen](https://uni-tuebingen.de/)
* [Universität zu Köln](https://www.uni-koeln.de/)

## Ansprechpersonen

* Sprecher: [Alexander Geyken](https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/geyken-alexander)
* Koordinator: [Axel Herold](https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/herold-axel)

Nutzen Sie auch gern den [Text\+-Helpdesk](/helpdesk).
