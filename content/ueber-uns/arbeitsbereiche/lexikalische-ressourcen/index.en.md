---
title: Lexical Resources

aliases:
- /en/ueber-uns/lexikalische-ressourcen/

---

# Lexical Resources

{{<image img="gfx/Website LR.jpg" license="diannehope14 (Pixabay), https://pixabay.com/de/photos/zusammenarbeit-mitarbeiter-buchen-1106196/">}}{{</image>}}

Lexical resources refer to data that describe the use of words in sentences, texts, and multimodal communication. This includes:

* Dictionaries (multilingual dictionaries, historical dictionaries, specialized dictionaries),
* Encyclopedias,
* Normative data,
* Terminological databases,
* Ontologies,
* Word lists,
* Word maps and linguistic atlases,
* Translation dictionaries (for human or machine translation),
* and other data.

The goal of the work in the *Lexical Resources* data domain is to provide the academic community with a wealth of lexical knowledge, offering insights into words from relevant perspectives.

## Tasks

### Reference Implementation

A foundational infrastructure is implemented and provided for the use of lexical data provided by Text+.

### Portfolio Development

Data already offered by Text+ and contributions from external contributors are curated, enriched, diversified, and linked to other datasets through standardized procedures and via standardized databases.

### Standardization

The Lexical Resources data domain works towards the standardization of data and data types following the [FAIR principles](https://www.nature.com/articles/sdata201618) and [CARE principles](https://zenodo.org/record/5995059#.YkqnZTWxU2w).

### User Engagement

Providing guidance to users, supporting education, and offering teaching materials and dissemination are essential components during the project's duration.

### Software Services

Tools, including domain-specific ones, are developed for the exploration, linking, and enrichment of lexical resources.

Additionally, members of the data domain also participate in various cross-domain working groups, such as *Federated Content Search*<!-- TODO: verlinken -->, *Linked Open Data*<!-- TODO: verlinken -->, or the [Helpdesk](/helpdesk).

## Participating Institutions

* [Berlin-Brandenburg Academy of Sciences and Humanities](https://www.bbaw.de/) (BBAW)
* [Competence Center – Trier Center for Digital Humanities](https://tcdh.uni-trier.de/en) (TCDH)
* [Leibniz Institute for the German Language, Mannheim](https://www.ids-mannheim.de/) (IDS)
* [Saxon Academy of Sciences in Leipzig](https://www.saw-leipzig.de/en) (SAW)
* [University of Tübingen](https://uni-tuebingen.de/)
* [University of Cologne](https://www.uni-koeln.de/)

## Contact Persons

* Spokesperson: [Alexander Geyken](https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/geyken-alexander)
* Coordinator: [Axel Herold](https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/herold-axel)

Feel free to also use the [Text\+ Helpdesk](/helpdesk).
