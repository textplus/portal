---
title: Collections

draft: false

aliases:
- /ueber-uns/collections/

---

# Collections

{{<image img="gfx/Website Collections.jpg" license="Hintergrundbild: Pexels (Pixabay), https://pixabay.com/de/users/pexels-2286921"/>}}


Die Datendomäne Collections umfasst sprach- und textbasierte Sammlungen geschriebener, gesprochener oder gebärdeter Sprache und Texte, die auf Grundlage wissenschaftlicher Kriterien erstellt wurden. Dazu gehören beispielsweise Textsammlungen und Korpora, mono- und multimodale Aufnahmen von Sprache, aber auch sprach- und textbezogene Experimental- oder Messdaten. Die Sammlungen werden in der verteilten Infrastruktur von Text+ von verschiedenen, zertifizierten [Datenzentren](/ueber-uns/daten-kompetenzzentren) mit je eigenen Spezialisierungen bereitgestellt und sind im Rahmen von Text+ über die [Registry](/daten-dienste/daten/#suche-nach-ressourcen--die-text-registry) wie auch die [föderierte Inhaltssuche](/daten-dienste/daten/#suche-in-den-forschungsdaten--die-föderierte-inhaltssuche) recherchierbar.

Für den Wissenstransfer und bei Fragen im Umgang mit Collections und deren Vernetzung ist der [Helpdesk von Text+](/helpdesk) die erste Anlaufstelle. Er dient auch als Kontaktadresse für Projekte, die Interesse an der [Integration von Forschungsdaten](/daten-dienste/depositing/) in die Infrastruktur von Text+ haben.

Besonderes Augenmerk legt die Datendomäne auf [rechtliche und ethische Fragestellungen](/themen-dokumentation/legal/), für die sie innerhalb des Konsortiums und darüber hinaus als Anlaufstelle fungiert. So stehen etwa [abgeleitete Textformate](/themen-dokumentation/atf/), die eine Beforschung urheberrechtlich geschützter Ressourcen ermöglichen, ohne das Urheberrecht zu verletzen, besonders im Fokus. Weitere Schwerpunkte sind die Community-basierte Weiterentwicklung von [Software Services](/daten-dienste/dienste/), große [Sprachmodelle und KI](/themen-dokumentation/llm/) sowie Fragen zu Themen wie [Forschungsdatenmanagement](/themen-dokumentation/forschungsdatenmanagement/) oder [Standardisierung](/themen-dokumentation/standardisierung/).

## Ansprechpersonen
Sprecher der Datendomäne Collections ist [Dr. Peter Leinen](mailto:p.leinen@dnb.de), koordiniert wird sie von [Philippe Genêt](mailto:p.genet@dnb.de). Beide arbeiten in der [Deutschen Nationalbibliothek](https://www.dnb.de).

## Beteiligte Institutionen
[Akademie der Wissenschaften in Hamburg](https://www.awhamburg.de/)  
[Albert-Ludwigs-Universität Freiburg](https://www.uni-freiburg.de/)  
[Bayerische Akademie der Wissenschaften](https://www.badw.de/die-akademie.html)  
[Berlin-Brandenburgische Akademie der Wissenschaften](https://www.bbaw.de/)  
[Deutsche Nationalbibliothek](https://www.dnb.de/)  
[Deutsches Literaturarchiv Marbach](https://www.dla-marbach.de/)  
[Eberhard Karls Universität Tübingen](https://uni-tuebingen.de/)  
[Gesellschaft für wissenschaftliche Datenverarbeitung mbH Göttingen](https://www.gwdg.de/)  
[Heidelberger Akademie der Wissenschaften](https://www.hadw-bw.de/)  
[Julius-Maximilians-Universität Würzburg](https://www.uni-wuerzburg.de/startseite/)  
[Klassik Stiftung Weimar](https://www.klassik-stiftung.de/)  
[Leibniz-Institut für Deutsche Sprache, Mannheim](https://www1.ids-mannheim.de/)  
[Ludwig-Maximilians-Universität München](https://www.uni-muenchen.de/index.html)  
[Niedersächsische Staats- und Universitätsbibliothek Göttingen](https://www.sub.uni-goettingen.de/sub-aktuell/)  
[Universität des Saarlandes](https://www.uni-saarland.de/start.html)  
[Universität Duisburg-Essen](https://www.uni-due.de/)  
[Universität Hamburg](https://www.uni-hamburg.de/)  
[Universität Trier](https://www.uni-trier.de/)  
[Universität zu Köln](https://www.uni-koeln.de/)  
