---
title: Collections

aliases:
- /en/ueber-uns/collections/

---

# Collections

{{<image img="gfx/Website Collections.jpg" license="Background image: Pexels (Pixabay), https://pixabay.com/de/users/pexels-2286921"/>}}

The Collections data domain encompasses language and text-based collections of written, spoken, or signed language and texts that were written based on scientific criteria. This includes text collections and corpora, mono- and multimodal recordings of language, as well as language- and text-related experimental or measurement data. The collections are provided in the distributed infrastructure of Text+ by various certified [data centres](/en/ueber-uns/daten-kompetenzzentren), each with their own specialisations, and can be searched within Text+ via the [registry](/en/daten-dienste/daten/#suche-nach-ressourcen--die-text-registry) as well as the [federated content search](/en/daten-dienste/daten/#suche-in-den-forschungsdaten--die-föderierte-inhaltssuche).

For knowledge transfer and assistance with questions related to Collections and their integration, the [Text+ Helpdesk](/en/helpdesk) is the primary contact point. It also serves as a contact address for projects interested in [integrating research data](/en/daten-dienste/depositing/) into the Text+ infrastructure. 

The data domain pays particular attention to [legal and ethical issues](/en/themen-dokumentation/legal/), for which it acts as a point of contact within the consortium and beyond. For example, [derived text formats](/en/themen-dokumentation/atf/) that enable research into copyright-protected resources without infringing copyright are a particular focus. Other priorities include the community-based further development of [software services](/en/daten-dienste/dienste/), [large language models and AI](/en/themen-dokumentation/llm/) as well as issues relating to topics such as [research data management](/en/themen-dokumentation/forschungsdatenmanagement/) and [standardisation](/en/themen-dokumentation/standardisierung/).

## Contact Persons
The spokesperson for the Collections data domain is [Dr. Peter Leinen](mailto:p.leinen@dnb.de), and it is coordinated by [Philippe Genêt](mailto:p.genet@dnb.de). Both work at the [German National Library](https://www.dnb.de/).

## Participating Institutions
- [Academy of Sciences in Hamburg](https://www.awhamburg.de/)
- [Albert Ludwig University of Freiburg](https://www.uni-freiburg.de/)
- [Bavarian Academy of Sciences](https://www.badw.de/die-akademie.html)
- [Berlin-Brandenburg Academy of Sciences and Humanities](https://www.bbaw.de/)
- [German National Library](https://www.dnb.de/)
- [German Literature Archive Marbach](https://www.dla-marbach.de/)
- [Eberhard Karls University of Tübingen](https://uni-tuebingen.de/)
- [GWDG - Gesellschaft für wissenschaftliche Datenverarbeitung mbH Göttingen](https://www.gwdg.de/)
- [Heidelberg Academy of Sciences](https://www.hadw-bw.de/)
- [Julius Maximilian University of Würzburg](https://www.uni-wuerzburg.de/startseite/)
- [Classical Foundation Weimar](https://www.klassik-stiftung.de/)
- [Leibniz Institute for the German Language, Mannheim](https://www1.ids-mannheim.de/)
- [Ludwig Maximilian University of Munich](https://www.uni-muenchen.de/index.html)
- [Lower Saxony State and University Library Göttingen](https://www.sub.uni-goettingen.de/sub-aktuell/)
- [University of Saarland](https://www.uni-saarland.de/start.html)
- [University of Duisburg-Essen](https://www.uni-due.de/)
- [University of Hamburg](https://www.uni-hamburg.de/)
- [University of Trier](https://www.uni-trier.de/)
- [University of Cologne](https://www.uni-koeln.de/)
