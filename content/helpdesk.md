---
title: "Helpdesk"

aliases:
- /contact
- /kontakt
---

# Text+ Helpdesk
Der Helpdesk ermöglicht Ihnen [direkten Zugang](helpdesk#kontaktformular) zum [Text+ Consulting](daten-dienste/consulting). Hierüber bieten wir individuelle Unterstützung für alle Dienste und Services von Text+ sowie für text- und sprachbasierte Forschungsdaten.

---
#### Spektrum und Ziel
Das Beratungsspektrum reicht von der Antragsberatung über allgemeine Fragen zum Forschungsdatenmanagement, von der Diskussion von Forschungsideen und -prozessen bis hin zu Spezialfragen zu bestimmten Tools und Technologien über den gesamten Datenlebenszyklus hinweg. 

Ziel der Beratungen ist die Verbesserung der Datenqualität im Sinne der FAIR-Prinzipien, die Verbreitung standardisierter, nachhaltiger Forschungspraktiken und somit letztlich die Erhöhung der Erfolgsaussichten Ihrer Forschungsvorhaben.

---
#### Ansprechbarkeit
Für den Helpdesk  steht Ihnen im Folgenden ein Webformular zur Verfügung.

Für allgemeine oder kurzfristige Anliegen bieten sich unsere [Text+ Research Rendezvous](https://events.gwdg.de/category/208/) an. Dabei handelt es sich um zweiwöchentlich stattfindende, offene Sprechstunden, für die keinerlei Anmeldung erforderlich ist. 

Sprechen Sie uns zudem jederzeit im Rahmen der vielfältigen [Veranstaltungen von und mit Text+](aktuelles/veranstaltungen/) an. Ihre Anliegen werden auf Wunsch weitergeleitet. 

---
## Kontaktformular
{{<support-form>}}

---
## Weiterführende Informationen

 Das [Text+ Consulting](daten-dienste/consulting) besteht aus einem vielfältigen Netzwerk von Expert:innen der [antragstellenden und beteiligten Institutionen von Text+](/ueber-uns/antragstellende-beteiligte-institutionen/). Bei Bedarf stellen wir darüber hinaus den Kontakt zu NFDI übergreifenden Strukturen sowie zu unseren nationalen und internationalen Partnern her - allen voran den Partnerkonsortien [NFDI4Culture](https://nfdi4culture.de/helpdesk.html), [NFDI4Memory](https://4memory.de/) und [NFDI4Objects](https://www.nfdi4objects.net/index.php/en/help-desk). Sofern Sie Ihr Anliegen bereits spezifizieren können, weisen Sie es einfach der gewünschten Datendomäne oder einem der vorgeschlagenen Bereiche zu. Bei Grenzfällen, übergreifenden oder sonstigen Anliegen wählen Sie einfach "Allgemeines" aus.


#### Allgemeines
Offen für allgemeine und projektübergreifende Anliegen zu text- und sprachbasierten Forschungsdaten, zu Text+ und NFDI, sowie Kooperationsanfragen.

#### Datendomäne Collections
Beratung zur Erstellung, Archivierung und Nutzung sprach- und textbasierter Sammlungen bzw. Forschungsdaten – z.B. zur Digitalisierung analoger Sprach- und Textobjekten oder zum Text- und Datamining. Beratung zu spezifischen, sammlungsbezogenen Angeboten sowie allgemeine Antragsberatung (z.B. zu Datenmanagementplänen).

#### Datendomäne Lexikalische Ressourcen
Bietet Beratung zu lexikalischen Ressourcen, z.B. der standardkonformen Digitalisierung und Datenkuratierung von Altbeständen sowie der empirischen Ressourcennutzung. Beratung zu spezifischen Angeboten und Datenmanagementplänen für lexikalische Ressourcen.

#### Datendomäne Editionen
Unterstützung für alle Arten von Editionen aus verschiedenen Forschungsfeldern: Wir beraten in allen Projekt- und Editionsphasen und zu allen Datenschichten von Editionen. Wir unterstützen zudem bei der Eintragung von Editionen in die Editions Registry und sind jederzeit offen für gemeinsame Veranstaltungen und Kooperationen aller Art. 

#### Infrastruktur/Betrieb
Beratung zu Infrastrukturangeboten und Querschnittsthemen wie z.B. Datenqualität, Interoperabilität, Langzeitarchivierung, Nachnutzbarkeit, Registries und Linked Open Data.

#### Technischer Support
Bietet Hilfestellung bei technischen Problemen mit dem Webportal und den Services von Text+

#### Rechtliches und Ethisches
Unterstützung bei rechtlichen und ethischen Fragen zu text- und sprachbasierten Forschungsdaten. Bitte beachten Sie, dass Text+ keine Rechtsberatung anbieten kann.
