---
title: "Home"
type: landing
---

{{<call-to-action title="Utilize and Preserve Text- and Language-based Research Data">}}
{{<lead-text>}}
Text+ provides an extensive collection of text collections and corpora, editions and lexical resources for re-use as part of the NFDI. In addition to its own data and services, the service portfolio also includes the option of integrating and preserving research data from the community in the long term.
{{</lead-text>}}

{{<button-row align="end">}}
        {{<button url="/en/daten-dienste/daten">}}Data Search{{</button>}}
        {{<button url="/daten-dienste/depositing">}}Data Depositing{{</button>}}
        {{<button url="/en/daten-dienste/dienste">}}Services{{</button>}}
        {{<button url="/en/daten-dienste/consulting">}}Consulting/Helpdesk{{</button>}}
{{</button-row>}}

{{</call-to-action>}}

## Text+ and the NFDI

As part of the [National Research Data Infrastructure (NFDI)](https://www.dfg.de/de/foerderung/foerderinitiativen/nfdi), the [DFG](https://www.dfg.de/) is funding a total of 26 consortia from the four fields of humanities and social sciences, engineering sciences, life sciences and natural sciences. The Text+ consortium is one of six consortia from the humanities, including [Berd@NFDI](https://www.berd-nfdi.de/), [KonsortSWD](https://www.konsortswd.de/), [NFDI4Culture](https://nfdi4culture.de/index.html), [NFDI4Memory](https://4memory.de/), [NFDI4Objects](https://www.nfdi4objects.net/). While there are many cross-consortia activities via the [NFDI sections](https://www.nfdi.de/sektionen/), the Text+ consortium is in particularly close dialogue with the humanities and social sciences consortia.

## Showcase

Using the example of the transfer of research data stored on discs, the outlined workflow provides an impression of data integration in Text+. It ranges from initial contact and consulting to the integration, re-utilisation and long-term preservation of the data (registry, FCS, MonaPipe, GND agency, TextGrid repository).

{{< carousel >}}
  {{<image img="gfx/image00001.png" context="carousel" license="Illustration: Paula Föhr"/>}}
  {{<image img="gfx/image00002.png" context="carousel" license="Illustration: Paula Föhr"/>}}
  {{<image img="gfx/image00003.png" context="carousel" license="Illustration: Paula Föhr"/>}}
  {{<image img="gfx/image00004.png" context="carousel" license="Illustration: Paula Föhr"/>}}
  {{<image img="gfx/image00005.png" context="carousel" license="Illustration: Paula Föhr"/>}}
  {{<image img="gfx/image00006.png" context="carousel" license="Illustration: Paula Föhr"/>}}
  {{<image img="gfx/image00007.png" context="carousel" license="Illustration: Paula Föhr"/>}}
{{< /carousel >}}


