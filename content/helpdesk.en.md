---
title: "Helpdesk"

aliases:
- /contact
- /kontakt
---

# Text+ Helpdesk
The helpdesk facilitates [direct access](helpdesk#contact-form) to [Text+ Consulting](daten-dienste/consulting). We offer individual assistance for all Text+ services as well as for text- and language-based research data.

---
#### Scope and goal
The consulting spectrum ranges from consultation on applications to general questions on research data management, from the discussion of research ideas and processes to specialised questions on specific tools and technologies across the entire data lifecycle. 

The purpose of the consulting services is to improve data quality in line with the FAIR principles, to propagate standardised, sustainable research practices and thus, ultimately, to increase the potential of your research projects.

---
#### Addressability
A contact form is available below for contacting the helpdesk.

Additionally, our [Text+ Research Rendezvous](https://events.gwdg.de/category/208/) are available for general or short-term enquiries. These are fortnightly open consultation hours for which no registration is required.

You can also contact us at any time during the various [events organised by and with Text+](aktuelles/veranstaltungen/). Your concerns will be forwarded on request. 

---
## Contact form
{{<support-form>}}

---
## Further Information
 [Text+ Consulting](daten-dienste/consulting) consists of a comprehensive network of experts from the [applying and participating institutions of Text+](/en/ueber-uns/antragstellende-beteiligte-institutionen/). If required, we can also establish contact with overarching NFDI structures as well as our national and international partners – above all the partner consortia [NFDI4Culture](https://nfdi4culture.de/helpdesk.html), [NFDI4Memory](https://4memory.de/) and [NFDI4Objects](https://www.nfdi4objects.net/index.php/en/help-desk). If you can already specify your request, simply assign it to the desired data domain or one of the suggested areas. For borderline cases, overarching or other concerns, simply select "General".


#### General
For general and cross-project enquiries about text- and language-based research data, Text+ and NFDI, as well as requests for cooperation.

#### Data domain Collections
Consultation on the creation, archiving and use of language- and text-based collections and research data – e.g. on the digitisation of analogue language- and text objects or on text- and data mining. Consulting on specific, collections-related services as well as general application advice (e.g. on data management plans).

#### Data domain Lexical Resources
Provides consulting on lexical resources, e.g. the standard-compliant digitisation and data curation of legacy collections and the empirical use of resources. Advice on specific services and data management plans for lexical resources.

#### Data domain Editions
Support for all types of editions from various fields of research: We provide consulting for all project and edition phases and for all data layers of scholarly editions. We also provide support with the input of editions into the Editions Registry and are always open to joint events and co-operations of all kinds. 

#### Infrastructure/Operations
Support on infrastructure services and cross-cutting issues such as data quality, interoperability, long-term archiving, reusability, registries and linked open data.

#### Technical support
Provides support for technical problems with the web portal and Text+ services.

#### Legal and Ethical Matters
Support with legal and ethical issues relating to text- and language-based research data. Please note that Text+ cannot provide legal advice.
