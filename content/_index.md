---
title: "Home"
type: landing
---

{{<call-to-action title="Text- und sprachbasierte Forschungsdaten nutzen und erhalten">}}
{{<lead-text>}}
Text+ stellt im Rahmen der NFDI einen umfangreichen Bestand an Textsammlungen und Korpora, Editionen und lexikalischen Ressourcen zur Nachnutzung bereit. Zum Angebotsportfolio gehört neben eigenen Daten und Diensten die Möglichkeit, Forschungsdaten der Community aufzunehmen und langfristig zu bewahren.
{{</lead-text>}}

{{<button-row align="end">}}
        {{<button url="/daten-dienste/daten">}}Daten suchen{{</button>}}
        {{<button url="/daten-dienste/depositing">}}Daten geben{{</button>}}
        {{<button url="/daten-dienste/dienste">}}Dienste{{</button>}}
        {{<button url="/daten-dienste/consulting">}}Beratung{{</button>}}
{{</button-row>}}

{{</call-to-action>}}

## Text+ und die NFDI

Die [DFG](https://www.dfg.de/) fördert im Rahmen der [Nationalen Forschungsdateninfrastruktur (NFDI)](https://www.dfg.de/de/foerderung/foerderinitiativen/nfdi) insgesamt 26 Konsortien aus den vier Bereichen der Geistes- und Sozialwissenschaften, Ingenierwissenschaften, Lebenswissenschaften und Naturwissenschaften. Der Verbund Text+ ist eines von sechs Konsortien aus den Humanities, das sind desweiteren [Berd@NFDI](https://www.berd-nfdi.de/), [KonsortSWD](https://www.konsortswd.de/), [NFDI4Culture](https://nfdi4culture.de/index.html), [NFDI4Memory](https://4memory.de/), [NFDI4Objects](https://www.nfdi4objects.net/). Während es über die [NFDI-Sektionen](https://www.nfdi.de/sektionen/) viele konsortienübergreifene Aktivitäten gibt, steht der Verbund Text+ mit den geistes- und sozialwissenschaftlichen Konsortien in besonders engem Austausch.

## Showcase

Der skizzierte Workflow bietet – am Beispiel der Übergabe von auf Disketten gespeicherten Forschungsdaten – einen Eindruck der Datenintegration in Text+. Er reicht vom Erstkontakt über das Consulting bis hin zur Aufnahme, Nachnutzbarmachung und langfristigen Bewahrung der Daten (Registry, FCS, MonaPipe, GND-Agentur, TextGrid Repository).

{{< carousel >}}
  {{<image img="gfx/image00001.png" context="carousel" license="Illustration: Paula Föhr"/>}}
  {{<image img="gfx/image00002.png" context="carousel" license="Illustration: Paula Föhr"/>}}
  {{<image img="gfx/image00003.png" context="carousel" license="Illustration: Paula Föhr"/>}}
  {{<image img="gfx/image00004.png" context="carousel" license="Illustration: Paula Föhr"/>}}
  {{<image img="gfx/image00005.png" context="carousel" license="Illustration: Paula Föhr"/>}}
  {{<image img="gfx/image00006.png" context="carousel" license="Illustration: Paula Föhr"/>}}
  {{<image img="gfx/image00007.png" context="carousel" license="Illustration: Paula Föhr"/>}}
{{< /carousel >}}
