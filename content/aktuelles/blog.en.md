---
title: Blog

aliases:
- /aktuelles-veranstaltungen/blog

menu:
  main:
    weight: 30
    parent: aktuelles

params:
  last_mod_today: true
---

# Text+ Blog
{{<image img="/aktuelles/Blog-Header.png" license=""/>}}

The [Text+ Blog](https://textplus.hypotheses.org/) deals with topics related to the scientific work with text- and language-based research data and provides a behind-the-scenes look at the consortium's work. A brief introduction to the goals and content of the blog is provided in the post [Text+ bloggt!](https://textplus.hypotheses.org/5) (DE only).

Here are the three most recent posts:

{{<blog-preview>}}
