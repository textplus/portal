---
# just a dummy file to set the title and menu

title: "News and Events"

menu:
  main:
    identifier: "aktuelles"
    weight: 50

build:
  render: never
---
