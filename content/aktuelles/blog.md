---
title: Blog

aliases:
- /aktuelles-veranstaltungen/blog

menu:
  main:
    weight: 30
    parent: aktuelles

params:
  last_mod_today: true
---

# Text+ Blog
{{<image img="/aktuelles/Blog-Header.png" license=""/>}}

Das [Text+ Blog](https://textplus.hypotheses.org/) beschäftigt sich mit Themen rund um die wissenschaftliche Arbeit mit text- und sprachbasierten Forschungsdaten und bietet einen Blick hinter die Kulissen des Konsortiums an. Eine kurze Vorstellung der Ziele und Inhalte des Blogs bietet der Beitrag [Text+ bloggt!](https://textplus.hypotheses.org/5).

Dies hier sind die drei aktuellsten Beiträge:

{{<blog-preview>}}
