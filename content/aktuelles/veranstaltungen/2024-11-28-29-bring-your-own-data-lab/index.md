---
type: event
title: "„Semantic Keyness für literarische Texte – Vergleich von Textgruppen“: Ein Bring-your-own-data-Lab"
start_date: 2024-11-28 14:00:00
end_date: 2024-11-29 13:30:00
all_day: false
location: Universität Trier
event_url: https://hermes-hub.de/events/intern/byodlab20241128.html
---

Im Rahmen des Datenkompetenzzenturm HERMES findet ein Bring-your-own-data-Lab zu „Semantic Keyness für literarische Texte – Vergleich von Textgruppen“ an der Universität Trier statt. Dr. Jan Oliver Rüdiger (IDS Mannheim) wird einen intensiven Einführungsworkshop geben, bei welchem die Teilnehmenden ihre eigenen Datensätze mit dem CorpusExplorer untersuchen und analysieren können. Jede Art von Vergleich von Textgruppen auf einer semantisch-konzeptuellen Ebene ist dabei denkbar und willkommen.

Am Ende der Veranstaltung werden die Teilnehmer*innen in der Lage sein, den CorpusExplorer zu nutzen und ihre eigenen Daten auf selbst gewählte Metadaten hin analysiert und visualisiert haben.

Eine **Anmeldung bis 22.11.** ist erforderlich, bei Interesse schreiben Sie an konstanciak [at] uni-trier.de.

## Weitere Informationen
Für [weitere Informationen hier](https://hermes-hub.de/events/intern/byodlab20241128.html) entlang.
