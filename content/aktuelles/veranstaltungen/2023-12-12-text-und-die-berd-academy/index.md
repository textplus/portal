---
type: event
title: "Text+ und die BERD Academy"
start_date: 2023-12-12 13:00:00
end_date: 2023-12-12 17:15:00
all_day: false
location: Seminargebäude Uni Leipzig, Universitätsstraße 1, 04109 Leipzig
---

Am 12. Dezember 2023 fand im Rahmen der BERD Academy in Leipzig der Workshop “[AI based Methods for Using Text as Data in the Social Sciences](https://www.berd-nfdi.de/berd-academy/aitext-2023/)” statt, in dem Angehörige der [Sächsischen Akademie der Wissenschaften zu Leipzig](https://www.saw-leipzig.de/) (SAW Leipzig), der [Universität Leipzig](https://www.uni-leipzig.de/) sowie des Center for Scalable Data Analytics and Artificial Intelligence ([ScaDS.AI](https://scads.ai/)) einen Überblick gaben über aktuelle Entwicklungen und konkrete Ansätze zur Arbeit mit Textdaten im Bereich Künstlicher Intelligenz.

Die BERD Academy bietet eine Reihe von Kursen, Workshops und anderen Bildungsmaterialien an, um Datenkompetenz im Allgemeinen und den Umgang mit unstrukturierten Daten im Besonderen zu fördern. Sie ist ein Angebot des Konsortiums [BERD@NFDI](https://www.berd-nfdi.de/), das eine leistungsfähige Plattform zur Sammlung, Verarbeitung, Analyse und Aufbereitung von Wirtschafts- und verwandten Daten im Kontext der Nationalen Forschungsdateninfrastruktur (NFDI) aufbaut.

Die auf dem Workshop vorgestellten Themen umfassten die Nutzung verschiedener Aufbereitungs- und Analysewerkzeuge für die qualitative und quantitative Erschließung von Textkollektionen. Weiterer Inhalt war die Bedeutung hochqualitativer Textdaten einerseits für die Erstellung komplexer Anwendungen, andererseits als Grundlage für Training und Finetuning von Sprachmodellen. Ebenso ging es um aktuelle Arbeiten im Bereich Active Learning und konkreten Hilfestellungen bei der Arbeit mit großen Sprachmodellen (Large Language Models, LLMs).

Blog Beitrag zum Event: https://textplus.hypotheses.org/9191