---
type: event
title: "Hans Kelsen Werke"
start_date: 2024-12-04T00:00:00+0100
end_date: 2024-12-04T23:59:59+0100
all_day: true
publishDate: 2024-12-04T02:00:00+0100
expiryDate: 2024-12-05T02:00:00+0100
featured_image: hans-kelsen-werke-adventskalender.png
location: Adventskalender
---

Wie der Übergang von klassisch erstellen gedruckten Monographien hin zum Single-Source-Verfahren auf XML-Basis gelingen kann, zeigen die [„Hans Kelsen Werke“](https://kelsen.online). Zudem werden die Forschungsdaten maschinenlesbar und nachhaltig nutzbar.

#textplus_advent #xml #tei #modeledition