---
type: event
title: "Wege für offenes Publizieren: Möglichkeiten der Zweitveröffentlichung"
start_date: 2024-02-05 13:30:00
end_date: 2024-02-05 15:00:00
all_day: false
location: virtuell
---

Open Access zu veröffentlichen, erhöht die eigene Sichtbarkeit, schafft
Vertrauen und Transparenz in der der Öffentlichkeit und fördert die Reproduzierbarkeit von
Forschung.

Trotzdem sind Fachzeitschriften in der Romanistik und anderen geisteswissenschaftlichen
Fächern weiterhin zumeist nicht offen zugänglich. Auch möchten viele Forschende nicht auf
den Service und das Renommee des Verlags und der Zeitschrift verzichten.

Die rechtlich gesicherte Möglichkeit, wissenschaftliche Artikel in periodisch
erscheinenden Sammlungen (Fachzeitschriften) nach Ablauf von 12 Monaten Open Access in der
Manuskriptfassung erneut zu veröffentlichen, ist vielen Forschenden nicht bekannt.

Wir zeigen im Workshop Wege auf, eigene Veröffentlichungen in offenen, frei zugänglichen
Repositorien zur Verfügung zu stellen. Dabei spielt es keine Rolle, ob es sich initial um
eine print oder digitale Publikation handelte.

Der Workshop wird vom FID Romanistik, der AG Digitale Romanistik im DRV (Deutscher
Romanistikverband) und der Max Weber Stiftung - Deutsche Geisteswissenschaftliche
Institute im Ausland in Kooperation mit dem NFDI-Konsortium Text+ veranstaltet.

[Eine Anmeldung ist hier möglich.](https://events.gwdg.de/event/586/)