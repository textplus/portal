---
title: "Editorik: Forschung und Infrastruktur im Dialog"
start_date: 2022-01-19 18:15:00
location: online
type: event

aliases:
- /events/editorik-forschung-und-infrastruktur-im-dialog/
---


Am Mittwoch, 19.1.2022, 18:15 Uhr gibt die SUB Göttingen gemeinsam mit weiteren Text+-Partner\*innen Einblicke in die Vielfalt und in die Herausforderungen digitaler Editorik im Kontext von Infrastruktur und Nachhaltigkeit. Die Teilnahme ist über Zoom möglich, eine Anmeldung ist bis zum 18.1.2022 erforderlich.

Neben Sammlungen und lexikalischen Ressourcen bilden digitale Editionen eine der drei Datendomänen in Text+. Ihnen kommt in geisteswissenschaftlichen Disziplinen und darüber hinaus eine zentrale Stellung zu, da sie sowohl Gegenstand als auch Ausgangspunkt text- und sprachbasierter Forschung sind. Die Vortragenden stellen Göttinger Editionsprojekte vor und informieren über die Partizipation an Text+ durch einzelne Editionsprojekte.

Anmeldungen zur Teilnahme an der Veranstaltung über Zoom werden per [E-Mail](mailto:office@text-plus.org) entgegengenommen.

Das [Programm](https://www.uni-goettingen.de/de/50226.html?cid=27662&date=2022-01-19) der Veranstaltung finden Sie im Veranstaltungskalender der Universität Göttingen verlinkt.
