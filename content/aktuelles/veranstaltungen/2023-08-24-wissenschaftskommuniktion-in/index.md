---
title: "Wissenschaftskommunikation in den Digital Humanities"
start_date: 2023-09-06 09:00:00
location: Virtuell via Zoom

all_day: false
type: event
featured_image: images/Wissenschaftskommunikation-in-den-Digital-Humanities.png
aliases:
- /events/wissenschaftskommuniktion-in-den-digital-humanities
---

Es ist soweit! Wir öffnen unsere Türen für den ersten Workshop zum Thema Wissenschaftskommunikation in den Digital Humanities. Der Workshop ist offen für alle, die sich für digitale Wissenschaftskommunikation interessieren.

Wir wollen mit euch über Wissenschaftskommunikation sprechen. Der Workshop ist für Einsteiger\*innen geeignet und bietet Einführungen zu (digitaler) Wissenschaftskommunikation, zur Zielgruppenanalyse und zu konkreten Formaten. Es wird viele praktische Einheiten geben, in denen ihr eure eigene Wissenschaftskommunikation vorbereiten könnt und Good und Bad Practices anderer Accounts analysiert. Abschließend kartieren wir gemeinsam die WissKomm-Aktivitäten und überlegen, welche Aktionen wir zusammen durchführen könnten.

  
Ziel ist es, dass du am Ende nicht nur weißt, was digitale Wissenschaftskommunikation ist und kann, sondern auch erste eigene Materialien entwickelt hast, die du für deine WissKomm-Aktivitäten nutzen kannst. Du musst kein AG-Mitglied sein, um am Workshop teilnehmen zu können, aber wir freuen uns natürlich immer über neue Mitglieder.
