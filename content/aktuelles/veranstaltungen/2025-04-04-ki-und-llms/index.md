---
type: event
title: "KI und Large Language Models: neue Impulse für die Hochschullehre in der Romanistik"
start_date: 2025-04-04 13:00:00
end_date: 2025-04-04 15:30:00
all_day: false
location: virtuell
event_url: https://events.gwdg.de/event/1064/
---
**Künstliche Intelligenz (KI) verändert die Hochschullehre – auch in der Romanistik**. Doch welche konkreten Anwendungsmöglichkeiten bieten Large Language Models (LLMs) und KI-gestützte Tools für die Lehre in der Sprach- und Literaturwissenschaft? Welche Chancen und Herausforderungen ergeben sich für Studierende und Lehrende im Umgang mit Sprache, Literatur und digitaler Forschungspraxis?

In diesem Workshop beleuchten Expert:innen aus verschiedenen Bereichen der Romanistik aktuelle KI-Entwicklungen und stellen Möglichkeiten zu deren Integration im Hochschulunterricht vor. Der Workshop wird veranstaltet von der [AG Digitale Romanistik](https://www.deutscher-romanistikverband.de/ag-digitale-romanistik/) (Verena Weiland und Ursula Winter) in Kooperation mit  [Text+](https://text-plus.org/).

### Zielgruppe
Der Workshop richtet sich an Lehrende, Forschende und Studierende der Romanistik sowie an alle Interessierten, die sich mit den Potentialen von KI in der geisteswissenschaftlichen Lehre auseinandersetzen möchten.

Wir freuen uns auf Ihre Teilnahme und einen spannenden Austausch!

### Anmeldung und weitere Informationen
Zur Anmeldung und für weitere Informationen bitte [hier](https://events.gwdg.de/event/1064/) entlang.

