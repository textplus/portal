---
type: event
title: "Werkstattreihe Standardisierung: INSeRT"
start_date: 2025-06-05 10:00:00
end_date: 2025-06-05 11:30:00
all_day: false
location: virtuell
#featured_image: 
---
Text+, das NFDI-Konsortium für die Text- und Sprachwissenschaften, lädt in einer neuen Reihe zum Austausch rund um Standardisierung von Forschungsdaten ein.

Anhand konkreter Anwendungsbeispiele erhalten die Teilnehmenden Einblicke in die Verwendung von Standards und standardbasierten Tools und können an den Erfahrungen der Vortragenden in ihren jeweiligen Projekten teilhaben. Ziel ist es, ihnen so die Planung und Umsetzung ihrer Vorhaben zu erleichtern.

Darüber hinaus soll die Werkstattreihe Weichen für zukünftige Datenintegrationen in die Text+ Infrastruktur stellen, die interne Reflektion über Angebotsentwicklung, Infrastruktur und Schnittstellen voranbringen sowie Partizipationsmöglichkeiten für Datengebende beleuchten.

Die Reihe ist eine gemeinsame Aktivität aller Task Areas in Text+ in Zusammenarbeit mit Kolleginnen und Kollegen aus der Community. 

## Die Anwendungsbeispiele
20. März: DTABf, Marius Hug, Frank Wiegand
11. April: correspSearch, Stefan Dumont
22. Mai: edition humboldt digital, Christian Thomas, Stefan Dumont
5. Juni: INSeRT, Felix Helfer, N.N.
17. Juli: PROPYLÄEN. Goethes Biographica, Martin Prell
18. September: DeReKo, N.N.
16. Oktober: Klaus Mollenhauer Ausgabe, Max Zeterberg
20. November: BERIA Collection, Isabel Compes

## Zielgruppe
Die Reihe spricht ein breites Publikum mit Bezug zu Sprach- und Textwissenschaften an. Sie ist sowohl für Neulinge, die einen ersten Einstieg in das Thema finden möchten (z.B. Promovierende, Forschende ohne Infrastrukturanbindung), aber auch für im Einsatz von Standards und Tools Versierte, die von Erfahrungen aus ähnlichen Vorhaben profitieren möchten bzw. ebenfalls ihre Erfahrungen mitteilen möchten, gleichermaßen geeignet.

## Anmeldung
Zur [Anmeldung bitte hier](https://events.gwdg.de/event/1022/) entlang.