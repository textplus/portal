---
type: event
title: "Social-Media-Daten: Show and Tell"
start_date: 2025-02-28 14:00:00
end_date: 2025-02-28 15:30:00
all_day: false
location: Virtuell
featured_image: csm_Alina_Grubnyak_Web_d8bb5f7bfb.jpeg
event_url: https://events.nfdi4culture.de/event/42/
---
Die Veranstaltungsreihe widmet sich in einer anderthalbstündigen Zoom-Session den Tools im Feld der Social Media-Forschung und beleuchtet Best Practices ausgewählter Projekte. Im Fokus stehen neben pragmatischen Lösungen und technischen Möglichkeiten (Schnittstellen, Repositorien, Metadatenstandards, Interoperabilität ...) auch ethische und rechtliche Herausforderungen (z.B. Persönlichkeits- und Urheberrechte) im nachhaltigen, sicheren und kritische Umgang mit diesen Daten (Code and Data Literacy, FAIR & CARE Prinzipien). Nicht zuletzt möchten wir dazu einladen, interdisziplinäre Forschungsansätze und Lehrmethoden zu diskutieren, die tradierte wie fachspezifische Rahmen und Werkzeuge strapazieren.

2025 wollen wir uns dezidiert mit dem Schwerpunkt Umgang mit Social Media Daten in der **Rechtsextremismus- und Demokratieforschung** beschäftigen.

Den Auftakt machen am **28.02.** um **14:00 Uhr** **[Christian Donner](https://bag-gegen-hass.net/team/christian-donner/)** von **[BAG »Gegen Hass im Netz«](https://bag-gegen-hass.net/)** und **[Pascal Siegers](https://www.gesis.org/institut/ueber-uns/mitarbeitendenverzeichnis/person/pascal.siegers)**, der das Datenportal für Rassismus- und Rechtsextremismusforschung **[DP-R|EX](https://www.gesis.org/forschung/drittmittelprojekte/details/project/187/dp-rex-das-datenportal-fuer-rassismus-und-rechtsextremismusforschung)** vorstellt.

Neben den beiden Projektpräsentationen stehen sieben übergeordnete Fragen zum Umgang mit Social-Media-Daten und die Diskussion mit der Community im Vordergrund.

Diese Reihe wird seit 2022 vom Arbeitskreis Social Media-Daten in der Forschungspraxis veranstaltet, eine Initiative von [NFDI4Culture](https://nfdi4culture.de/) gemeinsam mit [BERD@NFDI](https://www.berd-nfdi.de/), [KonsortSWD](https://www.konsortswd.de/) und [Text+](https://www.text-plus.org/) im Rahmen der [Nationalen Forschungsdateninfrastruktur](https://www.nfdi.de/).