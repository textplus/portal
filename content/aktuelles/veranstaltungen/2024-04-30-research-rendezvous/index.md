---
type: event
title: "Text+ Research Rendezvous"
start_date: 2024-04-30 09:00:00
end_date: 2024-04-30 10:00:00
all_day: false
location: Virtuell
---

Das NFDI-Konsortium Text+ bietet seit Oktober zusätzlich zum fortlaufenden Beratungsangebot eine virtuelle Sprechstunde des Helpdesks an.

Im zweiwöchigen Rhythmus können bei „Text+ Research Rendezvous“ Fragen rund um Sammlungen, Editionen und Lexikalische Ressourcen sowie aus dem Bereich Infrastruktur/Betrieb zu festen Terminen (dienstags 9–10 Uhr bzw. donnerstags 10–11 Uhr) direkt mit Mitarbeitenden aus Text+ besprochen werden. Für umfangreichere Fragen werden gerne individuelle (Folge-)Termine vereinbart. Eine Anmeldung zur Sprechstunde ist nicht erforderlich. Ein Zutritt zur Sprechstunde ist während der gesamten Stunde möglich.

Für Fragen zur virtuellen Sprechstunde sowie anderen Beratungsmöglichkeiten steht Ihnen das Team des Text+ Helpdesks über das Formular unter https://www.text-plus.org/kontakt/ gerne jederzeit zur Verfügung.

Nächste Termine der Reihe: 16.5. (10–11 Uhr), 28.5. (9–10 Uhr), 13.6. (10–11 Uhr), 25.6. (9–10 Uhr), 11.7. (10–11 Uhr), 23.7. (9–10 Uhr), 8.8. (10–11 Uhr), 20.8. (9–10 Uhr), 5.9. (10–11 Uhr), 17.9. (9–10 Uhr), 15.10. (10–11 Uhr), 31.10. (9–10 Uhr), 12.11. (10–11 Uhr), 28.11. (9–10 Uhr), 10.12. (10–11 Uhr)

https://events.gwdg.de/category/208/