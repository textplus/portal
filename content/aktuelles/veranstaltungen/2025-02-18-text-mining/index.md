---
type: event
title: "Einführung in das Text Mining"
start_date: 2025-02-18 17:00:00
end_date: 2025-02-18 18:30:00
all_day: false
location: Stuttgart,  Württembergische Landesbibliothek
event_url: https://kurse.wlb-stuttgart.de/course/view.php?id=63
---
Text Mining ist ein zentrales Thema in vielen Bereichen von Text+. An der Württembergischen Landesbibliothek in Stuttgart wird für Nutzende der Bibliothek zusammen mit Text+ eine Einführung in das Text-Mining angeboten.
