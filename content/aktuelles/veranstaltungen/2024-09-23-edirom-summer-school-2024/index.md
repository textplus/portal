---
type: event
title: "Edirom Summer School 2024"
start_date: 2024-09-23 09:15:00
end_date: 2024-09-27 12:45:00
all_day: true
location: Paderborn
summary: "Das Zentrum Musik – Edition – Medien (ZenMEM) und der Virtuelle Forschungsverbund Edirom (ViFE) laden sehr herzlich ein zur diesjährigen 15. Edirom Summer School (ESS) vom 23. bis 27. September im Heinz-Nixdorf-Institut der Universität Paderborn. Die ESS wird in Kooperation mit dem Konsortium Text+ durchgeführt."
---

Das Zentrum Musik – Edition – Medien (ZenMEM) und der Virtuelle Forschungsverbund Edirom (ViFE) laden sehr herzlich ein zur diesjährigen 15. Edirom Summer School (ESS) vom 23. bis 27. September im [Heinz-Nixdorf-Institut der Universität Paderborn (HNI)](https://www.hni.uni-paderborn.de/). Die ESS wird in Kooperation mit dem Konsortium Text+ durchgeführt.

**Keynote**

Zum Auftakt spricht Dr. Tessa Gengnagel am 23. Sept. ab 18:15 Uhr im Foyer des HNI (Fürstenallee 11) über „Walzerkrieg und Maschinengott: Zur Edition multimedialer Werkkomplexe am Beispiel von Film und Musik“. Zur öffentlichen Keynote sind Sie auch ohne vorherige Anmeldung sehr herzlich eingeladen. Die Keynote wird digital gestreamt, der Link wird noch bekanntgegeben.

**Programm**

Das [umfangreiche Kursprogramm](https://ess.uni-paderborn.de/2024/programm.html) rund um Digitale Edition, digitales Arbeiten in den Kultur- und Geisteswissenschaften, Textkodierung und Musikkodierung richtet sich an Interessierte aller Fächer.

**Anmeldung**

Die [Anmeldung zur ESS](https://ess.upb.de/2024/registrierung.html) ist noch bis zum 18. September möglich. Warten Sie aber nicht zu lange, denn wir können in diesem Jahr aus Kapazitätsgründen auch bei hoher Nachfrage keine Zusatzkurse anbieten! Für Studierende der UPB, die die ESS als Lehrveranstaltung (Blockseminar) besuchen, sind die Kurse kostenlos.
  
**Poster-Session**

Sie haben ein Projekt, eine Idee oder möchten einfach zeigen woran Sie arbeiten? Dann nichts wie raus aus dem Elfenbeinturm! Stellen Sie einfach ein Poster zusammen und bringen Sie es mit zur ESS. Wir freuen uns über alle Poster-Vorschläge, die uns (formlos) über
ess@edirom.de erreichen.

[Weitere Information](https://ess.uni-paderborn.de/)

Für Rückfragen ist das Orga-Team der Edirom-Summer-School unter ess(at)edirom(dot)de zu erreichen.