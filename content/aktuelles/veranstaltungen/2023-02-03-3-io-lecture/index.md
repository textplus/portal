---
type: event
title: "3. IO-Lecture: Redaktionsworkflow (GitLab-Pipeline)"
start_date: 2023-02-03 10:00:00
end_date: 2023-02-03 12:00:00
aliases:
- /events/3-io-lecture-redaktionsworkflow-gitlab-pipeline
---

Die 3. IO-Lecture findet am Mittwoch, dem 15. Februar von 10 bis 12 Uhr statt und widmet sich dem „Redaktionsworkflow Webportal (GitLab-Pipeline)“. Sie richtet sich somit an Kolleginnen und Kollegen, die bei der Pflege des zukünftigen Text+ Webportals mitwirken wollen. Alexander Steckel (SUB Göttingen) und Maik Wegener (GWDG) werden als Referenten den Workflow, das Redaktionsmodell sowie die zu nutzende Arbeitsumgebung vorstellen. Eine Teilnahme/Kenntnis der beiden vorangegangenen IO-Lectures ist nützlich, aber nicht erforderlich.
