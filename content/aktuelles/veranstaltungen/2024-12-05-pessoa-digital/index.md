---
type: event
title: "Pessoa Digital"
start_date: 2024-12-05T00:00:00+0100
end_date: 2024-12-05T23:59:59+0100
all_day: true
publishDate: 2024-12-05T02:00:00+0100
expiryDate: 2024-12-06T02:00:00+0100
featured_image: pessoa-adventskalender.png
location: Adventskalender
---

Im Rahmen der [Kooperationsprojekte 2023](https://zenodo.org/records/10257440) konnte die digitale Edition [„Fernando Pessoa“](https://www.pessoadigital.pt/en) die technische Infrastruktur aktualisieren und die nachhaltige Entwicklung dieses Langzeitprojekts verbessern.

#textplus_advent #modernismos #fernandopessoa #digitaledition