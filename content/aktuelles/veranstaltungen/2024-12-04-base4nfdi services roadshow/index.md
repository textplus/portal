---
type: event
title: "Base4NFDI Services Roadshow"
start_date: 2024-12-04 14:00:00
end_date: 2024-12-04 16:30:00
all_day: false
location: Virtuell
event_url: https://base4nfdi.de/news-events/events/services-roadshow-by-base4nfdi-12-2024
featured_image: services roadshow.png
---
Am Mittwoch, 4. Dezember, ab 14:00 Uhr veranstaltet das Projekt Base4NFDI seine erste „Services Roadshow“. In jeweils kurzen 15min-Slots stellen sich dabei alle bisher geförderten Basisdienstkandidaten vor. Nutzen Sie bei Interesse gerne diese Gelegenheit, einen Überblick über aktuelle Basisdienstentwicklungen zu erhalten.
