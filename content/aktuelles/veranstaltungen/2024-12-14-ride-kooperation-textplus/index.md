---
type: event
title: "RIDE Kooperation Text+"
start_date: 2024-12-14T00:00:00+0100
end_date: 2024-12-14T23:59:59+0100
all_day: true
publishDate: 2024-12-14T02:00:00+0100
expiryDate: 2024-12-15T02:00:00+0100
featured_image: ride-koop-adventskalender.png
location: Adventskalender
---

[Text+](https://text-plus.org) veröffentlichte gemeinsam mit dem [IDE](https://www.i-d-e.de/) 2 [RIDE](https://ride.i-d-e.de/)-Bände. Es werden digitale Editionen mit Blick auf die Anwendung der FAIR-Prinzipien rezensiert. [#issue16](https://ride.i-d-e.de/issues/issue-16/) [#issue17](https://ride.i-d-e.de/issues/issue-17/)

#textplus_advent #digitaleditions 