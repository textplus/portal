---
type: event
title: "edition2LD"
start_date: 2024-12-23T00:00:00+0100
end_date: 2024-12-23T23:59:59+0100
all_day: true
publishDate: 2024-12-23T02:00:00+0100
expiryDate: 2024-12-24T02:00:00+0100
featured_image: bhimasena-thapa.png
location: Adventskalender
---

Im Rahmen der [Kooperationsprojekte](https://text-plus.org/vernetzung/kooperationsprojekte/) erarbeitete [edition2LD](https://textplus.hypotheses.org/8723) Workflows, um interoperable Daten aus heterogenen Ressourcen, Sprachen/Sprachstufen, Datentypen/-formaten zu kuratieren.
 
#textplus_advent #LOD #LinkedData #DigitalEdition #Interoperability