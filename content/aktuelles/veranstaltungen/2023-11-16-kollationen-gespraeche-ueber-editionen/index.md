---
type: event
title: "Kollationen: Gespräche über Editionen - Digital und/oder gedruckt?"
start_date: 2023-11-16 18:00:00
end_date: 2023-11-16 19:30:00
all_day: false
location: Lesesaal, Emil-Abderhalden-Str. 36, 06108 Halle (Saale) und online
featured_image: images/csm_2023_Kollationen_Gespräche_über_digitale_Editionen_web_e28b59aff7.jpg
---

Im zweiten vorchristlichen Jahrhundert beginnen mit der kritischen Sichtung der Überlieferung und Erstellung allgemein verbindlichen Textausgaben der Homer-Epen erste editorische Praktiken, die relevant für die Entwicklung des Wissenschaftssystems sind. Insbesondere historische, mit Texten arbeitende Wissenschaften sind auf zuverlässige Textzeugen angewiesen. So forderte es bereits Karl Lachmann, einer der Begründer der modernen Editionsphilologie, Mitte des 19. Jahrhunderts. Die Edition stellt mithin eben jenen zuverlässigen Text dar, weil sie intersubjektiv verstehbar macht, auf welche Quellen sie sich bezieht und wie sie mit diesen verfahren ist. Wandelte sich die Arbeit von Editorinnen und Editoren mit der flächendeckenden Einführung des Buchdrucks technisch wie konzeptionell, so steht seit der Digitalisierung die nächste große Veränderung an. Nach wie vor bestehen sowohl digitale als auch gedruckte Editionen parallel nebeneinander, die digitale Edition scheint aber auf dem Vormarsch zu sein. Die recht große Anzahl an Hybrid-Editionen lässt vermuten, dass die gedruckte Edition nicht vollständig von der digitalen abgelöst werden kann.

Was genau sind die Möglichkeiten, die der Medienwechsel in den digitalen Raum mit sich bringt? Wie unterscheiden sich digitale und gedruckte Editionen sowohl produktions- als auch rezeptionsseitig? Geht bei der digitalen Edition auch etwas gegenüber der Druckedition verloren? Diesen Fragen widmet sich die Diskussion „Kollationen: Gespräche über (digitale) Editionen – Gedruckte oder digitale Edition?“

Unter dem Begriff „Kollation“ versteht die Editionswissenschaft den zeichengenauen Vergleich mehrere Überlieferungen eines Textes. Ziel ist es, Abweichungen und Übereinstimmungen zu ermitteln. Das Zentrum für Wissenschaftsforschung will diesem Prinzip mit einem neuen Veranstaltungsformat begegnen: Zwei Personen aus der Wissenschaft führen zu einem bestimmten Thema ein moderiertes Gespräch. Die Gesprächspartnerinnen bzw. -partner werden so ausgewählt, dass sie (mutmaßlich) unterschiedliche Perspektiven auf ein Thema haben. Im Anschluss an das etwa 45-minütige Gespräch wird die Runde für Beiträge aus dem Publikum geöffnet.

Die Diskussion widmet sich der Gegenüberstellung „Digitale und/oder gedruckt?“ mit diesen Gästen:

- Tessa Gengnagel ist Mitgeschäftsführerin des Cologne Center for eHumanities (CCeH) und national wie auch international langjährig erfahren im Umgang mit digitalen Editionen. Ihre Expertise bringt sie unter anderem in die Task Area „Editions“ des NFDI-Konsortiums Text+ ein. In ihrer eigenen Forschung befasst sie sich mit reflektierenden Fragen der (digitalen) Editionswissenschaft sowie der Theorie digitaler Geisteswissenschaften.

- Prof. Dr. Dieter Burdorf hat den Lehrstuhl für Neuere deutsche Literatur und Literaturtheorie an der Universität Leipzig inne. Neben seiner Arbeit zu literarischen Gattungen – insbesondere der Lyrik –, forscht er vor allem im Bereich der Briefedition und hat sich mit der Edition der Lyrik Friedrich Hölderlins auseinandergesetzt, die in der Geschichte der Editionswissenschaft eine herausgehobene Rolle einnimmt.

Weitere Informationen: https://www.leopoldina.org/veranstaltungen/veranstaltung/event/3117/