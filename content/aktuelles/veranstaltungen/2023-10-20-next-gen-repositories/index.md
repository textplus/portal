---
type: event
title: "Next Gen Repositories – Erfahrungen, Herausforderungen und Möglichkeiten"
start_date: 2023-10-20 09:15:00
end_date: 2023-10-20 16:00:00
all_day: false
location: Marburg
aliases:
- /events/next-gen-repositories
---

Repositorien gehören mittlerweile zu den wichtigsten Forschungsinfrastrukturen. Durch ihre
kontinuierliche technische Weiterentwicklung tragen sie neue Möglichkeiten des Forschens in
die Wissenschaft hinein, wie auch von der Wissenschaft neue Anforderungen und Bedarfe an
sie herangetragen werden. Dieses Wechselverhältnis zwischen technischer Innovation und
wissenschaftsgetriebener Forschungsinfrastrukturentwicklung wird besonders virulent, wenn
eine neue Generation von Repositorien in Erscheinung tritt, die neue Möglichkeiten der
Vernetzung bietet, aber auch technische und fachliche Herausforderungen mit sich bringt. Der
Workshop möchte deshalb drei Themenfelder in den Blick nehmen, die in diesem Kontext
besonders relevant erscheinen wie die Frage nach der Interoperabilität, den
Gestaltungsmöglichkeiten, die sich aus technischer Innovation wie fachlichen Bedarfen
ergeben sowie die technischen Herausforderungen, die entstehen, wenn es gilt,
Forschungsinfrastrukturen neu zu gestalten.


_Workshop des Reopositoriums für Medienwissenschaft media/rep/  
Forschungszentrum Deutscher Sprachatlas  
Pilgrimstein 16  
35037 Marburg  
20.10.2023_

9:15 – 10:15 Registrierung & Kaffee  
10:15 – 10:45 media/rep/ goes Next Gen  
10:45 – 12:15 Panel 1: Interoperabilität und Semantic Web  
12:15 – 13:15 Mittagsimbiss  
13:15 – 14:45 Panel 2: Next Gen Repositories und Gestaltungsmöglichkeiten  
14:45 – 15:00 Kaffeepause  
15:00 – 15:45 Panel 3: Next Gen Repositories und der Blick in den ‚Maschinenraum‘  
15:45 – 16:00 Schlussworte und Verabschiedung  


Zur Anmeldung für den Workshop schicken Sie bitte eine Mail an hayriye.kapusuz@uni-marburg.de

Kontakt:

**Dr. Hayriye Kapusuz** E-Mail: [hayriye.kapusuz@uni-marburg.de](mailto:hayriye.kapusuz@uni-marburg.de,)


Informationen: https://www.uni-marburg.de/de/fb09/medienwissenschaft/forschung/forschungsprojekte/mediarep/aktuelles/nachrichten/workshop-next-gen-repositories
