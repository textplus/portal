---
type: event
title: "IO-Lecture: Wie kommt mein Dienst ins Portal?"
start_date: 2024-12-04 11:00:00
end_date: 2024-12-04 12:00:00
all_day: false
location: Virtuell
event_url: https://events.gwdg.de/event/980/
---
Text+ informiert auf seinem Portal auf einer [eigenen Seite über Dienste und Werkzeuge](daten-dienste/dienste/), die entweder im Rahmen von Text+ entwickelt/bereitgestellt werden oder aber von den beitragenden Partnereinrichtungen aus anderen Kontexten stammen, aber für die Community von Interesse sind und genutzt werden dürfen.

In der Lecture soll es dabei vor allem darum gehen, wie wir die oben verlinkte „Diensteliste“ weiter ausbauen und einzelne Einträge anreichern können. Das heißt, wenn jemand einen für die Text+ Community interessanten Dienst oder Werkzeug anbieten möchte, sollte sie/er zu uns in die Lecture kommen.

## Zielgruppe der IO-Lecture
Die Veranstaltung richtet sich an Personen, die Forschung innerhalb der Sozial- und Geisteswissenschaften betreiben, bei einer Infrastruktureinrichtung des Feldes tätig sind oder einfach Interesse am Thema haben. Darüber hinaus freuen wir uns insbesondere über Teilnehmende aus anderen NFDI-Konsortien.
