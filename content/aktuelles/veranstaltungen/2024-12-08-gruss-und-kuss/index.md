---
type: event
title: "Gruß & Kuss"
start_date: 2024-12-08T00:00:00+0100
end_date: 2024-12-08T23:59:59+0100
all_day: true
publishDate: 2024-12-08T02:00:00+0100
expiryDate: 2024-12-09T02:00:00+0100
featured_image: gruss-kuss-adventskalender.jpeg
featured_image_caption: "Bild: Liebesbriefarchiv / Nadine Dietz."
location: Adventskalender
---

Bürger:innen und Wissenschaftler:innen in Zusammenarbeit? [Gruß & Kuss](https://liebesbriefarchiv.de/projekt-gruss-kuss/) ist ein Citizen-Science-Projekt, das interessierte Bürger:innen einbezieht, zu untersuchen, lesen, digitalisieren und erforschen eine einzigartige und bislang unzugängliche Quelle der Alltagskultur: ein Archiv authentischer privater [Liebesbriefe](https://liebesbriefarchiv.de/liebesbriefarchiv/).

#textplus_advent #citizenscience #liebesbrief #archiv #digitalhumanities