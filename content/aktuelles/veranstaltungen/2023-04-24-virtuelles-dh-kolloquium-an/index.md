---
type: event
title: "Virtuelles DH-Kolloquium an der BBAW"
start_date: 2023-04-24 16:15:00
all_day: false
aliases:
- /events/virtuelles-dh-kolloquium-an-der-bbaw-5
---

Im Rahmen des DH-Kolloquiums an der BBAW laden wir Sie herzlich zum nächsten Termin am Montag, den 24. April 2023, 16 Uhr c.t., ein (virtueller Raum: https://meet.gwdg.de/b/lou-eyn-nm6-t6b):

**Dr. Katrin Henzel (Universität Kiel)**

**Digitale Editionen inklusiv**

Barrierefreiheit in Editionen ist aktuell durch wissenschaftliche Veranstaltungsreihen (wie des von Text+ durchgeführten _FAIR February 2023_)[\[1\]](https://dhd-blog.org/?p=19268#_ftn1) verstärkt in den Fokus gerückt. Konsens besteht darin, dass ein freier Zugang zu Editionen gemäß den FAIR-Prinzipien auch Inklusion beinhaltet. Wie sich digitale Barrierefreiheit umsetzen lässt, bedarf einer komplexen Antwort. Dies erklärt sich daraus, dass Behinderungen – legt man Art. 1, Satz 2 der UN-Behindertenrechtskonvention zugrunde – „in Wechselwirkung mit verschiedenen Barrieren an der vollen, wirksamen und gleichberechtigten Teilhabe an der Gesellschaft hindern können.“[\[2\]](https://dhd-blog.org/?p=19268#_ftn2) Inklusion als aktive und partizipative gleichberechtigte Einbindung von Menschen mit Behinderungen verstanden, ist demzufolge prozessbezogen und situativ abhängig.

Welche Konsequenzen sich daraus für die barrierearme Gestaltung von Editionen ergeben (können), soll im Vortrag aufgezeigt und anschließend diskutiert werden. Zwei Schwerpunkte werden gesetzt:

1\. Geltungsbereiche

Digitale Editionen stellen Daten öffentlich auf einer graphischen Benutzeroberfläche bereit, für die es insbesondere mit den _Web Content Accessibility Guidelines_ (WCAG) 2.1 (2018) konkrete Empfehlungen für die barrierefreie Gestaltung gibt. Allerdings fehlt es an vergleichbaren Richtlinien für die Datengenerierung, deren Auffindbarkeit und Nachnutzung jenseits der Benutzeroberfläche (Henzel 2022). Im Forschungsdatenmanagement, insbesondere in Österreich, gibt es hierzu erste Ansätze (etwa Blumenberger 2019) und Initiativen, die im Vortrag vorgestellt werden.

Gleichermaßen gilt es bei digitalen Editionen die Rahmenbedingungen, konkret die Arbeitsbedingungen in den Editionsprojekten, auf Inklusion zu prüfen. Damit werden auch ethische Aspekte von Editionen thematisiert.

2\. Methoden der Evaluierung von Barrierefreiheit

Für die Prüfung digitaler Barrierefreiheit gibt es Software, die die rechtlichen Vorgaben und daraus abgeleitete Richtlinien zur Barrierefreiheit von Weboberflächen standardisiert prüfen kann. Doch sollten neben der Erhebung quantitativer Daten mithilfe standardisierter Verfahren (vgl. bereits Hegner 2003, S. 16–18) auch qualitative Methoden zur Feststellung subjektiv wahrgenommener digitaler Barrieren zum Einsatz kommen, um dem partizipativen Anspruch von Open Science gerecht zu werden. Wie dies geschehen könnte, wird ebenfalls im Vortrag skizziert.

**Referenzen**

Blumesberger, Susanne: Barrierefreiheit und Repositorien – Nachdenken über Open Science für alle. In: b.i.t. online 22/4 (2019), S. 297–302.

Hegner, Marcus: Methoden zur Evaluation von Software. Hg. v. Informationszentrum Sozialwissenschaften der Arbeitsgemeinschaft Sozialwissenschaftlicher Institute e.V. (ASI). Bonn 2003 (IZ-Arbeitsbericht Nr. 29).

Henzel, Katrin: Vermittlung auf Augenhöhe – digitale Editionen inklusiv gestaltet. In: editio 36 (2022), S. 72–88, doi: [https://doi.org/10.1515/editio-2022-0003](https://doi.org/10.1515/editio-2022-0003).

W3C: Web Content Accessibility Guidelines (WCAG) 2.1. Veröffentlicht am 5.6.2018, [https://www.w3.org/TR/WCAG21/](https://www.w3.org/TR/WCAG21/)

[\[1\]](https://dhd-blog.org/?p=19268#_ftnref1) [https://events.gwdg.de/event/413/](https://events.gwdg.de/event/413/)

[\[2\]](https://dhd-blog.org/?p=19268#_ftnref2) [https://www.bmas.de/SharedDocs/Downloads/DE/Teilhabe/uebereinkommen-ueber-die-rechte-behinderter-menschen.pdf?\_\_blob=publicationFile&v=2](https://www.bmas.de/SharedDocs/Downloads/DE/Teilhabe/uebereinkommen-ueber-die-rechte-behinderter-menschen.pdf?__blob=publicationFile&v=2)

**\*\*\***

Die Veranstaltung findet virtuell statt; eine Anmeldung ist nicht notwendig. Zum Termin ist der virtuelle Konferenzrraum über den Link [**https://meet.gwdg.de/b/lou-eyn-nm6-t6b**](https://dhd-blog.org/_wp_link_placeholder) erreichbar. Wir möchten Sie bitten, bei Eintritt in den Raum Mikrofon und Kamera zu deaktivieren. Nach Beginn der Diskussion können Wortmeldungen durch das Aktivieren der Kamera signalisiert werden.

Der Fokus der Veranstaltung liegt sowohl auf praxisnahen Themen und konkreten Anwendungsbeispielen als auch auf der kritischen Reflexion digitaler geisteswissenschaftlicher Forschung. Weitere Informationen finden Sie auf der [Website der BBAW](https://www.bbaw.de/bbaw-digital/dh-kolloquium).
