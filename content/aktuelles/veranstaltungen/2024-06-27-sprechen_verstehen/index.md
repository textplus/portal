---
type: event
title: "Sprechen verstehen: KI und gesprochene Sprache"
start_date: 2024-06-27 12:00:00
end_date: 2024-06-28 16:00:00
all_day: false
location: München, Stiftung Lyrik Kabinett
---
Künstliche Intelligenz revolutioniert das Generieren, Analysieren und
Transkribieren gesprochener Sprache. Wir sind dabei – und führen in Keynote-Vorträgen in
das Thema ein und stellen aktuelle Projekte, Tools und Verfahren zur Verarbeitung
gesprochener Sprache vor.

Weitere Informationen und Anmeldung unter https://events.gwdg.de/event/630/
