---
type: event
title: "Wohin damit? Storing and reusing my language data"
start_date: 2023-06-22 09:00:00
end_date: 2023-06-22 16:00:00
all_day: false
location: Leibniz-Institut für Deutsche Sprache
aliases:
- /events/wohin-damit-storing-and-reusing-my-language-data
---


DFG- und andere Forschungsprojekte stehen vor der Frage, wer ihre Daten langfristig im Rahmen der FAIR-Prinzipien entgegennehmen kann und unter welchen Voraussetzungen dies möglich ist. Dieser Marktplatz bietet für Forschende die Möglichkeit, sich direkt mit Daten- und Kompetenzzentren der Text+-Datendomäne Sammlungen auszutauschen, um nach Möglichkeiten der Datenübernahme zu suchen. Daneben werden wir anhand von einigen Beispielen vorstellen, wie eine Datenübernahme erfolgreich erfolgt ist.

Weitere Informationen und Anmeldung: https://events.gwdg.de/event/372/