---
type: event
title: "InFoDiTexT+ Vortrag: Mehr als nur Lückenfüller. Das Gender Data Gap als dringendes Thema für historische Disziplinen und Digital Humanities"
start_date: 2023-05-24 17:15:00
end_date: 2023-05-24 18:45:00
all_day: false
location: online
featured_image: images/InFoDiText2023_SarahLang.png
aliases:
- /events/infoditext-vortrag-mehr-als-nur-luckenfuller-das-gender-data-gap-als-dringendes-thema-fur-historische-disziplinen-und-digital-humanities
---

_Referentin: Sarah Lang, Universität Graz, ZIM_

Mit Manifesten wie "Data Feminism" (D'Ignazio/Klein 2020) und auch dem durchaus nicht unproblematischen Buch "Invisible Women" (Criado-Perez 2020) bekam das Thema des Gender Data Gap in den letzten Jahren mehr und mehr öffentliche Aufmerksamkeit. Doch die Umsetzung im Kontext der DH ist insofern komplexer als in zeitgenössischen Datensätzen, als dass man nicht überlieferte Daten ja nicht einfach neu erheben kann. Was wir trotzdem tun können und sollten, ist Thema dieses Vortrags.

Anmeldung: https://events.gwdg.de/event/449/