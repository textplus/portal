---
type: event
title: "Leipzig Research Data Forum"
start_date: 2024-09-23 09:00:00
end_date: 2024-09-23 16:30:00
all_day: false
location: Bibliotheca Albertina, Universitätsbibliothek Leipzig, Beethovenstraße 6, 04107 Leipzig
event_url: https://scads.ai/event/leipzig-research-data-forum/
summary: "Das Leipziger Forschungsdatenforum bringt die lebendige RDM-Community in Leipzig zusammen und bietet eine Plattform, um Kontakte zu knüpfen, Erkenntnisse auszutauschen und neue Möglichkeiten der Zusammenarbeit zu erkunden."
---

Am 23.09.2024 fand in der [Bibliotheca Albertina](https://www.ub.uni-leipzig.de/standorte/bibliotheca-albertina/) das [Leipzig Research Data Forum](https://scads.ai/event/leipzig-research-data-forum/) statt. Eine exklusive Networking-Veranstaltung für Experten für Forschungsdatenmanagement (RDM) aus den Nationalen Forschungsdateninfrastrukturen (NFDI) und anderen Organisationen und diejenigen, die ihr Fachwissen in diesem Bereich vertiefen mochten. Die Veranstaltung brachte die lebendige RDM-Community in Leipzig zusammen und bot eine Plattform, um Kontakte zu knüpfen, Erkenntnisse auszutauschen und neue Möglichkeiten der Zusammenarbeit zu erkunden.

Der Vormittag begann mit Pitch-Talks von Vertretern der einzelnen NFDIs und anderer Forschungsgruppen in Leipzig. Erik Körner von der Sächsischen Akademie der Wissenschaften zu Leipzig stellte dabei das Konsortium Text+ vor. Neben der Poster-Session gab es regen Austausch in den Kaffeepausen.

Am Nachmittag standen strukturierte Gruppendiskussionen auf dem Programm, die darauf abzielten, Synergien zu ermitteln und Kooperationen zu initiieren. Diese Diskussionen konzentrierten sich auf Themen, die von den Teilnehmern eingebracht wurden und die sich während der Vormittagssitzungen herauskristallisiert hatten. Es wurde sich auf spezifische Herausforderungen konzentriert, um aufbauend auf gebündeltem Fachwissen und Erfahrungen gemeinsame Ideen und Pläne für die Zukunft zu entwickeln. Ziel des Nachmittags war es, umsetzbare Ergebnisse zu erzielen, die den Grundstein für künftige Partnerschaften innerhalb der Forschungsdatenmanagement-Community in Leipzig legen.

Weiterführende Informationen: https://scads.ai/event/leipzig-research-data-forum/
