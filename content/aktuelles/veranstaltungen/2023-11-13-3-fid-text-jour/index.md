---
type: event
title: "3. FID / Text+ Jour Fixe"
start_date: 2023-11-13 13:00:00
end_date: 2023-11-14 13:00:00
all_day: true
location: Köln
type: event
featured_image: jffidtextplus.png
---

**3. FID / Text+ Jour Fixe live in Köln!**

Die gemeinsam etablierte Veranstaltungsreihe FID/Text+ Jour Fixe geht in die dritte Runde! Wir treffen uns dieses Mal am 13./14. November vor Ort in Köln.

Ein von der Task Area Editions zur Verfügung gestelltes Budget erlaubt uns freundlicherweise die Erstattung von Reise- und Übernachtungkosten für Vertreter:innen der FIDe.  
  
Eine [Anmeldung zur Veranstaltung](https://events.gwdg.de/event/530/) ist ab sofort möglich.

**Was ist nochmal der FID/Text+ Jour Fixe?**  
Die Reihe wurde von der AG FID Koop im NFDI Konsortium Text+ angestoßen, um beide Strukturen zu vernetzen und der Forschung ein gemeinsam abgestimmtes Angebot an Expertise, Ressourcen und Services zur Verfügung zu stellen. 
Nach der Auftaktveranstaltung im Oktober 2022 zum Schwerpunkt "Erwartungsmanagement" haben wir beim 2. Termin das Thema "Consulting" in den Fokus gerückt. Seitdem haben sich eine ganze Menge konkreter Formen der Zusammenarbeit entwickelt:  
Vertreter:innen von FIDen sind Mitglieder der Beiräte (SCC/OCC) von Text+. Sie gestalten die Ausrichtung des Konsortiums mit und begutachten Förderanträge, die jährlich im Rahmen der sogenannten "Text+ Kooperationsprojekte" vergeben werden. Wir haben uns zudem personaltechnisch über Matrixstellen vernetzt und unsere Zusammenarbeit auf der ersten NFDI Gesamtkonferenz (CoRDI) vom 12.-14. September in Karlsruhe vorgestellt (Poster | Abstract). Kolleg:innen verpartneter FIDe sind auf nahezu jeder Veranstaltung von Text+ vertreten.  

**Worum geht es beim 3. Jour Fixe?**  
Der nächste Jour Fixe soll die verschiedenen Formen unserer Zusammenarbeit möglichst konkret vorantreiben und zugleich neuen Partnern die Möglichkeit eines guten Einstiegs bieten.  
Das Spektrum der Workshopformate ist daher breit angelegt und reicht von Kleingruppenarbeit - bspw. die konkrete Testeingabe von Ressourcen in das Datenmodell der Registry von Text+ - über Impulse im Plenum zum Thema Normdaten bis hin zu freien Diskussionen im Barcampformat über gemeinsame Einreichungen oder Veranstaltungen.  
Ein weiteres Ziel des 3. Jour Fixe bildet ein gemeinsamer Beitrag für das Text+ Blog. Dort finden Sie auch die kürzlich veröffentlichte Nachlese unseres 2. Jour Fixe.  
Es wurde bewusst Platz im Programm gelassen für Themen, die Sie als FIDe behandeln möchten. Bei Interesse wenden Sie sich daher gerne mit Ihren Ideen und Vorschlägen an uns!  
  
Wir als AG FID Koop würden uns freuen, Sie am 13./14. November in Köln begrüßen zu dürfen. Sollte es Ihrerseits Fragen oder Anregungen geben, wenden Sie sich gern an uns.
