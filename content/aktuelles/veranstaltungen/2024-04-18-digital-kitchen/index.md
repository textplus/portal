---
type: event
title: "Digital Kitchen: NFDI-Konsortium Text+"
start_date: 2024-04-18 13:00:00
end_date: 2024-04-18 14:00:00
all_day: false
location: Online
---

Mit der Community – für die Community: Die Datendomäne „Editionen“ des
NFDI-Konsortiums Text+

Editionen sind durch Besonderheiten beim Umgang mit Forschungsdaten geprägt, die ihre eigenen Herausforderungen mitbringen. Hierfür baut die Datendomäne „Editionen“ im NFDI-Konsortium Text+ eine offene Infrastruktur auf, die Bedarfe und Perspektiven der Editionscommunity aufgreift und im Austausch mit Fachwissenschaftler:innen, Lehrenden und Lernenden weiterentwickelt wird.

In der Digital Kitchen stellen wir unsere Angebote vor. Der Fokus liegt dabei besonders auf den vielfältigen Beteiligungsmöglichkeiten für die Fachcommunity und dem Nutzen, den unsere Dienste für diese mitbringen.

Weitere Informationen: https://saxfdm.de/veranstaltungen/digital-kitchen-nfdi-text/
