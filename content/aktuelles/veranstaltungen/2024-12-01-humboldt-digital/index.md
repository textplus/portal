---
type: event
title: "Humboldt Digital"
start_date: 2024-12-01T00:00:00+0100
end_date: 2024-12-01T23:59:59+0100
all_day: true
publishDate: 2024-12-01T02:00:00+0100
expiryDate: 2024-12-02T02:00:00+0100
featured_image: humboldt-adventskalender.png
location: Adventstkalender
---

Die [edition humboldt digital](https://edition-humboldt.de/index.xql?l=de) besticht nicht nur durch ihr Aussehen, sondern auch durch ihre gelungene [Dokumentation](https://edition-humboldt.de/about/index.xql?id=H0016212&l=de).

#textplus_advent #modeledition #digitalhumanities

{{<image img="candles.webp" alt="Kerzen"/>}}