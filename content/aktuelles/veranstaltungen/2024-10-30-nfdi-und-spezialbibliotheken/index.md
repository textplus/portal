---
type: event
title: "NFDI und Spezialbibliotheken im Gespräch – eine Umfrage des NFDI Konsortiums Text+ zu Katalogdaten von Bibliotheken"
start_date: 2024-10-30 10:30:00
end_date: 2024-10-30 12:00:00
all_day: false
location: virtuell
---

Unter dem Titel „Welche Metadaten braucht die Literaturwissenschaft?“ wurde
2023 eine Umfrage vom NFDI-Konsortium Text+ gemeinsam mit dem DFG Schwerpunktprogramm
Computational Literary Studies und dem EU Projekt CLS-Infra durchgeführt.

In der gemeinsamen Veranstaltung der Sektion Wissenschaftliche Spezialbibliotheken im dbv
und des NFDI-Konsortiums Text+ werden die Ergebnisse der Umfrage vorgestellt und gefragt,
welche Titel- und Normdaten letztlich von Bibliotheken, Archiven, Repositorien
bereitgestellt werden sollten. In welchen Ressourcen (Normdaten, Kataloge, Repositorien)
sollten zukünftig strukturierte Informationen (Metadaten) entstehen und wie können sie mit
bibliothekarisch-archivarischen Katalogdaten in Beziehung gesetzt werden? Wie können diese
Informationen (Metadaten) durch eine semantische Verbindung zu anderen Ressourcen (u.a.
Normdaten) oder den Einsatz von KI und Natural Language Processing (NLP) aus den Korpora
selbst gewonnen werden? Wie weit könnten diese in die Katalog- und Normdaten
zurückfließen?

## Anmeldung
Zur [Anmeldung bitte hier](https://aspb.de/angebote/workshops/nfdi/) entlang.