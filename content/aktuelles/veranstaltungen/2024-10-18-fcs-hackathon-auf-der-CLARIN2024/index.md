---
type: event
title: "FCS Hackathon @ CLARIN2024"
start_date: 2024-10-18 09:00:00
end_date: 2024-10-18 17:00:00
all_day: false
location: Barcelo Sants, Barcelona, Spanien
event_url: https://www.clarin.eu/event/2024/fcs-hackathon-clarin2024
summary: "Wir laden am 18. Oktober zu einem neuen FCS Hackathon im Rahmen der CLARIN Annual Conference 2024 in Barcelona ein."
---

Wir laden am 18. Oktober 2024 im Anschluss an die [CLARIN Annual Conference 2024](https://www.clarin.eu/event/2024/clarin-annual-conference-2024) zu einem neuen FCS Hackathon ein.

Die [CLARIN **Federated Content Search (FCS)**](https://www.clarin.eu/content/content-search) ist eine [Suchmaschine](https://contentsearch.clarin.eu/) und Infrastruktur, die heterogene Datensammlungen und Suchmaschinen, die lokal in Instituten und Datenzentren gehostet werden, miteinander verbindet und den Nutzern eine einheitliche Schnittstelle zum Entdecken und Suchen in interessanten Sprachressourcen bietet.

Der Hackathon wird zuerst die theoretischen Grundlagen rund um die FCS und der Endpunktentwicklung vorstellen und im Anschluss werden Teilnehmer gemeinsam FCS Endpunkte entwickeln. Tutoren stehen für Fragen und Rat bereit. Am Ende des Hackathons soll jeder Teilnehmer einen eigenen Endpunkt entwickelt und Kenntnisse für weitere Anpassungen erworben haben und Teil der FCS Community werden.

[Zur Eventseite des vorherige Text+ FCS Hackathon an der SAW](/aktuelles/veranstaltungen/2023-11-16-text-resource-and-search-integration-hackathon/)
