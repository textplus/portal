---
type: event
title: "FCS Hackathon @ CLARIN2024"
start_date: 2024-10-18 09:00:00
end_date: 2024-10-18 17:00:00
all_day: false
location: Barcelo Sants, Barcelona, Spain
event_url: https://www.clarin.eu/event/2024/fcs-hackathon-clarin2024
summary: "We invite you to a new FCS Hackathon on October 18 as part of the CLARIN Annual Conference 2024 in Barcelona."
---

We invite you to a new FCS Hackathon on October 18, 2024 following the [CLARIN Annual Conference 2024](https://www.clarin.eu/event/2024/clarin-annual-conference-2024).

The [CLARIN **Federated Content Search (FCS)**](https://www.clarin.eu/content/content-search) is a [search engine](https://contentsearch.clarin.eu/) and infrastructure that connects heterogeneous data collections and search engines hosted locally in institutes and data centers and provides users with a unified interface to discover and search interesting language resources.

The hackathon will first introduce the theoretical foundations around FCS and endpoint development and then participants will develop FCS endpoints together. Tutors will be available for questions and advice. At the end of the hackathon, each participant should have developed their own endpoint, acquired knowledge for further customization and become part of the wonderful FCS community.

[To the event page of the previous Text+ FCS Hackathon at the SAW](/aktuelles/veranstaltungen/2023-11-16-text-resource-and-search-integration-hackathon/)

