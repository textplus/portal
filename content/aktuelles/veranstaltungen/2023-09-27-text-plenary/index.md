---
type: event
title: "Text+ Plenary 2023: Connecting People and Data"
start_date: 2023-09-27 14:00:00
end_date: 2023-09-29 14:00:00
all_day: false
location: Göttingen
featured_image: images/Goettingen_Marktplatz_Oct06-1024x567.jpg
aliases:
- /events/text-plenary-2023-2
---
Als Konsortium der Nationalen Forschungsdateninfrastruktur ([NFDI](https://www.nfdi.de/)) hat Text+ zum Ziel, sprach- und textbasierte Forschungsdaten langfristig zu erhalten und ihre Nutzung in der Wissenschaft zu ermöglichen.

Am 12. September 2022 traf sich das Text+ Konsortium zu einer ersten Plenartagung in Mannheim mit allen Mitarbeitenden und Interessierten, darunter auch Delegierte aus Fachverbänden und -verbünden.

Am 28. und 29. September 2023 findet nun das zweite Plenary von Text+ statt, diesmal an der [Niedersächsischen Staats- und Universitätsbibliothek Göttingen](https://www.sub.uni-goettingen.de/). 

Das Text+ Plenary ist die wichtigste, jährlich stattfindende Veranstaltung des NFDI-Konsortiums und bietet allen Mitarbeitenden, Assoziierten, der gesamten Community, weiteren NFDI-Konsortien sowie Interessierten innerhalb und außerhalb der NFDI Raum für Diskussionen, zur Vernetzung und Information. An diese ergeht herzliche Einladung zur Teilnahme am Text+ Plenary 2023!

Auf diesen Seiten finden Sie wichtige Informationen rund um das Plenary.

Das Plenary wird am 28. und 29. September (lunch to lunch) stattfinden und bietet eine große Auswahl an Vorträgen, Podiumsdiskussionen, Arbeitsgruppensitzungen und viel Raum zum gegenseitigen Austausch. Vorgeschaltet ist am 27. September ein Arbeitsgruppen- und Komiteetag, in dem projektintern wichtige Weichen für das dritte Projektjahr gestellt werden.

In Kürze entsteht an diesem Ort das Programm der Veranstaltungen, darauf folgen Hotel- und Anreiseempfehlungen sowie weitere wichtige Informationen zum Ablauf der Veranstaltung. Die Registrierung startet im Mai. Stay tuned!

Info & Anmeldung: https://events.gwdg.de/event/375/

<span style="color:gray">Foto: Daniel Schwen, GNU FDL v1.2 https://commons.wikimedia.org/wiki/File:Goettingen_Marktplatz_Oct06_Antilived.jpg
</span>