---
type: event
title: "Code Sprint for Humanities Data"
start_date: 2024-04-11 12:00:00
end_date: 2024-04-12 14:30:00
all_day: false
location: SUB Göttingen
---
# Code Sprint for Humanities Data

In diesem Code Sprint stellen wir einen neuen Workflow vor, der einen einsteigerfreundlichen Import von Korpora, Kollektionen und Editionen im TEI-Format in das TextGrid Repository (TextGridRep) sowie eine anschließende Publikation der Daten ermöglicht.

### TextGridRep
Das TextGridRep ist dank seines umfangreichen, durchsuch- und nachnutzbaren Bestands an Texten und Bildern ein beliebtes und relevantes Repositorium für geisteswissenschaftliche Forschungsdaten. Für Forschende bietet es eine nachhaltige, dauerhafte und sichere Möglichkeit zur zitierfähigen Publikation ihrer Forschungsdaten und zur verständlichen Beschreibung derselben durch erforderliche und erweiterte Metadaten, so dass die Publikation bestmöglich im TextGridRep präsentiert und nachgenutzt werden kann. Der zukünftige Weg der Daten ins TextGridRep wird von Tools ermöglicht werden, die aktuell in Text+ entwickelt werden (in der Task Area Infrastructure/Operations, Measure 5).

### Ziele des Code Sprints
In diesem Code-Sprint wird die auf diesen Tools basierende Lösung für einen erleichterten Datenimport in das TextGridRep vorgestellt und schließlich von den Teilnehmenden angewendet und so eine Publikation vorbereitet. Ein besonderer Fokus wird auf die Erstellung der TextGridRep-spezifischen Metadaten liegen, die unter anderem für die Auffindbarkeit der Daten im TextGridRep essentiell sind.

### Zielgruppe des Code Sprints
Zielgruppe dieses Code-Sprints sind Personen und Projekte, die entweder bereits über TEI-Daten verfügen und diese veröffentlichen wollen, oder vorhaben, TEI-Daten zu produzieren und zu veröffentlichen. Es wird sowohl die Möglichkeit geben, mit einem Beispiel-Datensatz zu arbeiten, als auch mit eigenen TEI-Daten. Es können bis zu 25 Personen teilnehmen.

Ziel des Code-Sprints ist es, die eigene Daten-Publikation im Rahmen der Veranstaltung vorzubereiten und mit Unterstützung im Nachgang umzusetzen.

## Anmeldung
Zur [Anmeldung bitte hier](https://events.gwdg.de/event/655/) entlang.