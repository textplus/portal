---
type: event
title: "Save the Date: DARIAH Annual Event 2025"
start_date: 2025-06-17
end_date: 2025-06-20
all_day: true
location: Göttingen
featured_image: DAE2025@SUB_Save-the-date.png
---
Mit den Eindrücken der DARIAH-Jahrestagung 2024 in Lissabon noch im Gedächtnis, freuen wir uns schon auf das Treffen im nächsten Jahr: das DARIAH-EU Annual Event 2025. Im Anschluss an das Text+ Plenary findet es vom 17. bis 20. Juni statt, mit den Hauptkonferenztagen vom 18. bis 20. Juni (Mittwoch bis Freitag). Weitere Informationen und Anmeldemöglichkeiten werden in den Monaten vor der Veranstaltung auf der DARIAH-EU-Website für die Jahrestagungen verfügbar sein.

