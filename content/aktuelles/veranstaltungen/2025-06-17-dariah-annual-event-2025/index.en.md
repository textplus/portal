---
type: event
title: "Save the Date: DARIAH Annual Event 2025"
start_date: 2025-06-17
end_date: 2025-06-20
all_day: true
location: Göttingen
featured_image: DAE2025@SUB_Save-the-date.png
---
With the impressions from the DARIAH Annual Event 2024 in Lisbon slowly fading, we are looking forward to next year’s meeting: The DARIAH-EU Annual Event 2025. Following the Text+ Plenary back to back, it will be held from June 17-20, with the main conference days on June 18–20 (Wednesday to Friday). Further information and registration options will be available in the months leading up to the event on the DARIAH-EU website for the annual events.

Until then, mark your calendars and save the date!