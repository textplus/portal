---
type: event
title: "Oxygen XML Editor"
start_date: 2024-12-10T00:00:00+0100
end_date: 2024-12-10T23:59:59+0100
all_day: true
publishDate: 2024-12-10T02:00:00+0100
expiryDate: 2024-12-11T02:00:00+0100
featured_image: oxygen-adventskalender.png
location: Adventskalender
---

Der Oxygen XML Editor ist eine XML-Entwicklungs- und Authoring-Plattform mit Unterstützung für alle wichtigen XML-Standards und -Technologien. Es bietet eine umfassende Lösung für Autor:innen, die XML-Dokumente visuell bearbeiten wollen, mit oder ohne umfangreiche Kenntnisse über XML und XML-bezogene Technologien. Mehr dazu in unserer Plattform für Editions-Tools: https://research-software.cceh.uni-koeln.de/software/oxygen-xml-editor

#textplus_advent #nfdiSoftware #tei #xml 