---
type: event
title: "Europäische Religionsfrieden Digital"
start_date: 2024-12-06T00:00:00+0100
end_date: 2024-12-06T23:59:59+0100
all_day: true
publishDate: 2024-12-06T02:00:00+0100
expiryDate: 2024-12-07T02:00:00+0100
featured_image: religionsfrieden-adventskalender.png
location: Adventskalender
---

Langfristige Nutzbarkeit, Offenheit: [„Europäische Religionsfrieden Digital“](https://eured.de/) bietet solide Editorik, aufbereitete und wiederverwendete Daten und Tools aus früheren Projekten in einem beispielhaften FAIR-Data-Anwendungsfall.

#textplus_advent #fair #datareuse #religionsfrieden