---
type: event
title: "Large Language Models and the future of scientific publishing"
start_date: 2025-02-11 10:00:00
end_date: 2025-02-11 16:00:00
all_day: false
location: DIPF Frankfurt
event_url: https://indico3-jsc.fz-juelich.de/event/202/
---

Große Sprachmodelle (Large Language Models, LLMs) haben bereits begonnen, wissenschaftliche Arbeitsabläufe erheblich zu beeinflussen. Ihr Einfluss wird noch weiter zunehmen, und zwar mit einer Geschwindigkeit, die frühere Umwälzungen übertrifft. LLMs werden sich am deutlichsten auf das zentralste Element der Wissenschaftskommunikation auswirken: Veröffentlichungen. Sie werden den Aufwand für die Erstellung und Bearbeitung wissenschaftlicher Artikel, die den Höhepunkt der Forschungsdatenproduktion darstellen, verringern. Es werden bereits Werkzeuge entwickelt (z. B. Paperpal), die die Dauer der Erstellung des ersten Entwurfs verkürzen. LLMs haben daher das Potenzial, die Zeit von der Datenerhebung bis zur Veröffentlichung erheblich zu verkürzen. Diese Softwareentwicklungen werden dazu beitragen, die Vorteile von Forschungsdaten zeitnah zu nutzen, werden aber letztlich auch zu einer Zunahme von Zeitschrifteneinreichungen führen. Bestehende Qualitätssicherungsmechanismen (insbesondere die Peer-Reviews) werden daher sowohl hinsichtlich des Umfangs als auch hinsichtlich der Kriterien für die Bewertung des Mehrwerts von Veröffentlichungen in Frage gestellt werden.

Dieser Workshop wird Forschende und Verleger zusammenbringen, um die bevorstehenden Herausforderungen für die Zeitschriften sowie die gestiegenen Anforderungen an die Gutachtenden und die breitere Forschungsgemeinschaft zu identifizieren und anzugehen. Darüber hinaus wird der Workshop mögliche Chancen aufzeigen, die sich durch die Integration von LLM-Technologien in den Schreib- und Publikationsprozess ergeben, und den Weg für zukünftige Diskussionen zu diesem Thema ebnen.    
