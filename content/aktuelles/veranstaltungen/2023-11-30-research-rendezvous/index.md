---
type: event
title: "Text+ Research Rendezvous"
start_date: 2023-11-30 10:00:00
end_date: 2023-11-30 11:00:00
all_day: false
location: Virtuell
---

Das NFDI-Konsortium Text+ bietet ab Oktober zusätzlich zum fortlaufenden Beratungsangebot eine virtuelle Sprechstunde des Helpdesks an.

Im zweiwöchigen Rhythmus können bei „Text+ Research Rendezvous“ Fragen rund um Sammlungen, Editionen und Lexikalische Ressourcen sowie aus dem Bereich Infrastruktur/Betrieb zu festen Terminen (dienstags 9–10 Uhr bzw. donnerstags 10–11 Uhr) direkt mit Mitarbeitenden aus Text+ besprochen werden. Für umfangreichere Fragen werden gerne individuelle (Folge-)Termine vereinbart. Eine Anmeldung zur Sprechstunde ist nicht erforderlich. Ein Zutritt zur Sprechstunde ist während der gesamten Stunde möglich.

Für Fragen zur virtuellen Sprechstunde sowie anderen Beratungsmöglichkeiten steht Ihnen das Team des Text+ Helpdesks über das Formular unter https://www.text-plus.org/kontakt/ gerne jederzeit zur Verfügung.

Termine der Reihe: 2.11. (10–11 Uhr), 14.11. (9–10 Uhr), 30.11. (10–11 Uhr), 12.12. (9–10 Uhr)

https://events.gwdg.de/event/564/