---
type: event
title: "15. Bayerisch-Österreichische Dialektologietagung (BÖDT)"
start_date: 2023-09-13
end_date: 2023-09-15
all_day: true
location: München
featured_image: images/csm_Poster_Tagung_DINA3_c887ce4803.png
aliases:
- /events/15-bayerisch-osterreichische-dialektologietagung-bodt
---


Unter dem Leitthema „200 Jahre Dialektologie – Forschungsstand und Auftrag“ findet die **15. 
BÖDT vom 13. bis 15. September 2023 in München** statt. Das Bayerische Wörterbuch freut sich  
als Gastgeber, Sie dort an der Bayerischen Akademie der Wissenschaften begrüßen zu dürfen.

Plenarvortragende sind Lars Bülow (LMU München), Alfred Lameli und Matthias Hahn (Deutscher Sprachatlas Marburg), Alexandra Lenz (Universität Wien, ÖAdW) und Konstantin Niehaus (Paris Lodron Universität Salzburg).

**12.9.2023, 19:00 Uhr: Warm Up**

**13.9.2023, 19:30 Uhr: Öffentliche Podiumsdiskussion zum Thema „Dialekt zwischen Kulturerbe und Klischee“** – mit Albert Füracker (Bayerischer Finanz- und Heimatminister), Hubert Klausmann (Impulsvortrag, Universität Tübingen), Hans Kratzer (Süddeutsche Zeitung), Anthony Rowley (LMU München, BAdW) und Franziska Wanninger (bayerische Kabarettistin). Hierfür ist keine Anmeldung erforderlich.

14.9.2023, 19:30 Uhr: Konferenzdinner im Wirtshaus im Schlachthof mit Franziska Wanninger

Weitere Informationen sowie die Möglichkeit zur Anmeldung finden Sie unter: https://boedt2023.badw.de/

Bitte denken Sie auch daran, Ihre Unterkunft zu buchen. Zeitgleich finden Messen in München statt und das Oktoberfest startet am 16. September 2023 unmittelbar im Anschluss an die Tagung.

Zur Teilnahme laden wir Sie herzlich ein und freuen uns auf ein Wiedersehen in München!

Es grüßen Sie herzlich

Andrea Schamberger-Hirt, Felicitas Erhard, Michael Schnabel, Vincenz Schwab