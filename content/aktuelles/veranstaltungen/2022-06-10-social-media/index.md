---
title: "Social-Media-Sprachkorpora"
start_date: 2022-06-10 14:00:00
end_date: 2022-06-10 15:30:00
location: online
type: event
aliases:
- /events/social-media-sprachkorpora
---

Show & Tell – Social Media-Daten in der Forschungspraxis: Vortrags- und Diskussionsreihe

Diese Veranstaltung ist Teil der Veranstaltungsreihe „Show & Tell – Social Media-Daten in der Forschungspraxis“.

Am 10. Juni um 14:00 Uhr (bis etwa 15:30 inkl. Diskussion) lädt der Arbeitskreis "Social Media-Daten", der auf Inititiative von [NFDI4Culture](https://nfdi4culture.de/) gemeinsam mit [KonsortSWD](https://www.konsortswd.de/) und Text+ im Rahmen der [Nationalen Forschungsdateninfrastruktur](https://www.nfdi.de/) betrieben wird, zur zweiten öffentlichen Veranstaltung der Reihe "Show and Tell – Social Media-Daten in der Forschungspraxis" ein.

Dieses Mal wird es mit [Sarah Steinsiek](https://www.uni-due.de/germanistik/beisswenger/teamsteinsiek.php) (Universität Duisburg-Essen) und [Harald Lüngen](https://www.ids-mannheim.de/digspra/personal/luengen/) (IDS – Mannheim) um "Social-Media-Sprachkorpora" gehen.

Die Reihe "Show & Tell – Social Media-Daten in der Forschungspraxis" widmet sich in kurzen Inputs den Tools im Feld der Social Media-Forschung. In je einer guten Zoom-Stunde möchte sie Best Practices und ausgewählte Forschungsprojekte beleuchten. Im Fokus stehen neben pragmatischen Lösungen und technischen Möglichkeiten (Software, Schnittstellen, Repositorien, Metadatenstandards, Interoperabilität…) u.a. die ethischen und rechtlichen Schranken (Persönlichkeits- und Urheberrechte) bei der Anlage und Auswertung von Datensets und Korpora sowie der nachhaltige, sichere und kritische Umgang damit (Code and Data Literacy, FAIR & CARE). Nicht zuletzt wollen wir dazu einladen, interdisziplinäre Forschungsansätze und Lehrmethoden zu diskutieren, die tradierte wie fachspezifische Rahmen und Werkzeuge strapazieren. Das erste Treffen fand am 13. Mai zum Thema 'Twitter Tools' statt. Weitere Termine folgen am 10. Juni ("Sprachkorpora") und 1. Juli ("Immer Ärger mit den Bildern").

[Bitte registrieren Sie sich hier für den Zoom-Raum](https://adwmainz.zoom.us/meeting/register/tJUvf-CvqzIpGN1B_G8cisRBmbwQYOM3cz67) (am besten mit einer institutionellen Adresse, der Link wird zeitnah vor dem Event verschickt).
