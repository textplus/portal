---
type: event
title: "Sprachgeschichte und Editionsphilologie: Vom digitalen Umgang mit historischen Texten"
start_date: 2023-09-19
end_date: 2023-09-21
all_day: true
location: Virtuell
featured_image: images/visli-1024x664.png
aliases:
- /events/sprachgeschichte-und-editionsphilologie-vom-digitalen-umgang-mit-historischen-texten/
---
In diesem digitalen Workshop wollen wir neue Perspektiven auf die germanistische und romanistische Sprachgeschichte und Editionsphilologie eröffnen.

Handschriftlich überlieferte mittelalterliche, erste gedruckte Editionen und jüngste digitale Publikationsformen stellen unseren Gegenstandsbereich dar und sollen zusammengeführt und präsentiert werden. Auf dieser Grundlage werden methodische Konzepte aus der Editionsphilolgie hinterfragt und an die neuen technischen Bedingungen adaptiert sowie innovative Analysemöglichkeiten und Anbindungen an verfügbare Schnittstellen untersucht.

Der Workshop besteht aus einzelnen Modulen von jeweils etwa drei beziehungsweise sechs Stunden, die auch einzeln besucht werden können: 

**Modul 1: Digitale Editionen und Korpora sprachhistorischer Zeugnisse** – Überblick, Herausforderungen und Zugänge (19. September, 15:30 bis 18:30 Uhr)  
**Modul 2: Von der mittelalterlichen Handschrift zur digitalen Edition** – Texterkennung und Transmedialisierung (20. September, 9 bis 12 Uhr)  
**Modul 3: Digitale Verfahren in der Textkritik** (20. September, 14 bis 17 Uhr)  
**Modul 4: Erschließung und Vernetzung digitaler Ressourcen** (21. September, 9 bis 17 Uhr)  

Das Programm und das Anmeldeformular finden sich auf der [Veranstaltungsseite](https://events.gwdg.de/event/532). Der Workshop wird geplant und umgesetzt vom [Projekt Historische Sprache(n) neu gesehen](https://www.visli.romanistik.uni-kiel.de/) und vom Konsortium Text+ der Nationalen Forschungsdateninfrastruktur.