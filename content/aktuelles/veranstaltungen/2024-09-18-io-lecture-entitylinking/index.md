---
type: event
title: "IO-Lecture: Entity Linking"
start_date: 2024-09-18 10:00:00
end_date: 2024-09-18 12:00:00
all_day: false
location: virtuell
---
# IO-Lecture: Entity Linking
Diese IO-Lecture befasst sich mit Entity Linking im Kontext von Text+. Nach einer kurzen generellen Einführung in das Thema Entity Linking werden zunächst existierende Werkzeuge und Daten besprochen, sowie ein konkreter Dienst in Form des Entity-Search-Prototypen der Federated Content Search der Sächsische Akademie der Wissenschaften zu Leipzig. Weiterhin werden laufende Arbeiten zu Entity Linking mit Large Language Models (LLMs) vorgestellt, sowie Experimente mit GND-Embeddings mit dem Wortschatz-Projekt (auch SAW), als Brückenschlag zu den Normdatenaktivitäten in Text+.

## Agenda

1) Einführung Entity Linking (Felix Helfer, SAW)

2) EL in der Praxis   
    2.1) Evaluation bestehender Tools für Deutsch (Pia Schwarz, IDS)
    2.2) Datenvorstellung TBD (TBD)
    2.3) EntityFCS (Felix Helfer/Erik Körner/Thomas Eckart, alle SAW)

3) Laufende Arbeiten in Text+
    3.1) Entity-Embeddings (Felix Helfer)
    3.2) Ausblick LLM-EL (Florian Barth, SUB)
    3.3) GND@LLM (TBD)

## Zielgruppe der IO-Lecture
Die Veranstaltung richtet sich in erster Linie an Mitarbeitende von Text+, die Forschung innerhalb der Sozial- und Geisteswissenschaften betreiben und/oder in der Beratung tätig sind. Darüber hinaus freuen wir uns aber auch über Teilnehmende aus anderen NFDI-Konsortien. Im Zweifelsfalle kontaktieren Sie gerne das Text+ [Operations Office](textplus-operations-office@lists.gwdg.de).

Zur Anmeldung bitte den untenstehenden Anmeldelink nutzen.

## Anmeldung
Zur [Anmeldung bitte hier](https://events.gwdg.de/event/913/) entlang.