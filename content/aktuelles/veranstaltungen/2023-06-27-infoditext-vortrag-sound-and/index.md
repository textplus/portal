---
type: event
title: "InFoDiTexT+ Vortrag: Sound and Suspense – Scary Sounds in 19th Century English Fiction"
start_date: 2023-06-27 17:15:00
end_date: 2023-06-27 18:45:00
all_day: false
location: online
featured_image: images/InFoDiText2023_SvenjaGuhr.png
aliases:
- /events/infoditext-vortrag-sound-and-suspense-scary-sounds-in-19th-century-english-fiction
---
  
_Speaker: Svenja Guhr, TU Darmstadt, Institut für Sprach- und Literaturwissenschaft_

In her talk, Svenja Guhr presents the findings of a project she conducted as a visiting research student at the Stanford Literary Lab during the Fall of 2022. In a computational literary study use case supervised by Prof. Mark Algee-Hewitt, she combined sound studies and theories of suspense to investigate the correlation between diegetic descriptions of ambient sounds and the affect of suspense in a corpus of 19th century English fiction. Using a combined approach of distant and close reading methods, including manual and automated annotation of sound markers in literary prose, and drawing on theories of suspense in literary fiction (i.a., Carroll 1996) as well as findings from the 2014–2018 Stanford Literary Lab Project "Suspense: Language, Narrative, Affect" (Algee-Hewitt et al. 2015), she argues that suspenseful passages contain more detailed descriptions of the story's soundscape than do unsuspenseful passages (e.g., the growl of a wild animal, the creak of a wooden floor in a silent room), which consequently leads to an increase in the degree of literary suspense.

Register here: https://events.gwdg.de/event/451/