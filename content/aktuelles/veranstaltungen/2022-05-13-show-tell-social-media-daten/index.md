---
title: "Show & Tell – Social Media-Daten in der Forschungspraxis"
start_date: 2022-05-13 14:00:00

type: event
aliases:
- /events/show-tell-social-media-daten-in-der-forschungspraxis

---


**Vortrags- und Diskussionsreihe**

Die Reihe "Show and Tell – Social Media-Daten in der Forschungspraxis" widmet sich in kurzen Inputs den Tools im Feld der Social Media-Forschung. In je einer Zoom-Stunde möchte sie Best Practices und ausgewählte Forschungsprojekte beleuchten. Im Fokus stehen neben pragmatischen Lösungen und technischen Möglichkeiten (Software, Schnittstellen, Repositorien, Metadatenstandards, Interoperabilität…) u.a. die ethischen und rechtlichen Schranken (Persönlichkeits- und Urheberrechte) bei der Anlage und Auswertung von Datensets und Korpora sowie der nachhaltige, sichere und kritische Umgang damit (Code and Data Literacy, FAIR & CARE). Nicht zuletzt wollen wir dazu einladen, interdisziplinäre Forschungsansätze und Lehrmethoden zu diskutieren, die tradierte wie fachspezifische Rahmen und Werkzeuge strapazieren. Die Veranstaltungsreihe wird von einem Arbeitskreis ausgerichtet, der auf Initiative von NFDI4Culture gemeinsam mit KonsortSWD und Text+ im Rahmen der Nationalen Forschungsdateninfrastruktur betrieben wird. Wir treffen uns am 13. Mai zum Thema 'Twitter Tools'. Weitere Termine folgen am 10. Juni und 1. Juli.

Den Auftakt machen Carolin Gerlitz (Digital Media and Methods, Universität Siegen)

und Johannes Breuer (GESIS, Köln / CAIS, Bochum) mit dem Thema

**"Twitter Tools: Daten sammeln, aufbereiten & analysieren"**

am 13. Mai um 14:00 Uhr.
