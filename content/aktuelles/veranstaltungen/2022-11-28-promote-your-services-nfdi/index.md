---
type: event
title: "Promote Your Services! NFDI-Angebote im SSH Open Marketplace international bewerben"
start_date: 2022-11-28 09:00:00
end_date: 2022-11-28 13:00:00
all_day: false
location: online https://events.gwdg.de/event/346/
aliases:
- /events/promote-your-services-nfdi-angebote-im-ssh-open-marketplace-international-bewerben
---

Der SSH Open Marketplace ist ein Angebot für die Sozial- und Geisteswissenschaften, die mit digitalen Ressourcen arbeiten. Hier werden Informationen zu Diensten und Werkzeugen, Datensätzen und Publikationen, Lehrmaterialien und Workflows zur Verfügung gestellt. Die Besonderheit liegt im partizipativen Design des Marketplaces: Alle können selbst Einträge und Verbesserungen vorschlagen, die in einem zweiten Schritt vom Editorial Board veröffentlicht werden. Wir konzentrieren uns im Workshop auf Dienste und Werkzeuge und werden Hands-On zeigen, wie sie im Marketplace beworben werden können. Ziel ist eine einheitliche und einfache Beschreibung mit entsprechender Nutzung des Metadatenmodells, das eine Interoperabilität mit anderen Service-Katalogen (z.B. EOSC Marketplace) erlaubt.

Im SSH Open Marketplace können auf europäischer Ebene sowohl Diensteanbieter als auch Forschende und Lehrende ihre Ressourcen selbst verwalten und kuratieren. Über einzelne Forschungsinfrastrukturprojekte hinaus (z.B. DARIAH, CLARIN, CESSDA, OPERAS sowie NFDI-Konsortien) können geistes- und sozialwissenschaftliche Ressourcen auf dem SSH Open Marketplace auf europäischer und internationaler Ebene repräsentiert und gefunden werden. Der SSH Open Marketplace funktioniert auf technischer Ebene als Registry und kann über eine entsprechende API mit anderen Portalen verknüpft werden.

Die Anmeldung zum Workshop funktioniert über https://events.gwdg.de/event/346/  
Dort finden Sie auch eine Übersicht über das Programm.
