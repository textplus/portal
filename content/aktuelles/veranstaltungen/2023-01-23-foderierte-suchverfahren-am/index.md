---
type: event
title: "Föderierte Suchverfahren am Beispiel der CLARIN FCS"
start_date: 2023-01-23
all_day: true
aliases:
- events/foderierte-suchverfahren-am-beispiel-der-clarin-fcs

---

Am 28. Juni 2022 fand eine virtuelle Einführungsveranstaltung zu föderierten Suchverfahren für Forschungsdaten statt. Vorgestellt wurde die im Rahmen des europäischen [CLARIN-Projektes](https://clarin.eu) entwickelte und betriebene _Federated Content Search_ ([CLARIN FCS](https://contentsearch.clarin.eu)). Diese ermöglicht den zentralen Zugriff auf verteilt vorliegende Forschungsdaten mit einem Fokus auf annotierte Volltexte.

Der inhaltliche Schwerpunkt des Vortragsteils (vorgestellt durch [Erik Körner](https://www.saw-leipzig.de/de/mitarbeiter/koerne), [Oliver Schonefeld](https://www.ids-mannheim.de/zentrale-bereiche/informationstechnik/personal/schonefeld/) und [Thomas Eckart](https://www.saw-leipzig.de/de/mitarbeiter/eckartt)) lag auf der Darstellung technischer Grundlagen auf Basis etablierter Standardprotokolle und Anfragesprachen (u.a. [Search/Retrieval via URL](https://www.loc.gov/standards/sru/), [OASIS searchRetrieve](https://docs.oasis-open.org/search-ws/searchRetrieve/v1.0/searchRetrieve-v1.0-part0-overview.html) und die [Contextual Query Language](https://www.loc.gov/standards/sru/cql/index.html) CQL), den Einblick in aktuelle Arbeiten sowie eine Übersicht über vorhandene Softwarelösungen, die einen Einstieg in die FCS für interessierte Institutionen erleichtert. Darüber hinaus wurde ein Überblick über die aktuellen Text+ Aktivitäten gegeben, die in der Datendomäne _Lexikalische Ressourcen_ mittlerweile zu einem ersten Prototypen geführt haben. Dieser wird beim kommenden Text+ Plenary erstmals der Öffentlichkeit vorgestellt.

Die hohe Anzahl von interessierten Personen (über 40 Teilnehmende), auch von außerhalb des Text+-Konsortiums, und die lebhafte Diskussion im Anschluß an den Vortragsteil verdeutlichte das große Interesse an diesem Thema. Als Folge der Veranstaltung wurde und wird das Thema föderierter Inhaltssuche insbesondere auch mit der Datendomäne _Sammlungen_ verstärkt diskutiert.
