---
type: event
title: "GND-Agentur"
start_date: 2024-12-20T00:00:00+0100
end_date: 2024-12-20T23:59:59+0100
all_day: true
publishDate: 2024-12-20T02:00:00+0100
expiryDate: 2024-12-21T02:00:00+0100
featured_image: gnd-agentur-adventskalender.png
location: Adventskalender
---

Die [GND-Agentur Text+](https://text-plus.org/themen-dokumentation/standardisierung/gnd-agentur/) ist ein Service für geistes- und kulturwissenschaftliche Projekte, die ihre Forschungsdaten als Normdaten in die [GND](https://gnd.network/Webs/gnd/DE/Home/home_node.html) einbringen möchten.

#textplus_advent #normdaten