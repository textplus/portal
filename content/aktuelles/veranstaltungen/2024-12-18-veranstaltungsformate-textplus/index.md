---
type: event
title: "Veranstaltungsformate Text+"
start_date: 2024-12-18T00:00:00+0100
end_date: 2024-12-18T23:59:59+0100
all_day: true
publishDate: 2024-12-18T02:00:00+0100
expiryDate: 2024-12-19T02:00:00+0100
featured_image: community-activities-adventskalender.png
location: Adventskalender
---

[Text+](https://text-plus.org) stärkt das Community-Engagement mit unterschiedlichen Formaten wie dem [FAIR February](https://events.gwdg.de/event/948/), dem [DH-Kolloquium](https://www.bbaw.de/bbaw-digital/dh-kolloquium) oder den [ediarum-Meetups](https://www.ediarum.org/meetups.html).

#textplus_advent #digitalhumanties #digitaleditions #ediarum