---
type: event
title: "ZDL-Jahrestagung"
start_date: 2024-05-16 14:00:00
end_date: 2024-05-16 21:00:00
all_day: false
location: Berlin-Brandenburgische Akademie der Wissenschaften, Leibniz-Saal
featured_image: Akademiegebaeude.jpg
---
Seit 2019 widmet sich das Zentrum für digitale Lexikographie der deutschen
Sprache (ZDL) der Dokumentation des deutschen Wortschatzes in Gegenwart und Geschichte.

Die Jahrestagung des ZDL gibt Einblicke sowohl in die fachliche Vielfalt der
lexikographischen Arbeit als auch in die zugrundeliegenden digitalen Methoden. 

In einem komprimierten Format werden drei ganz unterschiedliche Schlaglichter auf das ZDL geworfen, mit Workshops zu generativer KI und dem Einfluss, den Large Language Models auf die Lexikographie haben, mit Posterständen, die einen Überblick über die bislang erreichten Ergebnisse des ZDL in der ersten Projektphase geben. Und in der Abendveranstaltung widmen wir uns in einem Bühnengespräch und anschließender Lesung der Praxis des literarischen Übersetzens und der Rolle der Wörterbücher für diese Arbeit.

Weiterführende Informationen: https://events.gwdg.de/event/758/timetable/