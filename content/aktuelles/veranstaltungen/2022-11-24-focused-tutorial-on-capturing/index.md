---
type: event
title: "Focused Tutorial on Capturing, Enriching, Disseminating Research Data Objects"
start_date: 2022-11-24 10:00:00
aliases:
- /events/focused-tutorial-on-capturing-enriching-disseminating-research-data-objects
---

This cross-disciplinary tutorial on tools and methods provides brief introductions to use cases from NFDI4Culture, Text+ and BERD@NFDI.

Presented use cases address capture, enrichment and dissemination of research data objects. Capture involves the creation of digital surrogates (e.g. with OCR) or the representation of existing artefacts in a digital representation (e.g. transcription). This is followed by the enrichment (e.g. annotation, tagging and association) of research data objects and is summarised with their dissemination, i.e. making them available and sharing them to support collaboration and reuse.

This Focused Tutorial will provide knowledge, methodological and technical expertise in the areas of data, metadata, taxonomies and standards with a view to the FAIR principles, and promotes a cross-disciplinary exchange and networking between the participating consortia.
