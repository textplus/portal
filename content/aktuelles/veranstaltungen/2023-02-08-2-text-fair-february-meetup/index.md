---
type: event
title: "2. Text+ FAIR February Meetup – Let’s talk FAIR Digital Editions"
start_date: 2023-02-08 14:00:00
end_date: 2023-02-08 16:30:00
all_day: false
location: online
featured_image: 
aliases:
- /events/2-text-fair-february-meetup-lets-talk-fair-digital-editions
---

Im FAIR February widmet sich Text+ einen Monat lang wöchentlich in einer virtuellen Veranstaltung den vier FAIR-Prinzipien Findability, Accessibility, Interoperability und Reusability, die sich mit ihrer Veröffentlichung 2016 im Scientific Data zu Leitprinzipien innerhalb der Digital Humanities etabliert haben.  
Mit der virtuellen Veranstaltungsreihe wenden wir uns an Forschende und Mitarbeitende von laufenden oder geplanten Editionsprojekten, analog wie digital, und an Interessierte aus den Digital Humanities, um über die FAIR-Prinzipien und ihre Anwendung im Kontext wissenschaftlicher Editionen zu diskutieren:  
\- Was bedeuten die einzelnen Prinzipien ganz konkret für Editionen?  
\- Welche Unklarheiten und Lücken gibt es?  
\- An welcher Stelle bedarf es einer Ausdifferenzierung?

Erste Gesprächsimpulse geben Expert:innen verschiedener Fachbereiche aus technischer, editionswissenschaftlicher und bibliothekarischer Sicht.

Info & Anmeldung: https://events.gwdg.de/event/413/