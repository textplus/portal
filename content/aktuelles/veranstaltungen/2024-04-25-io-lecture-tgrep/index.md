---
type: event
title: "9. IO-Lecture: TextGrid Repository"
start_date: 2024-04-25 10:00:00
end_date: 2024-04-25 12:00:00
all_day: false
location: virtuell
---
# 9. IO-Lecture: TextGrid Repository

Das TextGrid Repository (TextGridRep) ist ein seit langem etabliertes digitales Langzeitarchiv für geisteswissenschaftliche Forschungsdaten (insbesondere Texte in XML/TEI) und bietet eine Vielzahl an Daten für Lehr- und Forschungszwecke. Es folgt den Prinzipien des Open Access, bedient offene Standards und erlaubt damit eine effiziente Nachnutzung für die Forschung. Das TextGridRep bietet Wissenschaftler*innen weiterhin umfassende und zuverlässige Dienste für die permanente Datensicherung, gut dokumentiert und mit stabilen Referenzen für Zitation und Nachhaltigkeit.

In dieser Lecture soll es um aktuelle Entwicklungen beim TextGridRep gehen und vorgestellt werden, wie das TextGridRep als Datenzentrumsangebot in Text+ funktioniert und genutzt werden kann. Im Einzelnen:
* Welchen Nutzen hat das TGRep für die Digital Humanities?
* Wie kommen Forschungsdaten ins TGRep?
* ELTeC als Beispiel für einen kuratierten Ingest
* Aktuelle Entwicklungen & praktische Beispiele

Vortragen werden KollegInnen, die das TextGridRep pflegen und weiterentwickeln: Florian Barth, Ubbo Veentjer, Mathias Göbel, José Calvo Tello

## Zielgruppe der IO-Lecture
Die Veranstaltung richtet sich in erster Linie an Mitarbeitende von Text+, die Forschung innerhalb der Sozial- und Geisteswissenschaften betreiben und/oder in der Beratung tätig sind. Darüber hinaus freuen wir uns aber auch über Teilnehmende aus anderen NFDI-Konsortien. Im Zweifelsfalle kontaktieren Sie gerne das Text+ Operations Office.

## Anmeldung
Zur [Anmeldung bitte hier](https://events.gwdg.de/event/680/) entlang.

[def]: ../2024-04-25-io-lecture-tgrep/index.md