---
draft: true
type: event
title: "Digitale Wörterwelten: Einblick in die Text+ Infrastruktur"
start_date: 2024-11-18 13:00:00
end_date: 2024-11-19 15:00:00
all_day: true
location: Berlin
event_url: https://events.gwdg.de/event/957/
summary: "Der Arbeitsbereich »Lexikalische Ressourcen« in Text+ veranstaltet am 18. und 19. November in Berlin einen Workshop, in dem die Arbeit in und mit der Forschungsinfrastruktur des Projektes praktisch vorgestellt wird."
---

Der Arbeitsbereich »Lexikalische Ressourcen« in Text+ veranstaltet am 18. und 19. November in Berlin einen Workshop, in dem die Arbeit in und mit der Forschungsinfrastruktur des Projektes praktisch vorgestellt wird.

Der Aufbau der Infrastruktur wird bis 2025 weitgehend abgeschlossen sein, große Teile können jedoch bereits jetzt aktiv genutzt werden. Der Workshop bietet die Gelegenheit, grundlegende Konzepte und Implementationen der Forschungsinfrastruktur für lexikalische Daten kennenzulernen, auf eigene Daten anzuwenden und sich über Schwerpunkte der zukünftigen Entwicklung auszutauschen.

Anmeldung bis zum 15.11. unter https://events.gwdg.de/event/957/