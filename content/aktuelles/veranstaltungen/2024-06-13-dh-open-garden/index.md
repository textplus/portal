---
type: event
title: "Digital Humanities Open Garden 2024"
start_date: 2024-06-13 15:15:00
end_date: 2024-06-13 22:00:00
all_day: false
location: Sächsische Akademie der Wissenschaften zu Leipzig, Karl-Tauchnitz-Str. 1
featured image: underdhog-plakat-2024-96dpi.jpg
---
Das Forum Digital Humanities Leipzig (FDHL) und die Sächsische Akademie der Wissenschaften zu Leipzig (SAW) laden herzlich ein zum Digital  Humanities Open Garden 2024. Der Digital Humanities Open Garden ist eine offene Veranstaltung, die die Möglichkeit geben soll, DH-Interessierte kennenzulernen und bei einem erfrischenden Getränk und Gegrilltem ins Gespräch zu kommen. Student:innen, Mitarbeiter:innen und alle anderen Interessierten sind herzlich eingeladen – unabhängig davon, ob sie bereits in den Digital Humanities aktiv sind oder (noch) nicht.

Zwei Vorträge mit den Themen "Chatbots, Wasserzeichen und Digitale Nachlässe. Aktuelle DH-Projekte an der Deutschen Nationalbibliothek", gehalten von Dr. Ramon Voges (DNB), und "Lamento – Latrine – Leyptzigk. Entitätenbasierte Inhaltssuche in verteilten Ressourcen", gehalten von Dr. Thomas Eckart, Felix Helfer, Uwe Kretschmer und Erik Körner (alle SAW Leipzig), leiten die Veranstaltung ein. Eine anschließende Diskussion eröffnet den Austausch, der beim nachfolgenden Grillen im Akademiegarten in lockerer Atmosphäre weitergeführt werden kann.

## Anmeldung
Sowie weitere Informationen auf der [Webseite des Forum Digital Humanities Leipzig](https://fdhl.info/).