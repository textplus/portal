---
type: event
title: "Edition der fränkischen Herrschererlasse"
start_date: 2024-12-11T00:00:00+0100
end_date: 2024-12-11T23:59:59+0100
all_day: true
publishDate: 2024-12-11T02:00:00+0100
expiryDate: 2024-12-12T02:00:00+0100
featured_image: capitularia-adventskalender.png
location: Adventskalender
---

Die [Edition der fränk. Herrschererlasse](https://capitularia.uni-koeln.de/) bietet neben Editionstexten u.a. ein [Kollationstool](https://capitularia.uni-koeln.de/tools/collation/) & eine ausführliche [techn. Doku](https://cceh.github.io/capitularia/).

#textplus_advent #modeledition #digitaleditions