---
type: event
title: "Text+ Show and Tell: Tools, Dienste, Projekte"
start_date: 2025-03-13 13:00:00
end_date: 2025-03-13 14:00:00
all_day: false
location: Virtuell
location_url: https://meet.gwdg.de/b/geo-myw-wn3-hb9
event_url: https://events.gwdg.de/e/textplus_showandtell
---

Unter dem Titel *Text+ Show and Tell: Tools, Dienste, Projekte* haben Interessierte in Text+ und darüber hinaus die Möglichkeit, Neues über laufende Projekte, Tools, Standards, Technologien und Services zu lernen sowie ihre eigene Arbeit vorzustellen und ins Gespräch zu kommen. Die Themen können dabei so vielfältig sein wie die NFDI selbst. 

In einem zweiwöchigen Turnus treffen wir uns für 60 min, in denen ein:e Speaker:in ein Thema kurz vorstellt, über welches sich dann in der Gruppe ausgetauscht werden kann. Wie das Thema vorgestellt wird, ist den jew. Speaker:innen überlassen (Live Demo, Folienpräsentation, Gespräch etc.).

Wir wollen diese Treffen auch nutzen, um Updates zu Angeboten in Text+ zu geben, an denen wir Veranstaltende oder Kolleg:innen beteiligt sind. Es wird wiederkehrende Themen geben, bei denen das Feedback der Comunity besonders wichtig ist und in die weitere Gestaltung der Angebote einfließen soll, wie etwa das Text+ Archiv, die Jupyter Notebooks auf dem Text+-Jupyterhub/in Jupyter4NFDI, der TEI-Datenimport ins TextGrid Repository.

Wir freuen uns über zahlreiches Erscheinen und einen interessanten Austausch! Bringt gerne Fragen und Denkanstöße mit, damit wir ins Gespräch kommen können. Überlegt euch auch gern, ob ihr selber in einem zukünftigen Termin etwas zeigen und besprechen wollt.

**Wir freuen uns, diesmal Anja Gerber (Klassik Stiftung Weimar) begrüßen zu dürfen. Sie wird uns einen Einblick in NFDI4Objects, insbesondere in den Umgang mit Forschungsdaten, geben. Es wird Zeit für Fragen und Use Cases geben, denn wir wollen gern mit euch ins Gespräch kommen – sowohl über das vorgestellte Angebot als auch jene Themen, die euch aktuell interessieren und beschäftigen.**