---
type: event
title: "Einführung in die Arbeit mit digitalen Texten in TEI und mit TextGrid"
start_date: 2024-01-29 
end_date: 2024-01-30
all_day: true
location: Humboldt- Universität zu Berlin
---

In diesem zweitägigen Workshop wird die Arbeit mit der Text Encoding Initiative (TEI) und dem TextGrid-Repository für die Romanistik vorgestellt.