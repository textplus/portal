---
type: event
title: "RDA-Deutschland Tagung 2023"
start_date: 2023-02-13 13:30:00
all_day: false
location: online
aliases:
- /events/rda-deutschland-tagung-2023
---

Workshop: As You Like It? Bedarfserhebungen als Instrumente der FDM-Strategieentwicklung in universitären, regionalen und nationalen Verbundkontexten

Info & Anmeldung: https://indico.desy.de/event/37011/contributions/132890/