---
type: event
title: "5. FID / Text+ Jour Fixe – Verzeichnen und Ablegen"
start_date: 2024-11-27 10:00:00
end_date: 2024-11-27 15:00:00
all_day: false
location: SUB Göttingen 
event_url: https://events.gwdg.de/event/960/
---

Die gemeinsam etablierte Veranstaltungsreihe FID/Text+ Jour Fixe geht in die fünfte Runde! Mit diesem Jubiläum geht auch die Vereinigung des Jour Fixe mit dem FID Roundtable einher. Das Treffen vor Ort in Göttingen am 27. November 2024 öffnet damit auch ein neues Kapitel, das sich durch eine noch engere Zusammenarbeit zwischen beiden Strukturen auszeichnen wird.

## Was ist nochmal der FID/Text+ Jour Fixe?  

Die Reihe wurde von der AG FID Koop im NFDI Konsortium Text+ angestoßen, um beide Strukturen zu vernetzen und der Forschung ein gemeinsam abgestimmtes Angebot an Expertise, Ressourcen und Services zur Verfügung zu stellen. 

Nach der Auftaktveranstaltung im Oktober 2022 zum Schwerpunkt "Erwartungsmanagement" haben wir beim 2. Termin das Thema "Consulting" in den Fokus gerückt. Der 3. Jour Fixe konnte vor Ort in Köln stattfinden und bot ein buntes Programm zu möglichen Formaten der Zusammenarbeit. Im April diesen Jahres fand das Begonnene unter dem Titel "Kontinuitäten" dann wieder virtuell seine Fortsetzung.

## Worum geht es beim 5. Jour Fixe?  

Thematisch widmet sich der Jour Fixe dieses Mal dem Verzeichnen und Ablegen von Daten. Die Fragen, wie und wo Daten nachhaltig abgelegt werden können, wie sie Sichtbarkeit erlangen, welche Repositorien es gibt und wo man Beratung und Unterstützung erhalten kann, treiben die gesamte Forschungscommunity um. Gemeinsam wollen wir uns diesen Fragen stellen, uns zu Bedarfen und Angeboten austauschen, um somit für uns und alle Forschenden etwas Licht ins Dunkel zu bringen. Darüber hinaus werden auch die organisatorischen Aspekte der Zusammenarbeit ins Zentrum gerückt. Das Programm wird gemeinsam von Vertreter:innen der FID und der AG FID Koop gestaltet.


## Programm und Anmeldung

https://events.gwdg.de/event/960/
