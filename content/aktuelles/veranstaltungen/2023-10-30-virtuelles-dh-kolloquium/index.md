---
type: event
title: "Virtuelles DH-Kolloquium an der BBAW"
start_date: 2023-10-30 16:15:00
all_day: false
location: virtuell (https://meet.gwdg.de/b/lou-eyn-nm6-t6b)
---

Andreas Vogel (Founding Director Legacy Page Ltd.) spricht über „Capture and Preserve Memoirs using AI and Web 3 Technology“

https://meet.gwdg.de/b/lou-eyn-nm6-t6b