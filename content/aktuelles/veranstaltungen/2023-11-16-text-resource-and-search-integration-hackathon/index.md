---
type: event
title: "Text+ Resource and Search Integration Hackathon"
start_date: 2023-11-16 09:00:00
end_date: 2023-11-17 15:00:00
all_day: false
location: Sächsische Akademie der Wissenschaften zu Leipzig, Karl-Tauchnitz-Str. 1, 04107 Leipzig
featured_image: images/textplus_fcs_color-1920x1080.png
---

Am 16. und 17. November 2023 fand der Text+ Resource and Search Integration Hackathon an der [Sächsische Akademie der Wissenschaften zu Leipzig](https://www.saw-leipzig.de/) (SAW) statt. Acht Personen von sieben wissenschaftlichen Einrichtungen in Deutschland nahmen teil, um ihre eigenen Ressourcen in die [Text+ Föderierte Inhaltssuche (FCS)](https://fcs.text-plus.org/) zu integrieren.

Wir freuen uns sehr, dass erste Endpunkte der Teilnehmenden bereits in der [Text+ FCS](https://fcs.text-plus.org/) integriert sind und sich so schon erkunden lassen. Darunter befinden sich Texte verschiedener Ressourcentypen, so z.B. der [*REDE Wenkeratlas*](https://regionalsprache.de/) vom _Forschungszentrum Deutscher Sprachatlas_, das [*Digitale Programmarchiv (DiPA)*](https://ppa.die-bonn.de/) vom _Deutschen Institut für Erwachsenenbildung – Leibniz-Zentrum für Lebenslanges Lernen e.V._ und das [*Deutsche Zeitungsportal*](https://www.deutsche-digitale-bibliothek.de/newspaper) von der _Deutschen Digitalen Bibliothek_.

Dies zeigt neben den bereits in der Text+ Inhaltssuche enthaltenen Endpunkten und Ressourcen, wie vielfältig textuelle Ressourcen ausgeprägt sein können und wie leicht sie über eine zentrale Stelle interessierten Nutzer:Innen zugänglich gemacht werden können.

Durchsuche selbst die Ressourcen in der [Text+ Inhaltssuche](#action-open-search-content)!

Blog-Beitrag zum FCS Hackathon: https://textplus.hypotheses.org/9750