---
type: event
title: "Enhance Your Research Skills with SSH Open Marketplace: An Online Workshop for PhD Students"
start_date: 2024-06-07 10:00:00
end_date: 2024-06-13 12:00:00
all_day: false
location: virtuell
---

## Overview
Are you a PhD student in the Social Sciences and Humanities (SSH) looking to elevate your research skills? Join us for an interactive online workshop designed to help you leverage the SSH Open Marketplace—a comprehensive discovery portal that connects you with the best resources for every stage of your research data life cycle.

## Workshop Highlights:
Discovering the SSH Open Marketplace:
* Learn how to navigate the platform to uncover essential resources for SSH research.
* Explore a curated collection of over 6,000 items sourced from 15+ trusted providers, research communities, and individual researchers.* 

Understanding the Research Data Life Cycle:
* Gain insights into research practices and solutions for each step of the research data life cycle.
* Discover tools and methodologies that promote sharing and re-use of workflows.

Community Collaboration and Curation:
* Understand the importance of community involvement in cataloguing and contextualizing resources.
* Learn about the roles of stakeholders, including funders, providers, moderators, and contributors, in maintaining a meaningful and relevant platform.

Ensuring Data Quality:
* Explore the curation routines performed by the Editorial Board, blending automatic and manual tasks, to maintain and improve metadata quality.
* Understand the mechanisms in place for continuous updates and improvements based on community feedback.

Who Should Attend?
This workshop is tailored for PhD students in the Social Sciences and Humanities who are eager to expand their research skills and utilize innovative tools and resources available through the SSH Open Marketplace.

Details:
* Date: 7. June 2024
* Time: 10 – 12 am CET
* Location: Zoom
* Register now: https://forms.gle/DF2YSappQUikjv9k8
* For questions contact community_engagement@dariah.eu