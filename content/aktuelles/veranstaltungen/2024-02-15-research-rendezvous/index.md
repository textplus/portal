---
type: event
title: "Text+ Research Rendezvous (Spezial zur Love Data Week)"
start_date: 2024-02-15 10:00:00
end_date: 2024-02-15 11:00:00
all_day: false
location: Virtuell
---

Das NFDI-Konsortium Text+ bietet seit Oktober zusätzlich zum fortlaufenden Beratungsangebot eine virtuelle Sprechstunde des Helpdesks an.

Im zweiwöchigen Rhythmus können bei „Text+ Research Rendezvous“ Fragen rund um Sammlungen, Editionen und Lexikalische Ressourcen sowie aus dem Bereich Infrastruktur/Betrieb zu festen Terminen (dienstags 9–10 Uhr bzw. donnerstags 10–11 Uhr) direkt mit Mitarbeitenden aus Text+ besprochen werden. Anlässlich der internationalen Aktionswoche [Love Data Week](https://forschungsdaten.info/fdm-im-deutschsprachigen-raum/love-data-week-2024/) ist die Sprechstunde zusätzlich geöffnet. Eine Anmeldung ist nicht erforderlich. Der Zutritt zur Sprechstunde ist während der gesamten Stunde möglich.

Nächste reguläre Termine der Reihe: 22.2. (10–11 Uhr), 5.3. (9–10 Uhr), 21.3. (10–11 Uhr), 2.4. (9–10 Uhr)

https://events.gwdg.de/category/208/