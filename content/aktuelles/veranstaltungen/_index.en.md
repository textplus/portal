---
title: Events
type: event
hide_in_list: true

aliases:
- /current-events/events/

menu:
  main:
    weight: 20
    parent: aktuelles
---

# Events

{{<lead-text>}}
Events by and about Text+. If you have an interesting event that should be featured here, please submit it to us for publication.

 <p class="text-gray-500"> Please note that we do not provide translated event descriptions. If an event is listed in German, it is safe to assume that the event language is German, too, so that a translation would not provide useful additional information.</p>
{{</lead-text>}}
{{<button url="en/aktuelles/veranstaltungen/#submit-event">}}Submit Event{{</button>}}

## Upcoming Events
<!-- TODO: title wording, needed for TOC -->
{{<upcoming-events-list>}}
{{<horizontal-line>}}

## Submit Event

Do you have an event that should appear here and is related to Text+? You are welcome to fill out the provided form and send it to us. We will contact you if further information is needed. If the form is completed in full and no questions remain, the event will appear on this page without further action on your part. Please allow up to three days for the event announcement to be published online.

If you do not receive a confirmation of receipt for your event announcement, please send an email to: textplus-support[at]gwdg.de
{{<event-form>}}
{{<horizontal-line>}}

## Past Events
<!-- the past events are handled in the list template which is needed for the pagination to work -->
<!-- see layouts/event/list.html -->