---
type: event
title: "IO-Lecture: Migration von RocketChat zu Matrix"
start_date: 2024-11-06 11:00:00
end_date: 2024-11-06 12:00:00
all_day: false
location: virtuell
---
# IO-Lecture: Migration von RocketChat zu Matrix (Mittwoch 06.11.2024, 11h-12h)
Diese Lecture wird in kompakter Weise alles Wissenswerte zum aktuellen Wechsel von RocketChat zu Matrix vorstellen. Sowohl für "normale" Nutzerinnen und Nutzer wie auch für Owner von Räumen und Kanälen.

Details zur Umstellung zum Nachlesen, Zeitplan und mehr sind auf den [Seiten der GWDG zu finden](https://status.gwdg.de/info_notices/174849).

## Zielgruppe der IO-Lecture
Die Veranstaltung richtet sich an Personen, die Forschung innerhalb der Sozial- und Geisteswissenschaften betreiben, bei einer Infrastruktureinrichtung des Feldes tätig sind oder einfach Interesse am Thema haben. Darüber hinaus freuen wir uns insbesondere über Teilnehmende aus anderen NFDI-Konsortien.

## Anmeldung
Zur [Anmeldung bitte hier](https://events.gwdg.de/event/979/) entlang.