---
type: event
title: "Virtuelles DH-Kolloquium an der BBAW"
start_date: 2023-10-30 16:15:00
all_day: false
location: virtuell https://meet.gwdg.de/b/lou-eyn-nm6-t6b
---

Matthias Boenig, Susanne Haaf und Marius Hug (alle Berlin-Brandenburgische
Akademie der Wissenschaften) sowie Nico Dorn, Volker Harm, Nathalie Mederake und Kerstin
Meyer-Hinrichs (alle Niedersächsische Akademie der Wissenschaften zu Göttingen) sprechen
über „Historische Textkorpora für die Lexikographie“.

Siehe auch: https://dhd-blog.org/?p=20142