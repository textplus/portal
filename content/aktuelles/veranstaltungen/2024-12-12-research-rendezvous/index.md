---
type: event
title: "Research Rendezvous"
start_date: 2024-12-12T00:00:00+0100
end_date: 2024-12-12T23:59:59+0100
all_day: true
publishDate: 2024-12-12T02:00:00+0100
expiryDate: 2024-12-13T02:00:00+0100
featured_image: research-rendezvous.png
location: Adventskalender
---

Bei unserer offenen Sprechstunde [Text+ Research Rendezvous](https://events.gwdg.de/category/208/) kann man einfach vorbeischauen und Anliegen zu text- & sprachbezogenen Forschungsdaten oder rund um #NFDI direkt mit Mitarbeitenden aus Text+ besprechen.

#textplus_advent #consulting #Forschungsdaten #academia #researchdata