---
type: event
title: "Text+ Registry"
start_date: 2024-12-22T00:00:00+0100
end_date: 2024-12-22T23:59:59+0100
all_day: true
publishDate: 2024-12-22T02:00:00+0100
expiryDate: 2024-12-23T02:00:00+0100
featured_image: registry-adventskalender.png
location: Adventskalender
---

Die [Text+ Registry](https://registry.text-plus.org/) bietet als Informations- und Nachweissystem einfachen Zugang zu Ressourcen, die für Forschung und Lehre in sprach-/textbasierten Disziplinen relevant sind, z.B. Korpora, Textsammlungen, Editionen und lexikalische Ressourcen.

#textplus_advent #digitalhumanities #Forschungsdaten #academia #researchdata 