---
type: event
title: "Von der Virtuellen Forschungsumgebung zur Nationalen Forschungsdateninfrastruktur"
start_date: 2022-11-10 14:00:00
end_date: 2022-11-10 17:00:00
all_day: false
location: online
aliases:
- /events/von-der-virtuellen-forschungsumgebung-zur-nationalen-forschungsdateninfrastruktur
---

