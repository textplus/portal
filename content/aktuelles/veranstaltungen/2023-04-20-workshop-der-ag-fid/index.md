---
type: event
title: "Workshop der AG-FID: \"Forschungsdatenmanagement gemeinsam voranbringen – Potenziale der Zusammenarbeit von NFDI und FID\""
start_date: 2023-04-20 09:30:00
end_date: 2023-04-20 15:45:00
all_day: false
location: online
featured_image: 
aliases:
- /events/workshop-der-ag-fid-forschungsdatenmanagement-gemeinsam-voranbringen-potenziale-der-zusammenarbeit-von-nfdi-und-fid
---


Ziel des Workshops ist es, die Potenziale der Zusammenarbeit von NFDI und FID beim Forschungsdatenmanagement zu eruieren. Hierfür stellen sich sieben aufgrund ihrer thematischen Nähe zu FIDs ausgewählte NFDI-Konsortien vor. Kooperationsmöglichkeiten sowie Arbeitsteilung und Abgrenzung werden insbesondere in den Themenbereichen „Normdaten & Metadaten“, „Recherche & Nachweis“, „Publikation & Archivierung“, „Beratung & Weiterbildung“ diskutiert.

Programm und Anmeldung: https://wikis.sub.uni-hamburg.de/webis/images/2/26/2023-04-20-FID-NFDI-Workshop_Programm.pdf