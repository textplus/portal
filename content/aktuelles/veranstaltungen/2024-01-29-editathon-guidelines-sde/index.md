---
type: event
title: "Editathon zu den Guidelines for Quality Assessment and Assurance for Digital Editions"
start_date: 2024-01-29 13:00:00
end_date: 2024-01-30 14:00:00
all_day: false
location: virtuell
---

Die Kolleg:innen des Text+-Measures M3 Standardisierung der Datendomäne Editionen veranstalten einen virtuellen Editathon. Ziel ist es, der ersten Version der Guidelines for Quality Assessment and Assurance for Digital Editions auf Grundlage des im laufenden Jahr erarbeiteten Konzepts eine konkrete Gestalt zu geben, indem so viele Textbausteine einzeln oder in Kleingruppen erarbeitet werden, wie möglich.
Die Schreibwerkstatt soll den direkten und unkomplizierten Gedankenaustausch zwischen den Vertreter:innen der editionswissenschaftlichen Community ermöglichen und die Vorüberlegungen bündeln, die im vergangenen Jahr in Text+ gemacht wurden. Die Arbeit findet in Breakout Rooms statt.

Datum und Uhrzeit: 
29. Januar 2024 / 13.30-16.30 Uhr
30. Januar 2024/ 9.00-12.00 Uhr

Link zum Veranstaltungsort: https://meet.gwdg.de/b/kar-zby-kd5-lcl

Da der Redaktionsworkflow in einer geschlossenen Umgebung stattfindet, bitte ich Sie sich vorab unter karoline.lemke@bbaw.de anzumelden, sodass die für die weitere Teilnahme erforderlichen Schritte eingeleitet werden können.