---
type: event
title: "FAIR February 2024"
start_date: 2024-02-01
end_date: 2024-02-22
all_day: true
location: https://uni-goettingen.zoom-x.de/j/69271597237?pwd=OHFKeE56bmhvSjRLQkpTL3hpNWVZQT09
---

Im FAIR February, der 2024 zum zweiten Mal veranstaltet wird, widmet sich
Text+ einen Monat lang wöchentlich in einer virtuellen Veranstaltung den vier
FAIR-Prinzipien Findability, Accessibility, Interoperability und Reusability, die sich
seit ihrer Veröffentlichung 2016 zu Leitlinien innerhalb der Digital Humanities entwickelt
haben. Anhand von Impulsen werden die einzelnen Prinzipien auf ihre Bedeutung, Anwendung
bzw. Anwendbarkeit im Bereich der digitalen Editionen diskutiert.

Die Veranstaltung findet jeweils donnerstags statt und wird von Vertreterinnen und
Vertretern der Task Area Editionen organisiert. Weitere Informationen finden sich auf der
Event-Seite (s. Link), über die die Anmeldung zu den einzelnen Meetups möglich ist.

Mehr Infos: https://events.gwdg.de/event/575/