---
type: event
title: "InFoDiTexT+ Vortrag: Semantische Erschließung in historisch-philologischen Kommentaren"
start_date: 2023-01-11
all_day: true
aliases:
- /events/infoditext-vortrag-semantische-erschliesung-in-historisch-philologischen-kommentaren
---

Das Interdisziplinäre Forum digitaler Textwissenschaften ist ein offener Treffpunkt für alle, die mit Methoden der Digital Humanities arbeiten und über deren Hintergründe und Theorien diskutieren möchten. Einmal monatlich während der Vorlesungszeit finden öffentliche Vorträge statt mit der Möglichkeit, sich daran anschließend in gemütlicher Runde auszutauschen. Alle am Thema Interessierten sind zu den Veranstaltungen eingeladen. Es sollen besonders Junior Researchers (Masterstudierende, Promovierende, Postdocs) angesprochen werden, eigene Projektideen im Rahmen des Forums vorzustellen und von der fachübergreifenden Expertise der Teilnehmer\*innen zu profitieren.

Bei der Programmplanung steht das ausgewogene Verhältnis der verschiedenen Textwissenschaften und Veranstaltungsformate im Mittelpunkt, weshalb sich im Programmangebot Vorträge, Projektvorstellungen, Methodenreflexionen und "Im Dialog mit ..."-Veranstaltungen abwechseln. Das Forum umfasst neben der Vortragsreihe auch weitere Formate. Insbesondere ist auch der Dialog mit externen Gesprächspartnern (z.B. Data Scientists, Projekte an anderen Universitäten usw.) eine zentrale Säule des Programms.

**Dozent**  
Philipp Hegel, Technische Universität Darmstadt, Institut für Sprach- und Literaturwissenschaft

**Anmeldung**  
InFoDiTexT+-Veranstaltungen finden virtuell statt. Eine Anmeldung ist bis zum Vortag der Veranstaltung bei Kevin Wunsch (mailto:kevin.wunsch@tu-darmstadt.de) möglich, von dem Sie auch die Zugangsdaten erhalten.
