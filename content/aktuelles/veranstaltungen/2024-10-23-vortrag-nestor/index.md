---
type: event
title: "Zertifizierung nach nestor"
start_date: 2024-10-23  09:00:00
end_date: 2024-10-23 10:15:00
all_day: false
location: virtuell
---

Auf der Grundlage der DIN-Norm 31644 „Kriterien für vertrauenswürdige digitale Langzeitarchive“ bietet nestor die Begutachtung von Archiven an, wie sie zum Beispiel im Rahmen von NFDI von verschiedenen Konsortien betrieben werden, um Forschungsdaten zu verwalten, aufzubewahren und verfügbar zu machen. In Text+ gibt es z.B. mehr als 20 Zentren, die Daten halten und zur Verfügung stellen. Einige davon haben bereits das Core Trust Seal als Zeritifikat erhalten, andere werden sich erstmals zertifizieren lassen und bereiten sich auf die nestor-Zertifizierung vor.

In einem 45 Minuten Vortrag wird vorgestellt, welche Kriterien für das nestor-Siegel erfüllt werden müssen und wie der Begutachtungsprozess abläuft. Nach dem Vortrag wird es weitere 30 Minuten Zeit für Fragen geben.

## Organisatorisches 
Der Vortrag findet via Zoom statt. Der Zoom-Link wird nach der [Anmeldung](https://events.gwdg.de/event/969/) vor der Veranstaltung per E-Mail verschickt. 

## Zielgruppe
Der Vortrag richtet sich an diejenigen, die Datenzentren betreiben und eine Zertifizierung im Rahmen von nestor planen. 

## Vortragend
[Dr. Astrid Schoger](https://orcid.org/0000-0002-2946-9203)

[Bayerische Staatsbibliothek](https://ror.org/031h71w90)

