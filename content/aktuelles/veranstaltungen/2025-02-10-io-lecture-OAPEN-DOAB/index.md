---
type: event
title: "Text+ IO-Lecture: OAPEN & Directory of Open Access Books"
start_date: 2025-02-10 11:00:00
end_date: 2025-02-10 12:45:00
all_day: false
location: virtuell
featured_image: OAPEN_Logo.png
event_url: https://events.gwdg.de/event/1009/
---
[OAPEN](https://oapen.org/) bietet ein breites Spektrum an Fachwissen, Dienstleistungen und gut kuratierten wissenschaftlichen Inhalten für Bibliothekare, Verleger, Forscher und Geldgeber. OAPEN fördert und unterstützt den Übergang zu Open Access für akademische Bücher durch die Bereitstellung offener Infrastrukturdienste für die Akteure der wissenschaftlichen Kommunikation. Wir arbeiten mit Verlagen zusammen, um eine qualitätsgeprüfte Sammlung von Open-Access-Büchern aufzubauen, und bieten Dienstleistungen für Verlage, Bibliotheken und Forschungsförderer in den Bereichen Hosting, Hinterlegung, Qualitätssicherung, Verbreitung und digitale Bewahrung.

Das [DOAB](https://www.doabooks.org/) (Directory of Open Access Books) ist ein von der Community betriebener Suchdienst, der wissenschaftliche, von Experten begutachtete Open-Access-Bücher indexiert und zugänglich macht und Nutzern hilft, vertrauenswürdige Open-Access-Buchverlage zu finden. Alle DOAB-Dienste sind kostenlos und alle Daten sind frei verfügbar.

Beide Initiativen sind für die Gemeinschaften rund um die NFDI-Konsortien der MoU-Gruppe von Bedeutung: NFDI4Culture, Text+, NFDI4Memory, und NFDI4Objects.

Silke Davison und Sebastiano Sali, Community Manager bei OAPEN & DOAB, werden die Zuhörer in beide Dienste einführen, die Vorteile für unsere Communities aufzeigen und über einige der vielen bereits bestehenden Arbeitsbeziehungen mit Institutionen der deutschen Wissenschaft sprechen.

Am Ende des Vortrags sollen die Teilnehmer Lust bekommen, sich mit den Diensten von OAPEN und DOAB sowie mit Open-Access-Publikationen zu beschäftigen.

Dieser Vortrag wird in Englisch gehalten.

## Zielpublikum
Die Veranstaltung richtet sich an Personen, die in den Sozial- und Geisteswissenschaften forschen, an einer Infrastruktureinrichtung in diesem Bereich arbeiten oder sich einfach für das Thema interessieren. Darüber hinaus sind auch Teilnehmer aus anderen NFDI-Konsortien besonders willkommen.
