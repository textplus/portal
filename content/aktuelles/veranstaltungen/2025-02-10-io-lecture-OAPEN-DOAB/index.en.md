---
type: event
title: "Text+ IO-Lecture: OAPEN & Directory of Open Access Books"
start_date: 2025-02-10 11:00:00
end_date: 2025-02-10 12:45:00
all_day: false
location: virtuell
featured_image: OAPEN_Logo.png
---
[OAPEN](https://oapen.org/) offers a wide range of expertise, services and well-curated academic content aimed at librarians, publishers, researchers, and funders. OAPEN promotes and supports the transition to open access for academic books by providing open infrastructure services to stakeholders in scholarly communication. We work with publishers to build a quality-controlled collection of open access books and provide services for publishers, libraries, and research funders in the areas of hosting, deposit, quality assurance, dissemination, and digital preservation.

The [DOAB](https://www.doabooks.org/) (Directory of Open Access Books) is a community-driven discovery service that indexes and provides access to scholarly, peer-reviewed open access books and helps users to find trusted open access book publishers. All DOAB services are free of charge and all data is freely available.

Both initiatives are of relevance for the communities around the NFDI consortia of the MoU group: NFDI4Culture, Text+, NFDI4Memory, and NFDI4Objects.

Silke Davison and Sebastiano Sali, Community Managers at OAPEN & DOAB, will introduce the audience into both services, highlight benefits four our communities, and speak about some of the many already existing working relations with institutions of the German academia.

At the end of this lecture, the participants shall feel intrigued to explore the services of OAPEN and DOAB and open access publishing for their own research projects.  

This lecture will be held in English.

## Intended Audience
The event is aimed at individuals conducting research in the social sciences and humanities, working at an infrastructure facility in the field, or simply interested in the topic. Furthermore, we especially welcome participants from other NFDI consortia.

## Registration
For [registration please click here](https://events.gwdg.de/event/1009/).