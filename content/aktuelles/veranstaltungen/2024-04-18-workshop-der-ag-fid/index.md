---
type: event
title: "4. FID / Text+ Jour Fixe - virtuell geht es weiter!"
start_date: 2024-04-18 13:00:00
end_date: 2024-04-18 16:30:00
all_day: false
location: virtuell
---
# 4. FID / Text+ Jour Fixe - virtuell geht es weiter!
Die gemeinsam etablierte Veranstaltungsreihe FID/Text+ Jour Fixe geht in die vierte Runde! Unter dem Oberbegriff "Kontinuitäten" treffen wir uns dieses Mal wieder online am 18. April 2024 ab 13 Uhr, um begonnene Diskussionen, Planungen und Projekte aufzunehmen und weiterzuführen sowie neue Arbeiten gemeinsam anzugehen.

## Was ist nochmal der FID/Text+ Jour Fixe?  
Die Reihe wurde von der AG FID Koop im NFDI Konsortium Text+ angestoßen, um beide Strukturen zu vernetzen und der Forschung ein gemeinsam abgestimmtes Angebot an Expertise, Ressourcen und Services zur Verfügung zu stellen. 

Nach der Auftaktveranstaltung im Oktober 2022 zum Schwerpunkt "Erwartungsmanagement" haben wir beim 2. Termin das Thema "Consulting" in den Fokus gerückt. Im letzten November hatten wir dann in Köln die Gelegenheit, endlich auch in Person zusammenzutreffen, um die verschiedenen Formen unserer Zusammenarbeit möglichst konkret vorantreiben und zugleich neuen Partnern die Möglichkeit eines guten Einstiegs zu bieten (Programm der Kölner Veranstaltung).

Seit dem Auftakt haben sich eine ganze Menge konkreter Formen der Zusammenarbeit entwickelt:  
Vertreter:innen von FIDen sind Mitglieder der Beiräte (SCC/OCC) von Text+. Sie gestalten die Ausrichtung des Konsortiums mit und begutachten Förderanträge, die jährlich im Rahmen der sogenannten "Text+ Kooperationsprojekte" vergeben werden. Wir haben uns zudem personaltechnisch über Matrixstellen vernetzt und unsere Zusammenarbeit auf der ersten NFDI Gesamtkonferenz (CoRDI) vom 12.-14. September 2023 in Karlsruhe vorgestellt (Poster | Abstract). Kolleg:innen verpartneter FIDe sind auf nahezu jeder Veranstaltung von Text+ vertreten. 

## Worum geht es beim 4. Jour Fixe?  
Da die Bearbeitung eines breiteren Themenspektrums sich beim letzten Treffen bewährt hat, wird der 4. Jour Fixe unter dem Titel "Kontinuitäten" stehen. Gemeinsame Projekte und Aufgaben wurden angegangen, Diskussionen begonnen, und es gilt, diese möglichst zielgerichtet weiterzuführen sowie die Kooperation(en) kontinuierlich auf- und auszubauen.  
Wir als AG FID Koop würden uns freuen, Sie am Nachmittag des 18. April auf Zoom (Link folgt) begrüßen zu dürfen. Sollte es Ihrerseits Fragen oder Anregungen geben, wenden Sie sich gern an uns.  

Die AG FID Koop

## Programm und Anmeldung
https://events.gwdg.de/event/647/