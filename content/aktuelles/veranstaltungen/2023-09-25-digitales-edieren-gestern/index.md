---
type: event
title: "Digitales Edieren gestern, heute und morgen | The Past, Present, and Future of Digital Editing"
start_date: 2023-09-25 13:15:00
end_date: 2023-09-26 20:00:00
all_day: false
location: Wolfenbüttel (hybrid)
featured_image: images/P1010116-1024x768.jpg
aliases:
- /events/digitales-edieren-gestern-heute-und-morgen-the-past-present-and-future-of-digital-editing
---

Eine wissenschaftliche Tagung an der Herzog August Bibliothek Wolfenbüttel vom 25. bis 27. September 2023.

Das Programm, eine Anmeldemöglichkeit sowie weitere Informationen finden Sie [hier](https://events.gwdg.de/event/363/).

Siehe auch: [Beitrag zur Veranstaltung im Text+ Blog](https://textplus.hypotheses.org/2895)
