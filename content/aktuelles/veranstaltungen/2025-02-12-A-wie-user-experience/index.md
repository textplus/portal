---
type: event
title: "FAIR February 2025: A wie User Experience"
start_date: 2025-02-12 14:00:00
end_date: 2025-02-12 16:30:00
all_day: false
location: virtuell
event_url: https://events.gwdg.de/event/948/
---
Text+ und das [Service Center for Digital Humanities Münster (SCDH)](https://www.uni-muenster.de/DH/scdh/) erstellen gemeinsam einen Interview-Leitfaden zur User Experience von Editionen. Erfahren Sie, wie wir mithilfe eines Interviewkonzepts eine Datenbasis schaffen wollen, die Aussagen darüber zulässt, welche Bedürfnisse, Absichten und Ziele Nutzende von (digitalen) Editionen verfolgen und welche Lehren sich daraus für diejenigen ziehen lassen, die Editionen erstellen oder fördern.