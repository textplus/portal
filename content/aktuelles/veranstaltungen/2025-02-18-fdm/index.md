---
type: event
title: "FDM Kurs für DoktorandInnen: FDM für die Geistes- und Sozialwissenschaften"
start_date: 2025-02-18 10:00:00
#end_date: 2025-02-18 16:00:00
all_day: false
location: Virtuell
event_url: https://uni-tuebingen.de/forschung/forschungsinfrastruktur/forschungsdatenmanagement-fdm/fdm-kurs-fuer-doktorandinnen/
featured_image: Poster_RDMCourse_PhDstudents_small.png
---
Kurs für Promovierende der Universität Tübingen in Zusammenarbeit mit Text+ zum Themenbereich FDM.

Weitere Termine:

- 19\. Februar, 10:00 st, Zoom
- 25\. Februar, 14:00 st, Zoom
