---
type: event
title: "Praktische Anwendung des Urheber- und Leistungsschutzrechts für
Vorhaben im Akademienprogramm"
start_date: 2023-10-26 11:00:00
end_date: 2023-10-26 17:00:00
all_day: false
location: Geistraße 10, 37073 Göttingen und Zoom-Raum
---

Themen: Rechtlichen Rahmenbedingungen des Online-Publizierens
(Urheberrechtsgesetz, Zitatrecht, Leistungsschutzrecht etc.) in den Akademievorhaben.
Publikationsformen (Open Access, CC-Lizenzen, FAIR, Verlagsgebunden etc.). Leitlinie zur
Bereitstellung von Forschungsdaten der NAWG. Referent: Moritz Griesel (Univ. Göttingen)

Format: in Präsenz, virtuelle Teilnahme mit Zoom möglich
Teilnehmer:innen: max. 25
Ort:  Geistraße 10, Göttingen und Zomm-Raum
Sprache: Deutsch, ggf. Englisch
Anmeldung: bis 18.10.2023

https://digitale-akademie.adw-goe.de/2023/03/02/fortbildung-urheber-und-leistungsschutzrecht-in-akademievorhaben-26-27-10-2023/