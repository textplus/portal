---
type: event
title: "BEACONe unde venis et quo vadis?"
start_date: 2024-02-27
end_date: 2024-02-27
all_day: true
location: DHd2024, Passau
---

Der Workshop, der im Rahmen der DHd 2024 stattfindet, zielt im Sinne der
FAIR-Prinzipien und im Hinblick auf konkrete Standardisierungsbestrebungen auf die
Vernetzung geistes- und kulturwissenschaftlicher Daten durch Erstellung sogenannter
BEACON-Dateien auf Basis der Gemeinsamen Normdatei (GND). In dem Workshop werden zunächst
zwei Beispiele vorgeführt, an denen das grundlegende Vorgehen erläutert wird. Der
Schwerpunkt liegt allerdings auf der praktischen Anwendung und Hands-On-Erprobung des
BEACON-Verfahrens. Abgerundet wird die Veranstaltung durch Ausblicke auf erweiterte
Anwendungsbereiche des Formats.

Der Workshop wird gemeinsam von Mitarbeitenden der Text+ Task Areas Editions und
Infrastructure/Operations bestritten und richtet sich an Forschende aller Disziplinen der
(Digital) Humanities mit Interesse an Vernetzung von Daten und Linked Data, insbesondere
auch an Nachwuchswissenschaftler:innen. Vorwissen wird nicht vorausgesetzt. Um einen guten
Betreuungsschlüssel zu gewährleisten, können bis zu 25 Personen am Workshop teilnehmen. Es
besteht die Möglichkeit, eigene Daten mitzubringen und mit diesen zu arbeiten.

Eine Anmeldung zur DHd ist über https://conftool.net/dhd2024 möglich.

Mehr Infos: https://dhd2024.dig-hum.de/