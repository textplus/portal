---
type: event
title: "Auftaktveranstaltung des Datenkompetenzzentrums QUADRIGA"
start_date: 2024-07-01 18:00:00
end_date: 2024-07-01 20:00:00
all_day: false
location: Virtuell
---
Wie lässt sich Datenkompetenz in heterogene geistes- und sozialwissenschaftliche Zielgruppen vermitteln? Wie lassen sich hierfür Open Educational Resources nutzen? Und welchen Beitrag können die [Datenkompetenzzentren](https://www.bildung-forschung.digital/digitalezukunft/de/wissen/Datenkompetenzen/datenkompetenzzentren_f%C3%BCr_die_wissenschaft_ordner/datenkompetenzzentren_fuer_die_wissenschaft_node.html) leisten, um hier einen Kulturwandel herbeizuführen?

Zu diesen und anderen Fragen möchten wir uns mit Ihnen im Rahmen der Vorstellung der Arbeit von QUADRIGA und einer Paneldiskussion gemeinsam austauschen. Die Veranstaltung findet am *per Zoom* statt. Weitere Infos zum Programm folgen in Kürze.

Den Zoomlink erhalten Sie bei [Anmeldung](https://hu-berlin.zoom-x.de/meeting/register/u5AoceqtqzIuGdKuPMmqPTbAc4mVSL2Cg7W8).

Weitere Infos zum [Datenkompetenzzentrum QUADRIGA](https://quadriga-dk.github.io/)