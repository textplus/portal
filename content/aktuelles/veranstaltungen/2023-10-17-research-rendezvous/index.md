---
type: event
title: "Text+ Research Rendezvous"
start_date: 2023-10-17 09:00:00
end_date: 2023-10-17 10:00:00
all_day: false
location: Virtuell
---

Im Rahmen einer offenen Sprechstunde unseres Helpdesks können Sie Anliegen und Fragen rund um die NFDI sowie zu text- und sprachbezogenen Forschungsdaten direkt mit Mitarbeitenden aus Text+ besprechen. Die Research Rendezvous finden ab 5. Oktober alle zwei Wochen im Wechsel dienstags (9:00) und donnerstags (10:00) statt.

Weitere Termine: 2.11. (10–11 Uhr), 14.11. (9–10 Uhr), 30.11. (10–11 Uhr), 12.12. (9–10 Uhr)

https://events.gwdg.de/event/564/