---
type: event
title: "Transkribus"
start_date: 2024-12-02T00:00:00+0100
end_date: 2024-12-02T23:59:59+0100
all_day: true
publishDate: 2024-12-02T00:02:00+0100
expiryDate: 2024-12-03T00:02:00+0100
featured_image: transkribus-adventskalender.png
location: Adventskalender
---

Transkribus ist eine umfassende und weit verbreitete Plattform für die Digitalisierung, Texterkennung mithilfe Künstlicher Intelligenz, Transkription und das Durchsuchen von historischen Dokumenten. Mehr dazu in unserer Plattform für Editions-Tools: https://research-software.cceh.uni-koeln.de/software/transkribus 

#textplus_advent #TextRecognition #MachineLearning #OCR #Transcription #nfdi.software