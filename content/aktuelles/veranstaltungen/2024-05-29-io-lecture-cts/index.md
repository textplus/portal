---
type: event
title: "10. IO-Lecture: Das TextGrid Repository & das Core Trust Seal"
start_date: 2024-05-29 10:00:00
end_date: 2024-05-29 12:00:00
all_day: false
location: virtuell
---
# 10. IO-Lecture: Das TextGrid Repository & das Core Trust Seal

Das TextGrid Repository (TextGridRep) ist ein seit langem etabliertes digitales Langzeitarchiv für geisteswissenschaftliche Forschungsdaten (insbesondere Texte in XML/TEI) und bietet eine Vielzahl an Daten für Lehr- und Forschungszwecke. Es folgt den Prinzipien des Open Access, bedient offene Standards und erlaubt damit eine effiziente Nachnutzung für die Forschung. Das TextGridRep bietet Wissenschaftler*innen weiterhin umfassende und zuverlässige Dienste für die permanente Datensicherung, gut dokumentiert und mit stabilen Referenzen für Zitation und Nachhaltigkeit.

In dieser Lecture soll es um das CoreTrust Seal gehen. Das TGRep war von 2020–2023 CoreTrust Seal-zertifiziert, die Wiederbeantragung ist bereits eingereicht. Durch das etablierte Qualitätssiegel CTS zeigt das TGRep, dass es ein vertrauenswürdiges Forschungsdatenrepositorium ist. Es wird angestrebt, das CTS in Text+ als Quasistandard für die beteiligten Datenzentren zu etablieren.

Vortragen werden Stefan E. Funk und Lukas Weimer, die den Zertifizierungsprozess für das TGRep durchgeführt haben.

## Zielgruppe der IO-Lecture
Die Veranstaltung richtet sich in erster Linie an Mitarbeitende von Text+, die Forschung innerhalb der Sozial- und Geisteswissenschaften betreiben und/oder in der Beratung tätig sind. Darüber hinaus freuen wir uns aber auch über Teilnehmende aus anderen NFDI-Konsortien. Im Zweifelsfalle kontaktieren Sie gerne das Text+ Operations Office.

## Anmeldung
Zur [Anmeldung bitte hier](https://events.gwdg.de/event/685/) entlang.

[def]: ../2024-05-29-io-lecture-cts/index.md