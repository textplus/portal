---
type: event
title: "Erschließen, Forschen, Analysieren"
start_date: 2024-11-14 09:00:00
end_date: 2024-11-14 17:00:00
all_day: false
location: virtuell
summary: "Ein offener Info- und Workshoptag der Deutschen Nationalbibliothek (DNB) zu ihren Metadaten und freien digitalen Objekten am 14. November 2024"
featured_image: EfaForEver_buehne-teaser.jpg
---

Sie interessieren sich für die Metadaten und die freien digitalen Objekte der Deutschen Nationalbibliothek? Sie wollen mit unseren Daten Ihre Bestände erschließen? Sie interessieren sich für Datenanalysen? Sie wollen wissen, wie Sie unsere Daten beziehen und analysieren können? Oder Sie sind neu im Themenfeld und wollen sich informieren?

Am 14. November 2024 findet der virtuelle Info- und Workshoptag „Erschließen, Forschen, Analysieren“ (EFA24@DNB) zu unseren Metadaten und freien digitalen Objekten statt.

Wir laden Sie ein, unsere Metadaten und freien digitalen Objekte kennenzulernen, und zeigen Ihnen, wie vielseitig diese genutzt, analysiert und visualisiert werden können.

Das Programm ist modular gestaltet, die Programmpunkte bauen logisch aufeinander auf und können je nach Interessenlage auch einzeln besucht werden. Vorkenntnisse sind nicht erforderlich. Der Info- und Workshoptag findet in deutscher Sprache statt.

## Programm
09:00 – 09:20 Uhr
Willkommen bei der Deutschen Nationalbibliothek!
Sie erhalten erste Einblicke in die Deutsche Nationalbibliothek, ihre Geschichte und ihre (digitalen) Sammlungen. Außerdem stellen wir Ihnen den Programmablauf vor.

09:20 – 10:15 Uhr
Die verschiedenen Metadaten und Metadatenformate der Deutschen Nationalbibliothek
Welche Metadaten nutze ich für welchen Zweck? Welche Formate gibt es?

10:15 – 10:45 Uhr
Die Gemeinsame Normdatei (GND)
Was ist eine Normdatei und von wem und für welche Zwecke wird die GND genutzt? Welche Daten kann man in der GND erwarten und wie können sie genutzt werden?

10:45 – 11:00 Uhr
Pause

11:00 – 11:10 Uhr
Zugang zu unseren Daten
Welche Zugänge gibt es zu unseren Daten, und welcher Zugang ist für welchen Zweck sinnvoll? Einige Zugänge haben wir bereits für Sie vorkonfiguriert. Welche sind das, und wie finden Sie Informationen dazu?

11:10 – 11:50 Uhr
Zugang spezial: Unsere offenen Schnittstellen
Es gibt verschiedene Schnittstellen, um unsere Daten und Inhalte zu nutzen. Welche sind das, welche Ausgabeformate gibt es, und was nutzt man für welchen Zweck?

11:50 – 12:10 Uhr
Das DNBLab
Als zentralen Anlaufpunkt für Abfragen und Analysen unserer Daten und freien Inhalte geben wir Ihnen einen kurzen Einblick ins DNBLab.

12:10 – 13:00 Uhr
Mittagspause

13:00 – 14:30 Uhr
Workshops (Block I):

DNBLab: Daten bereinigen und zusammenführen
Wir selektieren gemeinsam ein Datenset über die SRU-Schnittstelle und bereiten die Daten mit OpenRefine für eine exemplarische Analyse vor.

DNBLab: SRU Volltextdownload und Visualisierung
Mit Hilfe einer SRU-Abfrage und Python laden wir Volltexte im PDF-Format herunter. Anschließend visualisieren wir einzelne Aspekte dieser Daten.

Metadatendienste: Medien und Metadaten suchen und finden
Wir recherchieren Medien in verschiedenen Katalogen (KVK, WorldCat, DNB-Portal) und über die SRU-Schnittstelle und laden Metadaten im DNB-Datenshop herunter.

Literaturrecherche mit Künstlicher Intelligenz
Wir probieren gemeinsam aus, ob und wie gängige KI-Tools wie ChatGPT, Claude oder Perplexity für die Literaturrecherche genutzt werden können.

14:30 – 15:00 Uhr
Pause

15:00 – 16:30 Uhr
Workshops (Block II):

DNBLab: Daten bereinigen und zusammenführen
Wir selektieren gemeinsam ein Datenset über die SRU-Schnittstelle und bereiten die Daten mit OpenRefine für eine exemplarische Analyse vor.

DNBLab: MARC-Analyse
Gemeinsam extrahieren wir aus einem kleinen Datendump verschiedene Informationen aus MARC21-Feldern und analysieren diese.

Metadatendienste: Medien und Metadaten suchen und finden
Wir recherchieren Medien in verschiedenen Katalogen (KVK, WorldCat, DNB-Portal) und über die SRU-Schnittstelle und laden Metadaten im DNB-Datenshop herunter.

Literaturrecherche mit Künstlicher Intelligenz
Wir probieren gemeinsam aus, ob und wie gängige KI-Tools wie ChatGPT, Claude oder Perplexity für die Literaturrecherche genutzt werden können.

16:30 – 16:45 Uhr
Fragen und Feedback

Information und Kontakt
Kosten: kostenfrei
Kontakt: lab@dnb.de

## Anmeldung
Zur [Anmeldung über Zoom bitte hier](https://eu02web.zoom.us/meeting/register/u5Ivd-qppz0jHtLW7P_u9PO2poszkhiEuY4H) entlang.