---
type: event
title: "InFoDiTexT+ Vortrag: Propyläen: Goethes Biographica – zwischen Druckersatz und Datens[ch]atz"
start_date: 2023-06-13 17:15:00
end_date: 2023-06-13 18:45:00
all_day: false
location: online
featured_image: images/InFoDiText2023_ChristianThomas.png
aliases:
- /events/infoditext-vortrag-propylaen-goethes-biographica-zwischen-druckersatz-und-datenschatz
---

_Referent: Christian Thomas, Berlin-Brandenburgische Akademie der Wissenschaften / Klassik Stiftung Weimar_

Auf der Forschungsplattform Propyläen. Goethes Biographica (https://goethe-biographica.de/) kommen im Rahmen eines Akademienvorhabens der Klassik Stiftung Weimar, der Sächsischen Akademie der Wissenschaften zu Leipzig, der Akademie der Wissenschaften und der Literatur | Mainz sowie dem Freien Deutschen Hochstift in Frankfurt am Main gleich vier vormals eigenständige Editionsprojekte zusammen: Die Briefe von Goethe, Briefe an Goethe, Goethes Tagebücher sowie die Begegnungen und Gespräche. Im Bereich der digitalen Transformation, spezieller: Digitaler Edition bzw. hybrider Publikationsverfahren besteht die Herausforderung insbesondere bei traditionsreichen und (planmäßig) noch länger laufenden Vorhaben darin, eine große Menge an ‚Legacy‘-Daten aus Print-Vorstufen transformieren, kuratieren und fortlaufend pflegen zu müssen wie auch eine ebenfalls große Menge an ‚Born Digital‘-Daten mit naturgemäß höherer Informationsdichte orientiert an internationalen Standards und Best Practices auf effiziente, anschlussfähige und nachhaltige Weise bereitstellen zu wollen. Der Vortrag gibt Einblicke in diesen laufenden Prozess mit einem Schwerpunkt auf dem notwendig zu vollziehenden Paradigmenwechsel, weg von einer Druck-Orientierung und hin zu einer Fokussierung auf die Forschungsdaten als zentrales Ergebnis editorischer Bemühungen.

Anmeldung: https://events.gwdg.de/event/450/