---
type: event
title: "Text+ Research Rendezvous"
start_date: 2024-08-08 10:00:00
end_date: 2024-08-08 11:00:00
all_day: false
location: Virtuell
---

Das NFDI-Konsortium Text+ bietet zusätzlich zum fortlaufenden Beratungsangebot eine virtuelle Sprechstunde des Helpdesks an.

Im zweiwöchigen Rhythmus können bei „Text+ Research Rendezvous“ Fragen rund um Collections, Editionen und Lexikalische Ressourcen sowie aus dem Bereich Infrastruktur/Betrieb zu festen Terminen (dienstags 9–10 Uhr bzw. donnerstags 10–11 Uhr) direkt mit Mitarbeitenden aus Text+ besprochen werden. Für umfangreichere Fragen werden gerne individuelle (Folge-)Termine vereinbart. Eine Anmeldung zur Sprechstunde ist nicht erforderlich. Ein Zutritt zur Sprechstunde ist während der gesamten Stunde möglich.

Für Fragen zur virtuellen Sprechstunde sowie anderen Beratungsmöglichkeiten steht Ihnen das Team des Text+ Helpdesks über das Formular unter https://www.text-plus.org/kontakt/ gerne jederzeit zur Verfügung.

[Link zum Raum](https://uni-koeln.zoom.us/j/92357450077?pwd=RmZwZHBnRVl3aUNRc3J3MjJLcXhYQT09)

[Übersicht zur Veranstaltungsreihe und kommenden Terminen](https://events.gwdg.de/category/208/)