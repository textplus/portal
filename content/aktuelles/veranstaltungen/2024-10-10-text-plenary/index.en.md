---
type: event
title: "Text+ Plenary on October 10th and 11th, 2024"
start_date: 2024-10-10 11:00:00
end_date: 2024-10-11 14:00:00
all_day: true
location: Mannheim
featured_image: Schloss_Mannheim.jpg
---

As a consortium of the National Research Data Infrastructure ([NFDI](https://www.nfdi.de/)), Text+ follows the goal of preserving language and text-based research data in the long term and enabling their use in science. Text+ organizes a plenary every year. It is aimed at all participants in the consortium, associates and the entire community working with text and other language data. We also warmly invite representatives of other NFDI consortia and interested others within and outside the NFDI to attend.

The upcoming 3rd Plenary, which will take place on October 10 and 11, 2024 at Mannheim Palace, will focus on the topic "Large Language Models (LLMs) and their use". In presentations, panel discussions and working group sessions, participants will exchange ideas on how they can use these technologies in their work and contribute to further development with their data and services.

For those who have had little background in LLMs, there will be an opportunity to participate in a pre-conference tutorial on Large Language Models (LLM) on the afternoon before the first day of the event, i.e. Wednesday, October 9th from 4pm to 6pm. This tutorial will present the basics and background of language models and provide an introduction to the topic. It will take place in the main lecture hall of the Leibniz Institute for the German Language (IDS). Participation is optional and also free of charge.

All contributors to the Text+ consortium as well as anyone interested in the project are welcome to attend the first day of the event (October 10, 2024). The scientific program will take place from 11 am to 6 pm. From 6 pm, Text+ invites you to a social get-together in the castle premises.

On the second day of the event (October 11, 2024), Text+ employees will meet for internal work meetings. The program starts at 9 am and ends at 1 pm with a joint lunch.

Participation in all offers of the 3rd Text+ Plenary 2024 is free of charge!

You can register now until September 15 at [https://events.gwdg.de/event/638/](https://events.gwdg.de/event/638/). On the website you will also find hotel and travel recommendations. The event program will also be published there in the coming weeks.