---
type: event
title: "Text+ Plenary am 10. und 11. Oktober 2024"
start_date: 2024-10-10 11:00:00
end_date: 2024-10-11 14:00:00
all_day: true
location: Mannheim
featured_image: Schloss_Mannheim.jpg
---

Als Konsortium der Nationalen Forschungsdateninfrastruktur ([NFDI](https://www.nfdi.de/)) verfolgt Text+ das Ziel, sprach- und textbasierte Forschungsdaten langfristig zu erhalten und ihre Nutzung in der Wissenschaft zu ermöglichen. Jedes Jahr richtet Text+ ein Plenary aus. Es richtet sich an alle Mitarbeitenden im Konsortium, Assoziierte und die gesamte Community, die mit Text- und anderen Sprachdaten arbeitet. Daneben laden wir Vertreterinnen und Vertreter anderer NFDI-Konsortien sowie Interessierte innerhalb und außerhalb der NFDI herzlich zur Teilnahme ein.

Das bevorstehende 3. Plenary, das am 10. und 11. Oktober 2024 im Schloss Mannheim stattfindet, steht unter dem Thema „Große Sprachmodelle (LLMs) und deren Nutzung“. In Vorträgen, Podiumsdiskussionen und Arbeitsgruppensitzungen tauschen sich die Teilnehmenden dazu aus, wie sie diese Technologien in ihren Arbeiten nutzen sowie mit ihren Daten und  Angeboten zur weiteren Entwicklung beitragen können.

Für diejenigen, die sich bisher nicht mit den Hintergründen von LLMs beschäftigt haben, gibt es die  Möglichkeit, am Nachmittag vor dem ersten Veranstaltungstag, d.h. am Mittwoch, den 09.10. von 16 Uhr bis 18 Uhr, an einem Pre-Conference-Tutorial zum Thema Large Language Models (LLM) teilzunehmen. Dieses Tutorial wird Grundlagen und Hintergründe zu Sprachmodellen vorstellen und eine Einführung in das Thema bieten. Es findet im Großen Vortragssaal des Leibniz-Instituts für Deutsche Sprache (IDS) statt. Die Teilnahme ist optional und ebenfalls kostenfrei.

Zum ersten Tag der Veranstaltung (10. Oktober 2024) sind sowohl alle  Mitarbeitenden im Konsortium Text+ als auch alle am Projekt Interessierten herzlich eingeladen. Das wissenschaftliche Programm findet von 11 Uhr bis 18 Uhr statt. Ab 18 Uhr lädt Text+ noch zu einem geselligen Beisammensein in den Räumlichkeiten des Schlosses ein.

Am zweiten Tag der Veranstaltung (11. Oktober 2024) treffen sich die in Text+ Mitarbeitenden zu internen Arbeitsmeetings. Das Programm beginnt um 9 Uhr und endet um 13 Uhr mit einem gemeinsamen Mittagessen.

Die Teilnahme an allen Angeboten des 3. Text+ Plenary 2024 ist kostenfrei!

Ihre Anmeldung ist ab sofort  bis zum 15.09. unter [https://events.gwdg.de/event/638/](https://events.gwdg.de/event/638/) möglich. Dort finden Sie zusätzlich Hotel- und Anreiseempfehlungen. Auch werden das  Programm der Veranstaltung dort im Laufe der nächsten Wochen veröffentlicht und aktualisierte Informationen bereitgestellt.