---
type: event
title: "FID Koop"
start_date: 2024-12-21T00:00:00+0100
end_date: 2024-12-21T23:59:59+0100
all_day: true
publishDate: 2024-12-21T02:00:00+0100
expiryDate: 2024-12-22T02:00:00+0100
featured_image: community-activities-adventskalender.png
location: Adventskalender
---

[Fachinformationsdienste (FIDs) sind für uns eine wichtige Schnittstelle im Austausch mit Fachcommunities](https://text-plus.org/vernetzung/fid/). Gemeinsam arbeiten wir an Themen wie bspw. der GND-Anreicherung, halbjährlich finden Arbeitstreffen, FID/Text+ Jour Fixes, statt.

#textplus_advent