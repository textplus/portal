---
type: event
title: "Brücken schlagen zwischen Terminologien: Herausforderungen und Perspektiven"
start_date: 2024-03-20 13:00:00
end_date: 2024-03-21 16:00:00
all_day: false
location: Online
---

Daten unterschiedlicher Herkunft, die mit standardisierten Terminologien beschrieben werden, können übergreifend verstanden und genutzt werden, was ihre Eindeutigkeit, Interoperabilität, Wiederverwendbarkeit und damit ihre Qualität erheblich verbessern kann. Vokabulare, Normdaten und Anwendungsontologien spielen eine wichtige Rolle bei der semantischen Vernetzung dezentraler Datenbestände. Insbesondere die Kultur- und Geisteswissenschaften stehen vor einer besonderen Herausforderung, denn sie benötigen eine große Bandbreite an Vokabular für die Beschreibung und Referenzierung ihrer Gegenstände und Konzepte (Personen, Geografika, Zeitangaben, Körperschaften, Werke/Objekte etc.), aber auch für die hinreichend spezifische Sachverschlagwortung.

Die Veranstaltung von NFDI4Culture (Task Area 2 – Standards, Datenqualität und
Kuratierung) soll konsortienübergreifend und unter Beteiligung von Text+ Überschneidungen
im Umgang mit und im Verständnis von Terminologien ausloten.

Anmeldung bitte bis zum 18.3.2024 über die Webseite.

Weitere Informationen unter https://nfdi4culture.de/id/E5304