---
type: event
title: "FAIR February 2025: F wie Qualitätssicherung"
start_date: 2025-02-05 14:00:00
end_date: 2025-02-05 16:30:00
all_day: false
location: virtuell
event_url: https://events.gwdg.de/event/948/
---
Die „Guidelines for Quality Assessment and Assurance for Digital Editions“  werden im Januar 2025 in einer ersten vollständigen Version online verfügbar sein. Wir laden zum Praxistest des Living Handbooks ein. Finden Sie sich im FAIRen Forschungsdatenzyklus zurecht?