---
type: event
title: "FAIR February 2025: R wie Dokumentation"
start_date: 2025-02-26 14:00:00
end_date: 2025-02-26 16:30:00
all_day: false
location: Virtuell
event_url: https://events.gwdg.de/event/948/
---
Eine umfassende Dokumentation trägt zur Nachnutzbarkeit von Forschungsdaten bei. Lernen Sie, wie effektives und qualitativ hochwertiges Dokumentieren geschickt in den Forschungsalltag integriert werden kann.
