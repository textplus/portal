---
type: event
title: "FID Text+ Jour Fixe"
start_date: 2022-10-18
all_day: true
aliases:
- /events/fid-text-jour-fixe
---



Am 18. Oktober 2022 hat die Auftaktveranstaltung zur Zusammenarbeit von Text+ mit den Fachinformationsdiensten stattgefunden.

Die Veranstaltung setzte den Auftakt für regelmäßige Treffen zwischen Vertreter:innen von Fachinformationsdiensten sowie von Text+ bzw. von Text+-Partnerinstitutionen. Geplant sind zwei Treffen pro Jahr in großer Runde (d.h. mit möglichst vielen Beteiligten). Diese Treffen sollen dem inhaltlichen Austausch und der Organisation der gemeinsamen Arbeiten dienen. Dabei sind jeweils unterschiedliche Schwerpunktsetzungen vorgesehen, die in Absprache mit den FIDs ausgewählt werden. Zusätzlich zu den halbjährlichen Gesamttreffen können sich nach Bedarf und Absprache kleinere Gruppen zusammenfinden, die gemeinsam an konkreten Aufgaben arbeiten. Organisiert wird die Veranstaltungsreihe von der AG FID Koop.

Die Orgaseite zur Auftaktveranstaltung ist [hier](https://events.gwdg.de/event/345/) zu finden.
