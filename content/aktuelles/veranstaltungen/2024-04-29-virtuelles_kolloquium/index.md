---
type: event
title: "Virtuelles DH-Kolloquium an der BBAW, 29.04.2024:‚Kurios und faszinierend‘. Prompt Engineering als Arbeits- und Forschungsmethode"
start_date: 2024-04-29 16:00:00
end_date: 2024-04-29 18:00:00
all_day: false
location: Virtuell
---

Im Rahmen des DH-Kolloquiums an der BBAW laden wir Sie herzlich zum nächsten Termin am Montag, den 29. April 2024, 16 Uhr c.t., ein (virtueller Raum: https://meet.gwdg.de/b/lou-eyn-nm6-t6b):

### Christopher Pollin (Universität Graz)

„Prompting is weird. Prompting matters”, schreibt Ethan Mollick, Professor für Innovation, Entrepreneurship und KI an der Wharton School (USA), in seinem Blog. Der Einsatz von künstlicher Intelligenz in den Digital Humanities hat mit den Fortschritten im Bereich der Large Language Models (LLM) eine neue Stufe erreicht. Prompt Engineering, d.h. die Optimierung der Eingabe in ein LLM, um die „besten” Antworten zu erhalten, kann die Ergebnisse des LLM erheblich verbessern – auch wenn es manchmal kurios erscheint, was der „beste” Prompt ist. Insbesondere „Agentic Workflows” bieten weiteres Potenzial, den Output eines LLM deutlich zu verbessern. Darunter wird die Simulation von Agenten durch Modelle wie GPT-4 oder Claude 3 verstanden, die Entscheidungen treffen, Aktionen festlegen, Werkzeuge benutzen und zusammenarbeiten. Der Vortrag diskutiert Prompting Engineering und Agentic Workflows als Arbeits- und Forschungsmethode anhand verschiedener Beispiele aus den Digital Humanities, aber auch aus anderen Disziplinen wie der Psychologie oder Bereichen der Musik- und Bildgenerierung. Durch Prompting Engineering erzielen Modelle wie GPT-4 spannende Ergebnisse in den Bereichen Modellierung, Datengenerierung, Codegenerierung, Datenextraktion oder Datenanalyse. Welche Voraussetzungen müssen geschaffen werden und welche Herausforderungen bringen die neuen Arbeitsmethoden, die sich mit LLMs ergeben, mit sich?

Es ist keine Anmeldung erforderlich. Einfach den oben verlinkten Raum nutzen.