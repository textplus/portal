---
type: event
title: "Nachhaltige Archivierung Sozialer Medien – Twitter und danach"
start_date: 2024-03-19 12:00:00
end_date: 2024-03-20 16:00:00
all_day: false
location: Deutsche Nationalbibliothek, Frankfurt am Main
---

Soziale Medien sind Datenquelle und Gegenstand für unterschiedliche Forschungsansätze in den Geistes- und Sozialwissenschaften, in der Informatik und in den Natur- und Lebenswissenschaften. In ihrer historischen Entwicklung sind sie Teil des digitalen Kulturerbes, für das eine ausdifferenzierte institutionelle Archivierung und Dokumentation erst in Ansätzen existiert, nicht zuletzt aufgrund der medientechnischen, ökonomischen, sozialen und ästhetischen Besonderheiten. Dadurch sind Forschung, Forschungsinstitute und Kulturerbeeinrichtungen im Hinblick auf die Archivierung, Bereitstellung und Nachnutzung vor vielfältige Probleme gestellt. Beispielhaft zeigt sich dies an Twitter (inzwischen „X“). Die Monetarisierung des plattformeigenen Archivs, Teil des laufenden Umbaus der Plattform, hat zu einschneidenden Konsequenzen für die Forschung und Archivierung geführt. Während flexible Programmierschnittstellen und Zugriffsmöglichkeiten bis Anfang 2023 zu einem Boom von Forschungsarbeiten geführt haben und die Erstellung umfangreicher Sammlungen ermöglichten, wurde der Zugang für Forschung und Archivierung seitdem stetig erschwert.

Die Archivierung, Erschließung und Bereitstellung dynamischer Daten aus sozialen Medien ist dadurch mit Problemstellungen konfrontiert, die Forschende, Forschungseinrichtungen, Bibliotheken und Archive gleichermaßen betreffen, und für die im besten Fall gemeinsam Lösungsansätze entwickelt werden sollten. Dies erfordert umfassende Anstrengungen, die nicht allein aus einer Datencommunity oder Disziplin heraus zu leisten sind.

Ziel der Tagung ist daher die Vernetzung von Bibliotheken, Archiven, Forschungsinstituten und Forschenden im deutschsprachigen Raum, die sich mit der Archivierung und nachhaltigen Nutzung von Daten und digitalen Objekten aus sozialen Medien beschäftigen. Die Konferenzbeiträge sollten sich mit folgenden Themenschwerpunkten beschäftigen:

- Zusammenspiel von Forschung und Archiv
- Forschungsdatenprobleme, die der Tweet-basierten Forschung durch den Wegfall von Twitter als Datenprovider entstehen
- Status und Erhaltung von Social-Media aus archivarischer und kulturhistorischer Sicht, z. B. Posts, Interaktionen, Plattformelemente
- Zusammenführen von Sammlungen, Korpora, Beständen (Metadaten...)
- Initiativen zur Archivierung und Erschließung von Social-Media-Daten
- Konzepte für die Bereitstellung von Derivaten (aggregierten oder abgeleiteten Formaten) aus sozialen Medien und für deren Nutzung
- Ethische Fragen
- Rechtliche Fragen
- Möglichkeit einer Social-Media-Daten-Registry

Mehr Infos und CFP: https://www.dnb.de/twittertagung