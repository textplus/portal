---
type: event
title: "FAIR February 2025: I wie BEACON"
start_date: 2025-02-19 14:00:00
end_date: 2025-02-19 16:30:00
all_day: false
location: Virtuell
event_url: https://events.gwdg.de/event/948/
---
In der Veranstaltung wird gezeigt und ausprobiert, wie aus den Daten einer digitalen Edition eine BEACON- und eine RDF-Datei mit Anwendung von Normdaten erstellt und eine entsprechende Schnittstelle für die Edition eingerichtet werden kann. Außerdem wird ein GND-BEACON-Hub vorgestellt, der solche Dateien von verschiedenen digitalen Ressourcen durchsucht und aggregiert und diese so untereinander vernetzt.
