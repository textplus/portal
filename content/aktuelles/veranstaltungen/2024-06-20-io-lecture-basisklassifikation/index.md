---
type: event
title: "11. IO-Lecture: Die Basisklassifikation in den User Stories von Text+"
start_date: 2024-06-20 10:00:00
end_date: 2024-06-20 11:00:00
all_day: false
location: virtuell
---
# 11. IO-Lecture: Die Basisklassifikation in den User Stories von Text+
In der Lecture wird erläutert, welche Vorteile eine zusätzliche Klassifizierung der User Stories mittels Basisklassifikation für die Text+-Community hat, wie dieses Vorhaben umgesetzt und anschließend in einer durchsuchbaren Zotero-Bibliografie veröffentlicht wurde. Da die ursprünglich vom Konsortium verwendeten Schlagwörter ad-hoc während des Auswertungsprozess entstanden sind und keinem normierten Vokabular entsprechen, ist eine Vergleichbarkeit mit anderen Ressourcen im Sinne einer Integration von verteilten Ressourcen nur sehr erschwert möglich. Dieser Umstand wird durch die von uns beschriebene Vorgehensweise verbessert.

Vortragende: José Calvo-Tello und Charlotte Feidicker (beide SUB Göttingen)

## Zielgruppe der IO-Lecture
Die Veranstaltung richtet sich in erster Linie an Mitarbeitende von Text+, die Forschung innerhalb der Sozial- und Geisteswissenschaften betreiben und/oder in der Beratung tätig sind. Darüber hinaus freuen wir uns aber auch über Teilnehmende aus anderen NFDI-Konsortien. Im Zweifelsfalle kontaktieren Sie gerne das Text+ Operations Office.

## Anmeldung
Zur [Anmeldung bitte hier](https://events.gwdg.de/event/700/) entlang.