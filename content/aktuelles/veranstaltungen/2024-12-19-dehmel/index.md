---
type: event
title: "Dehmel_digital"
start_date: 2024-12-19T00:00:00+0100
end_date: 2024-12-19T23:59:59+0100
all_day: true
publishDate: 2024-12-19T02:00:00+0100
expiryDate: 2024-12-20T02:00:00+0100
featured_image: dehmel-adventskalender.png
location: Adventskalender
---

[Dehmel Digital](https://dehmel-digital.de/) macht die handschriftlichen Originalbriefe an das Ehepaar Dehmel zugänglich und bietet vielfältige Lektüremodi und Nutzungsmöglichkeiten, die mit Hilfe computationeller Verfahren realisiert wurden.

#computergestützte #Korrespondenznetzwerke 