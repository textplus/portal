---
type: event
title: "Edit-a-thon zur Beschreibung von Ressourcen im SSH Open Marketplace"
start_date: 2024-06-24 14:00:00
end_date: 2024-06-24 18:00:00
all_day: false
location: Max Weber Stiftung, Bonn-Bad Godesberg
---
# Edit-a-thon zur Beschreibung von Ressourcen im SSH Open Marketplace
Text+ und der Verein für Geistes- und Kulturwissenschaftliche Forschungsinfrastrukturen (GKFI e.V.) führen in Zusammenarbeit mit dem SSH Open Marketplace auf Einladung der Max Weber Stiftung am 24. Juni 2024 einen Editathon durch.

## Ziel
Ziel unserer Veranstaltung ist es, Ressourcen für die geistes- und kulturwissenschaftliche Community, bspw. Dienste, Tutorials, Software oder ganze Workflows, im SSH Open Marketplace entweder neu anzulegen oder bestehende Beschreibungen zu kuratieren.

## Warum?
Der SSH Open Marketplace wird sowohl vom NFDI-Konsortium Text+ als auch vom Verein Geistes- und kulturwissenschaftliche Forschungsinfrastrukturen für die Darstellung der eigenen Angebote genutzt. Anstelle von (ggf. proprietären) Eigenentwickungen setzen Text+ und GKFI auf den Marketplace als eine bestehende Lösung:
* die verfügbar und auch mindestens mittelfristig verfügbar bleiben wird,
* intuitiv manuell bedienbar ist,
* über Harvesting gut in andere Infrastrukturen einbindbar ist, sowie
* mit einem aktiven Editorial Board als Scharnier zur Community ausgestattet ist.  

## Ablauf
Der Editathon wird mit einem kurzen Impuls von Michael Kurzmeier (Austrian Centre for Digital Humanities and Cultural Heritage) beginnen. Michael ist Mitglied des Editorial Board des SSH Open Marketplace und wird einen Einblick in die Struktur des Marketplace und Anwendungsbeispiele aus verschiedenen Kontexten geben.

Danach werden wir uns kurz die Anwendungsfälle von Text+ und GKFI anschauen und die Arbeitsweise und Ziele des Editathons erklären. Anschließend werden wir uns in Einzelarbeit und in Gruppen die Zeit nehmen, in entspannter Atmosphäre zusammen Ressourcen im Marketplace zu kuratieren. Idealerweise haben wir am Ende als Ergebnis neu hinzugekommene Ressourcen, viele angereicherte bestehende Ressourcen sowie vielleicht auch zwei, drei neu angelegte Workflows.

## Wer kann mitmachen?
Wir sprechen in erster Linie Beteiligte von Text+ sowie vom GKFI an. Herzlich eingeladen sind aber auch Kolleginnen und Kollegen aus anderen NFDI-Konsortien oder anderen Kontexten, die den SSH Open Marketplace für die Beschreibung ihrer Ressourcen oder Workflows nutzen möchten. Die disziplinäre Klammer sind Ressourcen mit einem geistes- und kulturwissenschaftlichem sowie sozialwissenschaftlichen Bezug gemäß der SSH-Ausrichtung.

## Wie kann ich mitmachen?
Relativ einfach! Bitte prüfen Sie vor der Anmeldung, ob Sie sich mit bereits vorhandenen Credentials im SSH Open Marketplace anmelden können. Dafür einfach auf https://marketplace.sshopencloud.eu/ rechts oben die Sign In-Funktion nutzen. 

Falls Sie dabei auf Probleme stoßen, melden sich gerne bei uns. Auch alle weiteren Fragen oder Wünsche können jederzeit gerne an uns gerichtet werden: textplus-operations-office@lists.gwdg.de

## Muss ich was mitbringen?
Wir werden Material vorbereiten, mit dem die Teilnehmerinnen und Teilnehmer ohne Vorkenntnisse vor Ort arbeiten können. Damit man aber auch "mit etwas nach Hause gehen kann", wäre es wichtig, wenn sich jede/jeder konkrete Ressourcen überlegt, die neu in den Marketplace aufgenommen werden könnten. Für Text+ können das bspw. Ressourcen aus den Daten- und Kompetenzzentren sein, die - sobald sie im Marketplace erschlossen sein - automatisch auch auf dem Text+ Portal erscheinen. Für die Partnereinrichtungen des GKFIs ist dies eine sehr gute Gelegenheit, die eigenen In-Kind Contributions zu beschreiben. Für alle anderen: die Sichtbarkeit eigener Ressourcen im Marketplace ist auch für sich bereits ein lohnenswerter Anreiz und sehr sinnvoll für Uptake und Sichtbarkeit.

Darüber hinaus ist es wichtig, dass jede/jeder ein Arbeitsgerät dabei hat. Ob Smartphone, Tablet oder Notebook ist dabei egal.  

Die Teilnahmemöglichkeit vor Ort in der Geschäftsstelle der Max Weber Stiftung ist auf 20 Personen beschränkt.

## Anmeldung
Zur [Anmeldung bitte hier](https://events.gwdg.de/event/765/) entlang.