---
type: event
title: "Autodone"
start_date: 2024-12-24T00:00:00+0100
end_date: 2024-12-24T23:59:59+0100
all_day: true
publishDate: 2024-12-24T02:00:00+0100
expiryDate: 2024-12-25T02:00:00+0100
featured_image: autodone-adventskalender.png
location: Adventskalender
---

https://fedihum.org/@autodone ist ein Dienst für die automatisierte, zeitgesteuerte Veröffentlichung von Nachrichten auf jeder Mastodon-Instanz. Es wird unter freier Lizenz vom https://fedihum.org/@IDH_Cologne (Philip Schildkamp & https://fedihum.org/@spinfocl) entwickelt. Ein ideales WissKomm-Tool, was wir gern für unseren Adventskalender genutzt haben. :) Merry X-mas, everybody! Mehr Infos: https://research-software.cceh.uni-koeln.de/software/autodone

#textplus_advent #nfdi #wisskomm #textplus #digitalhumanities #scholarlyeditions #digitaleditions