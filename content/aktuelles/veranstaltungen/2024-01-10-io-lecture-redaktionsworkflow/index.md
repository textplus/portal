---
type: event
title: "8. IO-Lecture: Redaktionsworkflow Webportal"
start_date: 2024-01-10 10:00:00
end_date: 2024-01-10 12:00:00
all_day: false
location: virtuell
---
# 8. IO-Lecture: Redaktionsworkflow Webportal

Das Text+ Webportal wurde pünktlich zum Göttinger Plenary gelauncht und bietet mit seinem technischen Framework aus HUGO und GitLab sehr gute Voraussetzungen, es in verteilter Redaktionsarbeit mit Inhalten zu füllen und zu erweitern.

Basierend auf dem untenstehenden Redaktionsworkflow werden Maik Wegener (GWDG) und Alexander Steckel (SUB) eine Einführung geben und die Teilnehmerinnen und Teilnehmer in der Arbeit mit GitLab schulen. Dies wird der erste Schulungstermin sein, weitere werden folgen.

## Dokumentation
Die nach und nach entstehende Dokumentation ist ebenfalls im [GitLab](https://gitlab.gwdg.de/textplus/portal/-/wikis/Nutzer:innen-Dokumentation) zu finden.

## Zielgruppe der IO-Lecture
Die Veranstaltung richtet sich in erster Linie an Mitarbeitende von Text+, die Forschung innerhalb der Sozial- und Geisteswissenschaften betreiben und/oder in der Beratung tätig sind. Darüber hinaus freuen wir uns aber auch über Teilnehmende aus anderen NFDI-Konsortien. Im Zweifelsfalle kontaktieren Sie gerne das Text+ Operations Office.

## Anmeldung
Zur [Anmeldung bitte hier](https://events.gwdg.de/event/625/) entlang.