---
type: event
title: "Text+ Frühjahrstreffen"
start_date: 2024-03-12T12:00:00
end_date: 2024-03-13T14:00:00

location: Vortragssaal, Deutsche Nationalbibliothek, Frankfurt/Main 
featured_image: FrankfurtKongresszentrumSeitlich.jpg
---

Halbzeit – bei uns mit Pause

Am 12. und 13. März 2024 trifft sich das NFDI-Konsortium Text+ zu einem internen Meeting in Präsenz in Frankfurt. Zur Halbzeit der Förderphase – nach 2,5 Jahren – stehen thematische Arbeitstreffen in verschiedenen Konstellationen sowie der Task Area-interne Meetings im Fokus der Tagung. 

[Zur Anmeldung](https://events.gwdg.de/event/607/)