---
type: event
title: "Call for participation: DH2023 pre-conference workshop “Creating a DH workflow in the SSH Open Marketplace”"
start_date: 2023-07-11 09:00:00
end_date: 2023-07-11 12:30:00
all_day: false
location: online
featured_image: images/Marketplace_May-1024x482.jpg
aliases:
- /events/call-for-participation-dh2023-pre-conference-workshop-creating-a-dh-workflow-in-the-ssh-open-marketplace
---

**The workshop “Creating a DH workflow in the SSH Open Marketplace” aims at supporting researchers interested in creating a workflow in the SSH Open Marketplace, to share best practices methods with the community. Selected participants will be supported by the members of the Editorial Board of this discovery portal to write and document their research scenarios. Join us for this DH2023 pre-conference workshop!**

The [Social Sciences and Humanities Open Marketplace](https://marketplace.sshopencloud.eu/) (SSH Open Marketplace) is a discovery portal which pools and contextualises resources for Social Sciences and Humanities (SSH) research communities: tools, services, training materials, datasets, publications and workflows. A key asset of the SSH Open Marketplace is the contextualisation of the items in the catalogue, namely the relations created between the catalogues’ items to foster serendipity in the search. Resources can also be presented in [research workflows](https://marketplace.sshopencloud.eu/search?categories=workflow&order=label), where a step-by-step approach is followed and resources are associated to each stage of the work to describe a specific methodology.

This workshop aims at supporting researchers interested in creating a workflow in the SSH Open Marketplace in order to promote and enable the sharing of methodologies and best practices. Also called _research scenario_, a Marketplace workflow is defined as a “sequence of operations/steps performed on research data during their lifecycle. [Workflows](https://marketplace.sshopencloud.eu/about/data-population#workflows) can be achieved by using diverse tools and facilities, and useful resources are connected to each step.”

Three main reasons for creating a workflow are: 

- to reflect on the methodologies in a research project and contribute to fostering best practices in a given community.

- to get peer review, trigger feedback and gain visibility.

- to share methodical aspects of a project in another form than the usual blog or article (thus to offer a new way to disseminate research work).

This workshop is organised by four members of the SSH Open Marketplace Editorial Board – Laure Barbot, Klaus Illmayer, Barbara McGillivray and Alexander König – and will be held in Graz, as a [DH2023 pre-conference workshop](https://dh2023.adho.org/?page_id=616). Please be aware of the following registration indications from the DH Conference organisers: “All workshops are free of charge as part of the conference program. Please note that only registered participants of the DH2023 conference are eligible to participate in the workshops.”

**The SSH Open Marketplace workshop will take place on Tuesday July 11, from 9h to 12h30 CEST, at the University of Graz main campus.**

#### **Workshop call for participation**

The SSH Open Marketplace is designed so that the content can be collectively improved by the research community. This workshop is an opportunity for the DH conference participants to create (or improve existing) workflows of the SSH Open Marketplace.

The organisers are looking for 15 to 20 participants to attend this workshop. Participants are welcome to submit a group proposal to create common workflows but individual proposals are also possible. Workshop participation proposals have to include either a workflow topic (ideally not already covered by the existing workflows in the SSH Open Marketplace), or a list of resources based on which a workflow could be created.

The SSH Open Marketplace Editorial Board will act as the workshop’s Program Committee to review and select the proposals. **Three bursaries** to support travel costs (max. 500 euros each) to the DH Conference will be awarded as part of the selection process.

**If you are interested to receive one of the two remaining bursaries, please complete this form before – EXTENDED DEADLINE – the 24th of May: [https://forms.gle/MfBX3XNHt7oTLQ6GA](https://forms.gle/MfBX3XNHt7oTLQ6GA)**

Notifications of acceptance will be shared by the end of May. The form will remain open after the deadline for registration to the workshop.

#### **Workshop programme**

The SSH Open Marketplace workshop will take place on Tuesday July 11, from 9h to 12h30 CEST. The programme is as follows: 

- General introduction to the SSH Open Marketplace – 30 minutes 

- How to create a workflow in the SSH Open Marketplace? – 30 minutes 

- Hands-on: create your workflow – 1h30  

- Discussion and feedback – 30 minutes

At the end of the event, participants will not only have created a workflow but will also have a better understanding of the collective curation challenges and the institutional framework needed to run such a discovery service. 

During the first hour of the workshop, the introduction will set the scene of the service, explaining the rationale and intended audience, as well as involved stakeholders in terms of governance and maintenance. On the other hand it will explain the active usage of the catalogue, i.e. how to log in, and how to create metadata for individual entries in the SSH Open Marketplace, especially the workflows. This will allow participants to fully grasp the philosophy behind the workflows, what they are meant to do and how to conceive them.   

The central part of the workshop will consist of a hands-on session during which participants will be invited to create their own workflow in the SSH Open Marketplace. After refining the exact topic and perimeter of the workflow, participants will be supported by the workshop organisers to write, describe and document each step of the workflow, to associate the relevant metadata and identify the best resources to add. Finally, the last 30 minutes of the workshop will be kept to identify the most important questions to ask when creating a workflow, and possible limitations that participants experienced during their work. Although the aim is to finalise the participants’ workflows during the workshop, the Editorial Board members will be available to provide support afterwards if one or more participants need it.

More information: https://www.dariah.eu/2023/04/26/call-for-participation-dh2023-pre-conference-workshop-creating-a-dh-workflow-in-the-ssh-open-marketplace/