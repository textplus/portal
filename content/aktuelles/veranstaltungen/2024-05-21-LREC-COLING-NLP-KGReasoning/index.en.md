---
type: event
title: "Bridging Neurons and Symbols for Natural Language Processing and Knowledge Graphs Reasoning"
start_date: 2024-05-21
end_date: 2024-05-21
all_day: true
location: LREC-COLING 2024, Lingotto Conference Centre - Torino (Italia)

---
The workshop "Bridging Neurons and Symbols for Natural Language Processing and Knowledge Graphs Reasoning" is associated with LREC COLING 2024, in Torino (Italy) and Co-organized with partners from Text+. Recent exploration shows that LLMs may pass the Turing test in human-like chatting but have limited capability even for simple reasoning tasks. Human reasoning has been characterized as a dual-process phenomenon or as mechanisms of fast and slow thinking. These findings suggest two directions for exploring neural reasoning: starting from existing neural networks and starting from symbolic reasoning. These two directions will ideally meet somewhere in the middle and will lead to representations that can act as a bridge for novel neural computing and for novel symbolic computing. Hence the name of our workshop, with a focus on Natural Language Processing and Knowledge Graph reasoning. 

Further information can be obtained from the [Website of the Workshop Bridging Neurons and Symbols for Natural Language Processing and Knowledge Graphs Reasoning](https://lrec-coling-2024.org/workshop/bridging-neurons-and-symbols-for-natural-language-processing-and-knowledge-graphs-reasoning/).