---
type: event
title: "Bridging Neurons and Symbols for Natural Language Processing and Knowledge Graphs Reasoning"
start_date: 2024-05-21
end_date: 2024-05-21
all_day: true
location: LREC-COLING 2024, Lingotto Conference Centre - Torino (Italia)

---
Der Workshop "Bridging Neurons and Symbols for Natural Language Processing und Knowledge Graphs Reasoning" ist an die Konferenz LREC COLING 2024, angeschlossen, die in Turin (Italien) stattfdinden wird. Der Workshop wird gemeinsam mit Partnern von Text+ organisiert. Kürzlich durchgeführte Untersuchungen zeigen, dass LLMs den Turing-Test bei menschenähnlichen Chatten bestehen, aber selbst für einfache logische Aufgaben nur begrenzt geeignet sind. Menschliches Denken wurde als ein Phänomen mit zwei Prozessen charakterisiert oder als Mechanismen des schnellen und langsamen Denkens. Diese Ergebnisse legen zwei Richtungen nahe für die Erforschung des neuronalen Denkens: ausgehend von bestehenden neuronalen Netzen und ausgehend vom symbolischen Denken. Diese beiden Richtungen werden sich idealerweise irgendwo in der Mitte treffen und führen zu Darstellungen, die als Brücke für neuartige neuronale und für neuartige symbolische Berechnungen dienen können. Daher auch der Name des Workshops, mit dem Schwerpunkt auf natürlicher Sprachverarbeitung und Knowledge Graphen.

Weitere Informationen erhalten Sie auf der
[Webseite des Workshops Bridging Neurons and Symbols for Natural Language Processing and Knowledge Graphs Reasoning](https://lrec-coling-2024.org/workshop/bridging-neurons-and-symbols-for-natural-language-processing-and-knowledge-graphs-reasoning/), von der es auch einen Link zur Hauptkonferenz und zur Anmeldgemöglichkeit gibt.