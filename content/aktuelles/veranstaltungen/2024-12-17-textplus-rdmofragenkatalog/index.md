---
type: event
title: "Text+ RDMOFragenkatalog"
start_date: 2024-12-17T00:00:00+0100
end_date: 2024-12-17T23:59:59+0100
all_day: true
publishDate: 2024-12-17T02:00:00+0100
expiryDate: 2024-12-18T02:00:00+0100
featured_image: rdmofragenkatalog-adventskalender.png
location: Adventskalender
---

Basis einer gelungenen digitalen Edition ist ein guter Datenmanagementplan. Mit einer an Bedürfnisse von Text- und Sprachdaten angepassten [DMP-Vorlage](https://textplus.hypotheses.org/11685) auf [RDMO](https://rdmorganiser.github.io/)-Basis bietet Text+ ein nützliches FDM-Tool.

#textplus_advent #FDM #RDM #digitaleditions #scholarlyeditions