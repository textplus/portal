---
type: event
title: "IO-Lecture: RDMO in Text+"
start_date: 2024-10-24 10:00:00
end_date: 2024-10-24 11:00:00
all_day: false
location: virtuell
event_url: https://events.gwdg.de/event/914/
---

Diese IO-Lecture stellt die Ergebnisse des RDMO-Tasks aus IO vor. Konkret wird es um den Text+ Fragenkatalog für das Forschungsdatenmanagement gehen sowie gezeigt werden, wie sich der Katalog nutzen lässt.  Wir – die an Text+ beteiligten Institutionen – beraten Forschende von Beginn des Forschungsprozesses an bei allen Schritten der systematischen Organisation ihrer Forschungsdaten. Dabei stützen wir uns auf das Fachwissen unserer zertifizierten Datenrepositorien. Zur Orientierung in diesem Prozess und zur Dokumentation der Forschungsdaten in Form eines Datenmanagementplans bietet Text+ einen [Fragenkatalog](https://text-plus.org/themen-dokumentation/files/2024-05-29_Textplus_RDMO-Katalog_Formular_de.pdf) an, der sich eng am Standardkatalog des [Research Data Management Organiser (RDMO)](https://rdmorganiser.github.io/) orientiert. Als Grundlage für den Text+ Fragenkatalog dient der Fragenkatalog der Max Weber Stiftung "[MWS Ersterfassung](https://projects.academiccloud.de/api/v3/attachments/109481/content)" (Version 1). Der Text+ Fragenkatalog wurde in Zusammenarbeit mit Beteiligten des Text+ Konsortiums angepasst und mit disziplinspezifischen Beispielen erweitert. Hierbei handelt es sich um einen ersten Entwurf, der sukzessiv überarbeitet und erweitert wird und als nachnutzbarer XML-Katalog in die [eResearch Alliance der SUB Göttingen](https://www.eresearch.uni-goettingen.de/de/) eingebunden ist. 

Der Fragenkatalog ist eng mit unserem Beratungsangebot verknüpft. Forschende können eigenständig oder im Rahmen einer begleitenden Beratung durch den [Text+ Helpdesk](https://text-plus.org/helpdesk/) den Fragenkatalog ausfüllen und sich im direkten Austausch mit den Problemstellungen, die er potentiell eröffnet, auseinandersetzen. Der Fragenkatalog dient dabei der Selbstorganisation, soll aber auch für den Umgang mit Forschungsdaten sensibilisieren und die Arbeit mit ihnen erleichtern. Das von Text+ bereitgestellte Angebot richtet sich explizit an unsere Community, die sich schwerpunktmäßig mit sprach- und textbasierten Daten beschäftigt. Zu den Nutzungsszenarien zählen Antragsstellungen für Projekte, die Vorbereitung für die (langfristige) Speicherung von Forschungsdaten in entsprechenden Repositorien und die grundsätzliche Organisation von Forschungsdaten.

Vortragen werden Timo Henne (SUB Göttingen), Sandra König (Deutsche Akademie der Naturforscher Leopoldina e. V.). Melina L. Jander (SUB Göttingen) und Eva-Maria Gerstner (Max Weber Stiftung).

## Zielgruppe der IO-Lecture
Die Veranstaltung richtet sich alle Forschende und/oder Beratende der text- und sprachbasierten Wissenschaften. Darüber hinaus freuen wir uns auch über Teilnehmende aus anderen NFDI-Konsortien. Bei Fragen kontaktieren Sie gerne das Text+ Operations Office.
