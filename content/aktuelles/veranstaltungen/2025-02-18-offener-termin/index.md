---
type: event
title: "Text+ Community Plenary: Offener Termin des Programmkomitees"
start_date: 2025-02-18 14:00:00
end_date: 2025-02-18 16:00:00
all_day: false
location: virtuell
featured_image: DAE2025@SUB_Save-the-date.png
---
# Offener Termin des Programmkomitees des Text+ Community Plenarys

Das Programmkomitee für das Text+ Community Plenary 2025 führt sein Treffen am Dienstag, dem 18. Februar von 14 bis 16 Uhr, als offenen Termin durch. Damit gibt das Programmkomitee der Community die Möglichkeit, sich direkt in die Planung des Events einzubringen, Wünsche zu äußern oder auch Vorschläge zu Programmelementen zu machen. 

## Wie kann ich mitmachen?

Einfach am 18.02. um 14 Uhr in den [Zoomraum](https://uni-goettingen.zoom-x.de/j/68955894285?pwd=wATYjWfYeL7Msa5gKSzpkJlH0Cpc1r.1) kommen.
 
## Wie geht es danach weiter?

Je nach Uptake des offenen Termins wird das Programmkomitee auch noch einen zweiten offenen Termin anbieten. Für Fragen ist das Programmkomitee jederzeit auch unter der [verlinkten Mailadresse](textplusplenary25@sub.uni-goettingen.de) erreichbar.  

## Wer arbeitet im Programmkomitee mit?

* Philipp Wieder, GWDG (Vorsitz des Programmkomitees in seiner Rolle als Operations Speaker von Text+)
* Fernanda Alvares Freire, TU Darmstadt
* Jutta Bopp, IDS Mannheim
* Stefan Buddenbohm, SUB Göttingen
* Thomas Eckart, Sächsische Akademie der Wissenschaften zu Leipzig
* Barbara Fischer, Deutsche Nationalbibliothek
* Philippe Genet, Deutsche Nationalbibliothek
* Kilian Hensen, Universität Köln
* Marius Hug, Berlin-Brandenburgische Akademie der Wissenschaften
* Nanette Rißler-Pipka, DARIAH-EU/Max Weber Stiftung
* Alexander Steckel, SUB Göttingen
* Lukas Weimer, SUB Göttingen
* Antonina Werthmann, IDS Mannheim
