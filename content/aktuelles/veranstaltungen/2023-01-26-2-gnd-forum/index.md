---
title: "2. GND-Forum Text+"
start_date: 2023-01-26 13:00:00
end_date: 2023-01-26 17:00:00
all_day: false
location: virtuell via Zoom
featured_image: 
aliases:
- /events/2-gnd-forum-text
---


![](images/gnd_logo.png)Nach der Auftaktveranstaltung zum GND-Forum Text+ am 15. Juni 2022 setzen wir den Diskussionsprozess nun fort.

Der Austausch beim Auftaktforum sowie die [hier dokumentierten Ergebnisse](https://www.text-plus.org/events/knoten-knuepfen-sich-nicht-von-allein-gnd-forum-textplus/) bieten eine sehr gute Grundlage, die GND-Agentur Text+, den Arbeitsstand bzgl. Normdaten in den Text+ Datendomänen sowie ganz gezielt, ausgewählte Themen weiterzutreiben. Ein detailliertes Workshopprogramm folgt in den nächsten Tagen.

An wen richtet sich das 2. GND-Forum Text+? Zuerst an die Mitarbeitenden in den drei Datendomänen von Text+, also Editionen, Sammlungen und Lexikalische Ressourcen. Darüber hinaus dürfen sich aber auch Kolleg:innen, die mit textuellen Daten arbeiten, eingeladen fühlen. Vor allem möchten wir auch die anderen geistes- und kulturwissenschaftlichen NFDI-Konsortien NFDI4Culture, NFDI4Memory und NFDI4Objects einladen.

Anmeldung: https://events.gwdg.de/event/369/