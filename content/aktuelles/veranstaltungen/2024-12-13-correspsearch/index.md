---
type: event
title: "CorrespSearch"
start_date: 2024-12-13T00:00:00+0100
end_date: 2024-12-13T23:59:59+0100
all_day: true
publishDate: 2024-12-13T02:00:00+0100
expiryDate: 2024-12-14T02:00:00+0100
featured_image: correspsearch-adventskalender.png
location: Adventskalender
---

[CorrespSearch](https://correspsearch.net/de/start.html) ist ein Such- und Nachweissystem für Verzeichnisse digitaler und gedruckter Briefeditionen. Der Webservice funktioniert nach den Prinzipien des Open Access und arbeitet mit offenen Standards.

#textplus_advent #cmif #briefeditionen #apis