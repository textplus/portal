---
type: event
title: "Selbstzeugnisportal"
start_date: 2024-12-16T00:00:00+0100
end_date: 2024-12-16T23:59:59+0100
all_day: true
publishDate: 2024-12-16T02:00:00+0100
expiryDate: 2024-12-17T02:00:00+0100
featured_image: selbstzeugnis-adventskalender.png
location: Adventskalender
---

„Den 17ten December ist Mein Bruder … von Saldern gekommen.“ Das [Selbstzeugnisportal](http://selbstzeugnisse.hab.de/) vereinigt Editionen & bietet Zugriff auf [Forschungsdaten](http://selbstzeugnisse.hab.de/dienste) (u.a. RDF).

#textplus_advent #modeledition #digitaleditions (https://openbiblio.social/@hab_wf)