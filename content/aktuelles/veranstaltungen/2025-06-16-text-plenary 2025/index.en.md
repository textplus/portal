---
type: event
title: "Text+ Plenary on June 16th and 17th, 2025"
start_date: 2025-06-16
end_date: 2025-06-17
all_day: true
location: Göttingen
featured_image: Text+Plenary@SUB_Save-the-date.png
---
On June 16-17, 2025, the 4th Text+ Plenary will take place at [Göttingen State and University Library](https://www.sub.uni-goettingen.de/en/news/). The consortium, the community, representatives of other NFDI consortia, and interested parties are invited to attend. In lectures, panel discussions, and working group sessions, participants will discuss the current state of the consortium and possibilities for further development.

Participation in all sessions of the Plenary is free of charge. 


As soon as more information about the Text+ Plenary 2025 are available, they will be displayed here ...