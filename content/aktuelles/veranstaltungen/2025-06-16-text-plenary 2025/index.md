---
type: event
title: "Text+ Plenary am 16. und 17. Juni 2025"
start_date: 2025-06-16
end_date: 2025-06-17
all_day: true
location: Göttingen
featured_image: Text+Plenary@SUB_Save-the-date.png
---
Am 16. und 17. Juni 2025 findet das 4. Text+ Plenum in der [Niedersächsischen Staats- und Universitätsbibliothek Göttingen](https://www.sub.uni-goettingen.de/en/news/) statt. Eingeladen sind das Konsortium, die Community, Vertreterinnen und Vertreter anderer NFDI-Konsortien sowie Interessierte. In Vorträgen, Podiumsdiskussionen und Arbeitsgruppensitzungen diskutieren die Teilnehmenden den Stand des Konsortiums und die Möglichkeiten der weiteren Entwicklung.

Die Teilnahme an allen Sitzungen des Plenary ist kostenlos.

Sobald weitere Informationen über das Text+ Plenary 2025 verfügbar sind, werden diese hier angezeigt ...