---
title: "Text+ Plenary 2022"
start_date: 2022-09-12
end_date: 2022-09-13
location: Schloss Mannheim
all_day: true
type: event
featured_image: Banner-Website-Plenary-2022.jpg

aliases:
- /aktuelles/plenary-2022-2
- /events/text-plenary-2022
---

Text+ Plenary 2022