---
type: event
title: "Schleiermacher Digital"
start_date: 2024-12-15T00:00:00+0100
end_date: 2024-12-15T23:59:59+0100
all_day: true
publishDate: 2024-12-15T02:00:00+0100
expiryDate: 2024-12-16T02:00:00+0100
featured_image: schleiermacher-adventskalender.png
location: Adventskalender
---

[Schleiermacher digital](https://schleiermacher-digital.de/index.xql) stellt Personen- und Korrespondenzdaten über Schnittstellen wie [CMIF und BEACON](https://schleiermacher-digital.de/about/index.xql?id=S0010265#section3) zur Verfügung und ermöglicht damit die Nachnutzung von Forschungsdaten.

#textplus_advent #digitaleditions #apis