---
title: Veranstaltungen
type: event
hide_in_list: true

aliases:
- /aktuelles-veranstaltungen/veranstaltungen/

menu:
  main:
    weight: 20
    parent: aktuelles
---

# Veranstaltungen

{{<lead-text>}}
Veranstaltungen von sowie rund um Text+. Wenn Sie eine interessante Veranstaltung haben, die hier erscheinen sollte, reichen Sie sie gerne an uns zur Veröffentlichung hier ein.
{{</lead-text>}}
{{<button url="/aktuelles/veranstaltungen/#veranstaltung-eintragen">}}Veranstaltung einreichen{{</button>}}

## Zukünftige Veranstaltungen
{{<upcoming-events-list>}}
{{<horizontal-line>}}

## Veranstaltung eintragen

Sie haben eine Veranstaltung, die hier erscheinen soll und einen Bezug zu Text+ hat? Gerne können Sie das bereitgestellte Formular ausfüllen und an uns senden. Wir setzen uns ggf. mit Ihnen in Verbindung, um weitere Informationen einzuholen. Falls das Formular vollständig ausgefüllt wurde und keine Fragen offen sind, erscheint die Veranstaltung ohne weiteren Handlungsbedarf Ihrerseits hier auf dieser Seite. Bitte geben Sie uns bis zu drei Tage Zeit für das Onlinestellen der Veranstaltungsankündigung. Wir informieren Sie über die neu angelegte Veranstaltungsseite.

Sollten Sie keine Bestätigung zum Eingang Ihrer Veranstaltungsankündigung erhalten, schreiben Sie bitte eine E-Mail an: textplus-support[at]gwdg.de

### Selbst eine Veranstaltung anlegen

Text+ Mitarbeitende haben die Möglichkeit, ihre Veranstaltungen selbst anzulegen. Dafür findet sich im [Wiki für den Seitentyp Event eine kleine Anleitung](https://gitlab.gwdg.de/textplus/portal/-/wikis/Nutzer:innen-Dokumentation/Seitentypen#event). Bei Fragen hilft der Support weiter.

{{<event-form>}}
{{<horizontal-line>}}

## Vergangene Veranstaltungen
<!-- the past events are handled in the list template which is needed for the pagination to work -->
<!-- see layouts/event/list.html -->