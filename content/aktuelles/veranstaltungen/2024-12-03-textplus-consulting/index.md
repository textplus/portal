---
type: event
title: "Text+ Consulting"
start_date: 2024-12-03T00:00:00+0100
end_date: 2024-12-03T23:59:59+0100
all_day: true
publishDate: 2024-12-03T00:02:00+0100
expiryDate: 2024-12-04T00:02:00+0100
featured_image: consulting-adventskalender.png
location: Adventskalender
---

Das [Text+ Consulting](https://text-plus.org/daten-dienste/consulting/) leistet eine vollumfängliche Beratung zu allen Fragen rund um text-/sprachbasierte Forschungsdaten im gesamten Datenlebenszyklus, wie Antragsberatung, Datenmanagement, Forschungsprozesse oder Technologien.

#textplus_advent #nfdi #digitaleditions #scholarlyeditions #nfdirocks 