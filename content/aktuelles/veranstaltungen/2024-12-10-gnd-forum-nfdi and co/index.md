---
type: event
title: "GND Forum NFDI, FID & Co. Normdaten in der Forschung"
start_date: 2024-12-10 09:45:00
end_date: 2024-12-10 16:00:00
all_day: false
location: virtuell
event_url: https://gnd.network/Webs/gnd/SharedDocs/Veranstaltungen/DE/GndForumNfdiFidCo/20241210_gndForumNfdiFidCo_node.html
summary: "Ein GND Forum das vornehmlich der Information der unterschiedlichen Communities über Projekte, Produkte und Workflows rund um die GND im Umfeld der NFDI (NFDI4Culture, Text+, NFDI4Memory, NFDI4Objects) und weiteren Initiativen regionaler und thematischer Natur dient."
featured_image: gnd-logo-small.png
---
# Wie Forschende von der Gemeinsamen Normdatei Gebrauch machen. Neun Berichte aus der Praxis

In den vergangenen Monaten haben wir uns schon mehrfach im kleinen Kreis zum Arbeiten  getroffen und uns gegenseitig von unseren Vorhaben und Plänen erzählt. Jetzt ist es Zeit, die Ergebnisse in einem größeren Publikum zu präsentieren und zu diskutieren. Auf dem GND Forum NFDI, FID & Co erwarten Sie insgesamt neun Vorträge aus dem wachsenden Netzwerk der Gemeinsamen Normdatei (GND). Allen gemeinsam ist, sie wollen mit zuverlässigen Normdaten ganz unterschiedliche Ressourcen verknüpfen und dabei vor allem die Fehlermeldung 404 mittels dauerhafter und gepflegter URIs vermeiden. Sie nutzen deshalb das Persistent Identifier (PID) System GND.

Am Vormittag können sich alle Gäste einen Überblick verschaffen, wie in einzelnen Bereichen der Forschung die Arbeit schon heute mit der GND aktiv unterstützt wird. Es geht sowohl um Beratung als auch um konkrete Normdaten-Edition. Wir beginnen mit einem Besuch der GND-Services im FID-Netzwerk Künste & Kultur. Die FID-Koryphäen Maria Effinger, Elisabeth Hufnagel und Franziska Voss laden zu einem Schulterblick ein. Weiter geht es mit der GND in den Bereich der Orientalistik. Anett Krause und Christoph Rauch stellen die Normdaten im Qalamos-Portal vor. Alexander Faschon und Desirée Mayer berichten über ihre Arbeit unterschiedliche PID-Systeme, nämlich das Répertoire International des Sources Musicales (RISM) und Werkdatensätze in der GND, aufeinander abzustimmen. Den Vormittag beschließt Barbara Fischer mit einer Kurzpräsentation zweier E-Learnings, die künftig Einführungen einmal in die GND-Nutzung und zum anderen in die Mitarbeit in der GND zum Selbststudium bieten werden.

Am Nachmittag berichtet uns Anja Gerber über den Umgang mit Normdaten im Rahmen von NFDI4Objects. Sophie Rölle und Domenic Städtler beleuchten die Rolle der GND im Minimaldatensatz zu Objekten des kulturellen Erbes. Im Anschluss leiten wir dann über auf Werkstattberichte über die Nutzung von OpenRefine in der Kombination mit EntityXML in einem Forschungsprojekt im Rahmen von NFDI4Culture. Hier präsentieren Christine Erfurth, Johannes Haslauer und Uwe Sikora ihre Erfahrungen. Der zweite Werkstattbericht kommt von Hanna Heiners. Sie arbeitet ebenfalls mit OpenRefine aber ihr Fokus liegt auf der Qualitätssicherung und Anreicherung der Sammlungsdaten mit GND.

Kaum eine Konferenz mag heute auf das Buzz-Thema "Künstliche Intelligenz" verzichten. Auf dem GND-Forum stellt Cecilia Maas daher vor, wie durch den klugen Einsatz von KI basierter Methoden, Verschlagwortung von textbasierten Ressourcen automatisiert werden kann.

Die Leitung des Treffens übernehmen Barbara Fischer (DNB), Andrea Hemmer (DNB) sowie Stefan Buddenbohm (SUB). Wir freuen uns auf den Austausch mit Euch/Ihnen!

## Programm
09:45 Uhr: Begrüßung

10:00 Uhr: Ansichten auf die GND-Arbeit:
- Maria Effinger, Elisabeth Hufnagel und Franziska Voß: Die GND-Services im FID-Netzwerk Künste & Kultur 
- Anett Krause und Christoph Rauch: Normdaten-Entitäten für orientalische Handschriften: Gegenwärtige Praxis, Probleme und Perspektiven im Qalamos-Portal
- Alexander Faschon: Musikalische Werknormdaten und Quellen
- Barbara Fischer: Arbeiten mit der GND - Zwei einführende Kurse zum Selbststudium

12:00 Uhr: Mittagspause

13:00 Uhr: Ansichten auf die GND-Arbeit
- Gerber, Anja: Forschungsdaten in NFDI4Objects
- Sophie Rölle und Domenic Städtler: Die Minimaldatensatz-Empfehlung für Museen und Sammlungen
- Christine Erfurth, Johannes Haslauer und Uwe Sikora: OpenRefine meets entityXML and Marc21
- Hanna Meiners: Strategien zur Datenanreicherung und Qualitätssicherung unter Einsatz von Normdaten und OpenRefine

15:00 Uhr: Pause

15:10 Uhr: Cecilia Maas: KI-gestützte Erschließung mit der GND

15:30 Uhr: Resümee und Ausblick

16:00 Uhr: Veranstaltungsende

## Anmeldung und weitere Informationen
Zur [Anmeldung und für weitere Informationen bitte hier](https://gnd.network/Webs/gnd/SharedDocs/Veranstaltungen/DE/GndForumNfdiFidCo/20241210_gndForumNfdiFidCo_node.html) entlang. Den Link zur Veranstaltung verschicken wir wenige Tage vor Beginn. Falls wir über 300 Teilnehmende sein werden als Zoom-Webinar, für bis zu 300 Teilnehmende als normale Zoom-Konferenz.

[Hier der Link zum Blogpost im GND-Wiki, geschrieben von Barbara Fischer.](https://gnd.network/Webs/gnd/SharedDocs/Veranstaltungen/DE/GndForumNfdiFidCo/20241210_gndForumNfdiFidCo_node.html)