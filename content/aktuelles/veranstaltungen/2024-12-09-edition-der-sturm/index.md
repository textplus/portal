---
type: event
title: "DER STURM"
start_date: 2024-12-09T00:00:00+0100
end_date: 2024-12-09T23:59:59+0100
all_day: true
publishDate: 2024-12-09T02:00:00+0100
expiryDate: 2024-12-10T02:00:00+0100
featured_image: der-sturm-adventskalender.png
location: Adventskalender
---

Die Edition [„DER STURM“](https://sturm-edition.de) bietet spannende Informationen zur Geschichte der int. Avantgarde und zeigt, welchen Mehrwert dokumentierte Schnittstellen, persistente Identifikatoren sowie offene Standards haben. 

#textplus_advent #digitalhumanities #modeledition