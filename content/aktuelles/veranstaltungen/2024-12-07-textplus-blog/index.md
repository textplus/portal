---
type: event
title: "Text+ Blog"
start_date: 2024-12-07T00:00:00+0100
end_date: 2024-12-07T23:59:59+0100
all_day: true
publishDate: 2024-12-07T02:00:00+0100
expiryDate: 2024-12-08T02:00:00+0100
featured_image: textplusblog-adventskalender.png
location: Adventskalender
---

Das Text+ Blog bietet Themen rund um text- und sprachbasierte Forschungsdaten und gibt Einblicke in das Konsortium, z.B. mit der [Partner Parade](https://textplus.hypotheses.org/tag/partner-parade) oder dem [Ressourcen-Reigen](https://textplus.hypotheses.org/tag/ressourcen-reigen).

#textplus_advent #digitalhumanities #Forschungsdaten #academia #researchdata #TextplusBlog