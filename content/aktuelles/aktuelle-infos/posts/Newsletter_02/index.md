---
title: "Text+ Newsletter #2"
featured_image: Newsletter_v1.png
type: news
date: 2024-07-12
---

{{<image img="Newsletter_v1.png" alt="Newsletter"/>}}


## Herzlich willkommen!
Wir freuen uns, Euch und Ihnen den zweiten Text+ Newsletter präsentieren zu dürfen. Wir laden Sie herzlich ein, einen Blick in unser Projektgeschehen zu werfen. Über Feedback, Fragen oder Wünsche freuen wir uns. Diese erreichen uns am einfachsten über das [Office](<office@text-plus.org>).

## Text+ Plenary am 10. und 11. Oktober 2024
Am 10. und 11. Oktober 2024 findet im Schloss Mannheim das 3. Text+ Plenary statt. Eingeladen sind das Konsortium, die Community, Vertreterinnen und Vertreter anderer NFDI-Konsortien sowie Interessierte. Das diesjährige Plenary steht unter dem Thema „Große Sprachmodelle (LLMs) und deren Nutzung“. In Vorträgen, Podiumsdiskussionen und Arbeitsgruppensitzungen diskutieren die Teilnehmenden, wie sie diese Technologien in ihrer eigenen Arbeit nutzen und wie sie mit ihren Daten und Angeboten zur weiteren Entwicklung beitragen können.

Für diejenigen, die sich bisher nicht mit den Hintergründen von LLMs beschäftigt haben, gibt es die Möglichkeit, am Nachmittag vor dem ersten Veranstaltungstag, am Mittwoch, dem 9. Oktober von 16 bis 18 Uhr, an einem Pre-Conference-Tutorial zum Thema Large Language Models (LLMs) teilzunehmen. Das Tutorial wird Grundlagen und Hintergründe zu Sprachmodellen vorstellen und eine Einführung in das Thema bieten. Es findet im Großen Vortragssaal des Leibniz-Instituts für Deutsche Sprache statt.

Die Teilnahme an allen Angeboten des 3. Text+ Plenary 2024 ist kostenfrei. Weitere Informationen und die Möglichkeit zur Anmeldung finden Sie auf der [Webseite des 3. Text+ Plenary](https://text-plus.org/aktuelles/veranstaltungen/2024-10-10-text-plenary/). 

## Highlights aus dem Blog
In dieser Rubrik stellen wir interessante Beiträge aus dem Text+ Blog vor. Das Blog informiert rundum Text+ und stellt in Ergänzung zur Webseite auch Work in Progress vor oder erlaubt einen detaillierteren Blick auf individuelle Themen. Alle Beiträge sind mit DOIs versehen und zitierbar.

Beiträge von Gastautorinnen und -autoren zu Themen von Interesse für die Text+ Community sind herzlich willkommen! [Schreiben Sie uns, wenn Sie ein Thema haben.](https://textplus.hypotheses.org/uber-uns) 

### Mapping Online Book Reception Across Cultures and Languages (25-27 January 2024), ZiF-Workshop (Bielefeld)

{{<image img="zif_01-1536x1148.png">}}
   Die Teilnehmerinnen und Teilnehmer des Workshops
{{</image>}}

Mit einer (zumindest teilweisen) Verlagerung kultureller Diskurse ins Internet, stehen zunehmend Daten zur Buchrezeption in großer Menge und von Autorinnen und Autoren aus aller Welt zur Verfügung. Dieser Datenbestand bietet eine völlig neue Perspektive auf Fragen zur Leseerfahrung, zur literarischen Qualität und sogar zur Weltliteratur als solcher. Daher ist es wichtig, neue theoretische Modelle zu entwickeln sowie bewährte Verfahren in Bezug auf ausgewogene Ansätze zum Aufbau von Datensätzen, Datenschutz und Analysemethoden zu nutzen und zu bewerten, um der Online-Buchrezeption als globalem und multidimensionalem Phänomen Rechnung zu tragen.

Um diese Herausforderung anzugehen, versammelten sich 23 Forschende aus Europa, Nordamerika und Asien zum Workshop „Mapping Online Book Reception Across Cultures and Languages“ am Institut für Interdisziplinäre Forschung der Universität Bielefeld. Im Mittelpunkt des Workshops stand die Idee, Online-Buchrezensionen als kulturelle Praxis von nicht-professionellen Lesern im digitalen Bereich mit einem vielfältigen und interdisziplinären Methodenset zu untersuchen.

Der englischsprachige Blogbeitrag von Tina Ternes (Universität Basel) und Xenia Bojarski (Universität Zürich) ist im [Text+ Blog zu finden.](https://textplus.hypotheses.org/10418)

Diesen Blogbeitrag zitieren:
_Tina Ternes/Xenia Bojarski (2024, 13. Mai). Mapping Online Book Reception Across Cultures and Languages (25-27 January 2024), ZiF-Workshop (Bielefeld). Text+ Blog. Abgerufen am 17. Juni 2024, von https://doi.org/10.58079/11nxm_

### Fluffy Workflow: Neue Tools für den Datenimport ins TextGridRep
Frei nach John Cotton Dana mit dem ihm zugeschriebenen Zitat "Preservation is Use", bedeutet Langzeitarchivierung von Forschungsdaten, dass die Nachnutzung von Anfang an ein anleitendes Prinzip für die technische Infrastruktur sein muss. Wie kann eine Forschungsdateninfrastruktur diesem Prinzip Rechnung tragen? Wie müssen die Workflows für den Dateningest beschaffen sein? Wie müssen Forschungsdaten aufbereitet und präsentiert werden, damit sie zur Nachnutzung einladen? Diese Fragen versucht das Team hinter dem neuen Importworkflow für das TextGrid Repository (TextGridRep) zu begegnen.

Der [Beitrag](https://textplus.hypotheses.org/10328) berichtet über den zweiten Text+ Code Sprint for Humanities Data, der am 11. und 12. April an der SUB Göttingen stattfand. Organisiert von den Text+ Partnern SUB Göttingen, GWDG und der TU Dresden, zielte der Code Sprint darauf ab, Forschenden neue Tools für den Import und die Veröffentlichung eigener Daten im TextGridRep vorzustellen.

Das TextGridRep, ein wichtiges Repositorium für geisteswissenschaftliche Forschungsdaten, bietet eine nachhaltige, dauerhafte und sichere Publikation von Forschungsdaten, unterstützt durch detaillierte Metadaten. Während des Code Sprints wurden verschiedene Tools vorgestellt, darunter:
* [tg-model](https://marketplace.sshopencloud.eu/tool-or-service/dqBWGO): Ein Tool zur Generierung von TextGrid-Metadatendateien aus TEI-Daten
* [tgadmin](https://marketplace.sshopencloud.eu/tool-or-service/R7MhwZ) und [tgclients](https://marketplace.sshopencloud.eu/tool-or-service/mld5Py): Python-Tools für den Datenimport ins TextGridRep
* [TextGrid Import UI](https://marketplace.sshopencloud.eu/tool-or-service/um6OHd): Eine Jupyter Notebook basierte Web-Anwendung zur komfortablen Bedienung des Workflows

Die Tools wurden auf dem Text+ [JupyterHub](https://marketplace.sshopencloud.eu/tool-or-service/ghLs9d) bereitgestellt, sodass sie direkt im Browser verwendet werden können.

{{<image img="codesprint.jpg">}}
   Teilnehmende des Codesprints
{{</image>}}

Der neue Import-Workflow überwindet die bisherigen Hindernisse und ermöglicht einen komfortablen Datenimport ohne das TextGridLab. Die Teilnehmenden des Code Sprints sammelten wertvolle Erfahrungen und gaben Feedback zur Weiterentwicklung der Tools. Ein weiterer Code Sprint ist noch in diesem Jahr geplant.

Diesen Blogbeitrag zitieren:
_Florian Barth, Stefan Buddenbohm, José Calvo Tello, George Dogaru (GWDG), Stefan E. Funk, Mathias Göbel, Ralf Klammer (TU Dresden), Ubbo Veentjer (alle Autoren ohne Affiliationsangabe: SUB Göttingen) (2024, 8. Mai). Fluffy Workflow: Neue Tools für den Datenimport ins TextGridRep. Text+ Blog. Abgerufen am 17. Juni 2024, von https://doi.org/10.58079/11nmp_

## Werkstattberichte

### Text+ Forum Digitalisierung – Diskussionsforum zur Weiterentwicklung der DFG-Praxisregeln „Digitalisierung“
FAIRe Forschungsdaten fallen nicht vom Himmel. Insbesondere im Kontext der Retrodigitalisierung von Werken sind die Kriterien zur Sicherstellung von FAIRness in der Bereitstellung von Forschungsdaten sehr aufwändig. Weil es alles andere als einfach ist, sämtliche Anforderungen für das jeweilige Vorhaben im Blick zu haben, besteht eine zentrale Aufgabe der NFDI und Text+ darin, zum Thema Forschungsdaten zu beraten. Ein wichtiger Leitfaden stellt dabei seit vielen Jahren die sogenannten Praxisregeln „Digitalisierung“ der Deutschen Forschungsgemeinschaft (DFG) dar. Allerdings ist es den dynamischen Entwicklungen der digitalen Transformation geschuldet, dass das zuletzt 2022 aktualisierte Dokument immer auch einen unabgeschlossenen Charakter hat. Diese Blogpost-Reihe verfolgt das Ziel, materialspezifische Desiderata der Richtlinien aufzugreifen und einen Prozess anzustoßen, in dem entsprechende Empfehlungen ausgearbeitet werden. 

Im ersten Teil ging es um die Kontextualisierung des Themas „Richtlinien zur Digitalisierung“. 
[Weiterlesen.](https://textplus.hypotheses.org/10408)

Der zweite Beitrag widmet sich den audiovisuellen (AV) Medien und konkrete Formatempfehlungen. 
[Weiterlesen.](https://textplus.hypotheses.org/10732)

Die DFG hat bewusst die Expertinnen und Experten aus den jeweiligen Communities dazu eingeladen, an der weiteren Gestaltung der Praxisregeln „Digitalisierung“ mitzuwirken. Text+ möchte selbst einen Beitrag leisten und lädt zur Mitwirkung ein. Wir freuen uns daher auf Ihre Kommentare, Vorschläge für weitere materialspezifische Empfehlungen etc. – gerne direkt über die Kommentarfunktion des Text+ Blogs oder über unseren [Helpdesk](https://text-plus.org/helpdesk/).

### RDMO-Angebot
Text+ bietet zur Unterstützung bei der Erstellung von Datenmanagementplänen einen eigenen [Fragenkatalog](https://text-plus.org/themen-dokumentation/files/2024-05-29_Textplus_RDMO-Katalog_Formular_de.pdf) an, der sich an den Standardkatalog der [RDMO-Community](https://rdmorganiser.github.io/) anlehnt und zukünftig sukzessive überarbeitet und erweitert wird. 
Der Fragenkatalog unterstützt Forschende bei ihrer Selbstorganisation bzgl. des Forschungsdatenmanagements und sensibilisiert für einen nachhaltigen und FAIRen Umgang mit Forschungsdaten. Der Katalog ist auch in die RDMO-Instanz [GRO.plan](https://plan.goettingen-research-online.de/) der eResearch Alliance der SUB Göttingen integriert und mit den Beratungsleistungen von Text+ verknüpft. Forschende können die Fragen eigenständig oder im Rahmen einer begleitenden Beratung durch den [Text+ Helpdesk](https://text-plus.org/helpdesk/) beantworten und möglicherweise auftretende Fragen gemeinsam mit einem Helpdesk-Agenten bearbeiten. 
{{<image img="screenshot-RDMO-Fragenkatalog.png">}}
   RDMO-Fragenkatalog für Text+
{{</image>}}

### 1. Gemeinsames Treffen der Text+ Coordination Committees am 3.6.24 
Am 3. Juni 2024 fand das erste gemeinsame Treffen der Text+ Coordination Committees statt. Vor dem Hintergrund der Abgabe des Zwischenberichts und des Beginns der Folgeantragsphase bot dieses Treffen eine wichtige Gelegenheit, den aktuellen Stand des Projekts kritisch zu betrachten. Das übergeordnete Thema des Treffens waren die von Text+ bereitgestellten Angebote zum Forschungsdatenmanagement. Diese wurden von Philipp Wieder (Operations Speaker/Lead TA IO/OCC) und Andreas Witt (Sprecher des NFDI-Konsortiums Text+ und ab Oktober 2024 Scientific Speaker Text+) vorgestellt.

In der anschließenden gemeinsamen Diskussion brachten die Chairs der einzelnen Coordination Committees ihre Erwartungen und ihr Feedback zum aktuellen Stand des Text+ Portals ein. Zudem berichteten sie über die aktuellen Entwicklungen in ihren Communities und die daraus resultierenden Anforderungen für die Weiterentwicklung von Text+. Basierend auf den erhaltenen Rückmeldungen und der wertvollen Diskussion wird der Handlungsbedarf für das Projekt sorgfältig analysiert. Daraufhin werden gezielte Lösungsvorschläge entwickelt, um die identifizierten Herausforderungen effektiv anzugehen und die Weiterentwicklung des Projekts voranzutreiben.

Der Vorschlag, dieses Format zukünftig zweimal jährlich – einmal virtuell und einmal in Präsenz im Rahmen des Plenarys – durchzuführen, stieß auf breite Zustimmung. Wir danken allen Beteiligten des ersten gemeinsamen Treffens der Text+ Coordination Committees für ihre rege Teilnahme und ihr wertvolles Feedback. Ein besonderer Dank gilt auch den Mitgliedern, die ihr Feedback im Vorfeld des Treffens schriftlich abgegeben haben.

## Publikationen, Services & Infoangebote

Text+ pflegt seine Bibliographie bei [Zotero](https://www.zotero.org/groups/4533881/textplus/library) und stellt auf seinem [Portal eine strukturierte Ansicht](https://text-plus.org/themen-dokumentation/bibliographie/) dar.

### Dokumentation der Editions Registry
Mit „Towards a Registry for Digital Resources – The Text+ Registry for Editions“ in der Zeitschrift "Datenbank-Spektrum. Zeitschrift für Datenbanktechnologien und Information Retrieval" veröffentlicht Text+ die offizielle Dokumentation des derzeit im Aufbau befindlichen Verzeichnisses für Editionen. 

* _Gradl, T., Kudella, C., Lordick, H., Schulz, D. Towards a Registry for Digital Resources – The Text+ Registry for Editions. Datenbank Spektrum (2024). https://doi.org/10.1007/s13222-024-00479-0._

Das Editionsverzeichnis ist Teil der Text+ Registry, die als zentrales System zur Beschreibung und Katalogisierung verschiedener Arten von Ressourcen fungiert. Für den Bereich der Editionen verfolgt die [Task Area Editions](/ueber-uns/arbeitsbereiche/editionen/) in Text+ dabei einen offenen Ansatz, um möglichst sämtliche digitalen, hybriden und gedruckten Editionen erfassen zu können. Am Beispiel von Editionen und Editionsprojekten werden in der Publikation Desiderate und Herausforderungen in Bezug auf deren Auffindbarkeit skizziert und das Datenmodell beschrieben, das der großen Bandbreite unterschiedlicher Editionstypen gerecht wird. Mit ihrem übergreifenden und flexiblen Ansatz ist die Text+ Registry eine vielseitige und flexible technische Komponente, die für zukünftige Erweiterungen und breite Konnektivität ausgelegt ist. 

### Vom Suchen und Finden – Einführung in die Recherche mit der Gemeinsamen Normdatei 

Die Publikation “Vom Suchen und Finden – Einführung in die Recherche mit der Gemeinsamen Normdatei” von M. Strickert und B. Fischer ist erschienen. 

* _Strickert, M.; Fischer, B. (2024). Vom Suchen und Finden: Einführung in die Recherche mit der Gemeinsamen Normdatei. Leipzig/Frankfurt, M: Deutsche Nationalbibliothek (https://d-nb.info/1325174785)._

Sie erklärt, wie Normdaten die Suche nach Literatur und Daten verbessern. In einer leicht zugänglichen Einführung wird beschrieben, was Normdaten sind und welche Rolle sie bei der Beschreibung von Publikationen, Forschungsdaten und Sammlungsgut spielen. Im Mittelpunkt stehen die Normdaten der Gemeinsamen Normdatei (GND), das zentrale kontrollierte Vokabular für Kultur und Forschung im deutschsprachigen Raum. Anhand verschiedener Beispiele wird gezeigt, wie man in der GND passende Suchbegriffe findet und wie Normdaten die Auffindbarkeit eigener Texte und Daten erhöhen. Die [Publikation ist online](https://d-nb.info/1325174785/34) verfügbar.


## Events & Nachberichte

### BiblioCon
Vier Tage, bis zu 26 parallele Veranstaltungen und fast 5.000 Teilnehmende: Das war die 112. BiblioCon, die vom 4. bis 7. Juni 2024 im Congress Center Hamburg stattfand. Das vielfältige Programm bot auch für Forschungs-, Universitäts- und Spezialbibliotheken interessante Impulse. Ein eigener Themenkreis drehte sich um “Forschungsnahe Dienste und Open Science” – ein Forum, das auch viele an Text+ Beteiligte nutzten, um noch mehr Bibliotheken für die Themen und Fragestellungen der sprach- und textbasierten Forschungsdaten zu begeistern. [Dieser Beitrag fasst die Beiträge aus Text+ zum BiblioCon-Programm zusammen.](https://textplus.hypotheses.org/10684)

{{<image img="philippe_jose.jpg">}}
   Philippe Genet, DNB und José Calvo Tello, SUB Göttingen, vor dem Poster "Welche Metadaten braucht die Literaturwissenschaft?"
{{</image>}}

* Normdaten als gemeinsame Aufgabe von FID und NFDI (Susanne Al-Eryani, SUB Göttingen & Volker Adam, Universitäts- und Landesbibliothek Sachsen-Anhalt, Halle)
* Welche Metadaten braucht die Literaturwissenschaft? (José Calvo-Tello, SUB Göttingen)
* Frameworks und digitale Editionen: Open Source Tools für grundständige Editionen (Kevin Wunsch & Kevin Kuck, beide ULB Darmstadt)
* Die Gemeinsame Normdatei (GND) (Barbara Fischer, DNB)
* Sammlungen vernetzen und für die Forschung nutzbar machen – eine Aufgabe (auch) für Bibliotheken (Philippe Genet & Peter Leinen, beide DNB)

### Edit-a-thon zur Beschreibung von Ressourcen im SSH Open Marketplace
Am 24. Juni fand auf Einladung des Vereins für Geistes- und Kulturwissenschaftliche Forschungsinfrastrukturen e.V. und Text+ ein Edit-a-thon zur Beschreibung von Ressourcen im SSH Open Marketplace statt. Herzlichen Dank an die Max Weber Stiftung, die uns in ihren Räumlichkeiten in Bonn-Bad Godesberg tagen ließ und den passenden Rahmen für ein produktives Treffen bot.

{{<image img="editathon.jpg">}}
   (Fast alle) Teilnehmerinnen und Teilnehmer des Edit-a-thons
{{</image>}}

Ziel der Veranstaltung war es, Ressourcen für die geistes- und kulturwissenschaftliche Community, bspw. Dienste, Tutorials, Software oder ganze Workflows, im SSH Open Marketplace entweder neu anzulegen oder bestehende Beschreibungen zu kuratieren. Die Teilnehmenden des Edit-a-thons rekrutierten sich aus Text+, dem GKFI, NFDI4Culture sowie NFDI4Memory.

Mit dem Verein Geistes- und kulturwissenschaftliche Forschungsinfrastrukturen und Text+ gibt es zwei Anwendungsfälle, die den SSH Open Marketplace für die Darstellung der eigenen Angebote nutzen. Anstelle von (ggf. proprietären) Eigenentwicklungen setzen GKFI und Text+ auf den Marketplace als eine bestehende Lösung:
* die verfügbar und auch mindestens mittelfristig verfügbar bleiben wird,
* intuitiv manuell bedienbar ist,
* über Harvesting gut in andere Infrastrukturen einbindbar ist, sowie
* mit einem aktiven Editorial Board als Scharnier zur Community ausgestattet ist.

Der Edit-a-thon wurde von Michael Kurzmeier (Austrian Centre for Digital Humanities and Cultural Heritage) mit einer Vorstellung des SSH Open Marketplaces eingeleitet. Michael ist Mitglied des Editorial Board des SSH Open Marketplace und gab den Teilnehmen Einblicke in die Struktur des Marketplace und illustrierte mit verschiedenen Anwendungsbeispiele was möglich ist und vor allem wie niedrigschwellig der Dienst für Forschende und Projekte nutzbar ist.

[Nanette Rißler-Pipka](https://www.maxweberstiftung.de/ueber-uns/die-stiftung/organisation/geschaeftsstelle/ansprechpartner.html) und [Stefan Buddenbohm](https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/stefan-buddenbohm-1/) zeigten anhand der Anwendungsfälle von GKFI und Text+ wie der SSH Open Marketplace als Tool Registry bzw. Angebotskatalog auch in anderen Kontexten einfach nutzbar ist. Andere Kontexte können jegliche geistes- und kulturwissenschaftliche Initativen sein, wobei eine Anwendbarkeit insbesondere im FID- oder NFDI-Kontext gut vorstellbar ist.

{{<image img="diensteliste.jpg">}}
   Liste von Text+ Angeboten, die aus dem SSH Open Marketplace auf das Text+ Portal deployt wird
{{</image>}}

Anschließend wurden in Einzelarbeit und in Gruppen in entspannter Atmosphäre zusammen Ressourcen im Marketplace zu kuratiert. Dabei sind nicht nur einige neue Ressourcen dazugekommen, es wurden auch viele bestehende Ressourcen ergänzt/korrigiert. Ein wichtiges Ergebnis des Treffens ist auch das vielfältige Feedback sowohl zum SSH Open Marketplace als auch zur Angebotsdarstellung von Text+. 

### Sprechen verstehen: KI und gesprochene Sprache
Am 27. und 28. Juni 2024 fand in den Räumlichkeiten der Stiftung Lyrik Kabinett in München eine Text+ Veranstaltung zum Thema "Sprechen verstehen: KI und gesprochene Sprache" statt. Dabei wurde beleuchtet, wie die rasante Entwicklung der Künstlichen Intelligenz das Generieren, Analysieren und Transkribieren von gesprochener Sprache revolutioniert. In spannenden Keynote-Vorträgen wurde das Thema eingeführt, begleitet von vielen praxisnahen Kurzbeiträgen, die aktuelle Projekte, Tools und Verfahren zur Verarbeitung gesprochener Sprache vorstellten.
Johann Prenninger (BMW Group) gab zu Beginn des ersten Tages den Einstieg in die Thematik mit einem Vortrag über aktuelle Herausforderungen in der Umsetzung von Machine Learning und KI bei BMW mit Beispielen aus der Autoindustrie. Im Verlauf des Nachmittages wurden dann Transkriptionstools, eine ausführliche Evaluation bekannter Spracherkennungsmodelle und Auswertungen zur Effizienz automatisierter Transkription vorgestellt und auf Probleme der Spracherkennung bei Kindern eingegangen.
Der zweite Tag wurde von Barbara Plank (Munich AI and Natural Language Processing) eingeleitet mit einer Keynote über die Sprachverarbeitung von Dialekten, die in den Trainingsdaten von Sprachmodellen häufig unterrepräsentiert oder gar nicht vorhanden sind. Anschließend gab es Einblicke in die Erstellung und Untersuchung von Sprachkorpora aus dem hispanoamerikanischen und englischsprachigen westafrikanischem Raum. Weitere Beiträge behandelten die automatische Transkription von Podcastaufnahmen und die Optimierung von Workflows, die automatische Transkription beinhalten. Das Thema der Erkennung von Dialektmerkmalen und eine Live-Demonstration eines Optopalatographen rundeten den Vortragsteil ab bevor Christoph Draxler zum Abschluss noch einen kurzen Workshop über die Webdienste, Korpora und aktuelle Entwicklungen des Bayerische Archiv für Sprachsignale (BAS) gab.

[Weitere Informationen finden sich auf der Webseite des Events.](https://text-plus.org/aktuelles/veranstaltungen/2024-06-27-sprechen_verstehen/)

## Termine
Alle Veranstaltungen - sowohl kommende als auch bereits stattgefundene - finden Sie auch in unserer [Veranstaltungsrolle im Text+ Portal.](https://text-plus.org/aktuelles/veranstaltungen/)


{{< rawhtml >}}
<style>
tbody tr:nth-child(even) {
  background-color: #ededed;
  color: #000;
}

tbody tr:nth-child(even) td:first-child {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}

tbody tr:nth-child(even) td:last-child {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
}

td {
  padding: 0 5px 0 0;
}
</style>
<table>
  <tr>
    <th width="25%">Datum</th>
    <th width="60%">Event</th>
    <th width="15%">Ort</th>
  </tr>
    <tr valign="top">
    <td>23. Juli 2024</td>
    <td>Text+ Research Rendezvous! Die offene Sprechstunde von Text+</td>
    <td><a href="https://text-plus.org/aktuelles/veranstaltungen/2024-07-23-research-rendezvous/" target="_blank">
        virtuell
      </a></td>
  </tr>
  <tr valign="top">
    <td>09. Oktober 2024</td>
    <td>Pre-Conference-Tutorial zum Thema LLMs</td>
    <td>Mannheim</td>
  </tr>
  <tr valign="top">
    <td>10./11. Oktober 2024</td>
    <td>
      <a href="https://text-plus.org/aktuelles/veranstaltungen/2024-10-10-text-plenary/" target="_blank">
        3. Text+ Plenary
      </a>
    </td>
    <td>Mannheim</td>
  </tr>
</table>
{{< /rawhtml >}}

## Archiv
* [1. Newsletter vom 30.04.2024](https://text-plus.org/aktuelles/aktuelle-infos/posts/newsletter_01/)
