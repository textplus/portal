---
title: "Save the Date – Text+ Plenary und DARIAH-EU Annual Event"
featured_image: std-2025-tplusplenary-dae.png
type: news
date: 2024-11-01
---

{{<image img="std-2025-tplusplenary-dae.png" license="Foto: Göttingen Marktplatz, Daniel Schwen, Wikimedia Commons, GNU FDL v1.2" alt="2025 Plenum Jahrestagung"/>}}

# Save the Date

{{<subhead>}}Kalender und Bleistift finden zusammen{{</subhead>}}

##
Zwei Veranstaltungen hintereinander abzuhalten, ist sowohl eine logistische Herausforderung als auch eine große Chance für Zusammenarbeit. Dies ermöglicht alles von zufälligen Begegnungen bis hin zu geplanter Kooperation. Es bietet einen Austausch zwischen zahlreichen verwandten Interessengruppen und Disziplinen. DARIAH-EU umfasst das gesamte Spektrum der Geistes-, Kunst- und Kulturwissenschaften, während Text+ die text- und sprachbasierten Forschungsgemeinschaften in Deutschland sowie die früheren führenden Institutionen von CLARIN-D und DARIAH-DE repräsentiert.

Deutschland ist seit 2014 Gründungsmitglied des ERIC DARIAH-EU mit seiner führenden Institution SUB Göttingen. Göttingen spielte eine entscheidende Rolle in allen Projektphasen des deutschen DARIAH-Knotens DARIAH-DE von 2011 bis 2019 und unterstützte die DARIAH-DE Betriebskooperation bis 2021. Derzeit wird DARIAH-DE von der SUB Göttingen (DARIAH-DE-Koordinationsstelle), der Max Weber Stiftung (Nationalkoordinator) und der GWDG Göttingen (technische Leitung) geleitet. Alle drei Institutionen sind auch an Text+ und weiteren NFDI-Konsortien sowie am GKFI e.V. beteiligt.

Mit der Austragung des DARIAH-EU Annual Event 2025 in Göttingen fusionieren diese beiden aufeinanderfolgenden Treffen nationale und NFDI-Perspektiven mit der europäischen Sicht in einer gemeinsamen Veranstaltungswoche. Dies ermöglicht gemeinsame Arbeiten an Forschungsinfrastrukturen, Datenkompetenz, Open Science und der Umsetzung der FAIR-Prinzipien. Somit ist Göttingen, wo Geschichte und Gegenwart, nationale und internationale Zusammenarbeit, inhaltliche und technische Führung sowie Deutschland und Europa zusammenkommen, ein passender Veranstaltungsort für das DARIAH Annual Event 2025.

## Text+ Plenum: 16.–17. Juni 2025
Am 16. und 17. Juni 2025 findet das 4. Text+ Plenum in der [Niedersächsischen Staats- und Universitätsbibliothek Göttingen](https://www.sub.uni-goettingen.de/en/news/) statt. Eingeladen sind das Konsortium, die Community, Vertreterinnen und Vertreter anderer NFDI-Konsortien sowie Interessierte. In Vorträgen, Podiumsdiskussionen und Arbeitsgruppensitzungen diskutieren die Teilnehmenden den Stand des Konsortiums und die Möglichkeiten der weiteren Entwicklung.

Die Teilnahme an allen Sitzungen des Plenary ist kostenlos. Weitere Informationen und Anmeldemöglichkeiten werden in den Monaten vor der Veranstaltung hier auf dieser Website verfügbar sein.

## DARIAH-EU Annual Event 2025: 17.–20. Juni 2025
Mit den Eindrücken der [DARIAH-Jahrestagung 2024 in Lissabon](https://www.dariah.eu/event/dariah-annual-event-2024/) noch im Gedächtnis freuen wir uns auf das Treffen im nächsten Jahr: das DARIAH-EU Annual Event 2025. Im Anschluss an das Text+ Plenary findet es vom 17. bis 20. Juni statt, mit den Hauptkonferenztagen vom 18. bis 20. Juni (Mittwoch bis Freitag). Weitere Informationen und Anmeldemöglichkeiten werden in den Monaten vor der Veranstaltung auf der [DARIAH-EU-Website für die Jahrestagungen](https://annualevent.dariah.eu/) verfügbar sein.
