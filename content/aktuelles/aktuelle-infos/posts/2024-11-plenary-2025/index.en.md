---
title: "Save the Date – Text+ Plenary und DARIAH-EU Annual Event"
featured_image: std-2025-tplusplenary-dae.png
type: news
date: 2024-11-01
---

{{<image img="std-2025-tplusplenary-dae.png" license="Foto: Göttingen Marktplatz, Daniel Schwen, Wikimedia Commons, GNU FDL v1.2" alt="2025 Plenary Annual Event"/>}}

# Save the Date

{{<subhead>}}Mark your calendars and save the date{{</subhead>}}

##
Holding two events back to back is both logistic challenge and a huge opportunity for collaboration. It enables anything from chance encounters to planned cooperation. It allows for an exchange between numerous related interest groups and disciplines. DARIAH-EU covers the entire scope of humanities, arts, and cultural sciences, while Text+ represents the text- and language-based research communities in Germany and the former CLARIN-D and DARIAH-DE leading institutions.

Germany is a founding member of the ERIC DARIAH-EU with its leading institution SUB Göttingen since 2014. Göttingen played a pivotal role in all project phases of the German DARIAH node, DARIAH-DE, from 2011 to 2019, and continued to support the DARIAH-DE Operations Cooperation until 2021. Currently, DARIAH-DE is led by the SUB Göttingen (DARIAH-DE Coordination Office), the Max Weber Foundation (National Coordinator), and the GWDG Göttingen (technical lead). All three institutions also participate in Text+ and further NFDI consortia as well as the Association for Research Infrastructures in the Humanities and Cultural Studies.

By locating the DARIAH-EU Annual Event 2025 in Göttingen, these two back to back meetings fuse national and NFDI perspectives with European ones in a joint event week. This enables joint work on research infrastructures, data literacy, open science, and the implementation of the FAIR principles. Thus, it is a fitting choice for the DARIAH Annual Event 2025 to take place in Göttingen, where history and present, national and international cooperation, content and technical leadership, Germany and Europe converge.

## Text+ Plenary: June 16-17, 2025
On June 16-17, 2025, the 4th Text+ Plenary will take place at [Göttingen State and University Library](https://www.sub.uni-goettingen.de/en/news/). The consortium, the community, representatives of other NFDI consortia, and interested parties are invited to attend. In lectures, panel discussions, and working group sessions, participants will discuss the current state of the consortium and possibilities for further development.

Participation in all sessions of the Plenary is free of charge. Further information and registration options will be available here on this website in the months leading up to the event.

## DARIAH-EU Annual Event 2025: June 17-20, 2025
With the impressions from the [DARIAH Annual Event 2024 in Lisbon](https://www.dariah.eu/event/dariah-annual-event-2024/) slowly fading, we are looking forward to next year’s meeting: The DARIAH-EU Annual Event 2025. Following the Text+ Plenary back to back, it will be held from June 17-20, with the main conference days on June 18–20 (Wednesday to Friday). Further information and registration options will be available in the months leading up to the event on the [DARIAH-EU website for the annual events](https://annualevent.dariah.eu/).