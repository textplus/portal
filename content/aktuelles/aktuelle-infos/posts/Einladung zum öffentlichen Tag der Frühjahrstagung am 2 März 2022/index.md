---

title: Text+ Einladung zum öffentlichen Tag der Frühjahrstagung am 2. März 2022
type: news
date: 2022-02-17

---
# Text+: Einladung zum öffentlichen Tag der Frühjahrstagung am 2. März 2022

Text+ lädt Forschende zum öffentlichen Tag der Text+ Frühjahrstagung am 2. März 2022 von 9:00 bis 13:15 Uhr ein. Das Tagungsthema ist die „Text+ Participation Roadmap“. Sie präsentiert die Beteiligungsmöglichkeiten der Community am Angebot des Konsortiums.

## Mittwoch, 02.03.2022, 09:00 bis 13:15 Uhr, öffentlicher Tagungstag

| Zeit | Agendapunkt | Sprecher/-in bzw. Moderator/-in |
|---|---|---|
| 09:00 – 09:10 | Technische Vorstellung | Office |
|       | Begrüßung durch den wissenschaftlichen Sprecher | Erhard Hinrichs |
|       | Begrüßung Sprecher im Konsortium nach Satzung des NFDI-Vereins | Andreas Witt&nbsp; |
| 09:10 – 09:30 | Kurzeinführung Text+ und Governance mit Blick auf Community Involvement und die Koordinationskomitees&nbsp; | Erhard Hinrichs |
| 09:30 - 10:30 | Vorstellung Text+ Participation Roadmap sowie Vorstellung der Break-Out-Themen | Thorsten Trippel und Lukas Weimer |
|              | Vorstellung der Ausschreibung zur Förderung von Kooperationsprojekten durch Text+ | Ingrid Schröder |
|           | Vorstellung der Task Area Leads&nbsp; | Peter Leinen, Alexander Geyken, Andreas Speer, Regine Stein |
| 10:30 - 10:45 | PAUSE |          |
| 10:45 - 11:30 | Verteilung auf die Break-Out-Räume | Thorsten Trippel und Jutta Bopp |
|  |Break-Out-Räume zur Text+ Participation Roadmap - Wechsel zwischen den Gruppen möglich - |
|              | 1. User Stories | Stefan Buddenbohm und Lukas Weimer |
|              | 2. Kooperation mit anderen geisteswissenschaftlichen Konsortien der NFDI und den Sektionen der NFDI | Erhard Hinrichs und Andreas Witt |
|              | 3. Social Media Aktivitäten | Aleksandra Pushkina |
|              | 4. Integration von Angeboten am Beispiel von Daten | Andreas Henrich |
|              | 5. Workshops &amp; Summer Schools | Andrea Rapp |
|              | 6. Forschungsdatenmanagement-Angebote an die Community | Thorsten Trippel |
|              | Break-Out-Räume der Task Area Leads: Fragemöglichkeit zu Kooperationsprojekten durch potenzielle Antragstellende | Peter Leinen/Alexander Geyken/Andreas Speer/Regine Stein |
| 11:30 - 11:45 | PAUSE |          |
| 11:45 - 12:15 | Kurzvorstellung der Arbeitsergebnisse aus den Break-Out-Räumen zur Text+ Participation Roadmap | Moderator/-innen der jeweiligen Räume |
| 12:15 - 13:00 | Diskussion der Arbeitsergebnisse | Andreas Henrich |
| 13:00 - 13:15 | Zusammenfassung des Tages und Ausblick auf die weitere Zusammenarbeit mit der Community | Andrea Rapp |

Bitte beachten Sie, dass im Rahmen der Frühjahrstagung auch Informationen zu den Kooperationsprojekten vorgestellt werden: Hier können sich Forschende/Institutionen, die keine Förderung im Rahmen von Text+ erhalten, aber Daten und andere Angebote für die Integration in Text+ bereitstellen möchten, um Projektmittel bewerben. Im Rahmen der Break-Out-Räume von 10:45 bis 11:30 Uhr können potenzielle Antragstellende sich mit Verantwortlichen aus dem Projekt hinsichtlich Projektideen und Anknüpfungsmöglichkeiten austauschen.