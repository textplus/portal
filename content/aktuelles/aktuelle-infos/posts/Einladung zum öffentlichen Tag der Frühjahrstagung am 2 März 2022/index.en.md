---
title: Text+ Invitation to the Public Day of the Spring Meeting on March 2, 2022
type: news
date: 2022-02-17
---

# Text+: Invitation to the Public Day of the Spring Meeting on March 2, 2022

Text+ invites researchers to the public day of the Text+ Spring Meeting on March 2, 2022, from 9:00 am to 1:15 pm. The theme of the conference is the "Text+ Participation Roadmap," which presents the community's opportunities to engage with the consortium's offerings.

## Wednesday, March 2, 2022, 9:00 am to 1:15 pm, public conference day

| Time | Agenda Item | Speaker or Moderator |
|---|---|---|
| 09:00 am – 09:10 am | Technical Presentation | Office |
|       | Welcome by the Scientific Spokesperson | Erhard Hinrichs |
|       | Welcome by the Consortium's Speakers according to the NFDI Association Statute | Andreas Witt&nbsp; |
| 09:10 am – 09:30 am | Brief Introduction to Text+ and Governance with a Focus on Community Involvement and Coordination Committees | Erhard Hinrichs |
| 09:30 am - 10:30 am | Presentation of Text+ Participation Roadmap and Introduction of Break-Out Topics | Thorsten Trippel and Lukas Weimer |
|              | Presentation of the Call for Proposals for Cooperative Projects by Text+ | Ingrid Schröder |
|           | Introduction of Task Area Leads&nbsp; | Peter Leinen, Alexander Geyken, Andreas Speer, Regine Stein |
| 10:30 am - 10:45 am | BREAK |          |
| 10:45 am - 11:30 am | Breakout Session Assignments | Thorsten Trippel and Jutta Bopp |
|  | Breakout Rooms for Text+ Participation Roadmap - Switching between Groups Possible - |
|              | 1. User Stories | Stefan Buddenbohm and Lukas Weimer |
|              | 2. Collaboration with Other Humanities Consortia of the NFDI and NFDI Sections | Erhard Hinrichs and Andreas Witt |
|              | 3. Social Media Activities | Aleksandra Pushkina |
|              | 4. Integration of Offerings Using Data as an Example | Andreas Henrich |
|              | 5. Workshops &amp; Summer Schools | Andrea Rapp |
|              | 6. Research Data Management Offerings to the Community | Thorsten Trippel |
|              | Task Area Leads' Breakout Rooms: Questions Regarding Cooperative Projects by Potential Applicants | Peter Leinen/Alexander Geyken/Andreas Speer/Regine Stein |
| 11:30 am - 11:45 am | BREAK |          |
| 11:45 am - 12:15 pm | Brief Presentation of Results from the Breakout Sessions on the Text+ Participation Roadmap | Moderators of Respective Rooms |
| 12:15 pm - 1:00 pm | Discussion of Results | Andreas Henrich |
| 1:00 pm - 1:15 pm | Summary of the Day and Outlook on Further Collaboration with the Community | Andrea Rapp |

Please note that as part of the Spring Meeting, information about cooperative projects will also be presented: Researchers/institutions that do not receive funding from Text+ but wish to provide data and other offerings for integration into Text+ can apply for project funds. During the breakout sessions from 10:45 am to 11:30 am, potential applicants can exchange ideas with project representatives regarding project ideas and collaboration possibilities.