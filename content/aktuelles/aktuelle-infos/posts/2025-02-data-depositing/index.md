---
title: "Data Depositing online"
featured_image: datenaufnahme.png
type: news
date: 2025-02-05
---

{{<image img="datenaufnahme.png" license="Foto: Screenshot Text+, CC-BY 4.0" alt="Datenaufnahme"/>}}

# Datenaufnahme in Text+

## Sie möchten Forschungsdaten sicher und nachnutzbar archivieren?
Im Rahmen der NFDI ist Text+ Anlaufstelle für text- und sprachbasierte Forschungsdaten und bietet der Wissenschaftscommunity die Möglichkeit der Übernahme von Daten. Diese werden dadurch nicht nur langfristig archiviert, sondern entsprechend der rechtlichen Möglichkeiten zur weiteren Nachnutzung bereitgestellt. Der Zugang steht allen Forschenden mit einer akademischen Affiliation offen und unterstützt die Berücksichtigung datenschutz- und lizenzkonformer Zugriffsbedingungen.

## Übergabe von Forschungsdaten an ein Text+ Zentrum
Eines der Text+ Zentren übernimmt dafür die Daten und archiviert sie nachhaltig und interoperabel mit den Services und Werkzeugen von Text+. Die Text+ Zentren unterstützen in allen Belangen der Aufnahme von Forschungsdaten. Der Support reicht von der Aufbereitung, Auszeichnung und Übergabe der Forschungsdaten – inklusive der Beschreibung der Forschungsdaten mit Text+ spezifischen Metadaten (s. dazu auch die weiterführenden Informationen) – bis zur Auswahl eines geeigneten Datenzentrums. Nutzen Sie gerne einfach das Formular direkt im Anschluss, um uns eine erste Beschreibung Ihrer Daten zukommen zu lassen. Unser Helpdesk-Team klärt dann im Dialog mit Ihnen alles Notwendige für eine Veröffentlichung.

## Wie funktioniert es?
Als ersten Schritt bietet Text+ ein [Formular für die Datenübernahme an](https://text-plus.org/daten-dienste/depositing/#datenaufnahme). Danach werden die notwendigen Erschließungs- und Kurationsschritte besprochen, um schließlich die Forschungsdaten sicher und nachnutzbar in einer vertrauenswürdigen und disziplinär einschlägigen Umgebung bereitzustellen. Eine Such- und Findbarkeit der archivierten Forschungsdaten über die Text+ Registry ist dabei eine sinnvolle Option, um die Sichtbarkeit der Daten zu erhöhen. 