---
title: "Data Depositing"
featured_image: datenaufnahme.png
type: news
date: 2025-02-05
---

{{<image img="datenaufnahme.png" license="Foto: Screenshot Text+, CC-BY 4.0" alt="Datenaufnahme"/>}}

# Data Depositing in Text+  

## Would you like to securely archive and make your research data reusable?  
As part of the NFDI, Text+ serves as a hub for text- and language-based research data, offering the scientific community the opportunity to deposit their data. This ensures not only long-term archiving but also, within legal constraints, enables further reuse. Access is available to all researchers with an academic affiliation and supports compliance with data protection and licensing requirements.  

## Submitting Research Data to a Text+ Center  
A Text+ center will take over the data and archive it sustainably and interoperably using Text+ services and tools. The Text+ centers provide support in all aspects of research data ingestion. This includes data preparation, annotation, and submission—along with the description of research data using Text+-specific metadata (see additional information for details)—as well as selecting a suitable data center.  

Feel free to use the form below to provide an initial description of your data. Our helpdesk team will then coordinate with you to ensure everything necessary for publication is arranged.  

## How Does It Work?  
As a first step, Text+ offers a [data submission form](https://text-plus.org/daten-dienste/depositing/#datenaufnahme). Subsequently, the necessary processing and curation steps will be discussed to ensure that the research data is securely stored and made reusable in a trusted and discipline-specific environment.  

Making archived research data searchable and findable via the Text+ Registry is also a useful option to enhance data visibility.  