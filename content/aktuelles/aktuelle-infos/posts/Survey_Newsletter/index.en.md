---
title: "Survey Newsletter"
featured_image: Newsletter_v1.png
type: news
date: 2024-10-29
---

{{<image img="Newsletter_v1.png" alt="Newsletter"/>}}

## Evaluation of the Text+ Newsletter  
The Text+ Newsletter has now been running for almost a year. So far, it includes four issues with six sections and is distributed to over 400 email recipients as a link and made available in the [portal.](https://text-plus.org/aktuelles/aktuelle-infos/posts/newsletter_04/) For us, it is beyond question that a project like Text+ requires such a tool. However, we aim to further develop this tool, potentially introduce new content, while being mindful of your time and providing the community with an opportunity to interact with us.  

Therefore, we kindly ask you to [answer eight questions](https://forms.gle/sT2F5Waph8AZFvZ6A) or share more detailed feedback with us. For the latter, you can reach out directly via the well-known Text+ Office email address. We do not collect personalized data, and the survey is anonymous. The Text+ privacy policy applies.  

Thank you for your time and feedback!  

## Archive  
* [1st Newsletter from April 30, 2024](https://text-plus.org/aktuelles/aktuelle-infos/posts/newsletter_01/)  
* [2nd Newsletter from July 15, 2024](https://text-plus.org/aktuelles/aktuelle-infos/posts/newsletter_02/)  
* [3rd Newsletter from October 29, 2024](https://text-plus.org/aktuelles/aktuelle-infos/posts/newsletter_03/)  
* [4th Newsletter from January 14, 2025](https://text-plus.org/aktuelles/aktuelle-infos/posts/newsletter_04/)  