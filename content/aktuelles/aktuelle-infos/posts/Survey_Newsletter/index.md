---
title: "Survey zum Newsletter"
featured_image: Newsletter_v1.png
type: news
date: 2025-01-28
---

{{<image img="Newsletter_v1.png" alt="Newsletter"/>}}


## Evaluation des Text+ Newsletters
Den Text+ Newsletter gibt es nun fast ein Jahr. Er umfasst bisher vier Ausgaben mit derzeit sechs Rubriken und wird an über 400 Mailempfänger:innen als Link verteilt und im [Portal bereitgestellt.](https://text-plus.org/aktuelles/aktuelle-infos/posts/newsletter_04/) Dass ein Projekt wie Text+ ein solches Instrument benötigt, steht für uns außer Frage. Wir möchten aber das Instrument weiterentwickeln, vielleicht neue Inhalte präsentieren, gleichzeitig aber mit Ihrer Zeit sparsam umgehen und der Community die Möglichkeit zur Interaktion mit uns vermitteln.

Daher bitten wir Sie um die [Beantwortung von acht Fragen](https://forms.gle/sT2F5Waph8AZFvZ6A) oder um die Mitteilung ausführlicheren Feedbacks. Für letzteres können Sie uns direkt unter der bekannten Text+ Office-Adresse kontaktieren. Wir erheben keine personalisierten Daten, die Befragung ist anonym. Es gilt die Datenschutzerklärung von Text+.

Vielen Dank für Ihre Zeit und Ihr Feedback!

## Archiv
* [1. Newsletter vom 30.04.2024](https://text-plus.org/aktuelles/aktuelle-infos/posts/newsletter_01/)
* [2. Newsletter vom 15.07.2024](https://text-plus.org/aktuelles/aktuelle-infos/posts/newsletter_02/)
* [3. Newsletter vom 29.10.2024](https://text-plus.org/aktuelles/aktuelle-infos/posts/newsletter_03/)
* [4. Newsletter vom 14.01.2025](https://text-plus.org/aktuelles/aktuelle-infos/posts/newsletter_04/)