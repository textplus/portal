---
title: "Text+ Newsletter #3"
featured_image: Newsletter_v1.png
type: news
date: 2024-10-29
---

{{<image img="Newsletter_v1.png" alt="Newsletter"/>}}


## Herzlich willkommen!
Hier ist der dritte Text+ Newsletter im Oktober 2024. Wir laden Sie herzlich ein, einen Blick auf unser Projektgeschehen zu werfen. Über Feedback, Fragen oder Wünsche freuen wir uns. Diese erreichen uns am einfachsten über das [Office](mailto:office@text-plus.org).

## Das 3. Text+ Plenary: Faszinierende Einblicke in die Welt der Großen Sprachmodelle (LLMs)
Am 10. und 11. Oktober 2024 verwandelten sich die historischen Räumlichkeiten des Schlosses Mannheim in ein Zentrum für Innovation und Austausch, als das 3. Text+ Plenary stattfand. Rund 200 Teilnehmende erlebten ein spannendes Programm rund um das Thema „Große Sprachmodelle (LLMs) und deren Nutzung“.

Bereits vor dem offiziellen Start des Plenarys bot das Pre-Conference-Tutorial „Large Language Models: Eine praktische Einführung“ von Jennifer Ecker, Pia Schwarz und Rebecca Wilm eine hervorragende Gelegenheit, sich mit den Grundlagen und praktischen Anwendungen von LLMs vertraut zu machen. Das Tutorial, das im Vortragssaal des Leibniz-Instituts für Deutsche Sprache stattfand, wurde von den Teilnehmenden als  informativ und inspirierend gelobt. Für alle, die nicht dabei sein konnten, steht die Präsentation [unter dem Link zur Verfügung](https://events.gwdg.de/event/638/contributions/2093/). 

Der erste Tag des Plenarys stand ganz im Zeichen von Künstlicher Intelligenz und Maschinellem Lernen. In einer Reihe von Vorträgen wurde aufgezeigt, wie diese Technologien in den Geisteswissenschaften eingesetzt und weiterentwickelt werden können. Sina Zarrieß von der Universität Bielefeld berichtete über Möglichkeiten, auch mit kleineren Sprachmodellen wissenschaftliche Fragen zu untersuchen, während Juri Opitz über die Rolle der Sprachwissenschaften in der automatischen Sprachverarbeitung sprach. Maria Becker demonstrierte sehr praktisch, wie mit Sprachmodellen der Transfer von Forschungsergebnissen in eine breitere Öffentlichkeit erforscht werden kann. Bei Nils Reiter wurden die hohe Variation von Ergebnissen als Konsequenz von veränderten Prompts an generative Modelle deutlich, und Anne Lauscher wies auf soziale Komponenten der Verwendung von großen Sprachmodellen hin. Lalith Manjunath stellte im Abschluss der Vortragsreihe technische Aspekte vor, einschließlich seiner Erfahrungen bei openGPT-x. Eine Postersession mit 19 ausgewählten Beiträgen bot zusätzlich die Möglichkeit, ein breites Spektrum an Erfahrungen und Expertise zu LLMs zu präsentieren – von deren Weiterentwicklung bis hin zu ihrer konkreten Anwendung in der Forschung. Höhepunkt der Session war die Verleihung des Best-Poster-Awards, der mit 150 EUR dotiert war. Die Gewinner waren:

* Margret Mundorf (Universität Heidelberg) mit „Legal Linguistic Memos mit Large Language Models: Automatisierte Erfassung und Klassifizierung von Sachverhaltsbeschreibungen im Familienrecht“
* Steffen Steiner und Frank Krüger (Hochschule Wismar) mit „SwineBad: Tabellenextraktion und Informationsstrukturierung aus dem Swinemünder Badeanzeiger“
* Eric Dubey, Matteo Lorenzini, Martin Reisacher (Universität Basel) und Tim Rüdiger (Zentralbibliothek Zürich) mit „SwissGB4Science - ein Volltext Korpus für die Forschung“

{{< carousel >}}
  {{<image img="TextPlus_Plenary2024_Fotos/TextPlus_Plenary2024_Aula.jpg" context="carousel" license="Text+ Plenary 2024, Schloss Mannheim"/>}}
  {{<image img="TextPlus_Plenary2024_Fotos/TextPlus_Plenary2024_Registrierung.jpg" context="carousel" license="Text+ Plenary 2024, Schloss Mannheim"/>}}
  {{<image img="TextPlus_Plenary2024_Fotos/Eröffnung.jpg" context="carousel" license="Text+ Plenary 2024, Schloss Mannheim"/>}}  
  {{<image img="TextPlus_Plenary2024_Fotos/Vortrag_1.jpg" context="carousel" license="Text+ Plenary 2024, Schloss Mannheim"/>}}  
  {{<image img="TextPlus_Plenary2024_Fotos/Vortrag_2.jpg" context="carousel" license="Text+ Plenary 2024, Schloss Mannheim"/>}}  
  {{<image img="TextPlus_Plenary2024_Fotos/Vortrag_3.jpg" context="carousel" license="Text+ Plenary 2024, Schloss Mannheim"/>}}  
  {{<image img="TextPlus_Plenary2024_Fotos/Vortrag_4.jpg" context="carousel" license="Text+ Plenary 2024, Schloss Mannheim"/>}}  
  {{<image img="TextPlus_Plenary2024_Fotos/Vortrag_5.jpg" context="carousel" license="Text+ Plenary 2024, Schloss Mannheim"/>}}  
  {{<image img="TextPlus_Plenary2024_Fotos/TextPlus_Plenary2024_MA_1.jpg" context="carousel" license="Text+ Plenary 2024, Schloss Mannheim"/>}}  
  {{<image img="TextPlus_Plenary2024_Fotos/Verabschiedung.jpg" context="carousel" license="Text+ Plenary 2024, Schloss Mannheim"/>}}
  {{<image img="TextPlus_Plenary2024_Fotos/TextPlusPlenary2024.jpeg" context="carousel" license="Text+ Plenary 2024, Schloss Mannheim"/>}}
{{< /carousel >}}

Der Tag klang mit einem geselligen Abendempfang aus, bei dem die Teilnehmenden die Gelegenheit hatten, sich in entspannter Atmosphäre auszutauschen und neue Kontakte zu knüpfen.

Der zweite Tag war den internen Projekttreffen von Text+ gewidmet. Rund 100 Mitarbeitende des Projekts kamen zusammen, um in Arbeitsgruppen und Task Areas ihre laufenden Projekte zu besprechen und die Zusammenarbeit zu vertiefen.

Ein herzliches Dankeschön an alle Vortragenden und Teilnehmenden für ihre wertvollen Beiträge und den regen Austausch! Wir freuen uns schon sehr auf das nächste Text+ Plenary, das am 16. und 17. Juni in Göttingen  im zeitlichen Vorlauf des europäischen [DARIAH Annual Events](https://dhd-blog.org/?p=21227) stattfinden wird.


## Wahl der Coordination Committees
Die zweijährlich stattfindenden Wahlen der Koordinationskomitees von Text+ stehen an. Der Wahltermin ist der 6. November 2024. Die Wahl wird über ein elektronisches System erfolgen und eine Stimmabgabe vom Wahltermin bis zum 13. November 2024 ermöglichen.

Die Koordinationskomitees sind die zentralen Mitbestimmungsgremien der Text+ Communitys. Sie setzen sich aus drei verschiedenen Scientific Coordination Committees, die jeweils für eine der Datendomänen (Collections, Editions, Lexical Resources) zuständig sind, und einem Operations Coordination Committee zusammen. Ihre Aufgabe ist es, kontinuierlich das Portfolio an Daten, Werkzeugen und Services zu evaluieren und zu erweitern. Die Koordinationskomitees setzen sich aus Expertinnen und Experten der jeweiligen (Fach-)Domänen zusammen und werden alle zwei Jahre gewählt.

Gewählt werden die folgenden Komitees von Text+: 
- Scientific Coordination Committee für die Task Area Collection
- Scientific Coordination Committee für die Task Area Lexical Resources
- Scientific Coordination Committee für die Task Area Editions
- Operations Coordination Committee für die Task Area Infrastructure/Operations

Die Ansprechpersonen der Organisationen, Institutionen und Einrichtungen, die wahlberechtigt sind, erhalten zum Wahltermin die Zugangsdaten zum Wahlsystem.

Weitere Informationen finden Sie im [Text+ Portal](https://text-plus.org/ueber-uns/governance/komiteebesetzung/). Rückfragen richten Sie gerne jederzeit über office@text-plus.org an den Wahlausschuss.

## Highlights aus dem Blog
In dieser Rubrik stellen wir interessante Beiträge aus dem Text+ Blog vor. Das Blog informiert über Text+ und stellt in Ergänzung zur Webseite auch Work in Progress vor oder erlaubt einen detaillierteren Blick auf individuelle Themen. Alle Beiträge sind mit DOIs versehen und zitierbar.

Beiträge von Gastautorinnen und -autoren zu Themen von Interesse für die Text+ Community sind herzlich willkommen! [Schreiben Sie uns, wenn Sie ein Thema haben.](https://textplus.hypotheses.org/uber-uns)

### Textbox im TextGrid Repository
Die Integration existierender Ressourcen ist ein zentrales Anliegen der NFDI und genau darum geht es in diesem Beitrag. Es wird mit textbox eine Reihe von Korpora aus der Romanistik vorgestellt, die in das TextGrid Repository intergriert wurden und dabei ein neues Feature von TextGrid nutzen: Die Gattungszuordnung durch die GND.

Grundvoraussetzung für eine reibungslose Integration von Daten sind die entsprechenden Schnittstellen und Workflows, damit neue Inhalte zu den bereits vorhandenen Ressourcen in Beziehung gesetzt werden können. 

Genau mit diesem Ziel wurde der neue Import-Workflow, genannt Fluffy Import, erstellt. Dieser vereinfacht das Einspielen von TEI-Dokumenten in das TextGrid Repository und verbessert die allgemeine Qualität der Metadaten. Den entsprechenden Workflow konnten wir bereits auf einem Workshop vorstellen und werden bald mit weiteren Neuigkeiten hierzu aufwarten können. 

{{<image img="textbox.png">}}
   Textbox im TGR
{{</image>}}

Textbox enthält neun Korpora literarischer Texte aus den romanischen Sprachen Französisch, Italienisch, Spanisch und Portugiesisch (Reihung entsprechend der Anzahl der erfassten Texte). Die in TEI kodierten Korpora wurden im Rahmen des CLiGs-Projekts unter der Leitung von Christof Schöch (Universität Trier) am Lehrstuhl von Fotis Jannidis in Würzburg erstellt. Bis zu einem gewissen Grad diente textbox als Test für die späteren ELTeC-Korpora, die auch im TextGrid Repository zu finden sind. Ähnlich wie die ELTeC Korpora ist textbox ebenfalls bereits auf GitHub und Zenodo verfügbar. Im Rahmen des Call for User Stories für Text+ wurde eine entsprechende User Story zu textbox eingereicht.

Bitte lesen Sie weiter im [Text+ Blog](https://textplus.hypotheses.org/11406).

Diesen Blogbeitrag zitieren: 
_José Calvo Tello (24. Oktober 2024). Textbox in TextGrid Repository. Text+ Blog. Abgerufen am 28. Oktober 2024 von https://doi.org/10.58079/12kbl_

## Werkstattberichte
### Neugestaltete Webseite zu Text+ Daten- und Kompetenzzentren
Die [Webseite der Text+ Zentren](https://text-plus.org/ueber-uns/textpluszentren/) wurde umfassend überarbeitet und neu gestaltet. Das Ziel der Überarbeitung war es, die in Abstimmung mit den einzelnen Partnern vorgenommen wurde, die Informationen zu den einzelnen Zentren zu aktualisieren sowie übersichtlich und einheitlich strukturiert darzustellen.

{{<image img="centres.png">}}
   Text+ Zentren mit Filterfunktion
{{</image>}}

Die Daten- und Kompetenzzentren unterstützen die verteilte Infrastruktur von Text+: Während sich Datenzentren auf die Sammlung, Speicherung und Bereitstellung von Forschungsdaten konzentrieren, bieten Kompetenzzentren spezialisierte Unterstützung in der Datenverwaltung. Die Text+ Zentren sind den Bereichen Collections, Lexikalische Ressourcen, Editionen sowie Infrastruktur/Betrieb zugeordnet und arbeiten in thematischen Clustern, um spezifische Datenanforderungen zu erfüllen.

Die Webseite der Text+ Zentren wurde so gestaltet, dass die Daten- und Kompetenzzentren nach Arbeitsbereichen und Clustern gefiltert werden können. Über den Button „Mehr Infos“ gelangt man zu detaillierten Informationen über jedes Zentrum, darunter die angebotenen Daten und Dienste, die Möglichkeit der Entgegennahme von Daten Dritter sowie die zuständigen Ansprechpersonen. Die überarbeitete Struktur erleichtert die gezielte Navigation und ermöglicht es Nutzenden, schnell die relevanten Informationen zu den verschiedenen Zentren zu finden.

### Styleguide
Die Webseite wurde um [Informationen und Downloads zur Corporate Identity ergänzt](https://text-plus.org/ueber-uns/style-guide/). Diese Seite bietet Hinweise und Dateien für einen einfachen und stilsicheren Gebrauch der Text+-Marke. Sie bietet Orientierung für das Erstellen von Projektmaterialien aller Art. Berücksichtigt werden die wesentlichen Gestaltungselemente des Konsortiums – Logo, Farben und Schrift.

Desweiteren findet sich ein Vorschlag, um in Publikationen, Vorträgen oder Webseiten auf die Förderung von Text+ durch die DFG hinzuweisen.

{{<image img="verwendungsrichtlinien.jpg">}}
   Richtige Verwendung des Logos
{{</image>}}

### Zotero4NFDI: Kooperation von NFDI4Ing und Text+
Im Text+ Office werden regelmäßig Python-Skripte entwickelt, um verschiedene Tätigkeiten zu automatisieren. Diese umfassen beispielsweise das Auslesen von Inhalten aus Tabellen und Datenbanken oder die Auswertung von Nutzungsdaten der Social-Media-Kanäle. Ähnliche Ansätze werden auch im NFDI-Konsortium NFDI4Ing verfolgt.

Da es eine Vielzahl von Skripten gibt, die in beiden Konsortien entwickelt und genutzt werden, liegt es nahe, dass auch andere NFDI-Konsortien von der Nutzung dieser Skripte profitieren könnten. Vor diesem Hintergrund planen Text+ und NFDI4Ing die gemeinsame Veröffentlichung ihrer Skripte in Form von Umbrella-Publikationen.

Als erste Veröffentlichung wurden Skripte zum Auslesen einzelner oder aller Publikationen aus einer Zotero-Bibliothek hochgeladen, da beide Konsortien die Metadaten ihrer Publikationen systematisch in eigenen Zotero-Bibliographien speichern. Die Veröffentlichungen von Text+ sind unter https://zenodo.org/records/12605448 (DOI: 10.5281/zenodo.12605448) und die der NFDI4Ing unter https://zenodo.org/records/12680673 (DOI: 10.5281/zenodo.12680673) zu finden. Alle Skripte stehen unter der Lizenz CC BY 4.0 und die Nachnutzung wird ausdrücklich begrüßt.

Weitere gemeinsame Publikationen sind für die Zukunft geplant.

## Ergebnisse des Ausschreibungsverfahrens 2024 zur Förderung von Kooperationsprojekten durch Text+
Das NFDI-Konsortium Text+ vergibt jedes Jahr Fördergelder für Kooperationsprojekte, um die Angebote an Daten und Services von Text+ kontinuierlich zu erweitern und für die Community der Forschenden langfristig verfügbar zu machen. Dazu werden auf maximal ein Kalenderjahr befristete Projekte gefördert, deren Ergebnisse in die Text+ Infrastruktur integriert werden können.

Wir freuen uns, bekannt zu geben, dass im Ausschreibungsverfahren 2024 fünf herausragende Kooperationsprojekte eine Förderzusage erhalten haben: 
* Text+-Schnittstellen zu den Interview-Sammlungen in Oral-History.Digital (text+oh.d) eingereicht von Dr. Cord Pagenstecher (Universitätsbibliothek der Freien Universität Berlin) für die Datendomäne Collections
* Graeco-Arabicum – Open Data (GlossGA – OD) eingereicht von Dr. Rüdiger Arnzen (Friedrich-Alexander-Universität Erlangen-Nürnberg) für die Datendomäne Lexikalische Ressourcen
* HAdW GND-basierte Webservices – Beaconizer & Discoverer (Hagrid) eingereicht von Dr. Frank Grieshaber (Heidelberger Akademie der Wissenschaften) in der Infrastructure/Operations  
* Aufbau einer offenen digitalen Sammlung historischer musiktheoretischer Texte aus dem deutschsprachigen Raum anhand von Beispielen aus dem 19. Jahrhundert (DigiMusTh) eingereicht von Prof. Dr. Fabian C. Moss (Julius-Maximilians-Universität Würzburg) für die Datendomäne Collections
* LOD-Rollen-Modellierungen aus den Registern von Regestenwerken zum Mittelalter (LRM) eingereicht von Prof. Dr. Andreas Kuczera (Akademie der Wissenschaften und der Literatur Mainz) für die Datendomäne Editionen 

Wir sind gespannt auf die innovativen Ansätze und freuen uns auf eine produktive und enge Zusammenarbeit im kommenden Jahr. 

## Publikationen, Services & Infoangebote
Text+ pflegt seine Bibliographie bei [Zotero](https://www.zotero.org/groups/4533881/textplus/library) und stellt auf seinem [Portal eine strukturierte Ansicht](https://text-plus.org/themen-dokumentation/bibliographie/) dar.

### Sprechen verstehen: KI und gesprochene Sprache
Die Zusammenfassung der Vorträge aus dem Community-Workshops „Sprechen verstehen: KI und gesprochene Sprache“ ist veröffentlicht:

* *Draxler, C. (2024). KI und gesprochene Sprache. https://doi.org/10.5281/zenodo.12606959.*

Der Workshop fand am 27. und 28. Juni 2024 in München statt und befasste sich mit Fragen zur Verarbeitung von Dialekten durch KI, der Transkription großer Sprachmengen und innovativen Methoden zum Spracherwerb. 

Am ersten Tag hielt Johannes Prenninger (BMW Group) eine Keynote darüber, wie moderne vernetzte Autos große Menge von Sprachdaten generieren und wie diese verarbeitet werden, um benutzerfreundliche Sprachsteuerungssysteme zu entwickeln. Die Spracherkennung im Auto reagiert dabei nur auf spezifische Befehle, damit keine Privatgespräche überwacht wird. Anschließend folgten Fachvorträge, die sich mit verschiedenen Transkriptionstools und deren Anwendung in der Forschung beschäftigten.

Der zweite Tag begann mit einer Keynote von Barbara Plank (Ludwig-Maximilians-Universität München), die Herausforderungen bei der KI-Verarbeitung von Dialekten und nicht-standardisierten Sprachen thematisierte, wie sie häufig in sozialen Medien oder Citizen-Science-Projekten vorkommen. In den anschließenden Vorträgen wurden Projekte vorgestellt, darunter die automatische Transkription populärer Podcasts wie „Fest & Flauschig“ mithilfe des Dresdener Hochleistungsrechner-Clusters. Ein weiteres Highlight war die Präsentation eines neuen Palatographie-Systems, das mit optischen Sensoren arbeitet und in der Logopädie zur Analyse von Sprechmustern eingesetzt werden kann.

Zum Abschluss stellte das Bayerische Archiv für Sprachsignale (BAS) seine neuen Webdienste vor, die moderne Spracherkennungssysteme wie UWEBASR und whisperX integrieren und verschiedene Optionen zur Transkription von Sprachaufnahmen bieten. Die Teilnehmenden konnten dabei erfahren, wie sich diese Tools effizient für die Erstellung von Rohtranskripten nutzen lassen.

Weitere Details zum Workshop finden sich im Text+ Blogartikel von Christoph Draxler und Philippe Genêt "Wenn künstliche Intelligenz auf gesprochene Sprache trifft: Der Collections Community-Workshop 2024" (vom 25.08.2024, https://textplus.hypotheses.org/11229).


## Events & Nachberichte
### Digital Humanities Open Garden 2024
Am 13. Juni 2024 lud das *Forum Digital Humanities Leipzig* (FDHL) zum diesjährigen *Digital Humanities Open Garden* an der Sächsischen Akademie der Wissenschaften zu Leipzig (SAW Leipzig). Ziel der Veranstaltung ist es, in einem ungezwungenen Umfeld aktuelle Arbeiten im Bereich der digitalen Geisteswissenschaften vorzustellen und allgemein zu Austausch und Vernetzung anzuregen. Zielgruppe der Veranstaltung sind alle DH-Interessierte, insbesondere auch Studentinnen und Studenten entsprechender Studiengänge.

{{<image img="Bild_DH_Open_Garden.jpg">}}
   Die digitalen Geisteswissenschaften in der Diskussion vertieft
{{</image>}}

In diesem Jahr wurde das Vortragsprogramm von Vertretern gleich zweier Institutionen des Text+ Konsortiums bestritten: der Deutschen Nationalbibliothek (DNB) sowie der SAW Leipzig.

Dr. Ramon Voges stellte unter dem Titel “Chatbots, Wasserzeichen und Digitale Nachlässe” aktuelle DH-Projekte an der Deutschen Nationalbibliothek vor. Die DNB hat als zentrale Archivbibliothek die Aufgabe, alle in Deutschland veröffentlichten Werke sowie Werke über Deutschland und in deutscher Sprache weltweit zu sammeln, dauerhaft zu archivieren und bibliografisch zu verzeichnen. Sie betreibt Standorte in Leipzig und Frankfurt am Main und beherbergt spezialisierte Sammlungen wie das Deutsche Buch- und Schriftmuseum, das Deutsche Exilarchiv und das Deutsche Musikarchiv.

Die Projekte der DNB bewegen sich, nicht zuletzt wegen ihrer institutionellen Nähe zur Verlagsbranche, zum einen innerhalb der rechtlichen Vorgaben des Urheberrechts. Zum anderen definiert das Gesetz über die Deutsche Nationalbibliothek die Leitplanken ihrer Arbeit, insbesondere im Umgang mit Pflichtabgaben und der Archivierung von Publikationen. Trotz dieser mitunter engen Rahmenbedingungen gibt es viele Möglichkeiten, mit der DNB zusammenzuarbeiten. Dies wurde anhand dreier Beispiele demonstriert:

* Chatbot: Dieses Projekt beschäftigt sich mit der Entwicklung eines Chatbots, der z.B. auf häufig gestellte Fragen von Nutzern der DNB antwortet. Dafür kommt ein Retrieval-Augmented-Generation-System zum Einsatz, das lokal gespeicherte, urheberrechtlich geschützte Inhalte nutzen kann, um präzise und kontextbezogene Antworten zu generieren. 
* Wasserzeichen4Punkt0: Hierbei handelt es sich um ein Kooperationsprojekt, das die Identifikation und Klassifizierung von Wasserzeichen in historischen Papieren zum Ziel hat. Es nutzt Methoden wie CycleGANs und K-Nearest-Neighbor-Algorithmen zur Mustererkennung und Bestimmung.
* Digitale Nachlässe: Dieses Vorhaben konzentriert sich darauf, einen Geschäftsgang für die Sicherung, Kuratierung und Bereitstellung digitaler Nachlässe zu entwerfen.

Insbesondere der Wissenschaftliche Dienst und das DNBLab der DNB bieten umfassende Unterstützung und Zugang zu digitalen Beständen und Infrastrukturen. Der Wissenschaftliche Dienst unterstützt Projekte, die mit den Beständen oder Daten der DNB arbeiten, während das DNBLab als Plattform für den Zugang zu und die Arbeit mit digitalen Objekten dient.

Im zweiten Vortrag “Lamento – Latrine – Leyptzigk. Entitätenbasierte Inhaltssuche in verteilten Ressourcen” ([Folien](https://zenodo.org/records/13255592)) stellten Vertreter der Sächsischen Akademie der Wissenschaften zu Leipzig (Thomas Eckart, Felix Helfer, Uwe Kretschmer, Erik Körner) die aktuell im Rahmen von Text+ entwickelte Erweiterung der Föderierten Inhaltssuche (FCS) durch Entitäten-basierte Suchverfahren vor.

Dabei wurde anhand konkreter Fragestellungen auf Basis realer Wörterbücher, Korpora und Editionen die Bedeutung von zentralen Wissensbasen wie der Gemeinsamen Normdatei (GND), GeoNames, Wikidata und weiteren für moderne Forschungsfragen und für die Vernetzung in modernen Forschungsdateninfrastrukturen illustriert. Die Vorstellung aktueller Arbeiten an der SAW zu Verfahren des Entity Linkings verdeutlichten die Bandbreite möglicher Ansätze zur Erschließung von Ressourcen durch semi-automatische Entity-Annotation (u.a. unter Nutzung großer Sprachmodelle) und ging dabei auch auf die Arbeit der entsprechenden Text+ Taskforce ein.

Im Anschluss wurde das Thema der Nutzbarmachung vorhandener Annotationen im Kontext föderierter Such- und Recherchedienste in Text+ in den Vordergrund gestellt. Anhand der aktuell durchgeführten Erweiterung der etablierten Föderierten Inhaltssuche, die der benutzerfreundlichen Suche in Forschungsdaten dient, wurde durch entsprechende Anfrage- und Präsentationsmöglichkeiten für verschiedenste Formen von Normdaten die nahtlose Integration in bestehende Benutzeroberflächen demonstriert. Diverse Live-Demonstrationen auf Basis des aktuellen Entwicklungsstandes rundeten den Vortrag ab.

Die an den Vortragsteil anschließende Diskussion eröffnete den Austausch, der beim nachfolgenden Grillen im Akademiegarten in lockerer Atmosphäre weitergeführt wurde.

Weitere Informationen zur Veranstaltung: https://fdhl.info/opengarden2024/ bzw. https://www.saw-leipzig.de/de/aktuelles/digital-humanities-open-garden-2024.


### Text+ Zwischenbericht

Ende September wurde der Text+ Zwischenbericht fristgerecht bei der DFG eingereicht, entsprechend deren strengen Vorgaben (max. 25 Seiten, mit einer vorgegebenen Gliederung basierend auf Leitfragen). Der Bericht umfasst drei Teile:

* einen öffentlichen Teil, den die DFG auf ihrer Website veröffentlicht,
* einen internen Teil mit 25 Seiten
* und ein Datenblatt mit Indikatoren zu Text+, das über eine Umfrage von allen Kooperationspartnern erstellt wurde.

Der öffentliche Berichtsteil ist bereits auf der DFG-Webseite verfügbar und kann dort nachgelesen werden ([Link zum Dokument](https://www.dfg.de/resource/blob/344884/5f73cb9c50d6f6d5d7ee529ff4f96651/text--data.pdf)).

## Termine
Alle Veranstaltungen - sowohl kommende als auch bereits stattgefundene - finden Sie auch in unserer [Veranstaltungsrolle im Text+ Portal.](https://text-plus.org/aktuelles/veranstaltungen/).


{{< rawhtml >}}
    <style>
        tbody tr:nth-child(even) {
        background-color: #ededed;
        color: #000;
        }
        
        tbody tr:nth-child(even) td:first-child {
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
        }
        
        tbody tr:nth-child(even) td:last-child {
        border-top-right-radius: 4px;
        border-bottom-right-radius: 4px;
        }
        
        td {
        padding: 0 5px 0 0;
        }
    </style>
<table>
    <tr>
        <th width="25%">Datum</th>
        <th width="60%">Event</th>
        <th width="15%">Ort</th>
    </tr>
    <tr valign="top">
        <td>30. Oktober 2024</td>
        <td>NFDI und Spezialbibliotheken im Gespräch – eine Umfrage des NFDI Konsortiums Text+ zu Katalogdaten von Bibliotheken</td>
        <td><a href="https://aspb.de/angebote/workshops/nfdi/" target="_blank">
            virtuell
        </a></td>
    </tr>
    <tr valign="top">
        <td>31. Oktober 2024</td>
        <td>Text+ Research Rendezvous</td>
        <td><a href="https://text-plus.org/aktuelles/veranstaltungen/2024-10-31-research-rendezvous/" target="_blank">
            virtuell
        </a></td>
    </tr>
    <tr valign="top">
        <td>06. November 2024</td>
        <td>IO-Lecture: Migration von RocketChat zu Matrix</td>
        <td><a href="https://events.gwdg.de/event/979/" target="_blank">
            virtuell
        </a></td>
    </tr>
    <tr valign="top">
        <td>12. November 2024</td>
        <td>Text+ Research Rendezvous</td>
        <td><a href="https://text-plus.org/aktuelles/veranstaltungen/2024-11-12-research-rendezvous/" target="_blank">
            virtuell
        </a></td>
    </tr>
    <tr valign="top">
        <td>14. November 2024</td>
        <td>Erschließen, Forschen, Analysieren</td>
        <td><a href="https://text-plus.org/aktuelles/veranstaltungen/2024-11-14-efa/" target="_blank">
            virtuell
        </a></td>
    </tr>
    <tr valign="top">
        <td>18./19. November 2024</td>
        <td>
            <a href="https://events.gwdg.de/event/957/" target="_blank">
                Digitale Wörterwelten: Einblick in die Text+ Infrastruktur
            </a>
        </td>
        <td>Berlin</td>
    </tr>
 <tr valign="top">
        <td>20./21. November 2024</td>
        <td>
            <a href="https://events.gwdg.de/event/658/" target="_blank">
                1st Base4NFDI User Conference (UC4B2024)
            </a>
        </td>
        <td>Berlin</td>
    </tr>    
    <tr valign="top">
        <td>27. November 2024</td>
        <td>
            <a href="https://events.gwdg.de/event/960/">
                5. FID / Text+ Jour Fixe - Verzeichnen und Ablegen
            </a>
        </td>
        <td>SUB Göttingen</td>
    </tr>
    <tr valign="top">
        <td>28. November 2024</td>
        <td>Text+ Research Rendezvous</td>
        <td><a href="https://text-plus.org/aktuelles/veranstaltungen/2024-11-28-research-rendezvous/" target="_blank">
            virtuell
        </a></td>
    </tr>
        <tr valign="top">
        <td>04. Dezember 2024</td>
        <td>IO-Lecture: Wie kommt mein Dienst ins Portal?</td>
        <td><a href="https://events.gwdg.de/event/980/" target="_blank">
            virtuell
        </a></td>
    </tr>
    </tr>
        <tr valign="top">
        <td>04. Dezember 2024</td>
        <td>Verknüpfung und Kontextualisierung: Die Gemeinsame Normdatei als ein PID-System für Kulturelle Objekte in GLAM-Institutionen</td>
        <td><a href="https://www.pid-network.de/neuigkeiten/veranstaltungen/pids-fuer-kulturelle-objekte-glam" target="_blank">
            virtuell
        </a></td>
    </tr>
    <tr valign="top">
        <td>10. Dezember 2024</td>
        <td>Text+ Research Rendezvous</td>
        <td><a href="https://text-plus.org/aktuelles/veranstaltungen/2024-12-10-research-rendezvous/" target="_blank">
            virtuell
        </a></td>
    </tr>
    <tr valign="top">
        <td>10. Dezember 2024</td>
        <td>GND-Forum NFDI & Co.</td>
            <td><a href="https://events.gwdg.de/event/921/" target="_blank">
            virtuell
        </a></td>
    </tr>
</table>
{{< /rawhtml >}}

## Archiv
* [1. Newsletter vom 30.04.2024](https://text-plus.org/aktuelles/aktuelle-infos/posts/newsletter_01/)
* [2. Newsletter vom 15.07.2024](https://text-plus.org/aktuelles/aktuelle-infos/posts/newsletter_02/)