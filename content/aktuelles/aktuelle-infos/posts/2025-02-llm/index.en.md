---
title: "LLM Service"
featured_image: llm.png
type: news
date: 2025-02-05
---

{{<image img="llm.png" license="Foto: Screenshot Text+, CC-BY 4.0" alt="LLM Service"/>}}

# Text+ LLM Service Now Online  
The text- and language-based humanities offer a wide range of use cases for large language models (LLMs) through their research data. Text+ expands access to such research data via the Registry, the Federated Content Search (FCS), and the data centers and repositories of contributing partners.  

Now, a [web service is also available for the open-source LLMs (Meta) LLaMA, Mixtral, Qwen, and Codestral, as well as OpenAI’s ChatGPT](https://text-plus.org/daten-dienste/llm_service/). This service is made possible by GWDG, which, as a National High-Performance Computing and AI Center, supports the development and testing of research-related AI applications within Text+.  

## Who Can Use the LLM Service and How?  
Initially, all individuals directly involved in the project can access the service after logging in via Academic Cloud. An expansion of the user base is planned, though currently subject to licensing restrictions. Apart from the externally integrated ChatGPT models, all LLMs are hosted on GWDG servers, ensuring that no data is transferred externally.  

## Benefits  
- Free use of various open-source models  
- Free access to OpenAI GPT-4  
- AI chat without server-side chat history storage (except for ChatGPT)  
- Managed hosting of custom language models  
- Fine-tuning of LLMs on proprietary data  
- Retrieval-Augmented Generation (RAG) on personal documents  
- Compliance with legal regulations, particularly regarding user data protection