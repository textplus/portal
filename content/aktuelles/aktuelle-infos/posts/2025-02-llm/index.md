---
title: "LLM Service online"
featured_image: llm.png
type: news
date: 2025-02-05
---

{{<image img="llm.png" license="Foto: Screenshot Text+, CC-BY 4.0" alt="LLM Service"/>}}

# Text+ LLM-Service online
Mit ihren Forschungsdaten bieten die text- und sprachbasierten Geisteswissenschaften vielfältige Anwendungsfälle für den Einsatz großer Sprachmodelle (Large Language Models, LLMs). Text+ erweitert den Zugang zu solchen Forschungsdaten über die Registry, die Federated Content Search (FCS) sowie über die Datenzentren und Repositorien der beitragenden Partner.

Hinzu kommt nun die Bereitstellung eines [Web-Services für die Open Source LLMs (Meta) LLaMA, Mixtral, Qwen und Codestral sowie Chat-GPT von OpenAI](https://text-plus.org/daten-dienste/llm_service/). Dies wird durch die GWDG ermöglicht, die als Nationales Hochleistungsrechen- und KI-Zentrum die Entwicklung und Erprobung forschungsbezogener KI-Anwendungsfälle in Text+ unterstützt.

## Wer kann den LLM Service wie nutzen?
Vorerst alle unmittelbar am Projekt beteiligten Personen nach einem Login per Academic Cloud. Eine Erweiterung der User Base ist geplant, hat jedoch vorerst lizenzrechtliche Grenzen. Außer den beiden extern eingebundenen Chat-GPT Modellen werden die LLMs auf Servern der GWDG gehostet, so dass keine Daten nach extern abfließen.

## Vorteile
- Kostenlose Nutzung von diversen Open-Source-Modellen
- Kostenlose Nutzung von OpenAI GPT-4
- KI-Chat ohne serverseitige Speicherung des Chatverlaufs (außer Chat-GPT)
- Managed Hosting eigener Sprachmodelle
- Finetuning von LLMs auf eigenen Daten
- Retrieval-Augmented Generation (RAG) auf eigenen Dokumenten
- Einhaltung gesetzgeberischer Vorgaben und insbesondere auch der Datenschutzinteressen der Nutzenden