---

title: Solidaritätserklärung
featured_image: Ukraine-Solidaritaet-2048x480.png
type: news
date: 2022-03-16

---
# Solidaritätserklärung

## DE
**Text+ zur Situation in der Ukraine**

Die Beteiligten an Text+ sind fassungslos und entsetzt in Anbetracht des Krieges in der Ukraine. Viele von uns arbeiten seit Jahren mit Kolleginnen und Kollegen aus der Ukraine und Russland zusammen. Wir fühlen mit ihnen. Wir sind solidarisch mit den Menschen, denen derzeit Freiheit, Frieden und ein selbstbestimmtes Leben nicht vergönnt sind. Menschen, die sich stattdessen in Lebensgefahr sehen und Kämpfen ausgesetzt sind.
Den Menschen in der Ukraine gilt unsere uneingeschränkte Solidarität. Auch den Menschen in Russland, die sich gegen den Krieg stellen und dadurch Repressionen ausgesetzt sind, zollen wir Respekt. Als Forschende, die im Bereich der Sprache und Kultur engagiert sind, unterstützen wir die Stellungnahme der Allianz der Wissenschaftsorganisationen. Die Allianz sieht in diesem „kriegerischen und völkerrechtswidrigen Angriff Russlands auf die Ukraine“ einen „Angriff auf elementare Werte der Freiheit, Demokratie und Selbstbestimmung, auf denen Wissenschaftsfreiheit und wissenschaftliche Kooperationsmöglichkeiten basieren“.

https://www.leibniz-gemeinschaft.de/ueber-uns/neues/forschungsnachrichten/forschungsnachrichten-single/newsdetails/solidaritaet-mit-der-ukraine

Viele an Text+ beteiligten Institutionen arbeiten daran, Forschende und andere Menschen, die aus der Ukraine fliehen, zu unterstützen. Betroffene Forschende aus der Ukraine, insbesondere aus unseren Fachgebieten der Geistes- und Kulturwissenschaften, möchten wir ganz direkt helfen, damit sie ihre Arbeit in einem sicheren Umfeld fortführen können. Nehmen Sie Kontakt mit uns auf, entweder als Direktnachricht auf Twitter (https://twitter.com/Textplus_NFDI ) oder per E-Mail unter office@text-plus.org.

Daneben unterstützen wir auch die Aktivitäten der Nationalen Forschungsdateninfrastruktur, die Informationen für Forschende aus der Ukraine zusammenträgt:
https://www.nfdi.de/wichtige-links-fuer-wissenschaftlerinnen-aus-der-ukraine/

## UK
**Текст+ про ситуацію в Україні**

Учасники консорціуму Text+ вражені війною в Україні. Багато хто з нас роками працює з колегами з України та Росії, яким ми зараз співчуємо. Ми солідарні з людьми, які у цей час позбавлені свободи, миру та звичайного життя. З людьми, які натомість перебувають у смертельній небезпеці та щодня вимушені боротися за своє життя. Ми цілком солідарні з народом України. Ми також шануємо росіян, які виступають проти війни і тому зазнають репресій.
Як науковці, які працюють з мовою та культурою, ми підтримуємо заяву Альянсу наукових організацій. Альянс вбачає у цьому «військовому нападі Росії на Україну» «напад на елементарні цінності свободи, демократії та самовизначення, на яких засновані академічна свобода та можливості для академічної співпраці».

https://www.leibniz-gemeinschaft.de/ueber-uns/neues/forschungsnachrichten/forschungsnachrichten-single/newsdetails/solidaritaet-mit-der-ukraine

Багато інститутів та установ, що беруть участь у проекті Text+, працюють над підтримкою дослідників та інших людей, які вимушені залишити Україну. Ми хотіли б надати пряму допомогу постраждалим науковцям та дослідникам з України, особливо з областей гуманітарних наук та культурології, щоб вони могли продовжити свою роботу у безпечному середовищі. Зв’яжіться з нами, або написавши в директ нашого Twitter https://www.nfdi.de/important-links-for-scientists-from-ukraine/?lang=en (https://twitter.com/Textplus_NFDI) або за наступною адресою office@text-plus.org.
Крім того, ми також підтримуємо діяльність Національної інфраструктури дослідницьких даних, яка збирає інформацію для дослідників з України: https://www.nfdi.de/wichtige-links-fuer-wissenschaftlerinnen-aus-der-ukraine/

## RU
**Текст+ о ситуации в Украине**
Участники консорциума Text+ потрясены войной в Украине. Многие из нас годами работают с коллегами из Украины и России, которым мы сочувствуем в данный момент. Мы солидарны с людьми, которым в настоящее время не дарованы свобода, мир и обычная жизнь. С людьми, которые вместо этого находятся в смертельной опасности и каждый день борются за свою жизнь.
Мы полностью солидарны с народом Украины. Мы также отдаем дань уважения россиянам, которые выступают против войны и поэтому подвергаются репрессиям.

Как исследователи, работающие с языком и культурой, мы поддерживаем заявление Альянса научных организаций. Альянс усматривает в этом «военном нападении России на Украину» «нападение на элементарные ценности свободы, демократии и самоопределения, на которых основаны академическая свобода и возможности для академического сотрудничества».

https://www.leibniz-gemeinschaft.de/ueber-uns/neues/forschungsnachrichten/forschungsnachrichten-single/newsdetails/solidaritaet-mit-der-ukraine

Многие институты и учреждения, участвующие в проекте Text+, работают над поддержкой исследователей и других людей, покидающих Украину. Мы хотели бы оказать прямую помощь пострадавшим ученым и исследователям из Украины, особенно из областей гуманитарных наук и культурологии, чтобы они могли продолжить свою работу в безопасной среде. Свяжитесь с нами, либо написав в директ нашего Twitter https://www.nfdi.de/important-links-for-scientists-from-ukraine/?lang=en
(https://twitter.com/Textplus_NFDI) либо по следующему адресу office@text-plus.org.

Кроме того, мы также поддерживаем деятельность Национальной инфраструктуры научных данных, которая собирает информацию для исследователей из Украины:
https://www.nfdi.de/wichtige-links-fuer-wissenschaftlerinnen-aus-der-ukraine/

## EN
**Text+ on The Situation in Ukraine**
Those involved in Text+ are stunned and horrified in light of the war in Ukraine. Many of us have been working with colleagues from Ukraine and Russia for years. We feel for them. We stand in solidarity with the people who are currently denied freedom, peace and a self-determined life. People who instead find themselves in mortal danger and are exposed to fighting.
We show our full solidarity with the people in Ukraine. We also pay respect to the people in Russia who oppose the war and are exposed to repression as a result. As researchers engaged in the field of language and culture, we support the statement of the Alliance of Science Organisations, which sees this „belligerent attack by Russia on Ukraine in violation of international law“ as an „attack on elementary values of freedom, democracy and self-determination, on which scientific freedom and scientific cooperation opportunities are based“.

https://www.leibniz-gemeinschaft.de/ueber-uns/neues/forschungsnachrichten/forschungsnachrichten-single/newsdetails/solidaritaet-mit-der-ukraine

Many institutions involved in Text+ are working to support researchers and others fleeing Ukraine. Concerned researchers from Ukraine, especially from our fields of the Humanities, we would like to help very directly so that they can continue their work in a safe environment. Contact us either by a direct message on Twitter (https://twitter.com/Textplus_NFDI) or by email at office@text-plus.org.
In addition, we also support the activities of the National Research Data Infrastructure, which compiles information for researchers from Ukraine:
https://www.nfdi.de/wichtige-links-fuer-wissenschaftlerinnen-aus-der-ukraine/