---

title: Text+ Kooperationsprojekte Ausschreibungsrunde 2024 Forschungsdaten aufbereiten und sichern gemeinsam mit Text+
type: news
date: 2024-02-21

---
# Text+: Ausschreibungsrunde 2024: Forschungsdaten aufbereiten und sichern gemeinsam mit Text+
Text+ ist Teil der Nationalen Forschungsdateninfrastruktur (NFDI) mit dem Ziel, eine ortsverteilte Forschungsdateninfrastruktur für geisteswissenschaftliche Forschungsdaten aufzubauen. Der Fokus liegt dabei auf den Datenbereichen digitale Sammlungen, lexikalische Ressourcen und Editionen.

Ziel dieser Ausschreibung ist es, die Angebote an Daten und Services von Text+ kontinuierlich zu erweitern und für die Community der Forschenden langfristig verfügbar zu machen. Es ist auch möglich, dass Anträge gestellt werden, die bereits in Text+ vorhandene Angebote an Daten und Diensten in einer beispielhaften bzw. für das Text+ Angebotsportfolio nachnutzbaren Weise verwenden. Dazu werden auf maximal ein Kalenderjahr befristete Projekte gefördert, deren Ergebnisse in die Text+ Infrastruktur integriert werden (Siehe FAQ zur Integration von Daten und Diensten in Text+). Dies kann z. B. dadurch geschehen, dass im Rahmen dieser Projekte vorhandene Daten so aufbereitet werden, dass sie durch ein bereits an die Text+ Infrastruktur angebundenes Datenzentrum für die Community (dauerhaft) verfügbar werden. Alternativ können eigene institutionalisierte und nachhaltige Datenzentren mit Schnittstellen in die technische Infrastruktur von Text+ eingebunden werden, um die betreffenden Daten in der Text+ Infrastruktur verfügbar zu machen. Bei Software und anderen Services wird erwartet, dass das Ergebnis unter einer offenen Lizenz bereitgestellt wird, mit Text+ abgestimmt ist (Leistung eines Kohärenzbeitrags) und langfristig für die Infrastruktur betrieben werden kann. Die Bereitstellung entsprechend den FAIR- und CARE-Prinzipien ist dabei wesentlich. Nach den Richtlinien der NFDI sind nur Arbeiten förderfähig, die auf die Integration in das Angebotsportfolio von Text+ abzielen. Aufbau und Ausbau von Ressourcen selbst sind hingegen aus Eigenmitteln zu tragen.

Förderanträge können für eine der Datendomänen Sammlungen, lexikalische Ressourcen, Editionen sowie für die Task Area Infrastruktur/Betrieb gestellt werden (vgl. hierzu u.a. die Beschreibung der Domänen unter https://text-plus.org). Zur Vorbereitung der Anträge sollte ein Beratungsgespräch mit dem zuständigen Bereich erfolgen. Die Liste der Ansprechpersonen finden Sie am Ende dieser Ausschreibung.

Im Rahmen dieser Ausschreibungen können mehrere Projekte in der Regel mit einem Volumen zwischen 35.000 und 65.000 EUR gefördert werden. Zusätzlich wird auf die Projektsumme ein Overhead von 22% gewährt. Die Laufzeit der Projekte ist an das Kalenderjahr 2025 gebunden und beträgt dadurch maximal 12 Monate; eine Übertragung von nicht ausgegebenen Mitteln auf das Folgejahr ist nicht möglich. Nach Abschluss der Förderung wird ein Projektbericht erwartet, der auf der Text+ Webseite veröffentlicht werden soll.

Rückfragen können Sie gerne an office@text-plus.org adressieren. Weitere Informationen zum Antragsverfahren und zu den bisher bewilligten Projekten werden auf der [Text+ Webseite](https://text-plus.org/vernetzung/kooperationsprojekte/) bereitgestellt.

