---

title: Text+ Plenary 2022
featured_image: Banner-Website-Plenary-2022.jpg
type: news
date: 2022-05-17

---
# Text+ Plenary 2022

Am 12. und 13 September 2022 fand das Text+ Plenary in Mannheim statt. Die Anmeldung ist geschlossen. Informationen unter https://events.gwdg.de/event/269