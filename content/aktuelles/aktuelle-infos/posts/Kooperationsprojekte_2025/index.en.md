---

title: Text+ Call for Proposals 2025 Processing and Securing Research Data in collaboration with Text+
type: news
date: 2025-02-21

---

# Call for Proposals 2025: Processing and Securing Research Data in collaboration with Text+

Text+ is part of the German National Research Data Infrastructure (NFDI) with the goal of establishing a distributed research data infrastructure for humanities research data. At the current stage, it is focused on the data domains of digital collections, lexical resources, and editions.

The aim of this announcement is to continuously expand the offerings of data and services provided by Text+ and make them available to the research community in the long term. Alternatively, applications can be submitted that utilise the data and services available in Text+ specifically for innovative research questions. Likewise, own institutionalised and sustainable data centres with interfaces can be integrated into the technical infrastructure of Text+ in order to make the relevant data permanently available in the Text+ infrastructure. 

Applications for funding can be submitted for one of the Task Areas: Collections, Lexical Resources, Editions, and the Task Area Infrastructure/Operation (see the description of the domains at https://text-plus.org). 

Further information on the application process and previously approved projects will be provided on https://text-plus.org/en/vernetzung/kooperationsprojekte. 
