---

title: Text+ Kooperationsprojekte Ausschreibungsrunde 2025 Forschungsdaten aufbereiten und sichern gemeinsam mit Text+
type: news
date: 2025-02-21

---

# Text+: Ausschreibungsrunde 2025: Forschungsdaten aufbereiten und sichern gemeinsam mit Text+

Text+ ist Teil der Nationalen Forschungsdateninfrastruktur (NFDI) mit dem Ziel, eine ortsverteilte Forschungsdateninfrastruktur für geisteswissenschaftliche Forschungsdaten aufzubauen. Der Fokus liegt dabei auf den Datenbereichen digitale Sammlungen, lexikalische Ressourcen und Editionen. 

Ziel dieser Ausschreibung ist es, die Angebote an Daten und Services von Text+ kontinuierlich zu erweitern und für die Forschungscommunity langfristig verfügbar zu machen. Alternativ können Anträge gestellt werden, die die in Text+ verfügbaren Daten und Services gezielt für innovative Forschungsfragen nutzen. Ebenso können eigene institutionalisierte und nachhaltige Datenzentren mit Schnittstellen in die technische Infrastruktur von Text+ eingebunden werden, um die betreffenden Daten in der Text+ Infrastruktur dauerhaft verfügbar zu machen. 

Förderanträge können für eine der Datendomänen Collections, lexikalische Ressourcen, Editionen sowie für die Task Area Infrastruktur/Betrieb gestellt werden.

Weitere Informationen zum Antragsverfahren und zu den bisher bewilligten Projekten finden sich unter https://www.text-plus.org/forschungsdaten/kooperationsprojekte. 
