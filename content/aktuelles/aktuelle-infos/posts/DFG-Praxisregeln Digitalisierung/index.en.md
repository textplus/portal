---
title: DFG Guidelines on Digitisation – Updated Version 2022 Published on Zenodo
featured_image: Screenshot-2023-03-13-at-12.19.08.png
type: news
date: 2023-03-13 11:13:00
---

# DFG Guidelines on “Digitisation” – Updated Version 2022 Published on Zenodo

Since mid-February, the updated version of the DFG (German Research Foundation) guidelines on “Digitalization” has been available online:

[https://doi.org/10.5281/zenodo.7435724](https://doi.org/10.5281/zenodo.7435724)

Text+ played a significant role in coordinating this update in collaboration with NFDI4Culture.

The DFG guidelines on “Digitisation” serve as a fundamental framework for DFG-funded projects in the [“Digitisation and Development” program](https://www.dfg.de/en/research-funding/funding-opportunities/programmes/infrastructure/lis/funding-opportunities/digitisation-cataloguing). These guidelines establish standards and provide information on all organizational, methodological, and technical aspects related to the digitisation and development of objects of material cultural heritage. They make a significant contribution to the sustainability, accessibility, and compatibility of research data infrastructure.

The published document serves as a starting point for the upcoming material-specific differentiation of the guidelines by the communities. NFDI4Culture, NFDI4Memory, NFDI4Objects, and Text+ jointly serve as contact points and platforms for the next steps in this process. The guidelines on "Digitisation" are a crucial component in shaping standards within their respective research communities. All communities and institutions engaged in the digitalization and development of research-relevant objects are encouraged to contribute their expertise to this process of redesign.
