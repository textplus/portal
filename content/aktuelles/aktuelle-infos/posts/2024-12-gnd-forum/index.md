---
title: "Großes Interesse am GND-Forum NFDI, FIDs & Co."
featured_image: gnd_rgb.png
type: news
date: 2024-12-16
---

{{<image img="gnd_rgb.png" license="Foto: Gemeinsame Normdatei" alt="GND"/>}}

# Großes Interesse am GND-Forum NFDI, FIDs & Co. am 10. Dezember 2024

{{<subhead>}}Einmal quer Beet - Projektübergreifende Eindrücke zu Arbeiten rund um die GND{{</subhead>}}

##
Auch wenn sich noch weitaus mehr Menschen für das GND Forum NFDI, FID & Co am 10. Dezember 2024 angemeldet hatten, so waren wir von den tatsächlich teilnehmenden 250 Menschen wirklich sehr angetan. Für die Teilnehmenden hatte das Team der Niedersächsischen Staats- und Universitätsbibliothek Göttingen und der Arbeitsstelle für Standardisierung der Deutschen Nationalbibliothek aus den Communities der geisteswissenschaftlichen Konsortien der Nationalen Forschungsdateninfrastruktur (NFDI), den Fachinformationsdiensten der Wissenschaften und weiteren Forschungsvorhaben einen Strauß an Vorträgen zu einem dichten Programm zusammengestellt.

Das GND Forum NFDI, FID & Co lud so alle Interessierten ein, sich über aktuelle Vorhaben zur Gemeinsamen Normdatei (GND) im deutschsprachigen Raum zu informieren. Da es sich im Wesentlichen um eine Informationsveranstaltung handelte, liegt der Schwerpunkt der Dokumentation auf der Bereitstellung der Folien zu den neun Vorträgen. Die Vorträge behandelten die unterschiedlichen Aspekte der Arbeit mit der GND aus verschiedenen Perspektiven und stellten die Rolle der GND für die unterschiedlichen Communities vor. Die meisten Vorträge kann man gut mit der Kategorie ”Werkstattbericht” beschreiben. Sie waren so heterogen in Inhalt und vorausgesetztem Wissen, wie das Publikum selbst. Der neunte Vortrag rundete das Programm mit einer Präsentation einer Pilotanwendung für eine KI-gestützte Inhaltserschließung ab. Untenstehend das vollständige Programm.

{{<image img="programm.png" license="Foto: SUB Göttingen" alt="Programm des GND-Forums"/>}}

[Einen ausführlichen Bericht und Links zu allen Vorträgen enthält der im GND-Blog veröffentlichte Beitrag.](https://wiki.dnb.de/x/rrMtFw)
