---
title: "Great Uptake for the GND-Forum NFDI, FIDs & Co."
featured_image: gnd_rgb.png
type: news
date: 2024-12-16
---

{{<image img="gnd_rgb.png" license="Foto: Gemeinsame Normdatei" alt="GND"/>}}

# Great Uptake for the GND-Forum NFDI, FIDs & Co. December 10th, 2024

{{<subhead>}}A Bit of Everything – Cross-Project Insights into Work Surrounding the GND{{</subhead>}}

##
Even though many more people had registered for the GND Forum NFDI, FID & Co on December 10, 2024, we were truly impressed by the 250 participants who actually attended. For the attendees, the team from the Göttingen State and University Library of Lower Saxony and the Office for Standardization of the German National Library had put together a rich program featuring a variety of presentations from the communities of the humanities consortia of the National Research Data Infrastructure (NFDI), the specialized information services for science, and other research projects.

The GND Forum NFDI, FID & Co invited all interested parties to learn about current initiatives related to the Integrated Authority File (GND) in the German-speaking region. Since the event was primarily informational, the documentation focuses on providing the slides from the nine presentations. These presentations covered different aspects of working with the GND from various perspectives and highlighted its role for the different communities. Most of the presentations can be aptly described as "workshop reports." They were as diverse in content and assumed knowledge as the audience itself. The ninth presentation rounded off the program with a demonstration of a pilot application for AI-assisted content indexing. The full program can be found below.

{{<image img="programm.png" license="Foto: SUB Göttingen" alt="Program of the GND-Forum"/>}}

[A detailed review of the forum including links to all presentations has been published in the GND-Blog.](https://wiki.dnb.de/x/rrMtFw)
