---
title: "New Learning Resources on the Integrated Authority File (GND) – Promoting and Sharing Knowledge"
featured_image: gnd_logo.png
type: news
date: 2025-02-12
---

The *PID Network Germany* Standardization Office has published two accessible and interactive e-learning modules on the GND, funded by the German Research Foundation (DFG). Both modules are Open Educational Resources (OER) and aim to provide practical knowledge of the GND in a clear and sustainable way:

## Learning Module 1: "Einfach normiert – Mit der GND die Qualität von Sammlungsdaten verbessern"

This module is aimed at beginners and provides fundamental information about the GND, its benefits, and how to use the GND search. Accompanied by the fictional persona Dr. Anna Mayer, learners will learn how to use the GND to enrich their collection data in museums, archives, or research projects. The module includes, among other things, a quiz to reinforce the learned content.

{{<image img="screenshot_gnd_lerneinheit_1.png" license="Picture: Screenshot GND Learning Module 1, CC-BY 4.0" alt="GND Learning Resources"/>}}

[Click here](https://c18004-vod.l.core.cdn.streamfarm.net/18004initag/ondemand/app2080931841/gnd/DNB_modul1_einfach_normiert_de/layout/layout_bundesverwaltung_v002/frames.htm) for Module 1.

## Learning Module 2: "Mitarbeiten in der GND – Einführung in die kooperative Pflege von Normdaten in der GND”

This module is suitable for advanced users who are already familiar with the GND and want to become more active. It focuses on how to create new records and incorporate personal needs regarding formats and data schemas into the GND. Learners accompany the fictional persona Beate Batic and explore the organization of the GND and the eligibility criteria for including records. A final quiz assesses the knowledge gained.

{{<image img="screenshot_gnd_lerneinheit_2.png" license="Picture: Screenshot GND Learning Module 2, CC-BY 4.0" alt="GND Learning Resources"/>}}

[Click here](https://c18004-vod.l.core.cdn.streamfarm.net/18004initag/ondemand/app2080931841/gnd/DNB_modul2_anwendende_de/layout/layout_bundesverwaltung_v002/frames.htm) for Module 2.

Both modules feature a voice-over provided by an AI, allowing for easy adjustments and updates.