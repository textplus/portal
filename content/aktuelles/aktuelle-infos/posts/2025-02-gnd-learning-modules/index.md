---
title: "Neue Lernressourcen zur Gemeinsamen Normdatei (GND) – Wissen fördern und teilen"
featured_image: gnd_logo.png
type: news
date: 2025-02-12
---

Die Arbeitsstelle für Standardisierung im Projekt *PID Network Deutschland* hat mit Fördermitteln der Deutschen Forschungsgemeinschaft (DFG) zwei barrierefreie und interaktive E-Learning-Module zur GND veröffentlicht. Beide Module sind Open Educational Resources (OER) und zielen darauf ab, Wissen zur GND in der Praxis verständlich und nachhaltig zu vermitteln: 

## Lernmodul 1: "Einfach normiert – Mit der GND die Qualität von Sammlungsdaten verbessern"

Dieses Modul richtet sich an Einsteigende und vermittelt grundlegende Informationen über die GND, ihre Vorteile sowie die Nutzung der GND-Suche. Begleitet von der fiktiven Persona *Dr. Anna Mayer* erfahren die Lernenden, wie sie die GND zur Anreicherung ihrer Sammlungsdaten in Museen, Archiven oder Forschungsprojekten einsetzen können. Das Modul beinhaltet unter anderem ein Quiz zur Vertiefung des Gelernten.

{{<image img="screenshot_gnd_lerneinheit_1.png" license="Foto: Screenshot GND Lernmodul 1, CC-BY 4.0" alt="GND Lernressourcen"/>}}

[Hier](https://c18004-vod.l.core.cdn.streamfarm.net/18004initag/ondemand/app2080931841/gnd/DNB_modul1_einfach_normiert_de/layout/layout_bundesverwaltung_v002/frames.htm) entlang zum Modul 1.

## Lernmodul 2: "Mitarbeiten in der GND – Einführung in die kooperative Pflege von Normdaten in der GND”

Dieses Modul eignet sich für fortgeschrittene Nutzende, die bereits mit der GND vertraut sind und nun aktiver mitwirken möchten. Es geht darum, wie man neue Datensätze anlegt und eigene Bedürfnisse hinsichtlich Formaten und Datenschemata in die GND einbringt. Die Lernenden begleiten die fiktive Persona *Beate Batic* und beschäftigen sich mit der Organisation der GND und den Eignungskriterien für die Aufnahme von Datensätzen. Ein abschließendes Quiz prüft das erlernte Wissen.

{{<image img="screenshot_gnd_lerneinheit_2.png" license="Foto: Screenshot GND Lernmodul 2, CC-BY 4.0" alt="GND Lernressourcen"/>}}

[Hier](https://c18004-vod.l.core.cdn.streamfarm.net/18004initag/ondemand/app2080931841/gnd/DNB_modul2_anwendende_de/layout/layout_bundesverwaltung_v002/frames.htm) entlang zum Modul 2.

Beide Module wurden mit einer KI-Stimme vertont, was eine einfache Anpassung und Aktualisierung ermöglicht.