---
title: Text+ Blogs!
featured_image: Logo-Text-only-2048x714.png
type: news
date: 2022-11-07
---

# Text+ Blogs!

The time has finally come: Text+ now has its own blog: [textplus.hypotheses.org](https://textplus.hypotheses.org/). After the soft launch of the blog in September 2022 at the [Plenary Assembly](https://events.gwdg.de/event/269/timetable/), the blog officially goes live today. The blog focuses on topics related to scholarly work with text- and language-based research data: whether it's in your own research, research data management, on the IT side, or in teaching to prepare students and postgraduates for these fields of work.

In terms of content, it covers the current activities within the consortium, the individuals and institutions involved, collaborations, and projects. In short, it provides a behind-the-scenes look at a research data infrastructure that is still in the making. The blog complements other communication channels of Text+, such as the [Twitter account](https://twitter.com/Textplus_NFDI) and the [Zenodo Community](https://zenodo.org/communities/textplus_nfdi/). During the opening week, new posts will be added every day, and in the future, we plan to post at least once a month.

The contributions primarily come from members of the Text+ consortium. Guests are invited to contribute posts from an external or collaborative perspective. Blogging is primarily in German, but other languages are also possible and explicitly welcomed.

The Text+ Blog offers the opportunity to:
- Present both (interim) results and things that are still work in progress
- Initiate discussions
- Showcase best practices
- Provide tips and solutions
- Raise questions
- Take a stance
- Highlight events, calls, and publications
- Promote favorite tools
- Share interesting resources
- Increase visibility
- Promote networking
- And much more ...

The editorial team starts with a 10-member editorial team and welcomes new additions. The specific roles and responsibilities can be determined individually. The editorial team sees itself not as gatekeepers but as enablers: The blog thrives on the activities of the community!