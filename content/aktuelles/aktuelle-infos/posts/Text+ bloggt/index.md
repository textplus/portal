---
title: Text+ bloggt!
featured_image: Logo-Text-only-2048x714.png
type: news
date: 2022-11-07
---
# Text+ bloggt!

Nun ist es endlich soweit: Text+ hat ein eigenes Blog: [textplus.hypotheses.org](https://textplus.hypotheses.org/). Nach dem Soft-Launch des Blogs im September 2022 auf der [Plenarversammlung](https://events.gwdg.de/event/269/timetable/) gehen das Blog heute offiziell online. Das Blog beschäftigt sich mit Themen rund um die wissenschaftlichen Arbeit mit text- und sprachbasierten Forschungsdaten: Sei es in der eigenen Forschung, im  Forschungsdatenmanagement, auf IT-Seite oder in der Lehre, um Studierende und Postgraduierte auf diese Arbeitsfelder vorzubereiten.

Inhaltlich geht es um die aktuellen Aktivitäten innerhalb des Konsortiums, um beteiligte Personen und Institutionen, um Kooperationen und Projekte. Kurzum: Es geht um einen Blick hinter die Kulissen, darum, eine Forschungsdateninfrastruktur, die noch im Entstehen begriffen ist, sichtbar zu machen. Das Blog ergänzt damit andere Kommunikationskanäle von Text+ wie den [Twitteraccount](https://twitter.com/Textplus_NFDI) und die [Zenodo-Community](https://zenodo.org/communities/textplus_nfdi/). In der Eröffnungswoche kommen jeden Tag neue Beiträge hinzu, künftig werden wir mindestens einmal im Monat posten.

Die Beiträge stammen in erster Linie von Mitgliedern des Text+ Konsortiums. Gäste sind herzlich eingeladen, Posts aus der Außen- bzw. Kooperationsperspektive beizusteuern. Gebloggt wird hauptsächlich auf Deutsch, andere Sprachen sind ebenfalls möglich und ausdrücklich erwünscht.

Das Text+ Blog bietet die Möglichkeit,
- sowohl (Zwischen-)Ergebnisse vorzustellen als auch Dinge, die noch work in progress sind
- Diskussionen anzustoßen
- Beispiele guter Praxis zu präsentieren
- Tipps und Lösungsvorschläge zur Verfügung zu stellen
- Fragen aufzuwerfen
- Stellung zu beziehen
- auf Veranstaltungen, Ausschreibungen und Publikationen hinzuweisen
- Lieblingstools zu bewerben
- interessante Ressourcen bekannt zu machen
- Sichtbarkeit zu erhöhen
- Vernetzung voranzutreiben
- und und und …

Die Redaktion startet mit einem 10-köpfigen Redaktionsteam und freut sich über weitere Neuzugänge. Wer welche Aufgaben wie oft übernimmt, kann dabei individuell festgelegt werden. Die Redaktion versteht sich dabei nicht als gatekeeper, sondern als enabler: Das Blog lebt von den Aktivitäten der Community!