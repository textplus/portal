---
title: Meldungen 

aliases:
- /aktuelles-veranstaltungen/aktuelles

menu:
  main:
    weight: 10
    parent: aktuelles
type: news

---
# Meldungen
{{<post-teaser title="Text+ Kooperationsprojekte Ausschreibungsrunde 2025 Forschungsdaten aufbereiten und sichern gemeinsam mit Text+">}}
{{<post-teaser title="Neue Lernressourcen zur Gemeinsamen Normdatei (GND) – Wissen fördern und teilen">}}
{{<post-teaser title="LLM Service online">}}
{{<post-teaser title="Data Depositing online">}}
{{<post-teaser title="Survey zum Newsletter">}}
{{<post-teaser title="Text+ Newsletter #4">}}
{{<post-teaser title="Großes Interesse am GND-Forum NFDI, FIDs & Co.">}}
{{<post-teaser title="Save the Date – Text+ Plenary und DARIAH-EU Annual Event">}}
{{<post-teaser title="Text+ Newsletter #3">}}
{{<post-teaser title="NFDI-Kooperation">}}
{{<post-teaser title="Text+ Newsletter #2">}}
{{<post-teaser title="Text+ Newsletter #1">}}
{{<post-teaser title="Text+ Kooperationsprojekte Ausschreibungsrunde 2024 Forschungsdaten aufbereiten und sichern gemeinsam mit Text+" >}}
{{<post-teaser title="DFG-Praxisregeln „Digitalisierung“ – aktualisierte Fassung 2022 auf Zenodo publiziert">}}
{{<post-teaser title="Text+ bloggt!" >}}
{{<post-teaser title="Text+ Plenary 2022" >}}
{{<post-teaser title="Solidaritätserklärung" >}}
{{<post-teaser title="Text+ Einladung zum öffentlichen Tag der Frühjahrstagung am 2. März 2022" >}}
