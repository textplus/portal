---
title: News

aliases:
- /aktuelles-veranstaltungen/aktuelles

menu:
  main:
    weight: 10
    parent: aktuelles
type: news

---
# News
{{<post-teaser title="Text+ Call for Proposals 2025 Processing and Securing Research Data in collaboration with Text+">}}
{{<post-teaser title="New Learning Resources on the Integrated Authority File (GND) – Promoting and Sharing Knowledge">}}
{{<post-teaser title="LLM Service">}}
{{<post-teaser title="Data Depositing">}}
{{<post-teaser title="Survey Newsletter">}}
{{<post-teaser title="Text+ Newsletter #4">}}
{{<post-teaser title="Save the Date – Text+ Plenary und DARIAH-EU Annual Event">}}
{{<post-teaser title="Text+ Newsletter #3">}}
{{<post-teaser title="NFDI-Kooperation">}}
{{<post-teaser title="Text+ Newsletter #2">}}
{{<post-teaser title="Text+ Newsletter #1">}}
{{<post-teaser title="Text+ Kooperationsprojekte Ausschreibungsrunde 2024 Forschungsdaten aufbereiten und sichern gemeinsam mit Text+" >}}
{{<post-teaser title="DFG-Praxisregeln „Digitalisierung“ – aktualisierte Fassung 2022 auf Zenodo publiziert">}}
{{<post-teaser title="Text+ bloggt!" >}}
{{<post-teaser title="Text+ Plenary 2022" >}}
{{<post-teaser title="Solidaritätserklärung" >}}
{{<post-teaser title="Text+ Einladung zum öffentlichen Tag der Frühjahrstagung am 2. März 2022" >}}
