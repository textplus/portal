---
# just a dummy file to set the title and menu

title: "Aktuelles"

menu:
  main:
    identifier: "aktuelles"
    weight: 50

build:
  render: never
---
