---
title: Verbände und Verbünde

aliases:
- /ueber-uns/fachverbaende
- /ueber-uns/weitere-partner
- /vernetzung/verbuende

menu:
  main:
    weight: 50
    parent: vernetzung
---

## Fachverbände und Verbünde

Text+ kooperiert mit einer großen Anzahl geisteswissenschaftlicher
Fachverbände. Dies garantiert den engen Kontakt zur vielfältigen
Community und der Erfassung ihrer Bedarfe. Eine zentrale Rolle kommt
den Fachverbänden in der [Governancestruktur](/ueber-uns/governance)
von Text+ zuteil, da sie Vertretende in das Text+ Plenum entsenden und
Kandidatinnen und Kandidaten für die Wahl der [Coordination
Committees](/ueber-uns/governance) nominieren.

{{<image img="Networking-Initiativen-Gremien.png" alt="Übersicht der unten aufgelisteten Verbünde" />}}

Neben dem engen Kontakt zu Fachverbänden im deutschsprachigen Raum
kooperiert Text+ mit übergreifenden nationalen und internationalen
Verbünden. Dies sichert den Austausch zu internationalen Themen und
Entwicklungen.


{{<accordion>}}
{{<accordion-item title="Fachverbände">}}
* [Arbeitsgemeinschaft der romanistischen Fachverbände](https://deutscher-romanistenverband.de/interesse-an-romanistik/romanistische-fachverbaende/) ([LOS](vernetzung/verbaende-verbuende/letters/LoI-AG-Rom.pdf))
* [Deutsche Gesellschaft für Amerikastudien](https://dgfa.de/) ([LOI](vernetzung/verbaende-verbuende/letters/LoI-DGfA.pdf))
* [Deutsche Gesellschaft für Namenforschung](https://www.gfn.name/) ([LOS](vernetzung/verbaende-verbuende/letters/LOS_GfN.pdf))
* [Deutsche Gesellschaft für Philosophie](https://www.dgphil.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-DGfPhil.pdf))
* [Deutsche Gesellschaft für Sprachwissenschaft (DGfS)](https://dgfs.de/) ([LOI](vernetzung/verbaende-verbuende/letters/LoI-DGfS.pdf))
* [Deutsche Gesellschaft für Volkskunde (dgv)](https://www.d-g-v.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-DGV.pdf))
* [Deutsche Vereinigung für Politikwissenschaft (DVPW)](https://www.dvpw.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-DVPW.pdf))
* [Deutscher Anglistenverband](https://www.anglistenverband.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LOS-Anglistenverband.pdf))
* [Deutscher Germanistenverband](https://deutscher-germanistenverband.de/) ([LOI](vernetzung/verbaende-verbuende/letters/LoI-DGV.pdf))
* [Deutscher Slavistenverband](http://www.slavistenverband.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Slavistenverband.pdf))
* [Digital Humanities im deutschsprachigen Raum (DHd)](https://dig-hum.de/) ([LOI](vernetzung/verbaende-verbuende/letters/LoI-DHd.pdf))
* [Gesellschaft für analytische Philosophie](https://www.gap-im-netz.de/) ([LOI](vernetzung/verbaende-verbuende/letters/LoI-GAP.pdf))
* [Gesellschaft für Angewandte Linguistik](https://gal-ev.de/) ([LOI](vernetzung/verbaende-verbuende/letters/LoI-GAL.pdf))
* [Gesellschaft für bedrohte Sprachen](http://gbs.uni-koeln.de/) ([LOI](vernetzung/verbaende-verbuende/letters/LoI-GBS.pdf))
* [Gesellschaft für germanistische Sprachgeschichte](https://www.germanistische-sprachgeschichte.de/) ([LOI](vernetzung/verbaende-verbuende/letters/LoS-GGSG.pdf))
* [Gesellschaft für Musikforschung](https://www.musikforschung.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-GFM.pdf))
* [Gesellschaft für Sprachtechnologie & Computerlinguistik](https://gscl.org/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-GSCL.pdf))
* [Hochschulverband Informationswissenschaft](https://www.informationswissenschaft.org/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS_HI.pdf))
* [Mediävistenverband](https://www.mediaevistenverband.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Mediavistenverband.pdf))
* [Union der deutschen Akademien der Wissenschaften](https://www.akademienunion.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LOS-Akademieunion.pdf))
* [Verein für Niederdeutsche Sprachforschung](https://www.vnds.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-VndS.pdf))
{{</accordion-item>}}
{{<accordion-item title="Verbünde">}}
* [ALLEA – All European Academies](https://allea.org/) ([LOI](vernetzung/verbaende-verbuende/letters/LoS-ALLEA.pdf))
* [CLARIN ERIC](https://www.clarin.eu/) ([LOI](vernetzung/verbaende-verbuende/letters/LoI-CLARIN.pdf))
* [DARIAH ERIC](https://www.dariah.eu/) ([LOI](vernetzung/verbaende-verbuende/letters/LoI-DARIAH.pdf))
* [DataCite](https://datacite.org/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Data-Cite.pdf))
* [DDB – Deutsche Digitale Bibliothek](https://www.deutsche-digitale-bibliothek.de/) ([LOC](vernetzung/verbaende-verbuende/letters/LoS-DDB.pdf))
* [EOSC – European Open Science Cloud](https://open-science-cloud.ec.europa.eu/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Horstmann-EOSC.pdf))
* [ESFRI – European Strategy Forum on Research Infrastructures](https://www.esfri.eu/) ([LOI](vernetzung/verbaende-verbuende/letters/LoS-Witt-ESFRI.pdf))
* [Europeana](https://www.europeana.eu/)
* [HathiTrust Research Center (HTRC)](https://www.hathitrust.org/htrc) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-HTRC.pdf))
* [LIBER – Ligue des Bibliothèques Européennes de Recherche](https://libereurope.eu/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-LIBER.pdf))
* [OpenAIRE](https://www.openaire.eu/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-OpenaAIRE.pdf))
* [OPERAS – Open scholarly communication in the european research area for the social sciences and humanities](https://operas.hypotheses.org/) ([LOI](vernetzung/verbaende-verbuende/letters/LoI-OPERAS.pdf))
* [RDA – Research Data Alliance](https://rd-alliance.org/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-RDA.pdf))

{{</accordion-item>}}

<br>
<br>
{{</accordion>}}


## Weitere Partner

Als verteilte Infrastruktur ist Text+ mit einer Vielzahl weiterer
Partner eng verbunden, darunter sind sowohl Institutionen wie auch
Einzelpersonen.



{{<accordion>}}
{{<accordion-item title="Institutionen">}}
* [Arbeitsgemeinschaft für Germanistische Edition](https://www.ag-edition.org/)  ([LOS](vernetzung/verbaende-verbuende/letters/LoS-AgE.pdf))
* [Berlin Graduate School of Ancient Studies (BerGSAS)](https://www.berliner-antike-kolleg.org/bergsas/index.html) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-BerGSAS.pdf))
* [Bodleian Libraries, University of Oxford](https://www.bodleian.ox.ac.uk/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Bodleian-Libraries-Oxford.pdf))
* [Corpus Inscriptionum Latinarum (CIL)](https://cil.bbaw.de/) (Inter-Akademieprojekt) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-BBAW-IG-CIL.pdf))
* [Deutsche Digitale Bibliothek](https://www.deutsche-digitale-bibliothek.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-DDB.pdf))
* [Deutscher Bibliotheksverband](https://www.bibliotheksverband.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-dbv-s4.pdf))
* [Die Deutschen Inschriften des Mittelalters und der Frühen Neuzeit](https://www.inschriften.net/) (Inter-Akademieprojekt) ([LOS](vernetzung/verbaende-verbuende/letters/LoI-DI.pdf))
* [FID Allgemeine und Vergleichende Literaturwissenschaft](https://www.avldigital.de/) ([LOS](vernetzung/verbaende-verbuende/letters//LoS_-UB_Frankfurt_2022.pdf))
* [FID Anglo-American Culture](https://libaac.de/home/) ([LOS](vernetzung/verbaende-verbuende/letters//LoS-FID-AAC.pdf))
* [FID Asien – Cross Asia](https://crossasia.org/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-SBB.pdf))
* [FID Benelux / Low Countries Studies](https://www.fid-benelux.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS_FID_Benelux.pdf))
* [FID Buch-, Bibliotheks- und Informationswissenschaft](https://katalog.fid-bbi.de/Content/about) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-FID-BBI.pdf))
* [FID für internationale und interdisziplinäre Rechtsforschung](https://vifa-recht.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS_FID_Recht.pdf))
* [FID Germanistik](https://www.germanistik-im-netz.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-FID-Germanistik.pdf))
* [FID Lateinamerika, Karibik und Latino Studies](https://fid-lateinamerika.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS_FID_Lateinamerika.pdf))
* [FID Linguistik](https://www.ub.uni-frankfurt.de/projekte/fid-linguistik.html) ([LOI](vernetzung/verbaende-verbuende/letters/LoI-FID-Linguistik.pdf))
* [FID Medien-, Kommunikations- und Filmwissenschaft](https://katalog.adlr.link/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS_FID_Medien.pdf))
* [FID Nahost-, Nordafrika- und Islamstudien](https://nahost.fid-lizenzen.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-FID-Nahost.pdf))
* [FID Philosophie](https://philportal.de) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-FID-Philosophie.pdf))
* [FID Romanistik](https://www.fid-romanistik.de/startseite/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-FID-Romanistik.pdf))
* [FID Slawistik](https://staatsbibliothek-berlin.de/sammlungen/fachinformationsdienste/slawistik/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-FID-SLAW.pdf))
* [Forschungszentrum Deutscher Sprachatlas](http://www.deutscher-sprachatlas.de/) ([LOI](vernetzung/verbaende-verbuende/letters/LoI_DSA_Text.pdf))
* [forTEXT – Literatur digital erforschen](https://fortext.net/) ([LOI](vernetzung/verbaende-verbuende/letters//LoI-forText.pdf))
* [FIZ Karlsruhe – Leibniz-Institut für Informationsinfrastruktur](https://www.fiz-karlsruhe.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-FIZ-Karlsruhe.pdf))
* [Inscriptiones Graecae (IG)](https://ig.bbaw.de/) (Inter-Akademieprojekt) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-BBAW-IG-CIL.pdf))
* [Leibniz-Gemeinschaft](https://www.leibniz-gemeinschaft.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Leibniz-Association.pdf))
* [Leibniz Institut für europäische Geschichte (IEG)](https://www.ieg-mainz.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-IEG-Mainz.pdf))
* [Leibniz-Institut für Bildungsforschung und Bildungsinformation](https://www.dipf.de/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-DIPF.pdf))
* [Leibniz-Rechenzentrum (LRZ)](https://www.lrz.de/) ([LOC](vernetzung/verbaende-verbuende/letters/LoS-LRZ.pdf))
* [Leibniz-Zentrum Allgemeine Sprachwissenschaft (ZAS)](https://www.leibniz-zas.de/de/das-zas/institut/) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-ZAS.pdf))
* [Zuse-Institut Berlin](https://www.zib.de/de) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Zuse.pdf))
{{</accordion-item>}}
{{<accordion-item title="Personen">}}
* Jun.-Prof. Dr. habil. Stefanie Acquavella-Rauch ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Acquavella-Rauch.pdf))
* Reinhard Altenhöner ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Altenhoehner.pdf))
* Prof. Dr. Anne Bohnenkamp-Renken ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Bohnenkamp-Renken.pdf))
* Sabine Brünger-Weilandt ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Bruenger-Weilandt.pdf))
* Prof. Dr. Andreas Degkwitz ([LOI](vernetzung/verbaende-verbuende/letters/LoS-Degkwitz.pdf))
* Prof. Dr. Michael Friedrich ([LOC](vernetzung/verbaende-verbuende/letters/LoC-Friedrich.pdf))
* Prof. Dr. Thomas Gloning ([LOC](vernetzung/verbaende-verbuende/letters/LoC-Gloning.pdf))
* Prof. Dr. phil. Ulrich Heid ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Heid.pdf))
* Prof. Dr. Gerhard Heyer ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Heyer.pdf))
* Prof. Dr. Verena Klemm ([LOC](vernetzung/verbaende-verbuende/letters/LoC-Klemm.pdf))
* Prof. Dr. Carolin Müller-Spitzer ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Mueller-Spitzer.pdf))
* Prof. Vivien Petras, [Acting OCC Chair](/ueber-uns/governance/#coordination-committees) ([LOC](vernetzung/verbaende-verbuende/letters/LoC-Petras.pdf))
* Prof. Dr. Judith Pfeiffer, [Acting SCC Chair Editions](/ueber-uns/governance/#coordination-committees) ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Pfeiffer.pdf))
* Prof. Dr. Sandra Richter, [Acting SCC Chair Collections](/ueber-uns/governance/#coordination-committees) ([LOS](vernetzung/verbaende-verbuende/letters/LoC-Richter.pdf))
* PD Dr. habil. Roman Schneider ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Schneider.pdf))
* Prof. Dr. Ingrid Schröder, [Acting SCC Chair LexRes](/ueber-uns/governance/#coordination-committees) ([LOC](vernetzung/verbaende-verbuende/letters/LoC-Schroeder.pdf))
* Prof. Dr. Maria Selig ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Selig.pdf))
* Prof. Dr. Karl Ubl ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Ubl.pdf))
* Prof. Dr. Petra Wagner ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Wagner.pdf))
* Prof. Dr. Heike Zinsmeister ([LOS](vernetzung/verbaende-verbuende/letters/LoS-Zinsmeister.pdf))
{{</accordion-item>}}
{{</accordion>}}
