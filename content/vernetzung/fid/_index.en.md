---
title: Specialised information services 


menu:
  main:
    weight: 40
    parent: vernetzung
---

# Specialised information services

With their special subject collections, academic libraries are
traditionally responsible for the provision of information for science
in Germany. In addition to this basic supply, the 
[German Research Foundation (DFG)](https://www.dfg.de/) has been funding 
[specialised information services for science (FID)](https://www.dfg.de/de/foerderung/foerdermoeglichkeiten/programme/infrastruktur/lis/lis-foerderangebote/fachinfodienste-wissenschaft) 
since 2014. The aim of this system of discipline-specific services is
to provide researchers with the most direct access possible to
research-relevant information. For example, the FIDs develop services
for accessing digital resources. The aim is not to comprehensively
collect publications, but to provide a digital and
location-independent supply of information that complements local
services and is orientated towards the needs of the respective
subjects.


## How do Text+ and FIDs work together?

The FIDs are an important body for reaching scientists in the field of
cutting-edge research. They are an important partner of the NFDI in
general and Text+ in particular for the development of a competence
network in the field of [research data management](/en/themen-dokumentation/forschungsdatenmanagement) and 
[consulting](/en/daten-dienste/consulting), but
also with their expertise in the field of (meta-)data formats,
collection criteria, interfaces and services.

Cooperation between the FIDs (listed below) and the Text+ network
takes place on the basis of working meetings organised every six
months, the so-called [FID/Text+ Jour Fixes](https://events.gwdg.de/category/158/). They work together on
issues such as GND enrichment. In addition, the participants work on
joint submissions with the aim of dissemination at relevant
events/workshops. The cooperation is coordinated by the FID-KOOP
working group. Representatives of the FIDs are actively involved in
the organisation of Text+ through their participation in the
[Coordination Committees](/en/ueber-uns/governance/#coordination-committees).


## Which FIDs are onboard?

* [FID Allgemeine und Vergleichende Literaturwissenschaft](https://www.avldigital.de/)
* [FID Anglo-American Culture](https://libaac.de/home/)
* [FID Asien – Cross Asia](https://crossasia.org/)
* [FID Benelux](https://www.fid-benelux.de/)
* [FID Buch‑, Bibliotheks- und Informationswissenschaft](https://katalog.fid-bbi.de/Content/about)
* [FID Germanistik](https://www.germanistik-im-netz.de/)
* [FID für internationale und interdisziplinäre Rechtsforschung](https://vifa-recht.de/)
* [FID Lateinamerika, Karibik und Latino Studies](https://fid-lateinamerika.de/)
* [FID Linguistik](https://www.ub.uni-frankfurt.de/projekte/fid-linguistik.html)
* [FID für die Medien-, Kommunikations- und Filmwissenschaft](https://katalog.adlr.link/)
* [FID Nahost‑, Nordafrika- und Islamstudien](https://nahost.fid-lizenzen.de/)
* [FID Philosophie](https://philportal.de)
* [FID Religionswissenschaft](https://relbib.de/)
* [FID Romanistik](https://www.fid-romanistik.de/startseite/)
* [FID Slawistik](https://staatsbibliothek-berlin.de/sammlungen/fachinformationsdienste/slawistik/information)
* [FID Theologie](https://ixtheo.de/)

## Further Information

{{<accordion>}}
{{<accordion-item title="Contact">}}
Die AG FID KOOP is headed by Daniela Schulz (schulz@hab.de) and
Felix Rau (f.rau@uni-koeln.de).
{{</accordion-item>}}
{{<accordion-item title="Links/Publications">}}
* [DFG-Förderprogramm „Fachinformationsdienste für die
  Wissenschaft“](https://www.dfg.de/de/foerderung/foerdermoeglichkeiten/programme/infrastruktur/lis/lis-foerderangebote/fachinfodienste-wissenschaft)
* Eggert, Eric, Nils Geißler, Tobias Gradl, Kilian Hensen, Josef Peter Jeschke, Anna Lingnau, Daniela Schulz, Johannes von Vacano, and Lukas Weimer. “More than the Sum of Its Parts – Specialised Information Services and Text+ as Complementary Infrastructures (Abstract),” 2023. https://doi.org/10.5281/zenodo.8359452.
* Hug, Marius. “Nachlese: 2. FID/Text+ Jour Fixe zum Thema Beratung.” Text+ Blog (blog), September 5, 2023. https://textplus.hypotheses.org/7043.
* Text+ Blog-Redaktion. “2. FID / Text+ Jour Fixe am 17. Mai.” Text+ Blog (blog), April 26, 2023. https://textplus.hypotheses.org/4696.
* Geißler, Nils, and Daniela Schulz. “FID/Text+ Jour Fixe: Der erste Kontakt.” Text+ Blog (blog), December 20, 2022. https://textplus.hypotheses.org/2505.
* “Die AG FID Koop stellt sich vor...” Mannheim, September 12, 2022. https://doi.org/10.5281/zenodo.7252704.
* Lordick, Harald, Daniela Schulz, and Kilian Hensen. “Text+ @ FID Roundtable.” Presented at the FID Roundtable, online, April 7, 2022.
{{</accordion-item>}}
{{</accordion>}}


