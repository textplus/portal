---
title: Fachinformationsdienste 


menu:
  main:
    weight: 40
    parent: vernetzung
---

# Fachinformationsdienste 

Die wissenschaftlichen Bibliotheken sind mit ihren
Sondersammelgebieten traditionell zuständig für die
Informationsversorgung der Wissenschaft in Deutschland. Ergänzend zu
dieser Grundversorgung fördert die [Deutsche Forschungsgemeinschaft (DFG)](https://www.dfg.de/) 
seit 2014 [Fachinformationsdienste für die Wissenschaft (FID)](https://www.dfg.de/de/foerderung/foerdermoeglichkeiten/programme/infrastruktur/lis/lis-foerderangebote/fachinfodienste-wissenschaft).
Mit diesem System aus disziplinenspezifischen Angeboten soll
Wissenschaftlerinnen und Wissenschaftlern ein möglichst direkter
Zugriff auf forschungsrelevante Informationen ermöglicht werden. Von
den FIDs werden so bspw. Services für den Zugriff
auf digitale Ressourcen entwickelt. Ziel ist nicht das umfassende
Sammeln von Veröffentlichungen, sondern eine v.a.  digitale und
standortunabhängige Informationsversorgung, die lokale Angebote
ergänzt und sich an den Bedarfen der jeweiligen Fächer orientiert.


## Wie kooperieren Text+ & FIDs?

Die FIDs stellen ein wichtiges Organ dar, um WissenschaftlerInnen im
Bereich der Spitzenforschung zu erreichen. Für den Aufbau eines
Kompetenznetzwerks im Bereich des 
[Forschungsdatenmanagements](themen-dokumentation/forschungsdatenmanagement/) und
[Consultings](daten-dienste/consulting/), aber auch mit ihrer Expertise im Bereich von
(Meta-)Datenformaten, Erfassungskriterien, Schnittstellen und Services
sind sie ein wichtiger Partner der NFDI im Allgemeinen und Text+ im
Besonderen.

Die Kooperation zwischen den (unten genannten) FIDs und dem Verbund
Text+ erfolgt auf Basis von halbjährlich organisierten Arbeitstreffen,
den sogenannten [FID/Text+ Jour Fixes](https://events.gwdg.de/category/158/).  
Inhaltlich wird gemeinsam an Fragestellungen wie bspw. der
GND-Anreicherung gearbeitet. Zudem arbeiten die Beteiligten 
an gemeinsamen Einreichungen mit dem Ziel der Dissemination auf
einschlägigen Veranstaltungen/Workshops. Koordiniert wird die
Zusammenarbeit von der AG FID-KOOP. Repräsentant:innen der FIDs
wirken durch ihre Mitarbeit in den 
[Coordination Committees](ueber-uns/governance/#coordination-committees) 
aktiv an der Ausrichtung von Text+ mit.



## Welche FIDs sind dabei?

* [FID Allgemeine und Vergleichende Literaturwissenschaft](https://www.avldigital.de/)
* [FID Anglo-American Culture](https://libaac.de/home/)
* [FID Asien – Cross Asia](https://crossasia.org/)
* [FID Benelux](https://www.fid-benelux.de/)
* [FID Buch‑, Bibliotheks- und Informationswissenschaft](https://katalog.fid-bbi.de/Content/about)
* [FID Germanistik](https://www.germanistik-im-netz.de/)
* [FID für internationale und interdisziplinäre Rechtsforschung](https://vifa-recht.de/)
* [FID Lateinamerika, Karibik und Latino Studies](https://fid-lateinamerika.de/)
* [FID Linguistik](https://www.ub.uni-frankfurt.de/projekte/fid-linguistik.html)
* [FID für die Medien-, Kommunikations- und Filmwissenschaft](https://katalog.adlr.link/)
* [FID Nahost‑, Nordafrika- und Islamstudien](https://nahost.fid-lizenzen.de/)
* [FID Philosophie](https://philportal.de)
* [FID Religionswissenschaft](https://relbib.de/)
* [FID Romanistik](https://www.fid-romanistik.de/startseite/)
* [FID Slawistik](https://staatsbibliothek-berlin.de/sammlungen/fachinformationsdienste/slawistik/information)
* [FID Theologie](https://ixtheo.de/)

## Weitere Informationen

{{<accordion>}}
{{<accordion-item title="Kontakt">}}

Die AG FID KOOP wird geleitet von Daniela Schulz (schulz@hab.de) und
Felix Rau (f.rau@uni-koeln.de).
{{</accordion-item>}}
{{<accordion-item title="Links/Publikationen">}}

* [DFG-Förderprogramm „Fachinformationsdienste für die
  Wissenschaft“](https://www.dfg.de/de/foerderung/foerdermoeglichkeiten/programme/infrastruktur/lis/lis-foerderangebote/fachinfodienste-wissenschaft)
* Eggert, Eric, Nils Geißler, Tobias Gradl, Kilian Hensen, Josef Peter Jeschke, Anna Lingnau, Daniela Schulz, Johannes von Vacano, and Lukas Weimer. “More than the Sum of Its Parts – Specialised Information Services and Text+ as Complementary Infrastructures (Abstract),” 2023. https://doi.org/10.5281/zenodo.8359452.
* Hug, Marius. “Nachlese: 2. FID/Text+ Jour Fixe zum Thema Beratung.” Text+ Blog (blog), September 5, 2023. https://textplus.hypotheses.org/7043.
* Text+ Blog-Redaktion. “2. FID / Text+ Jour Fixe am 17. Mai.” Text+ Blog (blog), April 26, 2023. https://textplus.hypotheses.org/4696.
* Geißler, Nils, and Daniela Schulz. “FID/Text+ Jour Fixe: Der erste Kontakt.” Text+ Blog (blog), December 20, 2022. https://textplus.hypotheses.org/2505.
* “Die AG FID Koop stellt sich vor...” Mannheim, September 12, 2022. https://doi.org/10.5281/zenodo.7252704.
* Lordick, Harald, Daniela Schulz, and Kilian Hensen. “Text+ @ FID Roundtable.” Presented at the FID Roundtable, online, April 7, 2022.
{{</accordion-item>}}
{{</accordion>}}


