---
title: Internationale Vernetzung



menu:
  main:
    weight: 20
    parent: vernetzung
---

# Internationale Vernetzung von Text+

Die Internationalisierungsstrategie von Text+ stützt sich auf drei Hauptkomponenten:


- Zusammenarbeit mit Europäischen Forschungsinfrastruktur-Konsortien (ERICs) und anderen europäischen Cloud-Diensten, die sich an ähnliche Disziplinen und Nutzergruppen richten.
- Institutionalisierte internationale Zusammenarbeit zwischen den Partnereinrichtungen von Text+ im Bereich des Forschungsdatenmanagements (FDM).
- Teilnahme von Fachpersonen aus Text+ an internationalen Organisationen, um Standards festzulegen und FDM-Verfahren zu entwickeln.

## Zusammenarbeit mit europäischen Forschungsinfrastruktur-Initiativen

Text+ pflegt enge Beziehungen zu European Ressearch Infrastructure Consortia (ERICs)

- [CLARIN](https://www.clarin.eu): Einige Partnerinstitutionen sind Teil des europäischen CLARIN-Netzwerks. In diesem Rahmen stellen sie über Standardschnittstellen Metadaten für die dortigen Dienste bereit. Darüber hinaus beteiligen sie sich an der Entwicklung der dortigen Dienste. Die Ergebnisse der Arbeitsgruppen zu ethischen und rechtlichen Themen, Standards und Zertifizierung von Datenzentren fließen auch in die Arbeiten in Text+ ein.
- [DARIAH](https://www.dariah.eu): Einige Partnerinstitutionen sind Teil des europäischen DARIAH-Netzwerks. Sie sind Teil der Arbeitsgruppen in DARIAH, so dass ein Austausch der Ergebnisse möglich wird. Im Jahr 2025 findet die europäische DARIAH-Konferenz in Verbindung mit dem Text+ Plenary statt.

- [Social Sciences and Humanities Open Cloud (SSHOC)](https://www.sshopencloud.eu/): die Geistes- und Sozialwissenschaftlichen ESFRI-Projekte, darunter CLARIN und DARIAH, bereiten Dienste für die European Open Cloud vor. Darunter ist der [Social Sciences & Humanities Open Marketplace](https://marketplace.sshopencloud.eu/), der auch als Datenquelle für die Services von Text+ genutzt wird. Dazu wurde das Datenmodell so angepasst, dass die Services, die von Text+ bereitgestellt werden, identifiziert und geharvestet werden können.
- [EOSC](https://www.eosc.eu): Zusammen mit anderen Partnern der NFDI sind einige Partnerinstitutionen von Text+ auch Teil von EOSC. Die Angebote werden kontinuierlich entwickelt. Durch die Zusammenarbeit fließen Entwicklungen für EOSC auch unmittelbar in die Entwicklungen von Text+ ein.



## Institutionalisierte internationale Zusammenarbeit

- [Association of European Research Libraries (LIBER)](https://libereurope.eu/):  Mit der Niedersächsischen Staats- und Universitätsbibliothek Göttingen, der Universitäts- und Landesbibliothek Darmstadt und der Deutschen Nationalbibliothek sind drei Text+ Partner Teil von LIBER. Daneben bestehen auch über die Bibliotheken weiterer Partnerinstitutionen Kontakte zu LIBER.
- [Conference of European National Librarians (CENL )](https://www.cenl.org/): Die Vernetzung zu CENL erfolgt über die Deutsche Natioanalbibliothek.
- [European Federation of Academies of Sciences and Humanities (ALLEA)](https://allea.org/): Über die Akademien der Wissenschaft ist Text+ mit ALLEA vernetzt.
- [European Federation of National Institutions for Language (EFNIL)](https://efnil.org/): Der Text+ Partner Leibniz-Institut für Deutsche Sprache ist auch Teil von EFNIL.
- [International Federation of Library Associations and Institutions (IFLA)](https://www.ifla.org/): Auch über Europa hinaus besteht eine Vernetzung zu anderen Bibliotheken. Teil von IFLA sind z. B. die Deutsche Nationalbibliothek, die Niedersächsische Staats- und Universitätsbibliothek Göttingen und die Universitäts- und Landesbibliothek Darmstadt.


## Internationale Organisationen
- [Research Data Alliance (RDA)](https://www.rd-alliance.org/): Beteiligte an Text+ sind auch in Arbeitsgruppen der RDA aktiv.
- [Text Encoding Initiative (TEI)](https://tei-c.org/): Einige Text+ Partner sind an der Entwicklung der TEI beteiligt.
- [International Organization for Standardisation (ISO)](https://www.iso.org): Text+ Partner sind im Bereich der Normung an ISO-Arbeitsgruppen beteiligt, z. B. an dem [ISO Ausschuss zu Sprachressourcen](https://www.iso.org/committee/297592.html).
