---
title: International network


menu:
  main:
    weight: 20
    parent: vernetzung
---

# International Networking of Text+

The internationalization strategy of Text+ is based on three main components:

- Collaboration with European Research Infrastructure Consortia (ERICs) and other European cloud services that target similar disciplines and user groups.
- Institutionalized international collaboration between the partner institutions of Text+ in the field of research data management (RDM).
- Participation of individual experts from Text+ in international organizations for setting standards and developing RDM procedures.

## Collaboration with European Research Infrastructure Initiatives

Text+ maintains close relationships with European Research Infrastructure Consortia (ERICs):

- [CLARIN](https://www.clarin.eu): Some partner institutions are part of the European CLARIN network. In this context, they provide metadata for the services there via standard interfaces. They also participate in the development of the services there. The results of the working groups on ethical and legal issues, standards, and certification of data centers also flow into the work of Text+.
- [DARIAH](https://www.dariah.eu):  Some partner institutions are part of the European DARIAH network. They are part of the working groups in DARIAH, enabling an exchange of results. In 2025, the European DARIAH conference will be held in conjunction with the Text+ Plenary.

- [Social Sciences and Humanities Open Cloud (SSHOC)](https://www.sshopencloud.eu/): The humanities and social sciences ESFRI projects, including CLARIN and DARIAH, are preparing services for the European Open Cloud. This includes the Social Sciences & Humanities Open Marketplace, which is also used as a data source for the services of Text+. The data model has been adapted so that the services provided by Text+ can be identified and accordingly harvested.
- [EOSC](https://www.eosc.eu): Together with other partners of the NFDI, some partner institutions of Text+ are also part of EOSC. The offerings are continuously developed. Through collaboration, developments for EOSC also flow directly into the developments of Text+.

## Institutionalized International Collaboration

- [Association of European Research Libraries (LIBER)](https://libereurope.eu/): With the Göttingen State and University Library, the University and State Library Darmstadt, and the German National Library, three Text+ partners are part of LIBER. In addition, there are contacts with LIBER through the libraries of other partner institutions.
- [Conference of European National Librarians (CENL )](https://www.cenl.org/): Networking with CENL is done through the German National Library.
- [European Federation of Academies of Sciences and Humanities (ALLEA)](https://allea.org/): Through the academies of sciences, Text+ is connected with ALLEA.
- [European Federation of National Institutions for Language (EFNIL)](https://efnil.org/): As a Text+ partner, the Leibniz Institute for the German Language is also part of EFNIL.
- [International Federation of Library Associations and Institutions (IFLA)](https://www.ifla.org/):  Beyond Europe, there is networking with other libraries. For example, the German National Library, the Göttingen State and University Library, and the University and State Library Darmstadt are part of IFLA.

## International Organizations

- [Research Data Alliance (RDA)](https://www.rd-alliance.org/): Participants in Text+ are also active in RDA working groups.
- [Text Encoding Initiative (TEI)](https://tei-c.org/): Some participants in Text+ are involved in the development of TEI.
- [International Organization for Standardisation (ISO)](https://www.iso.org): In the field of standardization, some from Text+ are also involved in ISO working groups, such as the [ISO Committee on Language Resource Management](https://www.iso.org/committee/297592.html). 
