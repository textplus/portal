---
# just a dummy file to set the title and menu

title: "Networking"

menu:
  main:
    identifier: "vernetzung"
    weight: 30

build:
  render: never
---
