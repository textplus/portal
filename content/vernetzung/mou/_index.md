---

title: Konsortienübergreifende Kooperation  
menu:  
  main:  
    weight: 25  
    parent: vernetzung  

---

# Konsortienübergreifende Kooperation  

## NFDI-weite Zusammenarbeit  

Die disziplinspezifischen Bedarfe werden im Rahmen der Nationalen Forschungsdateninfrastruktur (NFDI) durch die [Konsortien](https://www.nfdi.de/konsortien/) adressiert. Darüber hinaus gibt es jedoch eine ganze Reihe von Querschnittsthemen – Themen also, die es konsortienübergreifend gemeinsam zu bearbeiten gilt. Um die Arbeit an Standards, Metadatenstandards und Formaten über die Grenzen der Konsortien hinweg zu organisieren, wurden insgesamt fünf sogenannte [Sektionen](https://www.nfdi.de/sektionen/) eingerichtet:  
* [Common Infrastructures](https://www.nfdi.de/section-infra/) (section-infra)  
* [Ethical, Legal and Social Aspects](https://www.nfdi.de/section-elsa/) (section-ELSA)  
* [Industry Engagement](https://www.nfdi.de/sektion-industry-engagement/) (section-industry)  
* [(Meta)daten, Terminologien, Provenienz](https://www.nfdi.de/section-metadata/) (section-metadata)  
* [Training & Education](https://www.nfdi.de/section-edutrain/) (section-edutrain)  

Text+ ist mit seinen Partnereinrichtungen aktiv an allen fünf Sektionen beteiligt.

## Geistes- und kulturwissenschaftliche Konsortien  

Um den vielfältigen Bedarfen der geistes- und kulturwissenschaftlichen Forschungslandschaft gerecht zu werden, haben sich bereits 2019 die vier Konsortien [NFDI4Culture](https://nfdi4culture.de/), [NFDI4Memory](https://4memory.de/), [NFDI4Objects](https://www.nfdi4objects.net/) und Text+ unter dem Dach eines [Memorandum of Understanding](https://doi.org/10.5281/zenodo.4045000) der NFDI-Konsortien aus den Geistes- und Kulturwissenschaften (Humanities@NFDI) zusammengeschlossen.

{{<image img="files/mou.jpg"/>}}

Forschungsdaten aus den Geistes- und Kulturwissenschaften sind von besonderer Bedeutung, da sie einen Zugang zum globalen kulturellen Erbe ermöglichen und dadurch über die Wissenschaft hinaus auch gesellschaftlich von allgemeinem Interesse sind. Durch das Memorandum of Understanding wird der Bedarf eines koordinierten, disziplinübergreifenden Managements der umfangreichen Datensammlungen in den Geistes- und Kulturwissenschaften bedient. Es belegt den Vorsatz der vier Konsortien, in der NFDI eng zusammenzuarbeiten, sich zu unterstützen und einen kulturellen Wandel im Umgang mit Forschungsdaten herbeizuführen, jeweils unter Berücksichtigung des breiten disziplinären Spektrums und der unterschiedlichen Datentypen mit deren unterschiedlicher Quantität, Abstraktion, Dynamik und Qualität.  

Aufbauend auf den Zielen, auf die sich die unterzeichnenden Konsortien in der [Berlin Declaration](https://openaccess.mpg.de/Berlin-Declaration) und der [Leipzig-Berlin-Erklärung](https://zenodo.org/records/3895209) verständigt haben, möchte die Gruppe der geistes- und kulturwissenschaftlichen Konsortien:  
* über regelmäßige Strategietreffen im kontinuierlichen Austausch bleiben und so gemeinsame Verantwortung übernehmen,  
* Ressourcen für die Bearbeitung von Querschnittsthemen bereitstellen,  
* Forschende von Anfang an in den Aufbauprozess der NFDI einbeziehen,  
* neben der Fach- von Beginn an auch die Infrastrukturexpertise der Einrichtungen berücksichtigen und in die Konsortialstruktur integrieren,  
* institutionell verbunden und dabei gleichzeitig organisatorisch wie auch personell offen und dynamisch sein,  
* ihr Portfolio kontinuierlich evaluieren und weiterentwickeln,  
* Expertise und Dienste in den gemeinsam identifizierten Handlungsfeldern (i) Meta-, Autoritätsdaten und Terminologien, (ii) Provenienz, (iii) Recht und Ethik und (iv) Data Literacy bereitstellen,  
* zur Mitarbeit ermutigen,  
* Zusammenarbeit statt Wettbewerb fördern.  

### Base4NFDI

{{<image img="b4n.png" license="Foto: Logo Base4NFDI" alt="Base4NFDI"/>}}

Die NFDI-Basisdiensteinitiative Base4NFDI ist für Text+ von großer Bedeutung. Text+ bringt sich auf verschiedenen Ebenen mit seiner Expertise, seinen Ressourcen und vor allem seinen Anforderungen als Konsortium für text- und sprachbasierte Forschungsdaten in Base4NFDI ein. So hat sich Text+ bereits bei der Konzeption und Beantragung von Base4NFDI durch Mitarbeit im Antragsteam beteiligt und es gibt bis heute personelle Überschneidungen zwischen beiden Vorhaben. Gleichzeitig sind Mitarbeitende von Text+ in den Sektionen des NFDI-Vereins aktiv und dort in die Konzeption und Auswahl von Basisdienstanträgen eingebunden. Bei den Basisdienstvorhaben DMP4NFDI und IAM4NFDI bringt sich Text+ mit Inkubatorprojekten ein. Das Vorhaben TS4NFDI wird von Text+ im Rahmen der Arbeitsgruppe Ontology Harmonization sowie mit infrastruktureller Interoperabilität - bspw. hinsichtlich der Text+ Registry - unterstützt.

An Text+ beteiligte Einrichtungen sind (Stand 2/2025) in vier Basisdienstvorhaben aktiv: IAM4NFDI, PID4NFDI, Jupyter4NFDI und nfdi.software. Weitere Basisdienstvorhaben werden hinzukommen.
### KonsortSWD  

Neben den drei bereits genannten geistes- und kulturwissenschaftlichen Konsortien kooperiert Text+ seit 2023 auch mit [KonsortSWD](https://www.konsortswd.de/). Für diese Zusammenarbeit liegen konkrete Ansatzpunkte beispielsweise in der qualitativen Sozialforschung, in der Texte als Daten für die interpretative Einordnung sozialer Entwicklung genutzt werden. Dabei gewinnen computerlinguistische Methoden durch das zunehmende Interesse an Textdaten aus den Sozialen Medien weiter an Bedeutung.  

Die im [Memorandum of Understanding](vernetzung/mou/files/20230731-MoU-TextPlus-KonsortSWD.pdf) festgehaltenen Ziele umfassen einen Interessensaustausch zu relevanten Themen und die Initiierung eines ggf. gemeinsamen Vorgehens. Gemeinsame Veranstaltungsreihen sollen beibehalten und erweitert, gemeinsame Impulse für Forschungsprojekte gesetzt werden. Es soll ein Erfahrungsaustausch hinsichtlich der Zertifizierung von Datenzentren stattfinden, eine qualitätsgesicherte und nachhaltige Bereitstellung von beidseitig relevanten Tools und Diensten soll unterstützt und ein gemeinsames Auftreten grundlegender Fragen in der NFDI koordiniert werden.