---
title: Cross-consortia cooperation 


menu:
  main:
    weight: 25
    parent: vernetzung

---

# Cross-consortia cooperation

## NFDI-wide cooperation 

Discipline-specific requirements within the framework of the National Research Data Infrastructure (NFDI) are addressed by the [consortia](https://www.nfdi.de/konsortien/). However, there is also a whole range of cross-cutting topics, i.e. topics that need to be addressed jointly across consortia. In order to organise the work on standards, metadata standards and formats beyond the boundaries of the consortia, a total of five so-called [sections](https://www.nfdi.de/sektionen/) have been set up:

* [Common Infrastructures](https://www.nfdi.de/section-infra/) (section-infra)
* [Ethical, Legal and Social Aspects](https://www.nfdi.de/section-elsa/) (section-ELSA)
* [Industry Engagement](https://www.nfdi.de/sektion-industry-engagement/) (section-industry)
* [(Meta)daten, Terminologien, Provenienz](https://www.nfdi.de/section-metadata/) (section-metadata)
* [Training & Education](https://www.nfdi.de/section-edutrain/) (section-edutrain)

Text+ and its partner organisations are actively involved in all five sections.

## Consortia of the Humanities and Cultural Studies

In order to meet the diverse needs of the humanities and cultural studies research landscape, the four consortia [NFDI4Culture](https://nfdi4culture.de/), [NFDI4Memory](https://4memory.de/), [NFDI4Objects](https://www.nfdi4objects.net/) and Text+ joined forces in 2019 to form a [Memorandum of Understanding](https://doi.org/10.5281/zenodo.4045000) between the NFDI consortia from the humanities and cultural studies (Humanities@NFDI).

{{<image img="files/mou.jpg"/>}}

Research data from the humanities and cultural sciences are of particular importance as they provide access to the global cultural heritage and are therefore of general interest to society beyond science. The Memorandum of Understanding fulfils the need for coordinated, cross-disciplinary management of the extensive data collections in the humanities and cultural sciences. It demonstrates the intention of the four consortia to work closely together in the NFDI, to support each other and to bring about a cultural change in the handling of research data, taking into account the broad disciplinary spectrum and the different types of data with their varying quantity, abstraction, dynamics and quality.

Building on the objectives agreed by the signatory consortia in the [Berlin Declaration](https://openaccess.mpg.de/Berlin-Declaration) and the [Leipzig-Berlin-Erklärung](https://zenodo.org/records/3895209), the group of humanities and cultural studies consortia would like to

* remain in continuous dialogue via regular strategy meetings and thus assume joint responsibility,
* provide resources for the processing of cross-cutting issues,
* involve researchers in the development process of the NFDI from the outset,
* take into account the infrastructure expertise of the institutions in addition to their specialist expertise from the outset and integrate it into the consortium structure,
* be institutionally connected and at the same time open and dynamic in terms of organisation and personnel,
* continuously evaluate and further develop their portfolio,
* provide expertise and services in the jointly identified fields of action (i) metadata, authority data and terminologies, (ii) provenance, (iii) law and ethics and (iv) data literacy,
* encourage collaboration,
* promote cooperation instead of competition.


### KonsortSWD

In addition to the three humanities and cultural studies consortia already mentioned, since 2023 Text+ has also been cooperating with [KonsortSWD](https://www.konsortswd.de/). There are concrete starting points for this collaboration, for example in qualitative social research, in which texts are used as data for the interpretative categorisation of social development.  The relevance of computational linguistic methods is becoming increasingly important due to the growing interest in text data from social media.

The objectives set out in the [Memorandum of Understanding](vernetzung/mou/files/20230731-MoU-TextPlus-KonsortSWD.pdf) include an exchange of interests on relevant topics and the initiation of a joint approach where appropriate. Joint series of events are to be maintained and expanded, and joint impetus for research projects is to be provided. An exchange of experience regarding data centre certification is to take place, a quality-assured and sustainable provision of mutually relevant tools and services is to be supported and a joint approach to fundamental issues in the NFDI is to be coordinated.
