---
# just a dummy file to set the title and menu

title: "Vernetzung"

menu:
  main:
    identifier: "vernetzung"
    weight: 30

build:
  render: never
---
