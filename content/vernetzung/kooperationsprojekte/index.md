---
title: Kooperationsprojekte

aliases:
- /forschungsdaten/kooperationsprojekte

menu:
  main:
    weight: 30
    parent: vernetzung
---

# Kooperationsprojekte

## Ausschreibungsrunde 2025: Forschungsdaten aufbereiten und sichern gemeinsam mit Text+

Text+ ist Teil der Nationalen Forschungsdateninfrastruktur (NFDI) mit dem Ziel, eine ortsverteilte Forschungsdateninfrastruktur für geisteswissenschaftliche Forschungsdaten aufzubauen. Der Fokus liegt dabei auf den Datenbereichen digitale Sammlungen, lexikalische Ressourcen und Editionen. 

Ziel dieser Ausschreibung ist es, die Angebote an Daten und Services von Text+ kontinuierlich zu erweitern und für die Forschungscommunity langfristig verfügbar zu machen. Alternativ können Anträge gestellt werden, die die in Text+ verfügbaren Daten und Services gezielt für innovative Forschungsfragen nutzen. Ebenso können eigene institutionalisierte und nachhaltige Datenzentren mit Schnittstellen in die technische Infrastruktur von Text+ eingebunden werden, um die betreffenden Daten in der Text+ Infrastruktur dauerhaft verfügbar zu machen. Bei Software und anderen Services wird erwartet, dass das Ergebnis unter einer offenen Lizenz bereitgestellt wird, mit Text+ abgestimmt ist (Leistung eines Kohärenzbeitrags) und langfristig für die Infrastruktur betrieben werden kann. Die Bereitstellung entsprechend den [FAIR-](https://doi.org/10.1038/sdata.2016.18) und [CARE-](https://www.gida-global.org/care) Prinzipien ist dabei wesentlich. Nach den Richtlinien der NFDI sind nur Arbeiten förderfähig, die auf die Integration in das Angebotsportfolio von Text+ abzielen. Aufbau und Ausbau von Ressourcen selbst sind hingegen aus Eigenmitteln zu tragen.

Förderanträge können für eine der Datendomänen Collections, lexikalische Ressourcen, Editionen sowie für die Task Area Infrastruktur/Betrieb gestellt werden (vgl. hierzu u.a. die Beschreibung der [Arbeitsbereiche](ueber-uns/arbeitsbereiche)). Zur Vorbereitung der Anträge sollte ein Beratungsgespräch mit dem zuständigen Bereich erfolgen. Die Liste der Ansprechpersonen finden Sie am Ende dieser Ausschreibung. 

Im Rahmen dieser Ausschreibungen können mehrere Projekte in der Regel mit einem Volumen zwischen 35.000 EUR und 65.000 EUR gefördert werden. Zusätzlich wird auf die Projektsumme ein Overhead von 22% gewährt. Die Laufzeit der Projekte ist an das Kalenderjahr 2026 gebunden und beträgt maximal neun Monate im Zeitraum Januar bis September; eine Übertragung von nicht ausgegebenen Mitteln auf einen späteren Zeitraum ist nicht möglich. Nach Abschluss der Förderung wird ein Projektbericht erwartet, der auf der Text+ Webseite veröffentlicht werden soll. 

Eventuelle Rückfragen können Sie gerne an <office@text-plus.org> adressieren. Weitere Informationen zum Antragsverfahren vgl. [FAQ](vernetzung/kooperationsprojekte/#faq).

### Zeitplanung

- Bewerbungsende: 31. März 2025 
- Bekanntgabe der Förderung bis Ende August 2025 
- Frühestmöglicher Förderbeginn: 01. Januar 2026
- Ende der Förderung spätestens: 30. September 2026


### Antragsberechtigung

Antragsberechtigt sind Abteilungen und Arbeitsgruppen, die nicht bereits im Rahmen von Text+ finanziell gefördert werden, die aber den [DFG-Förderrichtlinien](https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf) für die NFDI (Abschnitt III (1)) entsprechen.
Das Begutachtungsverfahren wird durch die Koordinationskomitees der jeweiligen Arbeitsbereiche geleitet. Falls Mitglieder der Koordinationskomitees an Anträgen beteiligt sind, werden diese Mitglieder vom Begutachtungsprozess im jeweiligen Jahr ausgeschlossen.

Die Förderung setzt den Abschluss eines Nachunternehmervertrags im Rahmen des Mittelweiterleitungs- und Kooperationsvertrags im NFDI-Projekt mit dem Leibniz-Institut für Deutsche Sprache Mannheim als hauptantragsstellender Einrichtung von Text+ voraus. Bei Vorliegen der notwendigen Voraussetzungen, insbesondere eines langfristigen und dauerhaften Beitrags zum Konsortium Text+ über das Kooperationsprojekt hinaus, ist alternativ der Beitritt zum Mittelweiterleitungs- und Kooperationsvertrag (und damit zum Konsortium Text+) möglich. Wiederholte Förderungen desselben Projektes als Kooperationsprojekt sind dagegen nicht möglich.


### Auswahlkriterien


Die Begutachtung der Anträge wird nach den folgenden Kriterien erfolgen:
- Verknüpfung und Integration in die Text+ Infrastruktur nach den FAIR- und CARE-Prinzipien und die Erweiterung des Angebotsportfolios von Text+ bezüglich Daten bzw. Services innerhalb des Förderzeitraums
- Relevanz und Innovation
  - potenzieller wissenschaftlicher Beitrag der Daten bzw. Services
  - klare Problembeschreibung und Bezug zu den Arbeitsprogrammen des Konsortiums Text+
  - Nachnutzbarkeit und Zugänglichkeit der Daten bzw. Services
- wissenschaftliche Qualität des Projekts
- Beitrag zu Diversität und Repräsentativität des Text+ Datenangebots
- Aufwandsabschätzung 
  - Angemessenheit der beantragten Fördersumme für die Realisierung 
  - Vorarbeiten und eigene Beiträge der Antragsstellenden


### Ansprechpersonen

- Sammlungen: [Dr. Peter Leinen](https://www.dnb.de/SharedDocs/Kontaktdaten/DE/leinenPeter.html) (<p.leinen@dnb.de>), [Philippe Genêt](https://www.dnb.de/SharedDocs/Kontaktdaten/DE/genetPhilippe.html) (<p.genet@dnb.de>)
- Lexikalische Ressourcen: [PD Dr. Alexander Geyken](https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/geyken-alexander) (<geyken@bbaw.de>), [Axel Herold](https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/herold-axel) (<herold@bbaw.de>)
- Editionen: [Prof. Dr. Andreas Speer](https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/andreas-speer) (<andreas.speer@uni-koeln.de>), [Kilian Erasmus Hensen](https://cceh.uni-koeln.de/personen/kilian-hensen/) (<Kilian.Hensen@uni-koeln.de>) 
- Infrastruktur/Betrieb: [Prof. Dr. Philipp Wieder](https://gwdg.de/research-education/researchgroup_wieder/) (<philipp.wieder@sub.uni-goettingen.de>), [Stefan Buddenbohm](https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/stefan-buddenbohm-1/) (<buddenbohm@sub.uni-goettingen.de>)



## Vorlage: Ausschreibungsrunde 2025

### Dokumente und Bewerbungsvorlage

Bitte nutzen Sie die hier hinterlegte [Word-Vorlage für Ihre Bewerbung](/vernetzung/kooperationsprojekte/Call2025_Template-Kooperationsprojekte_dt.docx "download").

&nbsp; 

## Ausschreibungsrunde 2024

Jedes Kooperationsprojekt ist mit der zugeordneten Text+ Task Area versehen, d.h. Col = Collections, Ed = Editions, LR = Lexical Resources sowie I/O=Infrastuktur/Betrieb. 

### Geförderte Projekte

- **Text+-Schnittstellen zu den Interview-Sammlungen in Oral-History.Digital (text+oh.d)**
  - **Institution:** Universitätsbibliothek der Freien Universität Berlin
  - **Projektleitung:** Dr. Cord Pagenstecher
  - **Task Area in Text+:** Collections (Col)

- **Glossarium Graeco-Arabicum – Open Data (GlossGA – OD)**
  - **Institution:** Friedrich-Alexander-Universität Erlangen-Nürnberg 
  - **Projektleitung:** Dr. Rüdiger Arnzen
  - **Task Area in Text+:** Lexical Resources (LR)

- **HAdW GND-basierte Webservices – Beaconizer & Discoverer (Hagrid)**
  - **Institution:** Heidelberger Akademie der Wissenschaften
  - **Projektleitung:** Dr. Frank Grieshaber
  - **Task Area in Text+:** Infrastructure/Operations (I/O)

- **Aufbau einer offenen digitalen Sammlung historischer musiktheoretischer Texte aus dem deutschsprachigen Raum anhand von Beispielen aus dem 19. Jahrhundert (DigiMusTh)**
  - **Institution:** Julius-Maximilians-Universität Würzburg
  - **Projektleitung:** Prof. Dr. Fabian C. Moss
  - **Task Area in Text+:** Collections (Col)

- **LOD-Rollen-Modellierungen aus den Registern von Regestenwerken zum Mittelalter (LRM)**
  - **Institution:** Akademie der Wissenschaften und der Literatur Mainz
  - **Projektleitung:** Prof. Dr. Andreas Kuczera
  - **Task Area in Text+:** Editions (Ed)
---

## Ausschreibungsrunde 2023

Jedes Kooperationsprojekt ist mit der zugeordneten Text+ Task Area versehen, d.h. Col = Collections, Ed = Editions, LR = Lexical Resources sowie I/O=Infrastuktur/Betrieb. 

### Geförderte Projekte

- **Die Beria Collection im Spracharchiv Köln: Erweiterung, Überarbeitung und Auswertung der Datensammlung einer unterbeschriebenen afrikanischen Sprache (BeriaCollection)**
  - **Institution:** Universität zu Köln, Institut für Linguistik
  - **Projektleitung:** Prof. Dr. Birgit Hellwig, Dr. Isabel Compes
  - **Task Area in Text+:** Collections (Col)

- **Thesaurus Linguae Aegyptiae – More Fair with APIs (LR TeLApi)**
  - **Institution:** Berlin-Brandenburgische Akademie der Wissenschaften, Zentrum Grundlagenforschung Alte Welt 
  - **Projektleitung:** Dr. Daniel Werning
  - **Task Area in Text+:** Lexical Resources (LR)

- **Das älteste Görlitzer Stadtbuch 1305-1416: Transformation, Kuratierung und doppelte digitale Publikation (Daten, Webanwendung) einer außergewöhnlichen Buchedition für die historischen Disziplinen (RedBook1305)**
  - **Institutionen:** Bergische Universität Wuppertal / Martin-Luther-Universität Halle-Wittenberg
  - **Projektleitung:** Prof. Dr. Patrick Sahle, Dr. Christian Speer
  - **Task Area in Text+:** Editions (Ed)

- **Werkzeugunterstützung für die automatische Extraktion von Tabellendaten aus historischen Zeitungen (TabellenExtraktion)**
  - **Institution:** Hochschule Wismar
  - **Projektleitung:** Prof. Dr.-Ing. Frank Krüger
  - **Task Area in Text+:** Infrastructure/Operations (I/O)

---

## Ausschreibungsrunde 2022

Jedes Kooperationsprojekt ist mit der zugeordneten Text+ Task Area versehen, d.h. Col = Collections, Ed = Editions sowie LR = Lexical Resources. Zuordnungen zur Task Area Infrastuktur/Betrieb sind erst ab der 2. Ausschreibungsrunde möglich.

### Geförderte Projekte

- **Erschließung von Diskettenmagazinen der 1980er und 1990er: Digitales Kulturerbe aus der Pionierzeit der Heimcomputer für textbasierte Diskmags Forschung re-digitalisieren (Diskmags)** 
  - **Institution:** Universität Würzburg
  - **Projektleitung:** Torsten Roeder
  - **Task Area in Text+:** Collections (Col)

- **Offener Zugang zu den Daten des DLA Marbach auf Basis forschungsprojektbezogener Fragestellungen. Entwickelt am Beispiel der Daten des Quellenrepertorium der Exilbibliotheken (Alfred Döblin und Siegfried Kracauer) (DLA Data+)** 
  - **Institution:** Deutsches Literaturachiv Marbach
  - **Projektleitung:** Karin Schmidgall
  - **Task Area in Text+:** Collections (Col)

- **Integration des Digitalen Programmarchivs deutscher Volkshochschulen in das NFDI-Konsortium Text+ (DiPA+)** 
  - **Institution:** Deutsches Institut für Erwachsenenbildung
  - **Projektleitung:** Kerstin Hoenig
  - **Task Area in Text+:** Collections (Col)

- **KOLIMO+. Optimierung und Offene Publikation des Korpus der literarischen Moderne (KOLIMO+)**
  - **Institution:** Universität Bielefeld
  - **Projektleitung:** Berenike Herrmann, Alan Lena van Beek
  - **Task Area in Text+:** Collections (Col)

- **Modellierung von Texteditionen als Linked Data (edition2LD)**
  - **Institution:** Heidelberger Akademie der Wissenschaften
  - **Projektleitung:** Dieta Svoboda-Baas, Sabine Tittel
  - **Task Area in Text+:** Editions (Ed)

- **Pessoa digital** 
  - **Institution:** Universität Rostock
  - **Projektleitung:** Ulrike Henny-Krahmer
  - **Task Area in Text+:** Editions (Ed)

- **Europäische Friedensverträge der Vormoderne in Daten (FriVer+)**
  - **Institution:** Leibniz-Institut für Europäische Geschichte (IEG)
  - **Projektleitung:** Thorsten Wübbena
  - **Task Area in Text+:** Editions (Ed)

- **Text+-Schnittstelle für das Mittelhochdeutsche Wörterbuch (MWB-APIplus)**
  - **Institutionen:** Universität Trier/Akademie Göttingen
  - **Projektleitung:** Ute Recker-Hamm, Ralf Plate, Jonas Richter
  - **Task Area in Text+:** Lexical Resources (LR)

- **Integration NiederSorbisch-deutscher Wörterbücher als lexikalischE Ressourcen in Text+ (INSERT)** 
  - **Institution:** Sorbisches Institut
  - **Projektleitung:** Hauke Bartels
  - **Task Area in Text+:** Lexical Resources (LR)

- **Corpus glossariorum Latinorum Online Lemmatization (CGLO)**
  - **Institution:** Bayerische Akademie der Wissenschaften
  - **Projektleitung:** Adam Gitner
  - **Task Area in Text+:** Lexical Resources (LR)

---

## FAQ
{{<accordion>}}
{{<accordion-item title="Ziel der Kooperationsprojekte">}}
**Was ist das Ziel der Kooperationsprojekte?**

Kooperationsprojekte im Rahmen von Text+ dienen dazu, das Angebotsportfolio von Text+ zu erweitern. Text+ ist offen für neue Angebote und Beteiligte und beschränkt sich nicht nur auf die Angebote der initialen Partner, sondern kann als Infrastruktur auch weitere Daten, Dienste und Services integrieren. Wer ein Kooperationsprojekt anbietet, kann auch Teil des Text+-Netzwerks werden, sich am weiteren Aufbau der Infrastruktur beteiligen und von den Entwicklungen profitieren. Der Förderzeitraum beträgt 9 Monate (Januar bis September 2026).
{{</accordion-item>}}

{{<accordion-item title="Antragsberechtigungen">}}

Wenn ein Vorschlag für ein Kooperationsprojekt in einem Jahr nicht gefördert wird, darf der Antrag in einer späteren Runde nochmals gestellt werden? Kann ein Antrag in einer der nächsten Runden auch eingereicht werden, falls zuvor keine Finanzierung möglich war?
Auf jeden Fall. Im Rahmen der Begutachtung der Projekte wird eine Prioritätenliste der Vorschläge erstellt, die bis zur Ausschöpfung des jeweiligen Budgets einer Finanzierungsrunde bewilligt werden können. Das bedeutet also nicht notwendigerweise, dass ein Projekt, das in einem Jahr nicht gefördert wurde, nicht förderfähig wäre. Natürlich kann ein Antrag vor der Neueinreichung weiter gestärkt und auch durch weitere eigene Vorarbeiten aktualisiert werden. Im konkreten Fall sollte zuvor das Gespräch mit denjenigen gesucht werden, die die jeweilige Datendomäne koordinieren (vgl. hierzu unter dem Punkt „Antragstellung“ die Frage „Wer kann im Vorfeld der Antragstellung angesprochen werden, um die Vorgaben, Erwartungen etc. bei der Planung und Konzeption der Kooperationsprojekte auf die Anforderungen von Text+ abzustimmen?“).

**Wer kann einen Antrag stellen? Können Studierende/Promovierende/Initiativen einen Antrag stellen?**

Antragsberechtigt sind Abteilungen und Arbeitsgruppen, die nicht bereits im Rahmen von Text+ finanziell gefördert werden, die aber den DFG-FÖRDERRICHTLINIEN FÜR DIE NFDI (ABSCHNITT III (1) (https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf)) entsprechen. Einzelforschende und Verbünde von Forschenden (z. B. in Verbundprojekten) können z. B. über ihre Hochschule oder Forschungseinrichtung einen Antrag stellen, wenn sie zu den in den DFG-Richtlinien genannten Einrichtungen zählen. Viele Universitäten und Forschungseinrichtungen haben dazu eigene Abteilungen, die sich mit Forschungsförderung und Drittmittelprojekten beschäftigen. Forschende sollten mit der entsprechenden Abteilung ihrer Heimatinstitution Kontakt aufnehmen, um die Voraussetzungen einer Antragstellung durch die Einrichtung zu klären.

**Können auch Einzelpersonen Anträge stellen oder müssen es immer Teams sein? Können auch zwei Wissenschaftler/Wissenschaftlerinnen Anträge stellen?**

Es gibt keine Vorgaben zur Größe eines Teams, das ein Kooperationsprojekt durchführt. Antragsberechtigt sind aber nur Abteilungen und Arbeitsgruppen, die nicht bereits im Rahmen von Text+ finanziell gefördert werden, die aber den DFG-FÖRDERRICHTLINIEN FÜR DIE NFDI (ABSCHNITT III (1), siehe https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf) entsprechen. Einzelforschende und Verbünde von Forschenden (z. B. in Verbundprojekten) können z. B. über ihre Hochschule oder Forschungseinrichtung einen Antrag stellen, wenn sie zu den in den DFG-Richtlinien genannten Einrichtungen zählen. Im konkreten Fall sollte zuvor das Gespräch mit denjenigen gesucht werden, die die jeweilige Datendomäne koordinieren (vgl. hierzu unter dem Punkt „Antragstellung“ die Frage „Wer kann im Vorfeld der Antragstellung angesprochen werden, um die Vorgaben, Erwartungen etc. bei der Planung und Konzeption der Kooperationsprojekte auf die Anforderungen von Text+ abzustimmen?“).

**Können sich auch Forschende aus anderen Ländern an einem Antrag beteiligen?**

In einigen Projekten haben Forschende aus dem Ausland einen wichtigen Beitrag zu leisten und sorgen für die fachliche Anbindung auch über die nationale Perspektive hinaus. Sie können ihre Beiträge auch im Rahmen von Unterstützungsschreiben ausdrücken. Zuwendungsempfänger sind an die Zuwendungsbestimmungen der DFG gebunden (siehe https://www.dfg.de/de/verwendungsrichtlinien-bedingungen-fuer-foerdervertraege-mit-der-deutschen-forschungsgemeinschaft-e-v-dfg-ueber-konsortien-im-rahmen-der-nationalen-forschungsdateninfrastruktur-246994), außerdem müssen die Zuwendungsempfänger antragsberechtigt sein (siehe DFG-Förderrichtlinien für die NFDI Abschnitt III (1) https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf).
{{</accordion-item>}}

{{<accordion-item title="Antragsstellung">}}
**Sind weitere Ausschreibungsrunden geplant?**

Nein, es ist keine weitere Ausschreibungsrunde im aktuellen Projekt geplant.

**Welche Rolle haben Fachverbände bei der Antragstellung?**

Den Fachverbänden und -verbünden kommt eine wichtige Scharnierfunktion bei Kooperationsprojekten zu: Als Teil der Plenarversammlung sind sie am Aufbau und an der Zusammensetzung der Komitees beteiligt, die die Priorisierung der Anträge vornehmen und die Ausschreibung begleiten. Daneben können Verbände und Verbünde auch Projektanträge unterstützen, z. B. durch die Abgabe von eigenen Einschätzungen zu den Projektanträgen im Rahmen eines Unterstützungsbriefes.

**Wie umfangreich soll der Antrag sein?**

Im Antragstemplate sind Angaben zu Seitenzahlen einzelner Abschnitte des Antrags hinterlegt. Dieses Template dient dazu, im Begutachtungsprozess einen schnellen Überblick über die eingereichten Projekte zu schaffen. Außerdem sollen die Seitenzahlen auch als Hinweise zum Umfang der jeweiligen Abschnitte verstanden werden. Antragstellende sollten sich an den dort angegebenen Umfängen orientieren. Im konkreten Fall können Rückfragen mit denjenigen geklärt werden, die die jeweilige Datendomäne koordinieren (vgl. hierzu unter dem Punkt „Antragstellung“ die Frage „Wer kann im Vorfeld der Antragstellung angesprochen werden, um die Vorgaben, Erwartungen, etc. bei der Planung und Konzeption der Kooperationsprojekte auf die Anforderungen von Text+ abzustimmen?“).

**Wer kann im Vorfeld der Antragstellung angesprochen werden, um die Vorgaben, Erwartungen, etc. bei der Planung und Konzeption der Kooperationsprojekte auf die Anforderungen von Text+ abzustimmen?**

Antragstellende, die ihren Projektvorschlag selbst einer der Domänen Sammlungen, lexikalische Ressourcen, Editionen und Infrastructure/Operations zuordnen können, wenden sich bitte an die jeweiligen, nachfolgend genannten Co-Sprechenden der entsprechenden Text+-Domäne. Sollten Sie Ihren Beitrag nicht eindeutig zuordnen können, wählen Sie einen der nachfolgend genannten Co-Sprechenden Ihrer Wahl; die Co-Sprechenden werden sich gegebenenfalls dazu abstimmen, wer Ihnen als Ansprechperson zur Verfügung stehen wird.

- Sammlungen: [Dr. Peter Leinen](https://www.dnb.de/SharedDocs/Kontaktdaten/DE/leinenPeter.html) (<p.leinen@dnb.de>), [Philippe Genêt](https://www.dnb.de/SharedDocs/Kontaktdaten/DE/genetPhilippe.html) (<p.genet@dnb.de>)
- Lexikalische Ressourcen: [PD Dr. Alexander Geyken](https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/geyken-alexander) (<geyken@bbaw.de>), [Axel Herold](https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/herold-axel) (<herold@bbaw.de>)
- Editionen: [Prof. Dr. Andreas Speer](https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/andreas-speer) (<andreas.speer@uni-koeln.de>), [Kilian Erasmus Hensen](https://cceh.uni-koeln.de/personen/kilian-hensen/) (<Kilian.Hensen@uni-koeln.de>) 
- Infrastruktur/Betrieb: [Prof. Dr. Philipp Wieder](https://gwdg.de/research-education/researchgroup_wieder/) (<philipp.wieder@sub.uni-goettingen.de>), [Stefan Buddenbohm](https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/stefan-buddenbohm-1/) (<buddenbohm@sub.uni-goettingen.de>)

Auch das Text+-Office steht Ihnen gerne unterstützend bei Rückfragen zur Verfügung <office@text-plus.org>.
{{</accordion-item>}}

{{<accordion-item title="Begutachtungs- und Bewertungsprozess">}}
**Wie werden Anträge begutachtet?**

Anträge können sich im Vorfeld einer Task Area zuordnen, die ihrem inhaltlichen Schwerpunkt entspricht, und Beratung bei der Antragsstellung erhalten. Die endgültige Zuordnung für das Begutachtungsverfahren übernimmt das Scientific Board von Text+. Die Begutachtung durch Text+ erfolgt in einem Begutachtungsverfahren unter Einbeziehung von externen Gutachtenden und Gutachtenden aus den jeweiligen Komitees, die eine Priorisierung der Projektvorschläge für jede Task Area vornehmen. Ggf. wird die Expertise aus anderen Task Areas einbezogen, insbesondere wird das Operations Coordination Committee die Anträge in Hinblick auf die technische Durchführbarkeit und Integrierbarkeit betrachten. Die Förderung erfolgt auf Grundlage der Empfehlungen des Text+ Scientific Boards nach einer Entscheidung der Leitungsgruppe von Text+, die unter Berücksichtigung aller eingegangenen Anträge und Gutachten getroffen wird.
{{</accordion-item>}}

{{<accordion-item title="Daten">}}

**Wie umfangreich sollen die Daten sein, die im Rahmen eines Kooperationsprojekts eingebracht werden können?**

Da es sehr unterschiedliche Datenarten gibt, die in Text+ adressiert werden, kann es keine allgemeine Aussage wie etwa in „Anzahl an Wörtern“, „Menge der Annotationen“, „Dateigröße in Gigabyte“ geben. Im konkreten Fall sollte zuvor das Gespräch mit denjenigen gesucht werden, die die jeweilige Datendomäne koordinieren (vgl. hierzu unter dem Punkt „Antragstellung“ die Frage „Wer kann im Vorfeld der Antragstellung angesprochen werden, um die Vorgaben, Erwartungen, etc. bei der Planung und Konzeption der Kooperationsprojekte auf die Anforderungen von Text+ abzustimmen?“).

**Welche Datenformate werden für Daten in Kooperationsprojekten von Text+-Partnern angenommen?**

Es gibt keine abgeschlossene Liste von empfohlenen und akzeptablen Datenformaten. Es wird empfohlen, sich an Formaten zu orientieren, die bereits von Partnern eingesetzt (und „für gut befunden“) wurden. Auch gibt es Standard-Formate, etwa der Text Encoding Initiative (TEI), des W3C und der ISO. Eine Faustregel ist, dass ein Format dann leichter zu integrieren ist, je näher es sich am Standard orientiert und es sich leicht in bereits innerhalb von Text+ verwendeten Formate konvertieren lässt. Im konkreten Fall sollte zuvor das Gespräch mit denjenigen gesucht werden, die die jeweilige Datendomäne koordinieren (vgl. hierzu unter dem Punkt „Antragstellung“ die Frage „Wer kann im Vorfeld der Antragstellung angesprochen werden, um die Vorgaben, Erwartungen, etc. bei der Planung und Konzeption der Kooperationsprojekte auf die Anforderungen von Text+ abzustimmen?“).

**Was bedeutet „Integration von Daten und Diensten in Text+“?**

Unter Integration werden die Maßnahmen verstanden, über die Daten und Dienste langfristig für Nutzende der Text+-Infrastruktur zugänglich gemacht werden. Dies kann zum Beispiel dadurch erfolgen, dass Daten von anderen Text+-Partnern gehostet werden. Eine andere Möglichkeit besteht darin, dass ein eigenes, nachhaltiges (zertifiziertes) Repositorium betrieben und über Schnittstellen an Text+ angebunden wird. Die Metadaten werden jeweils über entsprechende Schnittstellen bereitgestellt. Zur Integration gehört auch, dass keine Nachdigitalisierung nötig ist. 

Im Bereich von Software und Services bedeutet Integration, dass andere Angebote von Text+ diese Software und Services verwenden und sichergestellt werden kann, dass die Angebote langfristig in Text+ bereitgestellt werden, z. B. dadurch, dass die Angebote bei Konsortialpartnern von Text+ gehostet werden oder geplant ist, diese in Arbeitsgruppen von Text+ weiterzuentwickeln.  

**Wenn interessante Datensammlungen entdeckt werden, wie können sie über Text+ zugänglich gemacht werden? Kann man Text+ vorschlagen, sich um die Aufbereitung und Integration von Daten zu kümmern?**

Text+ legt den Schwerpunkt auf Forschungsdaten, die in digitalen Formaten vorliegen. Eine Digitalisierung durch Text+ ist derzeit nicht Teil des Angebotsportfolios. Wenn Ressourcentypen beigetragen werden sollen, die nicht im Schwerpunkt eines Daten- und Kompetenzzentrums von Text+ liegen, besteht eine Integrationsmöglichkeit darin, ein eigenes Repositorium für diese Datentypen aufzubauen. Eine Übernahme von Angeboten erfordert auf jeden Fall die Abklärung von Rechten und langfristigen Verantwortungen.
{{</accordion-item>}}

{{<accordion-item title="Formalia">}}
**Was konkret beinhaltet der Abschluss eines Kooperationsvertrages mit dem Leibniz-Institut für Deutsche Sprache?**

Kooperationsprojekte im Rahmen von Text+ beruhen auf den Verwendungsrichtlinien der DFG für NFDI-Konsortien (siehe https://www.dfg.de/de/verwendungsrichtlinien-bedingungen-fuer-foerdervertraege-mit-der-deutschen-forschungsgemeinschaft-e-v-dfg-ueber-konsortien-im-rahmen-der-nationalen-forschungsdateninfrastruktur-246994). Die Zusammenarbeit und Mittelweiterleitung ist in Text+ durch den Mittelweiterleitungs- und Kooperationsvertrag geregelt, der zwischen dem Leibniz-Institut für Deutsche Sprache und den jeweiligen Partnern inhaltsgleich bilateral geschlossen wird. Die Partner, die Kooperationsprojekte durchführen, schließen mit der antragstellenden Einrichtung von Text+ entweder einen Nachunternehmervertrag im Rahmen des Mittelweiterleitungs- und Kooperationsvertrags oder, wenn die Voraussetzungen dazu vorliegen, werden Teil des Text+-Konsortiums. Als Kontaktmöglichkeit für Fragen zum Vertrag steht das Text-Plus-Office (<office@text-plus.org>) zur Verfügung.

**Welcher Text+-Domäne (Sammlungen, lexikalische Ressourcen, Editionen, Infrastructure/Operations) wird ein Projektvorschlag zugeordnet?**

Die Struktur von Text+ richtet sich nach den Datendomänen Sammlungen, lexikalische Ressourcen und Editionen. Daneben gibt es übergreifend den Bereich „Infrastructure/Operations“ (vgl. hierzu die Beschreibung unter https://text-plus.org/ueber-uns/arbeitsbereiche/).
{{</accordion-item>}}

{{<accordion-item title="Mittel">}}
**Welche Kostenarten können beantragt werden?**

Gemäß den Förderrichtlinien der DFG können Personalkosten und Sachmittel (Reisemittel) beantragt werden. In der Summe sollten sie den Betrag von 65.000 Euro nicht überschreiten. Investitionsgüter (Server, Rechnerausstattung, Arbeitsplatz, Geräte, …) sind nicht förderfähig.
{{</accordion-item>}}

{{</accordion>}}
