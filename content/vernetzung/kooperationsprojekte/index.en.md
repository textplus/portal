---
title: Collaborative Projects

aliases:
- /research-data/collaborative-projects

menu:
  main:
    weight: 30
    parent: vernetzung
---

# Collaborative Projects

## Call for Proposals 2025: Processing and Securing Research Data in collaboration with Text+

Text+ is part of the German National Research Data Infrastructure (NFDI) with the goal of establishing a distributed research data infrastructure for humanities research data. At the current stage, it is focused on the data domains of digital collections, lexical resources, and editions.

The aim of this announcement is to continuously expand the offerings of data and services provided by Text+ and make them available to the research community in the long term. Alternatively, applications can be submitted that utilise the data and services available in Text+ specifically for innovative research questions. Likewise, own institutionalised and sustainable data centres with interfaces can be integrated into the technical infrastructure of Text+ in order to make the relevant data permanently available in the Text+ infrastructure. For software and other services, it is expected that the results will be provided under an open license, aligned with Text+ (contributing to coherence), and can be operated in the infrastructure in the long term. Following the FAIR and CARE principles is essential. According to the NFDI guidelines, only projects aiming at integration into the Text+ service portfolio are eligible for funding. The compilation and development of resources as such are to be financed with own funds.

Applications for funding can be submitted for one of the Task Areas: Collections, Lexical Resources, Editions, and the Task Area Infrastructure/Operation (see the description of the [Work Areas](en/ueber-uns/arbeitsbereiche/) in Text+). To prepare an application, a consultation with the respective Task Area is recommended. The list of contact people can be found at the end of this call.

In this call for applications, multiple projects can typically be funded with a volume between EUR 35,000 and EUR 65,000. Additionally, an overhead of 22% is granted on the project sum. The project duration is tied to the calendar year 2026, and thus, it is a maximum of 9 months (January to September); the transfer of unspent funds to the following year is not possible. After the completion of the funding, a project report is expected to be published on the Text+ website.

Any inquiries can be addressed to (<office@text-plus.org>). Further information on the application process see the [FAQ](/en/vernetzung/kooperationsprojekte/#faq). 


### Timeline

- Application deadline: March 31, 2025
- Announcement of results by the end of August 2025
- Earliest possible start of funding: January 1, 2026
- Latest possible end of funding: September 30, 2026


### Eligibility

Departments and working groups that have not yet been financially supported by Text+ but comply with the [DFG funding guidelines](https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf) for the NFDI (Section III (1)) are eligible to apply.

The evaluation process is led by the coordination committees of the respective work areas. If members of the coordination committees are involved in applications, these members will be excluded from the evaluation process in the respective year.

Funding requires the conclusion of a subcontracting agreement within the framework of the fund transfer and cooperation agreement in the NFDI project with the Leibniz Institute for the German Language Mannheim as the main applicant institution of Text+. If the necessary conditions are met, especially a long-term and permanent contribution to the Text+ consortium beyond the cooperation project, an alternative is joining the fund transfer and cooperation agreement (and thus the Text+ consortium). However, repeated funding of the same project as a cooperation project is not possible.

### Selection criteria

The evaluation of applications will be based on the following criteria:

- Linking and integration into the Text+ infrastructure according to the FAIR and CARE principles and the expansion of the Text+ service portfolio regarding data or services within the funding period.
- Relevance and innovation:
  - Potential scientific contribution of data or services.
  - Clear problem description and relevance to the working programs of the Text+ consortium.
  - Reusability and accessibility of data or services.
  - Scientific quality of the project.
- Contribution to diversity and representativeness of the Text+ data offering.
- Estimation of effort:
  - Appropriateness of the requested funding amount for realization.
  - Preliminary work and contributions of the applicants.


### Contact


- Collections: [Dr. Peter Leinen](https://www.dnb.de/SharedDocs/Kontaktdaten/DE/leinenPeter.html) (<p.leinen@dnb.de>), [Philippe Genêt](https://www.dnb.de/SharedDocs/Kontaktdaten/DE/genetPhilippe.html) (<p.genet@dnb.de>)
- Lexical Resources: [PD Dr. Alexander Geyken](https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/geyken-alexander) (<geyken@bbaw.de>), [Axel Herold](https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/herold-axel) (<herold@bbaw.de>)
- Editions: [Prof. Dr. Andreas Speer](https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/andreas-speer) (<andreas.speer@uni-koeln.de>), [Kilian Erasmus Hensen](https://cceh.uni-koeln.de/personen/kilian-hensen/) (<Kilian.Hensen@uni-koeln.de>) 
- Infrastructure/Operation: [Prof. Dr. Philipp Wieder](https://gwdg.de/research-education/researchgroup_wieder/) (<philipp.wieder@sub.uni-goettingen.de>), [Stefan Buddenbohm](https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/stefan-buddenbohm-1/) (<buddenbohm@sub.uni-goettingen.de>)


### Application form

Please use the [Word form](/vernetzung/kooperationsprojekte/Call2025_Template-Kooperationsprojekte_dt.docx "download") for your proposal.
Any questions can be addressed to <office@text-plus.org>.

&nbsp; 

## Call for Proposals 2024

One project from each Text+ Task Area, i.e. Collections (= Col), Editions (= Ed), Lexical Resources (= LR), and Infrastructure/Operations (= I/O), was considered.

### Funded Collaborative Projects

- **Text+ interfaces to the interview collections in Oral-History.Digital (text+oh.d)**
  - **Institution:** University Library of the Free University of Berlin
  - **Project Leadership:** Dr. Cord Pagenstecher
  - **Task Area in Text+:** Collections (Col)

- **Glossarium Graeco-Arabicum - Open Data (GlossGA - OD)**
  - **Institution:** Friedrich-Alexander-University Erlangen-Nuremberg 
  - **Project Leadership:** Dr. Rüdiger Arnzen
  - **Task Area in Text+:** Lexical Resources (LR)

- **HAdW GND-based web services - Beaconizer & Discoverer (Hagrid)**
  - **Institution:** Heidelberg Academy of Sciences and Humanities
  - **Project Leadership:** Dr. Frank Grieshaber
  - **Task Area in Text+:** Infrastructure/Operations (I/O)

- **Development of an open digital collection of historical music theory texts from the German-speaking world based on examples from the 19th century (DigiMusTh)**
  - **Institution:** Julius-Maximilians-Universität Würzburg
  - **Project Leadership:** Prof. Dr. Fabian C. Moss
  - **Task Area in Text+:** Collections (Col)

- **LOD role modeling from the registers of Regestenwerke zum Mittelalter (LRM)**
  - **Institution:** Academy of Sciences and Literature Mainz
  - **Project Leadership:** Prof. Dr. Andreas Kuczera
  - **Task Area in Text+:** Editions (Ed)

---

## Call for Proposals 2023

One project from each Text+ Task Area, i.e. Collections (= Col), Editions (= Ed), Lexical Resources (= LR), and Infrastructure/Operations (= I/O), was considered.

### Funded Collaborative Projects

- **The Beria Collection in the Language Archive Cologne: Expansion, revision, and evaluation of a data collection of an under-described African language (BeriaCollection)**
  - **Institution:** University of Cologne, Institute of Linguistics
  - **Project Leadership:** Prof. Dr. Birgit Hellwig, Dr. Isabel Compes
  - **Task Area in Text+:** Collections (Col)

- **Thesaurus Linguae Aegyptiae – More Fair with APIs (LR TeLApi)**
  - **Institution:** Berlin-Brandenburg Academy of Sciences and Humanities, Center for Research in the Ancient World
  - **Project Leadership:** Dr. Daniel Werning
  - **Task Area in Text+:** Lexical Resources (LR)

- **The oldest Görlitz City Book 1305-1416: Transformation, curation, and digital publication (data, web application) of an extraordinary book edition for the historical disciplines (RedBook1305)**
  - **Institutions:** University of Wuppertal / Martin Luther University Halle-Wittenberg
  - **Project Leadership:** Prof. Dr. Patrick Sahle, Dr. Christian Speer
  - **Task Area in Text+:** Editions (Ed)

- **Tool support for the automatic extraction of table data from historical newspapers (TabellenExtraktion)**
  - **Institution:** Wismar University
  - **Project Leadership:** Prof. Dr.-Ing. Frank Krüger
  - **Task Area in Text+:** Infrastructure/Operations (I/O)

---

## Call for Proposals 2022

Each collaborative project is associated with the assigned Text+ Task Area, i.e. Collections, Editions, and Lexical Resources. Assignments to the Task Area Infrastructure/Operation were only possible from the 2nd call onward.

### Funded Collaborative Projects

- **Unlocking Disk Magazines from the 1980s and 1990s: Re-Digitizing Digital Heritage from the Pioneer Era of Home Computers for Text-Based Diskmag Research (Diskmags)**
  - **Task Area:** Collections
  - **Institution:** University of Würzburg
  - **Project Leader:** Torsten Roeder

- **Open Access to DLA Marbach Data Based on Research Project-Related Questions. Developed Using the Example of Data from the Exile Library Source Repertory (Alfred Döblin and Siegfried Kracauer) (DLA Data+)**
  - **Task Area:** Collections
  - **Institution:** German Literature Archive Marbach
  - **Project Leader:** Karin Schmidgall

- **Integration of the Digital Program Archive of German Adult Education Centers into the NFDI Consortium Text+ (DiPA+)**
  - **Task Area:** Collections
  - **Institution:** German Institute for Adult Education
  - **Project Leader:** Kerstin Hoenig

- **KOLIMO+. Optimization and Open Publication of the Corpus of Literary Modernism (KOLIMO+)**
  - **Task Area:** Collections
  - **Institution:** University of Bielefeld
  - **Project Leaders:** Berenike Herrmann, Alan Lena van Beek

- **Modeling of Text Editions as Linked Data (edition2LD)**
  - **Task Area:** Editions
  - **Institution:** Heidelberg Academy of Sciences
  - **Project Leaders:** Dieta Svoboda-Baas and Sabine Tittel

- **Pessoa Digital**
  - **Task Area:** Editions
  - **Institution:** University of Rostock
  - **Project Leader:** Ulrike Henny-Krahmer

- **European Peace Treaties of the Premodern Era in Data (FriVer+)**
  - **Task Area:** Editions
  - **Institution:** Leibniz Institute for European History (IEG)
  - **Project Leader:** Thorsten Wübbena

- **Text+-Interface for the Middle High German Dictionary (MWB-APIplus)**
  - **Task Area:** Lexical Ressources
  - **Institution:** University of Trier / Göttingen Academy
  - **Project Leaders:** Ute Recker-Hamm, Ralf Plate, Jonas Richter

- **Integration of Lower Sorbian-German Dictionaries as Lexical Resources in Text+ (INSERT)**
  - **Task Area:** Lexical Ressources
  - **Institution:** Sorbian Institute
  - **Project Leader:** Hauke Bartels

- **Corpus Glossariorum Latinorum Online Lemmatization (CGLO)**
  - **Task Area:** Lexical Ressources
  - **Institution:** Bavarian Academy of Sciences
  - **Project Leader:** Adam Gitner

---

## FAQ

{{<accordion>}}
{{<accordion-item title="Goal of Collaboration Projects">}}
**What is the goal of collaboration projects?**

Collaboration projects within Text+ aim to expand the service portfolio of Text+. Text+ is open to new offerings and collaborators, not limiting itself to the offerings of initial partners. It can integrate additional data, services, and resources as part of its infrastructure. Applicants can become part of the Text+ network, participate in further infrastructure development, and benefit from ongoing developments. The funding period is 9 months (January to September 2026).
{{</accordion-item>}}

{{<accordion-item title="Eligibility">}}
**If a proposal for a collaboration project is not funded in one year, can the proposal be resubmitted in a later round? Can a proposal be submitted in subsequent rounds if funding was not possible earlier?**

Certainly. During the project evaluation, a priority list of proposals is established, which can be approved until the respective budget for a funding round is exhausted. This does not necessarily mean that a project not funded in one year is ineligible for future funding. Of course, applicants can strengthen and update their proposals before resubmission. In specific cases, discussions with the coordinators of the respective Task Areas (see "Who can be contacted before submitting the application to align the planning and conception of collaboration projects with the requirements of Text+?" under the "Application Process" section) should be initiated.

**Who can apply? Can students/Ph.D. candidates/initiatives submit an application?**

Eligible applicants are departments and working groups who are not financially supported by Text+, but who meet the criteria laid down in the DFG funding guidelines for the NFDI (Section III (1), see https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf). Individual researchers and consortia of researchers (e.g., in joint projects) can submit proposals through their university or research institution if they belong to the institutions mentioned in the DFG guidelines. Many universities and research institutions have dedicated departments dealing with research funding and third-party projects. Researchers should contact the respective department at their home institution to clarify the conditions for submitting a proposal.

**Can individual applicants submit a proposal, or are only teams eligible? Can two researchers submit a proposal?**

There are no specifications regarding the size of a team conducting a collaboration project. However, eligible applicants are limited to departments and working groups who are not financially supported by Text+, but who meet the criteria laid down in the DFG funding guidelines for the NFDI (Section III (1), see https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf). Individual researchers and consortia of researchers (e.g., in joint projects) can submit proposals through their university or research institution if they belong to the institutions who meet the criteria in the DFG guidelines. In specific cases, discussions with the coordinators of the respective Task Areas (see "Who can be contacted before submitting the application to align the planning and conception of collaboration projects with the requirements of Text+?" under the "Application Process" section) should be initiated.

**Can researchers from other countries participate in a proposal?**

Researchers from abroad can contribute significantly to some projects, ensuring expertise beyond the national perspective. Their contributions can also be expressed through support letters. Recipients of funding are bound by the DFG's funding regulations (see https://www.dfg.de/de/verwendungsrichtlinien-bedingungen-fuer-foerdervertraege-mit-der-deutschen-forschungsgemeinschaft-e-v-dfg-ueber-konsortien-im-rahmen-der-nationalen-forschungsdateninfrastruktur-246994). Additionally, only applications from eligible applicants will be considered (see DFG funding guidelines for the NFDI Section III (1), https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf).
{{</accordion-item>}}

{{<accordion-item title="Application Process">}}
**Are further application rounds planned?**

No, there is no further application round planned for the current project.

**What role do professional associations play in the application process?**

Professional associations and alliances play a crucial role in collaboration projects. As part of the plenary assembly, they are involved in the establishment and composition of committees that prioritize proposals and accompany the announcement. Moreover, associations and alliances can support project proposals by providing their own assessments of the project proposals through support letters.

**How long should the application be?**

The application template includes information on page numbers for individual sections of the application. This template serves to provide a quick overview of submitted projects during the evaluation process. The page numbers also serve as indications of the extent of each section. Applicants should align with the specified page limits. In specific cases, questions can be clarified with the coordinators of the respective Task Areas (see "Who can be contacted before submitting the application to align the planning and conception of collaboration projects with the requirements of Text+?" under the "Application Process" section).

**Who can be contacted before submitting the application to align the planning and conception of collaboration projects with the requirements of Text+?**

Applicants who can assign their project proposal to one of the domains (Collections, Lexical Resources, Editions, Infrastructure/Operations), should contact the coordinators for the relevant Text+ domains as listed below. If you cannot clearly assign your contribution, contact the coordinator of your choice; the coordinators will then decide between them who would be available as a contact person for you.

- Collections: [Dr. Peter Leinen](https://www.dnb.de/SharedDocs/Kontaktdaten/DE/leinenPeter.html) (<p.leinen@dnb.de>), [Philippe Genêt](https://www.dnb.de/SharedDocs/Kontaktdaten/DE/genetPhilippe.html) (<p.genet@dnb.de>)
- Lexical Resources: [PD Dr. Alexander Geyken](https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/geyken-alexander) (<geyken@bbaw.de>), [Axel Herold](https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/herold-axel) (<herold@bbaw.de>)
- Editions: [Prof. Dr. Andreas Speer](https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/andreas-speer) (<andreas.speer@uni-koeln.de>), [Kilian Erasmus Hensen](https://cceh.uni-koeln.de/personen/kilian-hensen/) (<Kilian.Hensen@uni-koeln.de>) 
- Infrastructure/Operation: [Prof. Dr. Philipp Wieder](https://gwdg.de/research-education/researchgroup_wieder/) (<philipp.wieder@sub.uni-goettingen.de>), [Stefan Buddenbohm](https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/stefan-buddenbohm-1/) (<buddenbohm@sub.uni-goettingen.de>)

Questions can also be addressed to the Text+ Office (<office@text-plus.org>).
{{</accordion-item>}}

{{<accordion-item title="Evaluation and Assessment Process">}}

**How are applications evaluated?**

Applications can be assigned to a specific Task Area that corresponds to their thematic focus in advance, and applicants can seek advice during the application process. The final allocation to a Task Area for the evaluation process is carried out by the Scientific Board of Text+. Text+ conducts the evaluation process involving external evaluators and evaluators from the respective committees, who prioritize the project proposals for each Task Area. If necessary, expertise from other Task Areas may be considered, with the Operations Coordination Committee specifically examining applications for technical feasibility and integrability. Funding is granted based on the recommendations of the Text+ Scientific Board following a decision by the Text+ leadership group, taking into account all received applications and evaluations.
{{</accordion-item>}}

{{<accordion-item title="Data">}}
**How extensive should the data that can be contributed within a collaboration project be?**

As there are various types of data addressed by Text+, there can be no general statement such as "number of words," "quantity of annotations," or "file size in gigabytes." In specific cases, discussions should be initiated with the coordinators of the respective Task Areas (see "Who can be contacted before submitting the application to align the planning and conception of collaboration projects with the requirements of Text+?" under the "Application Process" section).

**Which data formats are accepted for data in collaboration projects with Text+ partners?**

There is no definitive list of recommended and acceptable data formats. It is advisable to align with formats already used (and found to be suitable) by partners. There are also standard formats, such as those of the Text Encoding Initiative (TEI), W3C, and ISO. A general rule is that a format is easier to integrate if it aligns closely with the standard and can be easily converted into formats already used within Text+. In specific cases, discussions should be initiated with the coordinators of the respective Task Area (see "Who can be contacted before submitting the application to align the planning and conception of collaboration projects with the requirements of Text+?" under the "Application Process" section).

**What does "Integration of data and services in Text+" mean?**

Integration refers to measures through which data and services are made accessible to users of the Text+ infrastructure in the long term. This can be achieved, for example, by hosting the data in a repository provided by a Text+ partner. Another possibility is to operate a sustainable (certified) repository and connect it to Text+ through interfaces. Metadata is provided through respective interfaces. Integration also involves avoiding the need for post-digitization.

In the realm of software and services, integration means that other offerings from Text+ use these software and services, ensuring that the offerings are provided in Text+ in the long term. This can be achieved, for instance, by hosting the offerings at Text+ consortium partners or planning to further develop them in Text+ working groups.

**If interesting data collections are discovered, how can they be made accessible via Text+? Can one suggest Text+ take care of the preparation and integration of data?**

Text+ focuses on research data available in digital formats. Digitization by Text+ is currently not part of the service portfolio. If resource types are to be contributed that do not align with the focus of a Text+ data and competence center, one integration possibility is to establish a separate repository for these data types. Adopting offerings requires clarification of rights and long-term responsibilities.
{{</accordion-item>}}

{{<accordion-item title="Formalities">}}
**What specifically does the conclusion of a cooperation agreement with the Leibniz Institute for the German Language entail?**

Collaboration projects within Text+ are based on the usage guidelines of the DFG for NFDI consortia (see https://www.dfg.de/de/verwendungsrichtlinien-bedingungen-fuer-foerdervertraege-mit-der-deutschen-forschungsgemeinschaft-e-v-dfg-ueber-konsortien-im-rahmen-der-nationalen-forschungsdateninfrastruktur-246994). Cooperation and fund transfer in Text+ are regulated by the fund transfer and cooperation agreement, which is bilaterally concluded between the Leibniz Institute for the German Language and the respective partners. Partners conducting collaboration projects with Text+ either enter into a subcontractor agreement within the framework of the fund transfer and cooperation agreement or, if the conditions are met, become part of the Text+ consortium. The Text+ Office (<office@text-plus.org>) is available for questions regarding the agreement.

**To which Text+ domain (Collections, Lexical Resources, Editions, Infrastructure/Operations) will a project proposal be assigned?**

The structure of Text+ is based on the Task Areas of Collections, Lexical Resources, and Editions. Additionally, there is the overarching domain of "Infrastructure/Operations" (see the description at https://text-plus.org).
{{</accordion-item>}}

{{<accordion-item title="Funding">}}
**Which types of costs and expenses can be covered by the funding?**

According to the funding guidelines of the DFG, staff expenses and direct project costs can be requested. In total, they should not exceed the amount of 65,000 euros. Capital goods (servers, computing equipment, workplaces, devices, etc.) are not eligible for funding.
{{</accordion-item>}}

{{</accordion>}}
