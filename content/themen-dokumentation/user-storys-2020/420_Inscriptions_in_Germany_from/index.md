---
title: "Inscriptions in Germany from the Middle Ages to Early Modern Times (Die Deutschen Inschriften)"

type: user-story

slug: user-story-420

aliases:
- /en/research-data/user-story-420

dfg_areas:
- 102 Geschichtswissenschaften
- 103 Kunst-, Musik-, Theater- und Medienwissenschaften
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft
- 107 Theologie
- 108 Philosophie

text_plus_domains:
- Editionen

authors:
- Arbeitsstelle Inschriften
- Nordrhein-Westfälische Akademie der Wissenschaften und der Künste (University of Bonn)

---




#### Motivation


As an epigraphist I am part of a larger research team working for a project on inscriptions originating from the German-speaking areas in the period from the Middle Ages up to Early Modern Times. Here, in North Rhine-Westphalia we are focussing on urban centres. We study the wide range of writings in Latin and German placed on all kinds of objects in various manufacturing techniques and materials, from everyday items to precious artefacts. 

Inscriptions are important historical sources of great interdisciplinary value. By the inventory catalogue, critical text edition and additional commentary the inscriptions’ content and function become accessible for various scientific disciplines, such as History, Art History, Latin and German Philology, Cultural Anthropology, Philosophy, Theology and Church History. Equally, an essential genre of endangered cultural heritage is documented by text and image data and in some cases by 3D capturing of epigraphical objects. 

First step of the data capture is the systematic search for inscriptions in the relevant inventories, databases, publications or archival material like historical itineraries. Considered are original inscriptions still extant as well as copies of lost epigraphical evidence coming down to us via historical descriptions, drawings and photographs. Data are collected from handwritten, printed and digital sources. This phase is stocktaking in spreadsheet-form. 

During the following fieldwork the extant inscriptions are inspected at their current locations, described and documented by digital photographic images. Epigraphical metadata recorded “on-site” are the text transcription and the material, technique, dimensions, state of preservation of the inscription-bearing object and the writings. 

For the badly preserved or lost inscriptions further data, textual descriptions and images, must be added from archives, museums, libraries and special image-providing repositories. Ideally this source material would be digitized and reusable but that’s not to be taken for granted. 

Within the project, there are established guidelines for the systematic cataloguing and critical text edition of inscriptions, and there are several structured publication workflows for a printed book series and an online edition embedded in a virtual research environment. 

#### Objectives 


Epigraphical artefacts are a very specific textual genre, because the understanding of inscriptions crucially depends on the object they are written upon, their location and placement in the light of its historical context. Due to the long duration of the project a wide range of data has already been published with a high potential for connections between the NFDI initiatives in the Humanities. As to the planned NFDI4Memory one field could be Prosopography, since inscriptions name a lot of persons (for example as donor or creator of a work). A common domain with the planned NFDI4Objects would be the Ancient Epigraphy (for example with regard to the relation between inscription and the material bearing-object). Looking at medieval and early modern inscriptions we are faced with a research object of great variety, such as inscriptions on grave slabs, church bells, liturgical objects and vestments as well as for instance writings within a sculptural programme on the front of a town hall. This broad spectrum of inscription-bearing objects is closely corresponding to the data material the NFDI4Culture will be dealing with. The materiality of epigraphical evidence is an important connecting topic. A common need and data type are the digital representations of the cultural assets which provide an essential basis for the scientific work and its visualisation. 

Dealing with a very heterogeneous data situation poses a great challenge. The catalogue and the critical scholarly edition are composed by enrichment of metadata from different resources. Key aspects are the text (wording, language, linguistic form, indication of sources, references), the script (script type, palaeographical features and estimate of date) and the inscription-bearing object (object type, art form, material, technique, function). Some team members use traditional text processing software, others a special research software, developed in the project, which provides a standardized and need-driven workflow for the digital edition and publication of medieval and early modern inscriptions online as well as in print. This tool facilitates structured work and data management for the individual users as well as collaborative teams, but its usefulness depends on common agreements about controlled vocabularies, terminologies and standards. Up to now the discussions about these issues have been mainly project-centred and domain-specific. We hope that Text+ will, together with other NFDI initiatives in the Humanities, encourage the collaborative use of knowledge by linked open data and open access; and we need a central contact point for reliable information about the essential resources to find relevant research data easier and quicker. One great benefit of this would be the possibility to share knowledge and discuss and develop common standards for digital epigraphy (e.g. EpiDoc, CIDOC CRM etc.) together with researchers dealing with comparable epigraphical resources from other fields. This could include historical epochs beyond Antiquity and the Middle Ages and other cultural areas beyond Europe. 

