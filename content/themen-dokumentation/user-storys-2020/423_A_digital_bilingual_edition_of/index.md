---
title: "A digital bilingual edition of the scientific notes of a German scientist on his research in Ethiopia – Georg Wilhelm Schimper: In Abyssinia. Observations on Tigre, (edited by Andreas Gestrich and Dorothea McEwan in collaboration with Stefan Hanß)"

type: user-story

slug: user-story-423

aliases:
- /en/research-data/user-story-423

dfg_areas:
- 104 Sprachwissenschaften

text_plus_domains:
- Editionen

authors:
- Christina von Hodenberg
- Markus Mößlang (Deutsches Historisches Institut London)

---




#### Motivation


Within the framework of a co-operative project by the British Library, the German Historical Institute London, and the Royal Botanic Gardens, Kew, a team of historians, linguists, librarians and information scientists wanted to publish a bilingual digital edition (German and English) of the records of the German botanist and researcher of Ethiopian wildlife and culture Georg Heinrich Wilhelm Schimper (1804-1878). The project arose within the context of the DHI London’s interest in forms of colonial knowledge production as well as Anglo-German relations in this area.

Schimper’s notes, written in the course of a few years from 1864 to 1868, contain detailed botanical, geological and ethnological descriptions of his excursion to a part of the Ethiopian highlands. Since Schimper, who was one of the founders of modern plant ecology and is still considered the leading botanist for Ethiopia, the rediscovery of this manuscript in 2008 is of great importance for African botanists and geographers. Furthermore, this manuscript—which not only provides a description of the plants but also of their locations and precise maps of the investigated terrain as well as numerous historical and especially ethnographic observations—is one of the few secular sources on Ethiopia from this period.

### Objectives


The objective of the project was to create a digital edition with the following features:
* virtual presentation of the digitised images of the handwritten texts in German;
* virtual presentation of the transcription of the German text, footnoted and annotated, into a typed version;
* virtual presentation of the English translation of the German text in modern Standard English;
* an alphabetical index of botanical names used by Schimper, which can be searched online and is linked to the digitised specimens of the herbaria at the Royal Botanic Gardens, Kew;
* virtual presentation of the digitised images of the maps drawn by Schimper;
* linkage of tagged maps (place names) to the transcribed texts;
* an index of all place names in the texts and on the maps, which can be searched online.

The digital edition was to reproduce the German manuscript in a transcription true to the letter. At the same time, it should offer a modern English translation and the digitised original. Moreover, the aim was to link the botanical and geographical names included in the edition to the maps originally drawn by Schimper himself.

In addition to its historical, philological and linguistic value, the edition was also to provide new impulses for other areas of research:
* The plant-ecological exploration of Schimper’s herbarium; his diary contains important information about the history of the settlement of Tigre, and is therefore interesting for geographers and regional historians.
* The digital edition is an important basis for research contributions to the history of knowledge production in colonial contexts.

### Solution


The Digital Edition [*Georg Wilhelm Schimper – In Abyssinia. Observations on Tigre*](exist.ghil.ac.uk:8079/Schimper/) was based on the software “Digitale Editionen Neuzeitlicher Quellen” (DENQ) [[1]](#_ftn1) developed by Jörg Hörnschemeyer for the German Historical Institutes in Rome and London. Apart from the digital Edition itself, information on Schimper’s Biography, the maps designed by him, the plants described in his works and a bibliography were made available online. Furthermore, functions for searching and browsing the digital edition were included.

 ![Screenshot Georg Wilhelm Schimper - In Abyssinia. Observations on Tigre](Screenshot-Georg-Wilhelm-Schimper-In-Abyssinia.-Observations-on-Tigre-1024x447.png " Screenshot of the digital edition, manuscript and transcription")

 [[1]](#_ftnref1) For details about DENQ, see Jörg Hörnschemeyer: DENQ – Software für die Online-Edition, in: *Bleibt im Vatikanischen Geheimarchiv vieles zu geheim? Historische Grundlagenforschung in Mittelalter und Neuzeit*. Beiträge zur Sektion des Deutschen Historischen Instituts (DHI) Rom, organisiert in Verbindung mit der Westfälischen Wilhelms-Universität Münster, Seminar für Mittlere und Neue Kirchengeschichte. 47. Deutscher Historikertag, Dresden 30. September-3. Oktober 2008, hg. von Michael Matheus und Hubert Wolf, Rom 2009, pp. 13-18. URL:  [http://www.dhi-roma.it/Historikertag_Dresden.html](http://www.dhi-roma.it/Historikertag_Dresden.html).

