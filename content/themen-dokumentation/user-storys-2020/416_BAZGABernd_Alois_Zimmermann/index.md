---
title: "BAZ-GA/Bernd Alois Zimmermann: Requiem für einen jungen Dichter. Critical Edition of Tape Music"

type: user-story

slug: user-story-416

aliases:
- /en/research-data/user-story-416

dfg_areas:
- 103 Kunst-, Musik-, Theater- und Medienwissenschaften

text_plus_domains:
- Editionen

authors:
- Matthias Pasdzierny (Universität der Künste, Berlin)

---




#### Motivation


German-speaking musicology has a long tradition of critical-scientific editions of written compositions. Many of these editions, e.g. in the field of music theater, contain textual components that are already integrated into digital and hybrid editions. In addition, there are numerous musicological letter and script editions, some of which are also available in digital or hybrid form. What is still missing are methods, tools and standards for editions of such works that contain audio components with spoken and/or sung texts, for example on tape or other storage media. These elements play a central role in music after 1945. This is also the case with Bernd Alois Zimmermann’s *Requiem for a Young Poet**[Requiem**für****einen****jungen****Dichter**]*, where numerous recorded text passages and historical voice recordings (politicians’ speeches, etc.) are incorporated into a playback tape. The respective text passages gain their special meaning and integrity for the work from the detailed voice coloration of the speakers. In the case of the historical sound bites, this applies to the media-historical and technical constellations of their production (such as the sound of shortwave radio stations). The recording and labelling of such characteristics is therefore of fundamental importance for the critical-scientific edition of such compositions.  

#### Obejctives


How can works like the one described above be included in critical-scientific editions? How can the media/audio components of recorded texts be related to the underlying texts and their edition? How must tagging tools and data standards for these needs be designed in addition to existing approaches (TEI, audio editions of literary and media studies, Edirom Tools) so that the resulting research data are accessable and interoperable in the long term? There is a special need for an annotation tool for audio data that also includes sound aspects for recorded texts. 

#### Solution


An interdisciplinary discussion on the standardization and availability of research data for examples such as the above should be initiated and moderated by the Text+ consortium. The goal is to develop standards for the research data of critical-scientific editions with audio components. For the development of the editions and their digital publication, presentation will also require at least extensions of the existing possibilities (e.g. Edirom, existing transcription and tagging models from linguistics). It would also make sense to create an interface and/or exchange with NFDI4Culture. 

#### Challenges


Numerous works of music after 1945 are protected by copyright. In the case of recordings and playback components used in the context of a work, extensive ancillary copyrights and performance rights are sometimes added. Therefore, a flexible and at the same time sustainable rights management for the edition of such works must be developed. In addition, there are questions of technical liability for the selected digital publication tools and, in the case of subscription models, user and license management.  

#### Review by community


The developed solutions and tools should be reusable for similar projects. The developed standards should be usable for a variety of audio and/or audiovisual data in connection with recorded texts. 

