---
title: "Science in Ancient Egypt"

type: user-story

slug: user-story-341

aliases:
- /en/research-data/user-story-341

dfg_areas:
- 101 Ancient Cultures

text_plus_domains:
- Collections

authors:
- Peter Dils and Dirk Goldhahn (Saxon Academy of Sciences and Humanities in Leipzig)

---




#### Motivation


The project hosting the  [website “Science in Ancient Egypt”](https://sae.saw-leipzig.de/) at the Saxon Academy of Sciences and Humanities in Leipzig provides public access to the ancient texts containing epistemological knowledge of Pharaonic Egypt. The metadata for each text is provided simultaneously with a comprehensive bibliography. This and other metadata thesauri are also used in the joint Egyptological text database  [“Thesaurus Linguae Aegyptiae”](https://aaew.bbaw.de/tla/) hosted by the partner institute at the Berlin-Brandenburg Academy of Sciences and Humanities. 

This has resulted in various domain-specific metadata thesauri, which cover aspects such as dating and repository/collection. 

#### Objectives


A possibility to publish these thesauri or vocabularies in a standardized way would be desirable. They should also be linked to or compared with other vocabularies (such as Wikidata). Finally, the multilingualism of the resources should be taken into account and built up. In the overall process, considerations of quality control play a decisive role. 

#### Solution


Support from Text+ would be desirable, since the mentioned processes are likely to occur in many projects and human resources are limited. Specifications/recommendations for recurring processes, possible formats, recommended filing routines (repositories/thesaurus collections that guarantee searchability) and further proposals for standardization should be provided. The provision or recommendation of suitable tools for acceleration and simplification processes would also be of interest. 

We have already taken the necessary first steps in the context of our cooperation with the  [Thot project](https://thot.philo.ulg.ac.be/project.html), including the selection of standards for thesauri and of the necessary standards and formats, as well as potential software. In the absence of a framework of recommendations and guidelines, this process has probably already consumed an unnecessary amount of human resources. 

#### Challenges


Unfortunately, the timely practical implementation of the project is endangered by limited personnel resources. 

#### Review by Community


We would like to evaluate solutions proposed by Text+ for the aforementioned problems and, if possible, integrate them within our own decisions regarding their implementation. 

