---
title: "Making Sanskrit text sources available as electronic texts"

type: user-story

slug: user-story-318

aliases:
- /en/research-data/user-story-318

dfg_areas:
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft

text_plus_domains:
- Collections

authors:
- Jonas Buchholz (Heidelberg University)

---




#### Motivation


 ![Title page of the 1889 edition of the Kāñcīmāhātmya in Telugu script](Collections_Buchholz_Bild-635x1024.png) 



In the scope of my current DFG-funded research project I am working on a Sanskrit text, titled *Kāñcīmāhātmya*, which narrates the legendary stories of a number of Hindu temples in the South Indian city of Kanchipuram. This text is available through two printed editions, published in 1889 and 1967, respectively (see Fig. 1). However, both of these editions are very rare: copies are held only by few libraries in India. Moreover, both editions are printed in the Telugu script, a local script used in parts of South India, which, however, only few Indologists are aquainted with. This means that the text on which my research is based is not readily available for other scholars: most of them have no access to the editions of the text, and even if they had, they could not easily read the text because of the unfamiliar script in which the editions are printed. 

My motivation is to make *Kāñcīmāhātmya* easily accessible to other scholars to allow them to consult the text passages to which I refer in my publications to check my research findings, or perhaps even to independently engage with the same source material. This problem touches the more general question of reproducibility in the Humanities. By making my source material available, I hope to make the results of my research more easily verifiable. Similar issues might be relevant for other scholars in the field of Indology who work on rare texts in Sanskrit or other Indian languages. 

*Fig. 1: Title page of the 1889 edition of the Kāñcīmāhātmya in Telugu script.*

#### Objectives 


To make the *Kāñcīmāhātmya*available to other scholars, it would be ideal to publish it as electronic texts in an online repository. Currently, an electronic text of the *Kāñcīmāhātmya*in Roman transliteration is under preparation by the project team. When finished, this resource could be made accessible. What is an open question, however, is which platform could be used to make the electronic text available. Since developing a new platform is clearly not feasible within the limited scope of a DFG project, a pre-existing text repository would have to be made use of. This repository would have to meet the principles for the handling of research data required by the DFG, i.e, it should be openly accessible, sustainably stored, and comply with accepted data standards. Ideally, the repository should also be known among the Indological commmunity to ensure acceptance by other scholars in the field. 

#### Solution 


A possible solution for making the electronic text of the *Kāñcīmāhātmya*available would be to include it in the Göttingen Register of Electronic Texts in Indian Languages ( [GRETIL](https://gretil.sub.uni-goettingen.de/gretil.html)). GRETIL is, to my knowledge, the most comprehensive repository for electronic texts in the field of Indology, and certainly the best known one: within the Indological community, GRETIL is widely known and accepted as a valuable resource. Making the electronic text available on GRETIL would thus mean making use of an existing repository that is well-known among scholars of the field, thus maximizing the visibility of the output. 

#### Challenges 


While GRETIL is a valuable and widely accepted resouce, it, as far as I can judge, might not represent the technical state of the art. For example, GRETIL does not seem to support permanently citable addressing, e.g. through Digital Object Identifiers (DOIs). It is also unclear to me how sustainably the GRETIL data is stored. In this respect, it would be highly welcome if GRETIL could be updated to comply with best practices in data management. In this way, it would be possible to strengthen an existing community-driven resource to ensure that it will remain fit for the future. 

