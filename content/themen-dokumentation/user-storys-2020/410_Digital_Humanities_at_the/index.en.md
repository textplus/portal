---
title: "Digital Humanities at the Herzog August Bibliothek Wolfenbüttel"

type: user-story

slug: user-story-410

aliases:
- /en/research-data/user-story-410

dfg_areas:
- Interdisciplinary

text_plus_domains:
- Editions
- Comprehensive

authors:
- Daniela Schulz
- Hartmut Beyer (Herzog August Bibliothek Wolfenbüttel)

---




#### Motivation


As project manager of digital editions at one of the big research libraries of Germany, I would like to make my own work more visible to the public, and enhance my skills to be able to keep up with the ongoing technical developments. At the library, we develop and curate several digital editions as well as other resources mainly in TEI-XML. Although we have established workflows and a common framework for the publication of the digital editions and a stable (and growing) group of users, I think that my own work and the work of the library as a provider or publisher of digital resources both can greatly benefit from the future development of the NFDI. 

#### Objectives 


My demands touch upon different levels: The overall quality of our products would benefit from the availability of interoperable tools or even tool chains, one (i.e. also researchers that lack access to a more thorough technical expertise) could use more easily and in connection with each other. I could imagine an improved workflow in which good quality full texts are produced using specific OCR software, with the texts then annotated on a basic level with the help of already existing tools like WebLicht. Interoperable and interconnected services available in the context of a central infrastructure for cross-institutional use would definitely be an asset for many DH staff members and researchers in general.  

Despite our existing group of regular users, we would like to improve the visibility of our resources beyond the respective scientific community. Therefore, a comprehensive list or database of extant digital editions in Germany, in which editions are clustered according to certain categories (source material, editorial model, discipline, epoch etc.), would be a nice tool not only for institutions to have a central platform for presenting their work, but also for users or editors in search for best practice examples. Some preparatory work on such an overview is currently in preparation in the context of  [CLARIAH-DE](https://clariah.de/).  

We constantly need qualified staff, and hence suitable training formats in various domains (research data management, rights management, long-term preservation, data visualization) such as written material and documentation, but also specific workshops would be beneficial. Of course, there is already a lot on offer, but it would be great, if there were a central point of access to it. 

#### Solution 


The Text+ consortium could render a great service to the community of editors by supplying a visible access point for editions, documentation, tools and learning resources. A common point of reference, permanent and not limited to a special institution or project, would be essential for all aspects of the management of digital editions, from transcription to publishing, archiving and using with automated methods. 

