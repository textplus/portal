---
title: "Discussion-Analytic Communication Workshop for Medical Personnel"

type: user-story

slug: user-story-327

aliases:
- /en/research-data/user-story-327

dfg_areas:
- 104 Linguistics
- 205 Medicine

text_plus_domains:
- Collections

authors:
- Juliane Schopf (Universität Hamburg)

---




#### Motivation


In my linguistic research of the field of doctor-patient communication, there are always practical implementations of linguistic research in medical fields as well as links to university and clinical education and training of doctors and medical staff. Among other things, two colleagues and I received a request from the Klinikum Karlsruhe whether we could hold a workshop on communication with patients for doctors during their residency. This workshop should contain authentic example data, which the participating doctors can use to deal with the seminar contents. 

#### Objectives 


Especially for such workshops and advanced training courses, but also for lectures and conferences, I often need authentic data in the form of audio and video files from various (medical) discussion contexts as well as transcriptions in order to be able to show structures and convey content. However, there is often not enough time, resources and infrastructure available to collect one’s own data, or quite delicate (medical) conversational genres are – if at all – only accessible via manifold hurdles or personal contacts. In this context, the aim is to be able to fall back on already collected data and existing corpora. 

#### Solutions 


Text+ provides low-threshold access to repositories, corpora and databases that hold processed research data. A wide range of resources and services from different institutions can be used via a central access point and can be combined with each other as needed. For example, the database for spoken German (DGD) or the research and teaching corpus Spoken German (FOLK) of the IDS Mannheim, which provides audio and video data as well as transcripts from the medical setting, such as conversations during shift handovers in the hospital or various therapy interactions.  

 [EXMARaLDA ](https://exmaralda.org/en/)can also be used as a tool for transcription or post-transcription, flexible annotation or demand-oriented research in the speech data. The output of transcripts can also be tailored to target group. For example, minimal transcripts for easier reception can be selected for medical professionals with little experience in working with transcripts, while experienced seminar participants can work with basic or fine transcripts.  

This enables me as a researcher and teacher to create individually adapted materials for (interdisciplinary) courses and to shape the important exchange between the humanities and natural sciences. In addition, in cooperation with the corresponding clinical institutions, results from qualitative research such as conversation analysis can be made useful for medical practice. In this way, conversations can first be categorized according to diseases, stages of the course of treatment or the age of the patients in order to bring together the practical experience of the participants in the training course with research questions and approaches. In the long term, digital infrastructures can thus help to identify different constellations in medical communication. 

