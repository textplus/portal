---
title: "A Comparative Analysis of Contemporary European Museums Dedicated to the Rescue of Jews During the Holocaust"

type: user-story

slug: user-story-301

aliases:
- /en/research-data/user-story-301

dfg_areas:
- 102 History

text_plus_domains:
- Collections

authors:
- Zofia Wóycicka (DHI Warschau)

---




#### Motivation


I am a historian and museum curator. Currently, I am a research fellow at the  [German Historical Institute Warsaw](https://www.dhi.waw.pl/) (part of the  [Max Weber Foundation — German Humanities Institutes Abroad](https://www.maxweberstiftung.de/en/ueber-uns.html)). In my project, I analyse and compare how a specific topic from European history is presented in the permanent exhibitions of museums in different European countries. My research relies on a broad variety of sources, both textual and visual material. As my case studies are located in different European countries I have to use multiple OPACs and other discovery tools for literature research.

Other than standard text-based data such as media reports, scholarly literature, and written archival documents, I am also using text- and language-based data that is not available in digital format or machine-readable format: expert interviews I conduct with museum directors, curators, and other museum stakeholders in the field; exhibition texts; hand-written entries in the museum’s visitor’s books in various languages and in Latin and non-Latin scripts. To obtain such data, I visit the museums and produce photographic documentation of the exhibition display and the visitor’s books.

Concerning the entries in visitor’s books, apart from the photographic documentation, I also have a partial transcription of the entries: anonymized excerpts and translations into English (as Word documents). Exhibition texts and material from the museum archives I mostly also have as photographs, some as Word documents or PDFs. Concerning the expert interviews, I have both the recordings of the interviews and summaries with quotations as Word documents (most of them, I send for authorization).

#### Objectives


Some of my text-based data, I had to collect myself in the given museums by means of photographic documentation. I would like to be able to easily organise, store, and process these photographs and link them with the transcripts of the texts they show. In the case of museum displays and visitor’s book entries, the specific design, presentation and arrangement can also be significant, which means I cannot solely rely on transferred text and need to be able to work with visual and written material simultaneously. But there is also the legal question of whether I could make them available for other researchers.

Exhibition texts are not documented in publications and may change or be replaced over time, so it would be desirable to store this data and make it accessible for other researchers after I finished my study. Additional written materials are the archival documents from the museum archives. But here is also a legal issue: in how far can I make them accessible to other researchers, even after I have finished my project? Thus, tools for creating, managing and storing and publishing multi-modal collections including different data types and formats in accordance with the law are necessary.

When storing, processing, and publishing excerpts from the interviews, I have to observe privacy laws. I would like to use tools and storage solutions that function in accordance with legal and ethical guidelines for privacy protection.

