---
title: "Authority files in digital scholarly editions of correspondence"

type: user-story

slug: user-story-406

aliases:
- /en/research-data/user-story-406/

dfg_areas:
- 105 Literaturwissenschaft
- 102 Geschichtswissenschaften

text_plus_domains:
- Editionen

authors:
- Markus Bernauer
- Selma Jahnke
- Frederike Neuber
- Michael Rölcke (Berlin-Brandenburgische Akademie der Wissenschaften)

---




#### Motivation


Scholarly editions of correspondence have found their ideal ‘habitat’ in the digital medium. The structured recording of correspondence metadata allows linking letters and exploring networks of senders and recipients both within single scholarly editions and across various editions of letters (as the web service *correspSearch* does). An indispensable requirement for linking and analysing edited letters is the application of standardized metadata, especially for identifying people such as the sender of a letter, its recipient, and the people mentioned within its text.  

The current standard to identify persons in digital editions is the Integrated Authority Files (GND) offered by the German National library (Deutsche Nationalbibliothek, DNB) and the Virtual International Authority File (VIAF) provided by various international libraries. Almost all digital edition projects working with documents that involve “German personnel” are using the GND identifiers; this includes, for instance, projects from Literary Studies (DFG-Fachsystematik 105) and History (DFG-Fachsystematik 102).  

The editorial community can consider itself very fortunate that an institution as reliable as the DNB is taking care of the creation and maintenance of authority data. In order not only to take, but also *to give*, it would therefore be desirable that the vast knowledge about persons that is created in the edition projects can also be incorporated more easily into the GND.  

#### Objectives 


We are working as researchers at the digital edition project “Jean Paul – Sämtliche Briefe digital” which is located at the Berlin-Brandenburg Academy of Sciences and Humanities. The letters of Jean Paul himself have been available online since 2017. Meanwhile, we are preparing a digital edition of the “context letters”, meaning the letters sent between family members, friends and fellow writers of Jean Paul. As far as the use of standardized metadata is concerned, we are facing the challenge of not only dealing with main characters of history in both corpora of correspondence. Many people – whether they are correspondents or mentioned in the text – are family members or friends without GND or VIAF identifiers. Most of them are less of a well-known or even famous person, but rather small or medium civil servants, women or children. Thus their existence is rarely acknowledged by the GND or the VIAF, but within the framework of our editorial work we managed to find proof of existence.  

One of the main tasks we perform day by day is the identification, research and commentary of historical figures. The efforts of consulting parish registers, historical directories, birth and wedding announcements as well as obituaries in the coeval newspapers (by visiting physical and digital archives and libraries) require a lot of time, patience and experience. Especially for edition projects, extensive, editorially supported standards databases, such as the GND are the basis for content indexing within the letters. However, it is a pity that knowledge of people widely forgotten or even ignored by historical tradition we accumulate in the editing process will remain “locked up” in our edition. As it is the case for information about the same kind of figures gained in other editions that isn’t available to us via the “central reference point for authority files” GND. 

#### Solution 


We would suggest a curated, crowd-sourced admission process for new entries in the GND. The input of data could be facilitated by a form that allows users to propose a new person they want to be entered. In this scenario an indication of reliable bibliographical sources to prove the information would be necessary. 

In general, we hope that Text+ will strengthen the collaboration between edition projects and the German National Library. Perhaps a first step could be to organize an exchange between edition projects with large indexes of persons and those responsible for GND identifiers at the National Library, so that ideas, needs and concerns about a closer collaboration can be discussed.  

#### Challenges 


When doing research on correspondence networks, knowledge of lesser known or today forgotten individuals is an essential issue, as they are often an important factor in the dynamics of a network, whether they are protagonists in or objects of a conversation. The practice of documenting primarily the lives of “major players” by following a specific model of authorship and oeuvre, established in the late 18th century, has been problematized for the last decades. The digital possibilities of collecting and linking data are enabling us to uncover protagonists and activities behind the known (mainly male, Christian and upper class) surface and gain a new understanding of the dynamics. A special challenge in researching data of female, Jewish or foreign players are the changes in or varieties of names during their lifetime; a challenge concerning lower class actors is the missing genealogical information. These circumstances make the standardized acquisition expedient and desirable.    

#### Review by community 


Yes. 

