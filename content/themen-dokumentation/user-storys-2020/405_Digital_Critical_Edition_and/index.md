---
title: "Digital Critical Edition and systematic Collections of interconnected research data ressources on philosophical authors"

type: user-story

slug: user-story-405

aliases:
- /en/research-data/user-story-405

dfg_areas:
- 108 Philosophie

text_plus_domains:
- Editionen

authors:
- Christoph Bartmann
- Mark Eschweiler (Universität Köln)

---




#### Motivation


The Averroes Edition is a trilingual (Latin, Arabic, Hebrew) edition project (averroes.uni-koeln.de) and aims at producing critical editions of Averroes commentaries on Aristotle’s Natural Philosophy. The digital editions as well as all transcriptions of historical sources are going to be published on the further extended “Digital Averroes Research Environment“ (DARE, dare.uni-koeln.de). DARE makes accessible a big textual corpus of the writings of the Andalusian Philosopher Ibn Rushd or Averroes (1126-1198) by publishing digital copies of already published editions as well as descriptions of important manuscripts and transcription of the manifold sources for the Averroes edition. 

Both projects are continually working on enhancing and enriching the working environments for the project editors as well as the contents and user experience on the presentation platform. But some general challenges remain: Even if digital tools support the creation of the materials, most of the work has still to be done manually. There is a big need for tools and workflows allowing for the automatic (pre-)processing (especially automatic OCR) of non Latin scripts (NLS). Another problem concerns the fact that there is a big dependency between the presentation platform and the created materials. Therefore, today researchers are only able to access the materials as long as the DARE presentation platform can be mantained. Thus, similar to other platforms providing rich access to textual material, one big challenge for the Averroes and the DARE project is the preservation of the valuable materials and to guarantee their long-term availability.

A massive issue concerning almost all digital platforms presenting editions today is the lack of interoperability of the published materials with materials on other platforms. Therefore, an important goal of the DARE platform is to provide access to manuscripts and historical sources in a systematic way on the basis of Averroes’ works. The DARE platform tries to enhance the accessibility of these information by publishing them in standard formate, e.g. by using the Image Interoperability Framework IIIF. But in order to connect the DARE „knowledge graph“ with other information on similar platforms, more coordinated efforts are necessary with regard to the data in different repositories in order to create „knowledge networks“ based on the (Gemeinsame Normdatei) GND of the Germany National Library.

#### Objectives


Text+ should aim at tackle the „big“ challenges which cannot be solved by a single edition project, but requires collaborative efforts by supporting projects and initiatives and bringing together different projects: 

1. Text+ should develop projects and initiatives, which address the special challenges of working with non Latin scripts (NLS), especially by addressing the challenges associated with the automatic processing, editing and searching of those texts.

2. Text+ should especially concentrate on developing common infrastructures for the long-term availability of digital editions and associated materials to enable and guarantee access to those materials even when the original publishing platforms can no longer be maintained. This is not only a technical challenge but requires coherent strategies for encoding schemes which also allow for data aggregation and especially for a systematic search over materials from different repositories. The Text Encoding Initiative (TEI) as the de facto standard for digital editions and their source materials does not provide a sufficient basis to achieve the necessary interoperability of different projects and the reusability of research materials.

3. Text+ should develop strategic concepts for enabling a better collaboration and interoperability between research projects by developing infrastructures for knowledge networks. This includes a closer coordination between research projects and libraries, especially by common efforts to reuse common usage and improvement of vocabularies and entities collected in the GND. In this way the specific strengths of large institutions providing infrastructure and of smaller research projects providing critical knowledge can be identified and supported.

