---
title: "Infrastructural Needs for Romance Research Data"

type: user-story

slug: user-story-316

aliases:
- /en/research-data/user-story-316

dfg_areas:
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft

text_plus_domains:
- Collections
- Domänenübergreifend

authors:
- Johannes von Vacano (ULB Bonn)
- Doris Grüter (ULB Bonn)
- für FID Romanistik

---




#### Motivation


The status quo of research data management in Romance Studies was summarized in a work­ing paper (cf. Erben et al, 2018) published by the *Specialized Information Service for Romance Studies* (FID) – in cooperation with the *Working Group for Digital Romance Studies* (*AG Digitale Romanistik*), affiliated with the *German Association for Romance Studies* (Deutscher Romanisten­verband), and the association *romanistik.de*. The paper reflects the user’s perspective by drawing upon an array of sources:  
* a *Working Group* *for Digital Romance Studies* survey (cf. AG Digitale Romanistik, 2014),  
* various workshops organized by the FID and the *Working Group* *for Digital Romance Studies* for determining research data management requirements in Romance Studies and for enabling the development of necessary services (cf.  [reports on the FID-Website](https://fid-romanistik.de/forschungsdaten/workshops/)),  
* frequent discussion at FID advisory board meetings and  
* insights gained at relevant expert conferences. 

The paper highlights the unique characteristics of Romance research data and the resulting specific requirements for the development and further expansion of necessary infrastructures for sustainable research data storage, reusability and findability. 

Romance research data have the same properties as Humanities research data in general (long-term relevance, historicity, contextuality, etc.), but are specifically characterized by their diversity (in terms of the variety of disciplines, languages and regions covered and the data domains and formats utilized) and by their relevance for a multilingual, international community of researchers. 

This gives rise to special infrastructural requirements, such as: 
* options for multilingual documentation and search queries 
* implementation of international, open standards (to ensure data reusability across re­gions and disciplines) 
* free access (ideally), including for international researchers. 

For the field of Romance Studies, the current problems connected with the existing interdisci­plinary infrastructure are: 
* the poor findability of Romance research data due to distributed storage on disparate and insufficiently interlinked repositories and the lack of subject-specific and language-specific search options within the respective repositories, frequently in combination with a lack of the necessary metadata 
* usage barriers resulting from insufficient user support that is generally not tailored to the specific requirements of the community – concerning, for example, the multi­lingualism of Romance research data and linguistic diversity within the larger com­munity. 

#### Objectives

* **Research infrastructures** (virtual research environments, relevant tools, working en­vironments) need to be pooled, further developed and linked together within an inte­grated network. The functionalities that must be offered according to Romance Studies researchers include for example multilingual data description, multilingual user inter­fa­ces (for data upload, editing, presentation, search and retrieval), flexible integration of discipline-specific tools and support for all standards commonly used in the Romanic countries 
* Integration of the various **search systems** into a common network, bundling of re­search data references, virtual merging of widely distributed resources to enable inter­disciplinary and discipline-specific searching. This requires a filtering option for different languages. A multilingual search interface is desirable as well (multilingual subject terms). 
* Furnishing of a central infrastructure (or networking of peripheral infrastructures via a central contact point) for the **storage**and the**archiving** of research data, with suitable user workflows. 

#### References 


AG Digitale Romanistik: „Ergebnisse der Umfrage der AG Digitale Romanistik zur Langzeitarchivierung von digitalen Forschungsdaten für die Romanistik“, in: *Mitteilungsheft des Deutschen Romanistenverbands e.V.*, Frühjahr 2015, S. 36-40, hier: S. 37,  [http://deutscher-romanistenverband.de/wp-content/uploads/sites/14/Auswertung_Forschungsdaten-Umfrage.pdf](http://deutscher-romanistenverband.de/wp-content/uploads/sites/14/Auswertung_Forschungsdaten-Umfrage.pdf) (23.08.2018).

Erben, Maria / Grüter, Doris / Rohden, Jan: *Forschungsdatenmanagement in der Romanistik. Aktuelle Situation und zukünftige Perspektiven*, Bonn 2018,  [hdl.handle.net/20.500.11811/1178](https://hdl.handle.net/20.500.11811/1178).

Fachinformationsdienst Romanistik, Workshops zu Forschungsdaten,  [https://fid-romanistik.de/forschungsdaten/workshops/](https://fid-romanistik.de/forschungsdaten/workshops/).



