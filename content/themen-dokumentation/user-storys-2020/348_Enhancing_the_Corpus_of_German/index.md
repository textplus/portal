---
title: "Enhancing the Corpus of German Literature from ca. 1750–1920: Re-publishing canonical and forgotten (Women) Writers online and in Print"

type: user-story

slug: user-story-348

aliases:
- /en/research-data/user-story-348

dfg_areas:
- 105 Literaturwissenschaft

text_plus_domains:
- Collections

authors:
- Andreas Hungeling

---




#### Motivation


As former scientist (geophysics, polar explorer) with more than 25 years of work experience in the German petroleum industry, I came across authors of the 19th century online more or less by chance at the end of 2018. Today, these authors are widely unknown in Germany, which is to some extent due to the fact that their oeuvres are printed in Frakturschrift/black letter typeface. To most Germans, reading this writing appears to be difficult. Therefore, and to make the lesser known, and/or often often out-of-print works available again, I have chosen to re-type these texts in Antiqua writing, publish these texts in print/eBook format and thus, provide for a higher readability and dissemination. 

When searching for works which were already transcribed, I got to know the German Text Archive (Deutsches Textarchiv; DTA) at the Berlin-Brandenburgischen Akademie der Wissenschaften (BBAW) and I highly appreciate the excellent cooperation which developed since then. I have learned about the specifics of text transcription and re-usable annotation formats like the DTA-Base Format. Today I access the entire corpus of DTA, and frequently contribute to its unique process of quality assurance within the DTAQ platform. I re-use texts from the DTA and while preparing them for re-publication in printed form, I report the print- and transcription errors as tickets in DTAQ. At the same time, I myself transcribe works of my own antiquarian collection or search digital works (scans) in online libraries, which are not available at DTA. These transcribed works, I then provide to DTA. 

#### Objectives


I focus my activity on German literature of the time from 1750 to 1920. From personal discussions I learned that many people are tired of reading today’s books with more than 400 pages. For this reason, I mostly transcribe novellas with up to 120 pages. After consulting with DTA, I decided to further prioritize the works of female authors of the 19^th^ century, since these authors are underrepresented in today’s culture, as much as they are missing from the historical canon. 

I publish the transcribed and newly formatted texts under the label “Wiedergefundene Perlen der Literatur” (‘rediscovered pearls of literature’) through my  [publishing house “stimm-los”](https://www.stimm-los.de/) (voice-less), which I recently founded for exactly that purpose, because I would like to give these “forgotten” authors a voice: They have a lot to say, they are not voice-less. 

Back then, many authors used to publish their works under a pseudonym, and especially women often used their birth names and/or their names by marriage. Although information on many authors can be found in excellent sources like Sophie Pataky’s ( [Pataky 1898](https://www.deutschestextarchiv.de/pataky_lexikon01_1898), 2 vol.s) or Franz Brümmer’s lexika ( [Brümmer 1913](https://www.deutschestextarchiv.de/bruemmer_lexikon01_1913), 8 vol.s), not all authors are listed there, and to research cross-references and curriculum vitae often still is an elaborate effort. I aim to save the researched data in structured form (EXCEL, ACCESS), in order to facilitate research within my own database, and plan to share this knowledge resource (e.g. as editable CSV file) with other interested users in the near future. 

Many novellas and novels were not printed in book format but are to be found as serialized stories in newspapers and magazines. Example: The novella “Ein Opfer” by F. Ahrenfeld (pseudonym for Jenny Hirsch) was published in the Marburger Zeitung 1904/1905 in 41 sequences on 75 pages. It is my aim to research these kinds of works, which are not available in their entirety, to transcribe them and to put them together as one work. 

#### Solution


What would I ask for from the “NFDI”? 
1. A database which allows for researching authors according to their names, pseudonyms, birth names, etc., including relevant information on the vitae of these personalities, their works and where they were published.
2. A database showing the table of contents of newspapers and magazines of the 18th and 19th centuries. The targeted search for unknown authors and works in former newspapers and magazines is very time-consuming and success happens, if at all, rather by chance. However, especially these old newspapers and magazines often contain works which were not printed anywhere else.
3. An easy way to further extend the DTA collection by works which were published as serials throughout many newspaper issues. Today, these works cannot be reproduced in their entirety by DTA. Each newspaper issue is currently considered a complete work.

#### Challenges


The challenge regarding creating a database mentioned under 3. lies not in the technical implementation, but rather in researching, structuring and processing the necessary data. This research is time-consuming, only feasible in cooperation with domain experts (who might not happen to be ‘tech-savvy’ at the same time), would have to include several sources and on- and offline sites of knowledge, and might even require an international investigation. It is to be expected that much of the desired information does not exist in digital form, which will likely complicate research immensely.  

#### Review by Community


Yes. Needless to say, I am willing to contribute to reviewing the services of Text+ during the funding period. 

