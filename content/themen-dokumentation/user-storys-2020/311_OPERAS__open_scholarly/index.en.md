---
title: "OPERAS – open scholarly communication in the european research area for social sciences and humanities"

type: user-story

slug: user-story-311

aliases:
- /en/research-data/user-story-311

dfg_areas:
- Interdisciplinary

text_plus_domains:
- Collections

authors:
- Michael Kaiser
- Elisabeth Ernst
- Judith Schulte (Max Weber Stiftung)

---




#### Motivation


I am working as communication and project officer for the European distributed Research Infrastructure  [OPERAS](https://operas-eu.org/) at the  [Max Weber Stiftung](https://www.maxweberstiftung.de/startseite.html) in Germany. OPERAS coordinates and federates resources in Europe to efficiently address the scholarly communication needs of European researchers in the field of social sciences and humanities. The Max Weber Stiftung is involved in the coordination and the Executive Assembly of the Research Infrastructure and serves as national node for Germany. 

My work relies strongly on coordination with the infrastructure’s international members and on closely working together with many colleagues from different countries and backgrounds. This is especially true when it comes to the planning of joint workshops, creating common statements and publications, and using the extensive network to plan and execute surveys or other information seeking tasks together. While we are using a common chat and video call system as well as a file storage and text processing system, we do not yet have more specific common tools for collaborative work. 

**Objectives**

One of the main goals of the OPERAS project  [TRIPLE](https://www.gotriple.eu/) is to provide a single access point for users to discover and reuse open scholarly social sciences and humanities resources, e.g. research data and publications, currently scattered across local repositories. It also seeks to enable researchers to be able to find and connect with peers and projects across disciplinary and language boundaries. 

As part of this process and also in the context of the OPERAS project  [OPERAS-P](https://operas.hypotheses.org/operas-p), surveys were conducted in the beginning of 2020. Project partners, including MWS, were asked to conduct these anonymised surveys with researchers in their countries on how the TRIPLE platform should go further in its development to tackle specific challenges social sciences and humanities researchers might have. Those surveys were recorded, transcribed, anonymised, translated, and then collected by one project partner to extract the relevant information for TRIPLE, OPERAS-P and OPERAS. While this process worked well, every partner still had to think about storing the sensitive data conducted, which tools to use, and whom to contact for which part of the process. 

#### Solution


Text+ could help facilitate the work of OPERAS in two ways:
1. as a national data aggregator

For OPERAS, Text+ could serve as a German network to rely on for identifying projects and relevant research for any activities of the OPERAS projects. More specifically, for the TRIPLE platform, which will provide a single European access point for users to discover and reuse open scholarly social sciences and humanities resources, Text+ (as a national data aggregator that enables the TRIPLE platform to automatically read and integrate these data) would serve as a great facilitation. In that context, it would help to not just know where the data is available, but also who the responsible organisation, project, or researcher is, so that they can be contacted. 

2. as a service that stores sensitive data

For similar projects that OPERAS or other international consortia will carry out, Text+ could provide a single room with clear log-in and access rights, where a limited number of organisations from different European countries can work together. Most urgently needed would be a reliable central and long-term storage of the conducted data (e.g. from interviews) that is coherent with European data privacy. This would not only facilitate the work of all project partners as they would not need to think about how to store this data, it would also be centrally available to everyone with access rights throughout the project and after if the project’s grant agreements require it. In addition, the project lead could upload project-internal data on methods and methodologies, e.g. guidelines for conducting interviews or other projects, which recording, transcription, and translation tool to use and, if possible, also access rights to use these or integration with the SSHOC Open Cloud or the EOSC, as well as contact details for all people involved.

