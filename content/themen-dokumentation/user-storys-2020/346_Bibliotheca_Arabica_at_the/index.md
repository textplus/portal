---
title: "Bibliotheca Arabica at the Sächsische Akademie der Wissenschaften"

type: user-story

slug: user-story-346

aliases:
- /en/research-data/user-story-346

dfg_areas:
- 104 Sprachwissenschaften

text_plus_domains:
- Collections

authors:
- Boris Liebrenz (Saxon Academy of Sciences and Humanities)

---




#### Motivation


The academy project Bibliotheca Arabica, based at the Saxon Academy of Sciences in Leipzig, works towards a comprehensive bio-bibliographical collection of Arab literary history up to the 19th century, a period in which this literature was almost exclusively passed on via handwritten texts. The project goes beyond the usual approach of investigating which authors wrote which works (production), but also explicitly examines the concrete handwritten transmission (tradition) as well as the traces left by copyists, patrons, readers, owners or donors in the preserved manuscripts (reception). 

####    
Objectives 


a) Provision and linking of inventory data 

The goal of the project is not primarily the recataloguing of manuscripts, but rather the consolidation of the catalogs of already existing inventory data. These will be linked to the data from biographical encyclopedias, chronicles and bibliographical overviews, which are particularly extensive in Arabic literature. 

b) Acquisition of new data 

Unfortunately, the numerous provenance and usage data contained in the manuscripts haven’t been recorded neither regularly nor thoroughly. A systematic recording of this hitherto neglected documentary source genre, however, makes it possible to trace the concrete paths taken by individual books through different hands and institutions as well as the distribution of copies of individual works or entire genres in different periods or different regions. 

c) Standardization  

The clear identification of different entities across different projects through standardization forms the core of a meaningful referential work. In the long term, the Bibliotheca Arabica platform can thus become a working instrument for the entire Arabist professional community; the diversity of the contained data will provide a reliable data basis for researchers interested in literary history, history, social history and codicology. On the other hand, Bibliotheca Arabica will be able to benefit considerably from the expertise of various other disciplines and selective data collection, even for smaller projects. Our data model offers concrete links to several ongoing and planned projects at national and international level. 

Several projects in our subject area, funded by different institutions, are dedicated to a wide range of documentary and literary Arabic sources. Quite different knowledge interests notwithstanding, they combine the collection of similar data on literary works, manuscripts, persons, places and institutions. To bundle these different perspectives and data in one data model and to enable a constructive exchange is not only a tremendous opportunity but also a challenge. 

A prospective workshop in Leipzig in 2019 brought together several projects and, while there had been widespread interest in cooperation on content and the exchange of data, it also showed that the necessary resources for the implementation of concrete data technology cooperation in project equipment necessary in the general interest of the specialist community are usually not available. 

In the end, this will create ever new and volatile isolated solutions and will often end with the loss of research results. Due to the long project duration of Bibliotheca Arabica, however, there is a desire on several sides to establish a durable and integrative central hub. The integration and presentation of prosopographic, bibliographic, topographic and codicological data from various partners allows for an improved range of the individual projects and an optimized data basis for all partners. 

#### Solution 


It would be helpful to support the complicated data technical coordination of different projects with one office that also understands the special requirements of the Arabic script. 

Similarly, a technical interface for the standardization efforts of various partners would be of great benefit. 

