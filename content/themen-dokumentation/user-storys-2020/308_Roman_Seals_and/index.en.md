---
title: "Roman Seals and Inscriptions"

type: user-story

slug: user-story-308

aliases:
- /en/research-data/user-story-308

dfg_areas:
- 101 Ancient Cultures
- 102 History

text_plus_domains:
- Collections

authors:
- Marietta Horster (Berlin-Brandenburgische Akademie der Wissenschaften)

---




#### Motivation


I am an ancient historian. I am working on several types of objects: 
1. Seals in the Roman Empire (forms, material, inscriptions, pictorial ornaments, regional differences, gender-related differences, intended use, alternate function e.g. as decoration, jewellery, religious statement). 
2. Lost inscriptions in objects of a certain region, where I work on together with a group of archaeologists. 

#### Objectives 


I need data from these two types of objects. 
1. Seals are categorised as “instrumentum domesticum”, only rarely they are integrated into regional collections of inscriptions. Difficult enough, in such cases, only seals (often metal rings) with inscriptions are incorporated, the other ones without inscriptions are not even mentioned. More often they are left outside regional corpora together with amphora stamps or inscribed bricks or pipes etc.  
2. Besides, I need data on now lost inscriptions (either objects have disappeared or the object is still there but its text is no longer visible) of this region, as to get a complete corpus of all known inscriptions. 

#### Solution 


It would be great if by more collaboration in the different fields of “province” (objects, texts – archaeologists, philologists, historians, museums and collections) data exchange and data export would be possible. Many researchers, curators and collectors would benefit from such data organisation and coherence. Working in such an important but until now neglected field of legal, social and economic history (and as it has turned out – concerning superstition practices as well) is in urgent need of being supported to find the data.  

Concerning the inscriptions, it would be necessary to find the data: 
1. By selecting the region in question; 
2. Getting up to date information on the physical existence and in case it is no longer existent, the information where I find information or even better, receive the data. 

#### Challenges 


Masses of seals are known from the Byzantine period and from ancient Mesopotamian and Egyptian cultures. Corpora (analogous) presenting the data organised either by collections (most of the Byzantine corpora) and regions (most of the ones related to Egypt, Near/Middle East) exist for these periods.   

Competitive data are to be found in analogous forms (Corpora, but not in their indices – you have to search the pages). Few online databases (in Great Britain) on ancient inscriptions would provide such data, because of their unequivocal vocabulary (“lost”). 

#### Review by community 


Yes, I would like to. 

