---
title: "Semantic Uncertainty in DIN norms"

type: user-story

slug: user-story-605

aliases:
- /en/research-data/user-story-605

dfg_areas:
- 104 Linguistics
- 402 Mechanics and Constructive Mechanical Engineering

text_plus_domains:
- Comprehensive

authors:
- Jörn Stegmeier
- Peter F. Pelz
- Sabine Bartsch
- Andrea Rapp (TU Darmstadt)

---




#### Motivation


Our project, a sub-project of SFB 805, TU Darmstadt, titled “Semantic uncertainty in DIN norms”, is a pilot study carried out in cooperation between research area 104 (Linguistics) and 402 (Mechanics and Constructive Mechanical Engineering). 

Product development and product manufacturing need to be in compliance with often numerous norms and standards. Compliance is easily possible as long as these norms and standards are either expressed in numbers (see example [1]) or are expressed verbally in an unambiguous way (see examples [2], [3]). 

Example [1] (DIN 1988-100 2011: 7) 

Trinkwasserleitungen sind von Grundstückentwässerungsleitungen in einem Sicherungsabstand von mindestens 0,2 m zu verlegen. 

Example [2] (DIN 1988-200 2012: 23) 

Nach der TrinkwV [1] sind Leitungen […] farblich unterschiedlich […] zu kennzeichnen. 

|Benennung |Kurzzeichen |Farbe des Kurzzeichens |
|---|---|---|
|Trinkwasserleitung, kalt |PWC |Grün |
||…||


Example [3] (DIN 1988-100 2011: 10) 

ACHTUNG — Fluorchlorkohlenwasserstoffe sind als Wärmeträger gesetzlich nicht mehr zugelassen! 

In these examples, the norm text leaves no room for interpretation, compliance with the norm is a matter of knowledge and information flow only. If, however, norms can only be expressed in an ambiguous way, interpretation and thus a decision making process on how to act in compliance with the norm is necessary. Consider the following examples: 

Example [4] (DIN 1988-200 2012: 24) 

Die Leitungen sollten so geführt werden, dass Luftpolster vermieden werden. 

Example [5] (DIN 1988-200 2012: 13) 

Der Spitzendurchfluss kann dabei kurzfristig über dem Nenndurchfluss liegen. 

Example [6] (DIN 1988-200 2012: 38) 

Dies gilt insbesondere für Apparate, die einer regelmäßigen Inspektion und Wartung bedürfen. 

While all of these examples do convey a certain intentionality, they still necessitate interpretation. In all of these cases, norm compliant production or development depends on further information on the semantically uncertain word or phrase. In example [4], “sollten” makes the whole sentence more of a suggestion than anything else which might be construed as carte blanche to ignore the content altogether. In example [5], “kurzfristig” provides no reference from which to calculate whether seconds or minutes are to be considered short term. Example [6] does not specify where the need for maintenance can be looked up. 

We do not think that ambiguity in norm texts is there out of malicious intent. There are various reasons why ambiguous statements might be used in norm texts. Ambiguous statements can simply accurately reflect the knowledge which is available at a given time. They might be used deliberately to allow for innovation or for unforeseeable circumstances. Finally, ambiguous statements might enter into norms by accident. However, even though there might be good reasons for using ambiguous language, those reasons are not accessible to the reader. 

#### Objectives 


Our aims are twofold: First, we will develop a taxonomy of (forms of) semantic uncertainty in DIN norms which will be published as a lexical resource. Second, utilizing the taxonomy, we will develop a basic software tool which will provide engineers with an overview of all parts of a norm text which contain semantically uncertain linguistic entities. The taxonomy – and, hence, the tool – will not only contain (patterns of) linguistic units which are semantically uncertain but also possible solutions (if applicable). These solutions will reflect the experience of engineers and comprise, for example, references to pertinent information materials or action items. 

#### Solution 


For a project like ours, various tools and resources are needed. Our core need, of course, is a digitized corpus of all DIN norm texts – which is impossibly to obtain or create due to copyright restrictions. Cascading down from this is the need for easy ways to (manually) annotate the corpus and in turn interact with the annotations to provide engineers with information on the semantically uncertain parts of the corpus. Cascading even further down, we would need reliable and sustainable ways to interface with other pertinent resources in order to enhance our tool. 

I would like Text+/NFDI to take a load of this off by way of giving legal and technological/methodological advice and support. Ideally, Text+/NFDI’s help would exceed pure advice, however. Hands on support by, for example, brokering licensing agreements with institutions or providing networking opportunities to get in contact with experts in information science and engineering would be greatly appreciated. 

#### References 


DIN 1988-100 (2011):DIN 1988-100:2011-08, Technische Regeln für Trinkwasser-Installationen – Teil 100: Schutz des Trinkwassers, Erhaltung der Trinkwassergüte; Technische Regel des DVGW. Beuth Verlag GmbH. 

DIN 1988-200 (2012):DIN 1988-200:2012-05, Technische Regeln für Trinkwasser-Installationen – Teil 200: Installation Typ A (geschlossenes System) – Planung, Bauteile, Apparate, Werkstoffe; Technische Regel des DVGW. Beuth Verlag GmbH. 

