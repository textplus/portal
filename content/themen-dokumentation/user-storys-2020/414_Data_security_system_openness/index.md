---
title: "Data security, system openness, networking – wishes for the NFDI from view of the MEGA"

type: user-story

slug: user-story-414

aliases:
- /en/research-data/user-story-414

dfg_areas:
- 102 Geschichtswissenschaften
- 105 Literaturwissenschaft
- 108 Philosophie
- 111 Sozialwissenschaften
- 112 Rechtswissenschaften

text_plus_domains:
- Editionen

authors:
- Gerald Hubmann
- Regina Roth (Berlin-Brandenburgische Akademie der Wissenschaften)

---




#### Motivation


From the perspective of a comprehensive historical-critical edition such as the Marx-Engels-Gesamtausgabe (MEGA – 114 volumes are planned, 67 of them have already been published, 8 of these printed volumes are also available in digital form, and 3 others exclusively in digital form) – digital editing techniques and publishing methods offer more efficient ways of working, as well as global access possibilities, a refined information retrieval and the opportunity for cross-linking the edited material with other edition projects, archives or data bases of contextual value. 

Objectives The MEGA-project would like to ensure the following (listed according to priority) – and we hope Text+ and the NFDI will help to achieve and support these goals: 
1. The edition should be kept permanently available in the context of a secure and sustainable research data repository, preferably along with other digital editions. 
2. The data structure used in this process should allow for future supplements and revisions, further investigations into the material and new forms of its presentation and arrangement, e. g. cross-linking, indexing, new register options. 
3. Essentially, we would like to see digital editions to be more interconnected, guaranteed by consistent and documented standards for the linkage and aggregation of data. In our view correspSearch in particular proved to be a practical and viable approach in this direction.  

#### Solutions 


We would welcome the development of standards that incorporate the goals mentioned above. At the same time the different characteristics of different types of material (letters vs. excerpts vs. works) should be taken into account and source-specific standards should be provided. 

#### Challenges 


The compatibility of the data infrastructure with the diverse phenomena in the presented texts – e. g. marginalia, graphics, detailed tables, figures, formulae, Greek and Cyrillic passages etc. – is of special concern for the MEGA. In addition, access and navigation modes should be fitted to the type of material (e. g. browsing a collection of letters requires different navigation options than the study of journal clippings or handwritten excerpts). 

Another challenge in linking edition data is the usage of authority data or lack thereof, which is why we hope Text+ will facilitate the involvement of edition projects in generating Integrated Authority Files (GND). 

#### Review by community 


The MEGA will gladly test and comment on any proposed solutions. 

