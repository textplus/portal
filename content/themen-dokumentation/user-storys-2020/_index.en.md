---
title: User Stories
type: user-story
hide_in_list: true
menu:
  main:
    weight: 80
    parent: themen-dokumentation
---

# User Stories 2020

{{<lead-text>}}
To integrate user needs, in 2020 we invited researchers to contribute User Stories from their research environments. The goal was to capture a broad spectrum of disciplines, data domains, and research questions interested in Text+.
{{</lead-text>}}

After the approval of Text+'s revised funding proposal in the second NFDI round, we requested User Stories from the community. In a short time, we received more than 120 User Stories, of which we can publish 115 with the authors' consent. The contributions are organized along Text+'s three data domains (Collections, Lexical Resources, Editions), expanded with a fourth comprehensive, thematically overarching categorization. Additionally, they are listed according to the DFG subject classification (academic fields 101–113).

Many thanks to the active community for the valuable contributions and willingness to support Text+! Your feedback is essential to shape Text+'s offerings together. In general, we see these contributions as an important element of participation in Text+. The User Stories were submitted based on [this template](./files/Text_User-Stories_Template_dt.pdf). The [full report](https://doi.org/10.5281/zenodo.5384085) was published in September 2021. The [data for analysis](https://dx.doi.org/10.20375/0000-000E-67ED-4) were published in the DARIAH-DE Repository.

The contributions primarily focus on language and literary studies (104–105), with only a few exclusively related to history (102) or arts, music, theater, and media studies (103). Classical philology (101), social and cultural anthropology, non-European cultures, Judaic studies, and religious studies (106), as well as theology (107) and philosophy (108), have surprisingly strong engagement. Some contributions come from social science disciplines (111). Besides, several User Stories have a distinctly interdisciplinary focus. Many User Stories provide detailed references to infrastructure questions or possible services from Text+ and, based on individual research questions, concretely indicate the needs and proposed solutions they bring.

It is apparent that depending on the context, whether individual projects, larger research groups, or working groups, assessments of what Text+ and NFDI can and should achieve may diverge. The availability and usability of restricted-access research data are recurring themes in many User Stories. Another important concern aims at the possibility of making valuable but perhaps less prominent data from small languages or individual research projects reusable through Text+. The seemingly straightforward linkage of distributed resources is also an issue that is still present and not resolved for the community.

The User Stories were read by a team from the Text+ consortium. In total, we analyzed and tagged 118 User Stories submitted by the community. Most stories are tagged with several of the total 67 different terms, some with only one, others with up to 13. On average, six keywords were assigned per User Story. In total, 773 terms were distributed across all User Stories.

The keywords summarize several aspects that are important in the context of NFDI and for the requirements of Text+. Examples for the selection of keywords include: whether data is self-produced or needed (data producer, interest in further data), the data format available, the multilingualism of the data, the FAIR principles, possible needs for interaction between multiple resources, and the type of resources (corpus-corpus, lexical resource-corpus linking).

{{<image img="gfx-user-stories/percentage_keywords_all-1-2048x958.png" alt="Percentage of User Stories for the 10 most-used keywords"/>}}

{{<image img="gfx-user-stories/percentage_keywords_collections-1-2048x942.png" alt="Percentage of User Stories in Collections for the 10 most-used keywords"/>}}

{{<image img="gfx-user-stories/percentage_keywords_Lexical_Resources-1-2048x863.png" alt="Percentage of User Stories in Lexical Resources for the 10 most-used keywords"/>}}

{{<image img="gfx-user-stories/percentage_keywords_editions-1-2048x1013.png" alt="Percentage of User Stories in Editions for the 10 most-used keywords"/>}}

{{<image img="gfx-user-stories/percentage_keywords_comprehensive-1-2048x973.png" alt="Percentage of cross-domain User Stories for the 10 most-used keywords"/>}}

## User Stories by DFG Subject Classification

The order is based on the [DFG subject classification](https://www.dfg.de/resource/blob/172316/5863ef132d178054609f74940f6a27c9/fachsystematik-2016-2019-de-grafik-data.pdf) of the years 2016–2019 at the level of 101–113, and the sub-disciplines are evident from the texts themselves. Each User Story was assigned a number, and multiple mentions were possible.

{{<image img="gfx-user-stories/Userstories_number_of_dfgsubject_area-1536x768.png" alt="Number of User Stories by DFG Subject Classification"/>}}

{{< user-stories/accordion-dfg-area >}}

## User Stories by Text+ Data Domains

Here, you can find the User Stories divided into the three Text+ data domains (Collections, Lexical Resources, Editions) or a thematically overarching category (Comprehensive).

{{<image img="gfx-user-stories/percetange_per_data_domain-768x512.png" alt="Number of User Stories by Text+ Data Domains"/>}}

{{< user-stories/accordion-textplus-domain >}}
