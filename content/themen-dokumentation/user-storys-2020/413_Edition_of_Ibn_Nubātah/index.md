---
title: "Edition of Ibn Nubātah al-Miṣrī"

type: user-story

slug: user-story-413

aliases:
- /en/research-data/user-story-413

dfg_areas:
- 105 Literaturwissenschaft

text_plus_domains:
- Editionen

authors:
- Hakan Özkan (Universität Münster)

---




#### Motivation


At the Institute for Arabic and Islamic Studies at the University of Münster, the research group ALEA (Arabic Literature Eleventh to Eighteenth Centuries) is working on the edition of the complete works of the Egyptian poet Ibn Nubātah al-Misrī (1287-1366).  

I am responsible for this research group and represent the interface between the developers at the SCDH (Service-Center Digital Humanities of the ULB Münster) and the scientists. My previous experience in this area is naturally limited, as I am new to the field of DH. Therefore, I can only give a limited account of the initial situation in our field.  

We have chosen the XML format according to TEI standard. In addition, we have compiled the sources that are relevant and useful for us. These are available in different forms: e.g. as lexical resources (especially the Perseus Digital Library www. perseus. tufts. edu / hopper / collection? collection = Perseus: collection: Arabic) as well as digital collections of old texts relevant to Arabic and Islamic Studies (in particular  [https://www.shamela.ws](https://www.shamela.ws/)). These are not always Open Access Libraries, or the formats or encodings do not correspond to the TEI standard we have chosen for our work with the texts of Ibn Nubātah. Some resources like the Perseus Digital Library can be accessed via an interface. With the Alpheios extension, morphological information and definitions from Lane’s extensive Arabic-English dictionary (located in the Perseus Digital Library) can be accessed as a popup by double-clicking in the browser. We absolutely want to implement such connections. Therefore, there are no tags for dictionary definitions, except in cases where Lane does not cover the intended meaning. In the latter cases, a separate glossary will be prepared, which we want to make available to the general public as an Open Access Library. The integration of texts from the currently emerging (machine-readable) corpus of pre-modern Arabic texts [OpenITI](https://openiti.org/) will be evaluated and possibly implemented over the next few years.  

Throughout the entire duration of the project, registers on persons (Common Standards File or Virtual International Authority File) and places (GND, VIAF, Geonames) will be continuously updated and provided with an ID from the corresponding standards files (if an entry is available) to allow identification of persons and places across projects. The IDs noted in the register entries, which are taken from the standard data sets from VIAF and Geonames, can be used to relate further data via the JSON format. These IDs also allow the integration of further editions, lexicons and projects via BEACON interfaces that are relevant for our editions. 

As a persistent citation system, DTS (Distributed Text Services) is to be implemented, since it supports a delivery of the data via a REST-API in JSON/LD format. Projects relevant for us like Perseus Library, Alpheios and others are already working with this standard. However, the prerequisite is that the currently still existing problems regarding the resolution of URIs and other difficulties regarding identifiers as well as ontologies of metadata are solved.  

The GeoNames-ID provides geographical coordinates that can be used to call up places and institutions within maps of the freely accessible map service OpenStreetMap.  

All Ibn Nubātah texts are published under a free open access license in the text repository of the WWU Münster, which is currently under construction. Based on this, an independent web portal for the digital edition will be designed and released. In this portal an appropriate presentation of the edition, its structure and the individual works will be created. For this purpose, suitable presentation formats, such as dynamic HTML pages, will be created from the XML files using XSLT and the content will be prepared for different search engines using search engine technology. 

#### Objectives 


It is very important to us that the formats and encodings we have chosen, as well as interfaces and citation systems, are supported by NFDI and that an exchange of knowledge takes place in order to keep up with new developments, especially as our project is extends over 12 years. Furthermore, it is of great importance for our department that Arabic and mixed texts (Arabic + Latin) are fully supported. As my current state of knowledge does not allow me to give further details on specific requirements, I would be particularly concerned that NFDI should be able to react flexibly to requests for adaptation. Unfortunately, the very short deadline for submitting the user stories does not allow me to go into detail on some points. This would have required prior consultations with our Service Center DH.

