---
title: "Old High German Dictionary: External Links"

type: user-story

slug: user-story-520

aliases:
- /en/research-data/user-story-520

dfg_areas:
- 104 Linguistics

text_plus_domains:
- Lexical Resources

authors:
- Uwe Kretschmer
- Brigitte Bulitta (Sächsische Akademie der Wissenschaften zu Leipzig)

---




#### Motivation


The “Althochdeutsches Wörterbuch” (AWB) is a ten-volume dictionary project that attempts to make the earliest known German words from all types of texts accessible. Since 2017 the AWB is partly available online and is constantly being expanded. 

The word forms in the AWB are described phonetically, grammatically, semantically and syntactically, including the corresponding contexts. This information is supplemented by references, which are interpreted and listed in full, with the contexts being cited depending on their meaningfulness or difficulty. 

#### Objectives 


We aim to create intertextual references to other digital resources that the dictionary accesses or, vice versa, to the AWB. It would be valuable to link the document citations to the sources from which they originate, so that the wider context beyond the document citation is available if required. Furthermore, the AWB could be intertextually linked with other German or Latin dictionaries. This could be achieved by linking, or even embedding certain parts of the dictionary entries of the networked dictionaries. 

#### Solutions 


In order to create possibilities for linking dictionary resources, extensive specifications are required from NFDI Text+. In particular, the standardization of interfaces should be focused on to enable the exchange of dictionary data at different levels of granularity. 

#### Reviewby Community 


We are happy to use solutions proposed or worked out by Text+ for the problems described and look forward to an exchange of experiences. 

