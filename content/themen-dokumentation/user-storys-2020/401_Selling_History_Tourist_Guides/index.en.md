---
title: "Selling History: Tourist Guides, Bazaar Histories, and the Politics of the Past in Late 20th and Early 21st Century IndiaA corpus of texts in Indian languages"

type: user-story

slug: user-story-401

aliases:
- /en/research-data/user-story-401

dfg_areas:
- 102 History

text_plus_domains:
- Collections

authors:
- Neeladri Bhattacharya (Jawaharlal Nehru University, New Delhi)
- Indra Sengupta (MWS-IBO New Delhi and MWS-GHI London)

---




#### Motivation


Within the framework of the BMBF-funded Indo-German International Merian Centre Metamorphoses of the Political, this collaborative project of the MWS India Branch Office, run by Dr. Indra Sengupta (MWS-IBO New Delhi and MWS-GHI London) and Professor em. Neeladri Bhattacharya (Jawaharlal Nehru University, New Delhi) with research assistants, has collected cheaply produced and locally circulated travel guides, chap books and local histories on sites of historical, religious and social relevance that have played or continue to play a significant role in shaping politics and political shifts in India in the late 20^th^ and early 21^st^ centuries. The texts are in Indian regional languages, so far in Hindi and Bengali. In addition, our researchers have taken photographs of the sites. The aim is to use this textual material, along with other collections of oral narratives that will follow, to understand ‘popular’ historical consciousness outside the academy and study the relationship between an understanding of the past and political change. The collection provides us with locally embedded, historical-geographical information about the sites. It contains histories that have been partly taken out of official accounts and sometimes mythical tales about the historical characters that are related to the sites. In a country that is well known for historically neglecting archives, where historical sources are at best unevenly preserved and records patchily available, where access to archives can be difficult and where the tradition of maintaining written family records or letters is weak and limited only to middle-class, urban populations, this collection will augment official records. The material we have collected has rarely been collected in a systematic form, as this would involve a massive, nationwide enterprise of collecting on a minute, local level that has not yet been undertaken. Our project has initiated what we hope will become a systematic archive of text and oral history sources that will remain housed at the IBO in New Delhi even after the end of the ICAS:MP project. 

#### Objectives


The main objective of the collection is to enable us to understand the relationship between ideas of history and the political in areas of India that are far removed from the more secular, urban spheres that long dominated national politics. It will enable us to make sources that are scattered, not available in official archives, which are nevertheless essential to understand historical thinking and political change, accessible to a wider scholarly public. We have done this by collecting the material as a part of an ongoing process and storing it at the IBO; we are in the process of creating a detailed, digitised and annotated index of the collection along with a long introduction to the material we have collected. We are awaiting clear guidelines on digitisation, following which we intend to digitise the entire collection and make it available online. At present, access is by means of permission and physical presence at the IBO in New Delhi. Once accessible to all, we believe this collection will become an invaluable resource for scholars of South Asian history and those working on historiography or historical method. 

#### Solution


At present, the collection is a physical one. We are in discussion with the Merian Centre ICAS:MP regarding the creation of guidelines to digitize the collection. The index is being prepared for digital use with appropriate search functions. However, this work is still at an early stage and we believe this is where we would be able to work closely and meaningfully with the NFDI Consortium TextPlus.

#### Challenges


The collection faces several challenges:
* What kind of digitization tool can we use to make sure this collection is usable for a wider audience than the project ICAS:MP?
* How do we deal with the problem of digitizing material in Indian languages and make this accessible to users with no knowledge of Indian languages? How do we go about translating and transcribing the material?
* How can we create long-term, sustainable data storage both for the sake of long-term preservation itself and also to ensure usability by future users? How do we ensure the data remains usable even with changes in digital technology in future. 

We believe support, collaboration and exchange of know-how with TextPlus would help us overcome these challenges.

