---
title: "Delivering Support for Digital Edition Projects"

type: user-story

slug: user-story-422

aliases:
- /en/research-data/user-story-422

dfg_areas:
- 102 History
- 107 Theology

text_plus_domains:
- Editions

authors:
- Thomas Stäcker (ULB Darmstadt)

---




#### Motivation


In recent years, digital editions have become increasingly important in both the theologies (DFG subject area 107) and the historical sciences (DFG subject area 102). At the same time, the growing number of projects at different funding institutions makes it increasingly difficult to obtain an overview of existing or ongoing edition projects and identify related projects in terms of methodology and content. Although a technical-scientific exchange between such projects often takes place within the individual funding institutions (e.g. academies), there is no easily accessible platform that would enable cross-institutional networking. Such a platform would be practical for project management and the solution of editorial problems in related projects. 

#### Objectives


I would like to see Text+ develop resources for scientific community building in the field of digital editing. In many edition projects, comprehensive expertise in the planning and creation of digital editions (technical implementation of editorial requirements, personnel planning, virtual working environments, etc.) is now available. If other projects can draw on this experience in the future, it would make designing efficient and practical workflows easier. In our projects  [“Religious Peacekeeping and Peace Foundation in Europe” and “European Religious Peace Digital – EuReD”](http://www.religionsfrieden.de/) we were able to benefit and profit in many ways from the experiences of the employees in other edition projects. Making such experiences available to the scientific community across projects would not only be helpful for current projects but would also facilitate the planning and application for new projects in the long term. 

