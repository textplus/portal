---
title: "Discourse Analysis"

type: user-story

slug: user-story-518

aliases:
- /en/research-data/user-story-518

dfg_areas:
- 104 Sprachwissenschaften

text_plus_domains:
- Domänenübergreifend

authors:
- Use case based on the project Diskursmonitor and the integration of lexical resources described by Friedemann Vogel and Jan Oliver Rüdiger (Universität Siegen)

---




#### Motivation


Strategic communication is a process of infusing communication with an agenda, typically used by organizations to urge people to take specific actions or to advocate particular ideas. 

This project focuses on the question of how political interests are promoted through the use of strategic communication. An online platform is under construction that includes extensive information about the terms and methods used in strategic communication, as well as real-time analysis of current political discourse. 

The web portal is intended for people who work in fields such as politics, media, education, and justice, as well as researchers in areas such as social science and cultural sciences. The goal is to increase awareness and recognition of strategic communication, thereby reducing its effectiveness. 

The discourse analysis algorithms could be improved by integrating semantically related terms and concepts, as represented in wordnets. 

#### Objectives


The objective is to use wordnet data to improve the discourse analysis algorithms. A lexical-semantic wordnet consists of concepts that are linked to each other via relationships. The hypernym relations and semantic relatedness calculations are of particular interest for this project. 

#### Solution


The lexical-semantic wordnet GermaNet is ideal for the needs of this project, especially since the API has built-in capabilities for semantic relatedness calculations. However, the API would be more complete if it included relatedness functionality based on deep learning processes. 

#### Challenges

* Regular updates to the GermaNet lexical resource 
* Development of relatedness functionality based on deep learning methods

