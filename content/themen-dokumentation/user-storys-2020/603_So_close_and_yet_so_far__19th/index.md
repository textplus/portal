---
title: "So close and yet so far – 19th century library registration versus 21th century digital research. Appeal for equal opportunity in research"

type: user-story

slug: user-story-603

aliases:
- /en/research-data/user-story-603

dfg_areas:
- 102 Geschichtswissenschaften
- 104 Sprachwissenschaften
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft

text_plus_domains:
- Domänenübergreifend

authors:
- Eva Schmucker (Friedrich Schiller Universität Jena)

---




#### Motivation


As someone who considers themselves a cross sectional field researcher combining different aspects of European history (102), linguistics (104) and social and cultural anthropology (106) it has been an emotional roller coaster ride exploring the possibilities of digital research over the last few months of Covid-19 induced home-based work. Thankfully, the volume of essential stored data (scanned documents and books) is increasing on a daily basis, yet the accessibility is sometimes still frustratingly limited. As a researcher working from Germany I already enjoy many privileges and research opportunities compared to those in many other European and non-European countries. The challenges and limitations for others must be accordingly worse, so I hope my ideas to improve digital accessibility will contribute to equal opportunity in research world wide.

#### Objectives


Raise digital accessibility to European historic documents and books for researchers all over the world regardless of their place of residence, current location, personal and institutional funding situation, family situation (e.g. taking care of children and other family members), personal health issues or other restrictions that prevent them from travelling to different libraries and other research locations. 

  
1. Although it is now possible to access almost every library catalogue in Europe online and search for digitalized content, that said digitalized content is oftentimes blocked for external users. The problem is: many historic documents and texts have been edited and published in collections of source material. Although the historic sources as such contained in the collections are no longer protected by copyright laws, the newly edited books of course are.  Just one example: *Deutsche illustrierte Flugblätter des 16. Und 17. Jahrhunderts. Band IX. Die Sammlung des Kunstmuseums Moritzburg in Halle/ a.S., eds. Ewa Pietrzak, Michael Schilling. Berlin: de Gruyter 2018.* It’s digitally available for example in the Austrian National Library (ÖNB). Only registered users of said library may of course access the content (due to the copyright barriers mentioned above). One could argue it’s an effort worth taking to go through the quick and easy process of (free) registration for each national, state and other public/private library all over Europe. However, the process of registration is far from quick and easy, because:

a. you have to have to be a permanent resident in Austria in order to get a readers’ pass  
b. you have to appear at the ÖNB in person in order to finish your registration process and pick up your readers’ pass

This is just one specific example, the restrictions also apply to other European national, state and university libraries.

2. We still lack specific and comprehensive search engines and portals for sources in the research fields mentioned above.

#### Solution


1: Enable researchers from all over the world to register at European national and state libraries regardless of the researchers’ place of residence, and without having to travel in-person to each library. To do so, appropriate ways of online user identification/verification must be established without violating current Data Protection Acts. Ideally, form some kind of association of European national libraries so you only need one registration and one readers’ pass for several libraries.

2: Create a specific and comprehensive search engine for digitalized content in the fields of European history, literature, art, music, linguistics and ethnography.

#### Challenges


1: Undoubtedly, there will be many challenges ahead in such a comprehensive European-wide effort to make digitalized library content more accessible. But I think it’s worth it, beyond considering the current pandemic threat. It would mean equal opportunity for researchers world wide – independently of their personal funding situation, family situation and regardless of any physical travel obstacles.

2: Regarding the search engines, I don’t see too many challenges apart from finding skilled people and funding for such a project.

