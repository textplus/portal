---
title: "Intermedial Editions: Challenges for Infrastructure and Opportunities for Science Research"

type: user-story

slug: user-story-412

aliases:
- /en/research-data/user-story-412

dfg_areas:
- 102 History

text_plus_domains:
- Editions

authors:
- Torsten Roeder (Leopoldina, Halle/Saale)
- and DHd-AG Zeitungen & Zeitschriften

---




#### Motivation


Research on the development and history of science is often thematically oriented on the one hand and requires interdisciplinary approaches on the other. Research subjects include scientific communication networks, scientific publications as well as objects and the associated measurement and research data: A history of astronomy is, for example, induced by the interaction of observation devices, tabular observation data, scientific correspondence and articles published in scientific periodicals. Recently, I supervised a research project in this area during its application period, which researches in exactly this area, specifically on the correspondence between two well-known astronomers of the early modern era. At the same time, the working group “Zeitungen & Zeitschriften” provided a framework in which possibilities and requirements for scientific work with digitized periodicals could be reflected and discussed. 

#### Objectives 


But how can digital editions deal with the diversity of research subjects if, in addition to providing commentary, they also aim to achieve a clear and comprehensible presentation? The digital medium seems to offer us possibilities for this. On the one hand, we believe that these possibilities have not yet been sufficiently exploited in terms of making science and its history accessible to the public. On the other hand, the relatively small project lacks sufficient technical skills for implementation and financial means for long-term availability: not only a project website would be needed there, but ultimately also a relational database, a digital edition framework, and an image server behind it. 

A further problem arises from the fact that digitized data, texts and objects from different museums, archives and libraries are to be used in the project. Due to the distribution to different locations, our sources are subject to different conditions of use, which make it difficult or even impossible to create an edition that can be presented in context and effectively re-used. In the area of intermedia editions, the “state of the art” is also in constant development, so that we have to keep ourselves constantly up to date. 

#### Solution 


Text+ would be predestined to bring together the various data providers within the NFDI who otherwise have little to do with each other: firstly, providers of digitized scientific historical periodicals and letter editions, secondly, science or natural history museums, and thirdly, providers of current and historical research data. A greater consensus could be reached with regard to re-use conditions and the persistence of the respective resources provided. The working group “Zeitungen & Zeitschriften” can also contribute to this mediation work. This would facilitate both the creation and the subsequent use of intermedia editions. 

Subsequently, Text+ could provide hosting for historical editions and guarantee the availability of the edition in the long term – and last but not least for its scientific quality. 

Within the NFDI’s overall structure, intermedial editions that link texts, objects and data could be a key part of the cooperation between Text+, such as the consortia NFDI4Objects and NFDI4Culture. Text+ would be the ideal platform to reflect on the subject matter of intermedial editions and to advance it in discourse with users, editors and developers. 

#### Challenges 


The greatest external challenge is probably in the area of legal issues. Text+ can possibly set up a mediation center to communicate best practices for providers and users – or to provide individual solutions. This can be supported for the area of digitized periodicals by the working group “Zeitungen & Zeitschriften”.

#### Review by community 


As a working group, we would like to get involved within a community for intermedia editions at workshops, trainings or discussion events with a focus on the connections between perodica and other knowledge objects. Furthermore, individual members can support the evaluation of the Text+ offerings or try them out with the help of a case study from the projects mentioned here or other projects. 

