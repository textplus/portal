---
title: "Scholarly Editing"

type: user-story

slug: user-story-421

aliases:
- /en/research-data/user-story-421

dfg_areas:
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft

text_plus_domains:
- Editionen

authors:
- Stefanie Acquavella-Rauch (Johannes Gutenberg-Universität Mainz)
- Thorsten Ries (Universität Regensburg)

---




#### Motivation


As scholarly editors and digital humanities scholars who work on hybrid digital editions of modern German literary authors and of musical compositions, we crucially rely on sustainable, standards-compliant, API-stable research data infrastructures as a basis of our research and development process and long-term data planning strategy. Scholarly editions are expensive long-term projects. Result of years of meticulous work by often large academic teams, their authoritative, highest-quality output is created to last and serve research for decades. While books and libraries stand this test of time reliably, the lifecycle of the technological basis of digital scholarly editions often cannot be planned for a period longer than five years. This sustainability gap can only be closed if sustainable, structured standard formats and compatible modules are developed, improved and adopted by long-term preservation infrastructures that ensure basic functionality, accessibility, and APIs for future research exploitation as part of an international corpus. This issue arises not only at the end of digital scholarly editing projects, but already in the funding bid phase, as DFG requires a long-term data management plan.     

#### Objectives 


Text+ will serve as part of the sustainable research data management infrastructure, NFDI specialised in keeping textual scholarly data in generated in specific academic standard formats accessible and functional. Academic data standards such as MEI- and TEI-XML, its precursors (e.g. SGML, etc.) and their future interpretations (e.g. based on RDF / OWL, JSON, IIIF, NOSQL etc.) are not industry standards – in fact, they are often heterogeneous –, and will as such not be fully supported by industry-grade long-term repository systems. Functionality, access and exploitability of research results and expertise, based on years of work by large research teams, are at risk for many projects. Text+ is being created to provide sustainable functionality, access and API-functionality for digital scholarly editions in DFG essential format (a TEI-XML flavor), as well as the abovementioned structured data formats for digital scholarly editions. To streamline and standardise long-term preservation data formats, Text+ should support digital humanities research with developing open-source scholarly editing as well as publication platforms and standards. An important task will be to enable communication between the various disciplines in the humanities involved in questions of scholarly editions. Text+ also needs to invest in structural conversion applications that migrate TEI-XML to RDF (e.g.) while retaining structural, semantic and functional integritiy.  

#### Challenges 


This R&D process is as challenging as it is necessary. Balancing preservation of specific functionality, markup vs. standardisation, long-term preservation, migration and implementation to modern formats will be a difficult negotiation between multiple stakeholders, interest groups and practitioners. The AgE-DH commission for digital scholarly editing of German texts (AgE-DH of the *Arbeitsgemeinschaft für germanistische Edition,*AgE) has agreed to collaborate, assist and provide guidance in this long-term process, while advising members of the academic community on how to integrate NFDI Text+ into their research projects. Furthermore, the AgE-DH is willing to provide a workshop structure that will help establishing ways of exchange and communication between the various disciplines in the humanities that are involved in questions of scholarly editing. A documentation of the workshop results will enable further exchange within the community to determine its success. 

