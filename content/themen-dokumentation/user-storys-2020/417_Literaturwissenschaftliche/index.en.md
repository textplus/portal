---
title: "Literaturwissenschaftliche Editionen am Beispiel “Lyrik des deutschen Mittelalters”/ Literary editions using the example of “Poetry of the German Middle Ages“"

type: user-story

slug: user-story-417

aliases:
- /en/research-data/user-story-417

dfg_areas:
- 105 Literary Studies

text_plus_domains:
- Editions

authors:
- Manuel Braun (Gesellschaft für Hochschulgermanistik im Deutschen Germanistenverband/ Universität Stuttgart)

---




#### Motivation


Literary studies are increasingly turning to conceiving of large edition projects as digital from the outset because they are able to reach a broad public directly (open access; linking of tools such as dictionaries, encyclopedias), because they remain closer to the original text and at the same time can offer a variety of edition texts (provision of transcriptions and digital copies, editions of all versions with different degrees of editing), because they can thus record large amounts of data in different formats (texts, images, videos, sound recordings), because they can thus refer directly to contexts (linking of source texts, from other digital editions), etc. 

Funding institutions such as the DFG or the academies now also routinely ask applicants to plan their edition projects as digital. Currently, however, only the development of the editions is funded, not their long-term archiving. This also applies to DFG long-term projects such as “Poetry of the German Middle Ages” (LDM,  [https://ldm-digital.de](https://ldm-digital.de/)), which is now in the second of a total of three possible funding periods and whose editors are increasingly asking themselves what will happen to their editions in the medium to long term after completion or after the funding periods have expired.  

#### Objectives


Up to now, long-term archiving has been primarily intended as a means of backing up the acquired text data. In their applications, the participating literary scholars assure that they will encode their data in standard formats such as XML/TEI or manage their data in a secure way and thus ensure their readability for the future. If necessary, they name another institution that promises to host the data on their server in the long term. 

The preservation of raw text data seems to be solved today. However, the permanent availability of an edition such as the LDM, which depends on considerable public funding and a considerable amount of working time, requires a lot more: the permanent availability or retrievability in the network without loss of functionality. Data backup in a comprehensive sense therefore means not only the archiving of text data in XML/TEI (or other standard formats), but also the permanent preservation and maintenance of the complex database structures (for example in XML or MySQL) and the user interface. Otherwise, in the foreseeable future, access to the complex text data will only be possible with considerable IT expenditure, which a typical user with a research background in literature or linguistics cannot afford. The data is then available, namely as machine-readable data sets, but in the worst case it is lost for the compartments themselves.  

#### Solution


From the perspective of literary editors, there is currently no way to automatically adapt digital editions to changes in the common IT infrastructure (browser, databases, Java, etc.) so that they remain permanently available. Perhaps it would be possible to define a standard for interfaces that would make this task easier – a standard that would, of course, be able to preserve the full functionality of individual user interfaces as far as possible. It can also be assumed that with the establishment of standard solutions at permanent institutions (e.g. large libraries), newly emerging digital editions will opt for these standard solutions from the beginning. At the same time, however, it will be necessary to consider the data stock of existing digital editions when developing these standard solutions.  

#### Review by the Community


The edition project “Poetry of the German Middle Ages” is willing to constructively accompany the development of proposed solutions to the problem of the permanent usability of editions and to evaluate them. At the same time, there is a willingness to use the LDM data structure – at reasonable amount of work – to modify the project so that it is part of a standardized repository, in which both: the text data and its functionality are maintained. 

