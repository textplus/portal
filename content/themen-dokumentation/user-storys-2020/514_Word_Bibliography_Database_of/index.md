---
title: "Word Bibliography Database of the Göttingen Academy"

type: user-story

slug: user-story-514

aliases:
- /en/research-data/user-story-514

dfg_areas:
- 104 Sprachwissenschaften

text_plus_domains:
- Lexikalische Ressourcen

authors:
- Volker Harm (Akademie der Wissenschaften zu Göttingen)

---




#### Motivation 


Historical Linguistics of German boasts a rich tradition of research into historical word use, word history, and etymology. This tradition mainly roots in the paradigm of philology as it evolved during the 19th century, but it has also much benefitted from increasing research during the last decades. The growing interest of History into the evolution of concepts (in German *Begriffsgeschichte*) has also contributed to the research field of word history in a significant way. Often, word histories of German can only be described in a comparative perspective, which imposes generally a strong multilingual bias in lexicological research. In many cases, therefore, research literature on German words can be looked up successfully even when information on English or Dutch words (for example) is needed. 

#### Objectives 


With the Word Bibliography Database of the Göttingen Academy, I intend to provide a reliable and up-to-date research tool for all scholars who are interested in the history of words, in historical semantics, and historical texts. In particular, the database is designed for all lexicographers currently working in one of the larger historical dictionary projects (as, just to mention a few, Althochdeutschen Wörterbuch, Mittelhochdeutsches Wörterbuch, Deutsche Wortfeldetymologie, Wortgeschichte digital). Given the extremely large corpora those dictionaries are based on and the high philological standards they have to meet, the dictionary offices have to work in a efficient way and under much pressure. In this situation, any tool that could spare the lexicographer from endless bibliographical searches is highly welcome. In this sense, a reliable database giving direct access to the rich tradition of lexicological research could relief the lexicographer of a significant amount of work. Therefore, one of my aims in the Text+ initiative is to keep the bibliographical database up-to-date and to fill in still existing gaps in its coverage. 

But next to the improvement and enlargement of the database, I also intend to turn the database into a tool for a better dissemination of current research. The idea is that researchers can implement their own results of research in the database by a user interface. By this way, research in the field can easily be made accessible for the community. – I give an example: When working on the entry *Bildung* (‘culture, education’) in the *Deutsches Wörterbuch* von Jacob und Wilhelm Grimm, I published a paper on the history of these words in a journal. If I had the possibility to integrate the paper in the database by myself (by implementing the lemmata I treated and the key words of my paper), it would have been a nice tool for a better dissemination of my research. If my co-researchers in the field used the same tool, the bibliography would be growing by itself and out of the community. – What is important for me in this context is that not only research literature in the proper sense of the word is included in the database, but also, master theses and doctoral dissertations. By integrating the work of young researchers, which often remains inaccessible otherwise, can be made useful for the community. This, of course, entails a certain amount of quality management for which routines have to be developed, but the benefit surely is greater than the costs. 

#### Solution 


Text+ can provide human resources in order to close gaps in the data and to integrate more recent research which is still underrepresented in the database. Furthermore, Text+ can provide resources in order to improve the functionality of the database and to create an user interface. 

#### Review by Community


As the integration of the community is a key characteristic of the project, other researchers in the field of Historical Lexicography/Lexical Resources will be involved from the beginning. The project will be repeatedly presented at workshops and conferences, e.g. the “Arbeitsgespräch zur Historischen Lexikographie” or the “Jahrestagung der Gesellschaft für Geschichte der deutschen Sprache”; on these occasions, the community can articulate needs and wishes, and the tool can be evaluated. 

