---
title: "Knowledge Relations between Polish People’s Republic and Iraq in Architecture and Planning"

type: user-story

slug: user-story-343

aliases:
- /en/research-data/user-story-343

dfg_areas:
- 102 Geschichtswissenschaften
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft

text_plus_domains:
- Collections

authors:
- Dorota Woroniecka-Krzyżanowska (DHI Warschau)

---




#### Motivation


I am a social anthropologist specialising in Arabic language and culture. Currently, I am a research fellow at the  [German Historical Institute Warsaw](https://www.dhi.waw.pl/) (part of the  [Max Weber Foundation — German Humanities Institutes Abroad](https://www.maxweberstiftung.de/en/ueber-uns.html)). In my project, I analyse the intellectual and academic exchange between two countries, one from the so-called Eastern Bloc and one from the Middle East, from the 1950s to 1989. The project is methodologically based on the analysis of archive materials available in both countries, in-depth interviews with participants and witnesses of the exchange, as well as online resources and communication. 

To collect my research data, I have travelled to Iraq several times and researched several archives in Poland. In addition to that, I recently discovered material that former students share on private websites and Facebook, which includes not only stories about their memories on Polish lecturers, but also unpublished archival materials and photographs. With some of the research participants I was able to establish online communication through which I received written memoires and private messages sharing stories, reflections, and interpretations. 

Apart from the oral and textual sources, I have collected a lot of photographs, digitalized sketches, drawings and architectural designs that form a visual record of this cooperation. Other than this diversity of sources, the linguistic aspect must also be considered, for these are sources in different languages — English, Arabic, and Polish — that is in Latin and non-Latin script.

#### Objectives


Text- and language-based data, both written and oral, has to be collected and if applicable converted into digital form, transcribed, and translated. The long-term preservation must be ensured. This is especially challenging for private websites and Facebook posts in different languages and scripts. Thus, tools for creating, managing, storing, and publishing multimedia material including different data types and formats in accordance with the law are necessary. Given the topic of my research project and the resulting importance of visual data, such as photographs documenting the relations between the Polish staff and the Iraqi students and colleagues, students’ projects and drawings, it is important to have a methodological tool that allows for proper archiving and processing of the collected photographs, as well as — possibly — publishing it online in the form of an open archive. 

Due to wars and political unrest, many universities in Iraq — including the University of Mosul that I focus on — lost their archives. The destruction affected not only the buildings, but also all documentation that formed the institutional memory of these academic institutions. Therefore, it is important that the photographs, documents, and other materials collected from private individuals are properly digitalized, systematized, and archived; if agreed by the owners, and in light of the absence of formal archives, these materials could be stored and published in an English-Arabic online community archive that could be used by the alumni, the university, and researchers as a source of historical data. 

For several reasons, collecting data and conducting interviews in the Middle East is for a great many researchers from Europe or other regions difficult. This is why I would like to make (some) material accessible to researches from various disciplines such as social sciences, history, area studies, and cultural studies. My research focuses on the Iraqi perception of the Polish-Iraqi academic exchange in architecture and urban planning and therefore it is important for me to be able to make the Arabic sources — both oral and textual — accessible to a broader research community. This requires translation in different formats, e.g. subtitling of interviews, which is very time-consuming. I believe, however, that making Arabic resources accessible for wider audiences is an important step in decolonizing social sciences and developing a less Eurocentric approach to the region. Therefore, the data base would have to incorporate tools for working with Arabic and Latin script, independent from the user’s given hard- and software. 

When storing, processing, and publishing excerpts from the biographical interviews, I have to observe privacy laws. I would like to use tools and storage solutions that function in accordance with legal and ethical guidelines for privacy protection.  

