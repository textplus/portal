---
title: User Stories
type: user-story
hide_in_list: true
menu:
  main:
    weight: 80
    parent: themen-dokumentation
---

# User Stories 2020

{{<lead-text>}}  
 Zur Integration des Nutzendenbedarfs haben wir Forschende eingeladen, sich mit User Stories aus ihrem Forschungsumfeld zu beteiligen. Ziel war es, eine große Bandbreite der an Text+ interessierten Disziplinen, Datendomänen und Forschungsfragen zu erfassen.
{{</lead-text>}}

Nach der Bewilligung des überarbeiteten Förderantrags von Text+ in der zweiten NFDI-Runde haben wir 2020 die Community um User Stories gebeten. In kurzer Zeit erreichten uns mehr als 120 User Stories, von denen wir mit Zustimmung der Autor:innen 115 hier veröffentlichen können. Die Beiträge sind einmal entlang der drei Datendomänen von Text+ (Collections, Lexikalische Ressourcen, Edition) geordnet, erweitert um eine vierte, thematisch übergreifende Kategorisierung (in der Grafik unten als "Comprehensive" bezeichnet); außerdem werden sie auch nach der DFG-Fachsystematik (Fachkollegien 101–113) aufgelistet.

Vielen Dank an die aktive Community für die wertvollen Beiträge und die Bereitschaft, Text+ zu unterstützen! Ihre Rückmeldungen sind wichtig, um das Angebot von Text+ gemeinsam zu gestalten. Generell sehen wir diese Beiträge als ein wichtiges Element der Partizipation an Text+. Die User Stories wurden auf Grundlage [dieser Vorlage](files/Text_User-Stories_Template_dt.pdf) eingereicht. Der [Gesamtbericht](https://doi.org/10.5281/zenodo.5384085) wurde im September 2021 veröffentlicht. Die [Daten für die Analyse](https://dx.doi.org/10.20375/0000-000E-67ED-4) wurden im DARIAH-DE Repository veröffentlicht.

Die Beiträge haben insgesamt einen Schwerpunkt in den Sprach- und Literaturwissenschaften (104–105) und nur wenige beziehen sich ausschließlich auf Geschichte (102) oder Kunst-, Musik-, Theater- und Medienwissenschaften (103). Überraschend stark engagiert haben sich die Klassische Philologie (101) und die Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaften (106) sowie Theologie (107) und Philosophie (108). Auch sozialwissenschaftliche Fächer (111) haben sich zu einem kleinen Teil an unserem Call beteiligt. Daneben weisen einige User Stories einen dezidiert interdisziplinären Fokus auf. Viele User Stories nehmen detailliert Bezug auf infrastrukturelle Fragen oder mögliche Dienste von Text+ und zeigen anhand individueller Forschungsfragen sehr konkret, welche Bedarfe, aber auch welche Lösungsvorschläge sie einbringen.

Es zeichnet sich ab, dass je nach Kontext, ob Einzelprojekt, größere Forschungsgruppe oder AG die Einschätzungen über das, was Text+ und die NFDI leisten können und sollen, divergieren. Als roter Faden zieht sich die Beschäftigung mit der Verfügbarkeit und Nutzbarmachung beschränkt zugänglicher Forschungsdaten durch viele User Stories. Ein weiteres wichtiges Anliegen zielt auf die Möglichkeit ab, wertvolle, aber vielleicht weniger prominente Daten aus kleinen Sprachen oder individuellen Forschungsprojekten mithilfe von Text+ nachnutzbar zu machen. Auch die scheinbar schlichte Verknüpfung verteilter Ressourcen ist ein Anliegen, das weiterhin präsent und für die Community noch nicht gelöst ist.

Die User Stories wurden von einem Team des Text+-Konsortiums gelesen. Insgesamt haben wir 118 aus der Community eingereichte User Stories analysiert und mit Schlagwörtern versehen. Die meisten Stories sind mit mehreren der insgesamt 67 verschiedenen Begriffe verschlagwortet, einige mit einem einzigen, andere mit bis zu 13. Im Durchschnitt wurden sechs Schlagwörter pro User Story vergeben. Insgesamt wurden 773 Begriffe auf alle User Stories verteilt.

Die Schlagwörter fassen mehrere Aspekte zusammen, die im Kontext der NFDI und für die Anforderungen an Text+ wichtig sind. Beispiele für die Auswahl der Schlagwörter sind: Ob Daten selbst produziert oder benötigt werden (data producer, interest in further data), welches Datenformat vorliegt, die Mehrsprachigkeit der Daten, die FAIR-Prinzipien, möglicher Bedarf an Interaktion zwischen mehreren Ressourcen sowie die  Art der Ressourcen (corpus-corpus, lexical resource-corpora linking).

{{<image img="gfx-user-stories/percentage_keywords_all-1-2048x958.png" alt="Anteil der User Stories für die 10 meistgenutzten Schlagwörter"/>}}

{{<image img="gfx-user-stories/percentage_keywords_collections-1-2048x942.png" alt="Anteil der User Stories in Collections für die 10 meistgenutzten Schlagwörter"/>}}

{{<image img="gfx-user-stories/percentage_keywords_Lexical_Resources-1-2048x863.png" alt="Anteil der User Stories in Lexikalische Ressourcen für die 10 meistgenutzten Schlagwörter"/>}}

{{<image img="gfx-user-stories/percentage_keywords_editions-1-2048x1013.png" alt="Anteil der User Stories in Editionen für die 10 meistgenutzten Schlagwörter"/>}}

{{<image img="gfx-user-stories/percentage_keywords_comprehensive-1-2048x973.png" alt="Anteil der domenübergreifenden User Stories für die 10 meistgenutzten Schlagwörter"/>}}

## User Stories nach DFG-Fachsystematik

Die Ordnung richtet sich nach der [DFG-Fachsystematik](https://www.dfg.de/resource/blob/172316/5863ef132d178054609f74940f6a27c9/fachsystematik-2016-2019-de-grafik-data.pdf) der Jahre 2016–2019 auf der Ebene 101–113, die Subdisziplinen werden aus den Texten selbst ersichtlich. Allen User Stories wurde eine Zahl zugeordnet, Mehrfachnennungen waren möglich. 

{{<image img="gfx-user-stories/Userstories_number_of_dfgsubject_area-1536x768.png" alt="Anzahl der User Stories nach DFG-Fachsystematik"/>}}

{{< user-stories/accordion-dfg-area >}}


## User Stories nach Datendomänen von Text+ 

Hier finden sie die User Stories aufgeteilt auf die drei Datendomänen von Text+ (Collections, Lexikalische Ressourcen, Edition) bzw. einer thematisch übergreifenden Kategorie (Comprehensive) zugeordnet. 

{{<image img="gfx-user-stories/percetange_per_data_domain-768x512.png" alt="Anzahl der User Stories nach Datendomänen"/>}}

{{< user-stories/accordion-textplus-domain >}}
