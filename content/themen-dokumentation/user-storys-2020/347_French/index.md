---
title: "French “Connaisseur”"

type: user-story

slug: user-story-347

aliases:
- /en/research-data/user-story-347

dfg_areas:
- 101 Alte Kulturen
- 102 Geschichtswissenschaften
- 103 Kunst-, Musik-, Theater- und Medienwissenschaften
- 111 Sozialwissenschaften

text_plus_domains:
- Collections

authors:
- Marietta Horster (Berlin-Brandenburgische Akademie der Wissenschaften)

---




#### Motivation


I am an ancient historian. I just have given to a professional digitising entrepreneur in Berlin a notebook of a French “Connaisseur” (dated circa 1870), who had copied, traced, scratched and commented the later categorised “instrumentum domesticum” (individually or serially inscribed small objects of metal or clay). The most charming part of this notebook are small tracing papers cautiously glued into the pages of the book, so that one may fold them open to detect the text below.   

I detected by chance that the German Archaeological Institute has just restored a manuscript of Carl Otfried Müller of 1861 with glued-in small papers with written texts. Is there a chance to find this kind of information in the 19th century digital editions concerning the materiality of the text and its organisation? I would like to work on the aesthetics and the functionality of this very specific way to compose a text and to present information, not always but at least in these two cases as a preparatory study for a 19th century publication. 

#### Objectives 


The data I need should come from editions of the late 18th to the 19th century. Before even starting to venture in the content, I would need information to the organisation of the notebooks and other kinds of preparatory studies, perhaps even mixing handwriting to printed text. 

#### Challenges 


None: Text+ has profound expertise in digital editions of texts. 

#### Review by Community 


Yes, I would like to. 

