---
title: "Attitudes towards regional language forms in the city: Low German in Hamburg (NiH)"

type: user-story

slug: user-story-338

aliases:
- /en/research-data/user-story-338

dfg_areas:
- 104 Sprachwissenschaften

text_plus_domains:
- Collections

authors:
- Ingrid Schröder (Universität Hamburg)

---




#### Motivation


Since the 1990s there has been a trend towards the analysis of qualitative research data on language attitudes and language biographies. Previous studies on the status of Low German (GETAS 1984, INS 2007, IDS/INS 2016), however, have mainly provided quantitative results on attitudes towards Low German in Hamburg. Since qualitative studies have remained a desideratum, we examine whether regional language in Hamburg is perceived as a local feature and whether the social-symbolic meaning has taken more significance compared to the communicative meaning. Linguistic knowledge is to be analyzed as well as the underlying language concepts and evaluations are to be analyzed. To find out how speakers identify Low German with Hamburg, both qualitative (language biographical interviews) and quantitative data (questionnaire survey) are collected and evaluated. The project has ended in March 2018. All project results are to be published in 2021. 

#### Objectives


The audio recordings and the annotated transcripts as well as the data from the questionnaire survey are stored by the Research Data Management of the University of Hamburg (FDM). We would like to make the qualitative data more accessible to the scientific community. This aim addresses the continuous adaptation of the data to current search engines and analysis tools. 

The data has not yet been made sufficiently anonymous. It concerns personal names, but also other biographical details, e.g. profession or place names.  They are necessary to understand the language biographies, but could reveal the person’s identity. Guidelines for the anonymization of biographical interviews under consideration of the legal basis are lacking and should be provided. Furthermore, the data material is also suitable for testing the significance of other qualitative data compared to quantitative data.  Such tests would be usefull for various sociolinguistic questions. 

