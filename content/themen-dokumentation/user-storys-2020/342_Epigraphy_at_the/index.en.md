---
title: "Epigraphy at the BBAW"

type: user-story

slug: user-story-342

aliases:
- /en/research-data/user-story-342

dfg_areas:
- 101 Ancient Cultures

text_plus_domains:
- Collections

authors:
- Matthäus Heil (Berlin-Brandenburgische Akademie der Wissenschaften)

---




#### Motivation


We are the world’s oldest project in the field of Greek epigraphy; we are editing the ancient Greek inscriptions of Europe. The results of our activities in studying and publishing are paramount for the entire spectrum in the  [DFG subject area](https://www.dfg.de/en/dfg_profile/statutory_bodies/review_boards/subject_areas/index.jsp) 101 Ancient Cultures. The objects of our research have a special position between those disciplines dealing with material objects and those dealing with texts. As material objects, inscriptions are safeguarded in museums of the countries of origin, with which we closely cooperate. The primary publications of the texts themselves are for the most part scattered, the publications often difficult to access and are in different national languages. The “Inscriptiones Graecae” provide a systematic collection and edition of inscriptions based on our own verification of the originals. The “Inscriptiones Graecae” cooperate with the most important databases (particularly that of the Packard Humanities Institute, USA) and with the Supplementum Epigraphicum Graecum (an annual catalogue of all the new publications in the field of Greek epigraphy). They also publish all the volumes that have appeared since 1958 in digital form (because of copyright restrictions currently without introduction, apparatus criticus or commentary) as well as translations in various languages (hitherto about 15,000 inscriptions). The offer is being continuously expanded both by publishing new volumes and by digitizing the old ones. In order to better document the objects we study we use paper squeezes, i.e. exact reproductions on a scale 1:1, as well as (digital) photographs. The “Inscriptiones Graecae” have also the world’s largest and oldest archives of paper squeezes (with over 100,000 items). All our data are processed electronically.  

#### Objectives 


Creating an institution for securing and making available our research data for the long term is a matter of existential importance for the future of our discipline. The information we generate (and which should be saved for the long term) could be interconnected with databases of other German institutions, which otherwise might get lost. It should be provided for that the data can be easily retrieved in every moment and used again and again, and that the intercommunication between the different collections of the international epigraphic community (and with the relevant collections of related disciplines) remains possible. The diverse and international character of the epigraphic disciplines makes it difficult to achieve the necessary interoperability by means of a general standardisation. Rather, it is necessary to create suitable interfaces and junctions (not least by using metadata) that allow the exchange without limiting the creativity of future research.  

#### Challenges 

* *Conditio sine qua non*is ensuring that a central institution has sufficient financial resources to store the data safely in the long term. 
* We want to emphasize once more the need of developing practical interfaces, connectors and metadata, which undoubtedly is a very complex and demanding task. All the relevant data stocks held by other NDFDI consortia concerned with humanities, their specifications and the structural conditions of foreign databases must be taken into account. 
* The NFDI should consider the fact that the “Inscriptiones Graecae”, like many other research projects, are closely linked to international partners who are not part of the NFDI. Networking of NFDI activities beyond national borders is indispensable. 
* The legal frame (and thus the financial security too) of storing epigraphic data sets and of making them available remains a fundamental problem. It would be very welcome if the NDFI could develop rules, in accordance with the basic needs of open access, in order to overcome the still existing restrictions in the area of copyright (text and image rights, etc.) and publishing rights and to allow a better handling of these data sets within the framework of NFDI. 

#### Review by Community 


Unconditional, yes.

