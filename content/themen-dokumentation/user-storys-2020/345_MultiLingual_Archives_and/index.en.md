---
title: "Multi-Lingual Archives and Small Fields of Study"

type: user-story

slug: user-story-345

aliases:
- /en/research-data/user-story-345

dfg_areas:
- 101 Ancient Cultures
- 102 History
- 104 Linguistics
- 106 Social and Cultural Anthropology, Non-European Cultures, Jewish Studies and Religious Studies
- 107 Theology

text_plus_domains:
- Collections

authors:
- Hartmut Leppin
- Philip Michael Forness (Goethe-Universität Frankfurt)

---




#### Motivation


The fields of classics, (101-02), history (101-03, 102), historical linguistics (104-03), Middle Eastern studies (106-04), and theology (107) have all witnessed a growing interest in a dialect of Aramaic, known as Syriac, in recent years. Several digital humanities projects exist in Syriac studies, such as *[Syriaca.org: The Syriac Reference Portal](https://syriaca.org/)*, the *[Digital Syriac Corpus](https://syriaccorpus.org/)*, and *[Simtho: The Syriac Thesaurus](https://bethmardutho.org/simtho/)*. Such projects form a backbone for Syriac studies online, and specialists make regular use of each of these tools. But only a few digital humanities projects include Syriac texts within a multi-lingual environment so that they can be compared with texts written in the same time and place but in languages such as Arabic, Greek, and Latin.  

#### Objectives


We have recently proposed a long-term project that will result in a new edition and translation of and a commentary on one of the most important sources for the history of the Eastern Roman Empire and the Near East in the late sixth century. The *Ecclesiastical History*of John of Ephesus offers a distinct perspective on this time, as it was written in the non-imperial language of Syriac by a bishop who did not belong to the imperial church. Indeed, the author was imprisoned for much of the time period that he wrote the third part of this work, which covers the years 571 to 588/589. Numerous fields make use of John of Ephesus’s *Ecclesiastical History* for the history of the later Roman Empire, the history of the Arab tribes on the eve of Islam, and even the history of Nubia (pre-modern southern Egypt and the Sudan). The decision about where to publish an open access and digital version of the edition, translation, and commentary is of critical importance for its visibility and its resultant use in diverse fields. 

We found the necessary infrastructure for our project in the *Patristic Text Archive* ( [https://pta.bbaw.de/](https://pta.bbaw.de/)) being developed by the Berlin-Brandenburgische Akademie der Wissenschaften. This archive seeks to make available both older editions of works found in the print series Die Griechischen Christlichen Schriftsteller as well as born digital editions, such as that being developed for our project. Despite its roots in a Greek-language series, the archive has been designed as a deliberately multi-lingual environment and aims to include Latin texts as well as so-called eastern Christian languages such a Syriac. By publishing our edition, translation, and commentary in this multi-lingual archive, we will place this Syriac historiographical work alongside the major Greek ecclesiastical histories of the fourth and fifth centuries for the first time in a digital environment. This will enable searches across linguistic boundaries that reveal unexpected connections between these texts. While we will publish this text in several other settings with a more restricted audience of specialists in Syriac, the *Patristic Text Archive* with its multi-lingual environment allows us to bring this text into broader conversations. 

The BBAW’s support for the *Patristic Text Archive* facilitates the development of long-term projects by providing infrastructure. For a small field like Syriac studies, digital projects may find a very limited user base and eventually no longer be supported. For this reason, digital initiatives that group texts differently—by time, region, space, theme, etc.—provide an important service to lesser studied languages and cultures. They offer sustainability by preserving the research in a broadly conceived archive and, just as importantly, an environment in which non-specialists may encounter the text in the normal course of their research. 

We hope that applicant institutions for the Text+ consortium, such as the BBAW, will continue to think in broad terms to provide the necessary resources for digital humanities projects that cross linguistic boundaries and help small fields of study find a home for their projects. 

#### Solutions 


One of the challenges facing multi-lingual projects is support for languages either with a bound script or that are not written left-to-right. In the specific case of the right-to-left language Syriac, users frequently encounter problems when switching between operating systems or even different versions of the same word processing software: letters become unbound, diacritical marks fail to appear over the correct letters, words are written backwards, etc. The cause of many of these problems is the use of Unicode Characters developed for left-to-right languages, such as the colon (U+003A), combining diaresis (U+0308), or the full stop (U+002E). What is needed is a solution that works across different operating systems, software, and web browsers. The Text+ consortium can help by advocating for a unified approach that works for less common languages affected by such problems. This will facilitate the integration of texts from small fields of study into multi-lingual archives where they may be incorporated into broader studies. 

