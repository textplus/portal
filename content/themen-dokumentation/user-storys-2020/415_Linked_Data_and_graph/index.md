---
title: "Linked Data and graph technologies in textual scholarship"

type: user-story

slug: user-story-415

aliases:
- /en/research-data/user-story-415

dfg_areas:
- 102 Geschichtswissenschaften
- 103 Kunst-, Musik-, Theater- und Medienwissenschaften
- 105 Literaturwissenschaft
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft
- 107 Theologie
- 108 Philosophie

text_plus_domains:
- Editionen

authors:
- Aline Deicke
- Andreas Kuczera (Academy of Sciences and Literature, Mainz)

---




#### Motivation


This User Story is linked to the research data submission “The Socinian Correspondence” and „The Book of Letters of Hildegard of Bingen“ 

Today’s digital scholarly editions mostly use the TEI-standard in combination with XML as it is a “well-documented format for archival long-term preservation which covers a large number of textual phenomena in general ways”. But research data in general and the data of digital critical digital scholarly editions specifically are highly connected and are not easy to model within the hierarchy inherent in XML. Cummings (2018) even states that “[the] difficulty [XML has] with overlapping hierarchies is not, in itself, strictly a myth”. This challenge becomes even more complex if we include multiple perspectives of different researchers on the transcription and edition of the text. For these reasons, we need new network- and graph-based approaches for modelling text and the contextual information around it. 

#### Objectives 


Text+’s technic stacks should be open for new technological approaches. Text-as-Graph (TAG) models are more flexible concerning connectability to other (Linked Open Data) resources. Storing texts and other contextual information like layout or semantic annotations, they ensure that all data remains addressable even when storing contradicting information.  

#### Solution 


In Text+ it should be possible to store textual and related informations in a provenance knowledge graph. 

#### Challenges 


Rather than being a risk, using graph technologies could be an important step forward for the digital humanities. 

#### Reviewbycommunity 


The further development can be accompanied by the  [AG Graphentechnologien](https://dig-hum.de/ag-graphentechnologien) and their members to get into a feedback loop. 

