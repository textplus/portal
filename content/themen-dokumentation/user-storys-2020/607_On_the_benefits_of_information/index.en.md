---
title: "On the benefits of information infrastructures for small humanities disciplines"

type: user-story

slug: user-story-607

aliases:
- /en/research-data/user-story-607

dfg_areas:
- 101 Ancient Cultures

text_plus_domains:
- Editions

authors:
- Germaine Götzelmann
- Danah Tonne
- Rainer Stotzka (Karlsruher Institute of Technology)
- in collaboration with Jochem Kahl (FU Berlin)

---




#### Motivation


Creation of information infrastructure for data modelling, data storage and data analysis in small and traditionally working subjects like Egyptology ( [DFG Subject Area 101-05](https://www.dfg.de/en/dfg_profile/statutory_bodies/review_boards/subject_areas/index.jsp)) in collaboration with the DFG Collaborative Research Centre as a ‘pilot project’. It has been shown that new impulses for Egyptological work can arise from novel visualization and analysis possibilities of early information technology and knowledge creation in pre-modern cultures. However, it has also been shown that especially in projects of these ‘small subjects’ an enormous additional workload is created on the part of the specialist science when tools are to be designed and developed jointly.

#### Objectives


The aim of the joint project is to evaluate the potential of digital methods in the field of traditional sciences and to initiate the sustainable operation of digital services and tools.

The project focuses on ancient Egyptian Pyramid and Coffin Texts, which must first be transcribed as full-text transcriptions before they can be stored in the digital infrastructure and used for data analysis and visualization. The transcription data of several Coffin Texts of a sequence of spells in their manifestations on the various surviving text witnesses should make it possible to provide existing qualitative text-critical methods with quantitative procedures for the creation of collations and *stemma codicum*. This is particularly challenging because of the short and very heterogeneous variants of the spoken text as well as the strong fragmentation and partly unclear cause (omission as text variance, omission due to text loss). The transcription data are hardly accessible to the usual tools for natural language processing, which makes their technical preparation more difficult and requires a deeper understanding of the text data on hand.

#### Solutions


In the course of communicating the questions and research data, an extensive collection of existing Stemma information was created, which provides an overview of the ancient Egyptian text tradition of Pyramid Text and Coffin Text spells. The interactive visualization of these stemmata in a spatio-temporal dimension has proven to be an extremely valuable tool for communication between the departments. The  [DARIAH-DE Geo-Browser](https://geobrowser.de.dariah.eu/) and its underlying  [PLATIN ](http://platin.mpiwg-berlin.mpg.de/)framework was used. Especially the additional widgets for line overlay and pie charts could be used for facetted visualization of text transfers and their metadata. Furthermore, adaptations and additions were necessary (e.g. an interactive adaptation in case of several possible locations of a text witness to visualize spatial uncertainties). Thus, the DARIAH-DE Geo-Browser can be used as a generic tool, e.g. for a quick understanding of metadata sets, but can also serve as a starting point for prototyping to deepen the knowledge based on epistemic centers in the text transfer of Pyramid Text and Coffin Text spells.

 ![Visualization DARIAH-DE Geo-Browser](Goetzelmann-1024x570.png) *Figure 1: Interactive visualization of text transfers using the extended DARIAH-DE Geo-Browser.*

Within the framework of joint pilot-like project work, all necessary work steps can be carried out in close coordination between specialist science and development with small-scale technical support. However, it must also be foreseeable, especially for the technical science side, that a sustainable continued operation of services and infrastructures beyond project boundaries can be guaranteed. Only under these circumstances can the additional burden be legitimized as valuable. This continued operation must also be accompanied by training and the above-mentioned technical support in order to remain attractive for the domain experts. If these components are guaranteed, then the ‘small subjects’ mentioned above offer immense potential for both sides to expand the scientific boundaries with digital methods and tools and to take a new look at supposedly familiar subjects.

