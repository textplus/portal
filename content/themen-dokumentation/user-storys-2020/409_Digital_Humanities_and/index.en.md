---
title: "Digital Humanities and Medieval Literary Studies"

type: user-story

slug: user-story-409

aliases:
- /en/research-data/user-story-409

dfg_areas:
- 105 Literary Studies

text_plus_domains:
- Editions

authors:
- Gabriel Viehhauser (Universität Stuttgart)

---




#### Motivation


As a digital humanities scholar with a background in Medieval German Literature (DFG 105-01), I am currently receiving an increasing number of requests from ‘traditional’ literary scholars who are creating editions, which are mostly published in book form, but also – not least because of the DFG requirements – are to contain a digital component. Most of these colleagues have little or no experience in the digital field, and their time and resources are usually very limited. Therefore there is usually a need for: 1. **best practices**: The colleagues would like to know where they can obtain information about the professional production of digital editions or transcriptions or would like to have cooperation partners who can provide advice or training for the digital component (even if no project funding is available). Often transcriptions have been quickly created in Word as a by-product of editions; out of the awareness that it would be useful to make this work available to posterity, there is a desire to enrich these transcriptions or archive them. In addition, medieval editions have special requirements with regard to certain individual phenomena (e.g. handwritten corrections), for which TEI has a broader scope; here, even colleagues who are already somewhat familiar with digital work have a need for exchange or orientation on uniform solutions. Finally, there is also a need for the professional presentation of the digital component in project applications. 2. **Archiving** **possibilities** for source data, especially digital transcriptions, sometimes even if there is no time, competence, and resources available for a professional award. 3. For scholars who are especially interested in further analog text work, the **interface** of a digital edition plays a major role; the abstraction of a data layer from its presentation is not pragmatic enough. The colleagues therefore not only want archiving possibilities for primary data, but also the functionality of the interface to be preserved as long as possible, since it represents the ‘essence’ of the edition from the outside.  

#### Objectives 


The most important (and easiest to deal with) of the points mentioned under 1 would be a **low-threshold possibility for the (relatively) fast but sustainable archiving of primary data**, which were partly created in projects where resources for processing are scarce. TextGrid or DTA could be possible models but are sometimes perceived in the community as not sufficiently oriented towards medieval studies, or there is too little awareness that these possibilities exist, or there is fear of the effort of preparing the texts. 

The low-threshold presentation and communication of **best practices** could also be provided by Text+. A balance between the mostly very subject-specific wishes and general models would be desirable: editors should have the feeling that they know how to deal with their own questions that relate to medieval texts. A general TEI course often does not address one’s own problems. 

The initiation of cooperation possibilities could be promoted via exchange platforms, but of course depends on the respective capacities of the partners.  

The technically most difficult point is certainly the preservation of the functionality of **interfaces**. Nevertheless, it seems to me that this point is of great importance for researchers who do not work digitally, which is why the topic should not be ignored despite the difficulties in order to increase the acceptance of digital methods in traditional communities. It may also be possible to counteract this problem by making more template-like standard solutions available, but these should also be low-threshold and convincing in their implementation (perhaps worked out together with ‘analog’ scholars?). 

Finally, the interdisciplinary linking of resources seems to me to be of importance, since there is also too little knowledge about which digital sources are already available that can be linked to one’s own edition.  

#### ReviewbyCommunity 


The most important feedback would be about the acceptance of the solutions in the traditional professional community. As the interface between this professional community and DH, I would be happy to verify the services offered by Text+ during the eligibility period. In addition, such a review could be promoted within the framework of the Association of Mediaevalists or the AG Digitale Mediävistik within the Association.  

