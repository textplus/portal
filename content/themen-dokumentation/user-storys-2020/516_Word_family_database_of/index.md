---
title: "Word family database of historical German"

type: user-story

slug: user-story-516

aliases:
- /en/research-data/user-story-516

dfg_areas:
- 104 Sprachwissenschaften

text_plus_domains:
- Lexikalische Ressourcen

authors:
- Jost Gippert (Universität Frankfurt/M.)
- Sarah Ihden (Universität Hamburg)
- Ralf Plate (AdW Mainz/Universität Frankfurt)
- Ingrid Schröder (Universität Hamburg)

---




#### Motivation


Despite recent work on historical linguistic periods and parts of speech, little is known about the lexical structure of the historical vocabulary of German. Specifically, studies on the number, extent and structure of word families as well as their diachronic development and spatial distribution (High German, Low German) are lacking.  

In order to be able to describe word families and their structures, we plan to establish an online word family database for historical High and Low German from the beginnings to the 17th century. The word family database will contain all lemmas of the dictionaries for these varieties and linguistic periods of German. The lemmas will be linked via the identified core words or core word stems both to the online representations of these dictionaries and to the reference corpora on Old German (Old Low German and Old High German), Middle Low German, Middle High German and Early New High German. The methodological project for preparing an eHumanities Centre for Historical Lexicography (ZHistLex) is an important preliminary work.

#### Objectives 


Linking the word family database to involved digital resources such as language corpora and dictionaries is to be made sustainable and expandable.

#### Solutions 


Text+ could help linking the different resources in a sustainable way by providing an appropriate repository and by supporting the homogenization of the data formats of the different resources, especially the metadata. Text+ could create facilities of expanding the database with further levels (New High German, New Low German). Finally, Text+ could bundle and provide expertise for the development of tools and thus support the research process.

