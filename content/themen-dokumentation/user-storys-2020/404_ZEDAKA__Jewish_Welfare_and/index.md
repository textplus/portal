---
title: "ZEDAKA – Jewish Welfare and Social Policy"

type: user-story

slug: user-story-404

aliases:
- /en/research-data/user-story-404

dfg_areas:
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft
- 102 Geschichtswissenschaften

text_plus_domains:
- Editionen
- Lexikalische Ressourcen

authors:
- Harald Lordick (Steinheim-Institut)
- Beate Lehmann (TU Braunschweig)
- Arbeitskreis Jüdische Wohlfahrt

---




#### Motivation


The field of Jewish welfare and social policy is underrepresented in public perception, in research and also in the available information infrastructures. The Arbeitskreis Jüdische Wohlfahrt (AKJW), which was founded in 2003 and has about 80 members from university and non-university research institutions as well as practitioners from social institutions, is dedicated to researching this area (in a mainly historical perspective). Several conferences have been held and publications have been issued. 

Supported by third-party funding, a digital platform project was launched in August 2020. »ZEDAKA – Jewish Welfare and Social Policy« is designed as a digital reference work, as a Research Encyclopedia. At the same time, the compendium, supported by the Arbeitskreis Jüdische Wohlfahrt, is conceived as a long-term and dynamic research and community platform. The systematic articles (topics, biographies, organizations, institutions) are supplemented by a collaborative subject bibliography and key documents published and shared as digital editions. 

Involved subject areas and research perspectives: interdisciplinary, according to DFG subject systematics, among others: Religious Studies and Jewish Studies (106-05) and Modern and Contemporary History (102-03). 

#### Objectives


Project activities include the exploitation of extensive digital corpora (especially newspapers and magazines) and the preparation of texts as digital editions. 

The reference work is to be linked intensively and widely, both internally and with other external digital sources and information services. For this reason in particular, the project aims to achieve the most complete coverage of its contents in the Integrated Authority File (GND) through active participation therein. Project members (of STI) are authorized to edit and create new GND entries (limited, within the framework of GND4C). 

In addition to this linkage, which is based as much as possible on standards like GND (and also Wikidata), the project also seeks to connect to existing dictionary networks. The platfom offers domain-specific knowledge to numerous Hebrew terms that have become part of more general usage, such as “Alija”, “Hachschara”, “Zedaka”. The same applies to German terms with a clear domain-specific context such as “Kindertransport”, “Berufsumschichtung”, “Zentralwohlfahrtsstelle”, “Wiedergutmachung”. 

#### Solutions


The project uses Mediawiki in combination with Wikibase as a platform technology suitable for Linked Open Data applications. The data is made available under the licenses: CC-BY / CC-0. 

We hope to receive support from Text+ in the following areas: 
* Networking with other lexical resources 
* Exchange and consultation regarding active participation in the Integrated Authority File (e.g. ontology, further opening to science) 
* Named-Entity Recognition / Entity Linking in text collections, based on the domain-specific data and controlled vocabularies stored in the project 
* Digital-methodical support for the machine-based exploitation of comprehensive newspaper corpora 
* Support and individual adaptation of OCR workflows for selected digitized items with regard to their original appearance and the scan quality of the digital copy (to improve quality of text recognition) 
* Expertise (technical, legal etc.) regarding digital editions 
* Recommendation and supply of a suitable repository for key documents on Jewish welfare and social policy edited by the project (TEI/XML)

