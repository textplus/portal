---
title: "Onomastics: Research on Jewish names"

type: user-story

slug: user-story-507

aliases:
- /en/research-data/user-story-507

dfg_areas:
- 102 Geschichtswissenschaften
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft

text_plus_domains:
-  Lexikalische Ressourcen

authors:
- Nathanja Hüttenmeister
- Anna Martin (Salomon Ludwig Steinheim Institute, Essen)
- Ortal-Paz Saar (Universiteit Utrecht)

---




#### Motivation


Onomastics, the study of the origin, meaning, distribution and development of – in our case – Jewish names, is an exciting topic with great epistemological value for, among other things, Jewish history in Germany since the Middle Ages. The origin, distribution and development of Jewish proper names, of proper names adopted from their surroundings as well as of surnames and nicknames, give insight in a wide variety of research areas, for example, migration movements and language acquisition, religious attitudes and acculturation tendencies, and last but not least genealogical and family history issues.

The current standard work for proper names by Alexander Beider (A Dictionary of Ashkenazic Given Names: Their Origins, Structure, Pronunciation, and Migrations, Avotaynu, Incorporated, 2001) is based largely on printed documentation of Jewish cemeteries. For surnames the work “A Dictionary of German-Jewish Surnames” by Lars Menk (Avotaynu, Incorporated, 2005) is fundamental.

With the data available in epidat ( [Database of Hebrew gravestone inscriptions, Steinheim-Institute](http://www.steinheim-institut.de/wiki/index.php/Epidat)) and the closely cooperating  [PEACE Portal](https://peace.sites.uu.nl/), the amount of data available for onomastic evaluation is now expanded many times over. The partner databases all employ EpiDoc standard (TEI/XML). 

#### Objectives


The aim is the onomastic investigation of the digitally edited Hebrew (and German) inscriptions. One of the research questions relates to trends in naming over the centuries and in different geographical regions.

Epidat currently contains 218 digital editions of Jewish cemeteries with almost 39,000 gravestones. They cover the period from the 11th century to the present day, the geographical focus is on present-day Germany, but also includes neighbouring areas (e.g. Netherlands, Czech Republic). Additional data is available through the PEACE Portal: Over 800 Jewish epitaphs, dating to the 2nd – 11th century CE, from Rome and Southern Italy (Funerary Inscriptions of Jews from Italy / Utrecht University) and Inscriptions of Israel/Palestine (Brown University, USA). Each of these records contains information relevant for an onomastic analysis, which is automatically linked to a place (cemetery) and a date (date of death).

A Jewish name, as it is also given in the grave inscriptions, is composed of a first name and a patronymic, for women often supplemented or replaced by the name of the husband. Men bore a so-called synagogal name, the name given to a boy during circumcision and with which he is called to tora-reading in the service. This can consist of one, sometimes two or three mostly biblical or Hebrew names, often supplemented by a call name often borrowed from German. Women, on the other hand, in addition to a few Biblical names often bore names of German, Romanic or Slavic origin borrowed from their surroundings. Male descendants of the ancient priestly dynasties of the Levites and Kohanim are given an ancestral byname. Established surnames were usually only adopted with the corresponding legal decrees in the first half of the 19th century, but surnames based on origin or a (Frankfurt) house sign were widespread. Professions, especially religious offices and the medical profession, could also become inherited surnames.

In addition to these names given in the Hebrew inscriptions, civic first names and surnames appeared in the epitaphs in the course of the 19th century with the spread of (also) German texts. Often – but by far not always – the civic first names were directly related to the traditional Jewish names (by the same or similar sound or by meaning), while in the choice of civic surnames, apart from the origin from the priestly dynasties and patronymic names that were made into surnames, regional preferences played an important role.

Such names are partly contained in the existing collections of name research. We want to improve the understanding of these names by commenting on their Jewish background and tradition. Furthermore, we want to make this expertise available as a lexical resource.

#### Solution


Based on the data available, the idea is to use tools, infrastructure and especially the Know-how of Text+ to achieve our research goals. We want to identify and map spelling variants as well as different traditions in translation of names. There are traditional (sometimes different) correspondences of Hebrew names and their German, English and Italian forms. In the end, we aim to create a linked open database with normalized name forms (including variants) and the additional statements gender, date, place as a basis for further onomastic research.

There is a close connection to the Text+ proposal LR-P-M04 portfolio: Digital Onomastics Description as well as to the data set epidat brought into Text+. A networking is also possible with the existing infrastructure of  [https://www.namenforschung.net/](https://www.namenforschung.net/). 

#### Challenges


Do established text tools work for our specific research questions and data? We want to test this in detail.

There are also a number of other database projects on Jewish cemeteries, especially in Eastern Europe, which could be included in the longer term, e.g. the  [Ashkenazi Inscriptions Database](http://ashkenaziinscriptions.eu/), the Foundation for  [Documentation of Jewish Cemeteries in Poland](https://cemetery.jewish.org.pl/lang_en/) or  [Jewish Galicia & Bukovina](https://jgaliciabukovina.net/). We are in scientific contact with researchers of these projects which makes access to the data relevant for us likely. This strengthens the need for a multilingual solution.

