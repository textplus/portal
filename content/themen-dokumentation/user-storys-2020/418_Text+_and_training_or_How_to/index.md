---
title: "Text+ and training, or How to improve historians’ skills in digital editing"

type: user-story

slug: user-story-418

aliases:
- /en/research-data/user-story-418

dfg_areas:
- 102 Geschichtswissenschaften

text_plus_domains:
- Editionen

authors:
- Annelie Große, BBAW (Akademienvorhaben “Anpassungsstrategien der späten mitteleuropäischen Monarchie am preußischen Beispiel 1786-1918”)

---




#### Motivation


Editions of archival material are indispensable resources and working tools for historical research. In 19^th^-century history, archival material exists abundantly. Ideally, historical editions thus facilitate the historian’s work by making material scattered in different archives accessible. Additionally, they provide a first rudimentary contextualisation of the material via the critical apparatus that contains various types of metadata like archival context, content-related notes, indexes of persons, places, institutions, etc. In recent years funding authorities are increasingly concerned with the digital presentation, usability and storage of historical editions. Moreover, they often require the researcher who establishes the edition to additionally provide analyses of the edited material, preferably by drawing on the newest digital methods and tools. As a consequence, the number of multi-faceted digital historical editions is growing in Germany, each of them putting a lot of effort in processes of conceptualisation and implication of their edition and using different tools and data base systems.  

#### Objectives 


As a researcher I am working in a large-scale edition project of 19^th^-century Prussian sources hosted at the BBAW. Following a cultural historical approach, our edition consists of a heterogeneous corpus that includes for example private and official correspondences, state files, minutes, budget charts etc. It shall be published in print and online; additionally, a significant part of the collected material shall be processed via alternative analytical and presentational tools. These latter include simple tools such as structured and searchable biographical data as well as more complex ones like a cartographic representation of a dataset containing information about housing, biography and socio-economic situation of historical actors. Being a historian by training I am familiar with classical ways of analysing historical sources, yet the digital establishment, presentation and analysis of textual data is a new field to me. Moreover, in times of increasing demands to the visual and operational sophistication of digitally presented data from both funding authorities and users, I feel the need to be familiar with trends and methods in digital humanities. All these aspects require significant efforts in gaining expertise exceeding the historian’s usual tasks. Therefore, I would like Text+ to make tools for the creation of digital data accessible as well as to provide the necessary training in the use of these and other analytical tools.  

At my research institution we luckily can draw on the expertise of a digital humanities department called TELOTA. Our colleagues at TELOTA not only create specific TEI-XML-based tools for our needs concerning data presentation, they also train us in using these tools and repair dysfunctionalities. Being responsible for several different projects within the BBAW TELOTA can be considered as a local knowledge hub in digital editions. Nevertheless, for the single researcher or research team it is not always easy to receive the specialised support it needs, too huge and too various are the responsibilities of TELOTA. Working processes on both sides could become more efficient if we as historians were better trained in TEI-XML. For example, in our project we draw on the excellent XML-editor-surface *ediarum* that facilitates the work of non-experts because it is similar to a *Word* surface. However, without any knowledge of TEI-XML, documents can easily become erroneous; often already tiny mistakes require the support of the experts although they could be solved by us historians if we were more familiar with TEI-XML. Moreover, a fruitful collaboration between digital humanities scholars, IT-specialists and historians requires a shared vocabulary which, in my view, is difficult to acquire for historians in particular. It is desirable that NFDI Text+ supports the development of a certain literacy in digital editing among historians and other researchers that deal with editions. It is especially necessary to promote a shift in thinking from the analogous to the digital paradigm. All too often I realise that I try to apply my experiences with print editions to digital ones which can lead to distortions in the way I process my data. 

#### Solution 


Therefore, I would suggest that NFDI Text+ establishes a hub for training and consulting in digital editing that covers four central aspects: First, it should contain a repository that assembles expertise, software tools and examples of existing digital editions. Second, it should offer regular training programs for senior scholars or scholars not originally trained in digital humanities. Training should be free of charge since most edition projects do not possess special funding for these needs although they are required to provide digital data. Third, NFDI Text+ should establish quality standards for digital editions that can serve as guidelines for emerging projects. Fourth, it should provide a network through which researchers can exchange their experiences.   

#### Challenges 


I see two major problems regarding my suggestions. First, since the field of digital humanities is developing rapidly it will be difficult to make sure training in digital editing remains up-to-date. Second, there is a risk that the suggested establishment of a selection of recommended software tools and certain norms of practice impedes the creativity and capacity of innovation that drives the progress in digital humanities. These potential challenges require an enormous flexibility and adaptability of the structures that Text+ will develop. 

