---
title: "Corpus of Low German plays of the 15th to 18th century"

type: user-story

slug: user-story-353

aliases:
- /en/research-data/user-story-353

dfg_areas:
- 104 Linguistics

text_plus_domains:
- Collections

authors:
- Michael Elmentaler (University of Kiel)

---




#### Motivation


Since the entire development of language before the invention of the phonograph (1877) is only indirectly accessible to us via written language testimonies or meta-linguistic language testimonies, the reconstruction of the spoken language of previous language stages represents a central challenge for the historical linguistics of all linguistic disciplines (cf. e.g. “Historische Mündlichkeit” by Eggert & Kilian 2016). This applies not only to the pronunciation and grammar of historical varieties, but also to the question concerning mutual interactions of people 300 or 400 years ago, as well as the speech acts and grammatical means deployed in various contexts. The analysis of dialogues of dramatic texts offers various insights into historical spoken language, whereby comedies, whose characters have an intentionally everyday language style, offer particularly good starting points. A conference on “Bauernkomödien als sprachhistorische Quellen”, organized by Dr. Markus Denkler (Münster) and me in the fall of 2019, highlighted the tremendous possibilities these texts offer for the reconstruction of the peculiarities of spoken language of older periods. The performative dialogicity allows for the analysis of conversational processes, patterns of language action and other pragmalinguistic, but also sociolinguistic and grammatical features characteristic of everyday communication. The dialogues are often crude and drastic and contain linguistic activities with curses, insults or accusations that are otherwise rarely recorded in written sources. 

In order to conduct computational analyses of historical plays using corpus-linguistic methods, the corresponding texts must be available in digital form as text files. Regarding High German plays, such resources are now available in greater numbers. However, the Low German acting tradition has hardly been recorded so far. This will be the starting point of my project. 

**Objectives** 

With already existing text editions as a starting point – ranging from the collections of the 19th century of Johannes Bolte and Wilhelm Seelmann (“Niederdeutsche Schauspiele der älteren Zeit”, 1895) and Hermann Jellinghaus (“Niederdeutsche Bauernkomödien des siebzehnten Jahrhunderts”, 1880) to the more recent editions of “Teweschen Hochtiet” (Münster 2018) and “Tewesken Kindelbehr” (Münster 2019) – we would like to develop a comprehensive corpus of Low German plays from the 15th to the 18th century, starting from early rhymed texts of the 16th century, which are still strongly in the “Fastnachtsspiel” (carnival play) tradition (e.g. the “Henselyn” from ca. 1498, the “Röbeler Fragment” and the play of the “bösen Frauen” from the 16th century), through the likewise still rhymed peasant dramas and interludes of the early 17th century (e.g. “Scriba” from 1616, “Hanenreyerey” from 1618), to the later prose comedies (e.g. Scher’s “Komödie vom Schafe-Dieb” from 1638, “Teweschen Hochtiet” from 1640, “Tewesken Kindelbehr” from 1642, “”Slennerhincke”” from 1687) and the texts from the 18th century with which this tradition gradually faded away (e.g. Ludvig Holberg’s “De Kannengehter” from 1743). This should also include plays in which only individual characters speak Low German, such as the comic interludes in the dramas of Johann Rist or the Low German scenes in the dramas of Heinrich Julius von Braunschweig (1592–94), in Gabriel Rollenhagen’s “Amantes Amentes” (1609), in the Hamburg carnival play “Der Tischeler Gesellen” (1696) and the school plays of Johann Samuel Müller (1738), in Louise Gottsched’s comedy “Die Pietisterey im Fischbein-Rocke” (1736) or Konrad Ekhof’s “Blindekuhspiel” (1760). A good overview of this older Lower German acting tradition is provided by the comprehensive volume “Das niederdeutsche Drama von den Anfängen bis zur Franzosenzeit” by Karl Theodor Gaedertz (2nd ed. 1894). In addition to the plays mentioned there, further texts on systematic corpus research in the “Niederdeutsche Bibiliographie” by Borchling/Claussen (1936–57), in “VD 16”, “VD 17” and “VD 18” ( [http://www.vd17.de/](http://www.vd17.de/) etc.) as well as in the online library catalogs will be researched and evaluated. 

**Solution** 

All texts found must be compiled according to the original prints (now mostly available as facsimiles) and encoded as computer-readable files. Within the framework of the program “EXMARaLDA” the files will be extensively annotated with regard to sociolinguistic (e.g., social speaker types), pragmalinguistic (e.g., speech acts, language games, communicative misunderstandings), grammatical (e.g., contractions, assimilations, dialectisms, syntactic peculiarities), and semantic criteria (e.g., scatological, sexual, or nosological fields of metaphor). This will allow, for the first time, for a diachronic analysis of the forms of historical orality reflected in these plays, differentiated according to speech situations or social groups. 

