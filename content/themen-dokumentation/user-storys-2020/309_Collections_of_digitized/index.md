---
title: "Collections of digitized historical periodicals – challenges and interoperability"

type: user-story

slug: user-story-309

aliases:
- /en/research-data/user-story-309

dfg_areas:
- 102 Geschichtswissenschaften
- 103 Kunst-, Musik-, Theater- und Medienwissenschaften
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft

text_plus_domains:
- Collections
- Infrastruktur/Betrieb

authors:
- Nanette Rißler-Pipka (SUB Göttingen)
- DHd-AG Zeitungen & Zeitschriften

---




#### Motivation


The mass digitization in libraries, which began in Germany around 2005, was one of the major prerequisites for building and working with collections of digitized periodicals. Yet people soon realized that digitization alone does not mean that the material is saved from vanishing. While the fragile material (paper) is going to vanish, the expectation that it could be saved through simple digitization is often disappointing.  In Germany the DFG funded several pilot projects (2013-2015) to evaluate the challenges regarding the digitization of historical periodicals. One outcome of this evaluation was a masterplan ( [https://www.zeitschriftendatenbank.de/fileadmin/user_upload/ZDB/z/Masterplan.pdf](https://www.zeitschriftendatenbank.de/fileadmin/user_upload/ZDB/z/Masterplan.pdf)) for the digitization workflow (2017). For researchers working with historical periodicals as well as for providers of data collections this plan ostensibly improved the situation. Particularly the recommendations regarding the quality of data and metadata as well as the accessibility via standardized interfaces seem to provide ideal conditions for working with the material. Still, looking at the research results produced in the field of disciplines working on digitized historical periodicals, it must be stated that currently only a rather small group of researchers is properly using the massive quantity of data and metadata. Whereas at least the majority of researchers in cultural, literary and media studies still use only few examples of well-known and canonical magazines to prove their hypotheses – this might be different in other disciplines and for newspapers.****

#### Objectives


We ask why this massive data and metadata amount is not used in the community of historical periodical researchers or from other disciplines using the data for information retrieval? There are several problems concerning how the data is provided to the community with particular regard to issues of their findability, accessibility, and reusability. First of all, the researchers need to find the data they are interested in: collections of digitized newspapers, journals, and magazines are often the product of the historically developed holdings of each library. Projects like the “Zeitschriftendatenbank” or the “Deutsche Digitale Bibliothek” are changing this towards a virtual aggregation of resources. This positive development could be improved though – not speaking about legal restrictions. Data and metadata of historical periodicals are not necessarily interoperable – even if they are findable and accessible. While there are metadata standards like METS/MODS/ALTO it is likely that they contain insufficient information with regard to the needs of the researchers. This problem is further exacerbated by the fact that researchers generally do not know how to analyse this kind of data or how to evaluate the data quality as this is outside of the scope of their domain knowledge. Here, an investment in data quality, training, and knowledge transfer is necessary.

#### Solution


The connection of data providers for digitized historical periodicals in Germany is very advanced but could additionally include international networks of libraries and other data providers regarding these particular resources (the networks already exist in general like LIBER, IFLA, etc.). The data should be simply accessible via “data shops” (see the example by DNB:  [https://portal.dnb.de/metadataShop.htm](https://portal.dnb.de/metadataShop.htm)) with an indication regarding licences and re-usability. It would also help to have one single point of access to the data and metadata. This is partly realized via the DDB portal ( [https://www.dnb.de/DE/Professionell/ProjekteKooperationen/Projekte/DDB-Zeitungsportal/DDB-Zeitungsportal_node.html](https://www.dnb.de/DE/Professionell/ProjekteKooperationen/Projekte/DDB-Zeitungsportal/DDB-Zeitungsportal_node.html)) on the German level and by Europeana-newspapers ( [https://www.europeana.eu/de/collections/topic/18-newspapers](https://www.europeana.eu/de/collections/topic/18-newspapers)) on the European level. These two resources only cover newspapers, though. As such, the inclusion of journals and magazines would be a desideratum. Further, the scope of these resources should also be extended to include and provide direct access to the data and metadata produced by individual research projects.
