---
title: "Distributed editing of 19th century correspondences"

type: user-story

slug: user-story-411

aliases:
- /en/research-data/user-story-411

dfg_areas:
- 102 History
- 105 Literary Studies

text_plus_domains:
- Editions

authors:
- Rotraut Fischer
- Mareike Bassenge
- Sabine Bartsch
- Luise Borek
- Dario Kampkasper
- Andrea Rapp (TU Darmstadt)

---




#### Motivation


As an independent researcher without a dedicated anchorage in a corresponding institution or research project, I have been working for many years on an edition of various correspondences about the Grimm brothers (besides Jacob and Wilhelm Grimm, Clemens Brentano, Johann Wilhelm Heinrich Conradi and Friedrich Creuzer are among the prominent letter writers), which were originally intended for a print publication. The possibilities of digital publications – especially the perspective of integrating original facsimiles, the expandability as well as the opening up of whole networks and constellations convinced me to create a digital edition. I created the texts in different versions of Word. Some of the correspondence has already been printed, some of it is still available as Word files, and some has yet to be retro-digitized. I have collected digital copies of the originals from various archives and libraries. In order to relieve the commentary, I now work with standard data (GND), which is especially useful considering the large number of people involved. Since the Brothers Grimm and their correspondence partners are of great interest for a number of disciplines, the cataloguing via the web service correspSearch is planned. 

I have found cooperation partners in the computer philology department of the TU Darmstadt and the University and State Library Darmstadt, who will advise and support the conversion of the edition to digital. The Grimm Society in Kassel has also agreed to convert its printed volumes into a digital edition. Together we are working on the digital transformation and development of the letter edition. 

#### Objectives


Due to the heterogeneous initial situation, the requirements are manifold, different workflows must be addressed in order to obtain a coherent result. For this I need technological and organizational support as well as a sustainable publication site. In addition to an appropriate presentation of the edition, access and use of the GND are key elements for me. My edition can provide valuable, sometimes new information, even for less frequently attested persons that I would like to feed into the GND. Standard workflows or tools for a conversion of Word data into TEI/XML data – even for more complex structured data – would be enormously helpful.  

#### Solution 


It would be helpful and motivating if I could also receive advice and technical support from the NFDI Text+ consortium as an independent researcher, e.g. referral to local or regional experts, institutions, mentors, training courses. The tools should be freely usable. I also consider an evaluation and quality assurance of my data desirable (e.g. by scientific advisory boards) – not only with regard to the technical, but also, if applicable, the content quality and the transfer to repositories to make the data, which is generated outside of large research networks, more visible and secure in the long term. Especially in the field of editions, there are numerous such initiatives by individual researchers, which is both desired and necessary due to the abundance and heterogeneity of the cultural heritage to be edited. 

#### Challenges 


The challenge is to assess whether independent researchers without institutional ties can and want to bring projects to a conclusion; the form of possible quality assurance also costs time and resources that are difficult to assess and usually difficult to raise. 

Moreover, in the case of cross-linked 19th century correspondence, an overlap of different edition projects is to be expected. The challenge here is not only to make each other findable, but also to establish interoperability. 

