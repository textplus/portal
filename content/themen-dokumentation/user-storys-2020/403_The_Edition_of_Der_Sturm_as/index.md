---
title: "The Edition of “Der Sturm” as example for digitized periodicals"

type: user-story

slug: user-story-403

aliases:
- /en/research-data/user-story-403

dfg_areas:
- 102 Geschichtswissenschaften
- 103 Kunst-, Musik-, Theater- und Medienwissenschaften
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft

text_plus_domains:
- Editionen
- Infrastruktur/Betrieb

authors:
- Marjam Trautmann (Akademie der Wissenschaften und der Literatur | Mainz)
- DHd-AG Zeitungen & Zeitschriften

---




#### Motivation


As a researcher working on the history of the international Avantgarde me and my team are creating a digital scholarly edition of textual (letters, newspapers, literary writings, etc.) as well as non-textual historical sources (images of paintings, sculptures, costumes from stage performances and music) of the art-company Der *Sturm*(sturm-edition.de). Our mixed team consists of historians, literary philologists and digital humanities specialists. A major challenge is the multimodality of our research objects and the diverse types of research data and according metadata to describe it. Our goal is not only to create cross-references within the *Sturm*corpus – for example, when a letter deals with a particular article from an edited journal Der *Sturm*– but also to establish interoperable links to objects and texts from other editions and repositories of normdata (like the GND and WikiData). At the same time, it must also be possible to link to art phenomenona beyond their textual representation. To achieve this we would need the possibility to embed or enhance TEI/XML with other existing standards like the CIDOC CRM. Furthermore, the collected and modelled sources should enable analyses with digital methods, which require standardized data and metadata acquisition.

#### Objectives


For the creation of such an interoperable edition of heterogeneous media, reusable, digitized material is needed. The digitised journal should be available in a clean OCR, have a permanent identifier (e.g. DOI) – preferably not only for the individual issue, but at article level and the licensing should be clearly clarified. The latter has an impact on whether the journal can be included in the corpus at all or whether parts of it may have to be excluded. Also helpful are metadata on the individual journal at article level available in leading metadata standards (e.g. METS/MODS). These requirements depend on the infrastructure and knowledge of the providers, which in turn need the resources to comply with the required standards. Particularly in projects in which sources from different institutions are collected and processed, a consistent collection of data and metadata in the selected standards is required to ensure interoperability.

To develop interoperable coding schemata according to TEI/XML for the edition, networking and exchange with other journal and newspaper projects – and beyond – is necessary. After all, newspapers and magazines in particular do not model pure text; layout, images and works of art (e.g. wood prints), advertising and musical pieces also play an important role. Best practices can only be developed and implemented cooperatively by problematizing modelling questions of text objects and the texts themselves.

#### Solution


Concerning the availability of text content in newspapers and magazines as OCR text, Text+ can bring together providers and research projects and give infrastructural support, but also serve as a reference platform for individual researchers. Text+ as a discussion forum can also deal with the uniform implementation of the citation of periodicals with regard to the bibliographic depth of indexing (e.g. DOIs at article level). The example of the multi-source *Sturm*project shows that the consistent implementation of common data and metadata standards among providers, also across genres, is of great importance for interoperable research questions. Text+ can support the providers in a coherent implementation of the standards. Together with the other NFDI-Initiatives for the Humanities, Text+ can work to coordinate and implement common standards to support projects like the *Sturm*. With particular regard to digitization, annotation standards and interoperable linking of the mentioned non-textual works of art and their related research data Text+ might develop joint services in cooperation with NFDI4Culture (https://doi.org/10.3897/rio.6.e57036). In return, the interoperable research data generated in the projects can be made available through the NFDI’s infrastructure to the research community.
