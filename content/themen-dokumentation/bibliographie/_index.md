---
title: Bibliographie

menu:
  main:
    weight: 90
    parent: themen-dokumentation

type: bibliographie

params:
  last_mod_today: true
---

# Bibliographie

Wie in Forschungsprojekten üblich sind die Beteiligten an Text+ aufgerufen, ihre Ergebnisse in geeigneter Form zu veröffentlichen. Eine vollständige Projektbibliographie findet sich in der [Text+ Zotero Community](https://www.zotero.org/groups/4533881/textplus/library) des Literaturverwaltungswerkzeugs Zotero. Dort können die entsprechenden Publikationen auch für eigene Zitationen in verschiedenen Bibliographieformaten heruntergeladen werden. 

Auf dieser Seite befindet sich eine erste Übersicht der Veröffentlichungen. Die Kategorisierung ist - wie bei entsprechenden Listen - nur eine von vielen Darstellungsformen. Die Publikationen wurden nach der [FRBR-aligned Bibliographic Ontology (FaBiO)](http://www.sparontologies.net/ontologies/fabio) verschiedenen [Typen](https://sparontologies.github.io/fabio/current/fabio.html) zugeordnet. 

{{<zotero-collection-list>}}
