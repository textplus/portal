---
title: "Reporting"
type: non-scrolling-toc
---

## Publikationen nach FaBiO und Jahr

Die Zahlen in der Tabelle verlinken auf Zotero (externe Webseite) und beschränken die Publikationen je nach zutreffender FaBiO Typisierung und/oder Publikationsjahr.

{{< standout theme="white">}}
  {{< zotero-type-year-table >}}
{{< /standout >}}
