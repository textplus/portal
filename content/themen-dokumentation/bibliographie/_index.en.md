---
title: Bibliography

menu:
  main:
    weight: 90
    parent: themen-dokumentation

type: bibliographie

params:
  last_mod_today: true
---

# Bibliography

As is customary in research projects, participants in Text+ are encouraged to publish their results in a suitable form. A complete project bibliography can be found in the [Text+ Zotero Community](https://www.zotero.org/groups/4533881/textplus/library) using the Zotero reference management tool. There, the corresponding publications can be downloaded for individual citation in various bibliography formats.

On this page, you will find an initial overview of the publications. The categorization, as with similar lists, is just one of many presentation formats. The publications have been assigned to different [types](https://sparontologies.github.io/fabio/current/fabio.html) based on the [FRBR-aligned Bibliographic Ontology (FaBiO)](http://www.sparontologies.net/ontologies/fabio).

{{<zotero-collection-list>}}
