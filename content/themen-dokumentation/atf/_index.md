---
title: Abgeleitete Textformate

menu:
  main:
    weight: 35
    parent: themen-dokumentation

---
# Abgeleitete Textformate

Während viele der Sprach- und Textressourcen aus den Text+ Zentren der
Wissenschaft frei zur Verfügung stehen, sind andere wegen rechtlicher
Einschränkungen nur bedingt für die Forschung nutzbar – insbesondere
Werke, die urheberrechtlich oder durch Daten- und
Persönlichkeitsrechte geschützt sind bzw. lizenzrechtlichen
Limitierungen unterstehen.

Text+ verfolgt das Ziel, auch derart geschützte Werke für die
Wissenschaft zugänglich und verwertbar zu machen. Eine Möglichkeit
dazu bieten Abgeleitete Textformate (ATF, vgl. [Schöch et al.,
2020](https://dx.doi.org/10.17175/2020_006)).

## Was sind ATFs?

ATFs entstehen durch die Reduktion von Informationsgehalt in Texten –
nach einer ersten Anreicherungsphase. Sie können so hergestellt
werden, dass einerseits das Ergebnis noch die Beantwortung mindestens
einer Forschungsfrage ermöglicht, der verbleibende Rest andererseits
aber z.B. die Rechte der Urheberrechtsinhaber nicht mehr
beeinträchtigt.

Voraussetzung dafür ist u.a., dass keine Möglichkeit zur
Rekonstruktion des Ausgangstextes besteht. Solche ATFs können daher
frei veröffentlicht werden. Bei der Erzeugung von mehr als einem ATF
von einem Dokument bzw. einem Korpus ist darauf zu achten, dass auch
durch deren Kombination keine Rekonstruktion möglich ist. So sind
nicht alle ATFs automatisch rechtefrei. Ist eine Rekonstruktion des
Originaltextes ohne größeren Aufwand möglich, fällt das ATF weiterhin
unter das Urheberrecht.

## Wie werden ATF erzeugt?

ATFs entstehen auf der Basis des Originaltextes durch die Anwendung
einer Reihe von Veränderungen. In einem ersten Schritt wir der Text
durch Annotationen angereichert (z.B. Part-of-Speech-Tagging (POS),
Verlinkungen von Named Entities zu Normdaten oder statistische
Analysen auf dem Originaltext). Danach erfolgt eine gezielte
Informationsreduktion. Diese basiert einerseits auf einer Reihe von
Veränderungen, die typischerweise automatisiert ablaufen und ganz
wesentlich auf der Entscheidung, welche Granularität diesen
Operationen zu Grunde liegen.

Für die Informationsreduktion stehen vier Operationen zur Verfügung:

* Löschen
* Behalten
* Ersetzen 
* Vertauschen

Diese können auf verschiedenen Granularitätsebenen greifen (z.B. auf
Token-, Satz- oder Absatzebene) und in Bezug auf unterschiedliche
Größen (z.B. pro Dokument, pro Werk oder pro Korpus).

Verbreitete Formen von ATFs sind z.B. Term-Dokument-Matrizen,
N-Gramme, Texte mit maskierten Tokens oder Wort-Embeddings.

## Aktueller Stand

Text+ arbeitet derzeit an einem Vorschlag für einen DIN-Standard zu
ATFs sowie an einer Veröffentlichung zu rechtlichen Aspekten dieser
Formate, um wissenschaftliche Communitys, die diese Daten nutzen, und
Institutionen, die solche Daten bereitstellen möchten, gleichermaßen
mit der nötigen Expertise zu unterstützen.

Zugleich treibt Text+ die Forschung in diesem Bereich voran. So sind
zuletzt mehrere Analysen veröffentlicht worden, z.B. zur Eignung
verschiedener ATFs für die Autorschaftsattribution und zum Feintuning
von Sprachmodellen mit ATFs (siehe "Weiterführende Links"):


## Weiterführende Links
Die folgenden Linklisten führen zu existierenden ATFs, mit denen bereits gearbeitet werden kann, sowie zu Forschungsergebnissen, die sich entweder mit den Eigenschaften verschiedener ATFs auseinandersetzen oder auf Basis der Nutzung von ATFs entstanden sind.

{{<accordion>}}
  {{<accordion-item title="Links zu existierenden ATFs">}}
  * Beispiel-ATFs für Publikation „Abgeleitete Textformate: Text und Data Mining mit urheberrechtlich geschützten Textbeständen“ (Schöch et al., 2020) sowie Programmcode zu deren Erzeugung:  [Link](https://github.com/dh-trier/tmr) 
  * HTRC Extracted Features (ATFs von über 17 Mio. Werken): [Link](https://htrc.atlassian.net/wiki/spaces/COM/pages/43295914/Extracted+Features+v.2.0)
  * Google N-Grams (n-Gramme eines Korpus‘ von ca. 3,5 Mio. englischsprachiger Bücher): [Link](http://commondatastorage.googleapis.com/books/syntactic-ngrams/index.html) 
  * Sammlung von ATFs amerikanischer Dramen mit Fokus auf Strukturmerkmalen: [Link](https://textgridrep.org/project/TGPR-adf9b705-1533-b0ef-491b-674878350ecb) 
  * ATFs der spanischsprachigen Romane aus dem Korpus CoNSSA (Corpus of Novels of the Spanish Silver Age): [Link](https://textgridrep.org/search?query=&order=relevance&limit=20&mode=list&filter=format:application%2Fxml%3Bderived%3Dtrue&filter=project.id%3ATGPR-8b44ca41-6fa1-9b49-67b7-6374d97e29eb)
  * Dokument-Term-Matrix (Bag of words) des Romans „Don Quijote de la Mancha“ von Miguel de Cervantes (spanischsprachig): [Link](http://corpus.rae.es/frecCORDE/quijote1.TXT) 
  {{</accordion-item>}}
  {{<accordion-item title="Links zu Forschungsergebnissen rund um ATFs">}}
  * Classification of Genres through 500 Years of Spanish Literature in CORDE (Calvo Tello, 2024) [Link](https://heiup.uni-heidelberg.de/catalog/book/1157/chapter/19362)
  * Shifting Sentiments? What happens to BERT-based Sentiment Classification when derived text formats are used for fine-tuning (Du and Schöch, 2024) [Link](https://dh24-abstracts.netlify.app/assets/du_keli_shifting_sentiments__what_happens_to_bert_based_sent)
  * InvBERT: Reconstructing Text from Contextualized Word Embeddings by inverting the BERT pipeline (Kugler et al., 2023) [Link](https://doi.org/10.48694/jcls.3572)
  * Understanding the impact of three derived text formats on authorship classification with Delta (Du, 2023) [Link](https://doi.org/10.5281/zenodo.7715299)
  * Volltext vs. abgeleitetes Textformat: Systematische Evaluation der Performanz von Topic Modeling bei unterschiedlichen Textformaten mit Python (Kocula, 2022) [Link](https://www.parsqube.de/publikationen/volltext-vs-abgeleitetes-textformat-systematische-evaluation-der-performanz-von-topic-modeling-bei-unterschiedlichen-textformaten-mit-python)
  * Zugang zu großen Textkorpora des 20. und 21. Jahrhunderts mit Hilfe abgeleiteter Textformate (Raue und Schöch, 2020) [Link](https://irdt.uni-trier.de/wp-content/uploads/2020/11/Raue-Schoech-RuZ-2020-118-127.pdf)
  * Masking Treebanks for the Free Distribution of Linguistic Resources and Other Applications (Rehm et al., 2007) [Link](https://dspace.ut.ee/server/api/core/bitstreams/5dce8c9a-52d1-4f91-9f2f-13fb12d3de43/content)
  * Corpus Masking: Legally Bypassing Licensing Restrictions for the Free Distribution of Text Collections (Rehm et al., 2007) [Link](https://ids-pub.bsz-bw.de/files/4514/Rehm_Witt_Zinsmeister_Corpus_Masking_Legally_Bypassing_Licensing_Restrictions_2007.pdf)
  {{</accordion-item>}}
{{</accordion>}}
<br>

## Beispiele

Rang | N-Gramm | Häufigkeit|
:--   | --      | --:      |
1 | gott sei dank | 43
2| ja gnädigste frau| 17|
3| auch heute wieder| 13|
4| doch auch wieder| 11|
5| ist doch auch| 11|
6| ist immer so| 10|
7| gnädigste frau ist| 10|
8| war so war| 10|
9| nein gnädigste frau| 9|
10| wird ja wohl| 9|
11| ist doch recht| 9|
12| doch immer noch| 9|

Häufigkeiten von 3-Grammen über mehrere Texte hinweg, bei einer
Mindesthäufigkeit von 5. Beispieldaten auf der Grundlage von fünf
Erzähltexten von Theodor Fontane. [Schöch et al. 2020](https://dx.doi.org/10.17175/2020_006)

``von_APPR_von Hohen-Cremmen_NN_Hohen-Cremmen Georg_NE_Georg zu_APPR_zu
heller_ADJA_hell des_ART_die fiel_VVFIN_fallen schon_ADV_schon
bewohnten_ADJA_bewohnt In_APPR_in der_ART_die <SEG>
Mittagsstille_ADJA_Mittagsstille Gartenseite_NN_Gartenseite und_KON_und
erst_ADV_erst Park-_TRUNC_Park- Dorfstraße_NN_Dorfstraße ,_PUN_,
Seitenflügel_NN_Seitenflügel breiten_ADJA_breit die_ART_die hin_ADV_hin
während_KOUS_während angebauter_ADJA_angebaut der_ART_die nach_APPR_nach
ein_ART_eine Schatten_NN_Schatten auf_APPR_auf einen_ART_eine
rechtwinklig_ADJD_rechtwinklig <SEG> großes_ADJA_groß ,_PUN_,
auf_APPR_auf mit_APPR_mit in_APPR_in ein_ART_eine weiß_ADJD_weiß und_KON_und
über_APPR_über quadrierten_ADJA_quadrierten und_KON_und diesen_PDAT_dies
auf_APPR_auf Mitte_NN_Mitte seiner_PPOSAT_sein dann_ADV_dann
Fliesengang_NN_Fliesengang hinaus_ADV_hinaus einen_ART_eine grün_ADJD_grün
<SEG>``

Ausschnitt aus der Liste der Tokens mit Annotation bei segmentweiser
Aufhebung der Sequenzinformation für den Beginn von Fontanes Effi
Briest. Hier auf Unigramm-Basis und mit Wortform, Lemma und
Wortart-Information sowie einer Segmentlänge von 20 Tokens. Man
beachte die Markierung der Segmentgrenzen mit ``<SEG>`` nach jeweils 20
Tokens. [Schöch et al. 2020](https://dx.doi.org/10.17175/2020_006)
