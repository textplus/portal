---
# just a dummy file to set the title and menu

title: "Themen und Dokumentation"

menu:
  main:
    identifier: "themen-dokumentation"
    weight: 20

build:
  render: never
---
