---
title: Call for Data 2020
type: no-toc
menu:
  main:
    weight: 60
    parent: themen-dokumentation
aliases:
- /forschungsdaten/data-from-the-community/

draft: true
---

# Call for Data 2020
{{<lead-text>}}As part of the preparation for the application for the second NFDI funding round, the former Text+ initiative launched an open call for data. The application was approved in July 2021, and since then, we have been working on integrating the data into the infrastructure of the NFDI consortium Text+.{{</lead-text>}}

We present a selection of already integrated datasets from the community under the Data and Services menu. You can still offer research data. Please contact our [Helpdesk](https://text-plus.org/helpdesk) for this purpose.

{{<button url="https://dhd-blog.org/?p=14097">}}Call for Data 2020{{</button>}}

## Results of the Data Call
Below, we present some of the data that were suggested by the community for integration into the Text+ infrastructure.

{{<data-call-results>}}
