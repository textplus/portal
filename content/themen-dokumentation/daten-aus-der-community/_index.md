---
title: Call for Data 2020
type: no-toc
menu:
  main:
    weight: 60
    parent: themen-dokumentation
aliases:
- /forschungsdaten/daten-aus-der-community/

draft: true
---

#  Call for Data 2020
{{<lead-text>}}Im Rahmen der Vorbereitung des Antrages für die zweite NFDI-Förderrunde hat die damalige Initiative Text+ einen offenen Call for Data gestartet. Der Antrag wurde im Juli 2021 bewilligt und seitdem arbeiten wir daran, die Daten in die Infrastruktur des NFDI-Konsortiums Text+ zu integrieren.{{</lead-text>}}

Eine Auswahl an bereits integrierten Datenbeständen aus der Community präsentieren wir unter dem Menüpunkt Daten und Dienste. Forschungsdaten können weiterhin angeboten werden. Kontaktieren Sie dafür bitte unseren [Helpdesk](/helpdesk).

{{<button url="https://dhd-blog.org/?p=14097">}}Call for Data 2020{{</button>}}

## Ergebnisse des Daten-Calls
Im Folgenden präsentieren wir einige der Daten, die aus der Community zur Integration in die Infrastruktur von Text+ vorgeschlagen wurden.

{{<data-call-results>}}
