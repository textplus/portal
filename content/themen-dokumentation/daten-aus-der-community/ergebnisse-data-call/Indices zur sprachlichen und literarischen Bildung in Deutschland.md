---
title: Indices zur sprachlichen und literarischen Bildung in Deutschland

institutions: 
- Dr. Uwe Grund, Hannover

languages:
- Deutsch

modalities:
- geschrieben

text_plus_domains: 
- Collections 

dfg_areas:
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 109 Erziehungswissenschaft und Bildungsforschung

descriptive_areas: 
- Germanistik
- Erziehungswissenschaft

type: community-data

---

Die fünfbändige Druckfassung der INDICES (München u.a.O.: Saur, 1991ff.) verzeichnet und erschließt ca. 10.000 Dokumente von rund 3.000 Verfassern in fünf führenden Fachzeitschriften und zwei paradigmatischen Amtsblättern. Die Daten beruhen auf der Autopsie von rund 100.000 Druckseiten. Die seriellen Quellen (von ca. 1910 bis ca. 1970) werden mehrdimensional beschrieben (Textgenre / Inhaltsschwerpunkt / Verfahren der Verfasser). Sowohl Extraktions- wie Annotationsverfahren (via Thesaurus) kommen zu Anwendung. Steuerung und Qualitätssicherung (einheitliche Erschließungstiefe, Verknüpfbarkeit und Sortierbarkeit der Datensätze nach chronologischen, alphabetischen und taxonomischen Kriterien) erfolgte über eigens erstellte Regelwerke zur Datenerfassung, -auswertung und -weiterverarbeitung (unpubliziert). Einzelne Dateien zu monographischen Quellen (z. B. Lesebücher, Sprachbücher) liegen in Rohfassung vor.