---
title: Soldatenbriefe des 18. und 19. Jahrhunderts

institutions: 
- Justus-Liebig-Universität Gießen
- Institut für Germanistik

languages:
- "Deutsch (differenziert in: Norddeutsch, Nordoberdeutsch, Ostmitteldeutsch, Ostoberdeutsch, Westmitteldeutsch, Westoberdeutsch)"

modalities:
- geschrieben

text_plus_domains: 
- Editionen 
- Collections

dfg_areas:
- 101-02 Klassische Philologie
- 102 Geschichtswissenschaften
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft
- 108 Philosophie

descriptive_areas: 
- Linguistik
- Literaturwissenschaft
- Kulturwissenschaft
- Editionsphilologie
- Geschichte 
- Militärgeschichte
- (Neue) Politische Geschichte
- (ggf., nach Aufbereitung der Forschungsdaten) Korpuslinguistik, Computerlinguistik

type: community-data

---

170 „Soldatenbriefe“ aus den Jahren 1745 bis 1872; die Briefe stammen zu einem kleinen Teil aus älteren (nicht mehr urheberrechtlich geschützten), aber zuverlässigen Editionen, vor allem aber aus eigener Archivarbeit, wurden also vom Herausgeber Marko Neumann erstmals nach der Handschrift transkribiert und veröffentlicht. Diese sind über die Webseite des Heidelberger Universitätsverlags Winter verfügbar und können von dort kostenfrei heruntergeladen werden. Dieses Korpus ist sowohl aus linguistischer als auch aus historischer, insbesondere kultur-, literatur- und militärgeschichtlicher Perspektive höchst wertvoll.

Zu den Hürden (aus rechtlicher Sicht) und Schwierigkeiten (mit Blick auf Publikationsform und v.a. das Datenformat) für die Nachnutzung dieser wertvollen Daten vgl. die zu diesem Datenangebot gehörige User Story ["Soldatenbriefe des 18. und 19. Jahrhunderts: Von der PDF-Edition zu nachnutzbaren, interoperablen Forschungsdaten".](/themen-dokumentation/user-storys/user-story-508/)