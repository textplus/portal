---
title: Dokumente und Materialien zur ostmitteleuropäischen Geschichte

institutions: 
- Herder-Institut für historische Ostmitteleuropaforschung – Institut der Leibniz-Gemeinschaft

languages:
- mehrsprachig

modalities:
- geschrieben

text_plus_domains: 
- Editionen 

dfg_areas:
- 102 Geschichtswissenschaften


descriptive_areas: 
- Geschichte Ostmitteleuropas
- Slawistik
- Geschichstdidaktik

type: community-data

---

Die digitale Edition bietet Themenmodule für die universitäre Lehre zur ostmitteleuropäischen Geschichte in ihrer zeitlichen Tiefe und räumlichen Breite an, so dass Themenmodule zur mittelalterlichen Geschichte ebenso angeboten werden wie zur Zeitgeschichte. Alle Textquellen, aber auch andere Materialien wie Statistiken werden zur Sicherung der Zitierfähigkeit in der jeweiligen Originalsprache, in deutscher Übersetzung und möglichst als Scan aus der Originalquelle angeboten, außerdem werden weitere Materialien wie Karten, Abbildungen, eine Auswahlbibliografie mit Literatur in westlichen Sprachen und eine Chronologie zur Orientierung angeboten. Sämtliche Module unterliegen einem Double-blind-peer-review-Verfahren. Das Angebot wird ständig erweitert, überarbeitet und derzeit wird ein englischsprachiges Angebot erarbeitet.

[Übersicht](https://www.herder-institut.de/no_cache/digitale-angebote/dokumente-und-materialien/themenmodule.html)
[Beschreibung](https://www.herder-institut.de/holdings/?hol=HI000023)


