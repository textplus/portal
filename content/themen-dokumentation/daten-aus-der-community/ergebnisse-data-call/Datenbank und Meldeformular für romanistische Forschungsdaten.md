---
title: Datenbank und Meldeformular für romanistische Forschungsdaten

institutions: 
- Fachinformationsdienst Romanistik
- romanistik.de e.V.

languages:
- Deutsch

text_plus_domains: 
- Software Services 

dfg_areas:
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft

descriptive_areas: 
- Romanistik
- Disziplinen, in denen Daten mit romanistischer Relevanz anfallen

type: community-data

---

Von romanistik.de, der AG Digitale Romanistik und dem FID Romanistik entwickeltes Meldeformular auf der romanistischen Kommunikationsplattform romanistik.de, das es erlaubt, auf eigene Forschungsdaten wie auf traditionelle Publikationen aufmerksam zu machen. Gemeldete Forschungsdaten sind dann über die Plattform auffindbar und werden auch über den romanistik.de-Newsletter beworben.

[Gemeldete Ressourcen](https://www.romanistik.de/res)
[Meldeformular](https://fid-romanistik.de//forschungsdaten/meldung-von-forschungsdaten/)