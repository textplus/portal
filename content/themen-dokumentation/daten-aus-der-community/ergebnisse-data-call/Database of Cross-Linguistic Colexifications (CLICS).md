---
title: Database of Cross-Linguistic Colexifications (CLICS)

institutions: 
- Max-Planck-Institut für Menschheitsgeschichte

languages:
- mehrsprachig

modalities:
- geschrieben
- transkribiert

text_plus_domains: 
- Lexikalische Ressourcen
- Software Services

dfg_areas:
- 104 Sprachwissenschaften
- 110 Psychologie
- 206 Neurowissenschaften

descriptive_areas: 
- historische Linguistik
- linguistische Typologie
- Psychologie
- Neurowissenschaften

type: community-data

---

Die ursprüngliche Datenbank für sprachübergreifende Colexifikationen (CLICS) hat ein computergestütztes Framework für die interaktive Darstellung sprachübergreifender Colexifikationsmuster eingerichtet. Es hat sich als nützliches Instrument für verschiedene Arten der Untersuchung sprachübergreifender semantischer Assoziationen erwiesen, angefangen von Studien zum semantischen Wandel über Muster der Konzeptualisierung bis hin zur sprachlichen Paläontologie. CLICS wurde aber auch wegen offensichtlicher Mängel kritisiert. Aufbauend auf den Standardisierungsbemühungen der CLDF-Initiative und neuartigen Ansätzen für eine schnelle, effiziente und zuverlässige Datenaggregation hat CLICS² die ursprüngliche CLICS-Datenbank erweitert. CLICS³ – die dritte Ausgabe von CLICS – nutzt das in CLICS² entwickelte Framework, um die in der Datenbank aggregierte Datenmenge mehr als zu verdoppeln.

https://clics.clld.org