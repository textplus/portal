---
title: Tailored Corpora and Topic Models for Japanese Parliamentary Minutes

institutions: 
- Deutsches Institut für Japanstudien

languages:
- Japanisch

modalities:
- geschrieben

text_plus_domains: 
- Collections
- Lexikalische Ressourcen
- Software Services

dfg_areas:
- 102 Geschichtswissenschaften
- 104 Sprachwissenschaften
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft
- 111 Sozialwissenschaften

descriptive_areas: 
- Japanologie
- Ostasienwissenschaften
- Geschichte (insb. Begriffsgeschichte)
- Politikwissenschaft
- Linguistik

type: community-data

---

Die ursprüngliche Datenbank für sprachübergreifende Colexifikationen (CLICS) hat ein computergestütztes Framework für die interaktive Darstellung sprachübergreifender Colexifikationsmuster eingerichtet. Es hat sich als nützliches Instrument für verschiedene Arten der Untersuchung sprachübergreifender semantischer Assoziationen erwiesen, angefangen von Studien zum semantischen Wandel über Muster der Konzeptualisierung bis hin zur sprachlichen Paläontologie. CLICS wurde aber auch wegen offensichtlicher Mängel kritisiert. Aufbauend auf den Standardisierungsbemühungen der CLDF-Initiative und neuartigen Ansätzen für eine schnelle, effiziente und zuverlässige Datenaggregation hat CLICS² die ursprüngliche CLICS-Datenbank erweitert. CLICS³ – die dritte Ausgabe von CLICS – nutzt das in CLICS² entwickelte Framework, um die in der Datenbank aggregierte Datenmenge mehr als zu verdoppeln.

https://clics.clld.org