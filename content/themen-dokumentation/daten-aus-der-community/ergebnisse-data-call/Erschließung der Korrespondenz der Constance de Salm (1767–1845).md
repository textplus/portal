---
title: Erschließung der Korrespondenz der Constance de Salm (1767–1845)

institutions: 
- Deutsches Historisches Institut Paris

languages:
- "Deutsch"

modalities:
- geschrieben

text_plus_domains: 
- Editionen 

dfg_areas:
- 102 Geschichtswissenschaften
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft

descriptive_areas: 
- Geschichte
- Sprachwissenschaften
- Literaturwissenschaften

type: community-data

---

Es handelt sich um die Metadaten, mit denen die Korrespondenz der Constance de Salm (rund 11.000 Briefe) in einem langjährigen Projekt des Deutschen Historischen Instituts Paris inhaltlich und formal erschlossen wurden. Neben Empfänger- und Sendedaten wie Person, Ort und Datum gibt es eine inhaltliche Erschließung über Schlagworte.

https://constance-de-salm.de/