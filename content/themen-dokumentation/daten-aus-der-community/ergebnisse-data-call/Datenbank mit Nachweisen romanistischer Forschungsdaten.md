---
title: Datenbank mit Nachweisen romanistischer Forschungsdaten

institutions: 
- Fachinformationsdienst Romanistik

languages:
- Deutsch
- Sacherschließung teilweise zusätzlich Französisch

modalities:
- geschrieben
- gesprochen

text_plus_domains: 
- Software Services 

dfg_areas:
- 101-02 Klassische Philologie
- 102 Geschichtswissenschaften
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft
- 108 Philosophie

descriptive_areas: 
- Romanistik (Literaturwissenschaft, Sprachwissenschaft, Kultur- und Medienwissenschaft, Fachdidaktik)

type: community-data

---

Datenbank auf der Basis von Academic LinkShare, in der u.a. Forschungsdaten nach Dublin Core formal und sachlich beschrieben werden. Die Sacherschließung umfasst die Vergabe von GND-Schlagwörtern, von DDC-Hauptklassen, Klassifikationen nach Regionen und Ressourcentyp sowie Abstracts. Auszüge lassen sich nach Bedarf generieren und können separat auf einzelnen Webseiten präsentiert werden. Die Daten sind inzwischen auch in den Index des FID-Suchportals integriert worden (derzeit noch testweise) und darüber recherchierbar.

[Leitseite](https://fid-romanistik.de//forschungsdaten/suche-nach-forschungsdaten/fid-internetressourcen/romanistik-insgesamt/)
[Liste](https://discovery.fid-romanistik.de/Search/Results?filter%5B%5D=collection_details%3AFID_ROM_ALS&filter%5B%5D=topic_facet%3AForschungsdaten)
[Allgemeines Informationsangebot zum Forschungsdatenmanagement](https://fid-romanistik.de//forschungsdaten/)