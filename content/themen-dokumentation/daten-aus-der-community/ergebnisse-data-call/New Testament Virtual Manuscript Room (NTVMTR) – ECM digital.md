---
title: New Testament Virtual Manuscript Room (NTVMTR) – ECM digital

institutions: 
- Westfälische Wilhelms-Universität Münster, Institut für neutestamentliche Textforschung

languages:
- Griechisch
- Deutsch
- Englisch

modalities:
- geschrieben

text_plus_domains: 
- Collections
- Editionen

dfg_areas:
- 102 Geschichtswissenschaften
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 107 Theologie

descriptive_areas: 
- Philologien
- Theologie
- Editionswissenschaft
- Papyrologie

type: community-data

---

Im Virtuellen Handschriften-Lesesaal (Virtual Manuscript Room = VMR) wird die herkömmliche Kurzgefaßte Liste der griechischen Handschriften ergänzt durch alle Informationen, die zu den einzelnen Handschriften vorhanden sind. Vor allem werden, soweit die besitzenden Institutionen zustimmen, Fotos der Manuskripte über eine entsprechende Website zur Verfügung gestellt. Zu diesem Zweck werden die Mikrofilmbestände des INTF gescannt (z.T. auch neue Fotos beschafft) und inhaltlich so erschlossen, dass sie mit den im INTF erstellten Transkripten der Handschriften verbunden werden. Links verweisen auch auf anderweitig im Netz bereitgestellte Fotos und Informationen. Der VMR ist inzwischen zu einer interaktiven Editionsplattform ausgebaut werden und dient als Arbeitsgrundlage für die im INTF erstellte Editio Critica Maior (ECM) des griechischen Neuen Testaments, kann aber auch für andere textkritische Editionen handschriftlich überlieferter Werke genutzt werden. Zur Zeit wachsen ECM und VMR zu einer interaktiven kritischen Edition des Neuen Testaments zusammen.

https://ntvmr.uni-muenster.de/