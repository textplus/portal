---
title: GEI-Digital – Die digitale Schulbuch-Bibliothek

institutions: 
- Georg-Eckert-Institut – Leibniz-Institut für internationale Schulbuchforschung

languages:
- Deutsch

modalities:
- geschrieben

text_plus_domains: 
- Collections 
- Editionen

dfg_areas:
- 102 Geschichtswissenschaften
- 103 Kunst-, Musik-, Theater- und Medienwissenschaften
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 109 Erziehungswissenschaft und Bildungsforschung
- 111 Sozialwissenschaften

descriptive_areas: 
- Bildungsmedienforschung
- (Neuere) Geschichte
- Kulturgeschichte
- Literaturwissenschaft
- Soziologie
- Deutsche Philologie
- (historische) Linguistik

type: community-data

---

GEI-Digital bietet freien Zugang zu digitalisierten historischen deutschen Lehrbüchern aus der Zeit vor 1918 über digitale Bilder, OCR-Volltext und umfangreiche Metadaten. Es erlaubt die gezielte Volltextsuche in vorhandenen digitalisierten Sammlungen. Es enthält derzeit mehr als 6.300 Bände deutschsprachiger Lehrbücher, hauptsächlich Lesefibeln und den Fächern „Realienkunde“ (grundlegende Sozial- und Naturwissenschaften), Geographie und Geschichte.

https://gei-digital.gei.de/