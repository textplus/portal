---
title: Niedersorbische Textkorpora

institutions: 
- Sorbisches Institut Bautzen

languages:
- Niedersorbisch

modalities:
- geschrieben

text_plus_domains: 
- Collections
- Editionen

dfg_areas:
- 101-02 Klassische Philologie
- 102 Geschichtswissenschaften
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft
- 108 Philosophie

descriptive_areas: 
- Sprachwissenschaft
- Geschichtswissenschaft
- Kulturwissenschaft
- Computerlinguistik
- Digital Humanities

type: community-data

---

Unterschieden werden ein „altes“ sowie ein „neues“ Textkorpus (im Aufbau). Beide Korpora sind mit verschiedenen Zugriffsmethoden verbunden. Die Datengrundlage für letzteres umfasst zurzeit (2020) ca. 43 Millionen Tokens. Die Texte werden schrittweise annotiert (u.a. Normalisierung/Lemmatisierung). Die Suche erfordert keine vertieften Kenntnisse über die historische Schreibung und Formenvielfalt, greift aber aktuell noch auf wenig Texte zu. Das alte Textkorpus umfasst mehr als 23 Millionen Tokens, wovon ca. 15 Millionen online zur Verfügung stehen. Die Texte sind nicht annotiert und kaum weiter verarbeitet, es liefert ausschließlich die Originalschreibweise. Außerdem sind die Texte nicht korrigiert, so dass mit (Ab-)Schreibfehlern zu rechnen ist.

https://www.niedersorbisch.de/korpus/