---
title: JudaicaLink

institutions: 
- Hochschule der Medien, Stuttgart

languages:
- Deutsch
- Englisch
- Hebräisch
- Russisch

modalities:
- geschrieben

text_plus_domains: 
- Lexikalische Ressourcen
- Software Services 

dfg_areas:
- 102 Geschichtswissenschaften
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft
- 107 Theologie

descriptive_areas: 
- Judaistik
- Theologie
- Geschichtswissenschaften 

type: community-data

---

JudaicaLink ist ein RDF-basierter Knowledge-Graph, der Daten aus unterschiedlichen Quellen im Bereich Judaistik integriert. Dazu gehören vor allem Lexika und Biographien, Verlinkungen bestehen zu GDN, DBpedia/Wikipedia, Wikidata und vielen weiteren Datenquellen. Die Daten werden im FID Jüdische Studien genutzt, um Querverbindungen in Metadaten und aktuell auch Volltexten herzustellen.

https://web.judaicalink.org
