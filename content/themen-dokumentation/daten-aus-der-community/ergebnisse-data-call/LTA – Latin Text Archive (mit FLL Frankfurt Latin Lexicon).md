---
title: LTA – Latin Text Archive (mit FLL - Frankfurt Latin Lexicon)

institutions: 
- Goethe-Universität Frankfurt am Main

languages:
- Latein

modalities:
- geschrieben

text_plus_domains: 
- Collections
- Editionen 
- Lexikalische Ressourcen

dfg_areas:
- 101 Alte Kulturen
- 102 Geschichtswissenschaften
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 107 Theologie
- 113 Rechtswissenschaften

descriptive_areas: 
- Theologie
- Geschichte
- historische Kulturwissenschaften
- Rechtswissenschaften
- Romanistik
- Mittelalterliche Lateinische Philologie
- Linguistik

type: community-data

---

Das LTA ist ein frei zugängliches, webbasiertes analytisches Archiv von (großenteils) hochwertigen kritischen Editionen lateinischer Texte, die mit komplexen Metadaten versehen, vollständig lemmatisiert und mit einem Vollformen-Lexikon (FLL) verknüpft sind. Es bietet diachronisch organisierte Referenzkorpora nach Textgenres und Text Mining-Tools für die individuelle Korpusbildung und -analyse. Größe und Vielfalt des Inhalts ermöglichen es Forschern, zuverlässige diachrone Korpora (z. B. aus nur einem Texttyp) zur Analyse zu erstellen. Es umfasst die Textproduktion im lateinischsprachigen Europa von 400 bis 1500, wird aber kontinuierlich erweitert. Technisch basiert das LTA auf dem DTA (Deutsches Textarchiv).

[Neue Website, noch in der Testphase](https://lta.bbaw.de/)
[Die aktuelle Website mit einigen grundlegenden Analysetools, die von der Goethe-Universität gehostet werden, öffentlich zugänglich](http://www.comphistsem.org/home.html)
[Betriebsplattform für Vorverarbeitung, Annotation und Anwendung ausgearbeiteter Analysetools, kostenlose Registrierung erforderlich](https://hudesktop.hucompute.org/)
[Lexikon, aktueller Zugriff auf die FLL](http://www.comphistsem.org/70.html)


