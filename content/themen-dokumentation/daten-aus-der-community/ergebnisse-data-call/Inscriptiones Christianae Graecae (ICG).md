---
title: Inscriptiones Christianae Graecae (ICG)

institutions: 
- Humboldt Universität zu Berlin
- Christian-Albrechts-Universität zu Kiel
- Inscriptiones Graecae BBAW

languages:
- Griechisch

modalities:
- geschrieben

text_plus_domains: 
- Collections
- Editionen
- Lexikalische Ressourcen

dfg_areas:
- 101 Alte Kulturen
- 102 Geschichtswissenschaften
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 107 Theologie

descriptive_areas: 
- Religionsgeschichte
- Epigraphie
- Theologie
- Geschichte
- historische Kulturwissenschaften
- Linguistik

type: community-data

---

ICG umfasst fast alle christlichen Ehren-, Votiv-, Bau- und Grabinschriften aus Kleinasien und Griechenland. Derzeit umfasst die wachsende Datenbank 4297 griechische Inschriften. Jedem Monument ist eine ICG-Nummer zugewiesen. Jeder ICG-Eintrag enthält den griechischen Originaltext, eine deutsche oder englische Übersetzung, einen knappen kritischen Apparat und Kommentar, ein oder mehrere Bilder, wenn verfügbar (d. h. ein Foto oder eine Zeichnung, bisher 5812 Abbildungen), sowie alle relevanten Informationen Datierung, Typologie, antike und moderne Provenienz, aktueller Standort und Fundort und den Fundumständen und ist mit Geokoordinaten versehen, die mit digitalen Online-Karten verlinkt sind, nämlich Pleiades, dem iDAI.gazetteer, und GeoNames. Für die archäologischen Belege zum Aufstieg und Verbreitung des Christentums in Attika wurden Kataloge zu den frühchristlichen Kirchen und spätantiken Friedhöfen in Attika fertiggestellt.

ICG bietet einen geographisch strukturierten Zugang zu bisher vernachlässigten lokalisierbaren Primärquellen, indem es Tausende von Inschriften gesammelt hat, die zuvor über Hunderte von Publikationen (seit dem 19. Jahrhundert) verstreut waren. Als erste digitale Datenbank zu christlich-griechischen Inschriften eröffnet sie enorme Möglichkeiten der interdisziplinären Zusammenarbeit zwischen Historikern, die sich für den Aufstieg und die Ausbreitung des Christentums und seinen Fußabdruck von der spätkaiserlichen bis zur frühbyzantinischen Zeit in Kleinasien und Griechenland interessieren, und ist ein nützliches Werkzeug für Forscher aus der Kirchengeschichte, der Alten Geschichte, der Epigraphik, der Archäologie und der Geschichte der Religionen. Die Datenbank kann online über einen ‚Gast‘-Login aufgerufen und nach ICG-Referenznummern, Regionen, antiken und modernen Orten, Bildern, Inschriftentypen oder bibliographischen Einträgen durchsucht und/oder durchgeblättert werden. Darüber hinaus ermöglichen Suchfunktionen die Abfrage der griechischen Originaltexte, der Übersetzungen und der anderen Metadaten unter Verwendung einer Vielzahl von Filtern (z. B. Region, Datierung, Ort, Typ). Da jeder antike und moderne Ort mit Geokoordinaten versehen und mit digitalen Online-Karten verknüpft wurde, ist ICG eine unschätzbare Ressource für die Erforschung der Verbreitung, der sozialen Struktur und des Profils des frühen Christentums und der Entwicklung des Klerus vom 3. bis zum 6. Jahrhundert. Die Datenbank öffnet die Tür, um lokale christliche Gemeinschaften, ihren kulturellen Hintergrund, Geschlechterrollen und Familienbeziehungen, Formen der Führung, den Gebrauch der Schrift, den Aufstieg der Asketen und die Theologie zu studieren und sie mit nicht-christlichen Landsleuten zu vergleichen. Erste Ergebnisse finden sich in den Monographien zu Lykaonien von C. Breytenbach und C. Zimmermann (Leiden: Brill, 2018) und zu Attika von C. Breytenbach und E. Tzavella (Leiden: Brill, 2023).

Das ICG bildet derzeit die Grundlage für die Abfassung von 9 Monographien zum Aufstieg des Christentums in Regionen Kleinasiens (Ionien, Untermäander, Phrygien, Galatien), in Griechenland (Korinth und Peloponnes, Attika, Thessalien und Makedonien) und auf dem Balkan. Es ergänzt auch die archäologischen Beweise bei der Erstellung von Monographien über den Aufstieg und die Verbreitung des Christentums. Durch Verweise auf ICG werden die umfangreichen Primärdaten, einschließlich Karten und Bilder, den Lesern der Publikationen online zur Verfügung gestellt. Wenn ICG über Kleinasien und Griechenland hinaus erweitert werden kann, würde es die zukünftige Forschung zum frühen Christentum auf den griechischen Inseln, in Zypern, Syrien und Palästina sowie in Ägypten erheblich erleichtern.

[ICG – Inscriptiones Christianae Graecae: Eine Datenbank der frühchristlichen Inschriften Kleinasiens und Griechenlands](https://icg.uni-kiel.de/)
[Authorization of Earlcy Christian Knowledge Claims in Asia Minor and Greece](https://www.topoi.org/project/b-5-3/)