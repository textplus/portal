---
title: GND Agency Text+

draft: false
---
# GND Agency Text+

{{<cards>}}
{{<card image="gfx/gnd_logo.png" width="small">}}
{{</card>}}
{{</cards>}}

## Who Are We?
The GND Agency Text+ is a service established at [SUB Göttingen](https://www.sub.uni-goettingen.de/en/) as part of the NFDI-Program Text+.

As a partner of the GND ([Integrated Authority File](https://gnd.network/Webs/gnd/EN/Home/home_node.html)), the agency serves as the central point of contact, initially for projects in the humanities and cultural sciences that are funded under the NFDI consortium Text+. In the long term, there are plans to both institutionalize the service and expand its offerings to a broader project landscape.

## What Do We Do?
The agency's goal is to support projects in integrating their research data as standard data into the GND.

This includes:
* Providing guidance on the suitability criteria of data for the GND.
* Offering and advancing [entityXML](https://gitlab.gwdg.de/entities/entityxml), the data format for the exchange of standard data between projects and the agency.
* Providing a technical environment for data processing based on entityXML.
* Integrating potential candidates into the GND.
* Providing GND-IDs for the projects.

## How Do We Support?
**Contact Point:** We are the first point of contact when you would like to integrate your data into the GND. If there is a project-specific need, we may refer you to one of our GND partner agencies.

**Consultation Center (under development):** We provide advice on GND-related questions and offer information materials.

**Data Service:** We assist in creating GND-compliant standard data. Your data will be validated, cleaned, enriched, and converted into the format required for data ingestion into the GND, enriched with a GND-ID and returned to you by us.

**Contact:** gnd-agentur[at]sub.uni-goettingen.de

## Formats

* [entityXML](https://gitlab.gwdg.de/entities/entityxml) as:
    * Exchange format between data supplier and agency: [entityXML as exchange format](https://entities.pages.gwdg.de/entityxml/##als-austauschformat)
    * Storage format for research data: [entityXML as storage format](https://entities.pages.gwdg.de/entityxml/##als-speicherformat)
