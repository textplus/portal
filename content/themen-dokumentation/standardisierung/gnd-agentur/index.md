---
title: GND-Agentur Text+ 

draft: false
---


# GND-Agentur Text+

{{<cards>}}
{{<card image="gfx/gnd_logo.png" width="small">}}
{{</card>}}
{{</cards>}}

## Wer sind wir?
Die GND-Agentur Text+ ist ein Service, der im Rahmen des NFDI-Programms Text+ an der [SUB Göttingen](https://www.sub.uni-goettingen.de/) aufgebaut wird.

Als Partner der GND ([Gemeinsame Normdatei](https://gnd.network/Webs/gnd/DE/Home/home_node.html)) fungiert die Agentur als zentrale Anlaufstelle zunächst für geistes- und kulturwissenschaftliche Projekte, die im Rahmen des NFDI-Konsortiums Text+ gefördert werden. Langfristig ist geplant, sowohl den Service zu verstetigen als auch das Angebot auf eine breitere Projektlandschaft auszuweiten.

## Was machen wir?
Ziel der Agentur ist es, Projekte dabei zu unterstützen, ihre Forschungsdaten als Normdaten in die GND einzubringen.

Dazu gehören:
* Die inhaltliche Beratung hinsichtlich der Eignungskriterien von Daten für die GND.
* Die Bereitstellung und Weiterentwicklung von [entityXML](https://gitlab.gwdg.de/entities/entityxml), dem Datenformat für den Austausch von Normdaten zwischen Projekten und Agentur. 
* Die Bereitstellung einer technischen Umgebung für die Datenverarbeitung auf Grundlage von entityXML .
* Das Einspielen von potentiellen Kandidaten in die GND.
* Die Bereitstellung von GND-IDs für die Projekte.

## Wie unterstützen wir?
**Kontaktstelle:** Wir sind die erste Anlaufstelle, wenn Sie Ihre Daten in die GND einbringen möchten. Bei projektspezifischem Bedarf vermitteln wir Sie ggf. an eine unserer GND-Partneragenturen weiter.

**Beratungsstelle (im Aufbau):** Wir beraten Sie bei Fragen rund um die GND und stellen Ihnen Informationsmaterial zur Verfügung.

**Datenservice:** Wir unterstützen Sie bei der Erstellung von GND-konformen Normdaten. Ihre Daten werden validiert, bereinigt, angereichert und in das für den Dateningest in die GND erforderliche Format konvertiert. Die mit einem GND-ID angereicherten Daten werden von uns an Sie zurückgespielt.

**Kontakt:** gnd-agentur[at]sub.uni-goettingen.de

## Formate

* [entityXML](https://gitlab.gwdg.de/entities/entityxml) als:
    * Austauschformat zwischen Datenlieferant und Agenur: https://entities.pages.gwdg.de/entityxml/##als-austauschformat
    * Speicherformat für Forschungsdaten: https://entities.pages.gwdg.de/entityxml/##als-speicherformat
