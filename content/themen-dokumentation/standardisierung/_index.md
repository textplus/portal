---
title: Standardisierung

menu:
  main:
    weight: 40
    parent: themen-dokumentation

aliases:
- /daten-dienste/cross-cutting-topics/#standardisierungsgremien
- /daten-dienste/gnd-agentur/
---

# Standardisierung

Für [FAIRe](https://www.go-fair.org/fair-principles/) Forschungsdaten ist die Orientierung an etablierten und weit
verbreiteten Standards für Objekt- und Metadaten essentiell.
Insbesondere zur Gewährleistung von Auffindbarkeit, Interoperabilität
und Nachnutzbarkeit in einer ortsverteilten Infrastruktur wie der in
Text+ sind einheitliche Formate und Standards über die Repositorien
hinweg unerlässlich.

Text+ verfolgt im Hinblick auf Datenstandards mehrere Ziele: 
* Mittels Veranstaltungen und Beratung zum Forschungsdatenmanagement (z. B. durch den **[Text+ Helpdesk](https://text-plus.org/helpdesk/)**) soll die data literacy in der Community im
Bezug auf Standards erhöht werden. 
* Zahlreiche Mitarbeitende von Text+ sind in nationalen und internationalen
**[Standardisierungsgremien](themen-dokumentation/standardisierung/#standardisierungsgremien)** aktiv, um Standards (weiter) zu entwickeln und zu etablieren.
* Die **[GND-Agentur Text+](themen-dokumentation/standardisierung/gnd-agentur)** unterstützt Projekte, die im Rahmen des NFDI-Konsortiums Text+ gefördert werden, dabei ihre Forschungsdaten als Normdaten in die [Gemeinsame Normdatei](https://gnd.network/Webs/gnd/DE/Home/home_node.html) einzubringen.

## Standards für Text- und Sprachdaten

Damit Forschende mit den von Text+ zur Verfügung gestellten [Daten](daten-dienste/daten) gut arbeiten können, dokumentieren die Text+ Zentren die in ihren Ressourcen jeweils angewendeten Standards ausführlich. Doch auch bei der [Übernahme von Daten](daten-dienste/depositing) steht die Datenqualität – und damit auch die verwendeten Standards – im Mittelpunkt. Darum erfolgt im Vorfeld der Datenübernahme stets ein ausführliches bilaterales Beratungsgespräch
zwischen den Datengebenden und Mitarbeitenden des zuständigen [Text+ Zentrums](ueber-uns/daten-kompetenzzentren).

Allgemeine Informationen zu gängigen Standards für Sprach- und Textdaten stellt Text+ in mehreren Publikationen zur Verfügung:

* Standards und bevorzugte Datenformate für Korpora und Textsammlungen – sowohl für Meta- und Objektdaten als auch für Datenpakete – wurden in diesem Papier zusammengetragen: https://zenodo.org/doi/10.5281/zenodo.12800179.
* Empfehlung zur Erstellung, Bearbeitung und Publikation FAIRer Forschungsdaten in der Datendomäne Editionen - [A Living Handbook über das How-to der Bewertung und Gewährleistung FAIRer, qualitativ hochwertiger editionswissenschaftlichen Forschungsdaten](https://gitlab.gwdg.de/textplus/textplus-editions/guidelines_sde).
* Overview of writing systems and character encoding standards and article formats used in the domain: https://doi.org/10.5281/zenodo.13867283.

## Anwendungsbeispiele

Hier finden Sie Beispiele für einen gelungenen Einsatz von Standards im Bereich der Text- und Sprachdaten:

{{<button url="themen-dokumentation/standardisierung/anwendungsbeispiele" is_primary="true">}}Infoseite Anwendungsbeispiele {{</button>}}

## GND-Agentur

Die GND-Agentur Text+ ist ein Service, der im Rahmen des NFDI-Programms Text+ an der [SUB Göttingen](https://www.sub.uni-goettingen.de/) aufgebaut wird. Als Partner der GND ([Gemeinsame Normdatei](https://gnd.network/Webs/gnd/DE/Home/home_node.html)) fungiert die Agentur als zentrale Anlaufstelle zunächst für geistes- und kulturwissenschaftliche Projekte, die im Rahmen des NFDI-Konsortiums Text+ gefördert werden. Mehr zur GND-Agentur befindet sich hier:

{{<cards>}}
{{<card image="gnd-agentur/gfx/gnd_logo.png">}}
{{</card>}}
{{</cards>}}

{{<button url="themen-dokumentation/standardisierung/gnd-agentur" is_primary="true">}} Infoseite GND-Agentur {{</button>}}

## Standardisierungsgremien

Mitglieder von Text+ bringen sich aktiv in unterschiedliche Gremien und Organisationen ein, die die Weiterentwicklung anerkannter Standards betreiben.

{{<image img="files/Networking-Initiativen-Gremien_new.png" alt="Übersicht über Gremien in denen Text+-Mitglieder eingebunden sind"/>}}

- [DIN – Deutsches Institut für Normung](https://www.din.de/)
- [DINI – Deutsche Initiative für Netzwerkinformation e.V.](https://dini.de/) [(Letter of Support, PDF)](/vernetzung/verbaende-verbuende/letters/LoS-DINI.pdf)
- [IIIF – International Image Interoperability Framework](https://iiif.io/)
- [ISO – International Organization for Standardization](https://www.iso.org/)
- [RDA – Research Data Alliance](https://rd-alliance.org/) [(Letter of Support, PDF)](/vernetzung/verbaende-verbuende/letters/LoS-RDA.pdf)
- [TEI – Text Encoding Initiative](https://tei-c.org/)
- [W3C – World Wide Web Consortium](https://www.w3.org/)
