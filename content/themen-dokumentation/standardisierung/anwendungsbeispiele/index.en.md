---
title: Application examples

menu:
  main:
    weight: 40
    parent: Standardisation
---

# Application Examples

The research initiatives from the text and language sciences described below successfully use [standards recommended by Text+](/themen-dokumentation/standardisierung) and standards-based tools. A series of workshops will also be offered in the first half of 2025, where these application examples will be presented and discussed. Links to the events will be provided on this page in due course.

## correspSearch – Searching and Connecting Correspondence Editions

The [“correspSearch”](https://correspsearch.net) web service of the Berlin-Brandenburg Academy of Sciences and Humanities ([BBAW](https://www.bbaw.de/)) aggregates metadata from letters across distributed editions and repositories and provides it centrally through open interfaces under a free license. This allows users to search various digital and printed letter directories by sender, recipient, place of writing, and date.

{{<image img="cS-logo.png">}}
   Project Logo of [correspSearch](https://github.com/correspSearch)
{{</image>}}

The foundation of the “correspSearch” web service are digital letter directories provided online in the [Correspondence Metadata Interchange (CMI) format](https://correspsearch.net/en/participate.html). The CMI format is largely based on the [“correspDesc” (Correspondence Description)](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-correspDesc.html) extension for the [Text Encoding Initiative (TEI) guidelines](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/index.html). The correspDesc element was developed by the [TEI Correspondence SIG](https://www.tei-c.org/Activities/SIG/Correspondence/) to record the correspondence-specific metadata of letters, postcards, etc., in TEI-based editions.

{{<image img="funktion-cS.jpg">}}
   [Funcitionality of correspSearch](https://correspsearch.net/de/ueber.html)
{{</image>}}

## The German Reference Corpus (DeReKo)

{{<image img="ids-logo.svg">}}
   [IDS Logo](https://www.ids-mannheim.de/)
{{</image>}}

The [written contemporary language corpora](https://www.ids-mannheim.de/digspra/kl/projekte/korpora) of the Leibniz Institute for the German Language ([IDS](https://www.ids-mannheim.de/)) constitute the world’s largest linguistically motivated collection of electronic corpora containing written German texts from the present and recent past. They include literary, academic, and popular science texts, a large number of newspaper articles, and a broad range of other text types.

For efficient automatic analysis of large electronic text collections, the texts must be encoded in a unified data structure format. For written language corpora at IDS, this format is the so-called [IDS Text Model](https://www.ids-mannheim.de/digspra/kl/projekte/korpora/textmodell/), based on [TEI (Text Encoding Initiative)](https://tei-c.org/).

{{<image img="screenshot-projbeschr.png">}}
   [Project description of _DeReKo_](https://www.ids-mannheim.de/digspra/kl/projekte/korpora/)
{{</image>}}

## German Text Archive (DTA)

{{<image img="dta-logo.png">}}
   [DTA Logo](https://www.deutschestextarchiv.de/)
{{</image>}}

The [German Text Archive (DTA)](https://www.deutschestextarchiv.de/) of the Berlin-Brandenburg Academy of Sciences and Humanities ([BBAW](https://www.bbaw.de/)) provides a cross-disciplinary and genre-spanning selection of German-language texts with a publication focus from around 1600 to 1900 as a linguistically annotated full-text corpus.

The DTA places great emphasis on very high accuracy in data capture, structural and linguistic annotation of text data, and the reliability of metadata. A key tool in the development of the corpus was the establishment of a standardized format, the [DTA Base Format (DTABf)](https://www.deutschestextarchiv.de/doku/basisformat/), based on [TEI (Text Encoding Initiative)](https://tei-c.org/). The DTA Base Format is [recommended by the DFG for reuse](https://www.deutschestextarchiv.de/doku/basisformat/ziel.html).

{{<youtube id="q6rPFypzJaE" title="Video-Tutorial 01: Deutsches Textarchiv - Einführung">}}
Video-Tutorial 01: German Text Archive - Introduction (in German)
{{</youtube>}}

## The Beria-Collection

{{<image img="beria_01.png">}}
   [Logo of the Data Centers for the Humanities](https://dch.phil-fak.uni-koeln.de/)
{{</image>}}

{{<image img="beria_02.png">}}
   [Logo of the Language Archive Cologne](https://lac.uni-koeln.de//)
{{</image>}}

{{<image img="beria_04.png">}}
   [Map of the Beria-Collection](https://lac.uni-koeln.de//)
{{</image>}}

The Beria Collection comprises a selection of 12 texts from the Beria corpus. Beria – also known in literature under the exonym Zaghawa – is a language spoken in Darfur, in the border region between Sudan and Chad (Nilo-Saharan, 315,000 speakers). The selected texts were written by two adult men who speak the Sudanese dialect Wagi. They are available digitally in audio-video format in the Cologne Language Archive (LAC, link to online access will follow after publication) and will be fully transcribed, translated, time-aligned and annotated with detailed linguistic information in the form of interlinear glosses as part of the project. The aim is to integrate a machine-readable and searchable text corpus from an oral, non-European and little-described culture into the Text+ portfolio. A digital lexicon is currently being created in an upstream additional step, which will supplement the text corpus.
Standards of general linguistics, as formulated for language documentation corpora by the QUEST guidelines, are being used, including linguistic annotations in IPA and in accordance with the Leipzig Glossing Rules (LGR) in the XML-based programme ELAN, as well as the CMDI profile BLAM for metadata, which was developed by the LAC as part of CLARIN to describe audiovisual language data in particular.

## edition humboldt digital

{{<image img="berlin-logo.svg">}}
   [Logo of the Academy of Sciences in Berlin](https://www.bbaw.de/)
{{</image>}}

The project [“Alexander von Humboldt on his travels – Science in Motion”](https://edition-humboldt.de/) of the Berlin-Brandenburg Academy of Sciences and Humanities ([BBAW](https://www.bbaw.de/)) includes the complete edition of Alexander von Humboldt’s manuscripts on the theme of travel at the intersection of cultural and natural sciences. The corpus of the projected edition includes travel journals, diaries, memoranda, publications in the countries and regions visited, and correspondence.

edition humboldt digital works with the [DTA Base Format (DTABf)](https://www.deutschestextarchiv.de/doku/basisformat/) and the [Integrated Authority File (GND)](https://gnd.network/Webs/gnd/EN/Home/home_node.html), as well as a [variety of other established standards and tools](https://www.bbaw.de/files-bbaw/user_upload/BBAW_Vorhabenflyer_AvH_2019_online.pdf).

{{<image img="screenshot-edition-humboldt-digital.png">}}
   [Website of _edition humboldt digital_](https://edition-humboldt.de/)
{{</image>}}

## Klaus Mollenhauer Complete Edition (KMG)

The “Klaus Mollenhauer Complete Edition” (Link to Digital Edition to follow after publication) is a text-critical and annotated edition of the writings of educational scientist Klaus Mollenhauer (1928–1998). It includes both a digital edition and a printed and open-access book version with an apparatus.

The [joint project](https://www.uni-goettingen.de/en/klaus+mollenhauer+complete+edition+%28kmg%29/584741.html) works with [TextGrid](https://textgrid.de) and uses [TEI (Text Encoding Initiative)](https://tei-c.org/) for text markup and [LIDO (Lightweight Information Describing Objects)](https://lido-schema.org) for object description. Further standards and tools used are described [here](https://doi.org/10.25656/01:24852).

{{<image img="kmg-logo.jpg">}}
   [Logo of KMG](https://www.uni-goettingen.de/de/klaus+mollenhauer+gesamtausgabe+%28kmg%29/584741.html)
{{</image>}}

## Text+ Cooperation Project INSERT

As part of the Text+-funded cooperation project [“Integration of Lower Sorbian-German Dictionaries as Lexical Resources in Text+ (INSERT)”](https://textplus.hypotheses.org/10020), four of the most important Lower Sorbian-German dictionaries, which the [Sorbisches Institut](https://www.serbski-institut.de/) already provides online, were integrated into the digital infrastructures of Text+ at the [Saxon Academy of Sciences](https://www.saw-leipzig.de/en).

To do this, the existing XML data were first converted into the standard format [TEI Lex-0](https://dariah-eric.github.io/lexicalresources/pages/TEILex0/TEILex0.html) and then incorporated into the [repository of the Saxon Academy of Sciences in Leipzig (SAW)](https://repo.data.saw-leipzig.de/en), one of the Text+ data centers for lexical resources, and into the [Federated Content Search (FCS) of Text+](https://text-plus.org/#action-open-search-content).

{{<image img="screenshot-serbisch.png">}}
   [Exemplary dictionary entry](https://dolnoserbski.de/)
{{</image>}}

## Additional Application Examples

The [Text+ Registry](https://registry.text-plus.org/) lists numerous other corpora and text collections, editions, and lexical resources in addition to the examples mentioned here.
