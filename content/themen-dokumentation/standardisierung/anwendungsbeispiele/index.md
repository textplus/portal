---
title: Anwendungsbeispiele

menu:
  main:
    weight: 40
    parent: Standardisierung
---

# Anwendungsbeispiele

In den unten beschriebenen Vorhaben aus den Text- und Sprachwissenschaften werden [von Text+ empfohlene Standards](themen-dokumentation/standardisierung/) und standardbasierte Tools erfolgreich eingesetzt. Es wird in der ersten Jahreshälfte 2025 dazu auch eine Workshopreihe angeboten, in der die Anwendungsbeispiele vorgestellt und diskutiert werden können. Die Veranstaltungen werden zu gegebener Zeit auch auf dieser Seite verlinkt werden.

## correspSearch – Briefeditionen durchsuchen und vernetzen

Der Webservice [„correspSearch”](https://correspsearch.net) der Berlin-Brandenburgischen Akademie der Wissenschaften ([BBAW](https://www.bbaw.de/)) aggregiert aus verteilten Editionen und Repositorien Briefmetadaten und stellt sie über offene Schnittstellen unter einer freien Lizenz zentral zur Verfügung. So können Verzeichnisse verschiedener digitaler und gedruckter Briefeditionen nach Absender, Empfänger, Schreibort und -datum durchsucht werden.

{{<image img="cS-logo.png">}}
   Logo vom Projekt [correspSearch](https://github.com/correspSearch)
{{</image>}}

Grundlage des Webservices „correspSearch“ sind im [Correspondence Metadata Interchange (CMI) Format](https://correspsearch.net/de/mitmachen.html) online bereitgestellte digitale Briefverzeichnisse. Das CMI-Format basiert wesentlich auf der Erweiterung [„correspDesc“ (Correspondence Description)](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-correspDesc.html) für die [Richtlinien der Text Encoding Initiative (TEI)](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/index.html). Das Element correspDesc wurde von der [TEI Correspondence SIG](https://www.tei-c.org/Activities/SIG/Correspondence/) entwickelt, um die Korrespondenz-spezifischen Metadaten von Briefen, Postkarten etc. in TEI-basierten Editionen notieren zu können.  

{{<image img="funktion-cS.jpg">}}
   [Funktionsweise von correspSearch](https://correspsearch.net/de/ueber.html)
{{</image>}}

## Das Deutsche Referenzkorpus (DeReKo)

{{<image img="ids-logo.svg">}}
   [Logo vom IDS](https://www.ids-mannheim.de/)
{{</image>}}

Die [Korpora geschriebener Gegenwartssprache](https://www.ids-mannheim.de/digspra/kl/projekte/korpora) des Leibniz-Instituts für Deutsche Sprache ([IDS](https://www.ids-mannheim.de/)) bilden die weltweit größte linguistisch motivierte Sammlung elektronischer Korpora mit geschriebenen deutschsprachigen Texten aus der Gegenwart und der neueren Vergangenheit. Sie enthalten belletristische, wissenschaftliche und populärwissenschaftliche Texte, eine große Zahl von Zeitungstexten sowie eine breite Palette weiterer Textarten. 

Für die effiziente automatische Auswertung großer elektronischer Textsammlungen müssen die Texte in einem einheitlichen Datenstrukturformat kodiert sein. Für die Korpora geschriebener Sprache am IDS ist dieses Format das so genannte [IDS-Textmodell](https://www.ids-mannheim.de/digspra/kl/projekte/korpora/textmodell/), basierend auf [TEI (Text Encoding Initiative)](https://tei-c.org/). 

{{<image img="screenshot-projbeschr.png">}}
   [Projektbeschreibung von _DeReKo_](https://www.ids-mannheim.de/digspra/kl/projekte/korpora/)
{{</image>}}

## Deutsches Textarchiv (DTA) 

{{<image img="dta-logo.png">}}
   [Logo vom DTA](https://www.deutschestextarchiv.de/)
{{</image>}}

Das [Deutsche Textarchiv (DTA)](https://www.deutschestextarchiv.de/) der Berlin-Brandenburgischen Akademie der Wissenschaften ([BBAW](https://www.bbaw.de/)) stellt eine disziplinen- und gattungsübergreifende Auswahl deutschsprachiger Texte mit einem Publikationsschwerpunkt in der Zeit um 1600 bis 1900 als linguistisch annotiertes Volltextkorpus bereit. 

Das DTA legt großen Wert auf eine sehr hohe Erfassungsgenauigkeit, die strukturelle und linguistische Erschließung der Textdaten sowie die Verlässlichkeit der Metadaten. Ein wichtiges Instrumentarium bei der Entwicklung des Korpus war die Etablierung eines einheitlichen Formates, des [DTA-Basisformats (DTABf)](https://www.deutschestextarchiv.de/doku/basisformat/) auf der Grundlage von [TEI (Text Encoding Initiative)](https://tei-c.org/). Das DTA-Basisformat wird [von der DFG zur Nachnutzung empfohlen](https://www.deutschestextarchiv.de/doku/basisformat/ziel.html).

{{<youtube id="q6rPFypzJaE" title="Video-Tutorial 01: Deutsches Textarchiv - Einführung">}}
Video-Tutorial 01: Deutsches Textarchiv – Einführung
{{</youtube>}}

## Die Beria-Collection

{{<image img="beria_01.png">}}
   [Logo des Data Centers for the Humanities](https://dch.phil-fak.uni-koeln.de/)
{{</image>}}

{{<image img="beria_02.png">}}
   [Logo des Language Archive Cologne](https://lac.uni-koeln.de//)
{{</image>}}

{{<image img="beria_04.png">}}
   [Karte der Beria-Collection](https://lac.uni-koeln.de//)
{{</image>}}

Die Beria-Collection umfasst eine Auswahl von 12 Texten des Beria-Korpus. Beria – in der Literatur auch unter dem Exonym Zaghawa bekannt – ist eine Sprache, die im Darfur, im Grenzgebiet zwischen Sudan und Tschad gesprochen wird (Nilo-Saharanisch, 315.000 Sprecher). Die ausgewählten Texte stammen von zwei erwachsenen Männern, die den sudanesischen Dialekt Wagi sprechen. Sie liegen im Sprachenarchiv Köln (LAC, Link zum Online-Zugang folgt nach Veröffentlichung) digital im Audio-Video-Format vor und werden im Rahmen des Projekts vollständig transkribiert, übersetzt, zeitaligniert annotiert und mit detaillierten linguistischen Informationen in Form von interlinearen Glossen versehen. Ziel ist es, ein maschinenlesbares und durchsuchbares Textkorpus aus einer oralen, außereuropäischen und wenig beschriebenen Kultur in das Text+ Portfolio zu integrieren. Aktuell wird in einem vorgeschalteten Zusatzschritt ein digitales Lexikon erstellt, das das Textkorpus ergänzen wird.
Es werden Standards der allgemeinen Linguistik, wie sie für Sprachdokumentationskorpora durch die QUEST-Richtlinien formuliert wurden, verwendet, darunter linguistische Annotationen in IPA und gemäß den Leipzig Glossing Rules (LGR) im XML-basierten Programm ELAN sowie für Metadaten das CMDI-Profil BLAM, das vom LAC im Rahmen von CLARIN zur Beschreibung insbesondere audiovisueller Sprachdaten entwickelt wurde.

## edition humboldt digital

{{<image img="berlin-logo.svg">}}
   [Logo von der Akademie der Wissenschaften in Berlin](https://www.bbaw.de/)
{{</image>}}

Das Vorhaben [„Alexander von Humboldt auf Reisen – Wissenschaft aus der Bewegung“](https://edition-humboldt.de/) der Berlin-Brandenburgischen Akademie der Wissenschaften ([BBAW](https://www.bbaw.de/)) umfasst die vollständige Edition der Manuskripte Alexander von Humboldts zum Themenkomplex Reisen an der Schnittstelle von Kultur- und Naturwissenschaften. Das Korpus der projektierten Edition beinhaltet Reisejournale, Tagebücher, Denkschriften, Publikationen in den bereisten Ländern und Regionen sowie Korrespondenzen.

edition humboldt digital arbeitet mit dem [Basisformat des Deutschen Textarchivs (DTABf)](https://www.deutschestextarchiv.de/doku/basisformat/) und der [Gemeinsamen Normdatei (GND)](https://gnd.network/Webs/gnd/DE/Home/home_node.html) und setzt darüberhinaus eine [Vielzahl weiterer etablierter Standards und Tools](https://www.bbaw.de/files-bbaw/user_upload/BBAW_Vorhabenflyer_AvH_2019_online.pdf) ein.

{{<image img="screenshot-edition-humboldt-digital.png">}}
   [Website vom Projekt _edition humboldt digital_](https://edition-humboldt.de/)
{{</image>}}

## Klaus Mollenhauer Gesamtausgabe (KMG)

Die „Klaus Mollenhauer Gesamtausgabe“ (Link zur Digitalen Edition folgt nach Veröffentlichung) ist eine textkritische und kommentierte Edition der Schriften des Erziehungswissenschaftlers Klaus Mollenhauer (1928–1998). Sie umfasst sowohl eine digitale Edition als auch eine gedruckte und im Open Access zugängliche Buchfassung mit Apparat. 

Das [Verbundprojekt](https://www.uni-goettingen.de/de/klaus+mollenhauer+gesamtausgabe+%28kmg%29/584741.html) arbeitet mit [TextGrid](https://textgrid.de) und setzt u.a. [TEI(Text Encoding Initiative)](https://tei-c.org/) für die Auszeichnung der Texte und [LIDO (Lightweight Information Describing Objects)](https://lido-schema.org) für die Beschreibung von Objekten ein. Weitere verwendete Standards und Tools werden [hier](https://doi.org/10.25656/01:24852) beschrieben. 

{{<image img="kmg-logo.jpg">}}
   [Logo von KMG](https://www.uni-goettingen.de/de/klaus+mollenhauer+gesamtausgabe+%28kmg%29/584741.html)
{{</image>}}

## Text+ Kooperationsprojekt INSERT

Im Rahmen des von Text+ geförderten Kooperationsprojekts [„Integration niedersorbisch-deutscher Wörterbücher als lexikalische Ressourcen in Text+ (INSERT)“](https://textplus.hypotheses.org/10020) wurden an der [Sächsischen Akademie der Wissenschaften](https://www.saw-leipzig.de/de) vier der wichtigsten niedersorbisch-deutschen Wörterbücher, die das [Sorbische Institut](https://www.serbski-institut.de/) bereits online zur Verfügung stellt, in die digitalen Infrastrukturen von Text+ integriert.

Dazu wurden die bereits vorhandenen XML-Daten zunächst in das Standardformat [TEI Lex-0](https://dariah-eric.github.io/lexicalresources/pages/TEILex0/TEILex0.html) übersetzt und anschließend in das [Repositorium der Sächsischen Akademie der Wissenschaften zu Leipzig (SAW)](https://repo.data.saw-leipzig.de/de), eines der Text+ Datenzentren für lexikalische Ressourcen, und in die [Föderierte Inhaltssuche (FCS) von Text+](https://text-plus.org/#action-open-search-content) aufgenommen. 

{{<image img="screenshot-serbisch.png">}}
   [Exemplarischer Wörterbucheintrag](https://dolnoserbski.de/)
{{</image>}}

## Weitere Anwendungsbeispiele

Die [Text+ Registry](https://registry.text-plus.org/) verzeichnet neben den hier genannten Beispielen eine Vielzahl weiterer Korpora und Textsammlungen, Editionen sowie lexikalischer Ressourcen.
