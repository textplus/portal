---
title: Sprachmodelle und KI 

menu:
  main:
    weight: 30
    parent: themen-dokumentation
---

# Large Language Models (LLMs) und künstliche Intelligenz

Vor dem Hintergrund der rasant fortschreitenden Entwicklung der Large Language Models (LLMs) möchte Text+ Perspektiven für die Einsatzmöglichkeiten generativer Sprachmodelle, künstlicher Intelligenz und Transformermodelle in der Wissenschaft aufzeigen. Seine umfangreichen Bestände an Sprach- und Textdaten will das Konsortium dafür ebenso gewinnbringend einsetzen wie die leistungsfähigen Rechenzentren in den Reihen seiner Partnerinstitutionen.

Text+ dabei verfolgt dabei das Ziel, Anwendungen und Dienste für wissenschaftliche Communities zu entwickeln, die auf LLMs zurückgreifen. Darüber hinaus wollen die Text+ Zentren ihre Sprach- und Textressourcen qualitativ gezielt für das Training von Sprachmodellen aufbereiten. Modelle (Fine-Tuning vortrainierter Modelle oder RAG) für spezifische Aufgaben sollen von Text+ ebenso bereitgestellt werden wie Ressourcen – also Daten und Rechenleistung – für das Fine-Tuning von Modellen durch Forschende. Ferner will Text+ ausloten, wie Material mit (urheber-)rechtlichen Zugangsbeschränkungen in LLMs integriert werden kann, ob und wie LLMs mit [abgeleiteten Textformaten](themen-dokumentation/atf) trainiert werden können und für welche Forschungsfragen LLMs geeignet sind.

Folgende konkrete Use Cases sollen das Potential aufzeigen, das LLMs und Text+ zusammen bieten:

* *Daten-Preprocessing* am Beispiel Named Entity Recognition (NER): LLMs unterstützen beim Daten-Preprocessing zur späteren Anwendung eines speziell trainierten NER-Modells.
* *Laufzeitumgebung* für NLP-Tools: Klassifikatoren (z.B. aus [MONAPipe](https://marketplace.sshopencloud.eu/tool-or-service/9xGRAg) in Text+)  werden in Containern via API bereitgestellt und  mit GPU-Nodes zur effektiven Nutzung von Deep Learning-Modellen versehen.
* *Generierung von Beispielsätzen* bzw. von Kontext: Hier sollen LLMs dabei unterstützen, Einträge im lexikalisch-semantischen Wortnetz [GermaNet](https://marketplace.sshopencloud.eu/dataset/GdQdz2) anzureichern.
* *Query Generation* zur Suchunterstützung in der [Federated Content Search (FCS)](https://fcs.text-plus.org/) von Text+: Ein LLM-basierter ChatBot soll bei der Exploration der FCS unterstützen und dabei helfen, natürlichsprachliche Anfragen in syntaktisch korrekte Suchanfragen für die FCS zu übersetzen.
* *Entity Linking*: LLMs unterstützen bei der Verknüpfung von Named Entities in Volltexten mit Normdaten wie der GND oder Knowledge Bases wie Wikidata.
* *Historische Normalisierungen*: Mit Daten aus historischen Beständen nachtrainierte LLMs passen abweichende Schreibweisen aus verschiedenen Epochen an.
