---
title: Legal and Ethical Aspects

menu:
  main:
    weight: 50
    parent: themen-dokumentation
---
# Legal and Ethical Aspects

Legal and ethical issues are an important topic when dealing with
language and text data. Copyright law takes up by far the most space
here, but privacy and data protection aspects as well as the 
[CARE principles](https://www.gida-global.org/care) also play important roles.


## Help with legal questions

In Text+, lawyers from the Leibniz Institute for the German Language
and the German National Library offer support on legal and ethical
issues relating to language and text-based research data. Contact can
be made at any time via the 
[Text+ Helpdesk](https://text-plus.org/helpdesk/). Please note that Text+
cannot offer individual legal advice.

## Copyright and TDM

For text and data mining (TDM), German copyright law has had a barrier
- i.e. an exception - in 
[Section 60d UrhG](https://www.gesetze-im-internet.de/urhg/__60d.html) 
since 2018, which describes the conditions under which researchers are
permitted to carry out TDM.  This TDM exception was amended in 2021 to
transpose the European Union's DSM Directive into German law. Text+
has published a publication on this 
([Assessment of the Impact of the DSM-Directive on Text+](https://zenodo.org/doi/10.5281/zenodo.12759959)), 
accompanied by a handout that serves as a decision-making aid for the
most important questions regarding the TDM barrier.

{{<image img="files/Decision_support_EN_final.png"/>}}

A further publication is currently in progress, which deals with the
legal aspects of derived text formats. The publication is planned for
the end of 2024.

## Download

* [TDM decision support (PDF)](themen-dokumentation/legal/files/Decision_support_EN_final.pdf)

## Statement on LLM training

In light of the newly sparked debate on the applicability of legal text and data mining (TDM) restrictions to the training of large language models, Text+ has released an official statement.

{{<button url="themen-dokumentation/legal/llm-training" is_primary="true">}} Read Text+ statement {{</button>}}