---
title: Recht und Ethik

menu:
  main:
    weight: 50
    parent: themen-dokumentation
---
# Recht und Ethik

Ein wichtiges Themenfeld im Umgang mit Sprach- und Textdaten sind
rechtliche und ethische Fragestellungen. Dabei nimmt das Urheberrecht
den bei Weitem größten Raum ein, doch auch persönlichkeitsrechtliche
und Datenschutzaspekte sowie die
[CARE-Prinzipien](https://www.gida-global.org/care) spielen wichtige
Rollen.

## Hilfe bei rechtlichen Fragen

In Text+ bieten Jurist:innen des Leibniz-Instituts für Deutsche
Sprache und der Deutschen Nationalbibliothek Unterstützung bei
rechtlichen und ethischen Fragen rund um sprach- und textbasierte
Forschungsdaten. Die Kontaktaufnahme ist jederzeit über den Text+
[Helpdesk](https://text-plus.org/helpdesk/) möglich. Zu beachten ist, dass Text+ keine individuelle Rechtsberatung
anbieten kann.

## Urheberrecht und TDM

Für das Text- und Datamining (TDM) gibt es im deutschen Urheberrecht
seit 2018 in [§60d UrhG](https://www.gesetze-im-internet.de/urhg/__60d.html) eine Schranke – also eine Ausnahmeregelung –, in der
die Voraussetzungen beschrieben sind, unter denen Forschende TDM
betreiben dürfen. Diese TDM-Schranke wurde 2021 novelliert, um die
DSM-Richtlinie der Europäischen Union in deutsches Recht umzusetzen.
Text+ hat dazu eine Publikation veröffentlicht ([Assessment of the
Impact of the DSM-Directive on
Text+](https://zenodo.org/doi/10.5281/zenodo.12759959)), begleitet von einer
Handreichung, die als Entscheidungshilfe bei den wichtigsten Fragen
zur TDM-Schranke dient.

{{<image img="files/Entscheidungshilfe_DE_final.jpg"/>}}

Zurzeit ist eine weitere Publikation in Arbeit, die sich mit
rechtlichen Aspekten von Abgeleiteten Textformaten befasst. Die
Veröffentlichung ist Ende 2024 geplant.

## Download

* [Entscheidungshilfe zur TDM-Schranke (PDF)](themen-dokumentation/legal/files/Entscheidungshilfe_DE_final.pdf)

## Statement zu LLM-Training

Vor dem Hintergrund der neu entfachten Debatte zur Anwendbarkeit der gesetzlichen Text- und Datamining (TDM)-Schranken auf das Training großer Sprachmodelle hat Text+ ein offizielles Statement veröffentlicht.

{{<button url="themen-dokumentation/legal/llm-training" is_primary="true">}} Text+ Statement lesen {{</button>}}