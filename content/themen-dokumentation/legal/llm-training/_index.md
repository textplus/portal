---
title: LLM-Training unter TDM-Schranken

menu:
  main:
    weight: 50
    parent: Recht und Ethik
---
# Warum das Training großer Sprachmodelle von den Text- und Data-Mining-Schranken gedeckt ist

Die Frage, ob das Training von KI-Systemen auf die Text- und Datamining (TDM)-Schranken gestützt werden kann, war Gegenstand einer lebhaften Debatte. Seit einiger Zeit diskutieren Rechtswissenschaftler:innen und Stakeholder das Für und Wider der Anwendbarkeit dieser Gesetzesschranken auf das Training großer Sprachmodelle (LLMs). Die Einführung der KI-Verordnung (KI-VO) der EU schien dieser Diskussion ein Ende zu bereiten, da die Verordnung die Relevanz der TDM-Schranken für das KI-Training explizit anerkennt. Eine kürzlich veröffentlichte [Studie](https://doi.org/10.5771/9783748949558), die technologiebasierte Aspekte in den Vordergrund stellt, hat jedoch die Debatte um die Frage, ob das KI-Training auf die TDM-Schranken gestützt werden kann, neu entfacht. 

Die Schrankenregelungen für das Text- und Data-Mining wurden 2019 durch die EU-Richtlinie zum Urheberrecht im digitalen Binnenmarkt (2019/790) harmonisiert (Artikel 3 und 4). Nach der sehr weit gefassten Definition der Richtlinie bezeichnet Text und Data Mining eine Technik für die automatisierte Analyse von Texten und Daten in digitaler Form, mit deren Hilfe Informationen unter anderem – aber nicht ausschließlich – über Muster, Trends und Korrelationen gewonnen werden können.

Ein LLM ist ein großes Wahrscheinlichkeitsmodell der natürlichen Sprache, das Informationen über Muster, Trends und Korrelationen zwischen Wörtern und Ausdrücken in natürlicher Sprache enthält. Das Training eines LLM entspricht also der Definition von TDM, und zwar unabhängig davon, ob es sich um ein statisches oder dynamisches (generatives) Modell handelt. Ein LLM ist ausdrücklich kein „Speicherort“ für Trainingsdaten, aus dem die Trainingsdaten in unveränderter Form abgerufen werden können. Obwohl aus unterschiedlichen Quellen berichtet wurde, dass einige LLMs Teile der im Training verwendeten Daten „wiederkäuen“, handelt es sich dabei um einen seltenen Zufall oder resultiert aus sog. „hostile prompting“, also der gezielten Eingabe von Prompts, um die Herausgabe von Trainingsdaten zu provozieren. 

Die oben erwähnte Studie geht erheblich tiefer auf die technische Funktionsweise des KI-Trainings ein und argumentiert auf dieser Grundlage, dass KI-Training nicht von den TDM-Ausnahmen umfasst sei. Dieser Argumentationsansatz vermag jedoch nicht zu überzeugen. Der Wille des Gesetzgebers ist hier der entscheidende Faktor.

Es ist möglich, dass der EU-Gesetzgeber die Anwendbarkeit der TDM-Schranken als Grundlage für das Training von großen Sprachmodellen nicht vorhergesehen hat – die DSM-Richtlinie wurde 2016 vorgeschlagen und 2019 verabschiedet, als die Frage von LLM-Training wenig öffentliche Aufmerksamkeit erregte. In jedem Fall wurde die Definition von TDM in der Richtlinie absichtlich weit gefasst, um zukunftsfähig zu sein. Und spätestens mit Erlass der jüngsten europäischen Gesetzgebung – der KI-VO – hat der Gesetzgeber seinem Willen, KI-Training unter die TDM-Schranken zu fassen, unmissverständlich Ausdruck verliehen:

In Artikel 53 Absatz 1 Buchstabe c der KI-VO werden die Anbieter von KI-Modellen mit allgemeinem Verwendungszweck ausdrücklich verpflichtet, Maßnahmen zur Ermittlung und Einhaltung der von den Rechteinhabern geltend gemachten Rechtsvorbehalte festzulegen. Diese Bestimmung bezieht sich direkt auf Artikel 4 der DSM-Richtlinie, der es den Rechteinhabern erlaubt, die Nutzung ihrer Werke für TDM auszuschließen, indem sie einen Nutzungsvorbehalt in maschinenlesbarer Form anbringen. Durch diesen Verweis der KI-VO auf den Opt-out-Mechanismus der DSM-Richtlinie für TDM, erkennt die KI-VO die TDM-Schranken als rechtliche Grundlage für KI-Training an.

Weitere Unterstützung für diese Auslegung findet sich in einem veröffentlichten Fragebogen des Rates der Europäischen Union zum Verhältnis zwischen generativer künstlicher Intelligenz und dem Urheberrecht sowie den verwandten Schutzrechten. In der [Einleitung des Fragebogens](https://data.consilium.europa.eu/doc/document/ST-11575-2024-INIT/en/pdf) betont der Rat, dass die KI-VO die Anwendbarkeit der TDM Schranken auf das Training von LLMs bestätigt, einschließlich der Anwendbarkeit des Opt-out-Mechanismus. 

Die neu entfachte Debatte wird sich daher als gegenstandslos erweisen.

HAFTUNGSAUSSCHLUSS: *Diese Erklärung stellt keine Rechtsberatung dar und sollte auch nicht als solche interpretiert werden. Denken Sie daran, dass die Anwendbarkeit der TDM-Schranken an das Vorliegen bestimmter Voraussetzungen geknüpft ist.* 