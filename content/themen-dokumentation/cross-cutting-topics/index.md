---
title: Cross-Cutting Topics

aliases:
- /vernetzung/standardisierungsgremien
- /vernetzung/cross-cutting-topics

menu:
  main:
    weight: 70
    parent: themen-dokumentation 

draft: true
---

# Cross-Cutting Topics

Text+ hat die [Berliner
Erklärung](https://doi.org/10.5281/zenodo.3457213) zu
NFDI-Cross-Cutting Topics und die
[Leipzig-Berlin-Erklärung](https://doi.org/10.5281/zenodo.3895208) zu
NFDI-Cross-Cutting Topics der Infrastrukturentwicklung unterzeichnet.

Text+ wird zu folgenden übergreifenden Themen (Cross-Cutting Topics) durch den Austausch von Fachwissen und praktischer Erfahrung mit anderen NFDI-Konsortien beitragen:

**Datenkompetenz**: Text+ wird Schulungsveranstaltungen und Summer Schools für Nachwuchsforschende auf nationaler und internationaler Ebene organisieren, Plattformen zur Lehr- und Lernunterstützung bereitstellen sowie zur Entwicklung von Lehrplänen beitragen.

**Normdaten**: Text+ wird die Verwendung und Anreicherung fächerübergreifender Terminologien und Normdaten, insbesondere der Gemeinsamen Normdatei GND, aktiv unterstützen.

**Provenienz**: Text+ wird Lösungen für die Integration von Informationsquellen in Datenmodelle und Kurationspraktiken fördern und bei deren Entwicklung unterstützen, einschließlich ihrer vielfältigen technischen und rechtlichen Aspekte.

**Recht und Ethik**: Text+ wird Beratungsangebote zur Lizenzierung sowie zu Data Governance-Lösungen bereitstellen und für Datenverzerrung sensibilisieren.

**Rahmenbedingungen für die Daten-, Service- und Softwarequalität**: Text+ wird aktiv zur Entwicklung von Kurationskriterien und Qualitätsstandards für Forschungsdaten in den Geisteswissenschaften sowie zu den damit verbundenen Qualitätsmanagement-Prozessen, dem Qualitätsmanagement von Software und Services und dem Lifecycle-Management beitragen.

**Such- und Interoperabilitätslösungen**: Text+ wird eine föderierte Metadaten- und Dateninfrastruktur anbieten. Zu den wichtigen Diensten werden Hosting, Migration, Integration und Synchronisation sowie die Konvertierung von und nach RDF und die Verknüpfung mit Ressourcen in der Linked Open Data Cloud gehören.

**Technische Infrastrukturdienste**: Text+ wird Authentifizierungs- und Autorisierungsdienste, persistente Identifikatoren für Forschungsdaten, digitale Langzeitarchivierung von Forschungsdaten, Entwicklung und Einsatz von Text Data Mining-Techniken für sprach- und textbasierte Forschungsdaten anbieten.

