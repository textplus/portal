---
title: Cross-Cutting Topics and Standardization

aliases:
- /networking/standardization-committees
- /networking/cross-cutting-topics

menu:
  main:
    weight: 70
    parent: themen-dokumentation 

draft: true
---

## Cross-Cutting Topics
Text+ will contribute to the following cross-cutting topics by exchanging expertise and practical experience with other NFDI consortia:

**Data Competence**: Text+ will organize training events and summer schools for young researchers at the national and international levels, provide platforms for teaching and learning support, and contribute to curriculum development.

**Standardized Data**: Text+ will actively support the use and enrichment of interdisciplinary terminologies and standardized data, especially the Gemeinsame Normdatei (GND) (Integrated Authority File).

**Provenance**: Text+ will promote solutions for integrating information sources into data models and curation practices, providing support for their development, including diverse technical and legal aspects.

**Law and Ethics**: Text+ will offer advisory services on licensing and data governance solutions, raising awareness of data bias.

**Framework for Data, Service, and Software Quality**: Text+ will actively contribute to the development of curation criteria and quality standards for research data in the humanities, along with associated quality management processes, software and service quality management, and lifecycle management.

**Search and Interoperability Solutions**: Text+ will offer a federated metadata and data infrastructure. Key services will include hosting, migration, integration and synchronization, as well as conversion to and from RDF (Resource Description Framework) and linking with resources in the Linked Open Data Cloud.

**Technical Infrastructure Services**: Text+ will provide authentication and authorization services, persistent identifiers for research data, digital long-term archiving of research data, and the development and implementation of Text Data Mining techniques for language- and text-based research data.

### Technical Infrastructure Services

Text+ has signed the [Berlin Declaration](https://doi.org/10.5281/zenodo.3457213) on NFDI Cross-Cutting Topics and the [Leipzig-Berlin Declaration](https://doi.org/10.5281/zenodo.3895208) on NFDI Cross-Cutting Topics in infrastructure development. The humanities NFDI initiatives have specified their collaboration in a [Memorandum of Understanding](https://zenodo.org/record/4045000#.X3RHSmgzY2w).

## Standardization Committees

Members of Text+ actively participate in various committees and organizations that drive the development of recognized standards.

{{<image img="2020-06-30-Networking-Initiativen-Gremien-1024x576.png" alt="Overview of committees in which Text+ members are involved"/>}}

- [DIN – German Institute for Standardization](https://www.din.de/)
- [DINI – German Initiative for Networked Information](https://dini.de/) [(Letter of Support, PDF)](LoS-DINI.pdf)
- [IIIF – International Image Interoperability Framework](https://iiif.io/)
- [ISO – International Organization for Standardization](https://www.iso.org/)
- [RDA – Research Data Alliance](https://rd-alliance.org/) [(Letter of Support, PDF)](/networking/associations-alliances/letters/LoS-RDA.pdf)
- [TEI – Text Encoding Initiative](https://tei-c.org/)
- [W3C – World Wide Web Consortium](https://www.w3.org/)
