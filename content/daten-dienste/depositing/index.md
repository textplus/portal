---
title: Datenaufnahme

menu:
  main:
    weight: 35
    parent: daten-dienste

sidebar_image: datenaufnahme.jpg
---

# Forschungsdaten aufbewahren mit Text+

Im Rahmen der NFDI ist Text+ Anlaufstelle für text- und sprachbasierte Forschungsdaten und bietet der Wissenschaftscommunity die Möglichkeit der Übernahme von Daten. Diese werden dadurch nicht nur langfristig archiviert, sondern entsprechend der rechtlichen Möglichkeiten zur weiteren Nachnutzung bereitgestellt.  Der Zugang steht allen Forschenden mit einer akademischen Affiliation offen und unterstützt die Berücksichtigung datenschutz- und lizenzkonformer Zugriffsbedingungen.

Eines der [Text+ Zentren](/ueber-uns/daten-kompetenzzentren) übernimmt dafür die Daten und archiviert sie nachhaltig und interoperabel mit den Services und Werkzeugen von Text+. Die Text+ Zentren unterstützen in allen Belangen der Aufnahme von Forschungsdaten. Der Support reicht von der Aufbereitung, Auszeichnung und Übergabe der Forschungsdaten – inklusive der Beschreibung der Forschungsdaten mit Text+ spezifischen Metadaten (s. dazu auch die [weiterführenden Informationen](/daten-dienste/depositing/#weiterführende-informationen)) -- bis zur Auswahl eines geeigneten [Datenzentrums](/ueber-uns/daten-kompetenzzentren). Nutzen Sie gerne einfach das [Formular direkt im Anschluss](/daten-dienste/depositing/#datenaufnahme), um uns eine erste Beschreibung Ihrer Daten zukommen zu lassen. Unser Helpdesk-Team klärt dann im Dialog mit Ihnen alles Notwendige für eine Veröffentlichung.

{{<button url="/daten-dienste/depositing/#datenaufnahme" is_primary="true">}}Datenaufnahme in Text+ {{</button>}}
<br>

---

## Datenaufnahme

Mit den Angaben aus diesem einfachen Formular kann unser Helpdesk-Team einen Dialog mit Ihnen beginnen, an dessen Ende **veröffentlichte und nachnutzbare Daten** stehen. Füllen Sie aus, was Ihnen bereits jetzt klar ist; alles weitere klärt sich im Gespräch mit uns.

{{<data-submission-form>}}

---

## Vorteile der Archivierung von Daten in Text+

Forschungsdaten sind ein wertvolles Gut. Sie sind ein zentraler Bestandteil der eigenen wissenschaftlichen Arbeit sowie der aktuellen wissenschaftlichen Praxis.

Die Aufnahme und Bereitstellung von Forschungsdaten in Text+ erfolgt gemäß der [FAIR-Prinzipien](https://www.go-fair.org/fair-principles/) durch spezialisierte, zertifizierte [Text+ Zentren](/ueber-uns/daten-kompetenzzentren). Diese können Forschungsdaten nachhaltig – auch über 10 Jahre hinaus – aufbewahren und zur Nachnutzung bereitstellen. Über das zentrale Nachweissystem der [Registry](https://registry.text-plus.org/) stellt Text+ sicher, dass die Forschungsdaten innerhalb der Text+ Community – und darüber hinaus – sichtbar sind. Text+ bietet mit der [Föderierten Inhaltssuche (FCS)](/daten-dienste/daten/#föderierte-inhaltssuche) auch eine Möglichkeit, datenschutz- und lizenzkonform auf Daten zuzugreifen. Da Daten häufig mit weiteren Forschungsdaten verknüpft sind, garantiert Text+ als Teil der NFDI auch die disziplinenübergreifende Verknüpfung der Daten.

## Anschluss eines eigenen Archivs an Text+

Auch dies ist eine Möglichkeit: Ein eigenes Archiv stellt Forschungsdaten über Standardschnittstellen für Text+ zur Verfügung. Dazu wird die Nachhaltigkeit der Prozesse im Archiv in der Regel mit einem Zertifikat von Nestor oder dem Core Trust Seal dokumentiert. Mit den technischen Schnittstellen zur Infrastruktur von Text+ können diese Archive auch von den Text+ Software-Diensten profitieren.

Wenn Sie darüber nachdenken, mit einem eigenen Repositorium Teil des Text+ Netzwerks zu werden, kontaktieren Sie uns gerne über den [Helpdesk](/helpdesk).

{{<button url="/helpdesk/#kontaktformular" is_primary="true">}}zum Helpdesk {{</button>}}
<br>

## Weiterführende Informationen

{{<accordion>}}
{{<accordion-item title="Allgemeine Informationen zum Thema Dateningest">}}
**Der Ingest von Daten beinhaltet üblicherweise die folgenden Schritte:**

1. Sichten der Daten, Vorarbeiten zur Auszeichnung der Daten (Metadaten)
* Welche Datenformate liegen vor? Können proprietäre Formate durch offene Formate ersetzen werden?
* Mit welcher Thematik befassen sich die Daten (welches Datenzentrum ist fachlich am Besten geeignet)?
* Welchen Stand haben die Daten? Ist das Projekt beendet oder noch nicht? Gibt es eine Versionierung?
* Wer ist die Ansprechperson, die stellvertretend für die an der Datenerfassung und -auswertung Teilnehmenden sprechen kann?
* Wem gehören die Daten? Wo liegen die Rechte? Liegen Einschränkungen zur Nutzung der Daten vor (Datenschutz, Lizenzrechte)?
* Gibt es bereits eine Grundmenge von Metadaten, die für den Ingest genutzt werden kann bzw. eine Beschreibung der Forschungsdaten (z.B. ein Arbeitspapier, eine Publikation)?
* Wie hoch sind Aufwand und Kosten für die Archivierung?

2. Datenaufbereitung und Anreicherung der Daten mit Metadaten
* Erzeugung eines gut strukturierten Dateienbaums; Entfernung von Dubletten und temporären Dateien; Formatkonvertierung in für die Langzeitarchivierung (LZA) geeignete Formate
* In Kooperation mit dem aufnehmenden Datenzentrum: Ergänzung fehlender Metadaten, Überführung in maschinenlesbares Format unter Nutzung eines Datenschemas.

3. Aufnahme in das Archivsystem (Technischer Ingest)
Erzeugung/Ergänzung technischer Metadaten: Strukturmetadaten, Kopierschutzprüfung, Validitätsprüfung, Prüfsummen etc.

4. Langzeitkonservierungsvorarbeiten und Wartung
* Semantic preservation vs. Bitstream preservation
* Aktualisierung von Metadaten (z.B. Ansprechperson; Änderung der Rechte)
* Formatkonvertierung (z.B. auf neueste Audio/Videoformate)
{{</accordion-item>}}

{{<accordion-item title="Informationen zu Metadaten">}}

Die Auffindbarkeit und Wiederverwendbarkeit von Forschungsdaten erfordert ihre bestmögliche Beschreibung mit Metadaten. In Text+ sollten alle Forschungsdaten zumindest mit den Beschreibungsmitteln aus [Datacite](https://datacite-metadata-schema.readthedocs.io/en/4.5/properties/) ausgezeichnet werden. Im Wesentlichen sollten Sie Ihre Forschungsdaten mithilfe dieser 20 Felder beschreiben:

| Nr | Feld     | Beschreibung | Obligation |
| -------- | -------- | ------- | ------- |
| 1. | *Identifier*  | eindeutiger Bezeichner   | M |
| 2. | *Creator* | Urheber der Ressource    | M |
| 3. | *Title*    | Titel der Ressource    | M |
| 4. | *Publisher* | Herausgeber der Ressource |  M |
| 5. | *PublisherYear* | Zeitpunkt der Veröffentlichung | M |
| 6. | *ResourceType* | Art der Ressource |  M |
| 7. | *Subject* |Gegenstandsbereich | R |
| 8. | *Contributor* | Mitwirkende bei der Entstehung | R |
| 9. | *Date* | Datum der Entstehung | R |
| 10. | *Language* | In welcher Sprache liegt die Ressource vor | O |
| 11. | *AlternateIdentifier* | Alternative Bezeichner, falls vorhanden | O |
| 12. | *RelatedIdentifier* | Bezeichner von verwandten Ressourcen | R |
| 13. | *Size* | Grössenbeschreibung | O |
| 14. | *Format* | In welchen Formaten liegt die Ressource vor | O |
| 15. | *Version* | Versionierung | O |
| 16. | *Rights* | Lizenz- Urheberrechtliches| O |
| 17. | *Description* | Beschreibung der Ressource im Klartext | R |
| 18. | *Geolocation* | Verortung der Ressource| R|
| 19. | *FundingsReference* | Förderkennzeichen| O |
| 20. | *RelatedItem* | Verwandte Ressourcen| O |

Beachten Sie, dass die ersten sechs Felder verpflichtend sind (*M = Mandatory*), einige Felder empfohlen werden (*R = Recommended*), andere hingegen nur optional sind (*O = Optional*).

Viele Partner in Text+ zeichnen Ihre Forschungsdaten mit weiteren – oft Ressourcen-typischen – Merkmalen aus. Auch innerhalb der Datendomänen in Text+ gibt es eine Vielfalt von Beschreibungssystemen, z.B. um lexikalische Ressourcen auszuzeichnen. Innerhalb des Konsortiums wurde deshalb für jede Datendomäne (Lexikalische Ressourcen, Collections, Editionen) der gemeinsame Kern aller Metadatenbeschreibungen herausgearbeitet. Zur Orientierung können Sie die entsprechende Dokumentationen konsultieren:

* Collections: [Konzept und prototypische Implementierung beispielhafter Ressourcen](https://zenodo.org/records/12771256)
* Editions: [Datenmodell Editionenregistry (Text+)](https://zenodo.org/records/12799883)
* Lexikalische Ressourcen: *(folgt)*

{{</accordion-item>}}

{{<accordion-item title="Publikationen">}}
* Wohin damit? So kommen Ihre Forschungsdaten in die Text+ Infrastruktur. <https://zenodo.org/doi/10.5281/zenodo.10036031>
* How-to "Data Depositing in Text+": Your research data's way into the Text+ infrastructure. <https://zenodo.org/doi/10.5281/zenodo.11618653>
* Leitlinie für das Integrieren von Daten in Text+/NFDI <https://zenodo.org/doi/10.5281/zenodo.12744055>
{{</accordion-item>}}
{{</accordion>}}
