---
# just a dummy file to set the title and menu

title: "Data and Services"

menu:
  main:
    identifier: "daten-dienste"
    weight: 10

build:
  render: never
---
