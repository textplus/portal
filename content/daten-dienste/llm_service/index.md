---
title: LLM Service

menu:
  main:
    weight: 50
    parent: daten-dienste 


params:
  last_mod_today: true
---

# LLM Service
Mit ihren Forschungsdaten bieten die text- und sprachbasierten Geisteswissenschaften vielfältige Anwendungsfälle für den Einsatz großer Sprachmodelle (Large Language Models, LLMs). Text+ erweitert den Zugang zu solchen Forschungsdaten über die Registry, die Federated Content Search (FCS) sowie über die Datenzentren und Repositorien der beitragenden Partner. 

Hinzu kommt nun die Bereitstellung eines Web-Services für die Open Source LLMs (Meta) LLaMA, Mixtral, Qwen und Codestral sowie Chat-GPT von OpenAI. Dies wird durch die GWDG ermöglicht, die als Nationales Hochleistungsrechen- und KI-Zentrum die Entwicklung und Erprobung forschungsbezogener KI-Anwendungsfälle in Text+ unterstützt.

## Wer kann den LLM Service wie nutzen?
Vorerst alle unmittelbar am Projekt beteiligten Personen nach einem Login per Academic Cloud. Eine Erweiterung der User Base ist geplant, hat jedoch vorerst lizenzrechtliche Grenzen. Außer den beiden extern eingebundenen Chat-GPT Modellen werden die LLMs auf Servern der GWDG gehostet, so dass keine Daten nach extern abfließen.

## Vorteile
* Kostenlose Nutzung von diversen Open-Source-Modellen
* Kostenlose Nutzung von OpenAI GPT-4
* KI-Chat ohne serverseitige Speicherung des Chatverlaufs (außer Chat-GPT)
* Managed Hosting eigener Sprachmodelle
* Finetuning von LLMs auf eigenen Daten
* Retrieval-Augmented Generation (RAG) auf eigenen Dokumenten
* Einhaltung gesetzgeberischer Vorgaben und insbesondere auch der Datenschutzinteressen der Nutzenden

{{<button url="https://text-plus.org/chatbot" is_primary="true">}}LLM Service starten{{</button>}}

---

## Feedback erwünscht
Diese Einbindung von LLMs hat trotz der beschriebenen Vorteile den Status eines *ersten Angebots*, das über Zeit sowohl im Funktionsumfang wie auch in der Zugänglichkeit  ergänzt und verbessert werden soll. Feedback zur aktuellen Version ist daher ausdrücklich erwünscht! Senden Sie es uns gerne mittels des [Kontaktformulars](https://text-plus.org/helpdesk/).

## Anwendung in Text+ 
Mit Blick auf die Text+ Datendomänen - Editionen, Collections, Lexikalische Ressourcen - wird derzeit an den folgenden Anwendungszenarien gearbeitet:
- Datenpreprocessing bspw. mittels Named Entity Recognition (NER): Verschiedene NEs taggen bzw. preprocessing für NER, um anschließend speziell trainierte NER-Modelle auf die Daten anzuwenden/trainieren.
- Laufzeitumgebung: GPU-unterstützte Laufzeitumgebung für Dockercontainer. Möglichkeit der Öffnung von Ports nach außen für das Bereitstellen von APIs.
- Entity Linking: Retrieval Augmented Generation (RAG) mit Kontextwissen aus Wikidata, der GND und anderen Quellen. Voraussichtlich zusätzliche Datenbank/Suchmaschine für die Kontexte: Graph-Datenbank (z.B. BlazeGraph) oder ElasticSearch.
- Query Generation: Federated Content Search, d.h. Queryformulierung basierend auf natürlichsprachlichen Beschreibungen (evtl. Konversation im Chat zur Verfeinerung der Anforderung, anschließend Option zum Übernehmen der generierten Query in die FCS zur tatsächlichen Suche).
- Generierung von Beispielsätzen/Kontext: GermaNet-Einträge verbessern z.B. Beispielsätze für einen lexikalischen Eintrag generieren.
- Generierung von historischen Normalisierungen: seq2seq-Transformer-Modelle für historische Normalisierung (ggf. auch als Webservice).
- MONAPipe, APIs für Komponenten: neuronale Modelle (z.B. Redewiedergabe, Ereigniserkennung) als API zur Verfügung stellen.

## Generelle Anwendungsszenarien im Kontext sprach- und textbasierter Forschungsdaten
Im Folgenden sind einige Anwendungsfälle skizziert, die zeigen, wie LLMs generell in den text- und sprachbasierten Geisteswissenschaften als leistungsstarke Werkzeuge eingesetzt werden können, um die Forschung zu unterstützen und neue Erkenntnisse zu gewinnen. Dabei ist es wichtig, ethische Überlegungen und die Einhaltung von Datenschutzbestimmungen zu berücksichtigen, insbesondere im Umgang mit sensiblen oder urheberrechtlich geschützten Daten.
- Textanalyse und Textmining: LLMs können für Inhaltsanalyse genutzt werden, um große Textmengen systematisch zu analysieren und Themen, Motive oder stilistische Merkmale herauszuarbeiten. Beispielsweise können literarische Werke, historische Dokumente oder philosophische Texte automatisch auf wiederkehrende Themen, Sentiments oder sprachliche Muster untersucht werden. In umfangreichen textuellen Datenbeständen können LLMs historische Veränderungen von Begriffen und Ideen nachverfolgen oder Diskurse in verschiedenen Zeiträumen und Kulturen analysieren.
- Automatische Annotation und Kategorisierung: LLMs können verwendet werden, um Texte automatisch mit relevanten Metadaten zu versehen, wie etwa Schlagwörtern, Abstracts oder Klassifikationen. Das erleichtert die Erschließung und Wiederverwendung von Texten in Repositorien. Sie können dabei helfen, Daten in großen Repositorien nach thematischen, geographischen oder zeitlichen Kriterien zu sortieren und zu organisieren, was die Zugänglichkeit und Nutzung für Forscher erleichtert.
- Spracherkennung und Übersetzung: Für interdisziplinäre und internationale Forschungsprojekte können LLMs eingesetzt werden, um Texte aus verschiedenen Sprachen maschinell zu übersetzen, was den Zugang zu internationalen Forschungsergebnissen und Quellen erheblich erleichtert. LLMs könnten eingesetzt werden, um regionale Sprachvarianten, historische Sprachstufen oder dialektale Unterschiede zu identifizieren und zu analysieren.
- Generierung von Forschungshypothesen: LLMs können verwendet werden, um aus großen Mengen wissenschaftlicher Literatur zentrale Forschungsfragen und -hypothesen herauszuarbeiten, die als Grundlage für neue Studien dienen können (automatisierte Literaturübersichten). Durch die Analyse existierender Forschungsliteratur können LLMs Bereiche identifizieren, die bisher wenig untersucht wurden, und so neue Forschungsthemen vorschlagen.
- Analyse von multimodalen Daten: In Fällen von engen Verbindung zwischen Texten und visuellen Materialien (z.B. Manuskripte etc.)können LLMs in Kombination mit Bildanalysemodellen genutzt werden, um solche multimodalen Daten zu analysieren, z.B. durch die Analyse von Bildbeschreibungen oder die Verknüpfung von Texten mit entsprechenden visuellen Darstellungen.
- Historische Forschung: LLMs können helfen, historische Texte zu transkribieren, zu annotieren und in digitale Formate zu überführen, was die Erstellung und Analyse digitaler Editionen erleichtert. Durch die Analyse historischer Textkorpora können LLMs dazu beitragen, Diskursverläufe nachzuvollziehen und die Entwicklung von Ideen und Begriffen in der Geschichte zu erforschen.
- Unterstützung bei der Erstellung von Forschungsarbeiten: LLMs können verwendet werden, um erste Entwürfe oder Zusammenfassungen von wissenschaftlichen Arbeiten zu generieren, was besonders hilfreich sein kann, um Schreibblockaden zu überwinden oder große Datenmengen zu verarbeiten. Sie können genutzt werden, um passende Zitate und Literaturquellen basierend auf dem Textkontext vorzuschlagen und so den Schreibprozess effizienter zu gestalten.

### Changelog
* Februar 2025: Arbeiten an der Benutzeroberfläche, Einbindung ins Portal abgeschlossen
* Oktober 2024: initiale Bereitstellung des Dienstes mit Anbindung an die Academic Cloud sowie Hostinginfrastruktur