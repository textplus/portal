---
title: Daten

menu:
  main:
    weight: 20
    parent: daten-dienste
  
aliases:
- /daten-dienste/suche/#föderierte-inhaltssuche
- /daten-dienste/suche/
---

# Daten

Text+ richtet sich an alle Forschenden, die mit Text- und Sprachdaten im weitesten Sinn arbeiten, einschließlich, aber nicht beschränkt auf Linguistik, Literaturwissenschaft, Philologien auch der sog. „Kleinen Fächer“, Philosophie sowie sprach- und textbasierte Forschung in den Sozialwissenschaften und der Politikwissenschaft.

Die an Text+ beteiligten Institutionen stellen umfangreiche Ressourcen zur Nachnutzung bereit. Für den Nachweis und die Aufnahme von text- und sprachbasierten Forschungsdaten setzt der Verbund auf eine etablierte Infrastruktur. Eine Suche über den kompletten Bestand an Ressourcen ermöglicht das übergreifende Informations- und Nachweissystem der [Text+ Registry](https://registry.text-plus.org). Für eine Volltextsuche innerhalb der Text- und Sprachdaten steht die [Föderierte Inhaltssuche (FCS)](/daten-dienste/daten/#suche-in-den-forschungsdaten--die-föderierte-inhaltssuche) bereit.

Beide Dienste werden das Potenzial für verbesserte Precision & Recall von Ergebnissen durch die Nutzung des kontrollierten Vokabulars der [Gemeinsamen Normdatei (GND)](https://gnd.network/) nutzen. Durch die Verwendung der GND-Normdaten werden alle Einträge, die aufeinander Bezug nehmen, in der [Registry](/daten-dienste/daten/#suche-nach-ressourcen--die-text-registry) übergreifend verknüpft. In der [Förderierten Inhaltssuche (FCS)](/daten-dienste/daten/#suche-in-den-forschungsdaten--die-föderierte-inhaltssuche) bieten potentiell zehn Millionen Entitäten der GND eindeutige Sucheinstiege und verbesseren so die Suchergebnisse. Eine umfassende Verknüpfung der gelieferten Daten mit GND-IDs schöpft dieses Potential voll aus und steigert die Sichtbarkeit der Inhalte. Zusätzliche GND-Identifier zu Entitäten, die noch nicht in der GND enthalten sind, können über die [Text+ GND Agentur](/themen-dokumentation/standardisierung/gnd-agentur/) neu angesetzt und bestehende editiert werden.

{{<horizontal-line>}}

&nbsp;

## Suche nach Ressourcen – Die Text+ Registry
Die [Text+ Registry](https://registry.text-plus.org) ist ein übergreifendes Informations- und Nachweissystem, das einen einfachen Zugang zu Ressourcen bietet, die für Forschung und Lehre im Bereich sprach- und textbasierter Disziplinen relevant sind.

Unter Berücksichtigung der Bedarfe der jeweiligen Fachcommunites verzeichnet die Registry Korpora und Textsammlungen, Editionen sowie lexikalische Ressourcen. Sie aggregiert diese aus den verschiedenen Text+ Zentren und führt sie in einer zentralen Übersicht zusammen.

Für Editionen fungiert die Text+ Registry als offenes Verzeichnis: Zum einen werden einschlägige Kataloge und Nachweissysteme eingebunden, zum anderen können seitens der Community gedruckte, hybride und digitale Editionen manuell eingetragen und somit auffindbar gemacht werden – unabhängig davon, ob sie ein Text+ Zentrum bereitstellt.

Durch die Verwendung verschiedener Entitäten (z.B. Personen via GND) werden alle verzeichneten Ressourcen miteinander vernetzt und in Beziehung gesetzt. Indem Daten angereichert und über Schnittstellen verfügbar gemacht werden, ist eine übergreifende Anschlussfähigkeit an Entwicklungen der NFDI (z.B. Wissensgraphen) gewährleistet.

{{<button url="https://registry.text-plus.org/" is_primary="true">}} Zur Text+ Registry {{</button>}}

&nbsp;

<!-- Hier am besten ein Hinweis auf die Userdoku zur Registry, die bis zum 30.9. vorliegen soll. 
https://registry.text-plus.org/docs/ -->
<!--{{<accordion>}}
  {{<accordion-item title="Weitere Informationen">}}
  - Dokumentation erfolgt in Kürze
  {{</accordion-item>}}
{{</accordion>}} -->

{{<horizontal-line>}}

&nbsp;

## Suche in den Forschungsdaten – Die Föderierte Inhaltssuche
Die Suche nach Text- und Sprachdaten beruht auf der [Federated Content Search](https://www.clarin.eu/content/content-search) (dt. Föderierte Inhaltssuche, kurz: FCS). Dabei handelt es sich um eine etablierte Suchplattform, die den Zugriff auf verteilte Ressourcen unter Verwendung einer gemeinsamen Spezifikation von technischen Schnittstellen, Datenformaten und Abfragesprachen ermöglicht. Das Framework wurde im Rahmen des europäischen CLARIN-Projekts entwickelt und basiert auf Standardprotokollen wie [OASIS searchRetrieve](https://docs.oasis-open.org/search-ws/searchRetrieve/v1.0/os/part0-overview/searchRetrieve-v1.0-os-part0-overview.html) und der [Contextual Query Language (CQL)](https://www.loc.gov/standards/sru/cql/).

Im Rahmen von Text+ werden verschiedene Erweiterungen der FCS entwickelt. Dies umfasst die verbesserte Unterstützung der Abfrage von [Lexikalischen Ressourcen](/ueber-uns/lexikalische-ressourcen/), die Verknüpfung und Suche von Entitäten, sowie die vereinfachte Integration der FCS in neuen Anwendungskontexten.

Die Suchplattform kann sowohl als eigenständige Web-Anwendung ([FCS Aggregator](https://fcs.text-plus.org/)) als auch über die Integration in der Text+ Webseite genutzt werden.

{{<search-button is_data_search="true">}} Inhaltssuche öffnen {{</search-button>}}

&nbsp;

{{<accordion>}}
  {{<accordion-item title="Weitere Informationen">}}
  - [Text+](https://gitlab.gwdg.de/textplus/ag-fcs-documents/-/blob/main/awesome-fcs.md) und [CLARIN](https://github.com/clarin-eric/awesome-fcs) _Awesome FCS_ Listen (Linksammlung)
  - [Eintrag im SSH Open Marketplace](https://marketplace.sshopencloud.eu/tool-or-service/VFhPbo)
  - [Technische Informationen](https://clarin-eric.github.io/fcs-misc/) wie die offiziellen FCS-Spezifikationen und Anleitungen für die Endpunkt-Entwicklung
  {{</accordion-item>}}
{{</accordion>}}


{{<horizontal-line>}}

&nbsp;

## Suche innerhalb der Webseite

Zur Suche nach Inhalten dieser Webseite wählen Sie in der Kopfzeile der Suchmaske „Webseite“ aus. Es stehen folgende Filter zur Verfügung: _Webseitenkategorien_, _Task Areas_ und _DFG Fachgebiete_. Suchergebnisse aktualisieren sich während der Eingabe und sind entsprechend der Webseitenstruktur gegliedert. Ein Kettensymbol neben einem Ergebnis bedeutet, dass es sich hierbei um einen direkten Link auf eine konkrete Trefferseite handelt. Mit einer Zahl wird angezeigt, wie viele Treffer in einer Kategorie gefunden wurden. Der Eintrag lässt sich durch einen Klick aufklappen, so gelangen Sie zu den einzelnen Trefferseiten.

{{<image img="SuchbeispielDE.png" alt="Beispielsuche auf der Webeite, Suchbegriff Briefe, Filter Task Area Collections"/>}}

{{<search-button>}} Webseitensuche öffnen {{</search-button>}}

{{<horizontal-line>}}