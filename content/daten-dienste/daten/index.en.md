---
title: Data

menu:
  main:
    weight: 20
    parent: daten-dienste

aliases:
- /en/daten-dienste/suche/#federated-content-search
- /en/daten-dienste/suche/
---

# Data

Text+ is a consortium of the National Research Data Infrastructure (NFDI) in Germany and supports all researchers who work with text and language data in a broader sense, including but not limited to linguistics, literary studies, philology, the so-called “minor subjects”, philosophy and language and text-based research in the social sciences and political science.

The institutions involved in Text+ provide extensive resources for research. As a Data Space, it relies on an established infrastructure for the provision and inclusion of text- and language-based research data. Our comprehensive information and reference system in form of the [Text+ Registry](https://registry.text-plus.org) enables a search across the entire stock of resources. The [Federated Content Search (FCS)](/en/daten-dienste/daten/#federated-content-search) provides a full-text search within the text and language data.
Both services will utilize the potential for improved precision & recall of results by using the controlled vocabulary of the [Integrated Authority File (GND)](https://gnd.network/). By using the GND authority data, all entries that refer to each other are linked in the [Registry](/en/daten-dienste/daten/#search-for-resources---the-text-registry). In the [Federated Content Search (FCS)](/en/daten-dienste/daten/#federated-content-search), potentially ten million GND entities offer unique search entries and thus improve the search results. A comprehensive linking of the supplied data with GND IDs fully exploits this potential and increases the visibility of the content. Additional GND identifiers for entities that are not yet included in the GND can be added and existing ones edited via the [GND Agency Text+](/en/themen-dokumentation/standardisierung/gnd-agentur/).

{{<horizontal-line>}}

&nbsp;

## Search for resources - The Text+ Registry
The [Text+ Registry](https://registry.text-plus.org) is a comprehensive information and reference system that provides easy access to resources relevant for research and teaching across the spectrum of language and text-based research disciplines.

The registry lists corpora and text collections, editions and lexical resources, taking into account the needs of the respective subject communities. It aggregates resources from all Text+ centers and brings them together as a central hub.

For editions, the Text+ Registry functions as an open register: On the one hand, relevant catalogues and reference systems are integrated. On the other hand, printed, hybrid and digital editions can be entered manually on the part of the community and thus made findable - regardless of whether they are provided by a Text+ Center.

By using different entities (e.g. persons via GND), all recorded resources are linked and related to each other. By enriching data and making it available via interfaces, comprehensive connectivity to NFDI wide services (e.g. knowledge graphs) is guaranteed.


{{<button url="https://registry.text-plus.org/" is_primary="true">}} Text+ Registry {{</button>}}

&nbsp;

<!-- Hier am besten ein Hinweis auf die Userdoku zur Registry, die bis zum 30.9. vorliegen soll. 
https://registry.text-plus.org/docs/ -->
<!--{{<accordion>}}
  {{<accordion-item title="Weitere Informationen">}}
  - Dokumentation erfolgt in Kürze
  {{</accordion-item>}}
{{</accordion>}} -->

{{<horizontal-line>}}

&nbsp;

## Federated Content Search

The search for text and language data uses the [Federated Content Search (FCS)](https://www.clarin.eu/content/content-search), an established search platform that allows accessing distributed resources using a common specification of technical interfaces, data formats, and query languages. The framework was originally developed in the European CLARIN project and is based on standard protocols like [OASIS searchRetrieve](https://docs.oasis-open.org/search-ws/searchRetrieve/v1.0/os/part0-overview/searchRetrieve-v1.0-os-part0-overview.html) and the [Contextual Query Language (CQL)](https://www.loc.gov/standards/sru/cql/).

Various extensions are under development as part of Text+. These include the extended support of [Lexical Resources](/en/ueber-uns/lexikalische-ressourcen/), entity search, as well as the simplified integration of FCS in new application contexts.

The search platform can be used both as a stand-alone web application ([FCS Aggregator](https://fcs.text-plus.org/)) and via the integration into the Text+ website.

{{<search-button is_data_search="true">}} Open Content Search {{</search-button>}}


{{<accordion>}}
  {{<accordion-item title="Weitere Informationen">}}
  - [Text+](https://gitlab.gwdg.de/textplus/ag-fcs-documents/-/blob/main/awesome-fcs.md) and [CLARIN](https://github.com/clarin-eric/awesome-fcs) _Awesome FCS_ lists (collections of links)
  - [Service entry in the SSH Open Marketplace](https://marketplace.sshopencloud.eu/tool-or-service/VFhPbo)
  - [Technical information](https://clarin-eric.github.io/fcs-misc/) such as specifications and instructions for FCS endpoint development
 {{</accordion-item>}}
{{</accordion>}}


{{<horizontal-line>}}

&nbsp;

## Search within the website

To search for content on this website, select “Website” in the header of the search mask. The following filters are available: _Website Categories_, _Task Areas_ and _DFG Subject Areas_. Search results are updated as you type and are organized according to the website structure. A chain symbol next to a result means that this is a direct link to a specific hit page. A number indicates how many hits have been found in a category. The entry can be expanded by clicking on it to access the individual hit pages.

{{<image img="SuchbeispielENG.png" alt="Example search on the website, search term letters, filter task area Collections"/>}}

{{<search-button>}} Open website search {{</search-button>}}

{{<horizontal-line>}}
