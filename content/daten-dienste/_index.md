---
# just a dummy file to set the title and menu

title: "Daten und Dienste"

menu:
  main:
    identifier: "daten-dienste"
    weight: 10

build:
  render: never
---
