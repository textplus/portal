---
title: Consulting

menu:
  main:
    weight: 60
    parent: daten-dienste
---

# Text+ Consulting
In its definition and scope, we understand the concept of consulting to be broader than simply answering technical questions, as is often the case with traditional helpdesks.

Text+ Consulting provides comprehensive advice from a team of agents on all questions relating to text and language-based research data – from consultation on applications to general questions on research data management, from the discussion of research ideas and processes to specialised questions on specific tools and technologies across the entire data lifecycle. The purpose of the consulting services is to improve data quality in line with the FAIR principles, to propagate standardised, sustainable research practices and thus, ultimately, to increase the potential of your research projects.

Below you find various ways in on how to get in touch with us.

---
## Helpdesk
Our [helpdesk](helpdesk/) is a central point of entry into the consulting network of both Text+ and the NFDI. As part of the development of the NFDI under the guiding principle of “One NFDI”, the helpdesks of various consortia are becoming more and more interconnected in order to coordinate expertise and provide seamless support for technical and cross-disciplinary consulting cases. 
Text+ is already collaborating with the humanities partner consortia [NFDI4Culture](https://nfdi4culture.de/helpdesk.html), [NFDI4Memory](https://4memory.de/) and [NFDI4Objects](https://www.nfdi4objects.net/index.php/en/help-desk) in this regard.

---
## Community Activities
Requests for consultation, however, do not only find us via the helpdesk but also in the context of our [Community Activities](aktuelles/veranstaltungen/). Thus, simply approach us during events, e.g. at conferences or workshops. Your enquiry will be relayed. 

---
## Research Rendezvous 
### The open consulting hour of Text+

Our services are rounded off by the [Text+ Research Rendezvous](https://events.gwdg.de/category/208/), where concerns and questions about Text+ and its services, the NFDI and text- and language-related research data can be discussed directly with colleagues from Text+ without prior registration. Individual (follow-up) appointments can be arranged for more extensive questions. It is not necessary to register for consultation hours. Access to the consultation is possible during the entire hour.

The Research Rendezvous are held fortnightly, alternating between Tuesday (9:00–10:00) and Thursday (10:00–11:00).
We are looking forward to hearing from you!
