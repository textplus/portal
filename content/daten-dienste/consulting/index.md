---
title: Beratung

menu:
  main:
    weight: 60
    parent: daten-dienste
---

# Text+ Consulting
In seiner Definition und seinem Umfang verstehen wir den Begriff Consulting breiter als die kurze Beantwortung technisch geprägter Fragen, wie sie häufig über klassische Helpdesks abgewickelt werden.

Das Text+ Consulting leistet eine vollumfängliche Beratung durch ein Team von Agent:innen zu allen Fragen rund um text- und sprachbasierte Forschungsdaten – von der Antragsberatung über allgemeine Fragen zum Forschungsdatenmanagement, von der Diskussion von Forschungsideen und -prozessen bis hin zu Spezialfragen zu bestimmten Tools und Technologien über den gesamten Datenlebenszyklus hinweg. Ziel der Beratungen ist die Verbesserung der Datenqualität im Sinne der FAIR-Prinzipien, die Verbreitung standardisierter, nachhaltiger Forschungspraktiken und somit letztlich die Erhöhung der Erfolgsaussichten Ihrer Forschungsvorhaben.

Im Folgenden finden Sie verschiedene Möglichkeiten, wie Sie mit uns in Kontakt treten können.

---
## Helpdesk
Unser [Helpdesk](helpdesk/) stellt einen wesentlichen Einstiegspunkt in das Beratungsnetzwerk sowohl von Text+ als auch der NFDI dar. Im Zuge des Aufbaus der NFDI unter dem Leitgedanken „One NFDI” vernetzen sich die Helpdesks verschiedener Konsortien zunehmend, um Expertise abzustimmen sowie technisch und fachlich übergreifende Beratungsfälle nahtlos unterstützen zu können. 
Text+ arbeitet diesbezüglich bereits mit den geisteswissenschaftlichen Partnerkonsortien [NFDI4Culture](https://nfdi4culture.de/helpdesk.html), [NFDI4Memory](https://4memory.de/) und [NFDI4Objects](https://www.nfdi4objects.net/index.php/en/help-desk) zusammen.

---
## Community Activities
Beratungsanfragen erreichen uns allerdings nicht nur über den Helpdesk, sondern vor allem im Rahmen unserer [Community Activities](aktuelles/veranstaltungen/). Sprechen Sie uns einfach im Rahmen von Veranstaltungen, bspw. auf Tagungen oder Workshops an. Ihre Anfrage wird weitergeleitet. 

---

## Research Rendezvous 
### Die offene Sprechstunde von Text+

Abgerundet wird unser Angebot durch die [Text+ Research Rendezvous](https://events.gwdg.de/category/208/), in denen ohne vorherige Anmeldung Anliegen und Fragen rund um Text+ und dessen Services, die NFDI sowie zu text- und sprachbezogenen Forschungsdaten direkt mit Kolleg:innen aus Text+ besprochen werden können. Für umfangreichere Fragen werden gerne individuelle (Folge-)Termine vereinbart. Eine Anmeldung zur Sprechstunde ist nicht erforderlich. Ein Zutritt zur Sprechstunde ist während der gesamten Stunde möglich.

Die Sprechstunden finden zweiwöchentlich statt im Wechsel von Dienstag (9:00–10:00) und Donnerstag (10:00–11:00). 
Wir freuen uns auf Sie!
