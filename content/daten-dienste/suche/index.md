---
title: Suche

menu:
  main:
    weight: 50
    parent: daten-dienste


draft: true
---

# Suchen in Text+

Die Text+ Suchfunktionalität über den Button „Suche“ bietet zwei Optionen: Zum einen die Suche nach Inhalten auf der Text+ Webseite, zum anderen die föderierte Inhaltssuche in einer Vielzahl an linguistischen Daten. Über die Reiter in der Kopfzeile der Suchmaske können Sie zwischen diesen beiden Optionen wechseln.

{{<image img="SucheDeuTopBar.png" alt="Kopfzeile der Suchmaske mit den Reitern Webseite und Daten"/>}}

## Suche innerhalb der Webseite

Zur Suche nach Inhalten dieser Webseite wählen Sie in der Kopfzeile der Suchmaske „Webseite“ aus. Für die Suche stehen folgende Filter zur Verfügung: _Webseitenkategorien_, _Task Areas_ und _DFG Fachgebiete_. Suchergebnisse aktualisieren sich während der Eingabe und sind entsprechend der Webseitenstruktur gegliedert. Ein Kettensymbol neben einem Ergebnis bedeutet, dass es sich hierbei um einen direkten Link auf eine konkrete Trefferseite handelt. Mit einer Zahl wird angezeigt, wie viele Treffer in einer Kategorie gefunden wurden. Der Eintrag lässt sich durch einen Klick aufklappen, so gelangen Sie zu den einzelnen Trefferseiten.

{{<image img="SuchbeispielDE.png" alt="Beispielsuche auf der Webseite, Suchbegriff Briefe, Filter Task Area Collections"/>}}

{{<search-button>}} Webseitensuche öffnen {{</search-button>}}

## Föderierte Inhaltssuche
Die Suche nach Text- und Sprachdaten beruht auf der [Federated Content Search](https://www.clarin.eu/content/content-search) (dt. Föderierte Inhaltssuche, kurz: FCS). Dabei handelt es sich um eine etablierte Suchplattform, die den Zugriff auf verteilte Ressourcen unter Verwendung einer gemeinsamen Spezifikation von technischen Schnittstellen, Datenformaten und Abfragesprachen ermöglicht. Das Framework wurde im Rahmen des europäischen CLARIN-Projekts entwickelt und basiert auf Standardprotokollen wie [OASIS searchRetrieve](https://docs.oasis-open.org/search-ws/searchRetrieve/v1.0/os/part0-overview/searchRetrieve-v1.0-os-part0-overview.html) und der [Contextual Query Language (CQL)](https://www.loc.gov/standards/sru/cql/).

Im Rahmen von Text+ werden verschiedene Erweiterungen der FCS entwickelt. Dies umfasst die verbesserte Unterstützung der Abfrage von [Lexikalischen Ressourcen](/ueber-uns/lexikalische-ressourcen/), die Verknüpfung und Suche von Entitäten, sowie die vereinfachte Integration der FCS in neuen Anwendungskontexten.

Weitere Informationen finden Sie hier:

* [Text+](https://gitlab.gwdg.de/textplus/ag-fcs-documents/-/blob/main/awesome-fcs.md) und [CLARIN](https://github.com/clarin-eric/awesome-fcs) _Awesome FCS_ Listen (Linksammlung)
* [Eintrag im SSH Open Marketplace](https://marketplace.sshopencloud.eu/tool-or-service/VFhPbo)
* [Technische Informationen](https://clarin-eric.github.io/fcs-misc/) wie die offiziellen FCS-Spezifikationen und Anleitungen für die Endpunkt-Entwicklung

Die Suchplattform kann sowohl als eigenständige Web-Anwendung ([FCS Aggregator](https://fcs.text-plus.org/)) als auch über die Integration in der Text+ Webseite genutzt werden.

{{<search-button is_data_search="true">}} Inhaltssuche öffnen {{</search-button>}}
