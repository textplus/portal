---
title: Dienste

type: non-scrolling-toc

menu:
  main:
    weight: 30
    parent: daten-dienste 


params:
  last_mod_today: true
---

# Dienste und Werkzeuge

Text+ und die beteiligten Einrichtungen stellen eine Vielzahl von Angeboten rund um Sprach- und Textdaten zur Verfügung. Neben den Forschungsdaten sind Dienste und Werkzeuge ein wesentlicher Teil des Text+ Angebots für die Nutzenden.

Für die Bereitstellung und Pflege dieser Dienste und Werkzeuge nutzt Text+ den [SSH Open Marketplace](https://marketplace.sshopencloud.eu/), eine Discovery-Plattform aus dem Social Sciences and Humanities Cluster in der [EOSC](https://research-and-innovation.ec.europa.eu/strategy/strategy-2020-2024/our-digital-future/open-science/european-open-science-cloud-eosc_en). Der SSH Open Marketplace stellt seine Informationen in englischer Sprache zur Verfügung, die durch Text+ nicht übersetzt werden.

Die dargestellte Angebotsliste unterliegt ständiger Weiterentwicklung. Für einen komfortablen Zugriff ist sowohl eine Suche wie auch eine Filterfunktion implementiert. Feedback und Wünsche können an den [Helpdesk](/helpdesk/) mit dem Betreff Infrastruktur/Betrieb gerichtet werden.

{{< standout theme="white" col_width="9">}}
{{<service-list>}}
{{< /standout >}}

## Richtlinien für die Beschreibung von Text+ Diensten im SSH Open Marketplace

Diese kurze [Anleitung](https://zenodo.org/records/12657204) beschreibt, wie Dienste, die mit Text+ verbunden sind, im SSH Open Marketplace dargestellt werden können.

## Servicedefinition

Die Sammlung von Ressourcen auf dieser Webseite umfasst neben genuinen Text+ Entwicklungen auch weitere Angebote der zu Text+ beitragenden Partner. Wie Dienste Teil des Text+ Angebots werden, ist Gegenstand der [Text+ Services Policy, die als Version 0.9](https://dx.doi.org/10.5281/zenodo.12657204) Gegenstand projektinterner Diskussion und Weiterentwicklung ist. 

## Changelog

- upcoming: Differenzierung zwischen genuinen Text+ Angeboten sowie weiteren, für die Community relevanten Angeboten; Ergänzung von Förderkennzeichen in den Angebotsbeschreibungen
- Oktober 2024: Erweiterung der Sammlung auf 79 Ressourcen. Anreicherung und Kuration bestehender Beiträge. Ergänzung der Text+ Services Policy v0.9
- Juli 2024: Verlinkung der Anleitung und Beschreibungsrichtlinie für Text+ Dienste im SSH Open Marketplace; Ergänzung eines Filterings nach Kategorien und Keywords 
- Juni 2024: Erweiterung auf 35 Einträge sowie tlw. Überarbeitung individueller Beschreibungen sowie des Einleitungstextes dieser Seite
- Mai 2024: initiale Version einer Diensteliste mit 29 Einträgen
