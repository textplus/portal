---
title: Services

type: non-scrolling-toc

menu:
  main:
    weight: 30
    parent: daten-dienste

params:
  last_mod_today: true
---

# Tools and Services

Text+ and the participating institutions offer a wide range of services related to language and text data. In addition to research data, services and tools are an essential part of the Text+ offerings for users.

For the provision and maintenance of this service overview, Text+ uses
the [SSH Open Marketplace](https://marketplace.sshopencloud.eu/), a
discovery platform from the Social Sciences and Humanities Cluster
within the
[EOSC](https://research-and-innovation.ec.europa.eu/strategy/strategy-2020-2024/our-digital-future/open-science/european-open-science-cloud-eosc_en).

The list of offerings mentioned below is subject to constant
development and expansion. Both a search function and a filter function are implemented for convenient access. Feedback and requests can be directed to
the [Helpdesk](https://text-plus.org/helpdesk/) with the subject
Infrastructure/Operations.

{{< standout theme="white" col_width="9">}}
{{<service-list>}}
{{< /standout >}}

## Guidelines for the description of Text+ services on the SSH Open Marketplace 

This [short guideline](https://zenodo.org/records/12657204) documents how services affiliated with Text+ may be described in the SSH Open Marketplace.

## Service definition

The collection of resources on this website includes, in addition to genuine Text+ developments, further offerings from partners contributing to Text+. How services become part of the Text+ portfolio is addressed in the [Text+ Services Policy, Version 0.9](https://dx.doi.org/10.5281/zenodo.12657204), which is subject to ongoing internal project discussions and further development. 


## Changelog

- upcoming: differentiation between genuine Text+ offerings and other offerings relevant to the community; addition of funding references in the service descriptions
- October 2024: Expansion of the collection to 79 resources. Enhancement and curation of existing contributions. Update of the Text+ Services Policy to v0.9.
- July 2024: linking of the guide & description guidelines for Text+ services in the SSH Open Marketplace; addition of filtering by categories and keywords
- June 2024: expansion to 35 entries as well as partial revision of individual descriptions and the introductory text on this page
- May 2024: initial version of a service list with 29 entries
