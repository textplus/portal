## for local testing purposes

FROM node:lts-bookworm

LABEL maintainer="maik.wegener@gwdg.de"

ARG HUGO=hugo_extended
ARG HUGO_VERSION=0.145.0

RUN apt-get update && \
    apt-get -y install wget ca-certificates openssl tzdata git 

# install hugo_extended from https://github.com/gohugoio/hugo/releases
RUN wget -nv -O ${HUGO_VERSION}.tar.gz https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO}_${HUGO_VERSION}_Linux-64bit.tar.gz && \
    tar xf ${HUGO_VERSION}.tar.gz && mv hugo* /usr/bin/hugo && \
    hugo version

VOLUME /srv/hugo

EXPOSE 1313
WORKDIR /srv/hugo

COPY package.json .
COPY package-lock.json .

RUN git config --global --add safe.directory /srv/hugo

RUN npm install --loglevel verbose
# RUN npm uninstall textplus-fcs-vuetif && \
#     npm install --loglevel verbose git+https://git.saw-leipzig.de/text-plus/FCS/textplus-fcs-vuetify.git
# RUN npm uninstall textplus-chatbot-vue && \
#    npm install --loglevel verbose git+https://gitlab.gwdg.de/ubasara/textplus-chatbot-vue.git

# auto-refreshing hugo server as entry point
ENTRYPOINT ["hugo", "server", "--bind", "0.0.0.0", "--environment", "staging"]
