const purgecss = require('@fullhuman/postcss-purgecss')

module.exports = {
  plugins: [
    purgecss({
      content: ['./**/*.html', './**/*.ts', './**/*.js'],
      safelist: {
        greedy: [/modal|col/]
      }
    })
  ]
}
