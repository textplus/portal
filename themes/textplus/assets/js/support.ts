import { getCookie,setCookie } from "./util";

const endpoint = "9751f4bc-0258-4a5f-b276-1f3fa5d9fc0f";
const supportBackendUrl = "https://api.micros.academiccloud.de/support/api/v1/request"
// const supportBackendUrl = "https://stage.micros.gwdg.de/supports/api/v1/request"


// this obtains a token from the support API
const getFreshApiToken = async (endpoint: string): Promise<string | undefined> => {
    return fetch(
        supportBackendUrl + "/token/" + endpoint, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => {
            console.log(response)
            if (response.status == 200) return response.text()
            else if (response.status == 429) {
                showErrorMessage('{{ i18n "support-form-error-unable-to-load-token" }}')
                return undefined
            }
        })
        .catch (error => {
            showErrorMessage('{{ i18n "support-form-error-default-message" }}')
            return undefined
        })
}

const sendSupportRequest = async (endpoint: string, apiToken: string, name: string, mail: string, subject: string, message: string) => {
    fetch(supportBackendUrl + "/public/" + endpoint, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-API-Key': apiToken
        },
        body: JSON.stringify(
            {
                "fullname": name,
                "emailAddress": mail,
                "requestTitle": subject,
                "requestBody": message
            })
    })
        .then(response => handleServerResponse(response))
        .catch (error => {
            showErrorMessage('{{ i18n "support-form-error-default-message" }}')
            return undefined
        })
}

const sendSupportRequestWithAttachment = async (endpoint: string, apiToken: string, name: string, mail: string, subject: string, message: string, attachments: FileList) => {
    const formData = new FormData()
    formData.append("fullname", name)
    formData.append("emailAddress", mail)
    formData.append("requestTitle", subject)
    formData.append("requestBody", message)

    for (let i = 0; i < attachments.length; i++) {
        formData.append("files", attachments[i])
    }
    
    fetch(supportBackendUrl + "/public/" + endpoint + "/attachment", {
        method: 'POST',
        headers: {
            'X-API-Key': apiToken
        },
        body: formData
    })
        .then(response => handleServerResponse(response))
        .catch (error => {
            showErrorMessage('{{ i18n "support-form-error-default-message" }}')
            return undefined
        })
}

const handleServerResponse = async (response: Response) => {
    if (response.status == 201) {
        // success
        showSuccessMessage();
    }
    else if (response.status == 401) {
        // expired token, request new one
        updateApiCookie(endpoint)
        showErrorMessage('{{ i18n "support-form-error-token-invalid" }}')
    }
    else if (response.status == 429) {
        // too many requests (limit is 2 requests per 3 mins)
        showErrorMessage('{{ i18n "support-form-error-too-many-requests" }}')
    }
    else if (response.status == 500) {
        // server error
        showErrorMessage('{{ i18n "support-form-error-server-error" }}')
    }
}

const initApiTokenCookie = async (endpoint: string) => {
    const cookiename = "apitoken_" + endpoint;

    // check if cookie set (and valid) otherwise obtain 
    if (getCookie(cookiename).length == 0) {
        updateApiCookie(endpoint)
    }

    // update cookie every 4 minutes to make sure it is always valid
    setInterval(() => updateApiCookie(endpoint), 4 * 60 * 1000)
}

const updateApiCookie = async (endpoint: string) => {
    const cookiename = "apitoken_" + endpoint;
    const cookievalue = await getFreshApiToken(endpoint);
    const timevalid = 300;

    if (cookievalue !== undefined) {
        // write delayed because token gets valid after 10 seconds
        setTimeout(() => setCookie(cookiename, cookievalue, timevalid), 10 * 1000);
    }
}

const showErrorMessage = (message: string) => {
    hideSuccessMessage();
    const errorMessageParent = document.getElementById("support-form-error-field") as HTMLElement;
    errorMessageParent.classList.remove("d-none")
    errorMessageParent.textContent = message
}

const showSuccessMessage = () => {
    const successMessageParent = document.getElementById("support-form-success-field") as HTMLElement;
    successMessageParent.classList.remove("d-none")
    successMessageParent.textContent = '{{ i18n "support-form-message-submitted" }}'
}

const hideSuccessMessage = () => (document.getElementById("support-form-success-field") as HTMLElement).classList.add("d-none")

const hideErrorMessage = () => (document.getElementById("support-form-error-field") as HTMLElement).classList.add("d-none")

const validateFormInput = (name: string, mail: string, message: string, dppAccepted: boolean, title?: string, eventDateTime?: string, eventLocation?: string): string => {
    let errorMessages = Array<string>();
    const simpleEmailRe = /^\S+@\S+\.\S+$/; // see https://stackoverflow.com/a/9204568

    if (name.length < 5 || name.length > 200) {
        errorMessages.push('{{ i18n "support-form-error-name-invalid" }}')
    }

    if (!simpleEmailRe.test(mail)) {
        errorMessages.push('{{ i18n "support-form-error-mail-invalid" }}')
    }

    if (message != undefined && message == "") {
        errorMessages.push('{{ i18n "support-form-error-message-empty" }}')
    }

    if (title != undefined && title == "") {
        errorMessages.push('{{ i18n "support-form-error-title-empty" }}')
    }
    if (eventDateTime != undefined && eventDateTime == "") {
        errorMessages.push('{{ i18n "support-form-error-event-date-time-empty" }}')
    }

    if (eventLocation != undefined && eventLocation == "") {
        errorMessages.push('{{ i18n "support-form-error-event-location-empty" }}')
    }

    if (!dppAccepted) {
        errorMessages.push('{{ i18n "support-form-error-dpp-not-accepted" }}')
    }

    return errorMessages.join(" ")
}


const submitForm = (name: string, mail: string, subject: string, message: string, attachment: FileList) => {
    const apiToken = getCookie('apitoken_' + endpoint)
    if (apiToken.length > 0) {
        hideErrorMessage();
        if (attachment.length > 0) {
            sendSupportRequestWithAttachment(endpoint, apiToken, name, mail, subject, message, attachment);
        }
        else {
            sendSupportRequest(endpoint, apiToken, name, mail, subject, message);
        }
    }
    else {
        showErrorMessage('{{ i18n "support-form-error-token-invalid" }}')
    }
}


const setupSupportForm = () => {
    const supportFormSendButton = document.getElementById("support-form-send-button");
    supportFormSendButton?.addEventListener("click", () => {
        const name = (document.getElementById("support-form-name") as HTMLInputElement).value;
        const mail = (document.getElementById("support-form-email") as HTMLInputElement).value;
        const subject = (document.getElementById("support-form-textplus-area") as HTMLInputElement).value;
        const message = (document.getElementById("support-form-message") as HTMLInputElement).value;
        const dppAccepted = (document.getElementById("support-form-privacy-policy-box") as HTMLInputElement).checked;
        const attachments = (document.getElementById("support-form-attachments") as HTMLInputElement).files as FileList;

        const errorMessage = validateFormInput(name, mail, message, dppAccepted);
        if (errorMessage == "") {
            submitForm(name, mail, subject, message, attachments);
        }
        else {
            showErrorMessage(errorMessage)
        }
    });

    const supportFormDeleteAttachmentsButton = document.getElementById("support-form-delete-attachments-button");
    supportFormDeleteAttachmentsButton?.addEventListener("click", () => {
        (document.getElementById("support-form-attachments") as HTMLInputElement).value = '';
    });
}

const setupEventForm = () => {
    const eventFormSendButton = document.getElementById("event-form-send-button");
    eventFormSendButton?.addEventListener("click", () => {
        const name = (document.getElementById("event-form-name") as HTMLInputElement).value;
        const mail = (document.getElementById("event-form-email") as HTMLInputElement).value;
        const eventTitle = (document.getElementById("event-form-title") as HTMLInputElement).value;
        const eventDateTime = (document.getElementById("event-form-date-time") as HTMLInputElement).value;
        const eventDesc = (document.getElementById("event-form-desc") as HTMLInputElement).value;
        const eventLocation = (document.getElementById("event-form-location") as HTMLInputElement).value;
        const eventLocationUrl = (document.getElementById("event-form-location-url") as HTMLInputElement).value;
        const eventUrl = (document.getElementById("event-form-url") as HTMLInputElement).value;
        const eventImageAvailable = (document.getElementById("event-form-image-usable") as HTMLInputElement).checked;
        const dppAccepted = (document.getElementById("event-form-privacy-policy-box") as HTMLInputElement).checked;
        const attachments = (document.getElementById("event-form-attachments") as HTMLInputElement).files as FileList;

        const errorMessage = validateFormInput(name, mail, eventDesc, dppAccepted, eventTitle, eventDateTime, eventLocation);
        if (errorMessage == "") {
            // TODO: consider creating correct markdown from form for copy-paste into repo (pay attention to date formatting)
            const subject = "Neue Veranstaltung: " + eventTitle;
            const message = "Titel der Veranstaltung: " + eventTitle + "\n" +
                "Datum und Uhrzeit: " + eventDateTime + "\n" +
                "Ort: " + eventLocation + "\n" +
                "URL virtueller Raum: " + eventLocationUrl + "\n" +
                "Externe Veranstaltungs-URL: " + eventUrl + "\n" +
                "Bild vorhanden auf Infoseite/kann bereit gestellt werden: " + (eventImageAvailable ? "Ja" : "Nein") + "\n" +
                "Beschreibung: " + eventDesc;

                submitForm(name, mail, subject, message, attachments);
            }
        else {
            showErrorMessage(errorMessage)
        }
    });

    const eventFormDeleteAttachmentsButton = document.getElementById("event-form-delete-attachments-button");
    eventFormDeleteAttachmentsButton?.addEventListener("click", () => {
        (document.getElementById("event-form-attachments") as HTMLInputElement).value = '';
    });
}

const setupDataSubmissionForm = () => {
    const dsFormSendButton = document.getElementById("ds-form-send-button");
    dsFormSendButton?.addEventListener("click", () => {

        const dataSubmissionTitle = (document.getElementById("ds-form-title") as HTMLInputElement).value;
        const dataSubmissionDescription = (document.getElementById("ds-form-desc") as HTMLInputElement).value;
        const dataSubmissionTextplusArea = (document.getElementById("ds-form-textplus-area") as HTMLInputElement).value;
        const dataSubmissionSize = (document.getElementById("ds-form-file-size-estimation") as HTMLInputElement).value;
        const dataSubmissionCount = (document.getElementById("ds-form-file-count-estimation") as HTMLInputElement).value;
        const dataSubmissionPersonalData = (document.getElementById("ds-form-personal-data") as HTMLInputElement).value;
        const dataSubmissionThirdPartyData = (document.getElementById("ds-form-third-party-data") as HTMLInputElement).value;
        const dataSubmissionType = (document.getElementById("ds-form-data-type") as HTMLInputElement).value;
        const dataSubmissionLanguage = (document.getElementById("ds-form-language") as HTMLInputElement).value;
        const dataSubmissionProjectStatus = (document.getElementById("ds-form-project-status") as HTMLInputElement).value;
        const dataSubmissionPublicationTimeline = (document.getElementById("ds-form-publication-timeline") as HTMLInputElement).value;
        const dataSubmissionInternalProvision = (document.getElementById("ds-form-internal-provision") as HTMLInputElement).value;
        const dataSubmissionLicense = (document.getElementById("ds-form-pref-license") as HTMLInputElement).value;
        const dataSubmissionMessage = (document.getElementById("ds-form-message") as HTMLInputElement).value;
        const name = (document.getElementById("ds-form-name") as HTMLInputElement).value;
        const mail = (document.getElementById("ds-form-email") as HTMLInputElement).value;
        const dppAccepted = (document.getElementById("ds-form-privacy-policy-box") as HTMLInputElement).checked;
        const attachments = (document.getElementById("ds-form-attachments") as HTMLInputElement).files as FileList;

        const errorMessage = validateFormInput(name, mail, dataSubmissionDescription, dppAccepted, dataSubmissionTitle);
        if (errorMessage == "") {
            const subject = "Neue Datenaufnahme-Anfrage: " + dataSubmissionTitle;
            const message = "Titel: " + dataSubmissionTitle + "\n" +    
                "Beschreibung: " + dataSubmissionDescription + "\n" +
                "Text+ Arbeitsbereich: " + dataSubmissionTextplusArea + "\n" +
                "Nachricht: " + dataSubmissionMessage + "\n\n" +
                "Erweiterte Angaben: \n" +
                "Umfang der Daten (Schätzung): " + dataSubmissionSize + "\n" +
                "Anzahl der Dateien (Schätzung): " + dataSubmissionCount + "\n" +
                "Personenbezogene Daten: " + dataSubmissionPersonalData + "\n" +
                "Daten von Dritten/Verlagen: "  + dataSubmissionThirdPartyData + "\n" +
                "Art der Daten: " + dataSubmissionType + "\n" +
                "Sprache(n): " + dataSubmissionLanguage + "\n" +
                "Projektstatus: " + dataSubmissionProjectStatus + "\n" +
                "Geplanter Veröffentlichungszeitpunkt: " + dataSubmissionPublicationTimeline + "\n" +
                "Interne Bereitstellung/Embargo: " + dataSubmissionInternalProvision + "\n" +
                "Präferierte Lizenz: " + dataSubmissionLicense + "\n"; 

                submitForm(name, mail, subject, message, attachments);
            }
        else {
            showErrorMessage(errorMessage)
        }
    });
    const dataSubmissionFormDeleteAttachmentsButton = document.getElementById("ds-form-delete-attachments-button");
    dataSubmissionFormDeleteAttachmentsButton?.addEventListener("click", () => {
        (document.getElementById("ds-form-attachments") as HTMLInputElement).value = '';
    });
}

const setupForms = () => {
    initApiTokenCookie(endpoint);
    setupSupportForm();
    setupEventForm();
    setupDataSubmissionForm();
}

setupForms();