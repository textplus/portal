// TODO: remove when data submission form is ready

document.addEventListener("DOMContentLoaded", function () {
  // Initialize all tooltips
  var tooltipFields = document.querySelectorAll("[data-bs-toggle='tooltip']");
  tooltipFields.forEach(function (tooltipField) {
    new bootstrap.Tooltip(tooltipField, {
      trigger: "hover", // Tooltips show on hover
    });
  });

  // Initialize all popovers
  var popoverIcons = document.querySelectorAll("[data-bs-toggle='popover']");
  popoverIcons.forEach(function (popoverIcon) {
    var popover = new bootstrap.Popover(popoverIcon, {
      trigger: "focus", // Popovers show on focus (e.g., click or tab navigation)
    });

    // Ensure popover hides when clicking outside
    document.addEventListener("click", function (event) {
      if (!popoverIcon.contains(event.target)) {
        popover.hide();
      }
    });
  });
});
