export function getCookie(cookieName: string) {
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    const cookie = ca.filter(entry => entry.includes(cookieName))
    if (cookie.length != 0) {
        return cookie[0].split('=')[1].trim()
    }
    return ""
}

export function setCookie(cookieName: string, cookieValue: string, timeValid = 0) {
    var expires = ""
    if (timeValid > 0) {
        const d = new Date();
        d.setTime(d.getTime() + (timeValid * 1000));
        expires = "Expires=" + d.toUTCString() + ";";
    }
    const samesite = "SameSite=Strict"
    document.cookie = cookieName + "=" + cookieValue + ";" + expires + samesite + ";path=/";
}
