// auto scrolling TOC
const setUpScrollingSidebarImage = () => {
    let sticky = document.getElementById("sidebar-image-floating")
    if (sticky) {
        let stickyAnchor = sticky.parentNode as Element
        let state = false

        function getAnchorOffset() {
            return stickyAnchor.getBoundingClientRect().top
        }

        const updateSticky = () => {
            // We need to get the width of the toc div and apply it to the
            // sticky class to prevent a change in width when postion:sticky is set
            let tocWrapper = document.getElementById("sidebar-image-floating")

            if (sticky && tocWrapper) {
                var tocDivWidth = tocWrapper.offsetWidth
                if (!state && getAnchorOffset() < 51) {
                    sticky.classList.add("is-sticky")
                    sticky.style.width = tocDivWidth + "px"
                    state = true
                } else if (state && getAnchorOffset() >= 51) {
                    sticky.classList.remove("is-sticky")
                    sticky.style.width = ""
                    state = false
                }
            }
        }

        window.addEventListener("scroll", updateSticky)
        window.addEventListener("resize", updateSticky)

        updateSticky()
    }
}

setUpScrollingSidebarImage();