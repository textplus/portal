import * as bootstrap from "js/bootstrap.bundle.min.js";
// import "js/bootstrap.bundle.min.js";
window.bootstrap = bootstrap;

// @ts-ignore: process.env.HUGO_ENVIRONMENT is replaced in the head.html
const isDev: boolean = process.env.HUGO_ENVIRONMENT === "development";

/**
 * Logs a message to the console if we are in development mode.
 */
const logInfo = (...msg: any[]) => {
  if (isDev) console.log(...msg);
};

const setUpScrollingNav = () => {
  let navigation = document.querySelector(".nav-medium");
  window.addEventListener("scroll", () => {
    if (document.body.scrollTop > 140 || document.documentElement.scrollTop > 140) {
      navigation?.classList.add("fadein");
      navigation?.classList.remove("d-none");
    } else {
      navigation?.classList.add("d-none");
    }
  });
};

setUpScrollingNav();

const setupTooltips = () => {
  const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
  const tooltipList = [...tooltipTriggerList].map(
    (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl),
  );
};
setupTooltips();
