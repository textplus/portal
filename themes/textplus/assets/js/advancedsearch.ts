import { Search, useDialogStore, useFcsSearchStore, createApp } from "textplus-fcs-vuetify";
import {
  FCS_REST_BASE_URI,
  FCS_UI_BASE_URI,
  TEXTPLUS_PORTAL_SEARCH_DOCUMENTS,
  FCS_SERVICE_BLOCK,
} from "textplus-fcs-vuetify";

// injected config params from HUGO
import * as params from "@params";

createApp(Search)
  // configure API endpoints
  .provide(FCS_REST_BASE_URI, params.FCS_REST_BASE_URI)
  .provide(FCS_UI_BASE_URI, params.FCS_UI_BASE_URI)
  .provide(TEXTPLUS_PORTAL_SEARCH_DOCUMENTS, params.TEXTPLUS_PORTAL_SEARCH_DOCUMENTS)
  // customization
  .provide(FCS_SERVICE_BLOCK, params.FCS_SERVICE_BLOCK)
  // mount app to HTML DOM
  .mount("#advanced-search-app");

// temp(?) fix: add id to v-overlay-container due to PostCSS prefixing/pruning
// see: https://github.com/vuetifyjs/vuetify/blob/master/packages/vuetify/src/composables/teleport.ts#L8
let container = document.body.querySelector(":scope > .v-overlay-container");
if (!container) {
  container = document.createElement("div");
  container.className = "v-overlay-container";
  document.body.appendChild(container);
}
container.id = "advanced-search-dialog";

// expose open dialog functionality to vanillaJS
const dialog = useDialogStore();
const fcssearch = useFcsSearchStore();
const openDialog = dialog.open;
const openFromPermaUrl = dialog.openFromPermaUrl;
const switchLocale = dialog.switchLocale;

// enable perma urls
dialog.enablePermaURLs = true;
dialog.permaUrlIsHash = true;
dialog.permaUrlHashAction = "action-open-search";

// enable search history
fcssearch.enableSearchHistory = true;

const advancedsearch = {
  openDialog,
  openFromPermaUrl,
  switchLocale,
};
Object.assign(window, { advancedsearch });

// switch to language for dialog based on <html lang="de">
const language = document.documentElement.lang;
if (!!language) {
  advancedsearch.switchLocale(language);
}

// bind to button(s)
const setupSearchButton = () => {
  const searchButtons = document.getElementsByClassName("btn-search");
  for (var button of searchButtons) {
    if (button?.classList.contains("btn-data-search")) continue;
    button?.addEventListener("click", () => {
      advancedsearch.openDialog({ tab: "website" });
    });
  }

  const dataSearchButtons = document.getElementsByClassName("btn-data-search");
  for (var button of dataSearchButtons) {
    button?.addEventListener("click", () => {
      advancedsearch.openDialog({ tab: "content" });
    });
  }
};

setupSearchButton();

// instant search box opening on initial page opening using hash url
const checkURlAndOpenSearchModal = () => {
  const hashUrl = document.location.hash.substring(1);
  if (!hashUrl) return;
  // legacy actions, compatibility with old QR codes and paper URLs
  if (hashUrl === "action-open-search-content") {
    advancedsearch.openDialog({ tab: "content" });
  } else if (hashUrl === "action-open-search-website") {
    advancedsearch.openDialog({ tab: "website" });
  } else {
    // new full-featured perma-url evaluation
    dialog.openFromPermaUrl();
  }
};

checkURlAndOpenSearchModal();
// also hook to URL hashchange event to trigger search dialog if required
window.addEventListener("hashchange", (event) => checkURlAndOpenSearchModal());
