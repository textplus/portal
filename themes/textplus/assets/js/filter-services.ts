import List from "list.js";

const activeBtnClass = "btn-secondary";
const inactiveBtnClass = "btn-outline-secondary";

const currentActiveActivityFilters: Set<string> = new Set<string>();
const currentKeywordFilters: Set<string> = new Set<string>();

var options = {
  valueNames: ["name", "activities", "keywords", "short_description"],
};
const serviceList = new List("service-list", options);

const toggleBtnState = (btn: HTMLElement): Boolean => {
  if (btn.classList.contains(activeBtnClass)) {
    btn.classList.remove(activeBtnClass);
    btn.classList.add(inactiveBtnClass);
    return false;
  } else {
    btn.classList.remove(inactiveBtnClass);
    btn.classList.add(activeBtnClass);
    return true;
  }
};

const toggleFilterListView = (filterListId: string) => {
  let fullFilterList = document.getElementById(filterListId + "-filter-list-full")!;
  let showMoreButton = document.getElementById(filterListId + "-list-button-show-more")!;
  let showLessButton = document.getElementById(filterListId + "-list-button-show-less")!;

  let isPreviewListActive = document
    .getElementById(filterListId + "-filter-list-full")!
    .classList.contains("d-none");

  if (isPreviewListActive) {
    fullFilterList.classList.remove("d-none");
    showLessButton.classList.remove("d-none");

    showMoreButton.classList.add("d-none");
  } else {
    showMoreButton.classList.remove("d-none");

    fullFilterList.classList.add("d-none");
    showLessButton.classList.add("d-none");
  }
};

const filterService = (item: any): Boolean => {
  if (currentActiveActivityFilters.size == 0 && currentKeywordFilters.size == 0) return true;

  console.log(currentActiveActivityFilters);
  console.log(currentKeywordFilters);

  for (let activity of currentActiveActivityFilters) {
    if (!item.values().activities.includes(activity))
      return false
  }

  for (let keyword of currentKeywordFilters) {
    if (!item.values().keywords.includes(keyword))
      return false
  }

  return true;
};

const setUpFilterButtons = () => {
  document.getElementById("filter-activities-container")!.addEventListener("click", function (e) {
    if (e.target && (e.target as HTMLElement).nodeName == "BUTTON") {
      let clickedBtn = e.target as HTMLElement;
      let isButtonActive = toggleBtnState(clickedBtn);
      let currentTag = clickedBtn.dataset.tag!;

      if (isButtonActive) currentActiveActivityFilters.add(currentTag);
      else currentActiveActivityFilters.delete(currentTag);

      serviceList.filter(filterService);
    }
  });

  document.getElementById("filter-keywords-container")!.addEventListener("click", function (e) {
    if (e.target && (e.target as HTMLElement).nodeName == "BUTTON") {
      let clickedBtn = e.target as HTMLElement;
      let isButtonActive = toggleBtnState(clickedBtn);
      let currentTag = clickedBtn.dataset.tag!;

      if (isButtonActive) currentKeywordFilters.add(currentTag);
      else currentKeywordFilters.delete(currentTag);

      serviceList.filter(filterService);
    }
  });
};

const setUpShowMoreLessButtons = () => {
  document
    .getElementById("activity-list-button-show-more")!
    .addEventListener("click", (_) => toggleFilterListView("activity"));

  document
    .getElementById("activity-list-button-show-less")!
    .addEventListener("click", (_) => toggleFilterListView("activity"));

  document
    .getElementById("keyword-list-button-show-more")!
    .addEventListener("click", (_) => toggleFilterListView("keyword"));

  document
    .getElementById("keyword-list-button-show-less")!
    .addEventListener("click", (_) => toggleFilterListView("keyword"));
};

setUpFilterButtons();
setUpShowMoreLessButtons();
