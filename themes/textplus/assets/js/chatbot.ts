import {
  App,
  createApp,
  runAuth,
  createOidcSettings,
  CHAT_APP_API_URL,
  CHAT_APP_HOME_PATH,
  modelStore,
  conversationsStore,
  createStore,
  createRouterInstance,
} from "textplus-chatbot-vue";

import * as params from "@params";

// Merge the customOidcStore into the oidcStoreModule
const store = createStore({
  modules: {
    conversationsStore,
    modelStore,
  },
});

const oidcSettings = createOidcSettings({
  authority: `${params.CHATBOT_OIDC_BASE_URL}/auth/realms/${params.CHATBOT_OIDC_REALM}`,
  clientId: params.CHATBOT_OIDC_CLIENT_ID,
  redirectUri: `${window.location.origin}${params.CHATBOT_OIDC_REDIRECT_PATH}`
});
console.log(`${window.location.origin}${params.CHATBOT_OIDC_REDIRECT_PATH}`);
runAuth(oidcSettings);

// Create router with base path
const router = createRouterInstance(params.CHATBOT_APP_HOME_PATH, params.CHATBOT_OIDC_REDIRECT_PATH);

createApp(App)
  .provide(CHAT_APP_API_URL, params.CHATBOT_API_URL)
  .provide(CHAT_APP_HOME_PATH, params.CHATBOT_APP_HOME_PATH)
  .use(store)
  .use(router)
  .mount("#textplus-ai-assistant");
