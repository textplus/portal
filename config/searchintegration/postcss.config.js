const prefixer = require("postcss-prefix-selector");

const myRemovePrefix = "#totalnonse675";
const myPrefix = "#advanced-search-dialog";

module.exports = {
  plugins: [
    prefixer({
      prefix: myPrefix,
      // html, body, *, ::before/::after
      // /\.v-/
      transform: function (prefix, selector, prefixedSelector, filePath, rule) {
        // keep same if nested or with attribute or id or vuetify-prefix
        if (/>|\[|^\.v-|#/.test(selector)) return selector;
        // remove global by prefixing nonsense
        if (/\*/.test(selector)) return `${myRemovePrefix} > ${selector}`;
        // if from bootstrap
        if (/^\.border$/.test(selector)) return prefixedSelector;
        let ruleContent = rule.toString();
        // contains variable
        if (/--v-/.test(ruleContent)) return selector;
        // remove certain reset rules for buttons
        if (/button/.test(selector)) {
          // `font: inherit;`
          if (/font:\s*inherit/.test(ruleContent)) return `${myRemovePrefix} > ${selector}`;
          // `text-transform: none;`
          if (/text-transform:\s*none/.test(ruleContent)) return `${myRemovePrefix} > ${selector}`;
          // border/background reset
          if (/background-color:\s*transparent/.test(ruleContent) && /border-style:\s*none/.test(ruleContent)) return `${myRemovePrefix} > ${selector}`;
        }
        // default with prefix
        return prefixedSelector;
      },
    }),
    // clean up rules
    // (function (root) {
    //   root.walkRules(rule => {
    //     if (rule.selector.startsWith(`${myRemovePrefix} > `)) rule.remove();
    //   });
    // }),
    // add some overrides to fix stuff
    (function (root) {
      // from https://stackoverflow.com/a/73704784/9360161
      function trimIndent(strings, ...values) {
        const result = new Array(strings.length);
        result[0] = strings[0].replace(/^\n/, '');
        const endIndex = strings.length - 1;
        result[endIndex] = strings[endIndex].replace(/\n$/, '');

        var indent = result[0].match(/^\s+/g);
        result[0] = result[0].replace(/^\s+/, '');

        for (let i = 0, m = result.length; i < m; i++) {
          var input = result[i] ?? strings[i];
          result[i] = input.replace(new RegExp('\n' + indent, "gm"), '\n');
        }

        return result.flatMap((value, index) => {
          return value + (index < values.length ? values[index] : '')
        }).join("");
      }

      // add some of the global styles
      root.append(trimIndent`
      ${myPrefix} * {
        background-repeat:no-repeat;
        box-sizing:inherit;
      }
      `);
      // generall button styles, e.g. v-expansion-panels, v-chips
      root.append(trimIndent`
      ${myPrefix} button:not(.v-btn) {
        border-style: none;
        background-color: transparent;
      }
      `);
      /* v-chip close buttons */
      root.append(trimIndent`
      ${myPrefix} button.v-chip__close {
        margin: 0;
      }
      `);
      // revert bootstrap styles (primarily for help section content)
      root.append(trimIndent`
      ${myPrefix} p {
        margin: 0;
        padding: 0;
      }
      ${myPrefix} h1,
      ${myPrefix} h2,
      ${myPrefix} h3,
      ${myPrefix} h4,
      ${myPrefix} h5,
      ${myPrefix} h6 {
        font-size: revert;
        line-height: revert;
      }
      ${myPrefix} hr {
        margin: 0;
        padding: 0;
        color: revert;
      }
      `);
    }),
  ],
};
